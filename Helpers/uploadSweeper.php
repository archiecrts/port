<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

// sweeps up all files older than 24 hours to save space.

// Uploads Folder
$dir = "./upload";
$files = scandir($dir);

foreach ($files as $upload) {
    if (strlen($upload) > 2) {
        if (filemtime($dir . "/" . $upload) < (time() - (3600 * 24))) {
            unlink($dir . "/" . $upload);
        }
    }
}

// download folder.
$dir1 = "./download";
$files1 = scandir($dir1);

foreach ($files1 as $download) {
    if (strlen($download) > 2) {

        if (filemtime($dir1 . "/" . $download) < (time() - (3600 * 24))) {
            unlink($dir1 . "/" . $download);
        }
    }
}