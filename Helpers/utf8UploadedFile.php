<?php
/**
 * CSV files are in a random encoded format and need to be converted to UTF8.
 * This takes the contents of the file and turns it into UTF8. This doesn't solve
 * the whole problem though. I still have to encode the insert into the installations table
 * as latin1 so that the german characters are properly encoded.
 * 
 * @param STRING $filename origial file name
 * @return STRING new filename.
 */
// change every line in the file to UTF8.
function covertToUTF8($filename) {
    $line = file_get_contents ($filename);

    $utf8Line = iconv( mb_detect_encoding( utf8_encode($line) ), 'UTF-8', utf8_encode($line) );
    
    $time = time();
    $newFile = fopen("upload/UTF8".$time.".csv", "a");
    fwrite($newFile, $utf8Line);
    fclose($newFile);

    unlink($filename);
    // Should really turn this back into the original file name.
    return "upload/UTF8".$time.".csv";
}