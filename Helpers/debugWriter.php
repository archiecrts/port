<?php

/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
function debugWriter($file, $error) {
    $handle = fopen("./logs/".$file, "a");
    fwrite($handle, date("Y-m-d H:i:s", time()) ." " .$error."\r\n");
    fclose($handle);
}