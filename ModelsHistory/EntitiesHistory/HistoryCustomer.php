<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryCustomer
 *
 * @author Archie
 */
class HistoryCustomer {
    public $customerHistoryID;
    public $customerID;
    public $customerName;
    public $customerTelephone;
    public $customerFax;
    public $customerWebsite;
    public $customerAccountManager;
    public $customerTBHID;
    public $customerTBHAccountCode;
    public $customerLDID;
    public $customerLDAccountCode;
    public $customerHeaderNotes;
    public $seawebCode;
    public $parentSeawebCode;
    public $parentNationalityOfControl;
    public $companyStatus;
    public $companyFullName;
    public $navisionID;
    public $customerHistoryChangedByID;
    public $customerHistoryDateChanged;
    
    
    public function __construct($customerHistoryID, $customerID, $customerName, 
            $customerTelephone, $customerFax, $customerWebsite, 
            $customerAccountManager, $customerTBHID, $customerTBHAccountCode, 
            $customerLDID, $customerLDAccountCode, $customerHeaderNotes,
            $seawebCode, $parentSeawebCode, 
            $parentNationalityOfControl, $companyStatus, $companyFullName, $navisionID, 
            $customerHistoryChangedByID, $customerHistoryDateChanged) {
        
            $this->customerHistoryID = $customerHistoryID; 
            $this->customerID = $customerID; 
            $this->customerName = $customerName; 
            $this->customerTelephone = $customerTelephone; 
            $this->customerFax = $customerFax; 
            $this->customerWebsite = $customerWebsite; 
            $this->customerAccountManager = $customerAccountManager; 
            $this->customerTBHID = $customerTBHID; 
            $this->customerTBHAccountCode = $customerTBHAccountCode; 
            $this->customerLDID = $customerLDID; 
            $this->customerLDAccountCode = $customerLDAccountCode; 
            $this->customerHeaderNotes = $customerHeaderNotes;
            $this->seawebCode = $seawebCode;
            $this->parentSeawebCode = $parentSeawebCode;
            $this->parentNationalityOfControl = $parentNationalityOfControl;
            $this->companyStatus = $companyStatus;
            $this->companyFullName = $companyFullName;
            $this->navisionID = $navisionID;
            $this->customerHistoryChangedByID = $customerHistoryChangedByID; 
            $this->customerHistoryDateChanged = $customerHistoryDateChanged;
    }
}
