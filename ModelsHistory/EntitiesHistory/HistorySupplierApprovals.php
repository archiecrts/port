<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistorySupplierApprovals
 *
 * @author Archie
 */
class HistorySupplierApprovals {
    public $supplierApprovalsHistoryID;
    public $supplierApprovalsID;
    public $customerTblCustID;
    public $satApprovedAsSupplier;
    public $satApprovedAsTrader;
    public $satApprovedAsAgent;
    public $supplierApprovalStatusLookupID;
    public $satDateApprovalStatusConfirmed;
    public $satStatusReviewDate;
    public $satReasonForStatusChange;
    public $supplierApprovalsHistoryChangedByID;
    public $supplierApprovalsHistoryChangedDate;
    
    public function __construct($supplierApprovalsHistoryID, $supplierApprovalsID, $customerTblCustID,
     $satApprovedAsSupplier, $satApprovedAsTrader, $satApprovedAsAgent,
     $supplierApprovalStatusLookupID, $satDateApprovalStatusConfirmed, $satStatusReviewDate,
     $satReasonForStatusChange, $supplierApprovalsHistoryChangedByID, $supplierApprovalsHistoryChangedDate) {
        
        $this->supplierApprovalsHistoryID = $supplierApprovalsHistoryID;
        $this->supplierApprovalsID = $supplierApprovalsID;
        $this->customerTblCustID = $customerTblCustID;
        $this->satApprovedAsSupplier = $satApprovedAsSupplier;
        $this->satApprovedAsTrader = $satApprovedAsTrader;
        $this->satApprovedAsAgent = $satApprovedAsAgent;
        $this->supplierApprovalStatusLookupID = $supplierApprovalStatusLookupID;
        $this->satDateApprovalStatusConfirmed = $satDateApprovalStatusConfirmed;
        $this->satStatusReviewDate = $satStatusReviewDate;
        $this->satReasonForStatusChange = $satReasonForStatusChange;
        $this->supplierApprovalsHistoryChangedByID = $supplierApprovalsHistoryChangedByID;
        $this->supplierApprovalsHistoryChangedDate = $supplierApprovalsHistoryChangedDate;
    }
}
