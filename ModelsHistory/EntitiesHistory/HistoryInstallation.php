<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryInstallation
 *
 * @author Archie
 */
class HistoryInstallation {
    public $installationHistoryID;
    public $installationID;
    public $installationType;
    public $installation;
    public $installationName;
    public $installationIMO;
    public $installationStatusID;
    public $installationOwnerParentCompany;
    public $installationTechnicalManager;
    public $installationOriginalSource;
    public $installationOriginalSourceDate;
    public $installationPhotoLink;
    public $installationHullNumber;
    public $installationHullType;
    public $installationYardBuilt;
    public $installationDeadweight;
    public $installationLengthOverall;
    public $installationBeamExtreme;
    public $installationBeamMoulded;
    public $installationBuiltDate;
    public $installationDraught;
    public $installationGrossTonnage;
    public $installationLength;
    public $installationPropellerType;
    public $installationPropulsionUnitCount;
    public $installationShipBuilder;
    public $installationTEU;
    public $installationBuildYear;
    public $installationArchiveFlag;
    public $installationArchiveReason;
    public $customerAddressID;
    public $flagName;
    public $leadShipByIMO;
    public $shipTypeLevel4;
    public $classificationSociety;
    public $installationHistoryChangedByID;
    public $installationHistoryDateChanged;
    
    public function __construct($installationHistoryID, $installationID, $installationType, $installation,
            $installationName, $installationIMO, $installationStatusID, $installationOwnerParentCompany,
            $installationTechnicalManager, $installationOriginalSource, $installationOriginalSourceDate,  
            $installationPhotoLink, $installationHullNumber, $installationHullType, 
            $installationYardBuilt, $installationDeadweight, $installationLengthOverall, 
            $installationBeamExtreme, $installationBeamMoulded, $installationBuiltDate, 
            $installationDraught, $installationGrossTonnage, 
            $installationLength, $installationPropellerType, $installationPropulsionUnitCount, 
            $installationShipBuilder, $installationTEU, $installationBuildYear,
            $installationArchiveFlag, $installationArchiveReason, $customerAddressID,$flagName,
            $leadShipByIMO, $shipTypeLevel4, $classificationSociety,
            $installationHistoryChangedByID, $installationHistoryDateChanged) {
        
            $this->installationHistoryID = $installationHistoryID; 
            $this->installationID = $installationID; 
            $this->installationType = $installationType; 
            $this->installation = $installation; 
            $this->installationName = $installationName; 
            $this->installationIMO = $installationIMO; 
            $this->installationStatusID = $installationStatusID; 
            $this->installationOwnerParentCompany = $installationOwnerParentCompany;
            $this->installationTechnicalManager = $installationTechnicalManager; 
            $this->installationOriginalSource = $installationOriginalSource; 
            $this->installationOriginalSourceDate = $installationOriginalSourceDate; 
            $this->installationPhotoLink = $installationPhotoLink;
            $this->installationHullNumber = $installationHullNumber;
            $this->installationHullType = $installationHullType;
            $this->installationYardBuilt = $installationYardBuilt;
            $this->installationDeadweight = $installationDeadweight;
            $this->installationLengthOverall = $installationLengthOverall;
            $this->installationBeamExtreme = $installationBeamExtreme;
            $this->installationBeamMoulded = $installationBeamMoulded;
            $this->installationBuiltDate = $installationBuiltDate;
            $this->installationDraught = $installationDraught;
            $this->installationGrossTonnage = $installationGrossTonnage;
            $this->installationLength = $installationLength;
            $this->installationPropellerType = $installationPropellerType;
            $this->installationPropulsionUnitCount = $installationPropulsionUnitCount;
            $this->installationShipBuilder = $installationShipBuilder;
            $this->installationTEU = $installationTEU;
            $this->installationBuildYear = $installationBuildYear;
            $this->installationArchiveFlag = $installationArchiveFlag;
            $this->installationArchiveReason = $installationArchiveReason;
            $this->customerAddressID = $customerAddressID;
            $this->flagName = $flagName;
            $this->leadShipByIMO = $leadShipByIMO;
            $this->shipTypeLevel4 = $shipTypeLevel4;
            $this->classificationSociety = $classificationSociety;
            $this->installationHistoryChangedByID = $installationHistoryChangedByID; 
            $this->installationHistoryDateChanged = $installationHistoryDateChanged;
        
    }
}
