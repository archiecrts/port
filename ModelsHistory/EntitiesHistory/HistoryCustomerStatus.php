<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryCustomerStatus
 *
 * @author Archie
 */
class HistoryCustomerStatus {
    public $customerStatusHistoryID;
    public $customerStatusID;
    public $customerID;
    public $statusLookupID;
    public $customerStatusHistoryChangedByID;
    public $customerStatusHistoryDateChanged;
    
    
    public function __construct($customerStatusHistoryID, $customerStatusID, $customerID, 
            $statusLookupID, $customerStatusHistoryChangedByID, $customerStatusHistoryDateChanged) {
        $this->customerStatusHistoryID = $customerStatusHistoryID;
        $this->customerStatusID = $customerStatusID;
        $this->customerID = $customerID;
        $this->statusLookupID = $statusLookupID;
        $this->customerStatusHistoryChangedByID = $customerStatusHistoryChangedByID;
        $this->customerStatusHistoryDateChanged = $customerStatusHistoryDateChanged;
    }
}
