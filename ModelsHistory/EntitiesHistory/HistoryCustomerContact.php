<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryCustomerContact
 *
 * @author Archie
 */
class HistoryCustomerContact {
    public $contactHistoryID;
    public $contactID;
    public $salutation;
    public $firstName;
    public $surname;
    public $contactJobTitle;
    public $contactEmail;
    public $contactNumber;
    public $contactMobileNumber;
    public $contactInfoSource;
    public $contactInactive;
    public $contactValidated;
    public $customerID;
    public $addressID;
    public $contactHistoryChangedByID;
    public $contactHistoryDateChanged;
    
    /**
     * 
     * @param INT $contactHistoryID
     * @param INT $contactID
     * @param STRING $salutation
     * @param STRING $firstName
     * @param STRING $surname
     * @param STRING $contactJobTitle
     * @param STRING $contactEmail
     * @param STRING $contactNumber
     * @param STRING $contactMobileNumber
     * @param STRING $contactInfoSource
     * @param STRING $contactInactive
     * @param STRING $contactValidated
     * @param INT $customerID
     * @param INT $addressID
     * @param INT $contactHistoryChangedByID
     * @param DATETIME $contactHistoryDateChanged
     */
    public function __construct($contactHistoryID, $contactID, $salutation, $firstName, 
            $surname, $contactJobTitle, $contactEmail, $contactNumber, $contactMobileNumber, $contactInfoSource,
            $contactInactive, $contactValidated, $customerID, $addressID, 
            $contactHistoryChangedByID, $contactHistoryDateChanged) {
        
        $this->contactHistoryID = $contactHistoryID; 
        $this->contactID = $contactID; 
        $this->salutation = $salutation;
        $this->firstName = $firstName; 
        $this->surname = $surname;
        $this->contactJobTitle = $contactJobTitle; 
        $this->contactEmail = $contactEmail; 
        $this->contactNumber = $contactNumber; 
        $this->contactMobileNumber = $contactMobileNumber;
        $this->contactInfoSource = $contactInfoSource;
        $this->contactInactive = $contactInactive; 
        $this->contactValidated = $contactValidated; 
        $this->customerID = $customerID; 
        $this->addressID = $addressID; 
        $this->contactHistoryChangedByID = $contactHistoryChangedByID; 
        $this->contactHistoryDateChanged = $contactHistoryDateChanged;
    }
}
