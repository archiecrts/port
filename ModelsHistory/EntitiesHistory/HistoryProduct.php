<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryProduct
 *
 * @author Archie
 */
class HistoryProduct {
    public $productHistoryID;
    public $productID;
    public $productSerialNumber;
    public $productDescription;
    public $productDrawingNumber;
    public $productComments;
    public $productEnquiryOnlyFlag;
    public $customerTblCustID;
    public $installationsTblInstID;
    public $versionLookupTblVltID;
    public $productUnitName;
    public $sourceAliasID;
    public $productHistoryChangedByID;
    public $productHistoryChangedDate;
    
    public function __construct($productHistoryID, $productID, $productSerialNumber,
     $productDescription, $productDrawingNumber, $productComments,
     $productEnquiryOnlyFlag, $customerTblCustID, $installationsTblInstID,
     $versionLookupTblVltID, $productUnitName, $sourceAliasID, $productHistoryChangedByID, 
     $productHistoryChangedDate) {
        
        $this->productHistoryID = $productHistoryID;
        $this->productID = $productID;
        $this->productSerialNumber = $productSerialNumber;
        $this->productDescription = $productDescription;
        $this->productDrawingNumber = $productDrawingNumber;
        $this->productComments = $productComments;
        $this->productEnquiryOnlyFlag = $productEnquiryOnlyFlag;
        $this->customerTblCustID = $customerTblCustID;
        $this->installationsTblInstID = $installationsTblInstID;
        $this->versionLookupTblVltID = $versionLookupTblVltID;
        $this->productUnitName = $productUnitName;
        $this->sourceAliasID = $sourceAliasID;
        $this->productHistoryChangedByID = $productHistoryChangedByID;
        $this->productHistoryChangedDate = $productHistoryChangedDate;
    }
}
