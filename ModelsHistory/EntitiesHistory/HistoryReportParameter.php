<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryReportParameter
 *
 * @author Archie
 */
class HistoryReportParameter {
    public $parameterHistoryID;
    public $parameterID;
    public $reportID;
    public $parameter;
    public $parameterValue;
    public $parameterHistoryChangedByID;
    public $parameterHistoryDateChanged;
    
    public function __construct($parameterHistoryID, $parameterID, $reportID, $parameter, $parameterValue,
            $parameterHistoryChangedByID, $parameterHistoryDateChanged) {
        $this->parameterHistoryID = $parameterHistoryID;
        $this->parameterID = $parameterID;
        $this->reportID = $reportID;
        $this->parameter = $parameter;
        $this->parameterValue = $parameterValue;
        $this->parameterHistoryChangedByID = $parameterHistoryChangedByID;
        $this->parameterHistoryDateChanged = $parameterHistoryDateChanged;
        
    }
}
