<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryNoticeBoard
 *
 * @author Archie
 */
class HistoryNoticeBoard {
    public $noticeHistoryID;
    public $noticeBoardID;
    public $noticeBoardTitle;
    public $noticeBoardDate;
    public $noticeBoardContent;
    public $noticeBoardArchive;
    public $noticeBoardAuthorised;
    public $noticeBoardUserID;
    public $noticeBoardCategoryID;
    public $noticeBoardDocumentID;
    public $noticeHistoryChangedByID;
    public $noticeHistoryDateChanged;
    
    public function __construct($noticeHistoryID, $noticeBoardID, $noticeBoardTitle, 
            $noticeBoardDate, $noticeBoardContent, $noticeBoardArchive, $noticeBoardAuthorised,
            $noticeBoardUserID, $noticeBoardCategoryID, $noticeBoardDocumentID,
            $noticeHistoryChangedByID, $noticeHistoryDateChanged) {
        $this->noticeHistoryID = $noticeHistoryID;
        $this->noticeBoardID = $noticeBoardID;
        $this->noticeBoardTitle = $noticeBoardTitle;
        $this->noticeBoardDate = $noticeBoardDate;
        $this->noticeBoardContent = $noticeBoardContent;
        $this->noticeBoardArchive = $noticeBoardArchive;
        $this->noticeBoardAuthorised = $noticeBoardAuthorised;
        $this->noticeBoardUserID = $noticeBoardUserID;
        $this->noticeBoardCategoryID = $noticeBoardCategoryID;
        $this->noticeBoardDocumentID = $noticeBoardDocumentID;
        $this->noticeHistoryChangedByID = $noticeHistoryChangedByID;
        $this->noticeHistoryDateChanged = $noticeHistoryDateChanged;
    }
}
