<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryInstallationEngine
 *
 * @author Archie
 */
class HistoryInstallationEngine {
    public $engineHistoryID;
    public $engineID;
    public $engineModelDescription;
    public $engineMainAux;
    public $engineVerified;
    public $engineOutput;
    public $engineOutputUnitOfMeasure;
    public $engineFuelType;
    public $unitName;
    public $engineBoreSize;
    public $engineStroke;
    public $engineReleaseDate;
    public $engineEnquiryOnly;
    public $loadPriorityID;
    public $productID;
    public $positionIfMain;
    public $engineBuilder;
    public $engineHistoryChangedByID;
    public $engineHistoryDateChanged;
    
    public function __construct($engineHistoryID, $engineID, 
            $engineModelDescription, $engineMainAux,
             $engineVerified,  $engineOutput, $engineOutputUnitOfMeasure,
            $engineFuelType, $unitName, $engineBoreSize, $engineStroke, $engineReleaseDate,
            $engineEnquiryOnly,  $loadPriorityID, $productID, $positionIfMain, $engineBuilder, 
            $engineHistoryChangedByID, $engineHistoryDateChanged) {
        
        $this->engineHistoryID = $engineHistoryID;
        $this->engineID = $engineID;
        $this->engineModelDescription = $engineModelDescription;
        $this->engineMainAux = $engineMainAux;
        $this->engineVerified = $engineVerified;
        $this->engineOutput = $engineOutput;
        $this->engineOutputUnitOfMeasure = $engineOutputUnitOfMeasure;
        $this->engineFuelType = $engineFuelType;
        $this->unitName = $unitName;
        $this->engineBoreSize = $engineBoreSize;
        $this->engineStroke = $engineStroke;
        $this->engineReleaseDate = $engineReleaseDate;
        $this->engineEnquiryOnly = $engineEnquiryOnly;
        $this->loadPriorityID = $loadPriorityID;
        $this->productID = $productID;
        $this->positionIfMain = $positionIfMain;
        $this->engineBuilder = $engineBuilder;
        $this->engineHistoryChangedByID = $engineHistoryChangedByID;
        $this->engineHistoryDateChanged = $engineHistoryDateChanged;
    }
    
}
