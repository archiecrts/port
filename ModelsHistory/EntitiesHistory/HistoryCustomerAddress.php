<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryCustomerAddress
 *
 * @author Archie
 */
class HistoryCustomerAddress {
    public $addressHistoryID;
    public $addressID;
    public $addressOfficeName;
    public $address;
    public $addressCity;
    public $addressState;
    public $addressCountry;
    public $addressPostcode;
    public $addressVerified;
    public $customerID;
    public $addressArchived;
    public $addressDefaultAddress;
    public $fullAddress;
    public $addressHistoryChangedByID;
    public $addressHistoryDateChanged;
    
    
    public function __construct($addressHistoryID, $addressID, $addressOfficeName, 
            $address, $addressCity, $addressState, $addressCountry,
            $addressPostcode, $addressVerified, $customerID, $addressArchived, 
            $addressDefaultAddress, $fullAddress, $addressHistoryChangedByID, $addressHistoryDateChanged) {
        $this->addressHistoryID = $addressHistoryID;
        $this->addressID = $addressID;
        $this->addressOfficeName = $addressOfficeName;
        $this->address = $address;
        $this->addressCity = $addressCity;
        $this->addressState = $addressState;
        $this->addressCountry = $addressCountry;
        $this->addressPostcode = $addressPostcode;
        $this->addressVerified = $addressVerified;
        $this->customerID = $customerID;
        $this->addressArchived = $addressArchived; 
        $this->addressDefaultAddress = $addressDefaultAddress;
        $this->fullAddress = $fullAddress;
        $this->addressHistoryChangedByID = $addressHistoryChangedByID;
        $this->addressHistoryDateChanged = $addressHistoryDateChanged;
    }
        
}
