<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryDailyExchangeRate
 *
 * @author Archie
 */
class HistoryDailyExchangeRate {

    public $dailyHistoryID;
    public $dailyEXCID;
    public $dailyEXCCurrencyCode;
    public $dailyEXCCurrencyUnitPerPound;
    public $dailyEXCDateSet;
    public $dailyHistoryChangedByID;
    public $dailyHistoryDateChanged;

    public function __construct($dailyHistoryID, $dailyEXCID, $dailyEXCCurrencyCode, 
            $dailyEXCCurrencyUnitPerPound, $dailyEXCDateSet, 
            $dailyHistoryChangedByID, $dailyHistoryDateChanged) {

        $this->dailyHistoryID = $dailyHistoryID;
        $this->dailyEXCID = $dailyEXCID;
        $this->dailyEXCCurrencyCode = $dailyEXCCurrencyCode;
        $this->dailyEXCCurrencyUnitPerPound = $dailyEXCCurrencyUnitPerPound;
        $this->dailyEXCDateSet = $dailyEXCDateSet;
        $this->dailyHistoryChangedByID = $dailyHistoryChangedByID;
        $this->dailyHistoryDateChanged = $dailyHistoryDateChanged;
    }

}
