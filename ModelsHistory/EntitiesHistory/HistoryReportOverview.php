<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryReportOverview
 *
 * @author Archie
 */
class HistoryReportOverview {
    public $overviewHistoryID;
    public $reportID;
    public $reportDateCreated;
    public $reportUserID;
    public $reportEmailSent;
    public $reportDynamicFlag;
    public $reportActive;
    public $overviewHistoryChangedByID;
    public $overviewHistoryDateChanged;
    
    public function __construct($overviewHistoryID, $reportID, $reportDateCreated, $reportUserID,
            $reportEmailSent, $reportDynamicFlag, $reportActive, $overviewHistoryChangedByID, $overviewHistoryDateChanged) {
        $this->overviewHistoryID = $overviewHistoryID;
        $this->reportID = $reportID;
        $this->reportDateCreated = $reportDateCreated;
        $this->reportUserID = $reportUserID;
        $this->reportEmailSent = $reportEmailSent;
        $this->reportDynamicFlag = $reportDynamicFlag;
        $this->reportActive = $reportActive;
        $this->overviewHistoryChangedByID = $overviewHistoryChangedByID;
        $this->overviewHistoryDateChanged = $overviewHistoryDateChanged;
    }
}
