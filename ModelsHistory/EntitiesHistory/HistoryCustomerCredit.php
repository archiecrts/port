<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryCustomerCredit
 *
 * @author Archie
 */
class HistoryCustomerCredit {
    public $creditHistoryId;
    public $creditID;
    public $creditAccountStop;
    public $customerID;
    public $creditPaymentTerms;
    public $creditHistoryChangedByID;
    public $creditHistoryDateChanged;
    
    public function __construct($creditHistoryId, $creditID, $creditAccountStop, 
            $customerID, $creditPaymentTerms, $creditHistoryChangedByID, $creditHistoryDateChanged) {
        $this->creditHistoryId = $creditHistoryId;
        $this->creditID = $creditID;
        $this->creditAccountStop = $creditAccountStop;
        $this->customerID = $customerID;
        $this->creditPaymentTerms = $creditPaymentTerms;
        $this->creditHistoryChangedByID = $creditHistoryChangedByID;
        $this->creditHistoryDateChanged = $creditHistoryDateChanged;
    }
}
