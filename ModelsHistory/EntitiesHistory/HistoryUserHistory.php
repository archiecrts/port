<?php

/*
 * Property of S-T Diesel Marine Group.
 */

/**
 * Description of User
 * The User entity contains all the requirements for a user to log into the system.
 *
 * @author Archie
 */
class HistoryUserHistory {
    public $id;
    public $userID;
    public $login;
    public $password;
    public $name;
    public $level;
    public $department;
    public $email;
    public $initialLogin;
    public $retire;
    public $changedByUserID;
    public $dateOfChange;

    /**
     * Constructor for User Entity.
     * @param INT $id
     * @param INT $userID
     * @param STRING $login
     * @param STRING (MD5) $password
     * @param STRING $name
     * @param ENUM $level
     * @param STRING $department
     * @param STRING $email
     * @param BOOLEAN $initialLogin
     * @param BOOLEAN $retire
     * @param INT $changedByUserID
     * @param DATETIME $dateOfChange
     */
    public function __construct($id, $userID, $login, $password, $name, $level, $department, $email, $initialLogin,
            $retire, $changedByUserID, $dateOfChange) {
        $this->id = $id;
        $this->userID = $userID;
        $this->login = $login;
        $this->password = $password;
        $this->name = $name;
        $this->level = $level;
        $this->department = $department;
        $this->email = $email;
        $this->initialLogin = $initialLogin;
        $this->retire = $retire;
        $this->changedByUserID = $changedByUserID;
        $this->dateOfChange = $dateOfChange;
    }
}
