<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryHMRCExchangeRate
 *
 * @author Archie
 */
class HistoryHMRCExchangeRate {
    public $hmrcHistoryID;
    public $hmrcID;
    public $hmrcCountry;
    public $hmrcCurrency;
    public $hmrcCurrencyCode;
    public $hmrcCurrencyUnitsPerPound;
    public $hmrcStartDate;
    public $hmrcEndDate;
    public $hmrcHistoryChangedByID;
    public $hmrcHistoryDateChanged;
    
    public function __construct($hmrcHistoryID, $hmrcID, $hmrcCountry, $hmrcCurrency, $hmrcCurrencyCode, 
            $hmrcCurrencyUnitsPerPound, $hmrcStartDate, $hmrcEndDate, 
            $hmrcHistoryChangedByID, $hmrcHistoryDateChanged) {
        $this->hmrcHistoryID = $hmrcHistoryID;
        $this->hmrcID = $hmrcID;
        $this->hmrcCountry = $hmrcCountry;
        $this->hmrcCurrency = $hmrcCurrency;
        $this->hmrcCurrencyCode = $hmrcCurrencyCode;
        $this->hmrcCurrencyUnitsPerPound = $hmrcCurrencyUnitsPerPound;
        $this->hmrcStartDate = $hmrcStartDate;
        $this->hmrcEndDate = $hmrcEndDate;
        $this->hmrcHistoryChangedByID = $hmrcHistoryChangedByID;
        $this->hmrcHistoryDateChanged = $hmrcHistoryDateChanged;
    }
}
