<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryCustomerHasInstallations
 *
 * @author Archie
 */
class HistoryCustomerHasInstallations {
    public $customerHasInstallationsHistoryID;
    public $customerID;
    public $installationID;
    public $customerHasInstallationsHistoryChangedByID;
    public $customerHasInstallationsChangedDate;
    
    public function __construct($customerHasInstallationsHistoryID, $customerID, $installationID, 
            $customerHasInstallationsHistoryChangedByID, $customerHasInstallationsChangedDate) {
        $this->customerHasInstallationsHistoryID = $customerHasInstallationsHistoryID;
        $this->customerID = $customerID;
        $this->installationID = $installationID;
        $this->customerHasInstallationsHistoryChangedByID = $customerHasInstallationsHistoryChangedByID;
        $this->customerHasInstallationsChangedDate = $customerHasInstallationsChangedDate;
    }
}
