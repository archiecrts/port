<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
//include_once ("Models/DB.php");
//include_once("Models/Database.php");
include_once("Models/DatabaseHistory.php");
include_once ("ModelsHistory/EntitiesHistory/HistorySupplierApprovals.php");
include_once 'ModelsHistory/SQLConstructors/HistorySupplierApprovalsSQLConstructor.php';
/**
 * Description of SupplierApprovalsHistoryModel
 *
 * @author Archie
 */
class SupplierApprovalsHistoryModel {
    
    
    /**
     * setSupplierApprovalsHistory
     * @param type $supplierApprovalsHistory
     * @return type
     */
    public function setSupplierApprovalsHistory($supplierApprovalsHistory) 
    {
        $dbHistory = new DatabaseHistory();
        $result = "INSERT INTO $dbHistory->databaseHistory.supplier_approvals_history_tbl (sat_id, customer_tbl_cust_id, "
                . "sat_approved_as_supplier, sat_approved_as_trader, sat_approved_as_agent, "
                . "supplier_approval_status_lookup_tbl_saslt_id, sat_date_approval_status_confirmed, "
                . "sat_status_review_date, sat_reason_for_status_change, "
                . "supplier_approvals_history_changed_by_id, supplier_approvals_history_changed_date) "
                . "VALUES ("
                . "'".$supplierApprovalsHistory->supplierApprovalsID."', "
                . "'".$supplierApprovalsHistory->customerTblCustID."', "
                . "'".$supplierApprovalsHistory->satApprovedAsSupplier."', "
                . "'".$supplierApprovalsHistory->satApprovedAsTrader."', "
                . "'".$supplierApprovalsHistory->satApprovedAsAgent."', "
                . "'".$supplierApprovalsHistory->supplierApprovalStatusLookupID."', "
                . "'".$supplierApprovalsHistory->satDateApprovalStatusConfirmed."', "
                . "'".$supplierApprovalsHistory->satStatusReviewDate."', "
                . "'".$supplierApprovalsHistory->satReasonForStatusChange."', "
                . "'".$supplierApprovalsHistory->supplierApprovalsHistoryChangedByID."',"
                . "'".$supplierApprovalsHistory->supplierApprovalsHistoryChangedDate."')";
        
        if (!mysqli_query(DatabaseHistory::$connection, $result))
        {
            debugWriter("debug.txt", "setSupplierApprovalsHistory FAILED $result ".mysqli_error(DatabaseHistory::$connection));
            return null;
        } else {
            return mysqli_insert_id(DatabaseHistory::$connection);
        }
    }
    
}
