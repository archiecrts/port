<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
//include_once ("Models/DB.php");
include_once("Models/DatabaseHistory.php");
include_once ("ModelsHistory/EntitiesHistory/HistoryReportParameter.php");
include_once 'ModelsHistory/SQLConstructors/HistoryReportParameterSQLConstructor.php';
/**
 * Description of ReportParametersHistoryModel
 *
 * @author Archie
 */
class ReportParametersHistoryModel {
    
    /**
     * setNewReportParameter
     * @param type $reportParameter
     * @return type
     */
    public function setNewReportParameter($reportParameter) 
    {
        $dbHistory = new DatabaseHistory();
        $result = "INSERT INTO $dbHistory->databaseHistory.report_parameters_history_tbl ("
                . "parameters_history_id, par_id, rot_id, "
                . "par_parameter, par_value, "
                . "parameters_history_changed_by_id, parameters_history_date_changed) "
                . "VALUES ("
                . "'".$reportParameter->parameterHistoryID."', "
                . "'".$reportParameter->parameterID."', "
                . "'".$reportParameter->reportID."', "
                . "'".$reportParameter->parameter."', "
                . "'".$reportParameter->parameterValue."', "
                . "'".$reportParameter->parameterChangedByUserID."', "
                . "'".$reportParameter->parameterDateChanged."')";
        
        if (!mysqli_query(DatabaseHistory::$connection, $result))
        {
            debugWriter("debug.txt", "setNewReportParameter FAILED $result ".mysqli_error(DatabaseHistory::$connection));
            return null;
        } else {
            return mysqli_insert_id(DatabaseHistory::$connection);
        }
    }
    

    /**
     * getReportParameterHistoryByID
     * @param INT $reportID
     * @return type
     */
    public function getReportParameterHistoryByID($reportID) {
        $dbHistory = new DatabaseHistory();
        $historyReportParameterSQLConstructor = new HistoryReportParameterSQLConstructor();
        $result = mysqli_query(DatabaseHistory::$connection, "SELECT * FROM $dbHistory->databaseHistory.report_parameters_history_tbl "
                . "WHERE rot_id = '".$reportID."' ORDER BY parameters_history_date_changed DESC");
        
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['parameters_history_id']] = $historyReportParameterSQLConstructor->createHistoryReportParameter($row);
        }
        
        if (!isset($array)) {
            return null;
        } else {
            return $array;
        }
    }
}
