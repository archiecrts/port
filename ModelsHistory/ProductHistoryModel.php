<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
//include_once ("Models/DB.php");
include_once("Models/DatabaseHistory.php");
include_once ("ModelsHistory/EntitiesHistory/HistoryProduct.php");
include_once 'ModelsHistory/SQLConstructors/HistoryProductSQLConstructor.php';
/**
 * Description of ProductHistoryModel
 *
 * @author Archie
 */
class ProductHistoryModel {
    
    /**
     * setProductHistory
     * @param type $productHistory
     * @return type
     */
    public function setProductHistory($productHistory) 
    {
        $dbHistory = new DatabaseHistory();
        $result = "INSERT INTO $dbHistory->databaseHistory.product_history_tbl (prod_id, prod_serial_number, "
                . "prod_description, prod_drawing_number, prod_comments, prod_enquiry_only_flag, "
                . "customer_tbl_cust_id, installations_tbl_inst_id, version_lookup_tbl_vlt_id, prod_unit_name, "
                . "prod_source_alias_id, product_history_changed_by_id, product_history_changed_date) "
                . "VALUES ("
                . "'".$productHistory->productID."', "
                . "'".addslashes($productHistory->productSerialNumber)."', "
                . "'".addslashes($productHistory->productDescription)."', "
                . "'".addslashes($productHistory->productDrawingNumber)."', "
                . "'".addslashes($productHistory->productComments)."', "
                . "'".$productHistory->productEnquiryOnlyFlag."', "
                . "'".$productHistory->customerTblCustID."', "
                . "'".$productHistory->installationsTblInstID."', "
                . "'".$productHistory->versionLookupTblVltID."', "
                . "'".addslashes($productHistory->productUnitName)."', "
                . "'".$productHistory->sourceAliasID."', "
                . "'".$productHistory->productHistoryChangedByID."', "
                . "'".$productHistory->productHistoryChangedDate."')";
        
        if (!mysqli_query(DatabaseHistory::$connection, $result))
        {
            debugWriter("debug.txt", "setProductHistory FAILED $result ".mysqli_error(DatabaseHistory::$connection));
            return null;
        } else {
            return mysqli_insert_id(DatabaseHistory::$connection);
        }
    }
    
    /**
     * getProductHistory
     * @param type $productID
     * @return boolean
     */
    public function getProductHistory($productID) {
        $dbHistory = new DatabaseHistory();
        $historyProductSQLConstructor = new HistoryProductSQLConstructor();
        $result = mysqli_query(DatabaseHistory::$connection, "SELECT * FROM $dbHistory->databaseHistory.product_history_tbl WHERE prod_id = '$productID' ORDER BY product_history_id DESC");
        
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['product_history_id']] = $historyProductSQLConstructor->createHistoryProduct($row);
        }
        
        
        if (isset($array)) {
            return $array;
        } else {
            return false;
        }
    }
}
