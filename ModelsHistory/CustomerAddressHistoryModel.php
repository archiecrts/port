<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
//include_once ("Models/DB.php");
//include_once("Models/Database.php");
include_once("Models/DatabaseHistory.php");
include_once ("ModelsHistory/EntitiesHistory/HistoryCustomerAddress.php");
include_once 'ModelsHistory/SQLConstructors/HistoryCustomerAddressSQLConstructor.php';
/**
 * Description of CustomerAddressHistoryModel
 *
 * @author Archie
 */
class CustomerAddressHistoryModel {
    
    /**
     * setCustomerAddressHistory
     * @param Entity $customerAddressHistory
     * @return type
     */
    public function setCustomerAddressHistory($customerAddressHistory) 
    {
        $dbHistory = new DatabaseHistory();
        $result = "INSERT INTO $dbHistory->databaseHistory.customer_address_history_tbl (cadt_id, cadt_office_name, cadt_address, "
                . "cadt_city, cadt_state, cadt_country, cadt_postcode, cadt_address_verified, "
                . "customer_id, cadt_archived, cadt_default_address, cadt_full_address, "
                . "address_history_changed_by_id, address_history_date_changed) "
                . "VALUES ("
                . "'".$customerAddressHistory->addressID."', "
                . "'".addslashes($customerAddressHistory->addressOfficeName)."', "
                . "'".addslashes($customerAddressHistory->address)."', "
                . "'".addslashes($customerAddressHistory->addressCity)."', "
                . "'".addslashes($customerAddressHistory->addressState)."', "
                . "'".addslashes($customerAddressHistory->addressCountry)."', "
                . "'".addslashes($customerAddressHistory->addressPostcode)."', "
                . "'".$customerAddressHistory->addressVerified."', "
                . "'".$customerAddressHistory->customerID."',"
                . "'".$customerAddressHistory->addressArchived."', "
                . "'".$customerAddressHistory->addressDefaultAddress."', "
                . "'".addslashes($customerAddressHistory->fullAddress)."', "
                . "'".$customerAddressHistory->addressHistoryChangedByID."', "
                . "'".$customerAddressHistory->addressHistoryDateChanged."')";
        
        if (!mysqli_query(DatabaseHistory::$connection, $result))
        {
            debugWriter("debug.txt", "setCustomerAddressHistory FAILED $result ".mysqli_error(DatabaseHistory::$connection));
            return null;
        } else {
            return mysqli_insert_id(DatabaseHistory::$connection);
        }
    }
    
}
