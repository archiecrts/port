<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
//include_once ("Models/DB.php");
include_once("Models/DatabaseHistory.php");
include_once ("ModelsHistory/EntitiesHistory/HistoryNoticeBoard.php");
include_once 'ModelsHistory/SQLConstructors/HistoryNoticeBoardSQLConstructor.php';
/**
 * Description of NoticeBoardHistoryModel
 *
 * @author Archie
 */
class NoticeBoardHistoryModel {
    
    /**
     * setNewNoticeBoard
     * @param Entity $noticeBoard
     * @return INT | null
     */
    public function setNewNoticeBoard($noticeBoard) 
    {
        $dbHistory = new DatabaseHistory();
        $result = "INSERT INTO $dbHistory->databaseHistory.notice_board_edit_history_tbl (nbt_id, nbt_title, nbt_date, "
                . "nbt_content, nbt_archive, nbt_authorised, nbt_user_id, nbt_category_id, nbt_document_id, "
                . "notice_history_changed_by_id, notice_history_date_changed) "
                . "VALUES ("
                . "'".$noticeBoard->noticeBoardID."', "
                . "'".$noticeBoard->noticeBoardTitle."', "
                . "'".$noticeBoard->noticeBoardDate."', "
                . "'".$noticeBoard->noticeBoardContent."', "
                . "'".$noticeBoard->noticeBoardArchive."', "
                . "'".$noticeBoard->noticeBoardAuthorised."', "
                . "'".$noticeBoard->noticeBoardUserID."', "
                . "'".$noticeBoard->noticeBoardCategoryID."', "
                . "'".$noticeBoard->noticeBoardDocumentID."',"
                . "'".$noticeBoard->noticeHistoryChangedByID."', "
                . "'".$noticeBoard->noticeHistoryDateChanged."')";
        
        if (!mysqli_query(DatabaseHistory::$connection, $result))
        {
            debugWriter("debug.txt", "setNewNoticeBoard FAILED $result ".mysqli_error(DatabaseHistory::$connection));
            return null;
        } else {
            return mysqli_insert_id(DatabaseHistory::$connection);
        }
    }
}
