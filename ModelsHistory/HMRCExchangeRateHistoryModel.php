<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
//include_once ("Models/DB.php");
include_once("Models/DatabaseHistory.php");
include_once ("ModelsHistory/EntitiesHistory/HistoryHMRCExchangeRate.php");
include_once 'ModelsHistory/SQLConstructors/HistoryHMRCExchangeRateSQLConstructor.php';
/**
 * Description of HMRCExchangeRateHistoryModel
 *
 * @author Archie
 */
class HMRCExchangeRateHistoryModel {
    
    /**
     * setHMRCExchangeRateHistory
     * @param type $hmrcExchangeRateHistory
     * @return type
     */
    public function setHMRCExchangeRateHistory($hmrcExchangeRateHistory) 
    {
        $dbHistory = new DatabaseHistory();
        $result = "INSERT INTO $dbHistory->databaseHistory.hmrc_monthly_exchange_rate_history_tbl ("
                . "hmrc_id, hmrc_country, hmrc_currency, hmrc_currency_code, hmrc_currency_units_per_pound, "
                . "hmrc_start_date, hmrc_end_date, hmrc_history_changed_by_id, "
                . "hmrc_history_date_changed) "
                . "VALUES ("
                . "'".$hmrcExchangeRateHistory->hmrcID."', "
                . "'".addslashes($hmrcExchangeRateHistory->hmrcCountry)."', "
                . "'".addslashes($hmrcExchangeRateHistory->hmrcCurrency)."', "
                . "'".addslashes($hmrcExchangeRateHistory->hmrcCurrencyCode)."', "
                . "'".addslashes($hmrcExchangeRateHistory->hmrcCurrencyUnitsPerPound)."', "
                . "'".$hmrcExchangeRateHistory->hmrcStartDate."', "
                . "'".$hmrcExchangeRateHistory->hmrcEndDate."', "
                . "'".$hmrcExchangeRateHistory->hmrcHistoryChangedByID."', "
                . "'".$hmrcExchangeRateHistory->hmrcHistoryDateChanged."')";
        
        if (!mysqli_query(DatabaseHistory::$connection, $result)) {
            debugWriter("debug.txt", "setHMRCExchangeRateHistory FAILED $result ".mysqli_error(DatabaseHistory::$connection));
            return null;
        } else {
            return mysqli_insert_id(DatabaseHistory::$connection);
        }
    }
}
