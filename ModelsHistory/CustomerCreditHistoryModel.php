<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
//include_once ("Models/DB.php");
include_once("Models/DatabaseHistory.php");
include_once ("ModelsHistory/EntitiesHistory/HistoryCustomerCredit.php");
include_once 'ModelsHistory/SQLConstructors/HistoryCustomerCreditSQLConstructor.php';
/**
 * Description of CustomerCreditHistoryModel
 *
 * @author Archie
 */
class CustomerCreditHistoryModel {
    
    /**
     * setCustomerCreditHistory
     * @param Entity $customerCreditHistory
     * @return type
     */
    public function setCustomerCreditHistory($customerCreditHistory) 
    {
        $dbHistory = new DatabaseHistory();
        $result = "INSERT INTO $dbHistory->databaseHistory.customer_credit_history_tbl (cct_id, cct_credit_limit, "
                . "cct_account_stop, customer_id, cct_payment_terms, "
                . "credit_history_changed_by_id, credit_history_date_changed) "
                . "VALUES ("
                . "'".$customerCreditHistory->creditID."', "
                . "'".$customerCreditHistory->creditLimit."', "
                . "'".$customerCreditHistory->creditAccountStop."', "
                . "'".$customerCreditHistory->customerID."', "
                . "'".$customerCreditHistory->creditPaymentTerms."', "
                . "'".$customerCreditHistory->creditHistoryChangedByID."', "
                . "'".$customerCreditHistory->creditHistoryDateChanged."')";
        
        if (!mysqli_query(DatabaseHistory::$connection, $result))
        {
            debugWriter("debug.txt", "setCustomerCreditHistory FAILED $result ".mysqli_error(DatabaseHistory::$connection));
            return null;
        } else {
            return mysqli_insert_id(DatabaseHistory::$connection);
        }
    }
}
