<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
//include_once ("Models/DB.php");
include_once("Models/DatabaseHistory.php");
include_once ("ModelsHistory/EntitiesHistory/HistoryInstallationEngine.php");
include_once 'ModelsHistory/SQLConstructors/HistoryInstallationEngineSQLConstructor.php';
/**
 * Description of InstallationEngineHistoryModel
 *
 * @author Archie
 */
class InstallationEngineHistoryModel {
    
    /**
     * setInstallationEngineHistory
     * @param Entity $installationEngineHistory
     * @return type
     */
    public function setInstallationEngineHistory($installationEngineHistory) 
    {
        $dbHistory = new DatabaseHistory();
        $result = "INSERT INTO $dbHistory->databaseHistory.installation_engine_history_tbl (iet_id,  "
                . "iet_model_description,  "
                . "iet_main_aux,  iet_engine_verified, "
                . " iet_output, iet_output_unit_of_measure, "
                . "iet_fuel_type, iet_unit_name, "
                . " iet_bore_size, iet_stroke, iet_release_date, "
                . "iet_enquiry_only, load_priority_lookup_tbl_load_id, product_tbl_prod_id, "
                . "iet_position_if_main, iet_engine_builder, "
                . "installation_engine_history_changed_by_id, "
                . "installation_engine_history_date_changed) "
                . "VALUES ("
                . "'".$installationEngineHistory->engineID."', "
                . "'".$installationEngineHistory->engineModelDescription."', "
                . "'".$installationEngineHistory->engineMainAux."', "
                . "'".$installationEngineHistory->engineVerified."', "
                . "'".$installationEngineHistory->engineOutput."', "
                . "'".$installationEngineHistory->engineOutputUnitOfMeasure."', "
                . "'".$installationEngineHistory->engineFuelType."', "
                . "'".$installationEngineHistory->unitName."', "
                . "'".$installationEngineHistory->engineBoreSize."', "
                . "'".$installationEngineHistory->engineStroke."', "
                . "'".$installationEngineHistory->engineReleaseDate."', "
                . "'".$installationEngineHistory->engineEnquiryOnly."', "
                . "'".$installationEngineHistory->loadPriorityID."', "
                . "'".$installationEngineHistory->productID."', "
                . "'".$installationEngineHistory->positionIfMain."', "
                . "'".$installationEngineHistory->engineBuilder."', "
                . "'".$installationEngineHistory->engineHistoryChangedByID."', "
                . "'".$installationEngineHistory->engineHistoryDateChanged."')";
        
        if (!mysqli_query(DatabaseHistory::$connection, $result))
        {
            debugWriter("debug.txt", "setInstallationEngineHistory FAILED $result ".mysqli_error(DatabaseHistory::$connection));
            return null;
        } else {
            return mysqli_insert_id(DatabaseHistory::$connection);
        }
    }
}
