<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
//include_once ("Models/DB.php");
include_once("Models/DatabaseHistory.php");
include_once ("ModelsHistory/EntitiesHistory/HistoryCustomerStatus.php");
include_once 'ModelsHistory/SQLConstructors/HistoryCustomerStatusSQLConstructor.php';
/**
 * Description of CustomerStatusHistoryModel
 *
 * @author Archie
 */
class CustomerStatusHistoryModel {
    
    /**
     * setCustomerStatusHistory
     * @param History Customer Status Object $customerStatusHistory
     * @return INT | NULL
     */
    public function setCustomerStatusHistory($customerStatusHistory) 
    {
        $dbHistory = new DatabaseHistory();
        $result = "INSERT INTO $dbHistory->databaseHistory.customer_status_history_tbl "
                . "(cst_id, customer_tbl_cust_id, customer_status_lookup_tbl_cslt_id, "
                . "customer_status_history_changed_by_id, customer_status_history_date_changed) "
                . "VALUES ("
                . "'".$customerStatusHistory->customerStatusID."', "
                . "'".$customerStatusHistory->customerID."', "
                . "'".$customerStatusHistory->statusLookupID."', "
                . "'".$customerStatusHistory->customerStatusHistoryChangedByID."', "
                . "'".$customerStatusHistory->customerStatusHistoryDateChanged."')";
        
        if (!mysqli_query(DatabaseHistory::$connection, $result))
        {
            debugWriter("debug.txt", "setCustomerStatusHistory FAILED $result ".mysqli_error(DatabaseHistory::$connection));
            return null;
        } else {
            return mysqli_insert_id(DatabaseHistory::$connection);
        }
    }
    
}
