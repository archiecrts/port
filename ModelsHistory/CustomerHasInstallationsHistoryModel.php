<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
//include_once ("Models/DB.php");
include_once("Models/DatabaseHistory.php");
include_once ("ModelsHistory/EntitiesHistory/HistoryCustomerHasInstallations.php");
include_once 'ModelsHistory/SQLConstructors/HistoryCustomerHasInstallationsSQLConstructor.php';
/**
 * Description of CustomerHasInstallationsHistoryModel
 *
 * @author Archie
 */
class CustomerHasInstallationsHistoryModel {
    
    /**
     * setCustomerHasInstallationsHistory
     * @param type $customerHistory
     * @return type
     */
    public function setCustomerHasInstallationsHistory($customerHistory) 
    {
        $dbHistory = new DatabaseHistory();
        $result = "INSERT INTO $dbHistory->databaseHistory.customer_has_installations_history_tbl ("
                . "customer_tbl_cust_id, "
                . "installations_tbl_inst_id, "
                . "customer_has_installations_history_changed_by_id, "
                . "customer_has_installations_history_changed_date) "
                . "VALUES ("
                . "'".$customerHistory->customerID."', "
                . "'".$customerHistory->installationID."', "
                . "'".$customerHistory->customerHasInstallationsHistoryChangedByID."', "
                . "'".$customerHistory->customerHasInstallationsChangedDate."')";
        
        if (!mysqli_query(DatabaseHistory::$connection, $result))
        {
            debugWriter("debug.txt", "setCustomerHasInstallationsHistory FAILED $result ".mysqli_error(DatabaseHistory::$connection));
            return null;
        } else {
            return mysqli_insert_id(DatabaseHistory::$connection);
        }
    }
    
    /**
     * getHistoryForInstallation
     * @param type $installationID
     * @return type
     */
    public function getHistoryForInstallation($installationID) {
        $dbHistory = new DatabaseHistory();
        $customerHasInstallationsHistorySQLContstructor = new HistoryCustomerHasInstallationsSQLConstructor();
        
        $result = mysqli_query(DatabaseHistory::$connection, "SELECT * FROM ". $dbHistory->databaseHistory.".customer_has_installations_history_tbl "
                . "WHERE installations_tbl_inst_id = '$installationID'");
        
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['customer_has_installations_history_id']] = $customerHasInstallationsHistorySQLContstructor->createHistoryCustomerHasInstallations($row);
        }
        debugWriter("debug.txt", "SELECT * FROM port_history_db.customer_has_installations_history_tbl "
                . "WHERE installations_tbl_inst_id = '$installationID'");
        if (isset($array) && (sizeof($array)) > 0) {
            return $array;
        } else {
            return null;
        }
    }
}
