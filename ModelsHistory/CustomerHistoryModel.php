<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
//include_once ("Models/DB.php");
include_once("Models/DatabaseHistory.php");
include_once ("ModelsHistory/EntitiesHistory/HistoryCustomer.php");
include_once 'ModelsHistory/SQLConstructors/HistoryCustomerSQLConstructor.php';
/**
 * Description of CustomerHistoryModel
 *
 * @author Archie
 */
class CustomerHistoryModel {
    
    /**
     * setCustomerHistory
     * @param Entity $customerHistory
     * @return type
     */
    public function setCustomerHistory($customerHistory) 
    {
        $dbHistory = new DatabaseHistory();
        $result = "INSERT INTO $dbHistory->databaseHistory.customer_history_tbl (cust_id, cust_name, cust_telephone, "
                . "cust_fax, cust_website,  cust_account_manager, cust_tbh_id, "
                . "cust_tbh_account_code, cust_ld_id, cust_ld_account_code, cust_header_notes, "
                . "cust_seaweb_code, cust_parent_company_seaweb_code, "
                . "cust_parent_company_nationality_of_control, cust_company_status, "
                . "cust_company_full_name, cust_navision_id, "
                . "customer_history_changed_by_id, customer_history_date_changed) "
                . "VALUES ("
                . "'".$customerHistory->customerID."', "
                . "'".addslashes($customerHistory->customerName)."', "
                . "'".addslashes($customerHistory->customerTelephone)."', "
                . "'".addslashes($customerHistory->customerFax)."', "
                . "'".addslashes($customerHistory->customerWebsite)."', "
                . "'".$customerHistory->customerAccountManager."', "
                . "'".$customerHistory->customerTBHID."', "
                . "'".$customerHistory->customerTBHAccountCode."',"
                . "'".$customerHistory->customerLDID."', "
                . "'".$customerHistory->customerLDAccountCode."', "
                . "'".addslashes($customerHistory->customerHeaderNotes)."', "
                . "'".addslashes($customerHistory->seawebCode)."', "
                . "'".$customerHistory->parentSeawebCode."', "
                . "'".addslashes($customerHistory->parentNationalityOfControl)."', "
                . "'".addslashes($customerHistory->companyStatus)."', "
                . "'".addslashes($customerHistory->companyFullName)."', "
                . "'".$customerHistory->navisionID."', "
                . "'".$customerHistory->customerHistoryChangedByID."', "
                . "'".$customerHistory->customerHistoryDateChanged."')";
        
        if (!mysqli_query(DatabaseHistory::$connection, $result))
        {
            debugWriter("debug.txt", "setCustomerHistory FAILED $result ".mysqli_error(DatabaseHistory::$connection));
            return null;
        } else {
            return mysqli_insert_id(DatabaseHistory::$connection);
        }
    }
    
}
