<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
//include_once ("Models/DB.php");
include_once("Models/DatabaseHistory.php");
include_once ("ModelsHistory/EntitiesHistory/HistoryDailyExchangeRate.php");
include_once 'ModelsHistory/SQLConstructors/HistoryDailyExchangeRateSQLConstructor.php';
/**
 * Description of DailyExchangeRateHistoryModel
 *
 * @author Archie
 */
class DailyExchangeRateHistoryModel {
    
    /**
     * setDailyExchangeRateHistory
     * @param type $dailyExchangeRateHistory
     * @return type
     */
    public function setDailyExchangeRateHistory($dailyExchangeRateHistory) 
    {
        $dbHistory = new DatabaseHistory();
        $result = "INSERT INTO $dbHistory->databaseHistory.daily_exchange_rate_history_tbl ("
                . "daily_id, daily_currency_code, daily_currency_unit_per_pound, "
                . "daily_last_changed, daily_history_changed_by_id, "
                . "daily_history_date_changed) "
                . "VALUES ("
                . "'".$dailyExchangeRateHistory->dailyEXCID."', "
                . "'".$dailyExchangeRateHistory->dailyEXCCurrencyCode."', "
                . "'".$dailyExchangeRateHistory->dailyEXCCurrencyUnitPerPound."', "
                . "'".$dailyExchangeRateHistory->dailyEXCDateSet."', "
                . "'".$dailyExchangeRateHistory->dailyHistoryChangedByID."', "
                . "'".$dailyExchangeRateHistory->dailyHistoryDateChanged."')";
        
        if (!mysqli_query(DatabaseHistory::$connection, $result))
        {
            debugWriter("debug.txt", "setDailyExchangeRateHistory FAILED $result ".mysqli_error(DatabaseHistory::$connection));
            return null;
        } else {
            return mysqli_insert_id(DatabaseHistory::$connection);
        }
    }
}
