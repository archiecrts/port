<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
//include_once ("Models/DB.php");
include_once("Models/DatabaseHistory.php");
include_once ("ModelsHistory/EntitiesHistory/HistoryInstallation.php");
include_once 'ModelsHistory/SQLConstructors/HistoryInstallationSQLConstructor.php';
/**
 * Description of InstallationHistoryModel
 *
 * @author Archie
 */
class InstallationHistoryModel {
    
    /**
     * setInstallationHistory
     * @param Entity $installationHistory
     * @return type
     */
    public function setInstallationHistory($installationHistory) 
    {
        $dbHistory = new DatabaseHistory();
        $result = "INSERT INTO $dbHistory->databaseHistory.installations_history_tbl (inst_id, inst_type, "
                . "inst_installation, inst_installation_name, inst_imo, "
                . "installation_status_id, inst_owner_parent_company, inst_technical_manager_company, "
                . "inst_original_source, inst_original_source_date, inst_photo_link, "
                . "inst_hull_number, inst_hull_type, inst_yard_built, inst_deadweight, "
                . "inst_length_overall, inst_beam_extreme, inst_beam_moulded, inst_built_date, "
                . "inst_draught, inst_gross_tonnage, inst_length, inst_propeller_type, "
                . "inst_propulsion_unit_count, inst_ship_builder, inst_teu, inst_build_year, "
                . "inst_archive, inst_archive_reason, customer_address_tbl_cadt_id, "
                . "inst_flag_name, inst_lead_ship_by_imo, inst_ship_type_level_4, inst_classification_society, "
                . "installations_history_changed_by_id, "
                . "installations_history_date_changed) "
                . "VALUES ("
                . "'".$installationHistory->installationID."', "
                . "'".addslashes($installationHistory->installationType)."', "
                . "'".addslashes($installationHistory->installation)."', "
                . "'".addslashes($installationHistory->installationName)."', "
                . "'".addslashes($installationHistory->installationIMO)."', "
                . "'".addslashes($installationHistory->installationStatusID)."', "
                . "'".addslashes($installationHistory->installationOwnerParentCompany)."', "
                . "'".addslashes($installationHistory->installationTechnicalManager)."', "
                . "'".addslashes($installationHistory->installationOriginalSource)."', "
                . "'".addslashes($installationHistory->installationOriginalSourceDate)."', "
                . "'".addslashes($installationHistory->installationPhotoLink)."', "
                . "'".addslashes($installationHistory->installationHullNumber)."', "
                . "'".addslashes($installationHistory->installationHullType)."', "
                . "'".addslashes($installationHistory->installationYardBuilt)."', "
                . "'".addslashes($installationHistory->installationDeadweight)."', "
                . "'".addslashes($installationHistory->installationLengthOverall)."', "
                . "'".addslashes($installationHistory->installationBeamExtreme)."', "
                . "'".addslashes($installationHistory->installationBeamMoulded)."', "
                . "'".addslashes($installationHistory->installationBuiltDate)."', "
                . "'".addslashes($installationHistory->installationDraught)."', "
                . "'".addslashes($installationHistory->installationGrossTonnage)."', "
                . "'".addslashes($installationHistory->installationLength)."', "
                . "'".addslashes($installationHistory->installationPropellerType)."', "
                . "'".addslashes($installationHistory->installationPropulsionUnitCount)."', "
                . "'".addslashes($installationHistory->installationShipBuilder)."', "
                . "'".addslashes($installationHistory->installationTEU)."', "
                . "'".addslashes($installationHistory->installationBuildYear)."', "
                . "'".addslashes($installationHistory->installationArchiveFlag)."', "
                . "'".addslashes($installationHistory->installationArchiveReason)."', "
                . "'".addslashes($installationHistory->customerAddressID)."', "
                . "'".addslashes($installationHistory->flagName)."', "
                . "'".addslashes($installationHistory->leadShipByIMO)."', "
                . "'".addslashes($installationHistory->shipTypeLevel4)."', "
                . "'".addslashes($installationHistory->classificationSociety)."', "
                . "'".$installationHistory->installationHistoryChangedByID."', "
                . "'".$installationHistory->installationHistoryDateChanged."')";
        
        if (!mysqli_query(DatabaseHistory::$connection, $result))
        {
            debugWriter("debug.txt", "setInstallationHistory FAILED $result ".mysqli_error(DatabaseHistory::$connection));
            return null;
        } else {
            return mysqli_insert_id(DatabaseHistory::$connection);
        }
    }
    
    /**
     * getInstallationHistory
     * @param type $installationID
     * @return boolean
     */
    public function getInstallationHistory($installationID) {
        $dbHistory = new DatabaseHistory();
        $historyInstallationSQLConstructor = new HistoryInstallationSQLConstructor();
        $result = mysqli_query(DatabaseHistory::$connection, "SELECT * FROM $dbHistory->databaseHistory.installations_history_tbl "
                . "WHERE inst_id = '".$installationID."'  ORDER BY installations_history_id DESC");
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['installations_history_id']] = $historyInstallationSQLConstructor->createHistoryInstallation($row);
        }
        
        if (!isset($array)) {
            return false;
        } else {
            return $array;
        }
    }
}
