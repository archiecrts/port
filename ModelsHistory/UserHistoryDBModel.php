<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
//include_once ("Models/DB.php");
include_once("Models/DatabaseHistory.php");
include_once ("ModelsHistory/EntitiesHistory/HistoryUserHistory.php");
include_once 'ModelsHistory/SQLConstructors/HistoryUserHistorySQLConstructor.php';
/**
 * Description of UserHistoryModel
 *
 * @author Archie
 */
class UserHistoryDBModel {
    /**
     * setNewUser
     * @param Entity $user
     * @return type
     */
    public function setNewUser($user) 
    {
        $dbHistory = new DatabaseHistory();
        $result = "INSERT INTO $dbHistory->databaseHistory.user_history_tbl (usr_id, usr_login, usr_password, "
                . "usr_name, level_id, dept_id, usr_email, usr_initial_login, usr_retire_user_flag, "
                . "user_history_changed_by_id, user_history_date_changed) "
                . "VALUES ("
                . "'".$user->userID."', "
                . "'".$user->login."', "
                . "'".$user->password."', "
                . "'".$user->name."', "
                . "'".$user->level."', "
                . "'".$user->department."', "
                . "'".$user->email."', "
                . "'".$user->initialLogin."', "
                . "'".$user->retire."',"
                . "'".$user->changedByUserID."', "
                . "'".$user->dateOfChange."')";
        
        if (!mysqli_query(DatabaseHistory::$connection, $result))
        {
            debugWriter("debug.txt", "setNewUserHISTORY FAILED $result ".mysqli_error(DatabaseHistory::$connection));
            return null;
        } else {
            return mysqli_insert_id(DatabaseHistory::$connection);
        }
    }
    
    /**
     * getUserHistoryByID
     * @param INT $userID
     */
    public function getUserHistoryByID($userID) {
        $dbHistory = new DatabaseHistory();
        $historyUserHistorySQLConstructor = new HistoryUserHistorySQLConstructor();
        $result = mysqli_query(DatabaseHistory::$connection, "SELECT * FROM $dbHistory->databaseHistory.user_history_tbl "
                . "WHERE usr_id = '".$userID."' ORDER BY user_history_date_changed DESC");
        
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['user_history_id']] = $historyUserHistorySQLConstructor->createHistoryUserHistory($row);
        }
        
        if (!isset($array)) {
            return null;
        } else {
            return $array;
        }
    }
}
