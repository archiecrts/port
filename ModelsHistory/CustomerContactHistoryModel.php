<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
//include_once ("Models/DB.php");
//include_once("Models/Database.php");
include_once("Models/DatabaseHistory.php");
include_once ("ModelsHistory/EntitiesHistory/HistoryCustomerContact.php");
include_once 'ModelsHistory/SQLConstructors/HistoryCustomerContactSQLConstructor.php';
/**
 * Description of CustomerContactHistoryModel
 *
 * @author Archie
 */
class CustomerContactHistoryModel {
    
    /**
     * setCustomerContactHistory
     * @param Entity $customerContactHistory
     * @return type
     */
    public function setCustomerContactHistory($customerContactHistory) 
    {
        $dbHistory = new DatabaseHistory();
        $result = "INSERT INTO $dbHistory->databaseHistory.customer_contacts_history_tbl (ict_id, ict_salutation, "
                . "ict_first_name, ict_surname, ict_job_title, "
                . "ict_email, ict_number, ict_mobile_number, ict_info_source, ict_inactive, ict_contact_validated, "
                . "customer_id, address_id, "
                . "contact_history_changed_by_id, contact_history_date_changed) "
                . "VALUES ("
                . "'".$customerContactHistory->contactID."', "
                . "'".$customerContactHistory->salutation."', "
                . "'".$customerContactHistory->firstName."', "
                . "'".$customerContactHistory->surname."', "
                . "'".$customerContactHistory->contactJobTitle."', "
                . "'".$customerContactHistory->contactEmail."', "
                . "'".$customerContactHistory->contactNumber."', "
                . "'".$customerContactHistory->contactMobileNumber."', "
                . "'".$customerContactHistory->contactInfoSource."', "
                . "'".$customerContactHistory->contactInactive."', "
                . "'".$customerContactHistory->contactValidated."', "
                . "'".$customerContactHistory->customerID."',"
                . "'".$customerContactHistory->addressID."', "
                . "'".$customerContactHistory->contactHistoryChangedByID."', "
                . "'".$customerContactHistory->contactHistoryDateChanged."')";
        
        if (!mysqli_query(DatabaseHistory::$connection, $result))
        {
            debugWriter("debug.txt", "setCustomerContactHistory FAILED $result ".mysqli_error(DatabaseHistory::$connection));
            return null;
        } else {
            return mysqli_insert_id(DatabaseHistory::$connection);
        }
    }
}
