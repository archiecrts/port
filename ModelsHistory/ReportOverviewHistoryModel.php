<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
//include_once ("Models/DB.php");
include_once("Models/DatabaseHistory.php");
include_once ("ModelsHistory/EntitiesHistory/HistoryReportOverview.php");
include_once 'ModelsHistory/SQLConstructors/HistoryReportOverviewSQLConstructor.php';
/**
 * Description of ReportOverviewHistoryModel
 *
 * @author Archie
 */
class ReportOverviewHistoryModel {
    
    
    /**
     * setNewReportOverview
     * @param type $reportOverview
     * @return type
     */
    public function setNewReportOverview($reportOverview) 
    {
        $dbHistory = new DatabaseHistory();
        $result = "INSERT INTO $dbHistory->databaseHistory.report_overview_history_tbl (rot_id, rot_report_name, "
                . "rot_date_created, rot_user_id, rot_email_sent, rot_dynamic_flag, rot_report_active, "
                . "report_history_changed_by_id, report_history_date_changed) "
                . "VALUES ("
                . "'".$reportOverview->reportID."', "
                . "'".$reportOverview->reportName."', "
                . "'".$reportOverview->reportDateCreated."', "
                . "'".$reportOverview->reportUserID."', "
                . "'".$reportOverview->reportEmailSent."', "
                . "'".$reportOverview->reportDynamicFlag."', "
                . "'".$reportOverview->reportActive."', "
                . "'".$reportOverview->changedByUserID."', "
                . "'".$reportOverview->dateOfChange."')";
        
        if (!mysqli_query(DatabaseHistory::$connection, $result))
        {
            debugWriter("debug.txt", "setNewReportOverview FAILED $result ".mysqli_error(DatabaseHistory::$connection));
            return null;
        } else {
            return mysqli_insert_id(DatabaseHistory::$connection);
        }
    }
    
    /**
     * getReportOverviewHistoryByID
     * @param type $reportID
     * @return type
     */
    public function getReportOverviewHistoryByID($reportID) {
        $dbHistory = new DatabaseHistory();
        $historyReportOverviewSQLConstructor = new HistoryReportOverviewSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM $dbHistory->databaseHistory.report_overview_history_tbl "
                . "WHERE rot_id = '".$reportID."' ORDER BY report_history_date_changed DESC");
        
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['report_history_id']] = $historyReportOverviewSQLConstructor->createHistoryReportOverview($row);
        }
        
        if (!isset($array)) {
            return null;
        } else {
            return $array;
        }
    }
    
}
