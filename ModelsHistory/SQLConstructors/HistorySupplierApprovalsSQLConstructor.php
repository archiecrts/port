<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistorySupplierApprovalsSQLConstructor
 *
 * @author Archie
 */
class HistorySupplierApprovalsSQLConstructor {
    
    public function createHistorySupplierApprovals($row) {
        
        return new HistorySupplierApprovals(
                $row['supplier_approvals_history_id'],
                $row['sat_id'], 
                $row['customer_tbl_cust_id'], 
                $row['sat_approval_as_supplier'], 
                $row['sat_approval_as_trader'], 
                $row['sat_approval_as_agent'],
                $row['supplier_approval_status_lookup_tbl_saslt_id'],
                $row['sat_date_approval_status_confirmed'],
                $row['sat_status_review_date'],
                $row['sat_reason_for_status_change'],
                $row['supplier_approvals_history_changed_by_id'],
                $row['supplier_approvals_history_changed_date']);
    }
}
