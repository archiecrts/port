<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryReportOverviewSQLConstructor
 *
 * @author Archie
 */
class HistoryReportOverviewSQLConstructor {
    
    public function createHistoryReportOverview($row) {
        
        return new HistoryReportOverview(
                $row['report_history_id'],
                $row['rot_id'], 
                $row['rot_report_name'], 
                $row['rot_date_created'], 
                $row['rot_user_id'], 
                $row['rot_email_sent'],
                $row['rot_report_active'],
                $row['report_history_changed_by_id'],
                $row['report_history_date_changed']);
    }
}
