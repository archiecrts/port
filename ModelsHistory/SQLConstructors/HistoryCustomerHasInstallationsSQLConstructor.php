<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryCustomerHasInstallationsSQLConstructor
 *
 * @author Archie
 */
class HistoryCustomerHasInstallationsSQLConstructor {
    
    public function createHistoryCustomerHasInstallations($row) {
        
        return new HistoryCustomerHasInstallations(
                $row['customer_has_installations_history_id'],
                $row['customer_tbl_cust_id'], 
                $row['installations_tbl_inst_id'],
                $row['customer_has_installations_history_changed_by_id'],
                $row['customer_has_installations_history_changed_date']);
    }
    
}
