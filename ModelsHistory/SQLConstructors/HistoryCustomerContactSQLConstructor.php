<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryCustomerContactSQLConstructor
 *
 * @author Archie
 */
class HistoryCustomerContactSQLConstructor {
    
    public function createHistoryCustomerContact($row) {
        
        return new HistoryCustomerContact(
                $row['contact_history_id'],
                $row['ict_id'], 
                $row['ict_salutation'],
                $row['ict_first_name'], 
                $row['ict_surname'],
                $row['ict_job_title'], 
                $row['ict_email'], 
                $row['ict_number'],
                $row['ict_mobile_number'],
                $row['ict_info_source'],
                $row['ict_inactive'],
                $row['ict_contact_validated'],
                $row['customer_id'],
                $row['address_id'],
                $row['contact_history_changed_by_id'],
                $row['contact_history_date_changed']);
    }
}
