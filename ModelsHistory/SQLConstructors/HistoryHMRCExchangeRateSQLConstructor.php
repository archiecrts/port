<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryHMRCExchangeRateSQLConstructor
 *
 * @author Archie
 */
class HistoryHMRCExchangeRateSQLConstructor {
    
    public function createHistoryHMRCExchangeRate($row) {
        
        return new HistoryDailyExchangeRate(
                $row['hmrc_history_id'], 
                $row['hmrc_id'], 
                $row['hmrc_country'], 
                $row['hmrc_currency'], 
                $row['hmrc_currency_code'], 
                $row['hmrc_currency_units_per_pound'], 
                $row['hmrc_start_date'], 
                $row['hmrc_end_date'],
                $row['hmrc_history_changed_by_id'], 
                $row['hmrc_history_date_changed']);
    }
}
