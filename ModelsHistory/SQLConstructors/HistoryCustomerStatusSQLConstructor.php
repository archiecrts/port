<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryCustomerStatusSQLConstructor
 *
 * @author Archie
 */
class HistoryCustomerStatusSQLConstructor {
    
    public function createHistoryCustomerStatus($row) {
        
        return new HistoryCustomerStatus(
                $row['customer_status_history_id'],
                $row['cst_id'], 
                $row['customer_tbl_cust_id'], 
                $row['customer_status_lookup_tbl_cslt_id'], 
                $row['customer_status_history_changed_by_id'], 
                $row['customer_status_history_date_changed']);
    }
    
}
