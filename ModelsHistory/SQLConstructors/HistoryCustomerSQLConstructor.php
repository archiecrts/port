<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryCustomerSQLConstructor
 *
 * @author Archie
 */
class HistoryCustomerSQLConstructor {
    
    public function createHistoryCustomer($row) {
        
        return new HistoryCustomer(
                $row['customer_history_id'],
                $row['cust_id'], 
                $row['cust_name'], 
                $row['cust_telephone'], 
                $row['cust_fax'], 
                $row['cust_website'],
                $row['cust_account_manager'],
                $row['cust_tbh_id'],
                $row['cust_tbh_account_code'],
                $row['cust_ld_id'],
                $row['cust_ld_account_code'],
                $row['cust_header_notes'],
                $row['cust_seaweb_code'],
                $row['cust_parent_company_seaweb_code'],
                $row['cust_parent_company_nationality_of_control'],
                $row['cust_company_status'],
                $row['cust_company_full_name'],
                $row['cust_navision_id'],
                $row['customer_history_changed_by_id'],
                $row['customer_history_date_changed']);
    }
}