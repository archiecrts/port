<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryReportParameterSQLConstructor
 *
 * @author Archie
 */
class HistoryReportParameterSQLConstructor {
    
    public function createHistoryReportParameter($row) {
        
        return new HistoryReportParameter(
                $row['parameter_history_id'],
                $row['par_id'], 
                $row['rot_id'], 
                $row['par_parameter'], 
                $row['par_value'], 
                $row['parameter_history_changed_by_id'],
                $row['parameter_history_date_changed']);
    }
}
