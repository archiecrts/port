<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryCustomerCreditSQLConstructor
 *
 * @author Archie
 */
class HistoryCustomerCreditSQLConstructor {
    
    public function createHistoryCustomerCredit($row) {
        
        return new HistoryCustomerCredit(
                $row['credit_history_id'],
                $row['cct_id'], 
                $row['cct_credit_limit'], 
                $row['cct_account_stop'], 
                $row['customer_id'], 
                $row['cct_payment_terms'],
                $row['credit_history_changed_by_id'],
                $row['credit_history_date_changed']);
    }
}
