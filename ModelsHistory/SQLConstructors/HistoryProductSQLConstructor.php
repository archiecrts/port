<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryProductSQLConstructor
 *
 * @author Archie
 */
class HistoryProductSQLConstructor {
    
    public function createHistoryProduct($row) {
        
        return new HistoryProduct(
                $row['product_history_id'],
                $row['prod_id'], 
                $row['prod_serial_number'], 
                $row['prod_description'], 
                $row['prod_drawing_number'], 
                $row['prod_comments'],
                $row['prod_enquiry_only_flag'],
                $row['customer_tbl_cust_id'],
                $row['installations_tbl_inst_id'],
                $row['version_lookup_tbl_vlt_id'],
                $row['prod_unit_name'],
                $row['prod_source_alias_id'],
                $row['product_history_changed_by_id'],
                $row['product_history_changed_date']);
    }
}
