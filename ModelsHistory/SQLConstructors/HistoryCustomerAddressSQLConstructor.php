<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryCustomerAddressSQLConstructor
 *
 * @author Archie
 */
class HistoryCustomerAddressSQLConstructor {
    
    public function createHistoryCustomerAddress($row) {
        
        return new HistoryCustomerAddress(
                $row['address_history_id'],
                $row['cadt_id'], 
                $row['cadt_office_name'], 
                $row['cadt_address'], 
                $row['cadt_city'], 
                $row['cadt_state'],
                $row['cadt_country'],
                $row['cadt_postcode'],
                $row['cadt_address_verified'],
                $row['customer_id'],
                $row['cadt_archived'],
                $row['cadt_default_address'],
                $row['cadt_full_address'],
                $row['address_history_changed_by_id'],
                $row['address_history_date_changed']);
    }
}
