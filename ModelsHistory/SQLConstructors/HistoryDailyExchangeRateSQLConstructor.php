<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryDailyExchangeRateSQLConstructor
 *
 * @author Archie
 */
class HistoryDailyExchangeRateSQLConstructor {
    
    public function createHistoryDailyExchangeRate($row) {
        
        return new HistoryDailyExchangeRate(
                $row['daily_history_id'], 
                $row['daily_id'], 
                $row['daily_currency_code'], 
                $row['daily_currency_unit_per_pound'], 
                $row['daily_last_changed'], 
                $row['daily_history_changed_by_id'], 
                $row['daily_history_date_changed']);
    }
}
