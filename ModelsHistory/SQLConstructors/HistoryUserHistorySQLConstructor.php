<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of UserHistorySQLConstructor
 *
 * @author Archie
 */
class HistoryUserHistorySQLConstructor {
    
    public function createHistoryUserHistory($row) {
        
        return new HistoryUserHistory(
                $row['user_history_id'],
                $row['usr_id'], 
                $row['usr_login'], 
                $row['usr_password'], 
                $row['usr_name'], 
                $row['level_id'],
                $row['dept_id'],
                $row['usr_email'],
                $row['usr_initial_login'],
                $row['usr_retire_user_flag'],
                $row['user_history_changed_by_id'],
                $row['user_history_date_changed']);
    }
}
