<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HistoryNoticeBoardSQLConstructor
 *
 * @author Archie
 */
class HistoryNoticeBoardSQLConstructor {
    
    public function createHistoryNoticeBoard($row) {
        
        return new HistoryUserHistory(
                $row['notice_history_id'],
                $row['nbt_id'], 
                $row['nbt_title'], 
                $row['nbt_date'], 
                $row['nbt_content'], 
                $row['nbt_archive'],
                $row['nbt_authorised'],
                $row['nbt_user_id'],
                $row['nbt_category_id'],
                $row['nbt_document_id'],
                $row['notice_history_changed_by_id'],
                $row['notice_history_date_changed']);
    }
}
