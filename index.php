<?php
session_start();

include("Views/pageheader.php");

?>

        <div id="content">
        <?php
        include_once('Controllers/MasterController.php');
        include_once('Controllers/MaintenanceController.php');
        
        $maintenanceController = new MaintenanceController();
        $maintenanceController->getMaintenanceSwitch();
        
        $controller = new MasterController();
        $controller->invoke();
        ?>
        </div>
    </body>
</html>
