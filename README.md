# README #

Download repository, set up database, get crackin'.

### Procedures ###
Production server exists on the E: drive @ E:\INTRANET\xampp\htdocs\PORT
Release Candidate Server (DRYDOCK) exists on the F: drive @ F:\DRYDOCK\xampp\htdocs\DRYDOCK

On each server (referred to as PORT and DRYDOCK) XAMPP must be set up within either the INTRANET or DRYDOCK folder as appropriate. 

The configuration changes for the initial install of XAMPP are as follows:
Before starting either the Apache server or MySQL server open php.ini

### php.ini ###

* max_execution_time=720
* max_input_time=120
* memory_limit=128M
* post_max_size=15M
* upload_max_filesize=15M
* date.timezone=Europe/London

MySQL User Permissions need to be set on the server.
Password will not be kept on this page for security.
Change root@localhost and root@129.0.0.1 to have passwords.

The MySQL roles_mapping table gets messed up by doing this.
Using CMD (command line terminal) go to the mysql bin folder at

e:\INTRANET\xampp\mysql\bin (you might need to just type in cd mysql\bin if you have used the console from xampp)

type in at the #
**mysql_upgrade** and press enter. This will fix the permissions tables.

Use MySQL WORKBENCH forward engineering to create the initial installation of the database to the server. Check the logs for errors.

Use Source Tree to PULL down the latest repository from the PORT folder. Source Tree is already set up to do this.

### SETTING UP A SECOND SERVER FOR DRYDOCK ###
PORT will use the default server ports of 80 and 443. Because of this, DRYDOCK's server ports need to be altered so that both can be run concurrently.

To change the XAMPP Apache server port here the procedure :

1. Choose a free port number
Take a look to all your used ports with Netstat (integrated to XAMPP). 
Then you can see all used ports and here we see that the 80 port is already used by the System

Choose a free port number. I have used 8012

2. Edit the file "http.conf"
This file should be found in F:\DRYDOCK\xampp\apache\conf on Windows. Or just click on the Config button in XAMPP to find Apache (httpd.conf)
Locate the following lines :

Listen 80
ServerName localhost:80
Replace them by :

Listen 8012
ServerName localhost:8012.
Save the file.

Access to : http://localhost:8012 for check if it's work. You should see the dashboard.

If not, you musto edit the http-ssl.conf file. See step 3 below.

3. Edit the file "http-ssl.conf"
This file should be found in C:\xampp\apache\conf\extra on Windows or see this link for Linux.
Locate the following lines :

Listen 443
<VirtualHost _default_:443>
ServerName www.example.com:443 or  ServerName localhost:433
Replace them by with a other port number (8013 for this example) :

Listen 8013
<VirtualHost _default_:8013>
ServerName www.example.com:8013 or  ServerName localhost:8013
Save the file.

Restart the Apache Server.

Access to : http://localhost:8012 for check if it's work.

(I did this anyway, just in case we get SSL in the future)

Reach DRYDOCK at http://localhost:8012/DRYDOCK/ or http://smplxint:8012/DRYDOCK/ from outside of the server machine.

To change the listener for mysql from port 3306 to 3307 do the following.

1. Stop the xampp server, if it is already running.
2. Edit the value to "port" in 
                 xampp/mysql/bin/my.ini (again use the config button on the Apache line)

Code:

\# Password = your_password 

  port             =  3306  --->  3307

  socket           =  "/ xampp / mysql / mysql.sock"

and here also 

Code:

\# The MySQL server  [ mysqld ]

  port =  3306  --->  3307

  socket =  "/ xampp / mysql / mysql.sock"

2. Start mysql service

Goto xampp>phpMyAdmin Directory. Find config.inc.php file (or use XAMPP's Apache config button again). 

Now change this line: 

$cfg['Servers'][$i]['host'] = '127.0.0.1';

 To 

$cfg['Servers'][$i]['host'] = '127.0.0.1:3307'; 

Now, restart your server.

DRYDOCK phpmyadmin reached at http://localhost:8012/phpmyadmin/

If you receive the error **Fatal error: Call to undefined function mysql_connect() in ...**


Then uncomment extension=php_mysql.dll in php.ini

### Setting up WEBDAV for the Outlook Calendars ###


### Auto start on virtual server restart ###

http://kushalkolekar.blogspot.co.uk/2013/10/make-xampp-to-run-automatically-on.html


### What is this repository for? ###


* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact