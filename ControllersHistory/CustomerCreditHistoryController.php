<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include 'ModelsHistory/CustomerCreditHistoryModel.php';
/**
 * Description of CustomerCreditHistoryController
 *
 * @author Archie
 */
class CustomerCreditHistoryController {
    public $customerCreditHistoryModel;
    
    /**
     * __construct function.
     */
    public function __construct()
    {
        $this->customerCreditHistoryModel = new CustomerCreditHistoryModel();
    }
    
    /**
     * 
     */
    public function invoke()
    {

    }
    
    /**
     * setCustomerCreditHistory
     * @param Entity $customerCreditHistory
     * @return type
     */
    public function setCustomerCreditHistory($customerCreditHistory) 
    {
        return $this->customerCreditHistoryModel->setCustomerCreditHistory($customerCreditHistory);
    }
}
