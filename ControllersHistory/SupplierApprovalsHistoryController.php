<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include 'ModelsHistory/SupplierApprovalsHistoryModel.php';
/**
 * Description of SupplierApprovalsHistoryController
 *
 * @author Archie
 */
class SupplierApprovalsHistoryController {
    
    public $supplierApprovalsHistoryModel;
    
    /**
     * __construct function.
     */
    public function __construct()
    {
        $this->supplierApprovalsHistoryModel = new SupplierApprovalsHistoryModel();
    }
    
    /**
     * 
     */
    public function invoke()
    {

    }
    
    /**
     * setSupplierApprovalsHistory
     * @param type $supplierApprovalsHistory
     * @return type
     */
    public function setSupplierApprovalsHistory($supplierApprovalsHistory) {
        return $this->supplierApprovalsHistoryModel->setSupplierApprovalsHistory($supplierApprovalsHistory);
    }
}
