<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include 'ModelsHistory/ReportOverviewHistoryModel.php';
/**
 * Description of ReportOverviewHistoryController
 *
 * @author Archie
 */
class ReportOverviewHistoryController {

    public $reportOverviewHistoryModel;
    
    /**
     * __construct function.
     */
    public function __construct()
    {
        $this->reportOverviewHistoryModel = new ReportOverviewHistoryModel();
    }
    
    /**
     * 
     */
    public function invoke()
    {
        //include 'Views/Users/users.php';
    }
    
    
    /**
     * setNewReportOverview
     * @param type $reportOverview
     * @return type
     */
    public function setNewReportOverview($reportOverview) 
    {
        return $this->reportOverviewHistoryModel->setNewReportOverview($reportOverview);
    }
    
    /**
     * getReportOverviewHistoryByID
     * @param type $reportID
     * @return type
     */
    public function getReportOverviewHistoryByID($reportID) {
        return $this->reportOverviewHistoryModel->getReportOverviewHistoryByID($reportID);
    }
}
