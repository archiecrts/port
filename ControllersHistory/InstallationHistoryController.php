<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include 'ModelsHistory/InstallationHistoryModel.php';
/**
 * Description of InstallationHistoryController
 *
 * @author Archie
 */
class InstallationHistoryController {
    public $installationHistoryModel;
    
    /**
     * __construct function.
     */
    public function __construct()
    {
        $this->installationHistoryModel = new InstallationHistoryModel();
    }
    
    /**
     * 
     */
    public function invoke()
    {

    }
    
    /**
     * setInstallationHistory
     * @param Entity $installationHistory
     * @return type
     */
    public function setInstallationHistory($installationHistory) 
    {
        return $this->installationHistoryModel->setInstallationHistory($installationHistory);
    }
    
    /**
     * getInstallationHistory
     * @param type $installationID
     * @return boolean
     */
    public function getInstallationHistory($installationID) {
        return $this->installationHistoryModel->getInstallationHistory($installationID);
    }
}
