<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include 'ModelsHistory/CustomerHasInstallationsHistoryModel.php';
/**
 * Description of CustomerHasInstallationsHistoryController
 *
 * @author Archie
 */
class CustomerHasInstallationsHistoryController {
    public $customerHasInstallationsHistoryModel;
    
    /**
     * __construct function.
     */
    public function __construct()
    {
        $this->customerHasInstallationsHistoryModel = new CustomerHasInstallationsHistoryModel();
    }
    
    /**
     * 
     */
    public function invoke()
    {

    }
    
    /**
     * setCustomerHasInstallationsHistory
     * @param type $customerHistory
     * @return type
     */
    public function setCustomerHasInstallationsHistory($customerHistory) 
    {
        return $this->customerHasInstallationsHistoryModel->setCustomerHasInstallationsHistory($customerHistory);
    }
    
    /**
     * getHistoryForInstallation
     * @param type $installationID
     * @return type
     */
    public function getHistoryForInstallation($installationID) {
        return $this->customerHasInstallationsHistoryModel->getHistoryForInstallation($installationID);
    }
}
