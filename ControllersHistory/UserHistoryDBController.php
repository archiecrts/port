<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include 'ModelsHistory/UserHistoryDBModel.php';
/**
 * Description of UserHistoryController
 *
 * @author Archie
 */
class UserHistoryDBController {
    /**
     *
     * @var type UserModel
     */
    public $userHistoryDBModel;
    
    /**
     * __construct function.
     */
    public function __construct()
    {
        $this->userHistoryDBModel = new UserHistoryDBModel();
    }
    
    /**
     * 
     */
    public function invoke()
    {
        //include 'Views/Users/users.php';
    }
    
    /**
     * setNewUserHistory
     * @param type $user
     * @return type
     */
    public function setNewUserHistory($user) 
    {
        return $this->userHistoryDBModel->setNewUser($user);
    }
    
    /**
     * getUserHistoryByID
     * @param INT $userID
     */
    public function getUserHistoryByID($userID) {
        return $this->userHistoryDBModel->getUserHistoryByID($userID);
    }
   
}
