<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include 'ModelsHistory/CustomerAddressHistoryModel.php';
/**
 * Description of CustomerAddressHistoryController
 *
 * @author Archie
 */
class CustomerAddressHistoryController {
    public $customerAddressHistoryModel;
    
    /**
     * __construct function.
     */
    public function __construct()
    {
        $this->customerAddressHistoryModel = new CustomerAddressHistoryModel();
    }
    
    /**
     * 
     */
    public function invoke()
    {

    }
    
    /**
     * setCustomerAddressHistory
     * @param Entity $customerAddressHistory
     * @return type
     */
    public function setCustomerAddressHistory($customerAddressHistory) 
    {
        return $this->customerAddressHistoryModel->setCustomerAddressHistory($customerAddressHistory);
    }
}
