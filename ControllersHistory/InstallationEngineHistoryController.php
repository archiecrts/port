<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include 'ModelsHistory/InstallationEngineHistoryModel.php';
/**
 * Description of InstallationEngineHistoryController
 *
 * @author Archie
 */
class InstallationEngineHistoryController {
    public $installationEngineHistoryModel;
    
    /**
     * __construct function.
     */
    public function __construct()
    {
        $this->installationEngineHistoryModel = new InstallationEngineHistoryModel();
    }
    
    /**
     * 
     */
    public function invoke()
    {

    }
    
    /**
     * setInstallationEngineHistory
     * @param Entity $installationEngineHistory
     * @return type
     */
    public function setInstallationEngineHistory($installationEngineHistory) 
    {
        return $this->installationEngineHistoryModel->setInstallationEngineHistory($installationEngineHistory);
    }
}
