<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include 'ModelsHistory/DailyExchangeRateHistoryModel.php';
/**
 * Description of DailyExchangeRateHistoryController
 *
 * @author Archie
 */
class DailyExchangeRateHistoryController {
    public $dailyExchangeRateHistoryModel;
    
    /**
     * __construct function.
     */
    public function __construct()
    {
        $this->dailyExchangeRateHistoryModel = new DailyExchangeRateHistoryModel();
    }
    
    /**
     * 
     */
    public function invoke()
    {

    }
    
    
    /**
     * setDailyExchangeRateHistory
     * @param type $dailyExchangeRateHistory
     * @return type
     */
    public function setDailyExchangeRateHistory($dailyExchangeRateHistory) 
    {
        return $this->dailyExchangeRateHistoryModel->setDailyExchangeRateHistory($dailyExchangeRateHistory);
    }
}
