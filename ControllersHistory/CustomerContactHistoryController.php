<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include 'ModelsHistory/CustomerContactHistoryModel.php';
/**
 * Description of CustomerContactHistoryController
 *
 * @author Archie
 */
class CustomerContactHistoryController {
    public $customerContactHistoryModel;
    
    /**
     * __construct function.
     */
    public function __construct()
    {
        $this->customerContactHistoryModel = new CustomerContactHistoryModel();
    }
    
    /**
     * 
     */
    public function invoke()
    {

    }
    
    /**
     * setCustomerContactHistory
     * @param Entity $customerContactHistory
     * @return type
     */
    public function setCustomerContactHistory($customerContactHistory) 
    {
        return $this->customerContactHistoryModel->setCustomerContactHistory($customerContactHistory);
    }
}
