<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include 'ModelsHistory/CustomerHistoryModel.php';
/**
 * Description of CustomerHistoryController
 *
 * @author Archie
 */
class CustomerHistoryController {
    public $customerHistoryModel;
    
    /**
     * __construct function.
     */
    public function __construct()
    {
        $this->customerHistoryModel = new CustomerHistoryModel();
    }
    
    /**
     * 
     */
    public function invoke()
    {

    }
    
    /**
     * setCustomerHistory
     * @param Entity $customerHistory
     * @return type
     */
    public function setCustomerHistory($customerHistory) 
    {
        return $this->customerHistoryModel->setCustomerHistory($customerHistory);
    }
}
