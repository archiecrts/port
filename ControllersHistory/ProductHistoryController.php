<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include 'ModelsHistory/ProductHistoryModel.php';
/**
 * Description of ProductHistoryController
 *
 * @author Archie
 */
class ProductHistoryController {
    public $productHistoryModel;
    
    /**
     * __construct function.
     */
    public function __construct()
    {
        $this->productHistoryModel = new ProductHistoryModel();
    }
    
    /**
     * 
     */
    public function invoke()
    {

    }
    
    
    /**
     * setProductHistory
     * @param type $productHistory
     * @return type
     */
    public function setProductHistory($productHistory) 
    {
        return $this->productHistoryModel->setProductHistory($productHistory);
    }
    
    /**
     * getProductHistory
     * @param type $productID
     * @return boolean
     */
    public function getProductHistory($productID) {
        return $this->productHistoryModel->getProductHistory($productID);
    }
}
