<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include 'ModelsHistory/HMRCExchangeRateHistoryModel.php';
/**
 * Description of HMRCExchangeRateHistoryController
 *
 * @author Archie
 */
class HMRCExchangeRateHistoryController {
    public $hmrcExchangeRateHistoryModel;
    
    /**
     * __construct function.
     */
    public function __construct()
    {
        $this->hmrcExchangeRateHistoryModel = new HMRCExchangeRateHistoryModel();
    }
    
    /**
     * 
     */
    public function invoke()
    {

    }
    
    /**
     * setHMRCExchangeRateHistory
     * @param type $hmrcExchangeRateHistory
     * @return type
     */
    public function setHMRCExchangeRateHistory($hmrcExchangeRateHistory) 
    {
        return $this->hmrcExchangeRateHistoryModel->setHMRCExchangeRateHistory($hmrcExchangeRateHistory);
    }
}
