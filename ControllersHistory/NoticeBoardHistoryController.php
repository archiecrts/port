<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include 'ModelsHistory/NoticeBoardHistoryModel.php';
/**
 * Description of NoticeBoardHistoryController
 *
 * @author Archie
 */
class NoticeBoardHistoryController {
    
    
    public $noticeBoardHistoryModel;
    
    /**
     * __construct function.
     */
    public function __construct()
    {
        $this->noticeBoardHistoryModel = new NoticeBoardHistoryModel();
    }
    
    /**
     * 
     */
    public function invoke()
    {
        //include 'Views/Users/users.php';
    }
    
    /**
     * setNewNoticeBoard
     * @param Entity $noticeBoard
     * @return INT | null
     */
    public function setNewNoticeBoard($noticeBoard) 
    {
        return $this->noticeBoardHistoryModel->setNewNoticeBoard($noticeBoard);
    }
    
}
