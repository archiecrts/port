<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include 'ModelsHistory/CustomerStatusHistoryModel.php';
/**
 * Description of CustomerStatusHistoryModel
 *
 * @author Archie
 */
class CustomerStatusHistoryController {
    
    public $customerStatusHistoryModel;
    
    /**
     * __construct function.
     */
    public function __construct()
    {
        $this->customerStatusHistoryModel = new CustomerStatusHistoryModel();
    }
    
    /**
     * 
     */
    public function invoke()
    {

    }
    
    /**
     * setCustomerStatusHistory
     * @param History Customer Status Object $customerStatusHistory
     * @return INT | NULL
     */
    public function setCustomerStatusHistory($customerStatusHistory) 
    {
        return $this->customerStatusHistoryModel->setCustomerStatusHistory($customerStatusHistory);
    }
}
