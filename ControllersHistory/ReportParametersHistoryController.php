<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include 'ModelsHistory/ReportParametersHistoryModel.php';
/**
 * Description of ReportParametersHistoryController
 *
 * @author Archie
 */
class ReportParametersHistoryController {
    public $reportParametersHistoryModel;
    
    /**
     * __construct function.
     */
    public function __construct()
    {
        $this->reportParametersHistoryModel = new ReportParametersHistoryModel();
    }
    
    /**
     * 
     */
    public function invoke()
    {
        //include 'Views/Users/users.php';
    }
    
    /**
     * setNewReportParameter
     * @param type $reportParameter
     * @return type
     */
    public function setNewReportParameter($reportParameter) 
    {
        return $this->reportParametersHistoryModel->setNewReportParameter($reportParameter);
    }
    
    /**
     * getReportParameterHistoryByID
     * @param INT $reportID
     * @return type
     */
    public function getReportParameterHistoryByID($reportID) {
        return $this->reportParametersHistoryModel->getReportParameterHistoryByID($reportID);
    }
}
