
function validateAddNewInstallation() {
    
    var website = document.forms["instForm"]["website"].value;
    var tel = document.forms["instForm"]["telephoneNumber"].value;
    var fax = document.forms["instForm"]["faxNumber"].value;
    var imo = document.forms['instForm']['imo'].value;
    
    var ret = true;
    var retString = "";

    if (website.length > 0) {
        if (!validateUrl(website)) {
            retString = "The web address is not valid<br/>";
            ret = false;
        }
    }
    if (tel.length > 0) {
        if (!validateNumber(tel)) {
            retString += "Phone number is not correct format<br/>";
            ret = false;
        }
    }
    
    if (fax.length > 0) {
        if (!validateNumber(fax)) {
            retString += "Fax number is not correct format<br/>";
            ret = false;
        }
    }
    
    if (imo.length > 0) {
        if (!validateIMO(imo)) {
            retString += "IMO must be a number, with no letters or other characters<br/>";
            ret = false;
        }
    }
    document.getElementById("error").innerHTML = retString;
    
    return ret;
}


// All the other functions.
function validateUrl(value){
    return /((ftp|https?):\/\/)?(www\.)?[a-z0-9\-\.]{3,}\.[a-z]{3}$/.test(value);
}

function validateEmail(value) {
    return /[^\s@]+@[^\s@]+\.[^\s@]+/.test(value);
}

function validateNumber(value) {
    return /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/i.test(value);
}

function validateIMO(value) {
    return /^[0-9]/.test(value);
}