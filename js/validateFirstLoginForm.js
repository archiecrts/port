// This holds all form validation code.
function validateFirstLogin() {
    var password = document.forms["form"]["password"].value;
    var confirmPassword = document.forms["form"]["confirmPassword"].value;
    var lowerCaseTest = /[a-z]/g;
    var upperCaseTest = /[A-Z]/g;
    var numericTest = /[0-9]/g;
    
    var ret = true;
    var retString = "";
    
    if (password === null || password === "") {
        retString = "Password must be filled out<br/>";
        ret = false;
    }
    if (confirmPassword === null || confirmPassword === "") {
        retString += "Confirm Password must be filled out<br/>";
        ret = false;
    }
    if (password !== confirmPassword) {
        retString += "Passwords do not match<br/>";
        
        ret = false;
    }
    if ((password.length < 8) || (password.length > 15)) {
        retString += "Password is not within 8 - 15 characters.<br/>";
        ret = false;
    }
    
    if (lowerCaseTest.test(password) === false) {
        retString += "Password must contain lower case letters.<br/>";
        ret = false;
    }
    
    if (upperCaseTest.test(password) === false) {
        retString += "Password must contain upper case letters.<br/>";
        ret = false;
    }   
    
    if (numericTest.test(password) === false) {
        retString += "Password must contain at least one number.<br/>";
        ret = false;
    }
    
    document.getElementById("error").innerHTML = retString;
    return ret;
}