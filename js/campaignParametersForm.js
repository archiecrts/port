$().ready(function() {  
    // Engine Manufacturer
   $('#add').click(function() {  
        $(function() {
            return !$('#select1 option:selected').remove().appendTo('#select2'); 
        });
        return !$('#select2').children().detach().sort(function(a, b) {
            return $(a).text().localeCompare($(b).text());
        }).appendTo('#select2');
    });  
   $('#remove').click(function() { 
        $(function() {
            return !$('#select2 option:selected').remove().appendTo('#select1'); 
        });
        return !$('#select1').children().detach().sort(function(a, b) {
            return $(a).text().localeCompare($(b).text());
        }).appendTo('#select1');
   }); 
   
   // Engine Model
   $('#addModel').click(function() {  
        $(function() {
            return !$('#model1 option:selected').remove().appendTo('#model2'); 
        });
        return !$('#model2').children().detach().sort(function(a, b) {
            return $(a).text().localeCompare($(b).text());
        }).appendTo('#model2');
    });  
   $('#removeModel').click(function() { 
        $(function() {
            return !$('#model2 option:selected').remove().appendTo('#model1'); 
        });
        return !$('#model1').children().detach().sort(function(a, b) {
            return $(a).text().localeCompare($(b).text());
        }).appendTo('#model1');
   }); 
   
   
   // Aux Engine Manufacturer
   $('#addAux').click(function() { 
        $(function() {
            return !$('#select3 option:selected').remove().appendTo('#select4');  
        });
        return !$('#select4').children().detach().sort(function(a, b) {
            return $(a).text().localeCompare($(b).text());
        }).appendTo('#select4');
   });  
   $('#removeAux').click(function() {
       $(function() {
        return !$('#select4 option:selected').remove().appendTo('#select3');  
        });
        return !$('#select3').children().detach().sort(function(a, b) {
            return $(a).text().localeCompare($(b).text());
        }).appendTo('#select3');
   }); 
   
   // Aux Engine Model
   $('#addAuxModel').click(function() {  
        $(function() {
            return !$('#model3 option:selected').remove().appendTo('#model4'); 
        });
        return !$('#model4').children().detach().sort(function(a, b) {
            return $(a).text().localeCompare($(b).text());
        }).appendTo('#model4');
    });  
   $('#removeAuxModel').click(function() { 
        $(function() {
            return !$('#model4 option:selected').remove().appendTo('#model3'); 
        });
        return !$('#model3').children().detach().sort(function(a, b) {
            return $(a).text().localeCompare($(b).text());
        }).appendTo('#model3');
   }); 
   
   
   // Area
   $('#addArea').click(function() {  
        $(function() {
            return !$('#selectArea1 option:selected').remove().appendTo('#selectArea2'); 
        });
        return !$('#selectArea2').children().detach().sort(function(a, b) {
            return $(a).text().localeCompare($(b).text());
        }).appendTo('#selectArea2');
    });  
   $('#removeArea').click(function() { 
        $(function() {
            return !$('#selectArea2 option:selected').remove().appendTo('#selectArea1'); 
        });
        return !$('#selectArea1').children().detach().sort(function(a, b) {
            return $(a).text().localeCompare($(b).text());
        }).appendTo('#selectArea1');
   }); 
   
   // Continent
   $('#addContinent').click(function() {  
        $(function() {
            return !$('#selectContinent1 option:selected').remove().appendTo('#selectContinent2'); 
        });
        return !$('#selectContinent2').children().detach().sort(function(a, b) {
            return $(a).text().localeCompare($(b).text());
        }).appendTo('#selectContinent2');
    });  
   $('#removeContinent').click(function() { 
        $(function() {
            return !$('#selectContinent2 option:selected').remove().appendTo('#selectContinent1'); 
        });
        return !$('#selectContinent1').children().detach().sort(function(a, b) {
            return $(a).text().localeCompare($(b).text());
        }).appendTo('#selectContinent1');
   }); 
   
   // Country
   $('#addCountry').click(function() {  
        $(function() {
            return !$('#selectCountry1 option:selected').remove().appendTo('#selectCountry2'); 
        });
        return !$('#selectCountry2').children().detach().sort(function(a, b) {
            return $(a).text().localeCompare($(b).text());
        }).appendTo('#selectCountry2');
    });  
   $('#removeCountry').click(function() { 
        $(function() {
            return !$('#selectCountry2 option:selected').remove().appendTo('#selectCountry1'); 
        });
        return !$('#selectCountry1').children().detach().sort(function(a, b) {
            return $(a).text().localeCompare($(b).text());
        }).appendTo('#selectCountry1');
   }); 
   
}); 
