// This holds all form validation code.
function validateAddUser() {
    //alert('validate');
    var displayName = document.forms["createUserForm"]["displayName"].value;
    var login = document.forms["createUserForm"]["login"].value;
    var email = document.forms['createUserForm']['email'].value;

    
    var ret = true;
    var retString = "";
    
    if (displayName === null || displayName === "") {
        retString = "Display Name must be filled out<br/>";
        ret = false;
    }
    if (login === null || login === "") {
        retString += "Login Password must be filled out<br/>";
        ret = false;
    }
    re = /^\w+$/;
    if(!re.test(login)) {
      retString +=  "Username must contain only letters, numbers and underscores<br/>";
      ret = false;
    }
    if (email === null || email === "") {
        retString += "Email must be filled out<br/>";
        ret = false;
    }
    // email check.
    if (checkEmail(email) === false) {
        retString += "Email address is not correctly formatted.<br/>";
        ret = false;
    }
    
    
    
    document.getElementById("error").innerHTML = retString;
    return ret;
    //return false;
}

// regex to check the email . 
function checkEmail(emailAddress) {
  var sQtext = '[^\\x0d\\x22\\x5c\\x80-\\xff]';
  var sDtext = '[^\\x0d\\x5b-\\x5d\\x80-\\xff]';
  var sAtom = '[^\\x00-\\x20\\x22\\x28\\x29\\x2c\\x2e\\x3a-\\x3c\\x3e\\x40\\x5b-\\x5d\\x7f-\\xff]+';
  var sQuotedPair = '\\x5c[\\x00-\\x7f]';
  var sDomainLiteral = '\\x5b(' + sDtext + '|' + sQuotedPair + ')*\\x5d';
  var sQuotedString = '\\x22(' + sQtext + '|' + sQuotedPair + ')*\\x22';
  var sDomain_ref = sAtom;
  var sSubDomain = '(' + sDomain_ref + '|' + sDomainLiteral + ')';
  var sWord = '(' + sAtom + '|' + sQuotedString + ')';
  var sDomain = sSubDomain + '(\\x2e' + sSubDomain + ')*';
  var sLocalPart = sWord + '(\\x2e' + sWord + ')*';
  var sAddrSpec = sLocalPart + '\\x40' + sDomain; // complete RFC822 email address spec
  var sValidEmail = '^' + sAddrSpec + '$'; // as whole string

  var reValidEmail = new RegExp(sValidEmail);

  return reValidEmail.test(emailAddress);
}