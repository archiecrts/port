// This holds all form validation code.
function validateEditUser() {

    //alert('validate');
    var displayName = document.forms["editUserForm"]["displayName"].value;
    var username = document.forms["editUserForm"]["username"].value;
    var email = document.forms['editUserForm']['email'].value;
    var password = document.forms['editUserForm']['password'].value;
    var confirmPassword = document.forms['editUserForm']['confirmPassword'].value;
    //alert(username);

    var ret = true;
    var retString = "";

    if (displayName === null || displayName === "") {
        retString = "Display Name must be filled out<br/>";
        ret = false;
    }
    if (username === null || username === "") {
        retString += "Username must be filled out<br/>";
        ret = false;
    }
    var re = /[^a-z\d]/i;
    if (re.test(username)) {
        retString += "Username must contain only letters, numbers and underscores<br/>";
        ret = false;
    }
    if (email === null || email === "") {
        retString += "Email must be filled out<br/>";
        ret = false;
    }
    // email check.
    if (checkEmail(email) === false) {
        retString += "Email address is not correctly formatted.<br/>";
        ret = false;
    }



    if (password !== "" && (password === confirmPassword)) {
        if (password.length < 8) {
            retString += "Password must contain at least eight characters<br/>";
            ret = false;
        }
        if (password === username) {
            retString += "Password must be different from Username<br/>";
            ret = false;
        }
        re = /[0-9]/;
        if (!re.test(password)) {
            retString += "password must contain at least one number (0-9)<br/>";
            ret = false;
        }
        re = /[a-z]/;
        if (!re.test(password)) {
            retString += "password must contain at least one lowercase letter (a-z)<br/>";
            ret = false;
        }
        re = /[A-Z]/;
        if (!re.test(password)) {
            retString += "password must contain at least one uppercase letter (A-Z)<br/>";
            ret = false;
        }
    } else if (password !== "" && (password !== confirmPassword)) {
        retString += "Please check that you've entered and confirmed your password<br/>";
        ret = false;
    }


    document.getElementById("error").innerHTML = retString;
    return ret;
    //return false;
}

// regex to check the email . 
function checkEmail(emailAddress) {
    var sQtext = '[^\\x0d\\x22\\x5c\\x80-\\xff]';
    var sDtext = '[^\\x0d\\x5b-\\x5d\\x80-\\xff]';
    var sAtom = '[^\\x00-\\x20\\x22\\x28\\x29\\x2c\\x2e\\x3a-\\x3c\\x3e\\x40\\x5b-\\x5d\\x7f-\\xff]+';
    var sQuotedPair = '\\x5c[\\x00-\\x7f]';
    var sDomainLiteral = '\\x5b(' + sDtext + '|' + sQuotedPair + ')*\\x5d';
    var sQuotedString = '\\x22(' + sQtext + '|' + sQuotedPair + ')*\\x22';
    var sDomain_ref = sAtom;
    var sSubDomain = '(' + sDomain_ref + '|' + sDomainLiteral + ')';
    var sWord = '(' + sAtom + '|' + sQuotedString + ')';
    var sDomain = sSubDomain + '(\\x2e' + sSubDomain + ')*';
    var sLocalPart = sWord + '(\\x2e' + sWord + ')*';
    var sAddrSpec = sLocalPart + '\\x40' + sDomain; // complete RFC822 email address spec
    var sValidEmail = '^' + sAddrSpec + '$'; // as whole string

    var reValidEmail = new RegExp(sValidEmail);

    return reValidEmail.test(emailAddress);
}