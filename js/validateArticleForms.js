// This holds all form validation code.
function validateArticle() {
    var title = document.forms["form"]["title"].value;
    var date = document.forms["form"]["date"].value;
    var content = document.forms["form"]["content"].value;
    
    var ret = true;
    var retString = "";
    
    if (title == null || title == "") {
        retString = "Title must be filled out\r\n";
        ret = false;
    }
    if (date == null || date == "") {
        retString += "Date must be filled out\r\n";
        ret = false;
    }
    if (content == null || content == "") {
        document.getElementById("demo").innerHTML = "Please add the article's content";
        //retString += "Content must be filled out\r\n";
        ret = false;
    }
    /*if (ret !== "") {
        alert(retString);
    }*/
    return ret;
}