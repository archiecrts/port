<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/EnquiryType.php");
include_once 'Models/SQLConstructors/EnquiryTypeSQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of EnquiryTypeModel
 *
 * @author alexandra
 */
class EnquiryTypeModel {
    /**
     * getAllEnquiryTypes
     * @return Array | null
     */
    public function getAllEnquiryTypes() {
        $enquiryTypeSQLConstructor = new EnquiryTypeSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM enquiry_type_tbl ORDER BY et_type ASC");

        //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)){
           $array[$row['et_id']]=$enquiryTypeSQLConstructor->createEnquiryType($row);
        }

        if (isset($array)){
            return $array;
        } else {
            return null;
        }
     }
    /**
     * getEnquiryTypeByID
     * @param INT $typeID
     * @return ENTITY
     */
     public function getEnquiryTypeByID($typeID) {
        $enquiryTypeSQLConstructor = new EnquiryTypeSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM enquiry_type_tbl WHERE et_id='".$typeID."'");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);
        $enquiryType = $enquiryTypeSQLConstructor->createEnquiryType($row);
      

        
        return $enquiryType;
        
     }
}
