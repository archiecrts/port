<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/Installation.php");
//include_once ("Models/DB.php");
include_once("Models/Database.php");

/**
 * Description of ManualImportModel
 *
 * @author Archie
 */
class ManualImportModel {

// SET GLOBAL VARIABLES
    /**
     * getAllFromManualCheckTbl
     * @return type
     */
    public function getFromManualCheckTblByTempID($tempID) {

        $result = mysqli_query(Database::$connection, "SELECT * FROM manual_check_tbl WHERE temp_id = '" . $tempID . "'");

        $row = mysqli_fetch_assoc($result);

        if (isset($row)) {
            return $row;
        } else {
            debugWriter("debug.txt", "getFromManualCheckTblByTempID FAILED " . mysqli_error(Database::$connection));
            return null;
        }
    }

    /**
     * dropManualCheckTbl
     */
    public function dropManualCheckTbl() {
        $drop_table = "DROP TABLE manual_check_tbl";
        if (!mysqli_query(Database::$connection, $drop_table)) {
            debugWriter("debug.txt", "Can't drop manual_check_tbl - gravity failure : " . mysqli_error(Database::$connection) . "");
        }
    }

    /**
     * deleteFromManualCheckTblByTempID
     * @param type $tempID
     * @return boolean
     */
    public function deleteFromManualCheckTblByTempID($tempID) {
        $delete = "DELETE FROM manual_check_tbl WHERE temp_id='" . $tempID . "'";

        if (!mysqli_query(Database::$connection, $delete)) {
            debugWriter("debug.txt", "deleteFromManualCheckTblByTempID FAILED $delete " . mysqli_error(Database::$connection));
            return false;
        } else {
            return true;
        }
    }

    /**
     * createManualCustomers
     * @param type $tempID
     */
    public function createManualCustomers($tempID) {

        $describe = mysqli_query(Database::$connection, "DESCRIBE manual_check_tbl");
        $descArray = [];

        if (mysqli_num_rows($describe) > 0) {
            while ($row = mysqli_fetch_assoc($describe)) {

                array_push($descArray, $row['Field']);
            }
        }

        $customerQuery = "SELECT ";

        $keyArray = ['cust_name', 'cust_telephone', 'cust_fax', 'cust_website',
            'cadt_office_name', 'cadt_address', 'cadt_city',
            'cadt_state', 'cadt_country', 'cadt_postcode'];

        foreach ($keyArray as $name) {
            // Find all the relevant columns (up to three contacts is possible in uploadInstallationsTest.php)
            if (in_array($name, $descArray)) {
                $customerQuery .= " " . $name . ", ";
            }
        }
        $customerQuery .= " temp_id FROM manual_check_tbl WHERE temp_id = '" . $tempID . "'";


        $customerInsert = mysqli_query(Database::$connection, $customerQuery);
        // Clean the IMO field to remove dashes and #NAME entries.
        if ($customerInsert['inst_imo'] == "-") {
            $customerInsert['inst_imo'] = "";
        }
        if ($customerInsert['inst_imo'] == '#NAME') {
            $customerInsert['inst_imo'] = "";
        }

        $newCustomerCount = 0;
        $newAddressCount = 0;

        while ($customerQueryRow = mysqli_fetch_assoc($customerInsert)) {


            /*
             * Create a new entry.
             */
            $insertCustomer = "INSERT INTO customer_tbl (cust_name, cust_telephone, cust_fax,"
                    . "cust_website, cust_account_manager, cust_tbh_id, "
                    . "cust_tbh_account_code, cust_ld_id, cust_ld_account_code, cust_header_notes) VALUES ("
                    . "'" . addslashes((isset($customerQueryRow['cust_name']) ? $customerQueryRow['cust_name'] : "")) . "',"
                    . "'" . addslashes((isset($customerQueryRow['cust_telephone']) ? $customerQueryRow['cust_telephone'] : "")) . "', "
                    . "'" . addslashes((isset($customerQueryRow['cust_fax']) ? $customerQueryRow['cust_fax'] : "")) . "', "
                    . "'" . addslashes((isset($customerQueryRow['cust_website']) ? $customerQueryRow['cust_website'] : "")) . "', "
                    . "NULL, "
                    . "'0', "
                    . "'0', "
                    . "'0', "
                    . "'0', "
                    . "'')";

            if (!mysqli_query(Database::$connection, $insertCustomer)) {
                debugWriter("loadInstallationDataLogs.txt", "Cannot Insert Customer : " . mysqli_error(Database::$connection));
            } else {
                $newCustomerCount++;
            }
            $customerID = mysqli_insert_id(Database::$connection);
            
            $customerType = '1';
                if (isset($customerQueryRow['cust_type'])) {

                    $selectStatus = mysqli_query(Database::$connection, "SELECT cslt_id FROM customer_status_lookup_tbl "
                            . "WHERE cslt_status = '" . $customerQueryRow['cust_type'] . "'");
                    $statusRow = mysqli_fetch_assoc($selectStatus);
                    $customerType = $statusRow['cslt_id'];
                }
                // Status is held in a separate table because there can be 1 or many
                $insertCustomerStatus = "INSERT INTO customer_status_tbl "
                        . "(customer_tbl_cust_id, customer_status_lookup_tbl_cslt_id) "
                        . "VALUES "
                        . "('" . $customerID . "', "
                        . "'" . $customerType . "')";
                if (!mysqli_query(Database::$connection, $insertCustomerStatus)) {
                    debugWriter("loadInstallationDataLogs.txt", "Cannot Insert Customer Status : " . mysqli_error(Database::$connection));
                }
            /*
             * Create an entry into the address table.
             */

            $insertAddress = "INSERT INTO customer_address_tbl (cadt_office_name, cadt_address, "
                    . "cadt_city, cadt_state, cadt_country, cadt_postcode, cadt_address_verified, "
                    . "customer_tbl_cust_id, cadt_archived, cadt_default_address) "
                    . "VALUES ("
                    . "'" . addslashes((isset($customerQueryRow['cadt_office_name']) ? $customerQueryRow['cadt_office_name'] : "")) . "', "
                    . "'" . addslashes((isset($customerQueryRow['cadt_address']) ? $customerQueryRow['cadt_address'] : "")) . "', "
                    . "'" . addslashes((isset($customerQueryRow['cadt_city']) ? $customerQueryRow['cadt_city'] : "")) . "', "
                    . "'" . addslashes((isset($customerQueryRow['cadt_state']) ? $customerQueryRow['cadt_state'] : "")) . "', "
                    . "'" . addslashes((isset($customerQueryRow['cadt_country']) ? $customerQueryRow['cadt_country'] : "")) . "', "
                    . "'" . addslashes((isset($customerQueryRow['cadt_postcode']) ? $customerQueryRow['cadt_postcode'] : "")) . "', "
                    . "'0', "
                    . "'" . $customerID . "', "
                    . "'0', "
                    . "'1')";

            if (!mysqli_query(Database::$connection, $insertAddress)) {
                debugWriter("loadInstallationDataLogs.txt", "Cannot Insert Customer Address : " . mysqli_error(Database::$connection));
            } else {
                $newAddressCount++;
            }
        }


        print "<p>Added " . $newCustomerCount . " new customers</p>";
        print "<p>Added " . $newAddressCount . " new addresses</p>";
        return $customerID;
    }

    /**
     * createContacts
     */
    public function createManualContacts($tempID, $customerID = "") {
        $describe = mysqli_query(Database::$connection, "DESCRIBE manual_check_tbl");
        $descArray = [];

        if (mysqli_num_rows($describe) > 0) {
            while ($row = mysqli_fetch_assoc($describe)) {

                array_push($descArray, $row['Field']);
            }
        }

        $contactsString = "SELECT ";

        $keyArray = ['cust_name', 'ict_contact_name', 'ict_job_title', 'ict_email',
            'ict_number', 'ict_info_source'];

        foreach ($keyArray as $name) {
            // Find all the relevant columns (up to three contacts is possible in uploadInstallationsTest.php)
            if (in_array($name, $descArray)) {
                $contactsString .= " " . $name . ", ";
            }
        }
        $contactsString .= " temp_id FROM manual_check_tbl WHERE temp_id = '" . $tempID . "'";


        $contactsQuery = mysqli_query(Database::$connection, $contactsString); // get the records from the temporary table.
        // Start count of customers added
        $contactCount = 0;

        // Loop through the resultant records and perform the same actions on each row.
        while ($contactsQueryRow = mysqli_fetch_assoc($contactsQuery)) {
            // Create an array for each customer name and split the possible contacts into the array
            $contact = []; // empty array.
            //$customerID = ""; // empty customer ID
            foreach ($contactsQueryRow as $k => $r) {
                if ($k != "temp_id") {
                    if ($k != "cust_name") {
                        $id = substr($k, -1);
                        $contact[$contactsQueryRow['cust_name']][$id][substr($k, 0, -2)] = $r;
                    } else if ($customerID == ""){
                        // Get the customers ID
                        $getCustomerIDQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl WHERE "
                                . "cust_name = '" . $r . "'");

                        $customerIDRow = mysqli_fetch_assoc($getCustomerIDQuery);
                        $customerID = $customerIDRow['cust_id'];
                    }
                }
            }
            if (isset($contact[$contactsQueryRow['cust_name']])) {
                foreach ($contact[$contactsQueryRow['cust_name']] as $id => $columnName) {

                    $columnsString = "";
                    $valuesString = "";
                    $emptyRecord = 0; // this counts if the record has all empty columns and thus should not be inserted.
                    // check for existing customer contact records that match the new one to be inserted.
                    // Also check the history date to see when it was added.
                    $checkContacts = "SELECT count(*) FROM customer_contacts_tbl "
                            . "WHERE customer_tbl_cust_id = '" . $customerID . "' ";

                    foreach ($columnName as $col => $val) {
                        $columnsString .= "$col, ";
                        if ($val == '-' || $val == '#NAME?') { // if the data is useless then reset $val to be an empty string
                            $val = "";
                        }

                        if ($val != "") {
                            $val = addslashes($val);
                            $emptyRecord++;
                        }
                        $valuesString .= "'" . $val . "', ";
                        $checkContacts .= "AND $col = '$val' ";
                    }

                    if ($emptyRecord > 0) {

                        $checkContactsResult = mysqli_query(Database::$connection, $checkContacts);
                        if (!mysqli_query(Database::$connection, $checkContacts)) {
                            debugWriter("debug.txt", "SELECT FROM MANUAL CHECK TABLE FAILED : CHECK CONTACTS : " . $checkContacts . " " . mysqli_error(Database::$connection));
                        }
                        $rowCC = mysqli_fetch_assoc($checkContactsResult);

                        if ($rowCC['count(*)'] < 1) {
                            // create an insert statement.
                            $sqlString = "INSERT INTO customer_contacts_tbl ($columnsString "
                                    . "ict_inactive, ict_contact_validated, customer_tbl_cust_id, customer_address_tbl_cadt_id) "
                                    . "VALUES ($valuesString "
                                    . "'0', '0', '" . $customerID . "', NULL)";
                            if (!mysqli_query(Database::$connection, $sqlString)) {
                                debugWriter("debug.txt", "INSERT customer_contacts_tbl FAILED : " . $sqlString . " " . mysqli_error(Database::$connection));
                            } else {
                                $contactCount++;
                            }

                            // If the record contains something useful then we add it to the database.

                            $customerContactsID = mysqli_insert_id(Database::$connection);
                            // Insert a record into the history table as well.
                            $history = "INSERT INTO customer_contacts_history_tbl "
                                    . "(customer_contacts_tbl_ict_id, ccht_action, ccht_date, user_tbl_usr_id, ccht_origin) "
                                    . "VALUES ('" . $customerContactsID . "','Created', NOW(), '" . $_SESSION['user_id'] . "', 'MANUAL CHECK')";

                            if (!mysqli_query(Database::$connection, $history)) {
                                debugWriter("debug.txt", "INSERT HISTORY FAILED : " . $history . " " . mysqli_error(Database::$connection));
                            } else {
                                $customerContactsHistoryID = mysqli_insert_id(Database::$connection);
                                // add the address to the contact using the link table.
                                $checkAddress = mysqli_query(Database::$connection, "SELECT cadt_id FROM customer_address_tbl "
                                        . "WHERE customer_tbl_cust_id = '" . $customerID . "' "
                                        . "AND cadt_default_address = '1'");

                                $checkAddressRow = mysqli_fetch_assoc($checkAddress);

                                $addressHasContactsHistory = "INSERT INTO customer_address_tbl_has_customer_contacts_history_tbl "
                                        . "(customer_address_tbl_cadt_id, customer_contacts_history_tbl_ccht_id) "
                                        . "VALUES ('" . $checkAddressRow['cadt_id'] . "', '" . $customerContactsHistoryID . "')";

                                if (!mysqli_query(Database::$connection, $addressHasContactsHistory)) {
                                    debugWriter("debug.txt", "INSERT ADDRESS/HISTORY FAILED : " . $addressHasContactsHistory . " " . mysqli_error(Database::$connection));
                                }
                            }
                        }
                    }
                }
            }
        }
        print "<p>Added " . $contactCount . " new contacts<p/>";
    }

    /**
     * createInstallations
     */
    public function createManualInstallations($tempID, $customerID = "") {

        //debugWriter("debug.txt", "createManualInstallations CUSTOMER ID ".$customerID. " TEMP ID ".$tempID);
        $describe = mysqli_query(Database::$connection, "DESCRIBE manual_check_tbl");
        $descArray = [];

        if (mysqli_num_rows($describe) > 0) {
            while ($row = mysqli_fetch_assoc($describe)) {

                array_push($descArray, $row['Field']);
            }
        }

        $installationString = "SELECT ";

        $keyArray = ['cust_name', 'inst_type', 'inst_installation', 'inst_installation_name', 'inst_imo',
            'installation_status_tbl_stat_id', 'inst_owner_parent_company', 'inst_technical_manager_company', 
            'inst_original_source', 'inst_original_source_date', 'inst_photo_link', 'inst_hull_number', 
            'inst_hull_type', 'inst_yard_built', 'inst_deadweight', 'inst_length_overall', 'inst_beam_extreme', 
            'inst_beam_noulded', 'inst_built_date', 'inst_draught', 'inst_gross_tonnage', 'inst_length', 
            'inst_propeller_type', 'inst_propulsion_unit_count', 'inst_ship_builder', 'inst_teu', 'inst_build_year'];
        
        $statusIDCount = 0;
        foreach ($keyArray as $name) {
            // Find all the relevant columns (up to three contacts is possible in uploadInstallationsTest.php)
            
            if (in_array($name, $descArray)) {
                $installationString .= " " . $name . ", ";
                if ($name == 'installation_status_tbl_stat_id') {
                    $statusIDCount = 1;
                }
            }
        }
        $installationString .= " temp_id FROM manual_check_tbl where temp_id = '" . $tempID . "'";


        $installationQuery = mysqli_query(Database::$connection, $installationString); // get the records from the temporary table.
       // debugWriter("debug.txt", 'Inside manual check inst '.$installationString.' '.mysqli_error(Database::$connection));
 
        // Start count of installations
        $installationCount = 0;
        $return = "";
        // Loop through the resultant records and perform the same actions on each row.
        while ($installationQueryRow = mysqli_fetch_assoc($installationQuery)) {
            //debugWriter("debug.txt", "Inside the while loop");

            $installation = []; // empty array.
            

            /**
             * Set the customer name, or find its ID.
             */
            foreach ($installationQueryRow as $s => $t) {
                if ($s != "temp_id") {
                    if (($s != "cust_name") && 
                            ($s != 'inst_original_source_date')
                            && ($s != 'inst_built_date') 
                            && ($s != 'inst_build_year')) {
                        $installation[$installationQueryRow['cust_name']][$s] = $t;
                    } else if ($s == "inst_original_source_date") {
                        $installation[$installationQueryRow['cust_name']][$s] = $t."-01-01 00:00:00";
                    } else if ($s == "inst_built_date") {
                        $installation[$installationQueryRow['cust_name']][$s] = $t."-01-01 00:00:00";
                    } else if ($s == "inst_build_year") {
                        $installation[$installationQueryRow['cust_name']][$s] = $t."-01-01 00:00:00";
                    } else if (($s == "cust_name") && ($customerID == "")) {
                        // Get the customers ID
                        $getCustomerIDQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl WHERE "
                                . "cust_name = '" . $t . "'");

                        $customerIDRow = mysqli_fetch_assoc($getCustomerIDQuery);
                        $customerID = $customerIDRow['cust_id'];
                    }
                }
            }
            
            if ($statusIDCount == 0) {
                $installation[$installationQueryRow['cust_name']]["installation_status_tbl_stat_id"] = "1";
            }

            /*
             * check if installation name exists in the database, and check the customer has installation table
             * to see if the record exists there. If the installation exists but not with a customer, then it
             * should be attached to the customer. If it exists but with a different customer, then we need to
             * either swap the customers and add the record change to the history table, or we need to do something
             * manually to decide which customer the installation attaches to.
             */

            // Clean the customer IMO if necessary
            $imoString = "";
            if (isset($installation[$installationQueryRow['cust_name']]['inst_imo'])) {
                if ($installation[$installationQueryRow['cust_name']]['inst_imo'] == "-") {
                    $installation[$installationQueryRow['cust_name']]['inst_imo'] = "";
                }
                if ($installation[$installationQueryRow['cust_name']]['inst_imo'] == '#NAME') {
                    $installation[$installationQueryRow['cust_name']]['inst_imo'] = "";
                }
                if ($installation[$installationQueryRow['cust_name']]['inst_imo'] != "") {
                    $imoString = "AND inst_imo = '" . addslashes($installation[$installationQueryRow['cust_name']]['inst_imo']) . "'";
                }
            }




            $columnsString = "";
            $valuesString = "";
            $emptyRecord = 0;
            $locationType = "";


            foreach ($installation[$installationQueryRow['cust_name']] as $col => $val) {
                //debugWriter("debug.txt", "AT FOREACH HEAD $col => $val");
                if ($col == 'inst_type') {
                    $locationType = strtoupper($val);
                }
                if ($col == "inst_owner_parent_company") {
                    $getOwnerIDQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl WHERE "
                                . "cust_name = '" . addslashes($val) . "'");

                        $ownerIDRow = mysqli_fetch_assoc($getOwnerIDQuery);
                        $ownerID = $ownerIDRow['cust_id'];
                        
                        if ($ownerID == "" || !isset($ownerID)) {
                            $insertNewOwnerParent = "INSERT INTO customer_tbl (cust_name) VALUES ('".addslashes($val)."')";
                            if (!mysqli_query(Database::$connection, $insertNewOwnerParent)) {
                                debugWriter("debug.txt", "INSERT OWNER PARENT FAILED : " . $insertNewOwnerParent . " " . mysqli_error(Database::$connection));
                            }
                            $val = mysqli_insert_id(Database::$connection);
                        } else  {
                            $val = $ownerID;
                        }
                        
                }
                $columnsString .= "$col, ";
                if ($val == '-' || $val == '#NAME?') { // if the data is useless then reset $val to be an empty string
                    $val = "";
                }

                if ($val != "") {
                    if ($col == 'installation_status_tbl_stat_id') {
                       // debugWriter("debug.txt", "inside status ID");
                        // this value needs to be taken from the loop-up table.
                        $statusQuery = mysqli_query(Database::$connection, "SELECT stat_id FROM installation_status_tbl "
                                . "WHERE stat_status_description LIKE '%" . $val . "%' AND stat_type = '" . $locationType . "'");
                        $statusRow = mysqli_fetch_assoc($statusQuery);
                       // debugWriter("debug.txt", "Status ID error ".mysqli_error(Database::$connection));
                        if (!isset($statusRow['stat_id'])) {
                            if ($locationType == 'LAND') {
                                $val = 1;
                            } else {
                                $val = 5;
                            }
                        } else {
                            $val = $statusRow['stat_id'];
                        }
                    }
                    $val = addslashes($val);
                    $emptyRecord++;
                }
                $valuesString .= "'" . $val . "', ";
            }
           // debugWriter("debug.txt", "EMPTY RECORD VAL $emptyRecord");
            if ($emptyRecord > 0) {
               // debugWriter("debug.txt", "Inside if empty record > 0");

                $insertInstallation = "INSERT INTO installations_tbl (" . substr($columnsString, 0, -2) . ") VALUES (" . substr($valuesString, 0, -2) . ")";

                if (!mysqli_query(Database::$connection, $insertInstallation)) {
                    debugWriter("loadInstallationDataLogs.txt", "LINE 429 insertInstallation FAILED : $insertInstallation " . mysqli_error(Database::$connection));
                } else {
                    $installationCount++;
                    $return = mysqli_insert_id(Database::$connection);
                }

                $insertCustomerHasInstallation = "INSERT INTO customer_tbl_has_installations_tbl "
                        . "(customer_tbl_cust_id, installations_tbl_inst_id) VALUES ('$customerID', '" . mysqli_insert_id(Database::$connection) . "')";
                if (!mysqli_query(Database::$connection, $insertCustomerHasInstallation)) {
                    debugWriter("loadInstallationDataLogs.txt", "insertCustomerHasInstallation FAILED : $insertCustomerHasInstallation " . mysqli_error(Database::$connection));
                }
            }
            //debugWriter("debug.txt", "OUTSIDE if empty record > 0");
        }
        print "<p>Added " . $installationCount . " new installations</p>";
        return $return;
    }

    /**
     * createTechnicalData
     */
    public function createManualTechnicalData($tempID, $installationID = "", $customerID = "") {

        // First make product
        $describe = mysqli_query(Database::$connection, "DESCRIBE manual_check_tbl");
        $descArray = [];

        if (mysqli_num_rows($describe) > 0) {
            while ($row = mysqli_fetch_assoc($describe)) {

                array_push($descArray, $row['Field']);
            }
        }

        $technicalString = "SELECT ";

        $keyArray = ['cust_name', 'inst_installation_name', 'inst_imo', 'prod_serial_number',
            'prod_description', 'prod_drawing_number', 'prod_comments',
            'iet_engine_manufacturer', 'iet_series', 'iet_engine_model', 'iet_main_aux', 
            'iet_engine_cylinder_count', 'iet_engine_cylinder_configuration',
            'iet_bore_size', 'iet_stroke',  'iet_fuel_type', 'iet_output',
            'iet_unit_name', 'iet_serial_number', 'iet_release_date', 'iet_comment',
            'load_priority_lookup_tbl_load_id'];

        foreach ($keyArray as $name) {
            // Find all the relevant columns (up to three contacts is possible in uploadInstallationsTest.php)
            if (in_array($name, $descArray)) {
                $technicalString .= " " . $name . ", ";
            }
        }
        $technicalString .= " temp_id FROM manual_check_tbl where temp_id = '" . $tempID . "'";


        $technicalQuery = mysqli_query(Database::$connection, $technicalString); // get the records from the temporary table.
        $engineCount = 0;
        $productCount = 0;
        
        while ($technicalRow = mysqli_fetch_assoc($technicalQuery)) {
            // foreach record, get the customer ID for that engine.
            if ($customerID == "") {
                $getCustomerIDQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl WHERE "
                        . "cust_name = '" . addslashes($technicalRow['cust_name']) . "'");

                $customerIDRow = mysqli_fetch_assoc($getCustomerIDQuery);
                $customerID = $customerIDRow['cust_id'];
            }
            
            $imoString = "";
            if (isset($technicalRow['inst_imo'])) {
                if ($technicalRow['inst_imo'] == "-") {
                    $technicalRow['inst_imo'] = "";
                }
                if ($technicalRow['inst_imo'] == '#NAME') {
                    $technicalRow['inst_imo'] = "";
                }
                if ($technicalRow['inst_imo'] != "") {
                    $imoString = "AND inst_imo = '" . addslashes($technicalRow['inst_imo']) . "'";
                }
            }
            // get the installation ID if it exists.
            if (isset($technicalRow['inst_installation_name']) && ($installationID == "")) {

                $getInstallationIDQuery = mysqli_query(Database::$connection, "SELECT inst_id FROM installations_tbl WHERE "
                        . "inst_installation_name = '" . addslashes($technicalRow['inst_installation_name']) . "' $imoString");

                $installationIDRow = mysqli_fetch_assoc($getInstallationIDQuery);
                $installationID = $installationIDRow['inst_id'];
            }
            
            
            $designerLookupID = "";
            $seriesLookupID = "";
            $versionLookupID = "NULL";
            $typeOfProductID = "";
            $TOProductDescription = "";
            
            
            if (isset($technicalRow['iet_engine_manufacturer']) && ($technicalRow['iet_engine_manufacturer'] != "")) {
                $designerResult = mysqli_query(Database::$connection, "SELECT * FROM designer_lookup_tbl WHERE "
                        . "dlt_designer_description = '" . addslashes(utf8_decode($technicalRow['iet_engine_manufacturer'])) . "'");
                $rowDesigner = mysqli_fetch_assoc($designerResult);
                $designerLookupID = $rowDesigner['dlt_id'];
            }
            if (isset($technicalRow['iet_series'])) {
                if ($technicalRow['iet_series'] == "") {
                    $technicalRow['iet_series'] = 'Unknown';
                }
                $addDesignerLookupSQL  = "";
                if ($designerLookupID != "") {
                    $addDesignerLookupSQL = " AND designer_lookup_tbl_dlt_id = '".$designerLookupID."'";
                }
                
                $modelResult = mysqli_query(Database::$connection, "SELECT * FROM series_lookup_tbl "
                        . "WHERE mlt_series = '" . addslashes(utf8_decode($technicalRow['iet_series'])) . "' "
                        . "$addDesignerLookupSQL LIMIT 1");
                $rowModel = mysqli_fetch_assoc($modelResult);
                $seriesLookupID = $rowModel['mlt_id'];
                $typeOfProductID = $rowModel['type_of_product_lookup_tbl_toplt_id'];

                if ($typeOfProductID == "") {
                    $typeOfProductID = 1;
                }
                $typeOfProductResult = mysqli_query(Database::$connection, "SELECT * FROM type_of_product_lookup_tbl WHERE toplt_id='$typeOfProductID'");
                $rowTOP = mysqli_fetch_assoc($typeOfProductResult);
                $TOProductDescription = $rowTOP['toplt_product_description'];
                $versionResult = mysqli_query(Database::$connection, "SELECT * FROM version_lookup_tbl "
                        . "WHERE model_lookup_tbl_mlt_id = '" . $seriesLookupID . "' ");
                $rowVersion = mysqli_fetch_assoc($versionResult);
                $versionLookupID = "'".$rowVersion['vlt_id']."'";
                
                if ($versionLookupID == "''") {
                    $versionLookupID = "NULL";
                }
            }
            
            
            
            $productID = "";
            // Create the product
            $productInsert = "INSERT INTO product_tbl (prod_serial_number, prod_description, "
                    . "prod_drawing_number, prod_comments, prod_enquiry_only_flag, "
                    . "customer_tbl_cust_id, installations_tbl_inst_id, version_lookup_tbl_vlt_id) "
                    . "VALUES ("
                    . "'" . (isset($technicalRow['iet_serial_number']) ? addslashes($technicalRow['iet_serial_number']) : "") . "', "
                    . "'$TOProductDescription', "
                    . "'', "
                    . "'" . (isset($technicalRow['iet_comment']) ? addslashes($technicalRow['iet_comment']) : "") . "', "
                    . "'0', "
                    . "'$customerID', "
                    . "'".(isset($installationID) ? $installationID : "" )."', "
                    . "$versionLookupID)";

            if (!mysqli_query(Database::$connection, $productInsert)) {
                debugWriter("loadInstallationDataLogs.txt", "Product insert failed " . $productInsert . " " . mysqli_error(Database::$connection));
            } else {
                $productCount++;
                $productID = mysqli_insert_id(Database::$connection);
            }
            
            
            if ($typeOfProductID == 1) { // 1 = engine
            
                // make cylinder count and configuration not have n/a in them. this screws stuff up
                if (isset($technicalRow['iet_engine_cylinder_count'])) {
                    if (strcasecmp($technicalRow['iet_engine_cylinder_count'], "n/a") == 0) {
                        $technicalRow['iet_engine_cylinder_count'] = "0";
                    }
                }

                if (isset($technicalRow['iet_engine_cylinder_configuration'])) {
                    if (strcasecmp($technicalRow['iet_engine_cylinder_configuration'], "n/a") == 0) {
                        $technicalRow['iet_engine_cylinder_configuration'] = "";
                    }
                }

                if (isset($technicalRow['iet_engine_model'])) {
                    if (strcasecmp($technicalRow['iet_engine_model'], "n/a") == 0) {
                        $technicalRow['iet_engine_model'] = "";
                    }
                }

            $doesEngItNeedAnAND = 0;

            $engCylinderCountString = "";
            $engCylinderConfigString = "";
            $engBoreSizeString = "";
            $engStrokeString = "";
            $engReleaseDateString = "";
            $engLoadPriorityString = "";
            $engCommentString = "";
            $engMainAux = "";

           


            if (isset($technicalRow['iet_engine_cylinder_count']) && ($technicalRow['iet_engine_cylinder_count'] != "")) {
                    $and = "";
                    if ($doesEngItNeedAnAND == 1) {
                        $and = " AND ";
                    }
                    $engCylinderCountString = $and . "iet_engine_cylinder_count = '" . addslashes($technicalRow['iet_engine_cylinder_count']) . "'";

                    $doesEngItNeedAnAND = 1;
                }

                if (isset($technicalRow['iet_engine_cylinder_configuration']) && ($technicalRow['iet_engine_cylinder_configuration'] != "")) {
                    $and = "";
                    if ($doesEngItNeedAnAND == 1) {
                        $and = " AND ";
                    }
                    $engCylinderConfigString = $and . "iet_engine_cylinder_configuration = '" . addslashes($technicalRow['iet_engine_cylinder_configuration']) . "'";

                    $doesEngItNeedAnAND = 1;
                }

                if (isset($technicalRow['iet_bore_size']) && ($technicalRow['iet_bore_size'] != "")) {
                    $and = "";
                    if ($doesEngItNeedAnAND == 1) {
                        $and = " AND ";
                    }
                    $engBoreSizeString = $and . "iet_bore_size = '" . addslashes($technicalRow['iet_bore_size']) . "'";

                    $doesEngItNeedAnAND = 1;
                }

                if (isset($technicalRow['iet_stroke']) && ($technicalRow['iet_stroke'] != "")) {
                    $and = "";
                    if ($doesEngItNeedAnAND == 1) {
                        $and = " AND ";
                    }
                    $engStrokeString = $and . "iet_stroke = '" . addslashes($technicalRow['iet_stroke']) . "' ";

                    $doesEngItNeedAnAND = 1;
                }

                if (isset($technicalRow['iet_release_date']) && ($technicalRow['iet_release_date'] != "")) {
                    $and = "";
                    if ($doesEngItNeedAnAND == 1) {
                        $and = " AND ";
                    }
                    $engReleaseDateString = $and . "iet_release_date = '" . addslashes($technicalRow['iet_release_date']) . "-01-01 00:00:00' ";

                    $doesEngItNeedAnAND = 1;
                }

                if (isset($technicalRow['iet_comment']) && ($technicalRow['iet_comment'] != "")) {
                    $and = "";
                    if ($doesEngItNeedAnAND == 1) {
                        $and = " AND ";
                    }
                    $engCommentString = $and . "iet_comment = '" . addslashes($technicalRow['iet_comment']) . "' ";

                    $doesEngItNeedAnAND = 1;
                }

                if (isset($technicalRow['load_priority_lookup_tbl_load_id']) && ($technicalRow['load_priority_lookup_tbl_load_id'] != "")) {
                    $and = "";
                    if ($doesEngItNeedAnAND == 1) {
                        $and = " AND ";
                    }
                    $engLoadPriorityString = $and . "load_priority_lookup_tbl_load_id = '" . addslashes($technicalRow['load_priority_lookup_tbl_load_id']) . "' ";

                    $doesEngItNeedAnAND = 1;
                }
                
                if (isset($technicalRow['iet_main_aux']) && ($technicalRow['iet_main_aux'] != "")) {
                    $and = "";
                    if ($doesEngItNeedAnAND == 1) {
                        $and = " AND ";
                    }
                    $engMainAux = $and . "iet_main_aux = '" . addslashes(ucwords($technicalRow['iet_main_aux'])) . "' ";

                    $doesEngItNeedAnAND = 1;
                }


           

             $engStringStart = "";
                $engStringFinish = "";
                if ($designerLookupID != ""  || $seriesLookupID != "" ||
                        $engCylinderCountString != "" || $engCylinderConfigString != "") {
                    $engStringStart = " (";
                    $engStringFinish = " )";
                }

            

            if (($designerLookupID != "") || ($seriesLookupID != "")  ||
                        ($engCylinderCountString != "") || ($engCylinderConfigString != "") ||
                        ($engBoreSizeString != "") || ($engStrokeString != "")) {
                    // check if the engine exists already.// Need to use engine count as well.
                    
                    $mainEngineCheck = "SELECT count(iet_id) FROM installation_engine_tbl "
                            . "WHERE product_tbl_prod_id='$productID'";

                    //debugWriter("debug.txt", $mainEngineCheck);

                    $engineCheckSQL = mysqli_query(Database::$connection, $mainEngineCheck);

                    $engineCheckRow = mysqli_fetch_assoc($engineCheckSQL);
                    $noofExistingEngines = $engineCheckRow['count(iet_id)'];

                    if (!isset($technicalRow['engine_count'])) {
                        $technicalRow['engine_count'] = 1;
                    }

                    // the number of main engines held is less than the number of engines the CSV expects
                    if ($noofExistingEngines < $technicalRow['engine_count']) {
                        // debugWriter("debug.txt", "Marine Engine Check ". $mainEngineCheck . " | " . $noofExistingEngines . " : " . $technicalRow['engine_count']);
                        for ($i = $noofExistingEngines + 1; $i <= $technicalRow['engine_count']; $i++) {

                            if ((isset($technicalRow['iet_engine_manufacturer'])) && $technicalRow['iet_engine_manufacturer'] != "") {
                                $engine = "INSERT INTO installation_engine_tbl (iet_model_description, "
                                        . "iet_main_aux, iet_engine_verified, "
                                        . "iet_output, iet_output_unit_of_measure, "
                                        . "iet_fuel_type, iet_unit_name, "
                                        . "iet_bore_size, iet_stroke, iet_release_date, iet_enquiry_only, "
                                        . " load_priority_lookup_tbl_load_id, product_tbl_prod_id) "
                                        . "VALUES ("
                                        . "'".(isset($technicalRow['iet_engine_model']) ? addslashes($technicalRow['iet_engine_model']) : "")."', "
                                       . "'" . (isset($technicalRow['iet_main_aux']) ? addslashes(ucwords($technicalRow['iet_main_aux'])) : "Main") . "', "
                                        . "'0', "
                                        . "'" . (isset($technicalRow['iet_output']) ? addslashes($technicalRow['iet_output']) : "") . "', "
                                        . "'MW', "
                                        . "'" . (isset($technicalRow['iet_fuel_type']) ? addslashes($technicalRow['iet_fuel_type']) : "") . "', "
                                        . "'" . (isset($technicalRow['iet_unit_name']) ? addslashes($technicalRow['iet_unit_name']) : "") . "', "
                                        . "'" . (isset($technicalRow['iet_bore_size']) ? addslashes($technicalRow['iet_bore_size']) : "") . "', "
                                        . "'" . (isset($technicalRow['iet_stroke']) ? addslashes($technicalRow['iet_stroke']) : "") . "', "
                                        . "'" . (isset($technicalRow['iet_release_date']) ? addslashes($technicalRow['iet_release_date'] . "-01-01 00:00:00") : "") . "', "
                                        . "'0', "
                                        . "'3', " // " . (isset($technicalRow['load_priority_lookup_tbl_load_id']) ? addslashes($technicalRow['load_priority_lookup_tbl_load_id']) : "3") . "
                                        . "'$productID')";

                                if (!mysqli_query(Database::$connection, $engine)) {
                                    debugWriter("loadInstallationDataLogs.txt", "Engine insert failed " . $engine . " " . mysqli_error(Database::$connection));
                                } else {
                                    $engineCount++;
                                }
                            }
                        }
                    }
                }
            }
        }

        print "<p>Added " . $productCount . " Products</p>";
        print "<p>Added " . $engineCount . " Engines</p>";
    }

}
