<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/SupplierApproval.php");
include_once 'Models/SQLConstructors/SupplierApprovalSQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of SupplierApprovalModel
 *
 * @author Archie
 */
class SupplierApprovalModel {
    
    /**
     * getSupplierApprovalBySupplierID
     * @param type $supplierID
     * @return type
     */
    public function getSupplierApprovalBySupplierID($supplierID) {
        $supplierApprovalSQLConstructor = new SupplierApprovalSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM supplier_approvals_tbl WHERE customer_tbl_cust_id='$supplierID'");
        $row = mysqli_fetch_assoc($result);
        
        $part = $supplierApprovalSQLConstructor->createSupplierApproval($row);
        
        return $part;
    }
}
