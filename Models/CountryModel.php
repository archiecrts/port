<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once("Models/Entities/Country.php");
include_once("Models/SQLConstructors/CountrySQLConstructor.php");
////include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of CountryModel
 *
 * @author Archie
 */
class CountryModel {
    public function getCountries() 
    {
        
        $limitedAccount = $_SESSION['limited'];
        $applyLimit = "";
        //execute the SQL query and return records
        if ($limitedAccount == 1) {
            $applyLimit = "WHERE c_name IN (SELECT ut_territory_name FROM user_territory_tbl WHERE user_tbl_usr_id = '".$_SESSION['user_id']."')";
        }
        $countrySQLConstructor = new CountrySQLConstructor();
        
        //execute the SQL query and return records
            $result = mysqli_query(Database::$connection, "SELECT * FROM country_tbl $applyLimit ORDER BY c_name ASC");
           
            //fetch tha data from the database
            while ($row = mysqli_fetch_assoc($result)) {  
                
                $array[$row['c_name']] = $countrySQLConstructor->createCountry($row);
            }
            
            return $array;
    }
    
    /**
     * 
     * @return type
     */
    public function getDistinctAreas() 
    {
        
        $limitedAccount = $_SESSION['limited'];
        $applyLimit = "";
        //execute the SQL query and return records
        if ($limitedAccount == 1) {
            $applyLimit = "WHERE c_name IN (SELECT ut_territory_name FROM user_territory_tbl WHERE user_tbl_usr_id = '".$_SESSION['user_id']."')";
        }
        //execute the SQL query and return records
            $result = mysqli_query(Database::$connection, "SELECT DISTINCT(c_area) FROM country_tbl $applyLimit ORDER BY c_area ASC");
           
            //fetch tha data from the database
            while ($row = mysqli_fetch_assoc($result)) {             
                $array[$row['c_area']] = $row['c_area'];
            }
            
            return $array;
    }
    
    
    /**
     * getDistinctContinents
     * @return type
     */
    public function getDistinctContinents() 
    {
        
        $limitedAccount = $_SESSION['limited'];
        $applyLimit = "";
        //execute the SQL query and return records
        if ($limitedAccount == 1) {
            $applyLimit = "WHERE c_name IN (SELECT ut_territory_name FROM user_territory_tbl WHERE user_tbl_usr_id = '".$_SESSION['user_id']."')";
        }
        
        
            $result = mysqli_query(Database::$connection, "SELECT DISTINCT(c_continent) FROM country_tbl $applyLimit ORDER BY c_continent ASC");
           
            //fetch tha data from the database
            while ($row = mysqli_fetch_assoc($result)) {             
                $array[$row['c_continent']] = $row['c_continent'];
            }
            
            return $array;
    }
    
    /**
     * 
     * @param type $fileName
     * @return boolean
     */
    public function setAllCountries($fileName) {
        
        $insert_query = "LOAD DATA LOCAL INFILE '".$fileName."' 
                INTO TABLE country_tbl  
                COLUMNS TERMINATED BY ','
                ENCLOSED BY '\"'
                LINES TERMINATED BY '\r\n' 
                (c_name, c_timezone_from_gmt)";
        
        if (!mysqli_query(Database::$connection, $insert_query)) {
            return null;
        } else {
            
            return true;   
        }
    }
    

    
    public function getTimeZoneByCountryName($countryName) {
        $countrySQLConstructor = new CountrySQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM country_tbl WHERE c_name='".$countryName."'");
           
        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);   
        
        $country = $countrySQLConstructor->createCountry($row);
        
        return $country;
    }
}
