<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/PartManifest.php");
include_once("Models/SQLConstructors/PartManifestSQLConstructor.php");
//include_once ("Models/DB.php");
include_once("Models/Database.php");

/**
 * Description of PartManifestModel
 *
 * @author Archie
 */
class PartManifestModel {

    /**
     * getMainfestByPartID
     * @param type $partID
     */
    public function getMainfestByPartID($partID) {
        $partManifestSQLConstructor = new PartManifestSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM product_parts_manifest_tbl WHERE part_code_tbl_part_id = '$partID'");

        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['epm_id']] = $partManifestSQLConstructor->createPartManifest($row);
        }

        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }

    /**
     * getManifestByVersionID
     * @param INT $versionID
     * @return type
     */
    public function getManifestByVersionID($versionID) {
        $partManifestSQLConstructor = new PartManifestSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM product_parts_manifest_tbl WHERE version_lookup_tbl_vlt_id = '$versionID'");

        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['epm_id']] = $partManifestSQLConstructor->createPartManifest($row);
        }

        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }

    /**
     * insertIntoManifestBySeriesID
     * @param type $seriesID
     * @param type $partCodeID
     * @param type $quantity
     * @return boolean
     */
    public function insertIntoManifestBySeriesID($seriesID, $partCodeID, $quantity) {
        $select = mysqli_query(Database::$connection, "SELECT * FROM version_lookup_tbl WHERE model_lookup_tbl_mlt_id = '$seriesID'");
        while ($row = mysqli_fetch_assoc($select)) {
            //debugWriter("debug.txt", $row['vlt_id']);
            $quantityChecked = '';
            if ($quantity == '') {
                $quantityChecked = $row['vlt_cylinder_count'];
            } else {
                $quantityChecked = $quantity;
            }
            $insert = "INSERT INTO product_parts_manifest_tbl (part_code_tbl_part_id, "
                    . "epm_current_oe_number, epm_alt_number, epm_quantity_on_engine, "
                    . "epm_archive_part, version_lookup_tbl_vlt_id, epm_manual) VALUES ("
                    . "'$partCodeID', "
                    . "'0', "
                    . "'0', "
                    . "'$quantityChecked', "
                    . "'0', "
                    . "'" . $row['vlt_id'] . "', '' )";
            if (!mysqli_query(Database::$connection, $insert)) {
                debugWriter("debug.txt", "insertIntoManifestBySeriesID ERROR " . mysqli_error(Database::$connection));
                return false;
            }
        }
        return true;
    }

    /**
     * insertIntoManifestByVersionID
     * @param type $versionID
     * @param type $partCodeID
     * @param type $quantity
     * @return boolean
     */
    public function insertIntoManifestByVersionID($seriesID, $versionID, $partCodeID, $quantity) {
        $select = mysqli_query(Database::$connection, "SELECT * FROM version_lookup_tbl WHERE model_lookup_tbl_mlt_id = '$seriesID'");
        while ($row = mysqli_fetch_assoc($select)) {
            // debugWriter("debug.txt", $row['vlt_id']);
            $quantityChecked = '0';
            if ($row['vlt_id'] == $versionID) {
                if ($quantity == '') {
                    $quantityChecked = $row['vlt_cylinder_count'];
                } else {
                    $quantityChecked = $quantity;
                }
            }
            $insert = "INSERT INTO product_parts_manifest_tbl (part_code_tbl_part_id, "
                    . "epm_current_oe_number, epm_alt_number, epm_quantity_on_engine, "
                    . "epm_archive_part, version_lookup_tbl_vlt_id, epm_manual) VALUES ("
                    . "'$partCodeID', "
                    . "'0', "
                    . "'0', "
                    . "'$quantityChecked', "
                    . "'0', "
                    . "'" . $row['vlt_id'] . "', '' )";
            if (!mysqli_query(Database::$connection, $insert)) {
                debugWriter("debug.txt", "insertIntoManifestByVersionID ERROR " . mysqli_error(Database::$connection));
                return false;
            }
        }
        return true;
    }

    /**
     * getMainfestByPartIDAndSeriesID
     * @param type $partID
     * @param type $seriesID
     * @return type
     */
    public function getMainfestByPartIDAndSeriesID($partID, $seriesID) {
        $select = mysqli_query(Database::$connection, "SELECT * FROM version_lookup_tbl WHERE model_lookup_tbl_mlt_id = '$seriesID'");
        while ($row = mysqli_fetch_assoc($select)) {
            $partManifestSQLConstructor = new PartManifestSQLConstructor();
            $result = mysqli_query(Database::$connection, "SELECT * FROM product_parts_manifest_tbl WHERE part_code_tbl_part_id = '$partID' "
                    . "AND version_lookup_tbl_vlt_id = '" . $row['vlt_id'] . "'");

            while ($row = mysqli_fetch_assoc($result)) {
                $array[$partID][$row['epm_id']] = $partManifestSQLConstructor->createPartManifest($row);
            }
        }
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }

    
    /**
     * matchManifestEntry
     * @param type $partID
     * @param type $versionID
     * @param type $quantity
     * @return type
     */
    public function matchManifestEntry($partID, $versionID, $quantity) {

        $result = mysqli_query(Database::$connection, "SELECT count(*) FROM product_parts_manifest_tbl WHERE part_code_tbl_part_id = '$partID' "
                . "AND version_lookup_tbl_vlt_id = '" . $versionID . "' AND epm_quantity_on_engine = '$quantity'");

        $row = mysqli_fetch_assoc($result);

        return $row['count(*)'];
    }
    
    /**
     * insertManifestObject
     * @param type $manifest
     * @return boolean
     */
    public function insertManifestObject($manifest) {
        
        
        $insert = "INSERT INTO product_parts_manifest_tbl (part_code_tbl_part_id, "
                    . "epm_current_oe_number, epm_alt_number, epm_quantity_on_engine, "
                    . "epm_archive_part, version_lookup_tbl_vlt_id, epm_manual) VALUES ("
                    . "'$manifest->partCode', "
                    . "'$manifest->oeNumber', "
                    . "'$manifest->altNumber', "
                    . "'$manifest->quantityOnEngine', "
                    . "'$manifest->archivedPart', "
                    . "'$manifest->versionLookupID', "
                    . "'$manifest->manualURL')";
            if (!mysqli_query(Database::$connection, $insert)) {
                debugWriter("debug.txt", "insertIntoManifestByVersionID ERROR " . mysqli_error(Database::$connection));
                return false;
            } else {
                return mysqli_insert_id(Database::$connection);
            }
            
    }

}
