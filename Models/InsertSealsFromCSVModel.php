<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once 'Models/Entities/Product.php';
include_once 'Models/ProductModel.php';

/**
 * Description of InsertSealsIntoDB
 *
 * @author Archie
 */
class InsertSealsFromCSVModel {

    /**
     * createTemporaryTable
     */
    private function createTemporaryTable() {

        /*
         * Create a temporary table to contain the CSV data in the same order as the CSV.
         */
        $create_table_query = "CREATE TEMPORARY TABLE temporary_seals_table (temp_id SERIAL, "
                . "imo VARCHAR(255), ship_name VARCHAR(255), make_model VARCHAR(255), "
                . "drawing_number VARCHAR(255), serial_number VARCHAR(255), "
                . "comments LONGTEXT, PRIMARY KEY(temp_id))"; // set the primary key of the table


        /*
         * mysqli_query(Database::$connection, ) runs a sql query, putting ! in front of it says "if the query fails"
         */
        if (!mysqli_query(Database::$connection, $create_table_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't create table : " . $create_table_query . " \r\n" . mysqli_error(Database::$connection) . "");
        }
    }

    /**
     * loadFile
     * @param type $fileName
     */
    private function loadFile($fileName) {
        // Add the CSV data to the temp table.
        // removes the last character from the string (its a comma that shouldnt be there)
        // Load the data from the csv into the temporary table, using the column names generated on the verify page.
        $insert_temp_query = "LOAD DATA LOCAL INFILE '" . $fileName . "'
                INTO TABLE temporary_seals_table
                CHARACTER SET 'latin1'
                COLUMNS TERMINATED BY ','
                ENCLOSED BY '\"' LINES TERMINATED BY '\r\n'
                IGNORE 1 LINES  (imo, ship_name, make_model, "
                . "drawing_number, serial_number, "
                . "comments)";


        // DATABASE query generator. This say, if mysqli_query fails then write the first line. otherwise write the second.
        if (!mysqli_query(Database::$connection, $insert_temp_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't insert records into temporary table : " . $new_query_string . "*** " . mysqli_error(Database::$connection));
        }
    }

    /**
     * cleanTempData
     * @param type $fileName
     * @param type $columnNameArray
     * @param type $headers
     * @param type $type
     * @param type $dbColumnName
     * @param type $newColumnNameArray
     */
    private function cleanTempData() {
        // check each record for matching IMO numbers because serial numbers havent been shared where necessary.
        $select = mysqli_query(Database::$connection, "SELECT imo, serial_number FROM temporary_seals_table WHERE "
                . "serial_number != ''");
        while ($search = mysqli_fetch_assoc($select)) {
            $update = "UPDATE temporary_seals_table SET serial_number = '" . addslashes($search['serial_number']) . "' "
                    . "WHERE imo = '" . $search['imo'] . "'";

            if (!mysqli_query(Database::$connection, $update)) {
                debugWriter("debug.txt", "update Serial Numbers: " . mysqli_error(Database::$connection) . "\r\n");
            }
        }

        // Do a quick count of the new records, This is currently used for debug, but may stay in the code.
        /* $quickCount = mysqli_query(Database::$connection, "SELECT count(*) FROM temporary_seals_table");
          $countQuick = mysqli_fetch_assoc($quickCount);
          print "<p>Temp table Count: " . $countQuick['count(*)'] . ". Before duplicates are removed</p>"; */
    }

    /**
     * findMatchingShipsForSeals
     */
    private function findMatchingShipsForSeals() {

        $productModel = new ProductModel();

        $select = mysqli_query(Database::$connection, "SELECT * FROM temporary_seals_table");
        while ($search = mysqli_fetch_assoc($select)) {
            $installationID = "";
            $currentInstName = $search['ship_name']; // might be different from seals ship name.
            $drawingNumber = $search['drawing_number'];
            $serialNumber = $search['serial_number'];
            $comment = $search['comments'];
            $customerID = "";

            // find the matching installation first. If one doesnt exist then do something else.
            $result = mysqli_query(Database::$connection, "SELECT * FROM installations_tbl "
                    . "WHERE inst_imo = '" . $search['imo'] . "'");
            while ($ship = mysqli_fetch_assoc($result)) {
                $installationID = $ship['inst_id'];
                $currentInstName = $ship['inst_installation_name'];

                $customerResult = mysqli_query(Database::$connection, "SELECT customer_tbl_cust_id FROM "
                        . "customer_tbl_has_installations_tbl "
                        . "WHERE installations_tbl_inst_id = '" . $installationID . "'");
                $customer = mysqli_fetch_assoc($customerResult);
                $customerID = $customer['customer_tbl_cust_id'];
            }

            $unitName = $currentInstName . " SE";

            if ($customerID == "") {
                $customerResult = mysqli_query(Database::$connection, "SELECT cust_id FROM "
                        . "customer_tbl WHERE cust_name = 'UNKNOWN'");
                $customer = mysqli_fetch_assoc($customerResult);
                $customerID = $customer['cust_id'];
            }
            
            $addressResult = mysqli_query(Database::$connection, "SELECT cadt_id FROM "
                        . "customer_address_tbl "
                        . "WHERE customer_tbl_cust_id = '" . $customerID . "'");
            $address = mysqli_fetch_assoc($addressResult);
            $customerAddressID = $address['cadt_id'];

            if ($installationID == "") {
                $insertInstallation = "INSERT INTO installations_tbl (inst_type, inst_installation_name, 
                    inst_imo, inst_original_source, inst_original_source_date, 
                    installation_status_tbl_stat_id, inst_flag_name, customer_address_tbl_cadt_id) 
                    VALUES ('MARINE', '$currentInstName', '" . $search['imo'] . "', "
                        . "'SalesForce', NOW(), '5', 'Unknown', '$customerAddressID')";

                if (!mysqli_query(Database::$connection, $insertInstallation)) {
                    debugWriter("loadInstallationDataLogs.txt", "Can't insert Installation : " . $insertInstallation . "*** " . mysqli_error(Database::$connection));

                    $installationID = "";
                } else {
                    $installationID = mysqli_insert_id(Database::$connection);
                    // insert into customer has installations table.

                    $insertCustInst = "INSERT INTO customer_tbl_has_installations_tbl "
                            . "(customer_tbl_cust_id, installations_tbl_inst_id) VALUES "
                            . "('$customerID', '$installationID')";

                    if (!mysqli_query(Database::$connection, $insertCustInst)) {
                        debugWriter("loadInstallationDataLogs.txt", "Can't insert cust has installations (seals) : " . $insertCustInst . "*** " . mysqli_error(Database::$connection));
                    }
                }
            }


            // get version ID
            $makeModel = explode("-", $search['make_model']);
            $designer = trim($makeModel[0]);
            if (isset($makeModel[1])) {
                $model = trim($makeModel[1]);
            } else {
                $model = 'UNKNOWN';
            }



            $versionResult = mysqli_query(Database::$connection, "SELECT vlt_id FROM version_lookup_tbl AS v 
            LEFT JOIN series_lookup_tbl AS s ON s.mlt_id = v.model_lookup_tbl_mlt_id
            LEFT JOIN designer_lookup_tbl AS d ON d.dlt_id = s.designer_lookup_tbl_dlt_id
            WHERE d.dlt_designer_description = '" . addslashes($designer) . "' 
            AND s.type_of_product_lookup_tbl_toplt_id = '2' 
            AND s.mlt_series = '" . addslashes($model) . "'");


            $rowVersion = mysqli_fetch_assoc($versionResult);

            if (isset($rowVersion['vlt_id'])) {
                $versionLookupID = $rowVersion['vlt_id'];
            } else {
                $versionLookupID = "";
                // first check the designer exists. or create it
                $designerCheck = mysqli_query(Database::$connection, "SELECT dlt_id FROM designer_lookup_tbl AS d 
                    WHERE d.dlt_designer_description = '" . addslashes($designer) . "'");
                $rowDesignerCheck = mysqli_fetch_assoc($designerCheck);
                if (!isset($rowDesignerCheck['dlt_id'])) {
                    // create the record

                    $insertDesigner = "INSERT INTO designer_lookup_tbl "
                            . "(dlt_designer_description, dlt_stc_league_engine) VALUES "
                            . "('" . addslashes($designer) . "', '0')";

                    if (!mysqli_query(Database::$connection, $insertDesigner)) {
                        debugWriter("loadInstallationDataLogs.txt", "Can't insert designer : " . $insertDesigner . "*** " . mysqli_error(Database::$connection));

                        $designerID = "";
                    } else {
                        $designerID = mysqli_insert_id(Database::$connection);
                    }
                } else {
                    $designerID = $rowDesignerCheck['dlt_id'];
                }


                // next check series and create version
                $seriesCheck = mysqli_query(Database::$connection, "SELECT mlt_id FROM series_lookup_tbl AS s 
                    WHERE s.mlt_series = '" . addslashes($model) . "' 
                    AND s.designer_lookup_tbl_dlt_id = '$designerID' AND s.type_of_product_lookup_tbl_toplt_id = '2'");
                $rowSeriesCheck = mysqli_fetch_assoc($seriesCheck);

                if (!isset($rowSeriesCheck['mlt_id']) && ($designerID != "")) {
                    // create the record
                    $insertSeries = "INSERT INTO series_lookup_tbl "
                            . "(mlt_series, designer_lookup_tbl_dlt_id, "
                            . "type_of_product_lookup_tbl_toplt_id, mlt_stc_league_engine) VALUES "
                            . "('" . addslashes($model) . "', '$designerID', '2', '0')";

                    if (!mysqli_query(Database::$connection, $insertSeries)) {
                        debugWriter("loadInstallationDataLogs.txt", "Can't insert series : " . $insertSeries . "*** " . mysqli_error(Database::$connection));
                        $seriesID = "";
                    } else {
                        $seriesID = mysqli_insert_id(Database::$connection);
                    }
                } else {
                    $seriesID = $rowSeriesCheck['mlt_id'];
                }

                if ($seriesID != "") {
                    // add a version in.
                    $insertVersion = "INSERT INTO version_lookup_tbl "
                            . "(vlt_version_description, model_lookup_tbl_mlt_id, "
                            . "vlt_cylinder_count, vlt_cylinder_configuration) VALUES "
                            . "('N/A', '$seriesID', '0', 'A')";

                    if (!mysqli_query(Database::$connection, $insertVersion)) {
                        debugWriter("loadInstallationDataLogs.txt", "Can't insert version : " . $insertVersion . "*** " . mysqli_error(Database::$connection));
                        $versionLookupID = "";
                    } else {
                        $versionLookupID = mysqli_insert_id(Database::$connection);
                    }
                }
            }

            if (strpos($serialNumber, ",") !== false) {
                // "TWO PROPS!";
                $expSN = explode(",", $serialNumber);
                foreach ($expSN as $sn) {
                    $product = new Product("", addslashes($sn), "SEAL", addslashes($drawingNumber), addslashes($comment), 0, $customerID, $installationID, $versionLookupID, $unitName, "");
                    $productModel->setNewProduct($product);
                }
            } else {
                $product = new Product("", addslashes($serialNumber), "SEAL", addslashes($drawingNumber), addslashes($comment), 0, $customerID, $installationID, $versionLookupID, $unitName, "");
                $productModel->setNewProduct($product);
            }
        }
        print "Seals imported <br/>";
    }

    /**
     * $fileName
     * @param type $fileName
     */
    public function insertDataFromCSV($fileName) {

        /**
         * Make a temporary table.
         */
        $this->createTemporaryTable();
        $this->loadFile($fileName);
        $this->cleanTempData();
        $this->findMatchingShipsForSeals();


        return "hello";
    }

}
