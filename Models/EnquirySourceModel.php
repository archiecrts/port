<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/EnquirySource.php");
include_once 'Models/SQLConstructors/EnquirySourceSQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of EnquirySourceModel
 *
 * @author alexandra
 */
class EnquirySourceModel {
    
    /**
     * getAllEnquirySources
     * @return Array | null
     */
    public function getAllEnquirySources() {
        $enquirySourceSQLConstructor = new EnquirySourceSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM enquiry_source_tbl ORDER BY est_source ASC");

        //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)){
           $array[$row['est_id']]=$enquirySourceSQLConstructor->createEnquirySource($row);
        }

        if (isset($array)){
            return $array;
        } else {
            return null;
        }
     }
    /**
     * getEnquirySourceByID
     * @param INT $sourceID
     * @return ENTITY
     */
     public function getEnquirySourceByID($sourceID) {
        $enquirySourceSQLConstructor = new EnquirySourceSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM enquiry_source_tbl WHERE est_id='".$sourceID."'");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);
        $enquirySource = $enquirySourceSQLConstructor->createEnquirySource($row);
      

        
        return $enquirySource;
        
     }
}