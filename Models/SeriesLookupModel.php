<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once ("Models/Entities/SeriesLookup.php");
include_once ("Models/SQLConstructors/SeriesLookupSQLConstructor.php");
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of SeriesLookupModel
 *
 * @author Archie
 */
class SeriesLookupModel {
    
    /**
     * getSeriesByVersionLookupID
     * @param type $versionLookupID
     * @return type
     */
    public function getSeriesByVersionLookupID($versionLookupID) {
        $seriesLookupSQLConstructor = new SeriesLookupSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM series_lookup_tbl WHERE mlt_id = "
                . "(SELECT model_lookup_tbl_mlt_id FROM version_lookup_tbl WHERE vlt_id = '".$versionLookupID."')");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);
            
         return $seriesLookupSQLConstructor->createSeriesLookup($row);
       
    }
    
    /**
     * getSeriesByID
     * @param type $seriesID
     * @return type
     */
    public function getSeriesByID($seriesID) {
        $seriesLookupSQLConstructor = new SeriesLookupSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM series_lookup_tbl WHERE mlt_id = '".$seriesID."'");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);
            
        return $seriesLookupSQLConstructor->createSeriesLookup($row);
       
    }
    
    /**
     * getDistinctModelSeriesByDesigner
     * @param INT $versionLookupID
     * @param INT $designerID
     * @return Series Object
     */
    public function getDistinctModelSeriesByDesigner($designerID) {
        $seriesLookupSQLConstructor = new SeriesLookupSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM series_lookup_tbl WHERE "
                . " designer_lookup_tbl_dlt_id = '".$designerID."' GROUP BY mlt_series");

        //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['mlt_id']] = $seriesLookupSQLConstructor->createSeriesLookup($row);
        }
            
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
       
    }
    
    /**
     * getDistinctSeriesByCustomerID
     * @param type $customerID
     * @return type
     */
    public function getDistinctSeriesByCustomerID($customerID) {
        $seriesLookupSQLConstructor = new SeriesLookupSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM series_lookup_tbl AS s 
            LEFT JOIN version_lookup_tbl AS v ON s.mlt_id = v.model_lookup_tbl_mlt_id 
            LEFT JOIN product_parts_manifest_tbl AS m ON m.version_lookup_tbl_vlt_id = v.vlt_id 
            LEFT JOIN suppliers_parts_tbl AS p ON m.part_code_tbl_part_id = p.part_code_tbl_part_id 
            WHERE p.customer_tbl_cust_id = '$customerID' 
            GROUP BY s.mlt_id");

        //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['mlt_id']] = $seriesLookupSQLConstructor->createSeriesLookup($row);
        }
            
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    
    /**
     * getDistinctModelSeries
     * @return type
     */
    public function getDistinctModelSeries() {
        $seriesLookupSQLConstructor = new SeriesLookupSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM series_lookup_tbl GROUP BY mlt_series");
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['mlt_id']] = $seriesLookupSQLConstructor->createSeriesLookup($row);
        }
        
        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
        
    }
}
