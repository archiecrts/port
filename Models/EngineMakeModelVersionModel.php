<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/EngineMakeModelVersion.php");
include_once 'Models/SQLConstructors/EngineMakeModelVersionSQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of EngineMakeModelVersionModel
 *
 * @author Archie
 */
class EngineMakeModelVersionModel {
    
    
    public function getAllEngines() {
        $engineMakeModelVersionSQLConstructor = new EngineMakeModelVersionSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT eb_engine_builder_name, em_model_description, mv_description, mv_id "
                    . " FROM engine_builder_tbl, engine_model_tbl, engine_model_version_tbl "
                    . "WHERE eb_id=engine_builder_tbl_eb_id AND em_id=engine_model_tbl_em_id");
            
            
        //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['mv_id']] = $engineMakeModelVersionSQLConstructor->createEngineMakeModelVersion($row);
        }
        
        if(isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    
    /**
     * 
     * @param type $engineVersionID
     * @return type
     */
    public function getCountOfAssociatedServiceIntervals($engineVersionID) {
        $result = mysqli_query(Database::$connection, "SELECT count(epm_id) FROM engine_parts_manifest_tbl "
                . "WHERE engine_model_version_tbl_mv_id='".$engineVersionID."'");

        $countManifestRow = mysqli_fetch_assoc($result);
        return $countManifestRow['count(epm_id)'];
    }
    
    /**
     * 
     * @param type $versionID
     * @return type
     */
    public function getEngineByVersionID($versionID) {
        $result = mysqli_query(Database::$connection, "SELECT eb_id, eb_engine_builder_name, em_id, em_model_description, mv_description"
                    . " FROM engine_builder_tbl, engine_model_tbl, engine_model_version_tbl "
                    . "WHERE eb_id=engine_builder_tbl_eb_id AND em_id=engine_model_tbl_em_id "
                    . "AND mv_id=".$versionID."");
            
            
                
        $row = mysqli_fetch_assoc($result);
        // TODO: can this be made to use the constructor?
        $engine = ["makeID" => $row['eb_id'],
                    "make" => $row['eb_engine_builder_name'],
                    "modelID" => $row['em_id'],
                    "model" => $row['em_model_description'],
                    "version" => $row['mv_description']];
                
        return $engine;
    }
}
