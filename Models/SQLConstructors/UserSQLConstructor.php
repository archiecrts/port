<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of UserSQLConstructor
 *
 * @author Archie
 */
class UserSQLConstructor {
    
    
    public function createUser($row) {
        
        return new User(
                $row['usr_id'], 
                $row['usr_login'], 
                $row['usr_password'], 
                $row['usr_name'], 
                $row['user_level_tbl_level_id'], 
                $row['user_department_tbl_dept_id'], 
                $row['usr_email'], 
                $row['usr_initial_login'],
                $row['usr_retire_user_flag'],
                $row['usr_limit_to_territory']);
    }
}
