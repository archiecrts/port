<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of CountrySQLConstructor
 *
 * @author Archie
 */
class CountrySQLConstructor {
    
    /**
     * Creates a standard country object.
     * @param Array $row
     * @return \Country
     */
    public function createCountry($row) {
        
        return new Country(
                    $row['c_name'], 
                    $row['c_area'], 
                    $row['c_continent'], 
                    $row['c_timezone_from_gmt']);
    }
}
