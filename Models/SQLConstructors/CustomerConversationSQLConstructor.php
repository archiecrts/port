<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of CustomerConversationSQLConstructor
 *
 * @author Archie
 */
class CustomerConversationSQLConstructor {
    
    /**
     * Constructor for customer conversation
     * @param Array $row
     * @return \CustomerConversation Object
     */
    public function createCustomerConversation($row) {
        
        return new CustomerConversation(
                    $row['con_id'],
                    $row['customer_contacts_tbl_ict_id'],
                    $row['con_date'], 
                    $row['con_content'], 
                    $row['con_next_action_due_date'], 
                    $row['con_action_type'], 
                    $row['campaign_overview_tbl_cot_id'],
                    $row['con_reply_to_con_id']);
                    
    }
    
}
