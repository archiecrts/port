<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of ProductSummarySQLConstructor
 *
 * @author Archie
 */
class ProductSummarySQLConstructor {
    
    public function createProductSummary($row) {
        
        return new ProductSummary(
                    $row['COUNT(p.prod_id)'],
                    $row['version_lookup_tbl_vlt_id'],
                    $row['vlt_version_description'],
                    $row['SUM(v.vlt_cylinder_count)'],
                    $row['model_lookup_tbl_mlt_id'],
                    $row['iet_model_description']);

    }
}
