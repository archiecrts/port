<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of UserHistorySQLConstructor
 *
 * @author Archie
 */
class UserHistorySQLConstructor {
    
    public function createUserHistory($row) {
        
        return new UserHistory(
                    $row['uht_id'],
                    $row['uht_item_date'], 
                    $row['uht_item_type'], 
                    $row['uht_comment'], 
                    $row['user_tbl_usr_id']);
    }
}
