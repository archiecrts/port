<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once 'Models/Entities/InstallationStatus.php';
/**
 * Description of InstallationStatusSQLConstructor
 *
 * @author Archie
 */
class InstallationStatusSQLConstructor {
   
    /**
     * createInstallationStatus
     * @param type $row
     * @return \InstallationStatus
     */
    public function createInstallationStatus($row) {
        
        return new InstallationStatus(
                    $row['stat_id'],  
                    $row['stat_status_description'], 
                    $row['stat_type']);
    }
}
