<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
/**
 * Description of CustomerHasInstallationsSQLConstructor
 *
 * @author Archie
 */
class CustomerHasInstallationsSQLConstructor {
    
    public function createCustomerHasInstallations($row) {
        
        return new CustomerHasInstallations(
                    $row['customer_tbl_cust_id'],
                    $row['installations_tbl_inst_id']);
    }
}
