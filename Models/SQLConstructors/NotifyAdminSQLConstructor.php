<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of NotifyAdminSQLConstructor
 *
 * @author Archie
 */
class NotifyAdminSQLConstructor {
    
    /**
     * 
     * @param type $row
     * @return \NotifyAdmin
     */
    public function createNotifyAdmin($row) {
        
        return new NotifyAdmin(
                    $row['ait_id'], 
                    $row['user_tbl_usr_id'],
                    $row['ait_date_logged'], 
                    $row['ait_issue_description'], 
                    $row['ait_status'], 
                    $row['issue_types_tbl_itt_id']);
    }
}
