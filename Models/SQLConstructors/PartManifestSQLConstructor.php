<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of PartManifestSQLConstructor
 *
 * @author Archie
 */
class PartManifestSQLConstructor {
    
    /**
     * createPartManifest
     * @param type $row
     * @return \PartManifest
     */
    public function createPartManifest($row) {
        
        return new PartManifest(
                    $row['epm_id'], 
                    $row['part_code_tbl_part_id'], 
                    $row['epm_current_oe_number'], 
                    $row['epm_alt_number'],
                    $row['epm_quantity_on_engine'],
                    $row['epm_archive_part'],
                    $row['version_lookup_tbl_vlt_id'],
                    $row['epm_manual']);
    }
}
