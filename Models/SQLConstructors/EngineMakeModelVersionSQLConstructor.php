<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of EngineMakeModelVersionSQLConstructor
 *
 * @author Archie
 */
class EngineMakeModelVersionSQLConstructor {
    
    
    public function createEngineMakeModelVersion($row) {
        
        new EngineMakeModelVersion(
                             $row['eb_engine_builder_name'], 
                             $row['em_model_description'], 
                             $row['mv_description'], 
                             $row['mv_id']);
    }
    
}
