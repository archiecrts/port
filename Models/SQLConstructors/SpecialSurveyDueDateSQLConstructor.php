<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of SpecialSurveyDueDateSQLConstructor
 *
 * @author Archie
 */
class SpecialSurveyDueDateSQLConstructor {
    
    /**
     * createSpecialSurveyDueDate
     * @param type $row
     * @return \SpecialSurveyDue
     */
    public function createSpecialSurveyDueDate($row) {
        
        return new SpecialSurveyDue(
                $row['sddt_id'], 
                $row['sddt_special_survey_date'], 
                $row['installations_tbl_inst_id'], 
                $row['sddt_lrno']);
        
        
    }
    
}
