<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of ShipNameHistorySQLConstructor
 *
 * @author Archie
 */
class ShipNameHistorySQLConstructor {
    
    
    public function createShipNameHistory($row) {
        
        return new ShipNameHistory(
                    $row['snht_id'],  
                    $row['snht_lrno'], 
                    $row['snht_sequence'],
                    $row['snht_vessel_name'],
                    $row['snht_effective_date'],
                    $row['installations_tbl_inst_id']);
    }
}
