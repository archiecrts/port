<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of TradingZoneLastSeenSQLConstructor
 *
 * @author Archie
 */
class TradingZoneLastSeenSQLConstructor {
    
    public function createTradingZoneLastSeen($row) {
        
        return new TradingZoneLastSeen(
                    $row['tzlst_id'],
                    $row['tzlst_trading_zone'], 
                    $row['tzlst_last_seen_date'],
                    $row['installations_tbl_inst_id'],
                    $row['tzlst_lrno']);
    }
}
