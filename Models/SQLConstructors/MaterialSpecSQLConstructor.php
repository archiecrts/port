<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of MaterialSpecSQLConstructor
 *
 * @author Archie
 */
class MaterialSpecSQLConstructor {
    
    /**
     * createMaterialSpec
     * @param type $row
     * @return \PartMaterialSpec
     */
    public function createMaterialSpec($row) {
        
        return new PartMaterialSpec(
                    $row['mst_id'], 
                    $row['mst_code'], 
                    $row['mst_description'], 
                    $row['mst_hardness'],
                    $row['mst_din_standard'],
                    $row['mst_material_spec']);
    }
}
