<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of NoticeBoardCategoriesSQLConstructor
 *
 * @author Archie
 */
class NoticeBoardCategoriesSQLConstructor {
    
    
    public function createNoticeBoardCategories($row) {
        
        return new NoticeBoardCategories(
                            $row['nbtc_id'],
                            $row['nbtc_category'],
                            $row['nbtc_not_editable']);
    }
    
}
