<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of ReportParametersSQLConstructor
 *
 * @author Archie
 */
class ReportParametersSQLConstructor {
    
    
    public function createReportParameter($row) {
        
        return new ReportParameters(
                    $row['par_parameter'], 
                    $row['par_value'], 
                    $row['report_overview_tbl_rot_id']);
    }
}
