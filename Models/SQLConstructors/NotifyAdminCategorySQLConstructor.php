<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of NotifyAdminCategorySQLConstructor
 *
 * @author Archie
 */
class NotifyAdminCategorySQLConstructor {
   
    
    public function createNotifyAdminCategory($row) {
        
        return new NotifyAdminCategory(
                $row['itt_id'], 
                $row['itt_category']);
    }
    
}
