<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of DesignerLookupSQLConstructor
 *
 * @author Archie
 */
class DesignerLookupSQLConstructor {
    
    /**
     * createDesignerLookup
     * @param type $row
     * @return \DesignerLookup
     */
    public function createDesignerLookup($row) {
        
        return new DesignerLookup(
                $row['dlt_id'], 
                $row['dlt_designer_description']);
    }
}
