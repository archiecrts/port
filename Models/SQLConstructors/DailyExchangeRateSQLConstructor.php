<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of DailyExchangeRateSQLConstructor
 *
 * @author Archie
 */
class DailyExchangeRateSQLConstructor {
    
    public function createDailyExchnageRate($row) {
        
        return new DailyExchangeRate(
                    $row['daily_id'],
                    $row['daily_currency_code'],
                    $row['daily_currency_unit_per_pound'],
                    $row['daily_last_changed_date']);

    }
}
