<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of PartCodeSQLConstructor
 *
 * @author Archie
 */
class PartCodeSQLConstructor {
    
    /**
     * createPartCode
     * @param type $row
     * @return \PartCode
     */
    public function createPartCode($row) {
        
        return new PartCode(
                    $row['part_id'], 
                    $row['part_number'], 
                    $row['part_description_lookup_tbl_pdlt_id'], 
                    $row['part_weight'],
                    $row['part_total_stock'],
                    $row['part_minimum_stock'],
                    $row['part_reserved_stock'],
                    $row['part_free_stock_qty'],
                    $row['part_on_order_qty'],
                    $row['part_reserved_for_kits'],
                    $row['part_prefered_supplier_id'],
                    $row['part_cost_price'],
                    $row['part_cost_price_set_date'],
                    $row['part_wear_item'],
                    $row['dimensions_tbl_dim_id'],
                    $row['material_spec_tbl_mst_id'],
                    $row['part_colour_tbl_pct_id'],
                    $row['part_ld_part_number'],
                    $row['part_description'],
                    $row['part_stock_item']);
    }
}
