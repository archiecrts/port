<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of SpecialSurveyHistoryDateSQLConstructor
 *
 * @author Archie
 */
class SpecialSurveyHistoryDateSQLConstructor {
    
    /**
     * createSpecialSurveyDueDate
     * @param type $row
     * @return \SpecialSurveyDue
     */
    public function createSpecialSurveyHistoryDate($row) {
        
        return new SpecialSurveyHistoryDates(
                $row['sdht_id'], 
                $row['sdht_special_survey_date'], 
                $row['installations_tbl_inst_id'], 
                $row['sdht_lrno']);
        
        
    }
    
}
