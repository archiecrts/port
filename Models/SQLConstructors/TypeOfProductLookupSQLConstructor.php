<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of TypeOfProductLookupSQLConstructor
 *
 * @author Archie
 */
class TypeOfProductLookupSQLConstructor {
    
    /**
     * createTypeOfProductLookup
     * @param type $row
     * @return \TypeOfProductLookup
     */
    public function createTypeOfProductLookup($row) {
        
        return new TypeOfProductLookup(
                $row['toplt_id'], 
                $row['toplt_product_description'],
                $row['toplt_short_code']);
    }
}
