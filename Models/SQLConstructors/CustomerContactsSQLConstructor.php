<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of CustomerContactsSQLConstructor
 *
 * @author Archie
 */
class CustomerContactsSQLConstructor {
   
    
    public function createCustomerContact($row) {
        
        return new CustomerContact(
                    $row['ict_id'],
                    $row['ict_salutation'],
                    $row['ict_first_name'], 
                    $row['ict_surname'],
                    $row['ict_job_title'], 
                    $row['ict_email'], 
                    $row['ict_number'],
                    $row['ict_mobile_number'],
                    $row['ict_info_source'],
                    $row['ict_inactive'],
                    $row['ict_contact_validated'],
                    $row['customer_tbl_cust_id'],
                    $row['customer_address_tbl_cadt_id']);
    }
}
