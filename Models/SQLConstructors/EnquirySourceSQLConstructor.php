<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
//include 'Models/Entities/EnquirySource.php';
/**
 * Description of EnquirySource
 *
 * @author alexandra
 */
class EnquirySourceSQLConstructor {
   public function createEnquirySource($row) {
        
        return new EnquirySource(
                    $row['est_id'],
                    $row['est_source']);

    }
}
