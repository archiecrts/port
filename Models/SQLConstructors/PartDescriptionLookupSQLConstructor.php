<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of PartDescriptionLookupSQLConstructor
 *
 * @author Archie
 */
class PartDescriptionLookupSQLConstructor {
    
    /**
     * createPartDescriptionLookup
     * @param Array $row
     * @return \PartDescriptionLookup
     */
    public function createPartDescriptionLookup($row) {
        
        return new PartDescriptionLookup(
                    $row['pdlt_id'], 
                    $row['pdlt_major_type'], 
                    $row['pdlt_minor_type']);
    }
}
