<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of EnquiryEngineSQLConstructor
 *
 * @author Archie
 */
class EnquiryEngineSQLConstructor {
    
    public function createEnquiryEngine($row) {
        
        return new EnquiryEngine(
                $row['eet_id'], 
                $row['enquiry_tbl_enq_id'], 
                $row['installation_engine_tbl_iet_id']);
    }
}
