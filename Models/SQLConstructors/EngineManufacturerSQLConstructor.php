<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of EngineManufacturerSQLConstructor
 *
 * @author Archie
 */
class EngineManufacturerSQLConstructor {
    
    
    public function createEngineManufacturer($row) {
        return new EngineManufacturer($row['iet_engine_manufacturer']);
    }
    
    /*public function createAuxEngineManufacturer($row) {
        return new EngineManufacturer($row['inst_aux_engine_mfr']);
    }*/
    
    public function createEngineBuilder($row) {
        return new EngineBuilder($row['eb_id'], $row['eb_engine_builder_name']);
    }
}
