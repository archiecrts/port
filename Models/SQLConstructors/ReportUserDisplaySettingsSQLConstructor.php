<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of ReportUserDisplaySettingsSQLConstructor
 *
 * @author Archie
 */
class ReportUserDisplaySettingsSQLConstructor {
    
    /**
     * createReportUserDisplaySettings
     * @param Object Array $row
     * @return \ReportUserDisplayColumnName
     */
    public function createReportUserDisplaySettings($row) {
        
        return new ReportUserDisplayColumnName(
                $row['rudcn_id'], 
                $row['rudcn_column_name']); 
    }
}
