<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of EnquirySQLConstructor
 *
 * @author Archie
 */
class EnquirySQLConstructor {
    
    
    public function createEnquiry($row) {
        
        return new Enquiry(
                $row['enq_id'], 
                $row['enq_date'], 
                $row['user_tbl_usr_id'], 
                $row['enq_success_rate'], 
                $row['enq_header_note'], 
                $row['customer_tbl_cust_id'], 
                $row['enquiry_source_tbl_est_id'], 
                $row['enquiry_type_tbl_et_id'], 
                $row['enq_customer_reference'], 
                $row['customer_contacts_tbl_ict_id'],
                $row['enq_include_installation_in_quote'],
                $row['enq_our_reference']);
    }
}
