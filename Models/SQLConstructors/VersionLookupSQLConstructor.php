<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of VersionLookupSQLConstructor
 *
 * @author Archie
 */
class VersionLookupSQLConstructor {
    
    /**
     * createVersionLookup
     * @param type $row
     * @return \VersionLookup
     */
    public function createVersionLookup($row) {
        
        return new VersionLookup(
                $row['vlt_id'], 
                $row['vlt_version_description'],
                $row['model_lookup_tbl_mlt_id'],
                $row['vlt_cylinder_count'],
                $row['vlt_cylinder_configuration']);
    }
}
