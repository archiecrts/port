<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
//include 'Models/Entities/Customer.php';
/**
 * Description of CustomerSQLConstructor
 *
 * @author Archie
 */
class CustomerSQLConstructor {
    
    public function createCustomer($row) {
        
        return new Customer(
                    $row['cust_id'],
                    $row['cust_name'], 
                    $row['cust_telephone'], 
                    $row['cust_fax'], 
                    $row['cust_website'], 
                    $row['cust_account_manager'],
                    $row['cust_tbh_id'],
                    $row['cust_tbh_account_code'],
                    $row['cust_ld_id'],
                    $row['cust_ld_account_code'],
                    $row['cust_header_notes'],
                    $row['cust_seaweb_code'],
                    $row['cust_parent_company_seaweb_code'],
                    $row['cust_parent_company_nationality_of_control'],
                    $row['cust_company_status'],
                    $row['cust_company_full_name'],
                    $row['cust_navision_id']);
    }
}
