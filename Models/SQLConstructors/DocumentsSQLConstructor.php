<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of DocumentsSQLConstructor
 *
 * @author Archie
 */
class DocumentsSQLConstructor {
    
    
    public function createDocument($row) {
        
        return new Document(
                    $row['doc_id'], 
                    $row['doc_url'], 
                    $row['doc_loaded_date'], 
                    $row['doc_read_by_date'],
                    $row['doc_archived'],
                    $row['doc_destination']);
    }
}
