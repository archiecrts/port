<script>
// autocomplet : this function will be executed every time we change the text
    function autocompletePartCode() {
        var min_length = 1; // min caracters to display the autocomplete
        var keyword = $('#searchBoxOne').val();
        if (keyword.length >= min_length) {
            $.ajax({
                url: './Views/AutoCompleteSearch/autoCompleteSQLPartCode.php',
                type: 'POST',
                data: {keyword: keyword},
                success: function (data) {
                    $('#searchListOne').show();
                    $('#searchListOne').html(data);
                }
            });
        } else {
            $('#searchListOne').hide();
        }
    }

// set_item : this function will be executed when we select an item
    function set_part(item) {
        // change input value
        $('#searchBoxOne').val(item);
        // hide proposition list
        $('#searchListOne').hide();
    }



    // managing company is the customer
    function set_partcode_id(id) {
        // change the hidden value
        $('#partCodeID').val(id);
    }

    $(document).on('keyup keypress', 'form input[type="text"]', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            return false;
        }
    });
</script>

<style>

    #searchListOne {
        display: none;
    }
</style>
<?php
$seriesID = "";
$versionID = "";
if (isset($_GET['seriesID'])) {
    $seriesID = $_GET['seriesID'];
}
if (isset($_GET['versionID'])) {
    $versionID = $_GET['versionID'];
}

?>
<h3>Add to Parts Manifest (Group)</h3>

<p>This will add each component to every version in this series unless specified 
    and assume the number of parts correspond to the number of cylinders unless specified.</p>

<p>
<form name="form1" action="index.php?portal=parts&page=addToManifest&action=createGroupSave&seriesID=<?php echo $seriesID; ?>&versionID=<?php echo $versionID; ?>" method="post" enctype="multipart/form-data">

    <div>
        <select name="addTo">
            <option value="all">All</option>
            <?php
            $versionList = $versionLookupController->getVersionIDBySeriesID($_GET['seriesID']);

            foreach ($versionList as $verID => $version) {
                print "<option value='$verID'>$version->cylinderCount $version->cylinderConfiguration</option>";
            }
            ?>
        </select>
        <input type="text" id="quantity" name="quantity" value="" placeholder="Quantity (optional)" size="8"/>
        <input type="text" id="searchBoxOne" onkeyup="autocompletePartCode()"  autocomplete="off" placeholder="Type a part code or description">
        <input type="hidden" id="partCodeID" name="partCodeID" value=""/>
        <input type="submit" name="submit" value="Go"/>
        <ul id="searchListOne" multiple size="15"></ul>
    </div>


</form>
</p>
<?php
if (isset($_GET['action']) && (($_GET['action'] == 'createGroup') || ($_GET['action'] == 'createGroupSave'))) {
    print "<table><tr>";
    print "<th>Part Code</th>";
    print "<th>Description</th>";
    // create manifests for every version of a series.
    $versionList = $versionLookupController->getVersionIDBySeriesID($_GET['seriesID']);
    
    foreach ($versionList as $verID => $version) {
        print "<th>$version->cylinderCount $version->cylinderConfiguration</th>";
    }
    
    print "<th>Colour</th>";
    print "</tr>";
    // get all the parts in the database.
    $partList = $partCodeController->getAllPartCodes();
    print "<pre>";
    
    if (isset($partList)) {
        foreach ($partList as $pID => $code) {

            $manifestPart = $partManifestController->getMainfestByPartIDAndSeriesID($code->partID, $_GET['seriesID']);
            //print_r($manifestPart);
            if (isset($manifestPart)) {
                print "<tr>";
                print "<td>$code->partCode</td>";
                print "<td>".$partDescriptionLookupController->getDescriptionByID($code->partDescriptionLookupID)->majorType." ".$partDescriptionLookupController->getDescriptionByID($code->partDescriptionLookupID)->minorType."</td>";

                foreach ($manifestPart as $manIDp => $manifestp) {
                    foreach ($manifestp as $id => $manifest) {
                        print "<td>$manifest->quantityOnEngine</td>";
                    }
                }
                print "<td>".$partColourController->getPartColourByID($code->colourID)->partColour."</td>";
                print "</tr>";
            }

        }
    }
    
    print "</table>";
}