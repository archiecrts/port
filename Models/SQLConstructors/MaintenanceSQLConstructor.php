<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of MaintenanceSQLConstructor
 *
 * @author Archie
 */
class MaintenanceSQLConstructor {
    
    /**
     * createMaintenance
     * @param type $row
     * @return \Maintenance
     */
    public function createMaintenance($row) {
        
        return new Maintenance(
                $row['maint_id'], 
                $row['maint_active']);
    }
}
