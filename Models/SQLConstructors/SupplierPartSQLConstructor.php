<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of SupplierPartSQLConstructor
 *
 * @author Archie
 */
class SupplierPartSQLConstructor {
    
    /**
     * createSupplierParts
     * @param type $row
     * @return \SupplierParts
     */
    public function createSupplierParts($row) {
        
        return new SupplierParts(
                    $row['spt_id'], 
                    $row['customer_tbl_cust_id'],
                    $row['spt_stock_level'], 
                    $row['part_code_tbl_part_id'], 
                    $row['spt_price'], 
                    $row['spt_date_price_set'],
                    $row['spt_lead_time']);
    }
}
