<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of InstallationSQLConstructor
 *
 * @author Archie
 */
class InstallationSQLConstructor {

    public function createInstallation($row) {

        return new Installation(
                $row['inst_id'], 
                $row['inst_type'], 
                $row['inst_installation'], 
                $row['inst_installation_name'], 
                $row['inst_imo'], 
                $row['inst_owner_parent_company'], 
                $row['inst_technical_manager_company'],
                $row['inst_group_owner'],
                $row['installation_status_tbl_stat_id'],      
                $row['inst_original_source'],
                $row['inst_original_source_date'],
                $row['inst_photo_link'],
                $row['inst_hull_number'],
                $row['inst_hull_type'],
                $row['inst_yard_built'],
                $row['inst_deadweight'],
                $row['inst_length_overall'],
                $row['inst_beam_extreme'],
                $row['inst_beam_moulded'],
                $row['inst_built_date'],
                $row['inst_draught'],
                $row['inst_gross_tonnage'],
                $row['inst_length'],
                $row['inst_propeller_type'],
                $row['inst_propulsion_unit_count'],
                $row['inst_ship_builder'],
                $row['inst_teu'],
                $row['inst_build_year'],
                $row['inst_archive'],
                $row['inst_archive_reason'],
                $row['customer_address_tbl_cadt_id'],
                $row['inst_flag_name'],
                $row['inst_lead_ship_in_series_by_imo'],
                $row['inst_ship_type_level_4'],
                $row['inst_classification_society']
                );
// Add other fields
    }

}
