<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of SupplierApprovalSQLConstructor
 *
 * @author Archie
 */
class SupplierApprovalSQLConstructor {
   
    /**
     * createSupplierApproval
     * @param type $row
     * @return \SupplierApproval
     */
    public function createSupplierApproval($row) {
        
        return new SupplierApproval(
                    $row['sat_id'], 
                    $row['customer_tbl_cust_id'], 
                    $row['sat_approved_as_supplier'], 
                    $row['sat_approved_as_trader'],
                    $row['sat_approved_as_agent'],
                    $row['supplier_approval_status_lookup_tbl_saslt_id'],
                    $row['sat_date_approval_status_confirmed'],
                    $row['sat_status_review_date'],
                    $row['sat_reason_for_status_change']);
    }
}
