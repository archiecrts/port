<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of PartColourSQLConstructor
 *
 * @author Archie
 */
class PartColourSQLConstructor {
    
    /**
     * createPartColour
     * @param type $row
     * @return \PartColour
     */
    public function createPartColour($row) {
        
        return new PartColour(
                    $row['pct_id'], 
                    $row['pct_description']
                    );
    }
}
