<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of UpdateCustomerSQLConstructor
 *
 * @author Archie
 */
class UpdateCustomerSQLConstructor {
    
    /**
     * createUpdateCustomer
     * @param type $row
     * @return \UpdateCustomer
     */
    public function createUpdateCustomer($row) {
        
        return new UpdateCustomer(
                $row['temp_id'], 
                $row['owcode'], 
                $row['short_company_name'], 
                $row['country_name'],
                $row['town_name'],
                $row['telephone'],
                $row['email'],
                $row['website'],
                $row['company_status'],
                $row['full_company_name'],
                $row['full_address'],
                $row['customer_duplicate'],
                $row['address_duplicate'],
                $row['contact_duplicate'],
                $row['customer_exists'],
                $row['address_exists'],
                $row['contact_exists'],
                $row['customer_id'],
                $row['customer_address_id'],
                $row['customer_contact_id']);
    }
}
