<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of CustomerStatusLookupSQLConstructor
 *
 * @author Archie
 */
class CustomerStatusLookupSQLConstructor {
    
    public function createCustomerStatusLookup($row) {
        
        return new CustomerStatusLookup(
                    $row['cslt_id'],
                    $row['cslt_status']);

    }
}
