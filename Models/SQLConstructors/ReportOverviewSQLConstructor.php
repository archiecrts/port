<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of ReportOverviewSQLConstructor
 *
 * @author Archie
 */
class ReportOverviewSQLConstructor {
    
    
    public function createReportOverview($row) {
        
        return new ReportOverview(
                    $row['rot_id'],
                    $row['rot_report_name'], 
                    $row['rot_date_created'], 
                    $row['user_tbl_usr_id'], 
                    $row['rot_email_sent'], 
                    $row['rot_dynamic_flag'],
                    $row['rot_report_active']);
    }
}
