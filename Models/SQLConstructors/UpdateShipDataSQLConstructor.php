<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of UpdateShipDataSQLConstructor
 *
 * @author Archie
 */
class UpdateShipDataSQLConstructor {
    
    public function createUpdateShipData($row) {
        
        return new UpdateShipData(
                $row['temp_id'], 
                $row['lrimo'], 
                $row['ship_name'], 
                $row['ship_status'],
                $row['ship_type_level_2'],
                $row['ship_type_level_4'],
                $row['date_of_build'],
                $row['lead_ship_in_series_by_imo'],
                $row['flag_name'],
                $row['classification_society'],
                $row['ship_manager'],
                $row['ship_manager_company_code'],
                $row['technical_manager'],
                $row['technical_manager_code'],
                $row['noof_aux_engines'],
                $row['noof_main_engines'],
                $row['noof_propulsion_units'],
                $row['propeller_type'],
                $row['ship_builder'],
                $row['yard_number'],
                $row['ship_duplicate'],
                $row['ship_exists'],
                $row['installation_id']);
    }
}
