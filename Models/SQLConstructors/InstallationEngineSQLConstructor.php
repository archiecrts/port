<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of InstallationEngineSQLConstructor
 *
 * @author Archie
 */
class InstallationEngineSQLConstructor {
    public function createInstallationEngine($row) {

        return new InstallationEngine(
                $row['iet_id'],
                $row['product_tbl_prod_id'],
                $row['iet_model_description'], 
                $row['iet_main_aux'],
                $row['iet_engine_verified'],
                $row['iet_output'],
                $row['iet_output_unit_of_measure'],
                $row['iet_fuel_type'],
                $row['iet_bore_size'],
                $row['iet_stroke'],
                $row['iet_release_date'],
                $row['iet_enquiry_only'],
                $row['load_priority_lookup_tbl_load_id'], 
                $row['iet_position_if_main'],
                $row['iet_engine_builder']);
    }
}
