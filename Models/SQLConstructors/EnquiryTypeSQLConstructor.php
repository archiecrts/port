<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of EnquiryTypeSQLConstructor
 *
 * @author alexandra
 */
class EnquiryTypeSQLConstructor {
  
    public function createEnquiryType($row) {
        
        return new EnquiryType(
                    $row['et_id'],
                    $row['et_type'],
                    $row['et_prefix']);
    }
}
