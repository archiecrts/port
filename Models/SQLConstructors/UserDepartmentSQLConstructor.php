<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of UserDepartmentSQLConstructor
 *
 * @author Archie
 */
class UserDepartmentSQLConstructor {
    
    
    public function createUserDepartment($row) {
        
        return new UserDepartment(
                    $row['dept_id'], 
                    $row['dept_name']);
    }
}
