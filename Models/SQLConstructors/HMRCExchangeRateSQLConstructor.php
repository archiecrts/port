<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HMRCExchangeRateSQLConstructor
 *
 * @author Archie
 */
class HMRCExchangeRateSQLConstructor {
    
    
    public function createHMRCExchnageRate($row) {
        
        return new HMRCExchangeRate(
                    $row['hmrc_id'],
                    $row['hmrc_country'],
                    $row['hmrc_currency'],
                    $row['hmrc_currency_code'],
                    $row['hmrc_currency_units_per_pound'],
                    $row['hmrc_start_date'],
                    $row['hmrc_end_date']);

    }
}
