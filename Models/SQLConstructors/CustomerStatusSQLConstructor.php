<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of CustomerStatusSQLConstructor
 *
 * @author Archie
 */
class CustomerStatusSQLConstructor {
    
    public function createCustomerStatus($row) {
        
        return new CustomerStatus(
                    $row['cst_id'],
                    $row['customer_tbl_cust_id'],
                    $row['customer_status_lookup_tbl_cslt_id']);

    }
}
