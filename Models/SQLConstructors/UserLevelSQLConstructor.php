<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of UserLevelSQLConstructor
 *
 * @author Archie
 */
class UserLevelSQLConstructor {
    
    public function createUserLevel($row) {
        
        return new UserLevel(
                    $row['level_id'], 
                    $row['level_name']);
    }
    
}
