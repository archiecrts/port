<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
//include 'Models/Entities/CustomerAddress.php';
/**
 * Description of CustomerAddressSQLConstructor
 *
 * @author Archie
 */
class CustomerAddressSQLConstructor {
    
    public function createCustomerAddress($row) {
        
        
        
        $customerAddress = new CustomerAddress(
                $row['cadt_id'], 
                $row['cadt_office_name'], 
                $row['cadt_address'], 
                $row['cadt_city'], 
                $row['cadt_state'], 
                $row['cadt_region'],
                $row['cadt_country'], 
                $row['cadt_postcode'], 
                $row['cadt_address_verified'], 
                $row['customer_tbl_cust_id'],  
                $row['cadt_archived'],
                $row['cadt_default_address'],
                $row['cadt_full_address']
                );
        
        return $customerAddress;

    }

}
