<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of NoticeBoardSQLConstructor
 *
 * @author Archie
 */
class NoticeBoardSQLConstructor {
    
    
    public function createNoticeBoard($row) {
        
        return new NoticeBoard(
                    $row['nbt_id'],
                    $row['nbt_title'], 
                    $row['nbt_date'], 
                    $row['nbt_content'], 
                    $row['user_tbl_usr_id'], 
                    $row['notice_board_categories_tbl_nbtc_id'],
                    $row['nbt_archive'],
                    $row['nbt_authorised'],
                    $row['documents_tbl_doc_id']);
    }
    
}
