<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once 'Models/Entities/ReportResult.php';
/**
 * Description of ReportResultsSQLConstructor
 *
 * @author Archie
 */
class ReportResultsSQLConstructor {
    public function createReportResults($row) {

        return new ReportResult(
                $row['inst_type'], 
                $row['inst_installation'], 
                $row['inst_imo'],
                $row['inst_installation_name'], 
                $row['sddt_special_survey_date'], 
                $row['cust_name'], 
                $row['cadt_city'], 
                $row['cadt_region'], 
                $row['cadt_state'], 
                $row['cadt_country'], 
                $row['c_area'], 
                $row['c_continent'], 
                $row['inst_owner_parent_company'], 
                $row['inst_technical_manager_company'], 
                $row['installation_status_tbl_stat_id'], 
                $row['type_of_product_lookup_tbl_toplt_id'],
                $row['dlt_designer_description'], 
                $row['mlt_series'], 
                $row['iet_model_description'],
                $row['engine_count'],
                $row['iet_main_aux'], 
                $row['inst_original_source'], 
                $row['cust_account_manager'], 
                $row['cust_id'], 
                $row['inst_id'], 
                $row['cadt_id'], 
                $row['iet_id'],
                $row['inst_built_date'],
                $row['tm_country'],
                $row['tm_city'],
                $row['tm_area'],
                $row['tm_continent'],
                $row['prod_source_alias_id'],
                $row['iet_release_date']);

    }
}
