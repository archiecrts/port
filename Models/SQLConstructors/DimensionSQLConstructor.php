<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of DimensionSQLConstructor
 *
 * @author Archie
 */
class DimensionSQLConstructor {
    
    /**
     * createDimensions
     * @param type $row
     * @return \PartDimensions
     */
    public function createDimensions($row) {
        
        return new PartDimensions(
                    $row['dim_id'], 
                    $row['dim_outer_diameter'], 
                    $row['dim_profile_diameter'], 
                    $row['dim_length'],
                    $row['dim_din_number'],
                    $row['dim_head_type'],
                    $row['dim_valve_or_seat_angle_in_degrees'],
                    $row['dim_inner_diameter']);
    }
}
