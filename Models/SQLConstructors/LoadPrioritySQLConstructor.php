<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of LoadPrioritySQLConstructor
 *
 * @author Archie
 */
class LoadPrioritySQLConstructor {
    
    
    public function createLoadPriority($row) {

        return new LoadPriority(
                $row['load_id'], 
                $row['load_priority']
            );
    }
                
}
