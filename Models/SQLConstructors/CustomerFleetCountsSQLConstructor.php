<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of CustomerFleetCountsSQLConstructor
 *
 * @author Archie
 */
class CustomerFleetCountsSQLConstructor {
    
    /**
     * createCustomerFleetCounts
     * @param type $row
     * @return \CustomerFleetCounts
     */
    public function createCustomerFleetCounts($row) {
        
        return new CustomerFleetCounts(
                $row['cfct_id'], 
                $row['cfct_owcode'], 
                $row['customer_tbl_cust_id'], 
                $row['cfct_registered_owner_count'], 
                $row['cfct_shipmanager_count'], 
                $row['cfct_operator_count'], 
                $row['cfct_group_owner_count'], 
                $row['cfct_doc_count'], 
                $row['cfct_fleet_size'],
                $row['cfct_in_service_count']);
        
        
    }
}
