<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of UserTerritorySQLConstructor
 *
 * @author Archie
 */
class UserTerritorySQLConstructor {
    
    
    public function createUserTerritory($row) {
        
        return new UserTerritory(
                $row['ut_id'], 
                $row['user_tbl_usr_id'], 
                $row['ut_territory_name']);
    }
    
}
