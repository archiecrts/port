<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of TrainingSQLConstructor
 *
 * @author Archie
 */
class TrainingSQLConstructor {
    
    
    public function createTraining($row) {
        
        return new Training(
                $row['tr_id'], 
                $row['tr_has_been_read'], 
                $row['tr_hr_confirmation'], 
                $row['user_tbl_usr_id'], 
                $row['documents_tbl_doc_id']);
    }
    
}
