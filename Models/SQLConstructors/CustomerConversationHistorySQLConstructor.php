<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of CustomerConversationHistorySQLConstructor
 *
 * @author Archie
 */
class CustomerConversationHistorySQLConstructor {
    
    /**
     * createCustomerConversationHistory
     * @param type $row
     * @return \CustomerConversationHistory
     */
    public function createCustomerConversationHistory($row) {
        return new CustomerConversationHistory(
                    $row['ich_id'], 
                    $row['ich_status'], 
                    $row['customer_conversation_tbl_con_id'], 
                    $row['user_tbl_usr_id']);
    }
    
}
