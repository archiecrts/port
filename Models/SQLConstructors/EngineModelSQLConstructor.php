<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of EngineModelSQLConstructor
 *
 * @author Archie
 */
class EngineModelSQLConstructor {
    
    
    public function createEngineModel($row) {
        
        return new EngineModel($row['iet_engine_model']);
    }
    
    
    /*public function createAuxEngineModel($row) {
        
        return new EngineModel($row['inst_aux_engine_model']);
    }*/
    
    
    public function createEngineDBModel($row, $manufacturerID) {
        
        return new EngineDBModel(
                $row['em_id'], 
                $row['em_model_description'], 
                $manufacturerID);
    }
    
    /**
     * createEngineDBModelNoManufacturer
     * @param type $row
     * @return \EngineDBModel
     */
    public function createEngineDBModelNoManufacturer($row) {
        
        return new EngineDBModel(
                $row['em_id'], 
                $row['em_model_description'], 
                $row['engine_builder_tbl_eb_id']);
    }
}
