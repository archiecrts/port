<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of SupplierApprovalStatusLookupSQLConstructor
 *
 * @author Archie
 */
class SupplierApprovalStatusLookupSQLConstructor {
   
    /**
     * createSupplierApprovalStatusLookup
     * @param type $row
     * @return \SupplierApprovalStatus
     */
    public function createSupplierApprovalStatusLookup($row) {
        
        return new SupplierApprovalStatusLookup(
                    $row['saslt_id'], 
                    $row['saslt_approval_status']);
    }
    
}
