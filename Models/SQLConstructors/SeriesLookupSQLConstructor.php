<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of SeriesLookupSQLConstructor
 *
 * @author Archie
 */
class SeriesLookupSQLConstructor {
    
    public function createSeriesLookup($row) {
        
        return new SeriesLookup(
                $row['mlt_id'], 
                $row['mlt_series'],
                $row['designer_lookup_tbl_dlt_id'],
                $row['type_of_product_lookup_tbl_toplt_id']);
    }
}
