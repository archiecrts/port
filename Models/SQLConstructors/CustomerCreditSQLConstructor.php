<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of CustomerCreditSQLConstructor
 *
 * @author Archie
 */
class CustomerCreditSQLConstructor {
    
    public function createCustomerCredit($row) {
        
        return new CustomerCredit(
                    $row['cct_id'],
                    $row['cct_credit_limit'], 
                    $row['cct_account_stop'],
                    $row['customer_tbl_cust_id'],
                    $row['cct_payent_terms']
                    );
    }
}
