<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/CustomerHasInstallations.php");
include_once 'Models/SQLConstructors/CustomerHasInstallationsSQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of CustomerHasInstallationsModel
 *
 * @author Archie
 */
class CustomerHasInstallationsModel {
    
    /**
     * setNewCustomerHasInstallationsRecord
     * @param type $customerID
     * @param type $installationID
     * @return type
     */
    public function setNewCustomerHasInstallationsRecord($customerID, $installationID) {
        $insert = "INSERT INTO customer_tbl_has_installations_tbl (customer_tbl_cust_id, installations_tbl_inst_id) "
                . "VALUES ('$customerID', '$installationID')";
        
        if (!mysqli_query(Database::$connection, $insert)) {
            debugWriter("debug.txt", "insert into customer has installations failed ".mysqli_error(Database::$connection));
            return null;
        } else {
            return mysqli_insert_id(Database::$connection);
        }
    }
    
    /**
     * updateCustomerHasInstallationsRecord
     * @param type $customerID
     * @param type $installationID
     * @param type $newCustomerHasInstRecord
     * @return type
     */
    public function updateCustomerHasInstallationsRecord($customerID, $installationID, 
            $newCustomerHasInstRecord) {
        //debugWriter("debug.txt", "INSIDE UPDATE FUNCTION ".$customerID. " inst ".$installationID ." new ".$newCustomerHasInstRecord->customerID);
        $insert = "UPDATE customer_tbl_has_installations_tbl SET "
                . "customer_tbl_cust_id = '".$newCustomerHasInstRecord->customerID."' "
               // . "AND installations_tbl_inst_id = '".$newCustomerHasInstRecord->installationID."' "
                . "WHERE customer_tbl_cust_id = '$customerID' AND "
                . "installations_tbl_inst_id = '$installationID'";
        
        if (!mysqli_query(Database::$connection, $insert)) {
            debugWriter("debug.txt", "update customer has installations failed ".mysqli_error(Database::$connection));
            return null;
        } else {
            return true;
        }
    }
    
    
    /**
     * getCustomerIDFromInstallationID
     * @param type $installationID
     * @return type
     */
    public function getCustomerIDFromInstallationID($installationID) {
        $customerHasInstallationSQLConstructor = new CustomerHasInstallationsSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_tbl_has_installations_tbl WHERE "
                . "installations_tbl_inst_id = '$installationID'");
        
        $row = mysqli_fetch_assoc($result);
        $record = $customerHasInstallationSQLConstructor->createCustomerHasInstallations($row);
        
        if (isset($record)) {
            return $record;
        } else {
            return null;
        }
    }
    
    /**
     * getInstallationIDArrayFromCustomerID
     * @param type $customerID
     * @return type
     */
    public function getInstallationIDArrayFromCustomerID($customerID) {
        $customerHasInstallationSQLConstructor = new CustomerHasInstallationsSQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_tbl_has_installations_tbl WHERE "
                . "customer_tbl_cust_id = '$customerID'");
        
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['installations_tbl_inst_id']] = $customerHasInstallationSQLConstructor->createCustomerHasInstallations($row);
        }
        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
}
