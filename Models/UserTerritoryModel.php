<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once("Models/Entities/User.php");
include_once("Models/Entities/UserTerritory.php");
include_once 'Models/SQLConstructors/UserTerritorySQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of UserTerritoryModel
 *
 * @author Archie
 */
class UserTerritoryModel {
    
    // Get the user's territory(s).
    public function getTerritory($userID) 
    {
        $userTerritorySQLConstructor = new UserTerritorySQLConstructor();
        //execute the SQL query and return records
            $result = mysqli_query(Database::$connection, "SELECT * FROM user_territory_tbl "
                    . "WHERE user_tbl_usr_id='".$userID."'");
           
            //fetch tha data from the database
            while ($row = mysqli_fetch_assoc($result)) {             
                $array[$row['ut_territory_name']] = $userTerritorySQLConstructor->createUserTerritory($row);
            }
            
            if (isset($array)) {
                return $array;
            } else {
                return null;
            }
    }
    
    /**
     * 
     * @param type $territoryObject
     * @return boolean
     */
    public function setTerritory($territoryObject) {
        
        $insert = "INSERT INTO user_territory_tbl (user_tbl_usr_id, ut_territory_name) "
                . "VALUES ('".$territoryObject->userID."', '".$territoryObject->territory."')";
        
        if (!mysqli_query(Database::$connection, $insert)) {
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * 
     * @param type $territoryID
     * @return boolean
     */
    public function deleteTerritory($territoryID)
    {
        $delete = "DELETE FROM user_territory_tbl WHERE ut_id='".$territoryID."'";
        if (!mysqli_query(Database::$connection, $delete)) {
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * 
     * @return \UserTerritory
     */
    public function getAllTerritories()
    {
        $userTerritorySQLConstructor = new UserTerritorySQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM user_territory_tbl");
           
            //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {             
                $array[$row['ut_territory_name']] = $userTerritorySQLConstructor->createUserTerritory($row);
        }
        return $array;
    }
    
    
    /**
     * getUserIDByTerritory
     * @param STRING $area
     * @param STRING $country
     * @return INT
     */
    public function getUserIDByTerritory($country) {
        
        if ($country !== "") {
            $result = mysqli_query(Database::$connection, "select user_tbl_usr_id from user_territory_tbl where 
                ut_territory_name IN (select c_name from country_tbl where c_name = '".addslashes($country)."')
                OR ut_territory_name IN (select c_area from country_tbl where c_name = '".addslashes($country)."')
                OR ut_territory_name IN (select c_continent from country_tbl where c_name = '".addslashes($country)."')");

            $row = mysqli_fetch_assoc($result);
            
           // debugWriter("debug.txt", "getUserIDByTerritory: ". mysqli_error(Database::$connection). " ". $country);
        }    
        if (isset($row['user_tbl_usr_id'])) {
            return $row['user_tbl_usr_id'];
        } else {
            return null;
        }  
    }
}
