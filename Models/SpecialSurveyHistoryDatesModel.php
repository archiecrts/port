<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/SpecialSurveyHistoryDates.php");
include_once("Models/SQLConstructors/SpecialSurveyHistoryDateSQLConstructor.php");
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of SpecialSurveyHistoryDatesModel
 *
 * @author Archie
 */
class SpecialSurveyHistoryDatesModel {
    
    
    
    /**
     * getSpecialSurveyDueDateByInstallationID
     * @param type $installationID
     * @return type
     */        
    public function getSpecialSurveyHistoryDatesByInstallationID($installationID) {
        $specialSurveyHistoryDatesSQLConstructor = new SpecialSurveyHistoryDateSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM survey_date_history_tbl WHERE "
                . "installations_tbl_inst_id = '$installationID' AND sdht_special_survey_date IS NOT NULL");
        
        while($row = mysqli_fetch_assoc($result)) {
            $specialSurvey[$row['sdht_id']] = $specialSurveyHistoryDatesSQLConstructor->createSpecialSurveyHistoryDate($row);
        }
        
        if (isset($specialSurvey)) {
            return $specialSurvey;
        } else {
            return null;
        }
        
        
    }
}
