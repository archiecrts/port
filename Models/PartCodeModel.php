<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/PartCode.php");
include_once("Models/SQLConstructors/PartCodeSQLConstructor.php");
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of PartCodeModel
 *
 * @author Archie
 */
class PartCodeModel {
    
    /**
     * getPartCodeByID
     * @param type $partID
     * @return type
     */
    public function getPartCodeByID($partID) {
        $partCodeSQLConstructor = new PartCodeSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM part_code_tbl WHERE part_id='$partID'");
        $row = mysqli_fetch_assoc($result);
        
        $part = $partCodeSQLConstructor->createPartCode($row);
        
        return $part;
    }
    
    /**
     * getAllPartCodes
     * @return type
     */
    public function getAllPartCodes() {
        $partCodeSQLConstructor = new PartCodeSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM part_code_tbl");
        while($row = mysqli_fetch_assoc($result)) {
            $part[$row['part_id']] = $partCodeSQLConstructor->createPartCode($row);
        }
        if (isset($part)) {
            return $part;
        } else {
            return null;
        }
    }
    
    
    /**
     * insertPartCode
     * @param type $partCode
     * @return boolean
     */
    public function insertPartCode($partCode) {
        $insert = "INSERT INTO part_code_tbl (part_number, part_description_lookup_tbl_pdlt_id, part_weight, part_total_stock, "
                . "part_minimum_stock, part_reserved_stock, part_free_stock_qty, part_on_order_qty, part_reserved_for_kits, "
                . "part_prefered_supplier_id, part_cost_price, part_cost_price_set_date, part_wear_item, dimensions_tbl_dim_id, "
                . "material_spec_tbl_mst_id, part_colour_tbl_pct_id, part_ld_part_number, part_description, part_stock_item) "
                . "VALUES ('$partCode->partCode', "
                . "'$partCode->partDescriptionLookupID', "
                . "'$partCode->partWeight', "
                . "'$partCode->partTotalStock', "
                . "'$partCode->partMinimumStock', "
                . "'$partCode->partReservedStock', "
                . "'$partCode->partFreeStockQty', "
                . "'$partCode->partOnOrderQty', "
                . "'$partCode->partReservedForKits', "
                . "'$partCode->partPreferredSupplierID', "
                . "'$partCode->partCostPrice', "
                . "'$partCode->partCostPriceSetDate', "
                . "'$partCode->partWearItem', "
                . "'$partCode->dimensionID', "
                . "'$partCode->materialID', "
                . "'$partCode->colourID', "
                . "'$partCode->ldPartNumber', "
                . "'$partCode->description', "
                . "'$partCode->stockItem')";
        
        if (!mysqli_query(Database::$connection, $insert)) {
            debugWriter("debug.txt", "insertPartCode $insert | ". mysqli_error(Database::$connection));
            return false;
        } else {
            return mysqli_insert_id(Database::$connection);
        }
    }
    
    
    /**
     * getPartCodeByCriteriaOptions
     * @param type $partCode
     * @return type
     */
    public function getPartCodeByCriteriaOptions($partCode) {
        $result = mysqli_query(Database::$connection, "SELECT part_id FROM part_code_tbl WHERE "
                . "part_number = '$partCode->partCode' AND "
                . "part_description_lookup_tbl_pdlt_id = '$partCode->partDescriptionLookupID' AND "
                . "part_weight = '$partCode->partWeight' AND "
                . "part_wear_item = '$partCode->partWearItem' AND "
                . "dimensions_tbl_dim_id = '$partCode->dimensionID' AND "
                . "material_spec_tbl_mst_id = '$partCode->materialID' AND "
                . "part_colour_tbl_pct_id = '$partCode->colourID' AND "
                . "part_ld_part_number = '$partCode->ldPartNumber' AND "
                . "part_description = '$partCode->description'");
        $row = mysqli_fetch_assoc($result);
        debugWriter("debug.txt", "PART NUMBER ".$row['part_id']);
        if (isset($row['part_id'])) {
            return $row['part_id'];
        } else {
            return null;
        }
    }
}