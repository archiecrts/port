<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/UserLevel.php");
include_once("Models/SQLConstructors/UserLevelSQLConstructor.php");
//include_once ("Models/DB.php");
include_once("Models/Database.php");

/**
 * Description of UserLevelModel
 *
 * @author Archie
 */
class UserLevelModel {

    /**
     * getUserLevels
     * @return Array UserLevel
     */
    public function getUserLevels() {
        $userLevelSQLConstructor = new UserLevelSQLConstructor();

        //execute the SQL query and return records
        $result = mysqli_query(Database::$connection, "SELECT * FROM user_level_tbl");

        //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {

            $array[$row['level_id']] = $userLevelSQLConstructor->createUserLevel($row);
        }

        return $array;
    }

    /**
     * getUserLevelByID
     * @param INT $levelID
     * @return UserLevel
     */
    public function getUserLevelByID($levelID) {
        $userLevelSQLConstructor = new UserLevelSQLConstructor();

        //execute the SQL query and return records
        $result = mysqli_query(Database::$connection, "SELECT * FROM user_level_tbl WHERE level_id='".$levelID."'");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);

        $level = $userLevelSQLConstructor->createUserLevel($row);
        
        return $level;
    }

}
