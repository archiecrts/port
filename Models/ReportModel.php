<?php

/*
 * Property of -T Diesel Marine Group.
 */
include_once("Models/Entities/Report.php");
include_once("Models/Entities/Installation.php");
include_once("Models/SQLConstructors/InstallationSQLConstructor.php");
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of ReportModel
 *
 * @author Archie
 */
class ReportModel {
    
    /**
     * This will set the report participants ID into the report table.
     * Thus creating a snapshot of the report.
     * @param INT $installationID
     * @param INT $reportOverviewID
     * @return boolean | mysql error
     */
    public function setReportParticipant($installationID, $reportOverviewID) {
        $result = "INSERT INTO campaign_tbl "
                . "(campaign_overview_tbl_cot_id, camp_installation_id, "
                . "camp_response_received, camp_optout_received, camp_date_created) "
                . "VALUES ('".$reportOverviewID."', '".$installationID."', '0', '0', '".date("Y-m-d H:i:s")."')";
        
        if (!mysqli_query(Database::$connection, $result)) {
            $logger = fopen("./logs/createReport.txt", "a");
            fwrite($logger, "setReportParticipant: Report Overview ID: ".$reportOverviewID.", Inst ID ".$installationID." ".mysqli_error(Database::$connection). "\r\n");
            fclose($logger);
            return false;
        } else {
            
            return true;
        }
    }
    
    /**
     * This will check each installation to see if it appears on any campaigns in the past three months
     * @param INT $installationID
     * @return INT | NULL
     */
    public function checkInstallationForPreviousCampaignInclusion($installationID, $overviewID, $overviewDate) {
        // TODO: This needs to be changed for the new structure.
        /*$result = mysqli_query(Database::$connection, "SELECT EXISTS(SELECT 1 FROM campaign_tbl 
            WHERE camp_installation_id='".$installationID."' 
            AND camp_date_created >= DATE(NOW() - INTERVAL 3 MONTH) 
            AND camp_date_created <= '".$overviewDate."' 
            AND campaign_overview_tbl_cot_id != '".$overviewID."') AS mycheck");

        $row = mysqli_fetch_assoc($result);
        
        if ($row['mycheck'] === '1') {
            $result1 = mysqli_query(Database::$connection, "SELECT COUNT(1) AS mycount FROM campaign_tbl 
            WHERE camp_installation_id='".$installationID."' 
            AND camp_date_created >= DATE(NOW() - INTERVAL 3 MONTH) 
            AND camp_date_created <= '".$overviewDate."' 
            AND campaign_overview_tbl_cot_id != '".$overviewID."'");
            $row1 = mysqli_fetch_assoc($result1);
            return $row1['mycount'];
        }*/
        return '0';
    }
    
    /**
     * This uses previously created functions to decide which report IDs to remove.
     * @param INT $reportOverviewID
     * @param DATETIME $createdDate
     * @param BOOLEAN $orange
     * @param BOOLEAN $red
     * @return boolean TRUE
     */
    public function removeInstallationFromCurrentCampaign($reportOverviewID, $createdDate, $orange, $red) {
        $array = $this->getAllInstallationsByCampaignOverviewID($reportOverviewID);
        
        if (isset($array)) { // installation objects
            foreach($array as $object) {
                $result = $this->checkInstallationForPreviousCampaignInclusion($object->installationID, $reportOverviewID, $createdDate);
                
                if (($orange === '1') && ($result === '1')) {
                    // delete all with the number 1.
                    $delete = $this->deleteInstallationFromCampaign($reportOverviewID, $object->installationID);
                }
                if (($red === '1') && ($result > '1')) {
                    // delete all with 2 or more.
                    $delete = $this->deleteInstallationFromCampaign($reportOverviewID, $object->installationID);
                }
            }
        }
        
        return true;
    }
    
    /**
     * deleteInstallationFromCampaign, deletes an installation from a campaign.
     * @param INT $reportOverviewID
     * @param INT $installationID
     * @return boolean
     */
    private function deleteInstallationFromCampaign($reportOverviewID, $installationID) {
        $result = "DELETE FROM campaign_tbl "
                . "WHERE campaign_overview_tbl_cot_id='".$reportOverviewID ."' "
                . "AND camp_installation_id='".$installationID."'";
        
        if (!mysqli_query(Database::$connection, $result)) {
            $logger = fopen("./logs/createReport.txt", "a");
            fwrite($logger, " deleteInstallationFromCampaign: Report Overview ID".$reportOverviewID.", Inst ID ".$installationID." ".mysqli_error(Database::$connection). "\r\n");
            fclose($logger);
            return false;
        } else {
            return true;
        }
    }
    
    
    /**
     * This will return a set list of installations created at the time of a report creation event
     * @param INT $reportOverviewID
     * @return \Installation
     */
    public function getAllInstallationsByReportOverviewID($reportOverviewID) {
        $installatioSQLConstructor = new InstallationSQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM installations_tbl WHERE inst_id IN "
                . "(SELECT camp_installation_id FROM campaign_tbl "
                . "WHERE campaign_overview_tbl_cot_id = '".$reportOverviewID."')");
        
        while ($row = mysqli_fetch_assoc($result)) {
            
            $array[$row['inst_installation_name']] = $installatioSQLConstructor->createInstallation($row);
        }
        
        if (isset($array)) {
            return $array;
        } else {
            $logger = fopen("./logs/createReport.txt", "a");
            fwrite($logger, " getAllInstallationsByReportOverviewID: Report Overview ID: ".$reportOverviewID.", ".mysqli_error(Database::$connection). "\r\n");
            fclose($logger);
            return null;
        }
    }
    
}
