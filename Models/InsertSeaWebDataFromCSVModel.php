<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/Installation.php");
//include_once ("Models/DB.php");
include_once("Models/Database.php");

/**
 * Description of InsertSeaWebDataFromCSVModel
 *
 * @author Archie
 */
class InsertSeaWebDataFromCSVModel {

    private $dbColumnName;
    private $newColumnNameArray;
    private $replace;

    /**
     * createTemporaryTable
     * @param Array $columnNameArray
     */
    private function createTemporaryTable($columnNameArray) {

        $typeString = " inst_type VARCHAR(10), ";


        /*
         * Create a temporary table to contain the CSV data in the same order as the CSV.
         */
        $create_table_query = "CREATE TEMPORARY TABLE temporary_table (temp_id SERIAL, ";
        // Go through the column names array
        foreach ($columnNameArray as $p => $q) {
            $p = str_replace($this->replace, "_", $p); // Use the REGEX to replace bad characters with underscore

            $create_table_query .= " " . addslashes($p) . " VARCHAR(255), "; // This adds to the string that creates the table
            $this->dbColumnName .= " " . addslashes($p) . ", "; // This does similar for a select

            $key = str_replace($this->replace, "_", $p); // This shouldnt technically be needed but the code complained without it.
            $this->newColumnNameArray[$key] = $q; // create a new array with the new CSV column names
        }
        $create_table_query .= "$typeString PRIMARY KEY(temp_id))"; // set the primary key of the table


        /*
         * mysqli_query(Database::$connection, ) runs a sql query, putting ! in front of it says "if the query fails"
         */
        if (!mysqli_query(Database::$connection, $create_table_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't create table : " . $create_table_query . " \r\n" . mysqli_error(Database::$connection) . "");
        }
    }

    /**
     * loadFile
     * @param STRING $fileName
     * @param BOOLEAN $headers
     * @param STRING $type
     * @param STRING $dbColumnName
     */
    private function loadFile($fileName, $headers) {


        // Add the CSV data to the temp table.
        // Ignore the first line in the file because these are column headers.
        if ($headers == 1) {
            $ignoreFirstLine = "IGNORE 1 LINES ";
        } else {
            $ignoreFirstLine = "";
        }


        $typeString = "SET inst_type = 'MARINE'";


        $new_query_string = substr($this->dbColumnName, 0, -2); // removes the last character from the string (its a comma that shouldnt be there)
        // Load the data from the csv into the temporary table, using the column names generated on the verify page.
        $insert_temp_query = "LOAD DATA LOCAL INFILE '" . $fileName . "'
                INTO TABLE temporary_table
                CHARACTER SET 'latin1'
                COLUMNS TERMINATED BY ','
                ENCLOSED BY '\"' LINES TERMINATED BY '\r\n'
                $ignoreFirstLine ($new_query_string) $typeString";


        // DATABASE query generator. This say, if mysqli_query fails then write the first line. otherwise write the second.
        if (!mysqli_query(Database::$connection, $insert_temp_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't insert records into temporary table : " . $new_query_string . "*** " . mysqli_error(Database::$connection));
        }
    }

    /**
     * cleanTempData
     * @param type $fileName
     * @param type $columnNameArray
     * @param type $headers
     * @param type $type
     * @param type $dbColumnName
     * @param type $newColumnNameArray
     */
    private function cleanTempData() {
        $cleanQuery = "UPDATE temporary_table SET ";
        // clean out leading and trailing whitespaces 
        foreach ($this->newColumnNameArray as $key => $name) {
            $key = str_replace($this->replace, "_", $key);
            $cleanQuery .= " " . $key . "= TRIM(" . addslashes($key) . "), ";
        }
        $cleanedQuery = substr($cleanQuery, 0, -2);
        mysqli_query(Database::$connection, $cleanedQuery);



        // Do a quick count of the new records, This is currently used for debug, but may stay in the code.
        $quickCount = mysqli_query(Database::$connection, "SELECT count(*) FROM temporary_table");
        $countQuick = mysqli_fetch_assoc($quickCount);
        print "<p>Temp table Count: " . $countQuick['count(*)'] . ". Before duplicates are removed</p>";
    }

    /**
     * createCustomers
     * @param type $columnNameArray
     */
    private function createCustomers() {
        // customer name- its important that it is neither a duplicate or blank.
        // The database has a unique attribute attached to customer name.
        // Check all records inserted into the temporary table, if they are blank, assign a unique ID.
        // find everywhere there is no customer name created
        // array_search looks for the KEY in the array where the VALUE is equal to the array_search criteria

        $customerName = "";
        $seawebOWCode = "";

        if (array_search('cust_name', $this->newColumnNameArray) != "") {
            $customerName = ", " . array_search('cust_name', $this->newColumnNameArray) . " AS cust_name ";
        }

        if (array_search('cust_seaweb_name', $this->newColumnNameArray) != "") {
            $seawebOWCode = ", " . array_search('cust_seaweb_name', $this->newColumnNameArray) . " AS cust_seaweb_name ";
        }


        $emptyCustomer = "SELECT temp_id " . $customerName . " " . $seawebOWCode . " "
                . "FROM temporary_table WHERE " . array_search('cust_name', $this->newColumnNameArray) . " = ''";

        $emptyCustomerQuery = mysqli_query(Database::$connection, $emptyCustomer);


        while ($emptyCustomerQueryRow = mysqli_fetch_assoc($emptyCustomerQuery)) {
            // It does not exist in the DB so give it a unique name
            $updateEmptyName = "UPDATE temporary_table SET " . array_search('cust_name', $this->newColumnNameArray) . " = "
                    . "'" . uniqid("UNKNOWN.") . "' WHERE temp_id = '" . $emptyCustomerQueryRow['temp_id'] . "'";

            if (!mysqli_query(Database::$connection, $updateEmptyName)) {
                debugWriter("loadInstallationDataLogs.txt", "Cannot update empty customer name " . mysqli_error(Database::$connection));
            }
        }



        // start SQL statement to get all customer details.
        $customerQuery = "SELECT " . array_search('cust_name', $this->newColumnNameArray) . " AS cust_name ";
        $custOfficeName = "";
        $custAddress = "";
        $custCity = "";
        $custState = "";
        $custCountry = "";
        $custPostcode = "";

        foreach ($this->newColumnNameArray as $key => $name) {
            $key = str_replace($this->replace, "_", $key);
            if ($name == 'cust_telephone') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cust_fax') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cust_website') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cadt_office_name') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
                $custOfficeName = ", cadt_office_name";
            }
            if ($name == 'cadt_address') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
                $custAddress = ", cadt_address";
            }
            if ($name == 'cadt_city') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
                $custCity = ", cadt_city";
            }
            if ($name == 'cadt_state') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
                $custState = ", cadt_state";
            }
            if ($name == 'cadt_country') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
                $custCountry = ", cadt_country";
            }
            if ($name == 'cadt_postcode') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
                $custPostcode = ", cadt_postcode";
            }
            if ($name == 'cust_type') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cust_seaweb_code') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cust_parent_company_seaweb_code') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cust_parent_company_nationality_of_control') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cust_company_status') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cadt_full_address') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cust_company_full_name') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
        }

        $customerQuery .= " FROM temporary_table GROUP BY cust_seaweb_code, cust_name $custOfficeName $custAddress $custCity $custState $custCountry $custPostcode";

        $customerInsert = mysqli_query(Database::$connection, $customerQuery);

        $newCustomerCount = 0;
        $newAddressCount = 0;

        while ($customerQueryRow = mysqli_fetch_assoc($customerInsert)) {
            // Check the customer_tbl for entries that match
            $checkCustomerTablequery = mysqli_query(Database::$connection, "SELECT count(cust_seaweb_code) FROM customer_tbl WHERE "
                    . "cust_seaweb_code = '" . addslashes($customerQueryRow['cust_seaweb_code']) . "'");

            $checkCustomerTable = mysqli_fetch_assoc($checkCustomerTablequery);
            $customerID = "";

            // If there is no match because the number of entries is less than 1, then proceed.
            if ($checkCustomerTable['count(cust_seaweb_code)'] < 1) {
                /*
                 * Create a new entry.
                 */
                $insertCustomer = "INSERT INTO customer_tbl (cust_name, cust_telephone, cust_fax,"
                        . "cust_website, cust_account_manager, cust_tbh_id, "
                        . "cust_tbh_account_code, cust_ld_id, cust_ld_account_code, cust_header_notes, "
                        . "cust_seaweb_code, cust_parent_company_seaweb_code, "
                        . "cust_parent_company_nationality_of_control, cust_company_status, cust_company_full_name) VALUES ("
                        . "'" . addslashes((isset($customerQueryRow['cust_name']) ? $customerQueryRow['cust_name'] : "")) . "',"
                        . "'" . addslashes((isset($customerQueryRow['cust_telephone']) ? $customerQueryRow['cust_telephone'] : "")) . "', "
                        . "'" . addslashes((isset($customerQueryRow['cust_fax']) ? $customerQueryRow['cust_fax'] : "")) . "', "
                        . "'" . addslashes((isset($customerQueryRow['cust_website']) ? $customerQueryRow['cust_website'] : "")) . "', "
                        . "NULL, "
                        . "'0', "
                        . "'0', "
                        . "'0', "
                        . "'0', "
                        . "'', "
                        . "'" . addslashes((isset($customerQueryRow['cust_seaweb_code']) ? $customerQueryRow['cust_seaweb_code'] : "")) . "', "
                        . "'" . addslashes((isset($customerQueryRow['cust_parent_company_seaweb_code']) ? $customerQueryRow['cust_parent_company_seaweb_code'] : "0")) . "', "
                        . "'" . addslashes((isset($customerQueryRow['cust_parent_company_nationality_of_control']) ? $customerQueryRow['cust_parent_company_nationality_of_control'] : "")) . "', "
                        . "'" . addslashes((isset($customerQueryRow['cust_company_status']) ? $customerQueryRow['cust_company_status'] : "")) . "', "
                        . "'" . trim(addslashes((isset($customerQueryRow['cust_company_full_name']) ? $customerQueryRow['cust_company_full_name'] : ""))) . "')";

                if (!mysqli_query(Database::$connection, $insertCustomer)) {
                    debugWriter("loadInstallationDataLogs.txt", "Cannot Insert Customer : " . mysqli_error(Database::$connection));
                } else {
                    $newCustomerCount++;
                }
                $customerID = mysqli_insert_id(Database::$connection);

                $customerType = '1';
                if (isset($customerQueryRow['cust_type'])) {

                    $selectStatus = mysqli_query(Database::$connection, "SELECT cslt_id FROM customer_status_lookup_tbl "
                            . "WHERE cslt_status = '" . $customerQueryRow['cust_type'] . "'");
                    $statusRow = mysqli_fetch_assoc($selectStatus);
                    $customerType = $statusRow['cslt_id'];
                }
                // Status is held in a separate table because there can be 1 or many
                $insertCustomerStatus = "INSERT INTO customer_status_tbl "
                        . "(customer_tbl_cust_id, customer_status_lookup_tbl_cslt_id) "
                        . "VALUES "
                        . "('" . $customerID . "', "
                        . "'" . $customerType . "')";
                if (!mysqli_query(Database::$connection, $insertCustomerStatus)) {
                    debugWriter("loadInstallationDataLogs.txt", "Cannot Insert Customer Status : " . mysqli_error(Database::$connection));
                }

                if ($customerQueryRow['cadt_country'] == "China, People's Republic of") {
                    $customerQueryRow['cadt_country'] = "China";
                }
                
                if (($customerQueryRow['cadt_country'] == "Eire") || ($customerQueryRow['cadt_country'] == "Republic of Ireland") || 
                        ($customerQueryRow['cadt_country'] == "Irish Republic") || ($customerQueryRow['cadt_country'] == "Ireland")) {
                    $customerQueryRow['cadt_country'] = "Eire / Republic of Ireland";
                }

                /*
                 * Create an entry into the address table.
                 */
                $defaultAddress = 0;

                $selectDefault = mysqli_query(Database::$connection, "SELECT count(cadt_id) FROM customer_address_tbl "
                        . "WHERE cadt_default_address = '1' AND customer_tbl_cust_id = '$customerID'");
                $defaultRow = mysqli_fetch_assoc($selectDefault);
                if ($defaultRow['count(cadt_id)'] < 1) {
                    $defaultAddress = 1;
                }


                $insertAddress = "INSERT INTO customer_address_tbl (cadt_office_name, cadt_address, "
                        . "cadt_city, cadt_state, cadt_country, cadt_postcode, cadt_address_verified, "
                        . "customer_tbl_cust_id, cadt_archived, cadt_default_address, cadt_full_address) "
                        . "VALUES ("
                        . "'" . addslashes((isset($customerQueryRow['cadt_office_name']) ? $customerQueryRow['cadt_office_name'] : "Office")) . "', "
                        . "'" . addslashes((isset($customerQueryRow['cadt_address']) ? $customerQueryRow['cadt_address'] : "")) . "', "
                        . "'" . addslashes((isset($customerQueryRow['cadt_city']) ? $customerQueryRow['cadt_city'] : "")) . "', "
                        . "'" . addslashes((isset($customerQueryRow['cadt_state']) ? $customerQueryRow['cadt_state'] : "")) . "', "
                        . "'" . addslashes((isset($customerQueryRow['cadt_country']) ? $customerQueryRow['cadt_country'] : "")) . "', "
                        . "'" . addslashes((isset($customerQueryRow['cadt_postcode']) ? $customerQueryRow['cadt_postcode'] : "")) . "', "
                        . "'0', "
                        . "'" . $customerID . "', "
                        . "'0', "
                        . "'" . $defaultAddress . "', "
                        . "'" . addslashes((isset($customerQueryRow['cadt_full_address']) ? $customerQueryRow['cadt_full_address'] : "")) . "')";

                if (!mysqli_query(Database::$connection, $insertAddress)) {
                    debugWriter("loadInstallationDataLogs.txt", "Cannot Insert Customer Address : " . mysqli_error(Database::$connection));
                } else {
                    $newAddressCount++;
                }
            } else {
                // Get the customer ID.
                $getCustomerID = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl WHERE "
                        . "cust_name = '" . addslashes($customerQueryRow['cust_name']) . "'");

                $getCustomerIDRow = mysqli_fetch_assoc($getCustomerID);
                $customerID = $getCustomerIDRow['cust_id'];
                // Get a count of all addresses currently attached to that customer ID
                $customerAddress = (isset($customerQueryRow['cadt_address']) ? $customerQueryRow['cadt_address'] : ""); // This is a Ternary Logic statement it reads as (statement ? true : false). if the statement is true do true, else do false.
                $customerCity = (isset($customerQueryRow['cadt_city']) ? $customerQueryRow['cadt_city'] : "");
                $customerState = (isset($customerQueryRow['cadt_state']) ? $customerQueryRow['cadt_state'] : "");
                $customerCountry = (isset($customerQueryRow['cadt_country']) ? $customerQueryRow['cadt_country'] : "");
                $customerPostcode = (isset($customerQueryRow['cadt_postcode']) ? $customerQueryRow['cadt_postcode'] : "");

                // So the customer exists, but the address might be new. Check for that.
                $checkAddress = mysqli_query(Database::$connection, "SELECT count(cadt_id) FROM customer_address_tbl "
                        . "WHERE customer_tbl_cust_id = '" . $customerID . "' AND "
                        . "cadt_address = '" . addslashes($customerAddress) . "' AND "
                        . "cadt_city = '" . addslashes($customerCity) . "' AND "
                        . "cadt_state = '" . addslashes($customerState) . "' AND "
                        . "cadt_country = '" . addslashes($customerCountry) . "' AND "
                        . "cadt_postcode = '" . addslashes($customerPostcode) . "'");

                $checkAddressRow = mysqli_fetch_assoc($checkAddress);


                if ($checkAddressRow['count(cadt_id)'] < 1) {
                    //  debugWriter("debug.txt", "address not found");
                    $insertAddress = "INSERT INTO customer_address_tbl (cadt_office_name, cadt_address, "
                            . "cadt_city, cadt_state, cadt_country, cadt_postcode, cadt_address_verified, "
                            . "customer_tbl_cust_id, cadt_archived, cadt_default_address, cadt_full_address) "
                            . "VALUES ("
                            . "'" . addslashes((isset($customerQueryRow['cadt_office_name']) ? $customerQueryRow['cadt_office_name'] : "Office")) . "', "
                            . "'" . addslashes((isset($customerQueryRow['cadt_address']) ? $customerQueryRow['cadt_address'] : "")) . "', "
                            . "'" . addslashes((isset($customerQueryRow['cadt_city']) ? $customerQueryRow['cadt_city'] : "")) . "', "
                            . "'" . addslashes((isset($customerQueryRow['cadt_state']) ? $customerQueryRow['cadt_state'] : "")) . "', "
                            . "'" . addslashes((isset($customerQueryRow['cadt_country']) ? $customerQueryRow['cadt_country'] : "")) . "', "
                            . "'" . addslashes((isset($customerQueryRow['cadt_postcode']) ? $customerQueryRow['cadt_postcode'] : "")) . "', "
                            . "'0', "
                            . "'" . $customerID . "', "
                            . "'0', "
                            . "'0', "
                            . "'" . addslashes((isset($customerQueryRow['cadt_full_address']) ? $customerQueryRow['cadt_full_address'] : "")) . "')";

                    if (!mysqli_query(Database::$connection, $insertAddress)) {
                        debugWriter("loadInstallationDataLogs.txt", "Cannot Insert Customer Address : " . mysqli_error(Database::$connection));
                    } else {
                        $newAddressCount++;
                    }
                }


                // now check if the status matches and if not add a new status.
                $customerType = "1";
                if (isset($customerQueryRow['cust_type'])) {
                    $selectStatus = mysqli_query(Database::$connection, "SELECT cslt_id FROM customer_status_lookup_tbl "
                            . "WHERE cslt_status = '" . $customerQueryRow['cust_type'] . "'");
                    $statusRow = mysqli_fetch_assoc($selectStatus);
                    $customerType = $statusRow['cslt_id'];
                }



                $checkCustomerStatusInfo = mysqli_query(Database::$connection, "SELECT count(cst_id) FROM customer_status_tbl "
                        . "WHERE customer_tbl_cust_id = '" . $customerID . "' "
                        . "AND customer_status_lookup_tbl_cslt_id = '" . $customerType . "'");

                $checkCustomerStatusInfoRow = mysqli_fetch_assoc($checkCustomerStatusInfo);

                if ($checkCustomerStatusInfoRow['count(cst_id)'] == 0) {
                    //print "add to manual table";
                    $insertNewStatus = "INSERT INTO customer_status_tbl (customer_tbl_cust_id, "
                            . "customer_status_lookup_tbl_cslt_id) "
                            . "VALUES ('" . $customerID . "', '" . $customerType . "')";

                    if (!mysqli_query(Database::$connection, $insertNewStatus)) {
                        debugWriter("debug.txt", "COULD NOT ADD NEW CUSTOMER STATUS TBL ENTRY" . mysqli_error(Database::$connection));
                    }
                }
            }
        }

        print "<p>Added " . $newCustomerCount . " new customers</p>";
        print "<p>Added " . $newAddressCount . " new addresses</p>";
    }

    /**
     * createContacts
     */
    private function createContacts() {
        // Create the select statement
        $contactsString = "SELECT " . array_search('cust_name', $this->newColumnNameArray) . " AS cust_name ";


        foreach ($this->newColumnNameArray as $key => $name) {
            $key = str_replace($this->replace, "_", $key);
            // Find all the relevant columns (up to three contacts is possible in uploadInstallationsTest.php)
            // the contact details have a _1, _2 or _3 after each name which is why I use substr()
            if ($name == 'ict_salutation') {
                $contactsString .= ", " . addslashes($key) . " AS '" . $name . "' "; // AS works to create an alias for the column name, to make it easier to handle.
            }
            if ($name == 'ict_first_name') {
                $contactsString .= ", " . addslashes($key) . " AS '" . $name . "' "; // AS works to create an alias for the column name, to make it easier to handle.
            }
            if ($name == 'ict_surname') {
                $contactsString .= ", " . addslashes($key) . " AS '" . $name . "' "; // AS works to create an alias for the column name, to make it easier to handle.
            }
            if ($name == 'ict_job_title') {
                $contactsString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'ict_email') {
                $contactsString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'ict_number') {
                $contactsString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'ict_mobile_number') {
                $contactsString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'ict_info_source') {
                $contactsString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cust_seaweb_code') {
                $contactsString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
        }
        $contactsString .= " FROM temporary_table";

        $contactsQuery = mysqli_query(Database::$connection, $contactsString); // get the records from the temporary table.
        // Start count of customers added
        $contactCount = 0;

        // Loop through the resultant records and perform the same actions on each row.
        while ($contactsQueryRow = mysqli_fetch_assoc($contactsQuery)) {

            $customerID = ""; // empty customer ID
            $customerAddressID = NULL;
            foreach ($contactsQueryRow as $k => $r) {

                if ($k == "cust_seaweb_code") {

                    // Get the customers ID
                    $getCustomerIDQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl WHERE "
                            . "cust_seaweb_code = '" . $r . "'");

                    $customerIDRow = mysqli_fetch_assoc($getCustomerIDQuery);
                    $customerID = $customerIDRow['cust_id'];
                }


                if ($customerID != "") {
                    $addressQuery = mysqli_query(Database::$connection, "SELECT cadt_id FROM customer_address_tbl "
                            . "WHERE customer_tbl_cust_id = '$customerID' AND cadt_default_address = '1'");
                    $row = mysqli_fetch_assoc($addressQuery);
                    $customerAddressID = $row['cadt_id'];

                    // check for existing customer contact records that match the new one to be inserted.
                    // Also check the history date to see when it was added.
                    $checkContacts = mysqli_query(Database::$connection, "SELECT count(*) FROM customer_contacts_tbl "
                            . "WHERE customer_tbl_cust_id = '" . $customerID . "' "
                            . "AND ict_email = '" . $contactsQueryRow['ict_email'] . "' ");

                    $checkContactsResultRow = mysqli_fetch_assoc($checkContacts);

                    if ($checkContactsResultRow['count(*)'] < 1) {

                        // create an insert statement.
                        $sqlString = "INSERT INTO customer_contacts_tbl (ict_email, "
                                . "ict_inactive, ict_contact_validated, customer_tbl_cust_id, customer_address_tbl_cadt_id, ict_info_source) "
                                . "VALUES ('" . $contactsQueryRow['ict_email'] . "', "
                                . "'0', '0', '" . $customerID . "', '" . $customerAddressID . "', 'SeaWeb')";

                        if (!mysqli_query(Database::$connection, $sqlString)) {
                            debugWriter("debug.txt", "INSERT customer_contacts_tbl FAILED : " . $sqlString . " " . mysqli_error(Database::$connection));
                        } else {
                            $contactCount++;
                        }
                    }
                }
            }
        }
        print "<p>Added " . $contactCount . " new contacts<p/>";
    }

    /**
     * createInstallations
     */
    private function createInstallations() {
        $installationString = "SELECT inst_type, " . array_search('inst_installation_name', $this->newColumnNameArray) . " AS inst_installation_name ";


        foreach ($this->newColumnNameArray as $key => $name) {
            $key = str_replace($this->replace, "_", $key);

            if ($name == 'inst_installation') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_imo') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'shipmanager_code') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_built_date') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_lead_ship_in_series_by_imo') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_flag_name') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'installation_status_tbl_stat_id') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'reg_owner_code') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_technical_manager_code') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'gb_owner_company_code') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_original_source') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_original_source_date') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_photo_link') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_hull_number') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_hull_type') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_year_built') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_deadweight') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_length_overall') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_beam_extreme') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_beam_moulded') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_built_date') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_draught') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_gross_tonnage') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_length') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_propeller_type') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_propulsion_unit_count') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_ship_builder') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_teu') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_build_year') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_yard_built') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_ship_type_level_4') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_classification_society') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
        }
        $installationString .= ", temp_id FROM temporary_table";

        $installationQuery = mysqli_query(Database::$connection, $installationString); // get the records from the temporary table.
        // Start count of installations
        $installationCount = 0;
        $tempID = '';
        // Loop through the resultant records and perform the same actions on each row.
        while ($installationQueryRow = mysqli_fetch_assoc($installationQuery)) {

            if (isset($installationQueryRow['temp_id'])) {
                $tempID = $installationQueryRow['temp_id'];
            }

            // Get cust_id for the owner parent / ship manager / technical manager and group manager
            $shipManagerQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl "
                    . "WHERE cust_seaweb_code = '" . $installationQueryRow['shipmanager_code'] . "'");
            $shipManagerRow = mysqli_fetch_assoc($shipManagerQuery);
            $shipManagerID = $shipManagerRow['cust_id'];

            $shipManagerAddressQuery = mysqli_query(Database::$connection, "SELECT cadt_id FROM customer_address_tbl WHERE "
                    . "customer_tbl_cust_id = '" . $shipManagerID . "' AND cadt_default_address = '1'");

            $shipManagerAddressRow = mysqli_fetch_assoc($shipManagerAddressQuery);
            $shipManagerAddressID = $shipManagerAddressRow['cadt_id'];

            $technicalManagerQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl "
                    . "WHERE cust_seaweb_code = '" . $installationQueryRow['inst_technical_manager_code'] . "'");
            $technicalManagerRow = mysqli_fetch_assoc($technicalManagerQuery);
            $technicalManagerID = $technicalManagerRow['cust_id'];


            $groupOwnerID = "";
            if (isset($installationQueryRow['gb_owner_company_code'])) {
                $groupOwnerQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl "
                        . "WHERE cust_seaweb_code = '" . $installationQueryRow['gb_owner_company_code'] . "'");
                $groupOwnerRow = mysqli_fetch_assoc($groupOwnerQuery);
                $groupOwnerID = $groupOwnerRow['cust_id'];
            }

            $status = "5";
            if (isset($installationQueryRow['installation_status_tbl_stat_id'])) {
                // this value needs to be taken from the loop-up table.
                $statusQuery = mysqli_query(Database::$connection, "SELECT stat_id FROM installation_status_tbl WHERE stat_type = 'MARINE'
                                        AND MATCH (stat_status_description)
                                        AGAINST ('" . addslashes($installationQueryRow['installation_status_tbl_stat_id']) . "' IN NATURAL LANGUAGE MODE)");

                $statusRow = mysqli_fetch_assoc($statusQuery);

                if (isset($statusRow['stat_id'])) {
                    $status = $statusRow['stat_id'];
                }
            }

            $shipTypeLevel4 = "";
            if (isset($installationQueryRow['inst_ship_type_level_4'])) {
                $shipTypeLevel4 = $installationQueryRow['inst_ship_type_level_4'];
            }
            $classificationSociety = "";
            if (isset($installationQueryRow['inst_classification_society'])) {
                $classificationSociety = $installationQueryRow['inst_classification_society'];
            }


            $dateOfBuild = date("Y-m-d H:i:s", time());
            if (isset($installationQueryRow['inst_built_date'])) {
                $year = substr($installationQueryRow['inst_built_date'], 0, 4);
                $month = substr($installationQueryRow['inst_built_date'], 4, 2);
                if (($month == "00") || (intval($month) > 12)) {
                    $month = "01";
                }
                $dateOfBuild = $year . "-" . $month . "-01 00:00:00";
            }

            $installationObject = new Installation("", $installationQueryRow['inst_type'], $installationQueryRow['inst_installation'], $installationQueryRow['inst_installation_name'], $installationQueryRow['inst_imo'], NULL, $technicalManagerID, $groupOwnerID, $status, "SeaWeb", date("Y-m-d H:i:s", time()), "", "", "", $installationQueryRow['inst_yard_built'], "", "", "", "", $dateOfBuild, "", "", "", $installationQueryRow['inst_propeller_type'], $installationQueryRow['inst_propulsion_unit_count'], $installationQueryRow['inst_ship_builder'], "", NULL, 0, "", $shipManagerAddressID, $installationQueryRow['inst_flag_name'], $installationQueryRow['inst_lead_ship_in_series_by_imo'], $shipTypeLevel4, $classificationSociety);
//" . addslashes($installationObject->operatingCompany) != '' ? addslashes($installationObject->operatingCompany) : '0' . "
            $insert = "INSERT INTO installations_tbl (
                    inst_type, inst_installation, inst_installation_name, 
                    inst_imo,  installation_status_tbl_stat_id, inst_owner_parent_company, 
                    inst_technical_manager_company, inst_group_owner, inst_original_source, 
                    inst_original_source_date, inst_photo_link, inst_hull_number, 
                    inst_hull_type, inst_yard_built, inst_deadweight, inst_length_overall, 
                    inst_beam_extreme, inst_beam_moulded, inst_built_date, inst_draught, 
                    inst_gross_tonnage, inst_length, inst_propeller_type, inst_propulsion_unit_count, 
                    inst_ship_builder, inst_teu, inst_build_year, inst_archive, inst_archive_reason, 
                    customer_address_tbl_cadt_id, inst_flag_name, inst_lead_ship_in_series_by_imo, 
                    inst_ship_type_level_4, inst_classification_society)
                        VALUES(
                            '" . addslashes($installationObject->type) . "', 
                            '" . addslashes($installationObject->installation) . "', 
                            '" . addslashes($installationObject->installationName) . "', 
                            '" . addslashes($installationObject->imo) . "', 
                            '" . addslashes($installationObject->status) . "', 
                            '" . addslashes(isset($installationObject->operatingCompany) ? $installationObject->operatingCompany : '0') . "',
                            '" . addslashes($installationObject->technicalManager) . "',  
                            '" . addslashes($installationObject->groupOwner) . "', 
                            '" . addslashes($installationObject->originalSource) . "', 
                            '" . addslashes($installationObject->originalSourceDate) . "', 
                            '" . addslashes($installationObject->photoLink) . "', 
                            '" . addslashes($installationObject->hullNumber) . "', "
                    . "'" . addslashes($installationObject->hullType) . "', "
                    . "'" . addslashes($installationObject->yardBuilt) . "', "
                    . "'" . addslashes($installationObject->deadWeight) . "', "
                    . "'" . addslashes($installationObject->lengthOverall) . "', "
                    . "'" . addslashes($installationObject->beamExtreme) . "', "
                    . "'" . addslashes($installationObject->beamMoulded) . "', "
                    . "'" . addslashes($installationObject->builtDate) . "', "
                    . "'" . addslashes($installationObject->draught) . "', "
                    . "'" . addslashes($installationObject->grossTonnage) . "', "
                    . "'" . addslashes($installationObject->length) . "', "
                    . "'" . addslashes($installationObject->propellerType) . "', "
                    . "'" . addslashes($installationObject->propulsionUnitCount) . "', "
                    . "'" . addslashes($installationObject->shipBuilder) . "', "
                    . "'" . addslashes($installationObject->teu) . "', "
                    . "'" . addslashes($installationObject->buildYear) . "',"
                    . "'0', "
                    . "'',"
                    . "'" . $installationObject->customerAddressID . "', "
                    . "'" . addslashes($installationObject->flagName) . "', "
                    . "'" . $installationObject->leadShipByIMO . "', "
                    . "'" . addslashes($installationObject->shipTypeLevel4) . "', "
                    . "'" . addslashes($installationObject->classificationSociety) . "')";


            if (!(mysqli_query(Database::$connection, $insert))) {
                debugWriter("debug.txt", "createNewInstallationObject Failed $insert " . mysqli_error(Database::$connection) . "\r\n");
            } else {
                // add an entry into the linking table.
                $newInstallationID = mysqli_insert_id(Database::$connection);
                $insert = "INSERT INTO customer_tbl_has_installations_tbl "
                        . "(customer_tbl_cust_id, installations_tbl_inst_id) VALUES "
                        . "('" . $shipManagerID . "', '" . $newInstallationID . "')";
                if (!(mysqli_query(Database::$connection, $insert))) {
                    debugWriter("debug.txt", "createNewCustomerInstallationObject Failed (second phase) $insert " . mysqli_error(Database::$connection) . "\r\n");
                } else {
                    $installationCount++;
                }
            }
        }
        print "<p>Added " . $installationCount . " new installations</p>";
    }

    /**
     * createMainEngines
     */
    private function createMainEngines() {

        /*
         * Get all the data for the engines from the temporary table
         */
        $mainEngineString = "SELECT " . array_search('main_imo', $this->newColumnNameArray) . " AS main_imo ";


        foreach ($this->newColumnNameArray as $key => $name) {
            $key = str_replace($this->replace, "_", $key);
            // Find all the relevant columns (up to three contacts is possible in uploadInstallationsTest.php)

            $mainEngineString .= ", " . addslashes($key) . " AS '" . $name . "' ";
        }

        $mainEngineString .= ", temp_id FROM temporary_table";

        $mainEngineQuery = mysqli_query(Database::$connection, $mainEngineString); // get the records from the temporary table.
        // Start count of installations
        $mainCount = 0;
        $productCount = 0;

        // Loop through the resultant records and perform the same actions on each row.
        while ($mainEngineQueryRow = mysqli_fetch_assoc($mainEngineQuery)) {

            $versionID = "";
            $seriesID = "";
            $sourceDesignerID = ""; // this is the original designer ID as found from the seaweb file, just in case there is no direct series match and it is found in the alias table.
            // MAN-B&W is a pain, rather than having to change the differences in the LAND or MARINE files I just clean it here.
            if ($mainEngineQueryRow['dlt_designer_description'] == "MAN-B&W") {
                $mainEngineQueryRow['dlt_designer_description'] = str_replace("-", " ", $mainEngineQueryRow['dlt_designer_description']);
            }
            if ($mainEngineQueryRow['dlt_designer_description'] == "") {
                $mainEngineQueryRow['dlt_designer_description'] = "Unknown";
            }
            // search designer table for a match.
            $designerQueryString = "SELECT dlt_id FROM designer_lookup_tbl WHERE "
                    . "dlt_designer_description = '" . addslashes($mainEngineQueryRow['dlt_designer_description']) . "'";

            $designerQuery = mysqli_query(Database::$connection, $designerQueryString);
            $designerRow = mysqli_fetch_assoc($designerQuery);

            $designerID = $designerRow['dlt_id'];





            // there was no designer clean the designer name and try again.
            if ($designerID == "") {

                $cleanedDesigner = str_replace($this->replace, "", $mainEngineQueryRow['dlt_designer_description']);

                $designerQueryString2 = "SELECT dlt_id FROM designer_lookup_tbl WHERE "
                        . "dlt_designer_description = '" . $cleanedDesigner . "'";
                $designerQuery2 = mysqli_query(Database::$connection, $designerQueryString2);
                $designerRow2 = mysqli_fetch_assoc($designerQuery2);

                $designerID = $designerRow2['dlt_id'];
            }



            // if the model is empty then mark it as not specified.
            if (($mainEngineQueryRow['iet_model_description'] == "") || (substr($mainEngineQueryRow['iet_model_description'], 0, 2) == "..")) {
                $mainEngineQueryRow['iet_model_description'] = "Not Specified";
            }

            // something has been found for the designer
            if ($designerID != "") { // could still be a brand new designer. SEE } ELSE { BELOW
                $sourceDesignerID = $designerID; // this is set so that there is a historic record
                
                // First we need to check the original designers series available for matches, because Wartsila / stork/ sulzer and nohab have cross over issues.
                //USE THIS
                
                /*"SELECT mlt_id, mlt_series FROM series_lookup_tbl AS s  
                WHERE s.designer_lookup_tbl_dlt_id IN (SELECT designer_lookup_tbl_dlt_id FROM alias_tbl WHERE ali_designer_id = '263') 
                AND  '6RT-flex50'  LIKE CONCAT ('%',mlt_series,'%')"*/
                
                // find out if the source designer exists in the alias table.
                /*$aliasMatchString = "SELECT * FROM alias_tbl WHERE ali_designer_id = '" . $sourceDesignerID . "'";
                $aliasMatch = mysqli_query(Database::$connection, $aliasMatchString);
                $aliasRow = mysqli_fetch_assoc($aliasMatch);

                if (isset($aliasRow['designer_lookup_tbl_dlt_id'])) {
                    // the alias is now the designer.
                    $designerID = $aliasRow['designer_lookup_tbl_dlt_id'];
                }*/

                if ($mainEngineQueryRow['iet_model_description'] != 'Not Specified') {
                // find any series that match the engine designer. There are aliasses for designers so check these as well.
                    $seriesMatchString = "SELECT mlt_id, mlt_series FROM series_lookup_tbl AS s  
                    WHERE s.designer_lookup_tbl_dlt_id IN (SELECT designer_lookup_tbl_dlt_id FROM alias_tbl WHERE ali_designer_id = '$designerID') 
                    AND '" . addslashes($mainEngineQueryRow['iet_model_description']) . "' LIKE CONCAT ('%',mlt_series,'%')";
                } else {
                    $seriesMatchString = "SELECT mlt_id, mlt_series FROM series_lookup_tbl AS s  
                    WHERE s.designer_lookup_tbl_dlt_id  = '$designerID'
                    AND mlt_series = 'Not Specified' ";
                }


                $seriesMatch = mysqli_query(Database::$connection, $seriesMatchString);

                // going to check the percentage of match for each series so we get the right one.
                $greatestMatchSeriesID = 0;
                $greatestPercentage = 0;
                while ($seriesRow = mysqli_fetch_assoc($seriesMatch)) {
                    // this works out the percentage match
                    similar_text(addslashes($mainEngineQueryRow['iet_model_description']), $seriesRow['mlt_series'], $percent);
                    //debugWriter("debug.txt", $mainEngineQueryRow['iet_model_description']." ".$seriesRow['mlt_series']." ".$percent);
                    // mark the best match here.
                    if ($percent >= $greatestPercentage) {
                        $greatestPercentage = $percent;
                        $greatestMatchSeriesID = $seriesRow['mlt_id'];
                    }
                }

                // check if any match was found, if not return to the original designer and get their models
                if ($greatestMatchSeriesID == 0) {
                    $designerID = $sourceDesignerID;
                    $seriesMatchString = "SELECT mlt_id, mlt_series FROM series_lookup_tbl AS s  
                    WHERE s.designer_lookup_tbl_dlt_id = '$designerID' 
                    AND 
                    '" . addslashes($mainEngineQueryRow['iet_model_description']) . "' "
                            . "LIKE CONCAT ('%',mlt_series,'%')";

                    $seriesMatch = mysqli_query(Database::$connection, $seriesMatchString);

                    // going to check the percentage of match for each series so we get the right one.
                    $greatestMatchSeriesID = 0;
                    $greatestPercentage = 0;
                    while ($seriesRow1 = mysqli_fetch_assoc($seriesMatch)) {

                        similar_text(addslashes($mainEngineQueryRow['iet_model_description']), $seriesRow1['mlt_series'], $percent);

                        // mark the best match here.
                        if ($percent >= $greatestPercentage) {
                            $greatestPercentage = $percent;
                            $greatestMatchSeriesID = $seriesRow1['mlt_id'];
                        }
                    }
                }

                
                // We have some series IDs and we need to check which version to use.
                // Look at the version table to see if there is a match for the cylinder count and config for that match.
                if ($greatestMatchSeriesID != 0) {
                    $versionQuery = mysqli_query(Database::$connection, "SELECT vlt_id FROM version_lookup_tbl WHERE "
                            . "model_lookup_tbl_mlt_id = '" . $greatestMatchSeriesID . "' AND "
                            . "vlt_cylinder_count = '" . $mainEngineQueryRow['vlt_cylinder_count'] . "' "
                            . "AND vlt_cylinder_configuration = '" . $mainEngineQueryRow['vlt_cylinder_configuration'] . "'");

                    $versionRow = mysqli_fetch_assoc($versionQuery);
                    $versionID = $versionRow['vlt_id'];

                    // a version ID has been chosen and it is not blank so set the series ID to whichever
                    // one caused the version to be found.
                    if (isset($versionID) && ($versionID != "")) {
                        $seriesID = $greatestMatchSeriesID;
                    } else { // no version ID was found. Do I have enough information to make a new one?
                        // check if the cylinder count and config are present. If they are then we can make a new version.
                        if (($mainEngineQueryRow['vlt_cylinder_count'] != '0') && ($mainEngineQueryRow['vlt_cylinder_configuration'] != '0')) {
                            $versionID = $this->createNewVersion($greatestMatchSeriesID, $mainEngineQueryRow['vlt_cylinder_count'], $mainEngineQueryRow['vlt_cylinder_configuration']);
                            if (isset($versionID) && $versionID != "") {
                                $seriesID = $greatestMatchSeriesID;
                            }
                        } else { // couldnt make a new record out of the count and config so look for an N/A version. 
                            // If this doesnt exist then please make one.
                            $versionQuery = mysqli_query(Database::$connection, "SELECT vlt_id FROM version_lookup_tbl WHERE "
                                    . "model_lookup_tbl_mlt_id = '" . $greatestMatchSeriesID . "' AND "
                                    . "vlt_cylinder_count = '0' "
                                    . "AND vlt_cylinder_configuration = 'A'");

                            $versionRow = mysqli_fetch_assoc($versionQuery);
                            $versionID = $versionRow['vlt_id'];

                            if (isset($versionID) && ($versionID != "")) {
                                $seriesID = $greatestMatchSeriesID;
                            } else {

                                $versionID = $this->createNewVersion($greatestMatchSeriesID, "", "");
                                if (isset($versionID) && $versionID != "") {
                                    $seriesID = $greatestMatchSeriesID;
                                }
                            }
                        }
                    }
                } else {
                    
                    
                    // I want to make the new series.
                    // we have the DLT ID
                    $insertSeries = $this->createNewSeries($designerID, addslashes($mainEngineQueryRow['iet_model_description'])); // create a new series

                    if (isset($insertSeries) && $insertSeries != "") { // create a new version
                        $seriesID = $insertSeries;
                        $versionID = $this->createNewVersion($seriesID, $mainEngineQueryRow['vlt_cylinder_count'], $mainEngineQueryRow['vlt_cylinder_configuration']);
                    } else {

                    // No series ID was found for the given model
                    // find the generic series ID.
                    $seriesMatchTwo = mysqli_query(Database::$connection, "SELECT mlt_id FROM series_lookup_tbl "
                            . "WHERE designer_lookup_tbl_dlt_id = '$designerID' "
                            . "AND mlt_series = 'Not Specified'");


                    $seriesRowTwo = mysqli_fetch_assoc($seriesMatchTwo);

                    // hold this in reserve, try and make one first.
                    if (isset($seriesRowTwo) && $seriesRowTwo['mlt_id'] != "") {

                        if (($mainEngineQueryRow['vlt_cylinder_count'] != '0') && ($mainEngineQueryRow['vlt_cylinder_configuration'] != '0')) {
                            $versionQueryOne = mysqli_query(Database::$connection, "SELECT * FROM version_lookup_tbl WHERE "
                                    . "model_lookup_tbl_mlt_id = '" . $seriesRowTwo['mlt_id'] . "' AND "
                                    . "vlt_cylinder_count = '" . $mainEngineQueryRow['vlt_cylinder_count'] . "' "
                                    . "AND vlt_cylinder_configuration = '" . $mainEngineQueryRow['vlt_cylinder_configuration'] . "'");

                            $versionRowOne = mysqli_fetch_assoc($versionQueryOne);
                            $versionID = $versionRowOne['vlt_id'];

                            if (empty($versionID)) { // make one because its important to have the values.
                                $versionID = $this->createNewVersion($seriesRowTwo['mlt_id'], $mainEngineQueryRow['vlt_cylinder_count'], $mainEngineQueryRow['vlt_cylinder_configuration']);
                                if (isset($versionID) && $versionID != "") {
                                    $seriesID = $seriesRowTwo['mlt_id'];
                                }
                            }
                        } else {

                            $versionQueryOne = mysqli_query(Database::$connection, "SELECT vlt_id FROM version_lookup_tbl WHERE "
                                    . "model_lookup_tbl_mlt_id = '" . $seriesRowTwo['mlt_id'] . "' AND "
                                    . "vlt_cylinder_count = '0' "
                                    . "AND vlt_cylinder_configuration = 'A'");

                            $versionRowOne = mysqli_fetch_assoc($versionQueryOne);
                            $versionID = $versionRowOne['vlt_id'];


                            $seriesID = $seriesRowTwo['mlt_id'];
                        }
                    }


                    if ($versionID == "" && $seriesID != "") {

                        $versionID = $this->createNewVersion($seriesID, $mainEngineQueryRow['vlt_cylinder_count'], $mainEngineQueryRow['vlt_cylinder_configuration']);
                    } else if ($versionID == "" && $seriesID == "") {
                        $insertSeries = $this->createNewSeries($designerID, "Not Specified"); // create a new series

                        if (isset($insertSeries) && $insertSeries != "") { // create a new version
                            $seriesID = $insertSeries;
                            $versionID = $this->createNewVersion($seriesID, $mainEngineQueryRow['vlt_cylinder_count'], $mainEngineQueryRow['vlt_cylinder_configuration']);
                        }
                    }
                    }
                }
            } else { // else there is no designer and a new one needs adding.
                $insertDesigner = $this->createNewDesigner(addslashes($mainEngineQueryRow['dlt_designer_description']));

                if (isset($insertDesigner) && $insertDesigner != "") {
                    $designerID = $insertDesigner; // designer created
                    $sourceDesignerID = $insertDesigner;
                    $insertSeries = $this->createNewSeries($designerID, addslashes($mainEngineQueryRow['iet_model_description']));

                    if (isset($insertSeries) && $insertSeries != "") {
                        $seriesID = $insertSeries;
                        $versionID = $this->createNewVersion($seriesID, $mainEngineQueryRow['vlt_cylinder_count'], $mainEngineQueryRow['vlt_cylinder_configuration']);
                    }
                }
            }

            // insert a product

            $installationQuery = mysqli_query(Database::$connection, "SELECT inst_id, customer_address_tbl_cadt_id, "
                    . "inst_installation_name FROM installations_tbl "
                    . "WHERE inst_imo = '" . $mainEngineQueryRow['main_imo'] . "'");

            $instRow = mysqli_fetch_assoc($installationQuery);
            $installationID = $instRow['inst_id'];

            $customerAddressID = $instRow['customer_address_tbl_cadt_id'];

            $countProductsQuery = mysqli_query(Database::$connection, "SELECT count(*) FROM product_tbl WHERE installations_tbl_inst_id = '$installationID'");
            $countProdRow = mysqli_fetch_assoc($countProductsQuery);
            $countProducts = $countProdRow['count(*)'];

            $installationUnitName = $instRow['inst_installation_name'] . " IC " . ($countProducts + 1);

            $customerIDQuery = mysqli_query(Database::$connection, "SELECT customer_tbl_cust_id FROM customer_address_tbl WHERE "
                    . "cadt_id = '$customerAddressID'");
            $custRow = mysqli_fetch_assoc($customerIDQuery);
            $customerID = $custRow['customer_tbl_cust_id'];

            $insertProduct = "INSERT INTO product_tbl (prod_enquiry_only_flag, customer_tbl_cust_id, "
                    . "installations_tbl_inst_id, version_lookup_tbl_vlt_id, prod_unit_name, prod_source_alias_id) "
                    . "VALUES ("
                    . "'0', "
                    . "'" . addslashes($customerID) . "', "
                    . "'" . addslashes($installationID) . "', "
                    . "'" . addslashes($versionID) . "', "
                    . "'" . addslashes($installationUnitName) . "', "
                    . "'" . addslashes($sourceDesignerID) . "')";


            if (!(mysqli_query(Database::$connection, $insertProduct))) {
                debugWriter("debug.txt", "create new product Failed " . mysqli_error(Database::$connection) . "\r\n");
            } else {
                $productCount++;
                $productID = mysqli_insert_id(Database::$connection);

                $insertEngine = "INSERT INTO installation_engine_tbl (iet_model_description, "
                        . "iet_main_aux, iet_engine_verified, iet_output_unit_of_measure, "
                        . "iet_unit_name, iet_bore_size, iet_stroke, "
                        . "iet_enquiry_only, load_priority_lookup_tbl_load_id, product_tbl_prod_id, "
                        . "iet_position_if_main, iet_engine_builder) VALUES ("
                        . "'" . addslashes($mainEngineQueryRow['iet_model_description']) . "', "
                        . "'Main', "
                        . "'0', "
                        . "'MW', "
                        . "'" . addslashes($installationUnitName) . "', "
                        . "'" . addslashes($mainEngineQueryRow['iet_bore_size']) . "', "
                        . "'" . addslashes($mainEngineQueryRow['iet_stroke']) . "', "
                        . "'0', "
                        . "'3', "
                        . "'$productID', "
                        . "'" . addslashes($mainEngineQueryRow['iet_position_if_main']) . "', "
                        . "'" . addslashes($mainEngineQueryRow['iet_engine_builder']) . "')";

                if (!(mysqli_query(Database::$connection, $insertEngine))) {
                    debugWriter("debug.txt", "create new engine Failed " . mysqli_error(Database::$connection) . "\r\n");
                } else {
                    $mainCount++;
                }
            }
        }
        print "<p>Added " . $productCount . " new main products</p>";
        print "<p>Added " . $mainCount . " new main engines</p>";
    }

    /**
     * createNewDesigner REUSABLE CODE
     * @param type $designerDescription
     * @return type
     */
    private function createNewDesigner($designerDescription) {
        if ($designerDescription != "") {
            $insertDesigner = "INSERT INTO designer_lookup_tbl (dlt_designer_description) "
                    . "VALUES ('" . addslashes($designerDescription) . "')";

            if (!(mysqli_query(Database::$connection, $insertDesigner))) {
                debugWriter("debug.txt", "create new designer Failed " . mysqli_error(Database::$connection) . "\r\n");
                return null;
            } else {
                $designerID = mysqli_insert_id(Database::$connection); // designer created
                return $designerID;
            }
        } else {
            debugWriter("debug.txt", "create new designer Failed : Designer was empty\r\n");
            return null;
        }
    }

    /**
     * createNewSeries
     * @param type $designerID
     * @param type $modelDescription
     * @return type
     */
    private function createNewSeries($designerID, $modelDescription) {
        $seriesName = "Not Specified";
        if ($modelDescription != "") {
            $seriesName = $modelDescription;
        }
        // create the series
        $insertSeries = "INSERT INTO series_lookup_tbl (mlt_series, "
                . "designer_lookup_tbl_dlt_id, type_of_product_lookup_tbl_toplt_id) VALUES "
                . "('" . addslashes($seriesName) . "', '" . $designerID . "', '1')";


        if (!mysqli_query(Database::$connection, $insertSeries)) {
            debugWriter("debug.txt", "could not add new series ID " . mysqli_error(Database::$connection));
            print "<span style='color:purple;'>Error creating a new 'Not Specified' series record for designer ID " . $designerID . "</span><br/>";
            return null;
        } else {
            $seriesID = mysqli_insert_id(Database::$connection);
            return $seriesID;
        }
    }

    /**
     * createNewVersion
     * @param type $seriesID
     * @param type $cylCount
     * @param type $cylConfiguration
     */
    private function createNewVersion($seriesID, $cylCount, $cylConfiguration) {
        $versionDescription = 'N/A';
        $cylinderCount = '0';
        $configuration = 'A';

        if (($cylCount != "") && ($cylCount != "0")) {
            $cylinderCount = $cylCount;
        }
        if (($cylConfiguration != "") && ($cylConfiguration != "0")) {
            $configuration = $cylConfiguration;
        }
        if (($cylCount != "") && ($cylCount != "0") && ($cylConfiguration != "") && ($cylConfiguration != "0")) {
            $versionDescription = $cylCount . "" . $cylConfiguration;
        }

        $insertVersion = "INSERT INTO version_lookup_tbl (vlt_version_description, "
                . "model_lookup_tbl_mlt_id, vlt_cylinder_count, vlt_cylinder_configuration) VALUES "
                . "('" . addslashes($versionDescription) . "', '" . $seriesID . "', '$cylinderCount', '$configuration')";

        if (!mysqli_query(Database::$connection, $insertVersion)) {
            debugWriter("debug.txt", "could not add new version ID - $insertVersion - " . mysqli_error(Database::$connection));
            print "<span style='color:red;'>Error creating a new 'N/A' version record for series ID " . $seriesID . "</span><br/>";
            return null;
        } else {
            $versionID = mysqli_insert_id(Database::$connection);
            return $versionID;
        }
    }

    /**
     * createAuxEngines
     */
    private function createAuxEngines() {

        $mainEngineString = "SELECT " . array_search('aux_imo', $this->newColumnNameArray) . " AS aux_imo ";


        foreach ($this->newColumnNameArray as $key => $name) {
            $key = str_replace($this->replace, "_", $key);
            // Find all the relevant columns (up to three contacts is possible in uploadInstallationsTest.php)

            $mainEngineString .= ", " . addslashes($key) . " AS '" . $name . "' ";
        }

        $mainEngineString .= ", temp_id FROM temporary_table";

        $mainEngineQuery = mysqli_query(Database::$connection, $mainEngineString); // get the records from the temporary table.
        // Start count of installations
        $auxCount = 0;
        $productCount = 0;

        // Loop through the resultant records and perform the same actions on each row.
        while ($mainEngineQueryRow = mysqli_fetch_assoc($mainEngineQuery)) {

            $seriesID = "";
            $versionID = "";
            // MAN-B&W is a pain, rather than having to change the differences in the LAND or MARINE files I just clean it here.
            if (strcasecmp($mainEngineQueryRow['aux_engine_designer'], "MAN-B&W") == 0) {
                $mainEngineQueryRow['aux_engine_designer'] = str_replace("-", " ", $mainEngineQueryRow['aux_engine_designer']);
            }
            if ($mainEngineQueryRow['aux_engine_designer'] == "") {
                $mainEngineQueryRow['aux_engine_designer'] = "Unknown";
            }
            // search designer table for a match.
            $designerQuery = mysqli_query(Database::$connection, "SELECT dlt_id FROM designer_lookup_tbl WHERE "
                    . "dlt_designer_description = '" . addslashes($mainEngineQueryRow['aux_engine_designer']) . "'");

            $designerRow = mysqli_fetch_assoc($designerQuery);

            $designerID = $designerRow['dlt_id'];

            // there was no designer clean the designer name and try again.
            if ($designerID == '') {

                $cleanedDesigner = str_replace($this->replace, "", $mainEngineQueryRow['aux_engine_designer']);

                $designerQuery = mysqli_query(Database::$connection, "SELECT dlt_id FROM designer_lookup_tbl WHERE "
                        . "dlt_designer_description = '" . $cleanedDesigner . "'");

                $designerRow = mysqli_fetch_assoc($designerQuery);

                $designerID = $designerRow['dlt_id'];
            }

            // if the model is empty then mark it as not specified.
            if (($mainEngineQueryRow['aux_engine_model'] == "") || (substr($mainEngineQueryRow['aux_engine_model'], 0, 2) == "..")) {
                $mainEngineQueryRow['aux_engine_model'] = "Not Specified";
            }

            //print "original designer ID (2) ".$designerID."<br/>";

            if ($designerID != "") { // could still be a brand new designer.
                // find any series that match the engine designer. There are aliasses for designers so check these as well.
                $sourceDesignerID = $designerID; // this is set so that there is a historic record
                // find out if the source designer exists in the alias table.
                $aliasMatchString = "SELECT * FROM alias_tbl WHERE ali_designer_id = '" . $sourceDesignerID . "'";
                $aliasMatch = mysqli_query(Database::$connection, $aliasMatchString);
                $aliasRow = mysqli_fetch_assoc($aliasMatch);

                if (isset($aliasRow['designer_lookup_tbl_dlt_id'])) {
                    // the alias is now the designer.
                    $designerID = $aliasRow['designer_lookup_tbl_dlt_id'];
                }

                // find any series that match the engine designer. There are aliasses for designers so check these as well.
                $seriesMatchString = "SELECT mlt_id, mlt_series FROM series_lookup_tbl AS s  
                WHERE s.designer_lookup_tbl_dlt_id = '$designerID' 
                AND 
                '" . addslashes($mainEngineQueryRow['aux_engine_model']) . "' "
                        . "LIKE CONCAT ('%',s.mlt_series,'%')";



                $seriesMatch = mysqli_query(Database::$connection, $seriesMatchString);

                // going to check the percentage of match for each series so we get the right one.
                $greatestMatchSeriesID = 0;
                $greatestPercentage = 0;
                while ($seriesRow = mysqli_fetch_assoc($seriesMatch)) {
                    // this works out the percentage match
                    similar_text(addslashes($mainEngineQueryRow['aux_engine_model']), $seriesRow['mlt_series'], $percent);
                    // mark the best match here.
                    if ($percent >= $greatestPercentage) {
                        $greatestPercentage = $percent;
                        $greatestMatchSeriesID = $seriesRow['mlt_id'];
                    }
                }

                // check if any match was found, if not return to the original designer and get their models
                if ($greatestMatchSeriesID == 0) {

                    $designerID = $sourceDesignerID;
                    $seriesMatchString = "SELECT mlt_id, mlt_series FROM series_lookup_tbl AS s  
                    WHERE s.designer_lookup_tbl_dlt_id = '$designerID' 
                    AND 
                    '" . addslashes($mainEngineQueryRow['aux_engine_model']) . "' "
                            . "LIKE CONCAT ('%',s.mlt_series,'%')";

                    $seriesMatch = mysqli_query(Database::$connection, $seriesMatchString);

                    // going to check the percentage of match for each series so we get the right one.
                    $greatestMatchSeriesID = 0;
                    $greatestPercentage = 0;
                    while ($seriesRow1 = mysqli_fetch_assoc($seriesMatch)) {
                        similar_text(addslashes($mainEngineQueryRow['aux_engine_model']), $seriesRow1['mlt_series'], $percent);

                        // mark the best match here.
                        if ($percent >= $greatestPercentage) {
                            $greatestPercentage = $percent;
                            $greatestMatchSeriesID = $seriesRow1['mlt_id'];
                        }
                    }
                }

                // We have some series IDs and we need to check which version to use.
                // Look at the version table to see if there is a match for the cylinder count and config for that match.
                if ($greatestMatchSeriesID != 0) {
                    $versionQuery = mysqli_query(Database::$connection, "SELECT vlt_id FROM version_lookup_tbl WHERE "
                            . "model_lookup_tbl_mlt_id = '" . $greatestMatchSeriesID . "' AND "
                            . "vlt_cylinder_count = '" . $mainEngineQueryRow['aux_noof_cylinders'] . "' ");

                    $versionRow = mysqli_fetch_assoc($versionQuery);
                    $versionID = $versionRow['vlt_id'];

                    // a version ID has been chosen and it is not blank so set the series ID to whichever
                    // one caused the version to be found.
                    if (isset($versionID) && ($versionID != "")) {
                        $seriesID = $greatestMatchSeriesID;
                    } else { // no version ID was found. Do I have enough information to make a new one?
                        // check if the cylinder count and config are present. If they are then we can make a new version.
                        if (($mainEngineQueryRow['aux_noof_cylinders'] != '0')) {
                            $versionID = $this->createNewVersion($greatestMatchSeriesID, $mainEngineQueryRow['aux_noof_cylinders'], "-");
                            if (isset($versionID) && $versionID != "") {
                                $seriesID = $greatestMatchSeriesID;
                            }
                        } else { // couldnt make a new record out of the count and config so look for an N/A version. 
                            // If this doesnt exist then please make one.
                            $versionQuery = mysqli_query(Database::$connection, "SELECT vlt_id FROM version_lookup_tbl WHERE "
                                    . "model_lookup_tbl_mlt_id = '" . $greatestMatchSeriesID . "' AND "
                                    . "vlt_cylinder_count = '0' "
                                    . "AND vlt_cylinder_configuration = 'A'");

                            $versionRow = mysqli_fetch_assoc($versionQuery);
                            $versionID = $versionRow['vlt_id'];

                            if (isset($versionID) && ($versionID != "")) {
                                $seriesID = $greatestMatchSeriesID;
                            } else {

                                $versionID = $this->createNewVersion($greatestMatchSeriesID, "", "");
                                if (isset($versionID) && $versionID != "") {
                                    $seriesID = $greatestMatchSeriesID;
                                }
                            }
                        }
                    }
                } else {
                    //}
                    // No series ID was found for the given model
                    // find the generic series ID.
                    $seriesMatchTwo = mysqli_query(Database::$connection, "SELECT mlt_id FROM series_lookup_tbl "
                            . "WHERE designer_lookup_tbl_dlt_id = '$designerID' "
                            . "AND mlt_series = 'Not Specified'");


                    $seriesRowTwo = mysqli_fetch_assoc($seriesMatchTwo);


                    if (isset($seriesRowTwo) && $seriesRowTwo['mlt_id'] != "") {

                        if (($mainEngineQueryRow['aux_noof_cylinders'] != '0')) {
                            $versionQueryOne = mysqli_query(Database::$connection, "SELECT * FROM version_lookup_tbl WHERE "
                                    . "model_lookup_tbl_mlt_id = '" . $seriesRowTwo['mlt_id'] . "' AND "
                                    . "vlt_cylinder_count = '" . $mainEngineQueryRow['aux_noof_cylinders'] . "'");

                            $versionRowOne = mysqli_fetch_assoc($versionQueryOne);
                            $versionID = $versionRowOne['vlt_id'];

                            if (empty($versionID)) { // make one because its important to have the values.
                                $versionID = $this->createNewVersion($seriesRowTwo['mlt_id'], $mainEngineQueryRow['aux_noof_cylinders'], "-");
                                if (isset($versionID) && $versionID != "") {
                                    $seriesID = $seriesRowTwo['mlt_id'];
                                }
                            }
                        } else {

                            $versionQueryOne = mysqli_query(Database::$connection, "SELECT vlt_id FROM version_lookup_tbl WHERE "
                                    . "model_lookup_tbl_mlt_id = '" . $seriesRowTwo['mlt_id'] . "' AND "
                                    . "vlt_cylinder_count = '0' "
                                    . "AND vlt_cylinder_configuration = 'A'");

                            $versionRowOne = mysqli_fetch_assoc($versionQueryOne);
                            $versionID = $versionRowOne['vlt_id'];


                            $seriesID = $seriesRowTwo['mlt_id'];
                        }
                    }


                    if ($versionID == "" && $seriesID != "") {

                        $versionID = $this->createNewVersion($seriesID, $mainEngineQueryRow['aux_noof_cylinders'], "-");
                    } else if ($versionID == "" && $seriesID == "") {
                        $insertSeries = $this->createNewSeries($designerID, "Not Specified"); // create a new series

                        if (isset($insertSeries) && $insertSeries != "") { // create a new version
                            $seriesID = $insertSeries;
                            $versionID = $this->createNewVersion($seriesID, $mainEngineQueryRow['aux_noof_cylinders'], "-");
                        }
                    }
                }
            } else { // else there is no designer and a new one needs adding.
                $insertDesigner = $this->createNewDesigner(addslashes($mainEngineQueryRow['aux_engine_designer']));

                if (isset($insertDesigner) && $insertDesigner != "") {
                    $designerID = $insertDesigner; // designer created
                    $sourceDesignerID = $insertDesigner;
                    $insertSeries = $this->createNewSeries($designerID, addslashes($mainEngineQueryRow['aux_engine_model']));

                    if (isset($insertSeries) && $insertSeries != "") {
                        $seriesID = $insertSeries;
                        $versionID = $this->createNewVersion($seriesID, $mainEngineQueryRow['aux_noof_cylinders'], "-");
                    }
                }
            }


            // insert a product

            $installationQuery = mysqli_query(Database::$connection, "SELECT * FROM installations_tbl "
                    . "WHERE inst_imo = '" . $mainEngineQueryRow['aux_imo'] . "'");

            $instRow = mysqli_fetch_assoc($installationQuery);
            $installationID = $instRow['inst_id'];

            $customerAddressID = $instRow['customer_address_tbl_cadt_id'];

            $countProductsQuery = mysqli_query(Database::$connection, "SELECT count(*) FROM product_tbl WHERE installations_tbl_inst_id = '$installationID'");
            $countProdRow = mysqli_fetch_assoc($countProductsQuery);
            $countProducts = $countProdRow['count(*)'];

            $installationUnitName = $instRow['inst_installation_name'] . " IC " . ($countProducts + 1);

            $customerIDQuery = mysqli_query(Database::$connection, "SELECT customer_tbl_cust_id FROM customer_address_tbl WHERE "
                    . "cadt_id = '$customerAddressID'");
            $custRow = mysqli_fetch_assoc($customerIDQuery);
            $customerID = $custRow['customer_tbl_cust_id'];

            $insertProduct = "INSERT INTO product_tbl (prod_serial_number, prod_description, prod_drawing_number, "
                    . "prod_comments, prod_enquiry_only_flag, customer_tbl_cust_id, "
                    . "installations_tbl_inst_id, version_lookup_tbl_vlt_id, prod_unit_name, prod_source_alias_id) "
                    . "VALUES ("
                    . "'', "
                    . "'', "
                    . "'', "
                    . "'', "
                    . "'0', "
                    . "'" . addslashes($customerID) . "', "
                    . "'" . addslashes($installationID) . "', "
                    . "'" . addslashes($versionID) . "', "
                    . "'" . addslashes($installationUnitName) . "',"
                    . "'" . addslashes($sourceDesignerID) . "')";



            if (!(mysqli_query(Database::$connection, $insertProduct))) {
                debugWriter("debug.txt", "create new product Failed " . mysqli_error(Database::$connection) . "\r\n");
            } else {
                $productCount++;
                $productID = mysqli_insert_id(Database::$connection);

                $insertEngine = "INSERT INTO installation_engine_tbl (iet_model_description, "
                        . "iet_main_aux, iet_engine_verified, iet_output, iet_output_unit_of_measure, "
                        . "iet_fuel_type, iet_unit_name, iet_bore_size, iet_stroke, iet_release_date, "
                        . "iet_enquiry_only, load_priority_lookup_tbl_load_id, product_tbl_prod_id, "
                        . "iet_position_if_main, iet_engine_builder) VALUES ("
                        . "'" . addslashes($mainEngineQueryRow['aux_engine_model']) . "', "
                        . "'Aux', "
                        . "'0', "
                        . "'', "
                        . "'MW', "
                        . "'', "
                        . "'" . addslashes($installationUnitName) . "', "
                        . "'" . addslashes($mainEngineQueryRow['aux_bore']) . "', "
                        . "'" . addslashes($mainEngineQueryRow['aux_stroke']) . "', "
                        . "'', "
                        . "'0', "
                        . "'3', "
                        . "'$productID', "
                        . "'" . $mainEngineQueryRow['aux_sequence'] . "', "
                        . "'" . addslashes($mainEngineQueryRow['aux_engine_builder']) . "')";

                if (!(mysqli_query(Database::$connection, $insertEngine))) {
                    debugWriter("debug.txt", "create new engine Failed " . mysqli_error(Database::$connection) . "\r\n");
                } else {
                    $auxCount++;
                }
            }
        }
        print "<p>Added " . $productCount . " new aux products</p>";
        print "<p>Added " . $auxCount . " new aux engines</p>";
    }

    /**
     * createFleetCounts
     */
    private function createFleetCounts() {
        $createFleetCountsString = "SELECT " . array_search('cfct_owcode', $this->newColumnNameArray) . " AS cfct_owcode ";


        foreach ($this->newColumnNameArray as $key => $name) {
            $key = str_replace($this->replace, "_", $key);
            // Find all the relevant columns (up to three contacts is possible in uploadInstallationsTest.php)
            if ($name == 'cfct_cust_name') {
                $createFleetCountsString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cfct_registered_owner_count') {
                $createFleetCountsString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cfct_shipmanager_count') {
                $createFleetCountsString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cfct_operator_count') {
                $createFleetCountsString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cfct_group_owner_count') {
                $createFleetCountsString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cfct_doc_count') {
                $createFleetCountsString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cfct_fleet_size') {
                $createFleetCountsString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cfct_in_service_count') {
                $createFleetCountsString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
        }

        $createFleetCountsString .= ", temp_id FROM temporary_table";

        $createFleetCountsQuery = mysqli_query(Database::$connection, $createFleetCountsString); // get the records from the temporary table.
        // Start count of installations
        $fleetCount = 0;
        // Loop through the resultant records and perform the same actions on each row.
        while ($createFleetCountsQueryRow = mysqli_fetch_assoc($createFleetCountsQuery)) {


            $customerIDQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl WHERE "
                    . "cust_seaweb_code = '" . $createFleetCountsQueryRow['cfct_owcode'] . "'");
            $customerIDRow = mysqli_fetch_assoc($customerIDQuery);
            $customerID = $customerIDRow['cust_id'];


            $insertTZ = "INSERT INTO customer_fleet_counts_tbl ("
                    . "cfct_owcode, customer_tbl_cust_id, cfct_registered_owner_count, "
                    . "cfct_shipmanager_count, cfct_operator_count, cfct_group_owner_count, "
                    . "cfct_doc_count, cfct_fleet_size, cfct_in_service_count) VALUES ("
                    . "'" . addslashes($createFleetCountsQueryRow['cfct_owcode']) . "', "
                    . "'" . addslashes($customerID) . "', "
                    . "'" . addslashes($createFleetCountsQueryRow['cfct_registered_owner_count']) . "', "
                    . "'" . addslashes($createFleetCountsQueryRow['cfct_shipmanager_count']) . "', "
                    . "'" . addslashes($createFleetCountsQueryRow['cfct_operator_count']) . "', "
                    . "'" . addslashes($createFleetCountsQueryRow['cfct_group_owner_count']) . "', "
                    . "'" . addslashes($createFleetCountsQueryRow['cfct_doc_count']) . "', "
                    . "'" . addslashes($createFleetCountsQueryRow['cfct_fleet_size']) . "', "
                    . "'" . addslashes($createFleetCountsQueryRow['cfct_in_service_count']) . "')";

            if (!(mysqli_query(Database::$connection, $insertTZ))) {
                debugWriter("debug.txt", "createNewFLEETObject Failed " . mysqli_error(Database::$connection) . "\r\n");
            } else {
                $fleetCount++;
            }
        }
        print "<p>Added " . $fleetCount . " new fleet counts records</p>";
    }

    /**
     * createSurveyDateHistory
     */
    private function createSurveyDateHistory() {
        $surveyDateString = "SELECT " . array_search('sdht_lrno', $this->newColumnNameArray) . " AS sdht_lrno ";


        foreach ($this->newColumnNameArray as $key => $name) {
            $key = str_replace($this->replace, "_", $key);
            // Find all the relevant columns (up to three contacts is possible in uploadInstallationsTest.php)
            if ($name == 'sdht_special_survey_date') {
                $surveyDateString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
        }

        $surveyDateString .= ", temp_id FROM temporary_table";

        $surveyDateQuery = mysqli_query(Database::$connection, $surveyDateString); // get the records from the temporary table.
        // Start count of installations
        $sdhtCount = 0;
        // Loop through the resultant records and perform the same actions on each row.
        while ($surveyDateQueryRow = mysqli_fetch_assoc($surveyDateQuery)) {


            $installationIDQuery = mysqli_query(Database::$connection, "SELECT inst_id FROM installations_tbl WHERE "
                    . "inst_imo = '" . $surveyDateQueryRow['sdht_lrno'] . "'");
            $installationIDRow = mysqli_fetch_assoc($installationIDQuery);
            $installationID = $installationIDRow['inst_id'];

            $date = '';

            if (($surveyDateQueryRow['sdht_special_survey_date'] != "") && ($surveyDateQueryRow['sdht_special_survey_date'] != 'Not recorded')) {

                $date = $surveyDateQueryRow['sdht_special_survey_date'];
                if (strpos($date, "/") !== false) {
                    $exp = explode("/", $surveyDateQueryRow['sdht_special_survey_date']);
                    $date = $exp[2] . "-" . $exp[1] . "-" . $exp[0];
                }
                if (substr($surveyDateQueryRow['sdht_special_survey_date'], 0, -2) == '00') {
                    $date = substr($surveyDateQueryRow['sdht_special_survey_date'], 0, 8) . "01";
                }
            }
            $insertTZ = "INSERT INTO survey_date_history_tbl (sdht_special_survey_date, "
                    . "installations_tbl_inst_id, sdht_lrno) VALUES ("
                    . "'" . addslashes($date) . "', "
                    . "'" . addslashes($installationID) . "', "
                    . "'" . addslashes($surveyDateQueryRow['sdht_lrno']) . "')";

            if (!(mysqli_query(Database::$connection, $insertTZ))) {
                debugWriter("debug.txt", "createNewSDHTObject Failed " . mysqli_error(Database::$connection) . "\r\n");
            } else {
                $sdhtCount++;
            }
        }
        print "<p>Added " . $sdhtCount . " new special survey date history records</p>";
    }

    /**
     * createSurveyDate
     */
    private function createSurveyDate() {
        $surveyDateString = "SELECT " . array_search('sddt_lrno', $this->newColumnNameArray) . " AS sddt_lrno ";


        foreach ($this->newColumnNameArray as $key => $name) {
            $key = str_replace($this->replace, "_", $key);
            // Find all the relevant columns (up to three contacts is possible in uploadInstallationsTest.php)
            if ($name == 'sddt_special_survey_date') {
                $surveyDateString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
        }

        $surveyDateString .= ", temp_id FROM temporary_table";

        $surveyDateQuery = mysqli_query(Database::$connection, $surveyDateString); // get the records from the temporary table.
        // Start count of installations
        $sddtCount = 0;
        // Loop through the resultant records and perform the same actions on each row.
        while ($surveyDateQueryRow = mysqli_fetch_assoc($surveyDateQuery)) {


            $installationIDQuery = mysqli_query(Database::$connection, "SELECT inst_id FROM installations_tbl WHERE "
                    . "inst_imo = '" . $surveyDateQueryRow['sddt_lrno'] . "'");
            $installationIDRow = mysqli_fetch_assoc($installationIDQuery);
            $installationID = $installationIDRow['inst_id'];

            $date = '';

            if (($surveyDateQueryRow['sddt_special_survey_date'] != "") && ($surveyDateQueryRow['sddt_special_survey_date'] != 'Not recorded')) {
                $date = $surveyDateQueryRow['sddt_special_survey_date'];
                if (strpos($date, "/") !== false) {
                    $exp = explode("/", $surveyDateQueryRow['sddt_special_survey_date']);
                    $date = $exp[2] . "-" . $exp[1] . "-" . $exp[0];
                }
                if (substr($surveyDateQueryRow['sddt_special_survey_date'], 0, -2) == '00') {
                    $date = substr($surveyDateQueryRow['sddt_special_survey_date'], 0, 4) . "-" . substr($surveyDateQueryRow['sddt_special_survey_date'], 5, 2) . "-01";
                }
            }

            $classificationSociety = "";
            if (isset($surveyDateQueryRow['sddt_classification_society'])) {
                $classificationSociety = $surveyDateQueryRow['sddt_classification_society'];
            }
            $insertTZ = "INSERT INTO survey_due_date_tbl (sddt_special_survey_date, "
                    . "installations_tbl_inst_id, sddt_lrno) VALUES ("
                    . "'" . addslashes($date) . "', "
                    . "'" . addslashes($installationID) . "', "
                    . "'" . addslashes($surveyDateQueryRow['sddt_lrno']) . "')";

            //debugWriter("debug.txt", $insertTZ);

            if (!(mysqli_query(Database::$connection, $insertTZ))) {
                debugWriter("debug.txt", "createNewSDDTObject Failed " . mysqli_error(Database::$connection) . "\r\n");
            } else {
                $sddtCount++;
            }
        }
        print "<p>Added " . $sddtCount . " new special survey due date records</p>";
    }

    /**
     * createTradingZone
     */
    private function createTradingZone() {
        $tradingZoneString = "SELECT " . array_search('tzlst_lrno', $this->newColumnNameArray) . " AS tzlst_lrno ";


        foreach ($this->newColumnNameArray as $key => $name) {
            $key = str_replace($this->replace, "_", $key);
            // Find all the relevant columns (up to three contacts is possible in uploadInstallationsTest.php)
            if ($name == 'tzlst_trading_zone') {
                $tradingZoneString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'tzlst_last_seen_date') {
                $tradingZoneString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
        }

        $tradingZoneString .= ", temp_id FROM temporary_table";

        $tradingZoneQuery = mysqli_query(Database::$connection, $tradingZoneString); // get the records from the temporary table.
        // Start count of installations
        $tzCount = 0;
        // Loop through the resultant records and perform the same actions on each row.

        while ($tradingZoneQueryRow = mysqli_fetch_assoc($tradingZoneQuery)) {

            $date = $tradingZoneQueryRow['tzlst_last_seen_date'];
            if (strpos($date, "/") !== false) {
                $time = explode(" ", $tradingZoneQueryRow['tzlst_last_seen_date']);
                $exp = explode("/", $time[0]);
                $date = $exp[2] . "-" . $exp[1] . "-" . $exp[0] . " " . $time[1] . ":00";
            }


            $installationIDQuery = mysqli_query(Database::$connection, "SELECT inst_id FROM installations_tbl WHERE "
                    . "inst_imo = '" . $tradingZoneQueryRow['tzlst_lrno'] . "'");
            $installationIDRow = mysqli_fetch_assoc($installationIDQuery);
            $installationID = $installationIDRow['inst_id'];



            $insertTZ = "INSERT INTO trading_zone_last_seen_tbl (tzlst_trading_zone, tzlst_last_seen_date, "
                    . "installations_tbl_inst_id, tzlst_lrno) VALUES ("
                    . "'" . addslashes($tradingZoneQueryRow['tzlst_trading_zone']) . "', "
                    . "'" . $date . "', "
                    . "'" . addslashes($installationID) . "', "
                    . "'" . addslashes($tradingZoneQueryRow['tzlst_lrno']) . "')";

            if (!(mysqli_query(Database::$connection, $insertTZ))) {
                debugWriter("debug.txt", "createNewTZObject Failed " . mysqli_error(Database::$connection) . "\r\n");
            } else {
                $tzCount++;
            }
        }
        print "<p>Added " . $tzCount . " new trading zone records</p>";
    }

    /**
     * createShipNameHistory
     */
    private function createShipNameHistory() {
        $shipNameHistoryString = "SELECT " . array_search('snht_lrno', $this->newColumnNameArray) . " AS snht_lrno ";


        foreach ($this->newColumnNameArray as $key => $name) {
            $key = str_replace($this->replace, "_", $key);
            // Find all the relevant columns (up to three contacts is possible in uploadInstallationsTest.php)
            if ($name == 'snht_sequence') {
                $shipNameHistoryString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'snht_vessel_name') {
                $shipNameHistoryString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'snht_effective_date') {
                $shipNameHistoryString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
        }

        $shipNameHistoryString .= ", temp_id FROM temporary_table";

        $shipNameHistoryQuery = mysqli_query(Database::$connection, $shipNameHistoryString); // get the records from the temporary table.
        // Start count of installations
        $snhCount = 0;
        // Loop through the resultant records and perform the same actions on each row.
        while ($shipNameHistoryQueryRow = mysqli_fetch_assoc($shipNameHistoryQuery)) {


            $installationIDQuery = mysqli_query(Database::$connection, "SELECT inst_id FROM installations_tbl WHERE "
                    . "inst_imo = '" . $shipNameHistoryQueryRow['snht_lrno'] . "'");
            $installationIDRow = mysqli_fetch_assoc($installationIDQuery);
            $installationID = $installationIDRow['inst_id'];


            $date = '';
            if (($shipNameHistoryQueryRow['snht_effective_date'] != "") && ($shipNameHistoryQueryRow['snht_effective_date'] != 'Not recorded')) {

                if (substr($shipNameHistoryQueryRow['snht_effective_date'], 0, -2) == '00') {
                    $date = substr($shipNameHistoryQueryRow['snht_effective_date'], 0, 4) . "-01-01";
                } else {
                    $date = substr($shipNameHistoryQueryRow['snht_effective_date'], 0, 4) . "-" . substr($shipNameHistoryQueryRow['snht_effective_date'], 5, 2) . "-01";
                }
            }

            $insertTZ = "INSERT INTO ship_name_history_tbl (snht_lrno, snht_sequence, snht_vessel_name, "
                    . "snht_effective_date, installations_tbl_inst_id) VALUES ("
                    . "'" . addslashes($shipNameHistoryQueryRow['snht_lrno']) . "', "
                    . "'" . addslashes($shipNameHistoryQueryRow['snht_sequence']) . "', "
                    . "'" . addslashes($shipNameHistoryQueryRow['snht_vessel_name']) . "', "
                    . "'" . addslashes($date) . "', "
                    . "'" . addslashes($installationID) . "')";

            if (!(mysqli_query(Database::$connection, $insertTZ))) {
                debugWriter("debug.txt", "createNewship name history Object Failed $insertTZ " . mysqli_error(Database::$connection) . "\r\n");
            } else {
                $snhCount++;
            }
        }
        print "<p>Added " . $snhCount . " new ship name history records</p>";
    }

    /**
     * MAIN FUNCTION starts the import process
     * @param type $fileName
     * @param type $columnNameArray
     * @param type $headers
     */
    public function importSeaWebDataFromCSV($fileName, $columnNameArray, $headers, $fileType) {
        $this->dbColumnName = "";
        $this->newColumnNameArray = [];
        $this->replace = ["/", "-", " ", ".", "#"]; // REGEX (Regular Expression) for clearing out these characters
        // create the temporary table.
        $this->createTemporaryTable($columnNameArray);


        $this->loadFile($fileName, $headers);

        $this->cleanTempData();

        if ($fileType == 'companyData') {
            $this->createCustomers();

            $this->createContacts();
        }

        if ($fileType == 'shipData') {
            $this->createInstallations();
        }

        if ($fileType == 'mainEngines') {
            $this->createMainEngines();
        }

        if ($fileType == 'auxEngines') {
            $this->createAuxEngines();
        }

        if ($fileType == 'companyFleetCounts') {
            $this->createFleetCounts();
        }

        if ($fileType == 'surveyDateHistory') {
            $this->createSurveyDateHistory();
        }

        if ($fileType == 'surveyDates') {
            $this->createSurveyDate();
        }

        if ($fileType == 'tradingZone') {
            $this->createTradingZone();
        }

        if ($fileType == 'shipNameHistory') {
            $this->createShipNameHistory();
        }

        /**
         * Drop the temporary table.
         */
        $drop_table = "DROP TABLE IF EXISTS temporary_table";
        if (!mysqli_query(Database::$connection, $drop_table)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't drop table - gravity failure : " . mysqli_error(Database::$connection) . "");
        }
    }

}
