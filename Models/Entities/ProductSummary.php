<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of ProductSummary
 *
 * @author Archie
 */
class ProductSummary {
    public $countOfProducts;
    public $versionID;
    public $versionDescription;
    public $sumOfCylinders;
    public $seriesID;
    public $modelDescription;

    public function __construct($countOfProducts, $versionID, $versionDescription, 
            $sumOfCylinders, $seriesID, $modelDescription) {
        $this->countOfProducts = $countOfProducts;
        $this->versionID = $versionID;
        $this->versionDescription = $versionDescription;
        $this->sumOfCylinders = $sumOfCylinders;
        $this->seriesID = $seriesID;
        $this->modelDescription = $modelDescription;
    }
}
