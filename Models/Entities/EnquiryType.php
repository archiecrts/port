<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of EnquiryType
 *
 * @author alexandra
 */
class EnquiryType {
    public $typeID;
    public $type;
    public $prefix;
    
    public function __construct($typeID, $type, $prefix) {
        $this->typeID = $typeID;
        $this->type = $type;
        $this->prefix = $prefix;
    }
}
