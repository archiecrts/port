<?php

/*
 * Property of S-T Diesel Marine Group.
 */

/**
 * Description of CustomerConversation
 * Customer Conversation Entity contains the content and next step for each
 * conversation a user has with a contact.
 *
 * @author Archie
 */
class CustomerConversation {
    public $conversationID;
    public $contactID;
    public $date;
    public $content;
    public $nextActionDueDate;
    public $actionType;
    public $campaignOverviewID;
    public $replyToID;
    
    /**
     * Constructor for Customer Conversation Entity.
     * @param INT $conversationID
     * @param INT $contactID
     * @param DATE $date
     * @param STRING $content
     * @param DATE $nextActionDueDate
     * @param STRING $actionType
     * @param INT $campaignOverviewID
     * @param INT $replyToID
     */
    public function __construct($conversationID, $contactID, $date, $content, $nextActionDueDate, 
            $actionType, $campaignOverviewID, $replyToID) {
        $this->conversationID = $conversationID;
        $this->contactID = $contactID;
        $this->date = $date;
        $this->content = $content;
        $this->nextActionDueDate = $nextActionDueDate;
        $this->actionType = $actionType;
        $this->campaignOverviewID = $campaignOverviewID;
        $this->replyToID = $replyToID;
    }
    
}
