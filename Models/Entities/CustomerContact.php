<?php

/*
 * Property of S-T Diesel Marine Group.
 */

/**
 * Description of CustomerContact Objects
 * Customer Contact Entity contains a unique ID for each contact, 
 * and its associated Customer ID.
 * 
 * @author Archie
 */
class CustomerContact {
    public $customerContactID;
    public $salutation;
    public $firstName;
    public $surname;
    public $jobTitle;
    public $email;
    public $number;
    public $mobile;
    public $infoSource;
    public $inactive;
    public $contactValidated;
    public $customerID;
    public $customerAddressID;
    
    /**
     * Constructor for Customer Contact Entity.
     * @param INT $customerContactID
     * @param STRING $salutation
     * @param STRING $firstName
     * @param STRING $surname
     * @param STRING $jobTitle
     * @param STRING $email
     * @param STRING $number
     * @param STRING $mobile
     * @param STRING $infoSource
     * @param BOOLEAN $inactive
     * @param BOOLEAN $contactValidated
     * @param INT $customerID
     * @param INT $customerAddressID
     */
    public function __construct($customerContactID, $salutation, $firstName, $surname, $jobTitle, 
                                    $email, $number, $mobile, $infoSource, $inactive,
                                    $contactValidated, $customerID, $customerAddressID) {
        $this->customerContactID = $customerContactID;
        $this->salutation = $salutation;
        $this->firstName = $firstName;
        $this->surname = $surname;
        $this->jobTitle = $jobTitle;
        $this->email = $email;
        $this->number = $number;
        $this->mobile = $mobile;
        $this->infoSource = $infoSource;
        $this->inactive = $inactive;
        $this->contactValidated = $contactValidated;
        $this->customerID = $customerID;
        $this->customerAddressID = $customerAddressID;
    }
}
