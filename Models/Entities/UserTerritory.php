<?php

/*
 * Property of S-T Diesel Marine Group.
 */

/**
 * Description of UserTerritory.
 * User Territory holds a user ID and the territories the user is responsible for.
 *
 * @author Archie
 */
class UserTerritory {
    public $id;
    public $userID;
    public $territory;


    /**
     * Constructor for the User Territory Entity.
     * @param INT $id
     * @param INT $userID
     * @param STRING $territory
     */
    public function __construct($id, $userID, $territory) {
        $this->id = $id;
        $this->userID = $userID;
        $this->territory = $territory;

    }
}
