<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of SupplierParts
 *
 * @author Archie
 */
class SupplierParts {
    public $supPartID;
    public $customerID;
    public $stockLevel;
    public $partCodeID;
    public $partPrice;
    public $datePriceSet;
    public $leadTime;
    
    /**
     * Constructor for SupplierApprovalStatus
     * @param INT $supPartID
     * @param STRING $customerID
     * $stockLevel
     * $partCodeID
     * $partPrice
     * $datePriceSet
     * $leadTime
     */
    public function __construct($supPartID, $customerID, $stockLevel, $partCodeID,
            $partPrice, $datePriceSet, $leadTime) {
        $this->supPartID = $supPartID;
        $this->customerID = $customerID;
        $this->stockLevel = $stockLevel;
        $this->partCodeID = $partCodeID;
        $this->partPrice = $partPrice;
        $this->datePriceSet = $datePriceSet;
        $this->leadTime = $leadTime;
    }
}
