<?php

/*
 * Property of S-T Diesel Marine Group
 */

/**
 * Description of ReportParameters
 * Report Parameters Entity contains the parameters used to create the
 * report or report. This contains the parameter name, its value and its
 * associated report overview ID.
 * 
 * @author Archie
 */
class ReportParameters {
    public $parameter;
    public $value;
    public $reportOverviewID;
    
    /**
     * Constructor for Report Parameters.
     * @param STRING $parameter
     * @param STRING $value
     * @param INT $reportOverviewID
     */
    public function __construct($parameter, $value, $reportOverviewID) {
        $this->parameter = $parameter;
        $this->value = $value;
        $this->reportOverviewID = $reportOverviewID;
    }
}
