<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of UserHistory
 *
 * @author Archie
 */
class UserHistory {
    public $userHistoryID;
    public $itemDate;
    public $itemType;
    public $comment;
    public $userID;
    
    
    /**
     * This provides the user history for the users actions.
     * @param INT $userHistoryID
     * @param DATETIME $itemDate
     * @param ENUM $itemType
     * @param TEXT $comment
     * @param INT $userID
     */
    public function __construct($userHistoryID, $itemDate, 
                                    $itemType, $comment, $userID) 
    {
        $this->userHistoryID = $userHistoryID;
        $this->itemDate = $itemDate;
        $this->itemType = $itemType;
        $this->comment = $comment;
        $this->userID = $userID;
    }
}
