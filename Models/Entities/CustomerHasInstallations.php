<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of CustomerHasInstallations
 *
 * @author Archie
 */
class CustomerHasInstallations {
    public $customerID;
    public $installationID;
    
    /**
     * Constructor.
     * @param type $customerID
     * @param type $installationID
     */
    public function __construct($customerID, $installationID) {
        $this->customerID = $customerID;
        $this->installationID = $installationID;
    }
}
