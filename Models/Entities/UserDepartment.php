<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of UserDepartment
 *
 * @author Archie
 */
class UserDepartment {
    
    public $id;
    public $userDepartment;

    public function __construct($id, $userDepartment) {
        $this->id = $id;
        $this->userDepartment = $userDepartment;
    }
    
}
