<?php

/**
 * Description of EngineModel
 * This is a cut down object (from the installation object. showing only the engine 
 * model. it is used in a DISTINCT query and designed to make the query faster.
 * @author Archie
 */
class EngineModel {
    public $engineModel;

    /**
     * Constructor for Engine Model
     * @param STRING $engineModel
     */
    public function __construct($engineModel) {
        $this->engineModel = $engineModel;
    }
}
