<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of ReportResult
 *
 * @author Archie
 */
class ReportResult {
    
    public $customerID;
    public $installationID; 
    public $customerAddressID; 
    public $installationEngineID; 
    public $type; // location type
    public $installation; // ship type level 2
    public $imo;
    public $installationName; 
    public $surveyDueDate;
    public $customerName; 
    public $city; 
    public $region;
    public $state; 
    public $country; 
    public $area;
    public $continent;
    public $operatingCompany; 
    public $technicalManager; 
    public $tmCity;
    public $tmCountry;
    public $tmArea;
    public $tmContinent;
    public $status;
    public $productType;
    public $engineManufacturer; 
    public $engineSeries;
    public $engineModel; 
    public $engineCount;
    public $mainAux;
    public $buildYear;
    public $source; 
    public $accountManager;
    public $aliasID;
    public $engineReleaseDate;
    
    
    
    /**
     * 
     * @param STRING $locationType
     * @param STRING $installationType
     * @param STRING $installationIMO
     * @param STRING $installationName
     * @param STRING $surveyDueDate
     * @param STRING $customerName
     * @param STRING $customerAddressCity
     * @param STRING $customerAddressState
     * @param STRING $customerAddressCountry
     * @param STRING $customerAddressArea
     * @param STRING $customerAddressContinent
     * @param type $installationOperatingCompany
     * @param type $installationTechnicalManagerCompany
     * @param type $installationStatusID
     * @param STRING $installationEngineManufacturer
     * @param STRING $engineSeries
     * @param STRING $installationEngineModel
     * @param type $engineCount
     * @param STRING $installationEngineMainAux
     * @param STRING $installationSource
     * @param type $customerAccountManager
     * @param type $customerID
     * @param type $installationID
     * @param type $customerAddressID
     * @param type $installationEngineID
     * @param type $buildYear
     * @param STRING $tmCountry
     * @param STRING $tmCity
     * @param STRING $tmArea;
     * @param STRING $tmContinent;
     */
    public function __construct($locationType, $installationType, $installationIMO, 
                    $installationName, $surveyDueDate, $customerName,   
                    $customerAddressCity, $region, $customerAddressState, $customerAddressCountry, 
                    $customerAddressArea, $customerAddressContinent, 
                    $installationOperatingCompany, $installationTechnicalManagerCompany, 
                    $installationStatusID, $productType,
                    $installationEngineManufacturer, $engineSeries, $installationEngineModel,
                    $engineCount, $installationEngineMainAux, 
                    $installationSource, $customerAccountManager,
                    $customerID, $installationID, $customerAddressID, $installationEngineID, 
                    $buildYear, $tmCountry, $tmCity, $tmArea, $tmContinent, $aliasID, $engineReleaseDate) 
    {
        
        $this->customerID = $customerID;
        $this->installationID = $installationID;
        $this->customerAddressID = $customerAddressID;
        $this->installationEngineID = $installationEngineID;
        $this->type = $locationType;
        $this->installation = $installationType;
        $this->imo = $installationIMO;
        $this->installationName = $installationName;
        $this->surveyDueDate = $surveyDueDate;
        $this->customerName = $customerName;
        $this->city = $customerAddressCity;
        $this->region = $region;
        $this->state = $customerAddressState;
        $this->country = $customerAddressCountry;
        $this->area = $customerAddressArea;
        $this->continent = $customerAddressContinent;
        $this->operatingCompany = $installationOperatingCompany;
        $this->technicalManager = $installationTechnicalManagerCompany;
        $this->tmCity = $tmCity;
        $this->tmCountry = $tmCountry;
        $this->tmArea = $tmArea;
        $this->tmContinent = $tmContinent;
        $this->status = $installationStatusID;
        $this->productType = $productType;
        $this->engineManufacturer = $installationEngineManufacturer;
        $this->engineSeries = $engineSeries;
        $this->engineModel = $installationEngineModel;
        $this->engineCount = $engineCount;
        $this->mainAux = $installationEngineMainAux;
        $this->buildYear = $buildYear;
        $this->source = $installationSource;
        $this->accountManager = $customerAccountManager;
        $this->aliasID = $aliasID;
        $this->engineReleaseDate = $engineReleaseDate;
    }
}
