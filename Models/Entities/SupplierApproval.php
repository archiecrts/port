<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of SupplierApproval
 *
 * @author Archie
 */
class SupplierApproval {
    
    public $approvalID;
    public $customerID;
    public $approvedAsSupplier;
    public $approvedAsTrader;
    public $approvedAsAgent;
    public $approvalStatusID;
    public $approvalStatusDateChanged;
    public $reviewDate;
    public $reasonForStatusChange;
    
    /**
     * Constructor for SupplierApproval
     * @param INT $approvalID
     * @param INT $customerID
     * @param BOOLEAN $approvedAsSupplier
     * @param BOOLEAN $approvedAsTrader
     * @param BOOLEAN $approvedAsAgent
     * @param INT $approvalStatusID
     * @param DATE $approvalStatusDateChanged
     * @param DATE $reviewDate
     * @param TEXT $reasonForStatusChange
     */
    public function __construct($approvalID, $customerID, $approvedAsSupplier, $approvedAsTrader,
            $approvedAsAgent, $approvalStatusID, $approvalStatusDateChanged, 
            $reviewDate, $reasonForStatusChange) {
        $this->approvalID = $approvalID;
        $this->customerID = $customerID;
        $this->approvedAsSupplier = $approvedAsSupplier;
        $this->approvedAsTrader = $approvedAsTrader;
        $this->approvedAsAgent = $approvedAsAgent;
        $this->approvalStatusID = $approvalStatusID;
        $this->approvalStatusDateChanged = $approvalStatusDateChanged;
        $this->reviewDate = $reviewDate;
        $this->reasonForStatusChange = $reasonForStatusChange;
    }
    
}
