<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of ShipNameHistory
 *
 * @author Archie
 */
class ShipNameHistory {
    public $shipNameHistoryID;
    public $shipNameHistoryIMO;
    public $shipNameHistorySequence;
    public $shipNameHistoryVesselName;
    public $shipNameHistoryEffectiveDate;
    public $installationID;
    
    
    public function __construct($shipNameHistoryID, $shipNameHistoryIMO, $shipNameHistorySequence, 
            $shipNameHistoryVesselName, $shipNameHistoryEffectiveDate,
            $installationID) {

        $this->shipNameHistoryID = $shipNameHistoryID;
        $this->shipNameHistoryIMO = $shipNameHistoryIMO;
        $this->shipNameHistorySequence = $shipNameHistorySequence;
        $this->shipNameHistoryVesselName = $shipNameHistoryVesselName;
        $this->shipNameHistoryEffectiveDate = $shipNameHistoryEffectiveDate;
        $this->installationID = $installationID;
     }
}
