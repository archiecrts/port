<?php

/*
 * Property of S-T Diesel Marine Group.
 */

/**
 * Description of Country
 * The country entity contains the country name, area and timezone.
 *
 * @author Archie
 */
class Country {
    public $countryName;
    public $geoArea;
    public $continent;
    public $timezone;
    
    /**
     * Constructor for Country
     * @param STRING $countryName
     * @param STRING $geoArea
     * @param STRING $continent
     * @param INT $timezone
     */
    public function __construct($countryName, $geoArea, $continent, $timezone) {
        $this->countryName = $countryName;
        $this->geoArea = $geoArea;
        $this->continent = $continent;
        $this->timezone = $timezone;
    }
}
