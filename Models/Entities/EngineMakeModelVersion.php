<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of EngineMakeModelVersion
 * Engine Make Model Version is an overall description of an engine, It is not associated
 * with a physical DB table but is an amalgamation of three data tables, allowing the 
 * creation of a whole engine and the engine version ID which is used to identify each
 * individual engine configuration.
 *
 * @author Archie
 */
class EngineMakeModelVersion {
    public $engineManufacturer;
    public $engineModel;
    public $engineVersion;
    public $engineVersionID;
    
    /**
     * Constructor of an Engine Make Model Version Entity
     * @param STRING $engineManufacturer
     * @param STRING $engineModel
     * @param STRING $engineVersion
     * @param INT $engineVersionID
     */
    public function __construct($engineManufacturer, $engineModel, $engineVersion, $engineVersionID) {
        $this->engineManufacturer = $engineManufacturer;
        $this->engineModel = $engineModel;
        $this->engineVersion = $engineVersion;
        $this->engineVersionID = $engineVersionID;
    }
}
