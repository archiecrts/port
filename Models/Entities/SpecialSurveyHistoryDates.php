<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of SpecialSurveyHistoryDates
 *
 * @author Archie
 */
class SpecialSurveyHistoryDates {
    public $specialSurveyHistoryID;
    public $specialSurveyHistoryDate;
    public $installationID;
    public $imo;
    
    /**
     * Constructor for special survey history
     * @param type $specialSurveyHistoryID
     * @param type $specialSurveyHistoryDate
     * @param type $installationID
     * @param type $imo
     */
    public function __construct($specialSurveyHistoryID, $specialSurveyHistoryDate, $installationID, $imo) {
        $this->specialSurveyHistoryID = $specialSurveyHistoryID;
        $this->specialSurveyHistoryDate = $specialSurveyHistoryDate;
        $this->installationID = $installationID;
        $this->imo = $imo;
    }
}
