<?php

/*
 * Property of S-T Diesel Marine Group
 */

/**
 * Description of Training.
 * The training entity contains details about each employees training.
 *
 * @author Archie
 */
class Training {
    public $trainingID;
    public $hasBeenReadFlag;
    public $hrConfirmationFlag;
    public $userID;
    public $documentID;
    
    /**
     * Constructor for Training Entity.
     * @param INT $trainingID
     * @param BOOLEAN $hasBeenReadFlag
     * @param BOOLEAN $hrConfirmationFlag
     * @param INT $userID
     * @param INT $documentID
     */
    public function __construct($trainingID, 
            $hasBeenReadFlag, $hrConfirmationFlag, $userID, $documentID) {
        $this->trainingID = $trainingID;
        $this->hasBeenReadFlag = $hasBeenReadFlag;
        $this->hrConfirmationFlag = $hrConfirmationFlag;
        $this->userID = $userID;
        $this->documentID = $documentID;
    }
}
