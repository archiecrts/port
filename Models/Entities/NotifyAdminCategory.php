<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of NotifyAdminCategory
 *
 * @author Archie
 */
class NotifyAdminCategory {
    public $NACategoryID;
    public $category;
    
    
    /**
     * Constructor for Campaign Entity
     * @param INT $NACategoryID
     * @param STRING $category
     */
    public function __construct($NACategoryID, $category) 
    {
        $this->NACategoryID = $NACategoryID;
        $this->category = $category;
        
    }
}
