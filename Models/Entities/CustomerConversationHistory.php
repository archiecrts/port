<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of CustomerConversationHistory
 *
 * @author Archie
 */
class CustomerConversationHistory {
    public $historyID;
    public $status;
    public $conversationID;
    public $userID;
    
    /**
     * Constructor for Customer Conversation History Tbl
     * @param INT $historyID
     * @param STRING $status
     * @param INT $conversationID
     * @param INT $userID
     */
    public function __construct($historyID, $status, $conversationID, $userID) {
        $this->historyID = $historyID;
        $this->status = $status;
        $this->conversationID = $conversationID;
        $this->userID = $userID;
    }
}
