<?php


/**
 * Description of EngineManufacturer
 * This is a cut down object (from the installation object. showing only the engine 
 * manufacturer. it is used in a DISTINCT query and designed to make the query faster.
 * @author Archie
 */
class EngineManufacturer {
    public $engineManufacturer;

    /**
     * Constructor for Engine Manufacturer
     * @param STRING $engineManufacturer
     */
    public function __construct($engineManufacturer) {
        $this->engineManufacturer = $engineManufacturer;
    }
}
