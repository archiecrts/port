<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of VersionLookup
 *
 * @author Archie
 */
class VersionLookup {
    public $versionID;
    public $versionProductDescription;
    public $seriesLookupID;
    public $cylinderCount;
    public $cylinderConfiguration;
    
    /**
     * Constructor for TypeOfProductLookup
     * @param type $toplID
     * @param type $toplProductDescription
     * @param type $seriesLookupID
     * @param type $cylinderCount
     * @param type $cylinderConfiguration
     */
    public function __construct($toplID, $toplProductDescription, $seriesLookupID, $cylinderCount, $cylinderConfiguration) {
        $this->versionID = $toplID;
        $this->versionProductDescription = $toplProductDescription;
        $this->seriesLookupID = $seriesLookupID;
        $this->cylinderCount = $cylinderCount;
        $this->cylinderConfiguration = $cylinderConfiguration;
    }
}
