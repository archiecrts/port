<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of EnquiryEngine
 *
 * @author Archie
 */
class EnquiryEngine {
    public $enqEngineID;
    public $enqEngineTblID;
    public $installationID;
    
    public function __construct($enqEngineID, $enqEngineTblID, $installationID) {
        $this->enqEngineID = $enqEngineID;
        $this->enqEngineTblID = $enqEngineTblID;
        $this->installationID = $installationID;
    }
}
