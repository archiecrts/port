<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of UpdateCustome
 *
 * @author Archie
 */
class UpdateCustomer {
    
    public $tempID;
    public $owcode;
    public $shortCompanyName;
    public $countryName;
    public $townName;
    public $telephone;
    public $email;
    public $website;
    public $company_status;
    public $fullCompanyName;
    public $fullAddress;
    public $customerDuplicate;
    public $addressDuplicate;
    public $contactDuplicate;
    public $customerExists;
    public $addressExists;
    public $contactExists;
    public $customerID;
    public $customerAddressID;
    public $customerContactID;


    /**
     * Constructor for Customer.
     * @param type $tempID
     * @param STRING $owcode
     * @param STRING $shortCompanyName
     * @param STRING $countryName
     * @param STRING $townName
     * @param STRING $telephone
     * @param STRING $email
     * @param STRING $website
     * @param STRING $company_status
     * @param STRING $fullCompanyName
     * @param STRING $fullAddress
     * @param BOOLEAN $customerDuplicate
     * @param BOOLEAN $addressDuplicate
     * @param BOOLEAN $contactDuplicate
     * @param BOOLEAN $customerExists
     * @param BOOLEAN $addressExists
     * @param BOOLEAN $contactExists
     * @param INT $customerID
     * @param INT $customerAddressID
     * @param INT $customerContactID
     */
    public function __construct($tempID, $owcode, $shortCompanyName, $countryName,
            $townName, $telephone, $email, $website, $company_status,
            $fullCompanyName, $fullAddress, $customerDuplicate, $addressDuplicate, 
            $contactDuplicate, $customerExists, $addressExists, $contactExists, 
            $customerID, $customerAddressID, $customerContactID) {
        
        $this->tempID = $tempID;
        $this->owcode = $owcode;
        $this->shortCompanyName = $shortCompanyName;
        $this->countryName = $countryName;
        $this->townName = $townName;
        $this->telephone = $telephone;
        $this->email = $email;
        $this->website = $website;
        $this->company_status = $company_status;
        $this->fullCompanyName = $fullCompanyName;
        $this->fullAddress = $fullAddress;
        $this->customerDuplicate = $customerDuplicate;
        $this->addressDuplicate = $addressDuplicate;
        $this->contactDuplicate = $contactDuplicate;
        $this->customerExists = $customerExists;
        $this->addressExists = $addressExists;
        $this->contactExists = $contactExists;
        $this->customerID = $customerID;
        $this->customerAddressID = $customerAddressID;
        $this->customerContactID = $customerContactID;
    }
}