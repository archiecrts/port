<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of PartDimensions
 *
 * @author Archie
 */
class PartDimensions {
    public $dimID;
    public $dimOuterDiameter;
    public $dimProfileDiameter;
    public $dimLength;
    public $dimDINNumber;
    public $dimHeadType;
    public $dimAngleInDegrees;
    public $dimInnerDiameter;
    
    /**
     * __construct
     * @param type $dimID
     * @param type $dimOuterDiameter
     * @param type $dimProfileDiameter
     * @param type $dimLength
     * @param type $dimDINNumber
     * @param type $dimHeadType
     * @param type $dimAngleInDegrees
     * @param type $dimInnerDiameter
     */
    public function __construct($dimID, $dimOuterDiameter, $dimProfileDiameter, $dimLength,
            $dimDINNumber, $dimHeadType, $dimAngleInDegrees, $dimInnerDiameter) {
        $this->dimID = $dimID;
        $this->dimOuterDiameter = $dimOuterDiameter;
        $this->dimProfileDiameter = $dimProfileDiameter;
        $this->dimLength = $dimLength;
        $this->dimDINNumber = $dimDINNumber;
        $this->dimHeadType = $dimHeadType;
        $this->dimAngleInDegrees = $dimAngleInDegrees;
        $this->dimInnerDiameter = $dimInnerDiameter;
    }
}
