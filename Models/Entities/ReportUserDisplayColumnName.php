<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of reportUserDisplayColumnName
 *
 * @author Archie
 */
class ReportUserDisplayColumnName {
    public $columnNameID;
    public $columnName;

    /**
     * Constructor
     * @param type $columnNameID
     * @param type $columnName
     */
    public function __construct($columnNameID, $columnName) {
        $this->columnNameID = $columnNameID;
        $this->columnName = $columnName;
    }
}