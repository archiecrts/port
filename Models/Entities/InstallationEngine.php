<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of InstallationEngine
 *
 * @author Archie
 */
class InstallationEngine {
    public $installationEngineID;
    public $productID;
    public $modelDescription;
    public $mainOrAux;
    public $engineVerified;
    public $output;
    public $outputUnitOfMeasurement;
    public $fuelType;
    public $unitName;
    public $boreSize;
    public $stroke;
    public $releaseDate;
    public $enquiryOnly;
    public $loadPriorityID;
    public $positionIfMain;
    public $engineBuilder;
    
    /**
     * Constructor for Installation Engine Entity.
     * @param INT $installationEngineID
     * @param STRING $productID
     * @param STRING $modelDescription
     * @param STRING $mainOrAux
     * @param BOOLEAN $engineVerified
     * @param STRING $output
     * @param STRING $outputUnitOfMeasurement
     * @param STRING $fuelType
     * @param INT $boreSize
     * @param INT $stroke
     * @param DATETIME $releaseDate
     * @param BOOLEAN $enquiryOnly
     * @param INT $loadPriorityID
     * @param STRING $positionIfMain
     * @param STRING $engineBuilder
     */
    public function __construct($installationEngineID, $productID, 
            $modelDescription, $mainOrAux, 
            $engineVerified,  $output, $outputUnitOfMeasurement, 
            $fuelType, $boreSize, $stroke,
            $releaseDate, $enquiryOnly, $loadPriorityID, $positionIfMain, $engineBuilder) {
        
        $this->installationEngineID = $installationEngineID;
        $this->productID = $productID;
        $this->modelDescription = $modelDescription;
        $this->mainOrAux = $mainOrAux;
        $this->engineVerified = $engineVerified;
        $this->output = $output;
        $this->outputUnitOfMeasurement = $outputUnitOfMeasurement;
        $this->fuelType = $fuelType;
        $this->boreSize = $boreSize;
        $this->stroke = $stroke;
        $this->releaseDate = $releaseDate;
        $this->enquiryOnly = $enquiryOnly;
        $this->loadPriorityID = $loadPriorityID;
        $this->positionIfMain = $positionIfMain;
        $this->engineBuilder = $engineBuilder;
    }
}
