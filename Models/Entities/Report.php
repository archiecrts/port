<?php

/*
 * Property of S-T Diesel Marine Group
 */

/**
 * Description of Report
 * The report entity contains each individual contact added to a report,
 * The data origin, the response received and whether the user chose to opt out
 * of this type of report.
 *
 * @author Archie
 */
class Report {
    public $reportOverviewID;
    public $contactID;
    public $responseReceived;
    public $optoutReceived;
    public $createdDate;
    
    /**
     * Constructor for Report Entity
     * @param INT $reportOverviewID
     * @param INT $contactID
     * @param BOOLEAN $responseReceived
     * @param BOOLEAN $optoutReceived
     * @param DATETIME $createdDate
     */
    public function __construct($reportOverviewID, $contactID, 
                                    $responseReceived, $optoutReceived, $createdDate) 
    {
        $this->reportOverviewID = $reportOverviewID;
        $this->contactID = $contactID;
        $this->responseReceived = $responseReceived;
        $this->optoutReceived = $optoutReceived;
        $this->createdDate = $createdDate;
    }
}
