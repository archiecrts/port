<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of TypeOfProductLookup
 *
 * @author Archie
 */
class TypeOfProductLookup {
    public $toplID;
    public $toplProductDescription;
    public $toplShortCode;
    
    /**
     * Constructor for TypeOfProductLookup
     * @param INT $topl_id
     * @param STRING $topl_product_description
     * @param String $toplShortCode
     */
    public function __construct($topl_id, $topl_product_description, $toplShortCode) {
        $this->toplID = $topl_id;
        $this->toplProductDescription = $topl_product_description;
        $this->toplShortCode = $toplShortCode;
    }
}
