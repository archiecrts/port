<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of SeriesLookup
 *
 * @author Archie
 */
class SeriesLookup {
    public $seriesID;
    public $seriesDescription;
    public $designerLookupID;
    public $typeOfProductLookupID;
    
    /**
     * Constructor for TypeOfProductLookup
     * @param INT $seriesID
     * @param STRING $seriesDescription
     * @param STRING $designerLookupID
     * @param STRING $typeOfProductLookupID
     */
    public function __construct($seriesID, $seriesDescription, $designerLookupID,
            $typeOfProductLookupID) {
        $this->seriesID = $seriesID;
        $this->seriesDescription = $seriesDescription;
        $this->designerLookupID = $designerLookupID;
        $this->typeOfProductLookupID = $typeOfProductLookupID;
    }
}
