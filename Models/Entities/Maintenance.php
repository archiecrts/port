<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of Maintenance
 *
 * @author Archie
 */
class Maintenance {
    public $maintenanceID;
    public $maintenanceActiveSwitch;
    
    /**
     * Constructor
     * @param INT $maintenanceID
     * @param BOOLEAN $maintenanceActiveSwitch
     */
    public function __construct($maintenanceID, $maintenanceActiveSwitch) {
        $this->maintenanceID = $maintenanceID;
        $this->maintenanceActiveSwitch = $maintenanceActiveSwitch;
    }
}
