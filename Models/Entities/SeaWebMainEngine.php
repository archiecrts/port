<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of SeaWebMainEngine
 *
 * @author Archie
 */
class SeaWebMainEngine {
    public $lrno;
    public $position;
    public $engineDesigner;
    public $engineBuilder;
    public $engineModel;
    public $numberOfCylinders;
    public $bore;
    public $stroke;
   // public $cylinderArrangement;
    public $portID;
    public $installationID;
    
    public function __construct($lrno, $position, $engineDesigner, $engineBuilder, $engineModel, 
            $numberOfCylinders, $bore, $stroke, $portID, $installationID) {
        $this->lrno = $lrno;
        $this->position = $position;
        $this->engineDesigner = $engineDesigner;
        $this->engineBuilder = $engineBuilder;
        $this->engineModel = $engineModel;
        $this->numberOfCylinders = $numberOfCylinders;
        $this->bore = $bore;
        $this->stroke = $stroke;
        //$this->cylinderArrangement = $cylinderArrangement;
        $this->portID = $portID;
        $this->installationID = $installationID;
    }
}
