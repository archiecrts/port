<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of SpecialSurveyDue
 *
 * @author Archie
 */
class SpecialSurveyDue {
    public $specialSurveyDueID;
    public $specialSurveyDueDate;
    public $installationID;
    public $imo;
    
    /**
     * 
     * @param type $specialSurveyDueID
     * @param type $specialSurveyDueDate
     * @param type $installationID
     * @param type $imo
     * @param type $classificationSociety
     */
    public function __construct($specialSurveyDueID, $specialSurveyDueDate, $installationID, $imo) {
        $this->specialSurveyDueID = $specialSurveyDueID;
        $this->specialSurveyDueDate = $specialSurveyDueDate;
        $this->installationID = $installationID;
        $this->imo = $imo;
    }
}
