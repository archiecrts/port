<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of DesignerLookup
 *
 * @author Archie
 */
class DesignerLookup {
    public $designerID;
    public $designerDescription;
    
    /**
     * Constructor for TypeOfProductLookup
     * @param INT $designerID
     * @param STRING $designerDescription
     */
    public function __construct($designerID, $designerDescription) {
        $this->designerID = $designerID;
        $this->designerDescription = $designerDescription;
    }
}
