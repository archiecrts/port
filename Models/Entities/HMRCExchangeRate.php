<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of HMRCExchangeRate
 *
 * @author Archie
 */
class HMRCExchangeRate {
    public $hmrcID;
    public $hmrcCountry;
    public $hmrcCurrency;
    public $hmrcCurrencyCode;
    public $hmrcCurrencyUnitsPerPound;
    public $hmrcStartDate;
    public $hmrcEndDate;
    
    public function __construct($hmrcID, $hmrcCountry, $hmrcCurrency, $hmrcCurrencyCode, 
            $hmrcCurrencyUnitsPerPound, $hmrcStartDate, $hmrcEndDate) {
        $this->hmrcID = $hmrcID;
        $this->hmrcCountry = $hmrcCountry;
        $this->hmrcCurrency = $hmrcCurrency;
        $this->hmrcCurrencyCode = $hmrcCurrencyCode;
        $this->hmrcCurrencyUnitsPerPound = $hmrcCurrencyUnitsPerPound;
        $this->hmrcStartDate = $hmrcStartDate;
        $this->hmrcEndDate = $hmrcEndDate;
    }
}
