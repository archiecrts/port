<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of PartMaterialSpec
 *
 * @author Archie
 */
class PartMaterialSpec {
    public $materialID;
    public $materialCode;
    public $materialDescription;
    public $materialHardness;
    public $dinStandard;
    public $materialSpec;
    
    /**
     * __construct
     * @param type $materialID
     * @param type $materialCode
     * @param type $materialDescription
     * @param type $materialHardness
     * @param type $dinStandard
     * @param type $materialSpec
     */
    public function __construct($materialID, $materialCode,  $materialDescription, $materialHardness,
            $dinStandard, $materialSpec) {
        $this->materialID = $materialID;
        $this->materialCode = $materialCode;
        $this->materialDescription = $materialDescription;
        $this->materialHardness = $materialHardness;
        $this->dinStandard = $dinStandard;
        $this->materialSpec = $materialSpec;
    }
}
