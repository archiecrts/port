<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of UpdateShipData
 *
 * @author Archie
 */
class UpdateShipData {
    
    public $tempID;
    public $lrimo;
    public $shipName;
    public $shipStatus;
    public $shipTypeLevel2;
    public $shipTypeLevel4;
    public $dateOfBuild;
    public $leadShipInSeriesByIMO;
    public $flagName;
    public $classificationSociety;
    public $shipManager;
    public $shipManagerCompanyCode;
    public $technicalManager;
    public $technicalManagerCode;
    public $noofAuxEngines;
    public $noofMainEngines;
    public $noofPropulsionUnits;
    public $propellerType;
    public $shipBuilder;
    public $yardNumber;
    public $shipDuplicate;
    public $shipExists;
    public $installationID;
    
   
    public function __construct($tempID, $lrimo, $shipName, $shipStatus, 
            $shipTypeLevel2, $shipTypeLevel4, $dateOfBuild, $leadShipInSeriesByIMO, 
            $flagName, $classificationSociety, $shipManager, $shipManagerCompanyCode, 
            $technicalManager, $technicalManagerCode, $noofAuxEngines, $noofMainEngines, 
            $noofPropulsionUnits, $propellerType, $shipBuilder, 
            $yardNumber, $shipDuplicate, $shipExists, $installationID) {
        
        $this->tempID = $tempID;
        $this->lrimo = $lrimo;
        $this->shipName = $shipName;
        $this->shipStatus = $shipStatus;
        $this->shipTypeLevel2 = $shipTypeLevel2;
        $this->shipTypeLevel4 = $shipTypeLevel4;
        $this->dateOfBuild = $dateOfBuild;
        $this->leadShipInSeriesByIMO = $leadShipInSeriesByIMO;
        $this->flagName = $flagName;
        $this->classificationSociety = $classificationSociety;
        $this->shipManager = $shipManager;
        $this->shipManagerCompanyCode = $shipManagerCompanyCode;
        $this->technicalManager = $technicalManager;
        $this->technicalManagerCode = $technicalManagerCode;
        $this->noofAuxEngines = $noofAuxEngines;
        $this->noofMainEngines = $noofMainEngines;
        $this->noofPropulsionUnits = $noofPropulsionUnits;
        $this->propellerType = $propellerType;
        $this->shipBuilder = $shipBuilder;
        $this->yardNumber = $yardNumber;
        $this->shipDuplicate = $shipDuplicate;
        $this->shipExists = $shipExists;
        $this->installationID = $installationID;
    }
    
}
