<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of NotifyAdmin
 *
 * @author Archie
 */
class NotifyAdmin {
    public $notifyAdminID;
    public $userID;
    public $dateLogged;
    public $issueDescription;
    public $status;
    public $issueTypeID;
    
    /**
     * Constructor for Campaign Entity
     * @param INT $notifyAdminID
     * @param INT $userID
     * @param DATETIME $dateLogged
     * @param STRING $issueDescription
     * @param INT $issueTypeID
     */
    public function __construct($notifyAdminID, $userID, $dateLogged, $issueDescription, 
                                        $status, $issueTypeID) 
    {
        $this->notifyAdminID = $notifyAdminID;
        $this->userID = $userID;
        $this->dateLogged = $dateLogged;
        $this->issueDescription = $issueDescription;
        $this->status = $status;
        $this->issueTypeID = $issueTypeID;
    }
}
