<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of UserLevel
 *
 * @author Archie
 */
class UserLevel {

    public $id;
    public $userLevel;

    public function __construct($id, $userLevel) {
        $this->id = $id;
        $this->userLevel = $userLevel;
    }

}
