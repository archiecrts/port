<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of DailyExchangeRate
 *
 * @author Archie
 */
class DailyExchangeRate {

    public $dailyEXCID;
    public $dailyEXCCurrencyCode;
    public $dailyEXCCurrencyUnitPerPound;
    public $dailyEXCDateSet;

    public function __construct($dailyEXCID, $dailyEXCCurrencyCode, $dailyEXCCurrencyUnitPerPound, 
            $dailyEXCDateSet) {
        $this->dailyEXCID = $dailyEXCID;
        $this->dailyEXCCurrencyCode = $dailyEXCCurrencyCode;
        $this->dailyEXCCurrencyUnitPerPound = $dailyEXCCurrencyUnitPerPound;
        $this->dailyEXCDateSet = $dailyEXCDateSet;
    }

}
