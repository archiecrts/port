<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of Customer
 *
 * @author Archie
 */
class Customer {
    public $customerID;
    public $customerName;
    public $mainTelephone;
    public $mainFax;
    public $website;
    public $accountManager;
    public $tbhID;
    public $tbhAccountCode;
    public $ldID;
    public $ldAccountCode;
    public $headerNotes;
    public $seawebCode;
    public $parentSeawebCode;
    public $parentNationalityOfControl;
    public $companyStatus;
    public $companyFullName;
    public $navisionID;
    
    /**
     * Constructor for Customer.
     * @param INT $customerID
     * @param STRING $customerName
     * @param STRING $mainTelephone
     * @param STRING $mainFax
     * @param STRING $website
     * @param INT $accountManager
     * @param INT $tbhID
     * @param STRING $tbhAccountCode
     * @param INT $ldID
     * @param STRING $ldAccountCode
     * @param STRING $headerNotes
     * @param STRING $seawebCode
     * @param STRING $parentSeawebCode
     * @param STRING $parentNationalityOfControl
     * @param STRING $companyStatus
     * @param STRING $navisionID
     */
    public function __construct($customerID, $customerName, $mainTelephone, $mainFax,
            $website, $accountManager, $tbhID, $tbhAccountCode, $ldID,
            $ldAccountCode, $headerNotes, $seawebCode, $parentSeawebCode, 
            $parentNationalityOfControl, $companyStatus, $companyFullName, $navisionID) {
        
        $this->customerID = $customerID;
        $this->customerName = $customerName;
        $this->mainTelephone = $mainTelephone;
        $this->mainFax = $mainFax;
        $this->website = $website;
        $this->accountManager = $accountManager;
        $this->tbhID = $tbhID;
        $this->tbhAccountCode = $tbhAccountCode;
        $this->ldID = $ldID;
        $this->ldAccountCode = $ldAccountCode;
        $this->headerNotes = $headerNotes;
        $this->seawebCode = $seawebCode;
        $this->parentSeawebCode = $parentSeawebCode;
        $this->parentNationalityOfControl = $parentNationalityOfControl;
        $this->companyStatus = $companyStatus;
        $this->companyFullName = $companyFullName;
        $this->navisionID = $navisionID;
    }
}
