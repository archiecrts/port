<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of CustomerCredit
 *
 * @author Archie
 */
class CustomerCredit {
    public $creditID;
    public $creditLimit;
    public $accountOnStop;
    public $customerID;
    public $paymentTerms;
    
    /**
     * Constructor for Customer Credit Entity
     * @param INT $creditID
     * @param FLOAT $creditLimit
     * @param BOOLEAN $accountOnStop
     * @param INT $customerID
     * @param INT $paymentTerms
     */
    public function __construct($creditID, $creditLimit, $accountOnStop, 
            $customerID, $paymentTerms) {
        $this->creditID = $creditID;
        $this->creditLimit = $creditLimit;
        $this->accountOnStop = $accountOnStop;
        $this->customerID = $customerID;
        $this->paymentTerms = $paymentTerms;
        
    }
}
