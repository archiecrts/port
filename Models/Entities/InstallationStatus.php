<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of InstallationStatus
 *
 * @author Archie
 */
class InstallationStatus {
    public $statusID;
    public $statusDescription;
    public $type;
   
    /**
     * Constructor
     * @param INT $statusID
     * @param STRING $statusDescription
     * @param STRING $type
     */
    public function __construct($statusID, $statusDescription, $type) {
        $this->statusID = $statusID;
        $this->statusDescription = $statusDescription;
        $this->type = $type;
    }
}
