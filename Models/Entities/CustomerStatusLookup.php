<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of CustomerStatusLookup
 *
 * @author Archie
 */
class CustomerStatusLookup {
    public $custStatusLookupID;
    public $custStatusDescription;
    
    public function __construct($custStatusLookupID, $custStatusDescription) {
        $this->custStatusLookupID = $custStatusLookupID;
        $this->custStatusDescription = $custStatusDescription;
    }
}
