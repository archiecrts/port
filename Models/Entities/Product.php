<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of Product
 *
 * @author Archie
 */
class Product {
    public $productID;
    public $productSerialNumber;
    public $productDescription;
    public $productDrawingNumber;
    public $productComment;
    public $productEnquiryOnlyFlag;
    public $customerID;
    public $installationID;
    public $versionLookupID;
    public $productUnitName;
    public $sourceAliasID;
    
    
    /**
     * Constructor for Installation Engine Entity.
     * 
     */
    public function __construct($productID, $productSerialNumber, 
            $productDescription, $productDrawingNumber, $productComment, 
            $productEnquiryOnlyFlag,  $customerID, $installationID, 
            $versionLookupID, $productUnitName, $sourceAliasID) {
        
        $this->productID = $productID;
        $this->productSerialNumber = $productSerialNumber;
        $this->productDescription = $productDescription;
        $this->productDrawingNumber = $productDrawingNumber;
        $this->productComment = $productComment;
        $this->productEnquiryOnlyFlag = $productEnquiryOnlyFlag;
        $this->customerID = $customerID;
        $this->installationID = $installationID;
        $this->versionLookupID = $versionLookupID;
        $this->productUnitName = $productUnitName;
        $this->sourceAliasID = $sourceAliasID;
    }
}
