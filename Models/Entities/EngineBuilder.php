<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of EngineBuilder
 * Engine Builder entity contains an ID for the engine builder and the Engine
 * Builders Name. This is a top level entity for describing a full engine.
 *
 * @author Archie
 */
class EngineBuilder {
    public $ID;
    public $engineBuilder;

    /**
     * Constructor for Engine Builder.
     * @param INT $ID
     * @param SRTING $engineBuilder
     */
    public function __construct($ID, $engineBuilder) {
        $this->ID = $ID;
        $this->engineBuilder = $engineBuilder;
    }
}
