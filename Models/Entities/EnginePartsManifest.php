<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of EnginePartsManifest
 * Engine Parts Manifest Entity contains all the information captured about each individual part
 * associated with a version of an engine. The number of each part required.
 *
 * @author Archie
 */
class EnginePartsManifest {
    public $ID;
    public $engineModelVersionID;
    public $vInline;
    public $stcCode;
    public $oeNumber;
    public $altNumber;
    public $description;
    public $weight;
    public $price;
    public $priceYear;
    public $wearItem;
    public $inStock;
    public $minStockLevel;
    public $mainSupplier;
    public $fourCycleQuantity;
    public $fiveCycleQuantity;
    public $sixCycleQuantity;
    public $sevenCycleQuantity;
    public $eightCycleQuantity;
    public $nineCycleQuantity;
    public $tenCycleQuantity;
    public $twelveCycleQuantity;
    public $fourteenCycleQuantity;
    public $sixteenCycleQuantity;
    public $eighteenCycleQuantity;
    public $twentyCycleQuantity;
    public $archivePart;
    
    
    /**
     * Constructor for Engine Parts Manifest
     * @param INT $ID
     * @param INT $engineModelVersionID
     * @param STRING $vInline
     * @param STRING $stcCode
     * @param STRING $oeNumber
     * @param STRING $altNumber
     * @param STRING $description
     * @param FLOAT $weight
     * @param FLOAT $price
     * @param DATE $priceYear
     * @param BOOLEAN $wearItem
     * @param BOOLEAN $inStock
     * @param INT $minStockLevel
     * @param STRING $mainSupplier
     * @param INT $fourCycleQuantity
     * @param INT $fiveCycleQuantity
     * @param INT $sixCycleQuantity
     * @param INT $sevenCycleQuantity
     * @param INT $eightCycleQuantity
     * @param INT $nineCycleQuantity
     * @param INT $tenCycleQuantity
     * @param INT $twelveCycleQuantity
     * @param INT $fourteenCycleQuantity
     * @param INT $sixteenCycleQuantity
     * @param INT $eighteenCycleQuantity
     * @param INT $twentyCycleQuantity
     * @param BOOLEAN $archivePart
     */
    public function __construct($ID, $engineModelVersionID, $vInline, $stcCode, $oeNumber, 
            $altNumber, $description, $weight, $price, $priceYear, $wearItem, $inStock, 
            $minStockLevel, $mainSupplier, $fourCycleQuantity, $fiveCycleQuantity, 
            $sixCycleQuantity, $sevenCycleQuantity, $eightCycleQuantity, $nineCycleQuantity, 
            $tenCycleQuantity, $twelveCycleQuantity, $fourteenCycleQuantity, $sixteenCycleQuantity, 
            $eighteenCycleQuantity, $twentyCycleQuantity, $archivePart) {
        $this->ID = $ID;
        $this->engineModelVersionID = $engineModelVersionID;
        $this->vInline = $vInline;
        $this->stcCode = $stcCode;
        $this->oeNumber = $oeNumber;
        $this->altNumber = $altNumber;
        $this->description = $description;
        $this->weight = $weight;
        $this->price = $price;
        $this->priceYear = $priceYear;
        $this->wearItem = $wearItem;
        $this->inStock = $inStock;
        $this->minStockLevel = $minStockLevel;
        $this->mainSupplier = $mainSupplier;
        $this->fourCycleQuantity = $fourCycleQuantity;
        $this->fiveCycleQuantity = $fiveCycleQuantity;
        $this->sixCycleQuantity = $sixCycleQuantity;
        $this->sevenCycleQuantity = $sevenCycleQuantity;
        $this->eightCycleQuantity = $eightCycleQuantity;
        $this->nineCycleQuantity = $nineCycleQuantity;
        $this->tenCycleQuantity = $tenCycleQuantity;
        $this->twelveCycleQuantity = $twelveCycleQuantity;
        $this->fourteenCycleQuantity = $fourteenCycleQuantity;
        $this->sixteenCycleQuantity = $sixteenCycleQuantity;
        $this->eighteenCycleQuantity = $eighteenCycleQuantity;
        $this->twentyCycleQuantity = $twentyCycleQuantity;
        $this->archivePart = $archivePart;
        
    }
}
