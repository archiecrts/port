<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of PartCode
 *
 * @author Archie
 */
class PartCode {
    
    public $partID;
    public $partCode;
    public $partDescriptionLookupID;
    public $partWeight;
    public $partTotalStock;
    public $partMinimumStock;
    public $partReservedStock;
    public $partFreeStockQty;
    public $partOnOrderQty;
    public $partReservedForKits;
    public $partPreferredSupplierID;
    public $partCostPrice;
    public $partCostPriceSetDate;
    public $partWearItem;
    public $dimensionID;
    public $materialID;
    public $colourID;
    public $ldPartNumber;
    public $description;
    public $stockItem;
    
    /**
     * Constructor
     * @param type $partID
     * @param type $partCode
     * @param type $partDescriptionLookupID
     * @param type $partWeight
     * @param type $partTotalStock
     * @param type $partMinimumStock
     * @param type $partReservedStock
     * @param type $partFreeStockQty
     * @param type $partOnOrderQty
     * @param type $partReservedForKits
     * @param type $partPreferredSupplierID
     * @param type $partCostPrice
     * @param type $partCostPriceSetDate
     * @param type $partWearItem
     * @param type $dimensionID
     * @param type $materialID
     * @param type $colourID
     * @param type $ldPartNumber
     * @param type $description
     * @param type $stockItem
     */
    public function __construct($partID, $partCode, $partDescriptionLookupID, $partWeight,
            $partTotalStock, $partMinimumStock, $partReservedStock, $partFreeStockQty,
            $partOnOrderQty, $partReservedForKits, $partPreferredSupplierID,
            $partCostPrice, $partCostPriceSetDate, $partWearItem, $dimensionID,
            $materialID, $colourID, $ldPartNumber, $description, $stockItem) {
        $this->partID = $partID;
        $this->partCode = $partCode;
        $this->partDescriptionLookupID = $partDescriptionLookupID;
        $this->partWeight = $partWeight;
        $this->partTotalStock = $partTotalStock;
        $this->partMinimumStock = $partMinimumStock;
        $this->partReservedStock = $partReservedStock;
        $this->partFreeStockQty = $partFreeStockQty;
        $this->partOnOrderQty = $partOnOrderQty;
        $this->partReservedForKits = $partReservedForKits;
        $this->partPreferredSupplierID = $partPreferredSupplierID;
        $this->partCostPrice = $partCostPrice;
        $this->partCostPriceSetDate = $partCostPriceSetDate;
        $this->partWearItem = $partWearItem;
        $this->dimensionID = $dimensionID;
        $this->materialID = $materialID;
        $this->colourID = $colourID;
        $this->ldPartNumber = $ldPartNumber;
        $this->description = $description;
        $this->stockItem = $stockItem;
    }
}
