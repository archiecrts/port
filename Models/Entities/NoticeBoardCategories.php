<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of NoticeBoardCategories.
 * This entity allows the creation of Categories for the noticeboard.
 * This table is created to allow the noticeboard admin to create new categories
 * without involving the webmaster.
 *
 * @author Archie
 */
class NoticeBoardCategories {
    public $categoryID;
    public $category;
    public $notEditableFlag;
    
    /**
     * Constructor for NoticeBoardCategories Entity.
     * @param INT $categoryID
     * @param STRING $category
     * @param BOOLEAN $notEditableFlag
     */
    public function __construct($categoryID, $category, $notEditableFlag) {
        $this->categoryID = $categoryID;
        $this->category = $category;
        $this->notEditableFlag = $notEditableFlag;
    }
}
