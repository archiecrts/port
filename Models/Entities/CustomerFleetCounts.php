<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of CustomerFleetCounts
 *
 * @author Archie
 */
class CustomerFleetCounts {
    public $fleetCountID;
    public $fleetCountOWCode;
    public $customerID;
    public $fleetCountRegisteredOwnerCount;
    public $fleetCountShipManagerCount;
    public $fleetCountOperatorCount;
    public $fleetCountGroupOwnerCount;
    public $fleetCountDocCount;
    public $fleetCountFleetSize;
    public $fleetCountInServiceCount;
    
    /**
     * Constructor
     * @param type $fleetCountID
     * @param type $fleetCountOWCode
     * @param INT $customerID
     * @param type $fleetCountRegisteredOwnerCount
     * @param type $fleetCountShipManagerCount
     * @param type $fleetCountOperatorCount
     * @param type $fleetCountGroupOwnerCount
     * @param type $fleetCountDocCount
     * @param type $fleetCountFleetSize
     * @param type $fleetCountInServiceCount
     */
    public function __construct($fleetCountID, $fleetCountOWCode, $customerID, $fleetCountRegisteredOwnerCount, 
            $fleetCountShipManagerCount, $fleetCountOperatorCount, $fleetCountGroupOwnerCount, 
            $fleetCountDocCount, $fleetCountFleetSize, $fleetCountInServiceCount) {
        $this->fleetCountID = $fleetCountID;
        $this->fleetCountOWCode = $fleetCountOWCode;
        $this->customerID = $customerID;
        $this->fleetCountRegisteredOwnerCount = $fleetCountRegisteredOwnerCount;
        $this->fleetCountShipManagerCount = $fleetCountShipManagerCount;
        $this->fleetCountOperatorCount = $fleetCountOperatorCount;
        $this->fleetCountGroupOwnerCount = $fleetCountGroupOwnerCount;
        $this->fleetCountDocCount = $fleetCountDocCount;
        $this->fleetCountFleetSize = $fleetCountFleetSize;
        $this->fleetCountInServiceCount = $fleetCountInServiceCount;
    }
}
