<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of Enquiry
 *
 * @author Archie
 */
class Enquiry {
    public $enquiryID;
    public $enquiryDate;
    public $userID;
    public $enquirySuccessRate;
    public $enquiryHeaderNote;
    public $customerID;
    public $enquirySourceID;
    public $enquiryTypeID;
    public $enquiryCustomerReference;
    public $customerContactID;
    public $includeInstallationInQuote;
    public $ourReference;
    
    
    public function __construct($enquiryID, $enquiryDate, $userID, $enquirySuccessRate,
            $enquiryHeaderNote, $customerID, $enquirySourceID, $enquiryTypeID, 
            $enquiryCustomerReference, $customerContactID, $includeInstallationInQuote, $ourReference) {
        $this->enquiryID = $enquiryID;
        $this->enquiryDate = $enquiryDate;
        $this->userID = $userID;
        $this->enquirySuccessRate = $enquirySuccessRate;
        $this->enquiryHeaderNote = $enquiryHeaderNote;
        $this->customerID = $customerID;
        $this->enquirySourceID = $enquirySourceID;
        $this->enquiryTypeID = $enquiryTypeID;
        $this->enquiryCustomerReference = $enquiryCustomerReference;
        $this->customerContactID = $customerContactID;
        $this->includeInstallationInQuote = $includeInstallationInQuote;
        $this->ourReference = $ourReference;
    }
}
