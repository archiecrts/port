<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of EngineDBModel
 * The Engine Model entity contains an ID for the engine model, its model description
 * and its associated engine builder id. There are two engine models at present because this
 * is an amalgamation of two databases. The EngineDBModel relates to the Engine Database and the 
 * Other relates to Market Research. When the DB design is more stable, efforts will be made
 * to amalgamate these two entities.
 *
 * @author Archie
 */
class EngineDBModel {
    public $ID;
    public $engineDBModel;
    public $engineBuilderID;
    

    /**
     * Constructor for Engine Model
     * @param type $ID
     * @param type $engineDBModel
     * @param type $engineBuilderID
     */
    public function __construct($ID, $engineDBModel, $engineBuilderID) {
        $this->ID = $ID;
        $this->engineDBModel = $engineDBModel;
        $this->engineBuilderID = $engineBuilderID;
    }
}