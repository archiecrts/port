<?php

/*
 * Property of S-T Diesel Marine Group
 */

/**
 * Description of ReportOverview
 * Report Overview Entity contains an overview of a Report.
 * It contains the overview ID, a report name, date created and the date run,
 * an associated User ID, a copy/link to of the email sent, a dynamic flag that marks
 *
 * @author Archie
 */
class ReportOverview {
    public $reportOverviewID;
    public $reportName;
    public $dateCreated;
    public $userID;
    public $emailSent;
    public $dynamicFlag;
    public $reportActive;

    /**
     * Constructor for Report Overview.
     * @param INT $reportOverviewID
     * @param STRING $reportName
     * @param DATE TIME $dateCreated
     * @param DATE TIME $dateReportRun
     * @param INT $userID
     * @param STRING $emailSent
     * @param BOOLEAN $dynamicFlag
     * @param BOOLEAN $reportActive
     */
    public function __construct($reportOverviewID, $reportName, $dateCreated, 
                                            $userID, $emailSent, $dynamicFlag, $reportActive) 
    {
        $this->reportOverviewID = $reportOverviewID;
        $this->reportName = $reportName;
        $this->dateCreated = $dateCreated;
        $this->userID = $userID;
        $this->emailSent = $emailSent;
        $this->dynamicFlag = $dynamicFlag;
        $this->reportActive = $reportActive;
    }
}
