<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of LoadPriority
 *
 * @author Archie
 */
class LoadPriority {
    public $loadID;
    public $loadPriority;
    
    public function __construct($loadID, $loadPriority) {
        $this->loadID = $loadID;
        $this->loadPriority = $loadPriority;
    }
}
