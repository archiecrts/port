<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of Supplier.
 * The supplier table just contains a unique id and the suppliers name.
 *
 * @author Archie
 */
class Supplier {
    public $ID;
    public $supplierName;
    
    /**
     * Constructor for Supplier Entity
     * @param INT $ID
     * @param STRING $supplierName
     */
    public function __construct($ID, $supplierName) {
        $this->ID = $ID;
        $this->supplierName = $supplierName;
    } 
}