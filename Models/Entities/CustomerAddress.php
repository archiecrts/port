<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of CustomerAddress
 *
 * @author Archie
 */
class CustomerAddress {
    public $addressID;
    public $officeName;
    public $address;
    public $city;
    public $state;
    public $region;
    public $country;
    public $postcode;
    public $addressVerified;
    public $customerID;
    public $archived;
    public $defaultAddress;
    public $fullAddress;
    
    /**
     * Constructor for the Customer Address Object.
     * @param INT $addressID
     * @param STRING $officeName
     * @param STRING $address
     * @param STRING $city
     * @param STRING $state
     * @param STRING $region
     * @param STRING $country
     * @param STRING $postcode
     * @param BOOLEAN $addressVerified
     * @param INT $customerID
     * @param BOOLEAN $archived
     * @param BOOLEAN $defaultAddress
     * @param STRING $fullAddress
     */
    public function __construct($addressID, $officeName, $address, $city, 
            $state, $region, $country, $postcode, $addressVerified, $customerID, 
            $archived, $defaultAddress, $fullAddress) {
        
        $this->addressID = $addressID;
        $this->officeName = $officeName;
        $this->address = $address;
        $this->city = $city;
        $this->state = $state;
        $this->region = $region;
        $this->country = $country;
        $this->postcode = $postcode;
        $this->addressVerified = $addressVerified;
        $this->customerID = $customerID;
        $this->archived = $archived;
        $this->defaultAddress = $defaultAddress;
        $this->fullAddress = $fullAddress;
    }
}
