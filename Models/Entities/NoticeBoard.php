<?php

/*
 * Property of S-T Diesel Marine Group.
 */

/**
 * Description of NoticeBoard
 * Notice Board Entity contains constructor for an article created to be displayed in
 * the notice board.
 *
 * @author Archie
 */
class NoticeBoard {
    public $ID;
    public $title;
    public $date;
    public $content;
    public $userID;
    public $categoryID;
    public $archive;
    public $authorised;
    public $documentID;

    /**
     * Constructor for Notice Board Entity.
     * @param INT $id
     * @param STRING $title
     * @param DATE $date
     * @param TEXT $content
     * @param INT $userID
     * @param STRING $categoryID
     * @param BOOLEAN $archive
     * @param BOOLEAN $authorised
     * @param INT $documentID
     */
    public function __construct($id, $title, $date, $content, $userID, 
            $categoryID, $archive, $authorised, $documentID) {
        $this->ID = $id;
        $this->title = $title;
        $this->date = $date;
        $this->content = $content;
        $this->userID = $userID;
        $this->categoryID = $categoryID;
        $this->archive = $archive;
        $this->authorised = $authorised;
        $this->documentID = $documentID;
    }
}
