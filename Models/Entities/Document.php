<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of Document
 * A location and details for a document.
 * @author Archie
 */
class Document {
    public $documentID;
    public $url;
    public $loadedDate;
    public $readByDate;
    public $archived;
    public $destination;
    
    /**
     * Constructor for Document Entity
     * @param INT $documentID
     * @param STRING $url
     * @param DATETIME $loadedDate
     * @param DATETIME $readByDate
     * @param BOOLEAN $archived
     * @param STRING $destination
     */
    public function __construct($documentID, $url, $loadedDate, $readByDate, $archived, $destination) {
        $this->documentID = $documentID;
        $this->url = $url;
        $this->loadedDate = $loadedDate;
        $this->readByDate = $readByDate;
        $this->archived = $archived;
        $this->destination = $destination;
    }
}
