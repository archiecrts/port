<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of ModelLookup
 *
 * @author Archie
 */
class ModelLookup {
    public $modelID;
    public $modelDescription;
    public $modelSeries;
    public $designerLookupID;
    public $typeOfProductLookupID;
    
    /**
     * Constructor for TypeOfProductLookup
     * @param INT $modelID
     * @param STRING $modelDescription
     * @param STRING $modelSeries
     * @param STRING $designerLookupID
     * @param STRING $typeOfProductLookupID
     */
    public function __construct($modelID, $modelDescription, $modelSeries, $designerLookupID,
            $typeOfProductLookupID) {
        $this->modelID = $modelID;
        $this->modelDescription = $modelDescription;
        $this->modelSeries = $modelSeries;
        $this->designerLookupID = $designerLookupID;
        $this->typeOfProductLookupID = $typeOfProductLookupID;
    }
}
