<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of SupplierApprovalStatus
 *
 * @author Archie
 */
class SupplierApprovalStatusLookup {
    
    public $approvalStatusID;
    public $approvalStatus;
    
    /**
     * Constructor for SupplierApprovalStatus
     * @param INT $approvalStatusID
     * @param STRING $approvalStatus
     */
    public function __construct($approvalStatusID, $approvalStatus) {
        $this->approvalStatusID = $approvalStatusID;
        $this->approvalStatus = $approvalStatus;
    }
}
