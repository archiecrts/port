<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of EngineModelVersion
 * Engine Model Version entity comes out of the Engine DB. It contains a unique ID
 * an associated engine Model ID and a description of the version.
 *
 * @author Archie
 */
class EngineModelVersion {
    public $ID;
    public $engineModelID;
    public $description;
    
    /**
     * Constructor for Engine Model Version.
     * @param INT $ID
     * @param INT $engineModelID
     * @param STRING $description
     */
    public function __construct($ID, $engineModelID, $description) {
        $this->ID = $ID;
        $this->engineModelID = $engineModelID;
        $this->description = $description;
    }
}
