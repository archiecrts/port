<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of EngineServicingIntervals
 * Engine Servicing Intervals Entity contains a unique ID for the interval, an associated engine
 * manufacturer/builder ID, the actual interval, whether its considered a major service interval
 * and its associated column number.
 *
 * @author Archie
 */
class EngineServicingIntervals {
    public $ID;
    public $engineBuilderID;
    public $interval;
    public $majorFlag;
    public $columnNumber;
    
    /**
     * Constructor for Engine Servicing Intervals Entity.
     * @param INT $ID
     * @param INT $engineBuilderID
     * @param INT $interval
     * @param BOOLEAN $majorFlag
     * @param INT $columnNumber
     */
    public function __construct($ID, $engineBuilderID, $interval, $majorFlag, $columnNumber) {
        $this->ID = $ID;
        $this->engineBuilderID = $engineBuilderID;
        $this->interval = $interval;
        $this->majorFlag = $majorFlag;
        $this->columnNumber = $columnNumber;
    }
}
