<?php

/*
 * Property of S-T DIesel Marine Group.
 */

/**
 * Description of Installation
 * Installation Entity describes an installation (containing engines).
 *
 * @author Archie
 */
class Installation {

    public $installationID;
    public $type;
    public $installation;
    public $installationName;
    public $imo;
    public $status;
    public $operatingCompany;
    public $technicalManager;
    public $groupOwner;
    public $originalSource;
    public $originalSourceDate;
    public $photoLink;
    public $hullNumber;
    public $hullType;
    public $yardBuilt;
    public $deadWeight;
    public $lengthOverall;
    public $beamExtreme;
    public $beamMoulded;
    public $builtDate;
    public $draught;
    public $grossTonnage;
    public $length;
    public $propellerType;
    public $propulsionUnitCount;
    public $shipBuilder;
    public $teu;
    public $buildYear;
    // archive
    public $archiveFlag;
    public $archiveReason;
    // address of installation
    public $customerAddressID;
    public $flagName;
    public $leadShipByIMO;
    public $shipTypeLevel4;
    public $classificationSociety;

    /**
     * Constructor for Installation Entity.
     * @param type $installationID
     * @param type $type
     * @param type $installation
     * @param type $installationName
     * @param type $imo
     * @param type $operatingCompany
     * @param type $technicalManager
     * @param type $groupOwner
     * @param type $status
     * @param type $originalSource
     * @param type $originalSourceDate
     * @param STRING $photoLink
     * @param type $hullNumber
     * @param type $hullType
     * @param type $yardBuilt
     * @param type $deadWeight
     * @param type $lengthOverall
     * @param type $beamExtreme
     * @param type $beamMoulded
     * @param type $builtDate
     * @param type $draught
     * @param type $grossTonnage
     * @param type $length
     * @param type $propellerType
     * @param type $propulsionUnitCount
     * @param type $shipBuilder
     * @param type $teu
     * @param type $buildYear
     * @param type $archiveFlag
     * @param type $archiveReason
     * 
     * @param int $customerAddressID
     * @param string $classificationSociety
     */
    public function __construct($installationID, $type, $installation, $installationName, 
            $imo, $operatingCompany, $technicalManager, $groupOwner, $status, $originalSource, 
            $originalSourceDate, $photoLink, $hullNumber, $hullType, $yardBuilt, 
            $deadWeight, $lengthOverall, $beamExtreme, $beamMoulded, $builtDate, 
            $draught, $grossTonnage, $length, $propellerType, $propulsionUnitCount, 
            $shipBuilder, $teu, $buildYear, $archiveFlag, $archiveReason, $customerAddressID,
            $flagName, $leadShipByIMO, $shipTypeLevel4, $classificationSociety) {

        $this->installationID = $installationID;
        $this->type = $type;
        $this->installation = $installation;
        $this->installationName = $installationName;
        $this->imo = $imo;
        $this->status = $status;
        $this->operatingCompany = $operatingCompany;
        $this->technicalManager = $technicalManager;
        $this->groupOwner = $groupOwner;
        $this->originalSource = $originalSource;
        $this->originalSourceDate = $originalSourceDate;
        $this->photoLink = $photoLink;
        $this->hullNumber = $hullNumber;
        $this->hullType = $hullType;
        $this->yardBuilt = $yardBuilt;
        $this->deadWeight = $deadWeight;
        $this->lengthOverall = $lengthOverall;
        $this->beamExtreme = $beamExtreme;
        $this->beamMoulded = $beamMoulded;
        $this->builtDate = $builtDate;
        $this->draught = $draught;
        $this->grossTonnage = $grossTonnage;
        $this->length = $length;
        $this->propellerType = $propellerType;
        $this->propulsionUnitCount = $propulsionUnitCount;
        $this->shipBuilder = $shipBuilder;
        $this->teu = $teu;
        $this->buildYear = $buildYear;
        $this->archiveFlag = $archiveFlag;
        $this->archiveReason = $archiveReason;
        $this->customerAddressID = $customerAddressID;
        $this->flagName = $flagName;
        $this->leadShipByIMO = $leadShipByIMO;
        $this->shipTypeLevel4 = $shipTypeLevel4;
        $this->classificationSociety = $classificationSociety;
    }

}
