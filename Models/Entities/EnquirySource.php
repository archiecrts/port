<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of EnquirySource
 *
 * @author alexandra
 */
class EnquirySource {
    public $sourceID;
    public $source;
    
    public function __construct($sourceID, $source) {
        $this->sourceID=$sourceID;
        $this->source=$source;
    }
}
