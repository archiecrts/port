<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of ReportUserDisplaySettings
 *
 * @author Archie
 */
class ReportUserDisplaySettings {
    public $settingsID;
    public $columnID;
    public $userID;
    public $active;
    
    /**
     * Constructor for Report User Display Settings
     * @param INT $settingsID
     * @param INT $columnID
     * @param INT $userID
     * @param BOOLEAN $active
     */
    public function __construct($settingsID, $columnID, $userID, $active) {
        $this->settingsID = $settingsID;
        $this->columnID = $columnID;
        $this->userID = $userID;
        $this->active = $active;
    }
}
