<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of PartDescriptionLookup
 *
 * @author Archie
 */
class PartDescriptionLookup {
    public $partDescriptionID;
    public $majorType;
    public $minorType;
    
    /**
     * Constructor
     * @param INT $partDescriptionID
     * @param STRING $majorType
     * @param STRING $minorType
     */
    public function __construct($partDescriptionID, $majorType, $minorType) {
        $this->partDescriptionID = $partDescriptionID;
        $this->majorType = $majorType;
        $this->minorType = $minorType;
        
    }
}
