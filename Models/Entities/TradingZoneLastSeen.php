<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of TradingZoneLastSeen
 *
 * @author Archie
 */
class TradingZoneLastSeen {
    public $tradingZoneID;
    public $tradingZone;
    public $lastSeenDate;
    public $installationID;
    public $LRNO;


    public function __construct($tradingZoneID, $tradingZone, $lastSeenDate, $installationID, $LRNO) {
        
        $this->tradingZoneID = $tradingZoneID;
        $this->tradingZone = $tradingZone;
        $this->lastSeenDate = $lastSeenDate;
        $this->installationID = $installationID;
        $this->LRNO = $LRNO;
    }
}
