<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of PartColour
 *
 * @author Archie
 */
class PartColour {
    public $partColourID;
    public $partColour;
    
    /**
     * __construct
     * @param type $partColourID
     * @param type $partColour
     */
    public function __construct($partColourID, $partColour) {
        $this->partColourID = $partColourID;
        $this->partColour = $partColour;
    }
        
}
