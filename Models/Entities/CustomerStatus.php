<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of CustomerStatus
 *
 * @author Archie
 */
class CustomerStatus {
    public $custStatusID;
    public $customerID;
    public $custStatusLookupID;
    
    public function __construct($custStatusID, $customerID, $custStatusLookupID) {
        $this->custStatusID = $custStatusID;
        $this->customerID = $customerID;
        $this->custStatusLookupID = $custStatusLookupID;
    }
}
