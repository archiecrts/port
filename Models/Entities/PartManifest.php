<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of PartManifest
 *
 * @author Archie
 */
class PartManifest {
    public $manifestID;
    public $partCode;
    public $oeNumber;
    public $altNumber;
    public $quantityOnEngine;
    public $archivedPart;
    public $versionLookupID;
    public $manualURL;
    
    /**
     * Constructor
     * @param INT $manifestID
     * @param STRING $partCode
     * @param STRING $oeNumber
     * @param STRING $altNumber
     * @param INT $quantityOnEngine
     * @param BOOLEAN $archivedPart
     * @param STRING $manualURL
     */
    public function __construct($manifestID, $partCode, $oeNumber, $altNumber, $quantityOnEngine,
            $archivedPart, $versionLookupID, $manualURL) {
        $this->manifestID = $manifestID;
        $this->partCode = $partCode;
        $this->oeNumber = $oeNumber;
        $this->altNumber = $altNumber;
        $this->quantityOnEngine = $quantityOnEngine;
        $this->archivedPart = $archivedPart;
        $this->versionLookupID = $versionLookupID;
        $this->manualURL = $manualURL;
    }
}
