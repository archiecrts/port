<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/CustomerFleetCounts.php");
include_once("Models/SQLConstructors/CustomerFleetCountsSQLConstructor.php");
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of CustomerFleetCountsModel
 *
 * @author Archie
 */
class CustomerFleetCountsModel {
    
    
    /**
     * getCustomerFleetCountsByCustomerID
     * @param type $customerID
     * @return type
     */
    public function getCustomerFleetCountsByCustomerID($customerID) {
        $customerFleetCountsSQLConstructor = new CustomerFleetCountsSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_fleet_counts_tbl WHERE "
                . "customer_tbl_cust_id = '$customerID'");
        
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['cfct_id']] = $customerFleetCountsSQLConstructor->createCustomerFleetCounts($row);
        }
        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
        
    }
}
