<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once("Models/Entities/CustomerContact.php");
include_once 'Models/SQLConstructors/CustomerContactsSQLConstructor.php';
////include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of CustomerContactsModel
 *
 * @author Archie
 */
class CustomerContactsModel {
    
    /**
     * 
     * @param type $customerID
     * @return \CustomerContacts
     */
    public function getContactsByCustomerID($customerID)
    {
        $customerContactsSQLConstructor = new CustomerContactsSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_contacts_tbl WHERE customer_tbl_cust_id='".$customerID."' "
                . "ORDER BY customer_address_tbl_cadt_id ASC");

        //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['ict_id']] = $customerContactsSQLConstructor->createCustomerContact($row);
        }
        if (isset($array)){
            return $array;
        } else {
            return null;
        }
    }
    
    /**
     * getFirstContactByCustomerID
     * @param type $customerID
     * @return type
     */
    public function getFirstContactByCustomerID($customerID)
    {
        $customerContactsSQLConstructor = new CustomerContactsSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_contacts_tbl WHERE customer_tbl_cust_id='".$customerID."' "
                . "LIMIT 1");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);
        $record  = $customerContactsSQLConstructor->createCustomerContact($row);
        
        if (isset($record)){
            return $record;
        } else {
            return null;
        }
    }
    
    /**
     * 
     * @param type $customerID
     * @return \CustomerContacts
     */
    public function getActiveContactsByCustomerID($customerID)
    {
        $customerContactsSQLConstructor = new CustomerContactsSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_contacts_tbl WHERE customer_tbl_cust_id='".$customerID."' "
                . "AND ict_inactive = '0' ORDER BY customer_address_tbl_cadt_id ASC");

        //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['ict_id']] = $customerContactsSQLConstructor->createCustomerContact($row);
        }
        if (isset($array)){
            return $array;
        } else {
            return null;
        }
    }
    
    
    /**
     * 
     * @param type $contactID
     * @return \CustomerContacts
     */
    public function getContactByID($contactID)
    {
        $customerContactsSQLConstructor = new CustomerContactsSQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_contacts_tbl WHERE ict_id='".$contactID."' ");
        $row = mysqli_fetch_assoc($result);
        
        if (isset($row)) {
            $contact = $customerContactsSQLConstructor->createCustomerContact($row);
            return $contact;
        } else {
            return null;
        }
        
    }
    
    /**
     * updateContactDetails
     * @param type $contactObject
     * @return boolean
     */
    public function updateContactDetails($contactObject) {
        $customerAddress = " NULL ";
        if ($contactObject->customerAddressID != "") {
            $customerAddress = "'".$contactObject->customerAddressID."'";
        }
        $result = "UPDATE customer_contacts_tbl SET "
                . "ict_salutation='".$contactObject->salutation."', "
                . "ict_first_name='".$contactObject->firstName."', "
                . "ict_surname='".$contactObject->surname."', "
                . "ict_job_title = '".$contactObject->jobTitle."', "
                . "ict_email = '".$contactObject->email."', "
                . "ict_number = '".$contactObject->number."', "
                . "ict_mobile_number = '".$contactObject->mobile."', "
                . "ict_info_source = '".$contactObject->infoSource."', "
                . "ict_inactive = '".$contactObject->inactive."',"
                . "ict_contact_validated = '".$contactObject->contactValidated."',"
                . "customer_address_tbl_cadt_id = ".$customerAddress." "
                . "WHERE ict_id='".$contactObject->customerContactID."'";
        
        if (!mysqli_query(Database::$connection, $result)) {
            
            debugWriter("debug.txt", "updateContactDetails : ".$customerAddress." ".  mysqli_error(Database::$connection)."\r\n");
            return null;
        } else {
            $customerContactID = $contactObject->customerContactID;
            if ($customerAddress != NULL) {
                $history = "INSERT INTO customer_contacts_history_tbl "
                        . "(customer_contacts_tbl_ict_id, ccht_action, ccht_date, user_tbl_usr_id) "
                        . "VALUES ('".$customerContactID."','Updated', NOW(), '".$_SESSION['user_id']."')";
                if (!mysqli_query(Database::$connection, $history)) {
                    debugWriter("debug.txt", "setContact : customer_contacts_history_tbl : ".$customerContactID." ".  mysqli_error(Database::$connection)."\r\n");
                    return $customerContactID;
                } else {
                    $historyID = mysqli_insert_id(Database::$connection);
                    $history2 = "INSERT INTO customer_address_tbl_has_customer_contacts_history_tbl "
                        . "(customer_address_tbl_cadt_id, customer_contacts_history_tbl_ccht_id) "
                        . "VALUES (".$customerAddress.", '".$historyID."')";
                    if (!mysqli_query(Database::$connection, $history2)) {
                        debugWriter("debug.txt", "setContact : customer_address_has_customer_contacts_history : CAdd:".$customerAddress." HIST:".$historyID." ".  mysqli_error(Database::$connection)."\r\n");
                        return $customerContactID;
                    }
                }
                
            }
            return true;
        }
    }
    
    /**
     * setContact
     * @param type $contactObject
     * @return boolean
     */
    public function setContact($contactObject) {
        $customerAddress = " NULL ";
        if ($contactObject->customerAddressID != "") {
            $customerAddress = "'".$contactObject->customerAddressID."'";
        }
       
        $result = "INSERT INTO customer_contacts_tbl ("
                . "ict_salutation, ict_first_name, ict_surname, ict_job_title, ict_email, "
                . "ict_number, ict_mobile_number, ict_info_source, "
                . "ict_inactive, ict_contact_validated, customer_tbl_cust_id, customer_address_tbl_cadt_id) "
                . "VALUES ("
                . "'".$contactObject->salutation."', "
                . "'".$contactObject->firstName."', "
                . "'".$contactObject->surname."', "
                . "'".$contactObject->jobTitle."', "
                . "'".$contactObject->email."', "
                . "'".$contactObject->number."', "
                . "'".$contactObject->mobile."', "
                . "'".$contactObject->infoSource."', "
                . "'".$contactObject->inactive."',"
                . "'".$contactObject->contactValidated."',"
                . "'".$contactObject->customerID."',"
                . "".$customerAddress.")";
        if (!mysqli_query(Database::$connection, $result)) {
            debugWriter("debug.txt", "setContact : ".$customerAddress." ".  mysqli_error(Database::$connection)."\r\n");
            return null;
        } else {
            $customerContactID = mysqli_insert_id(Database::$connection);
            if ($customerAddress != NULL) {
                $history = "INSERT INTO customer_contacts_history_tbl "
                        . "(customer_contacts_tbl_ict_id, ccht_action, ccht_date, user_tbl_usr_id) "
                        . "VALUES ('".$customerContactID."','Created', NOW(), '".$_SESSION['user_id']."')";
                if (!mysqli_query(Database::$connection, $history)) {
                    debugWriter("debug.txt", "setContact : customer_contacts_history_tbl : ".$customerContactID." ".  mysqli_error(Database::$connection)."\r\n");
                    return $customerContactID;
                } else {
                    $historyID = mysqli_insert_id(Database::$connection);
                    $history2 = "INSERT INTO customer_address_tbl_has_customer_contacts_history_tbl "
                        . "(customer_address_tbl_cadt_id, customer_contacts_history_tbl_ccht_id) "
                        . "VALUES (".$customerAddress.", '".$historyID."')";
                    if (!mysqli_query(Database::$connection, $history2)) {
                        debugWriter("debug.txt", "setContact : customer_address_has_customer_contacts_history : CAdd:".$customerAddress." HIST:".$historyID." ".  mysqli_error(Database::$connection)."\r\n");
                        return $customerContactID;
                    }
                }
                
            }
            return $customerContactID;
        }
    }
}