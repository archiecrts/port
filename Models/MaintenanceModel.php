<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/Maintenance.php");
include_once("Models/SQLConstructors/MaintenanceSQLConstructor.php");
////include_once ("Models/DB.php");
include_once("Models/Database.php");

/**
 * Description of MaintenanceModel
 *
 * @author Archie
 */
class MaintenanceModel {

    /**
     * getMaintenanceSwitch
     * @return type
     */
    public function getMaintenanceSwitch() {
        
        $maintenanceSQLConstructor = new MaintenanceSQLConstructor();

        //execute the SQL query and return records
        $result = mysqli_query(Database::$connection, "SELECT * FROM maintenance_tbl WHERE maint_id='1'");
       
        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);

        $maintenance = $maintenanceSQLConstructor->createMaintenance($row);

        if ($row['maint_active'] == 1) {
            $_SESSION['maintenance'] = 1;
        } else {
            unset($_SESSION['maintenance']);
        }
        return $maintenance;
    }

    /**
     * setMaintenanceSwitch
     * @param INT $switch
     * @return boolean
     */
    public function setMaintenanceSwitch() {

        //execute the SQL query and return records
        $result = mysqli_query(Database::$connection, "SELECT * FROM maintenance_tbl WHERE maint_id='1'");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);
        
        $switch = "0";
        if ($row['maint_active'] == '1') {
            $switch = 0;
        } else {
            $switch = 1;
        }

        $update = "UPDATE maintenance_tbl SET maint_active = '" . $switch . "' WHERE maint_id='1'";

        if (!mysqli_query(Database::$connection, $update)) {
            debugWriter("debug.txt", "setMaintenanceSwitch FAILED : $update " . mysqli_error(Database::$connection));
        } else {
            
            if ($switch == 1) {
                $_SESSION['maintenance'] = 1;
            } else {
                unset($_SESSION['maintenance']);
            }
            return true;
        }
    }

}
