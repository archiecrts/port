<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/NotifyAdmin.php");
include_once 'Models/SQLConstructors/NotifyAdminSQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of NotifyAdminModel
 *
 * @author Archie
 */
class NotifyAdminModel {
    
    /**
     * Set Notify Admin Enquiry into table.
     * @param type $notifyAdminObject
     * @return boolean
     */
    public function setEnquiry($notifyAdminObject) {
        $result = "INSERT INTO admin_issue_tracking_tbl (user_tbl_usr_id, ait_date_logged, "
                . "ait_issue_description, ait_status, issue_types_tbl_itt_id) VALUES ("
                . "'" . $notifyAdminObject->userID . "', "
                . "'" . $notifyAdminObject->dateLogged . "', "
                . "'" . $notifyAdminObject->issueDescription . "', "
                . "'" . $notifyAdminObject->status  . "', "
                . "'" . $notifyAdminObject->issueTypeID  . "')";
        
        if (!mysqli_query(Database::$connection, $result)) {
            return null;
        } else {
            return mysqli_insert_id(Database::$connection);
        }
    }
    
    
    /**
     * getAllEnquiriesByCategory.
     * @param INT $categoryID
     * @param STRING $status
     * @return \NotifyAdmin
     */
    public function getAllEnquiriesByCategory($categoryID, $status) {
        $notifyAdminSQLConstructor = new NotifyAdminSQLConstructor();
        
        $sql = "";
        if ($status !== "All") {
            $sql = "AND ait_status = '".$status."'";
        }
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM admin_issue_tracking_tbl WHERE "
                . "issue_types_tbl_itt_id = '".$categoryID."' ".$sql." ORDER BY ait_date_logged DESC");
        
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['ait_id']] = 
                    $notifyAdminSQLConstructor->createNotifyAdmin($row);
        }
        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    
    /**
     * getAllEnquiriesByCategoryForUser
     * @param INT $categoryID
     * @param STRING $status
     * @param INT $userID
     * @return type
     */
    public function getAllEnquiriesByCategoryForUser($categoryID, $status, $userID) {
        $notifyAdminSQLConstructor = new NotifyAdminSQLConstructor();
        
        $sql = "";
        if ($status !== "All") {
            $sql = "AND ait_status = '".$status."'";
        }
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM admin_issue_tracking_tbl WHERE "
                . "user_tbl_usr_id = '".$userID."' AND issue_types_tbl_itt_id = '".$categoryID."' ".$sql." ORDER BY ait_date_logged DESC");
        
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['ait_id']] = 
                    $notifyAdminSQLConstructor->createNotifyAdmin($row);
        }
        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
}
