<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/SupplierApprovalStatusLookup.php");
include_once 'Models/SQLConstructors/SupplierApprovalStatusLookupSQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of SupplierApprovalLookupModel
 *
 * @author Archie
 */
class SupplierApprovalLookupModel {
    
    /**
     * getSupplierApprovalLookupByID
     */
    public function getSupplierApprovalLookupByID($lookupID) {
        $supplierApprovalStatusLookupSQLConstructor = new SupplierApprovalStatusLookupSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM supplier_approval_status_lookup_tbl WHERE saslt_id='$lookupID'");
        $row = mysqli_fetch_assoc($result);
        
        $part = $supplierApprovalStatusLookupSQLConstructor->createSupplierApprovalStatusLookup($row);
        
        return $part;
    }
}
