<?php
include_once("Models/Entities/DesignerLookup.php");
include_once 'Models/SQLConstructors/DesignerLookupSQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of DesignLookupModel
 *
 * @author Archie
 */
class DesignerLookupModel {
    
    /**
     * getDesignerByVersionLookupID
     * @param type $versionLookupID
     * @return type
     */
    public function getDesignerByVersionLookupID($versionLookupID) {
        $designerLookupSQLConstructor = new DesignerLookupSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM designer_lookup_tbl WHERE dlt_id = "
                . "(SELECT designer_lookup_tbl_dlt_id FROM series_lookup_tbl WHERE mlt_id = "
                . "(SELECT model_lookup_tbl_mlt_id FROM version_lookup_tbl WHERE vlt_id = '".$versionLookupID."'))");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);
            
        return $designerLookupSQLConstructor->createDesignerLookup($row);
       
    }
    
    /**
     * getDesignerByDesignerID
     * @param type $designerID
     * @return type
     */
    public function getDesignerByDesignerID($designerID) {
        $designerLookupSQLConstructor = new DesignerLookupSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM designer_lookup_tbl WHERE dlt_id = '".$designerID."'");
        
        $row = mysqli_fetch_assoc($result);
            
        return $designerLookupSQLConstructor->createDesignerLookup($row);
    }
    
    /**
     * getDistinctDesigners
     * @return type
     */
    public function getDistinctDesigners() {
        $designerLookupSQLConstructor = new DesignerLookupSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT d.* FROM designer_lookup_tbl AS d WHERE 
d.dlt_id IN (SELECT distinct (a.designer_lookup_tbl_dlt_id) FROM alias_tbl AS a) ORDER BY dlt_designer_description ASC");
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['dlt_id']] = $designerLookupSQLConstructor->createDesignerLookup($row);
            
        }
        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    /**
     * getDistinctDesignersLeagueOnly
     * @return type
     */
    public function getDistinctDesignersLeagueOnly() {
        $designerLookupSQLConstructor = new DesignerLookupSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT d.* FROM designer_lookup_tbl AS d WHERE d.dlt_stc_league_engine = '1' ");
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['dlt_id']] = $designerLookupSQLConstructor->createDesignerLookup($row);
            
        }
        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    /**
     * getDistinctDesignersByTypeOfProductID
     * @param type $typeOfProductID
     * @return type
     */
    public function getDistinctDesignersByTypeOfProductID($typeOfProductID) {
        $designerLookupSQLConstructor = new DesignerLookupSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM designer_lookup_tbl WHERE dlt_id IN 
                                (select designer_lookup_tbl_dlt_id FROM series_lookup_tbl 
                                WHERE type_of_product_lookup_tbl_toplt_id = '".$typeOfProductID."')");
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['dlt_id']] = $designerLookupSQLConstructor->createDesignerLookup($row);
            
        }
        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    /**
     * getDistinctDesignersWithIntervals
     * @return type
     */
    public function getDistinctDesignersWithIntervals() {
        $designerLookupSQLConstructor = new DesignerLookupSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM designer_lookup_tbl WHERE dlt_id IN "
                    . "(SELECT designer_lookup_tbl_dlt_id FROM engine_servicing_intervals_tbl "
                    . "GROUP BY designer_lookup_tbl_dlt_id)");
            
            
            //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['dlt_id']] 
                    = $designerLookupSQLConstructor->createDesignerLookup($row);
        }
        if(isset($array)) {
            return $array;
        } else {
            return null;
        }
        
    }
}
