<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/NoticeBoardCategories.php");
include_once 'Models/SQLConstructors/NoticeBoardCategoriesSQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of NoticeBoardCategoriesModel.
 * This model provides all functions related to the NoticeBoardCategories Entity.
 *
 * @author Archie
 */
class NoticeBoardCategoriesModel {
    
    /**
     * getCategories returns an array of Notice Board Categories.
     * @return Array of NoticeBoardCategories
     */
    public function getCategories()
    {
        $noticeBoardSQLConstructor = new NoticeBoardCategoriesSQLConstructor();
        //execute the SQL query and return records
        $result = mysqli_query(Database::$connection, "SELECT * FROM notice_board_categories_tbl ORDER BY nbtc_category ASC");

        //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['nbtc_id']] = $noticeBoardSQLConstructor->createNoticeBoardCategories($row);
        }

        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
        
    }
    
    /**
     * getCategoryByID returns a single category object based on its unique id.
     * @param INT $categoryID
     * @return Object NoticeBoardCategories
     */
    public function getCategoryByID($categoryID)
    {
        $noticeBoardSQLConstructor = new NoticeBoardCategoriesSQLConstructor();
        
        //execute the SQL query and return records
        $result = mysqli_query(Database::$connection, "SELECT * FROM notice_board_categories_tbl WHERE nbtc_id = '".$categoryID."'");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);
        $category = $noticeBoardSQLConstructor->createNoticeBoardCategories($row);
        

        if (isset($category)) {
            return $category;
        } else {
            return null;
        }
        
    }
    
    /**
     * setNoticeBoardCategory creates a new category entry in the Notice Board Categories table.
     * @param String $category
     * @param BOOLEAN $locked
     * @return boolean
     */
    public function setNoticeBoardCategory($category, $locked)
    {
        $insert = "INSERT INTO notice_board_categories_tbl "
               . "(nbtc_category, nbtc_not_editable) VALUES ('".$category."', '".$locked."')";
       
            
        if (!mysqli_query(Database::$connection, $insert)) {
            return false;
        } else {
            return true;   
        }   
    }
    
    /**
     * Get a category by name.
     * @param String $name
     * @return \NoticeBoardCategories
     */
    public function getCategoryByName($name) {
        
        $noticeBoardSQLConstructor = new NoticeBoardCategoriesSQLConstructor();
        
        //execute the SQL query and return records
        $result = mysqli_query(Database::$connection, "SELECT * FROM notice_board_categories_tbl WHERE nbtc_category = '".$name."'");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);
        $category = $noticeBoardSQLConstructor->createNoticeBoardCategories($row);
        

        if (isset($category)) {
            return $category;
        } else {
            return null;
        }
    }
}
