<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/PartDimensions.php");
include_once("Models/SQLConstructors/DimensionSQLConstructor.php");
include_once("Models/Database.php");
/**
 * Description of PartDimensionsModel
 *
 * @author Archie
 */
class PartDimensionsModel {
    
    /**
     * getDimensionsByID
     * @param type $dimID
     * @return type
     */
    public function getDimensionsByID($dimID) {
        $partDimensionsSQLConstructor = new DimensionSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM dimensions_tbl WHERE dim_id = '$dimID'");
        
        $row = mysqli_fetch_assoc($result);
        
        $dimensions = $partDimensionsSQLConstructor->createDimensions($row);
         
        return $dimensions;
    }
    
    /**
     * insertDimensions
     * @param type $dimension
     * @return boolean
     */
    public function insertDimensions($dimension) {
        $insert = "INSERT INTO dimensions_tbl (dim_outer_diameter, dim_profile_diameter, dim_length, dim_din_number, dim_head_type, "
                . "dim_valve_or_seat_angle_in_degrees, dim_inner_diameter) "
                . "VALUES ('$dimension->dimOuterDiameter', "
                . "'$dimension->dimProfileDiameter', "
                . "'$dimension->dimLength', "
                . "'$dimension->dimDINNumber', "
                . "'$dimension->dimHeadType', "
                . "'$dimension->dimAngleInDegrees', "
                . "'$dimension->dimInnerDiameter')";
        
        if (!mysqli_query(Database::$connection, $insert)) {
            return false;
        } else {
            return mysqli_insert_id(Database::$connection);
        }
    }
    
    
    /**
     * getDimensionByObjectCriteria
     * @param type $dimension
     * @return type
     */
    public function getDimensionByObjectCriteria($dimension) {
        $partDimensionsSQLConstructor = new DimensionSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT dim_id FROM dimensions_tbl WHERE "
                . "dim_outer_diameter = '$dimension->dimOuterDiameter' "
                . "AND dim_profile_diameter = '$dimension->dimProfileDiameter' "
                . "AND dim_length = '$dimension->dimLength' "
                . "AND dim_din_number = '$dimension->dimDINNumber' "
                . "AND dim_head_type = '$dimension->dimHeadType' "
                . "AND dim_valve_or_seat_angle_in_degrees = '$dimension->dimAngleInDegrees' "
                . "AND dim_inner_diameter = '$dimension->dimInnerDiameter'");
        
        $row = mysqli_fetch_assoc($result);
        if (isset($row['dim_id'])) {
            return $row['dim_id'];
        } else {
            return null;
        }
    }
}
