<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/PartDescriptionLookup.php");
include_once("Models/SQLConstructors/PartDescriptionLookupSQLConstructor.php");
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of PartDescriptionLookupModel
 *
 * @author Archie
 */
class PartDescriptionLookupModel {
    
    /**
     * getAllDescriptions
     * @return array
     */
    public function getAllDescriptions() {
        $partDescriptionLookupSQLConstructor = new PartDescriptionLookupSQLConstructor();
        $result =  mysqli_query(Database::$connection, "SELECT * FROM part_description_lookup_tbl");
        
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['pdlt_id']] = $partDescriptionLookupSQLConstructor->createPartDescriptionLookup($row);
        }
        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    
    /**
     * getAllMajorDescriptions
     * @return type
     */
    public function getAllMajorDescriptions() {
        $partDescriptionLookupSQLConstructor = new PartDescriptionLookupSQLConstructor();
        $result =  mysqli_query(Database::$connection, "SELECT * FROM part_description_lookup_tbl GROUP BY pdlt_major_type");
        
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['pdlt_id']] = $partDescriptionLookupSQLConstructor->createPartDescriptionLookup($row);
        }
        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    /**
     * getAllDescriptionsLikeString
     * @param STRING $likeString
     * @return type
     */
    public function getAllDescriptionsLikeString($likeString) {
        $partDescriptionLookupSQLConstructor = new PartDescriptionLookupSQLConstructor();
        $result =  mysqli_query(Database::$connection, "SELECT * FROM part_description_lookup_tbl "
                . "WHERE pdlt_major_type LIKE '%".$likeString."%' OR "
                . "pdlt_minor_type LIKE '%".$likeString."%'");
        
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['pdlt_id']] = $partDescriptionLookupSQLConstructor->createPartDescriptionLookup($row);
        }
        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    /**
     * getDescriptionByID
     * @param type $descID
     * @return type
     */
    public function getDescriptionByID($descID) {
        $partDescriptionLookupSQLConstructor = new PartDescriptionLookupSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM part_description_lookup_tbl "
                . "WHERE pdlt_id = '$descID'");
        
        $row = mysqli_fetch_assoc($result);
        $description = $partDescriptionLookupSQLConstructor->createPartDescriptionLookup($row);
        
        return $description;
        
    }
}
