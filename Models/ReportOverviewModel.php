<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once("Models/Entities/ReportOverview.php");
include_once("Models/SQLConstructors/ReportOverviewSQLConstructor.php");
//include_once ("Models/DB.php");
include_once("Models/Database.php");

/**
 * Description of ReportOverviewModel
 *
 * @author Archie
 */
class ReportOverviewModel {

    /**
     * getAllReportOverviews Gets all report Overview objects
     * @return \ReportOverview
     */
    public function getAllReportOverviews() {
        $reportOverviewSQLConstructor = new ReportOverviewSQLConstructor();
        //execute the SQL query and return records
        $result = mysqli_query(Database::$connection, "SELECT * FROM report_overview_tbl WHERE rot_report_active='1' "
                . "ORDER BY rot_date_created DESC");

        //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {
            
            $array[$row['rot_id']] = $reportOverviewSQLConstructor->createReportOverview($row);

        }

        if(isset($array))
        {
            return $array;
        } else {
            return null;
        }
    }
    
    /**
     * 
     * @param type $userID
     * @return \ReportOverview
     */
    public function getAllReportOverviewsByUserID($userID) {
        $reportOverviewSQLConstructor = new ReportOverviewSQLConstructor();
        
        //execute the SQL query and return records
        $result = mysqli_query(Database::$connection, "SELECT * FROM report_overview_tbl "
                . "WHERE user_tbl_usr_id='".$userID."' AND rot_report_active='1' ORDER BY rot_date_created DESC");

        //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {
            
            $array[$row['rot_id']] = $reportOverviewSQLConstructor->createReportOverview($row);

        }

        if(isset($array))
        {
            return $array;
        } else {
            return null;
        }
    }
    
    // TODO, not sending object correctly.
    public function getSingleReportByReportOverviewID($reportOverviewID)
    {
        $reportOverviewSQLConstructor = new ReportOverviewSQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM report_overview_tbl WHERE rot_id='".$reportOverviewID."'");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);
        // TODO creating a warning here. Fix this.
        
        $report = $reportOverviewSQLConstructor->createReportOverview($row);

        return $report;
    }
    
    /**
     * Set the report overview record.
     * @param type ReportOverview reportOverviewObject
     * @return type INT mysql_insert_id
     */
    public function setReportOverview($reportOverviewObject) 
    {
        $report_overview_insert = "INSERT INTO report_overview_tbl "
               . "(rot_report_name, rot_date_created, user_tbl_usr_id, rot_dynamic_flag, rot_report_active) "
               . "VALUES ('".$reportOverviewObject->reportName."', "
                . "'".$reportOverviewObject->dateCreated."', "
                . "'".$reportOverviewObject->userID."', "
                . "'".$reportOverviewObject->dynamicFlag."', "
                . "'".$reportOverviewObject->reportActive."' )";
       
        //return mysqli_insert_id(Database::$connection);
            
        if (!mysqli_query(Database::$connection, $report_overview_insert)) {
            return null;
        } else {
            // return the unique row ID created by the insert.
            return mysqli_insert_id(Database::$connection);   
        }
    }
    
    /**
     * Update the name of the report Overview object
     * @param type $reportOverviewID INT ID
     * @param type $name STRING report name
     * @return boolean true or false
     */
    public function updateReportOverview($reportOverviewID, $name, $dynamicFlag)
    {
        $update = "UPDATE report_overview_tbl 
                    SET rot_report_name = '".addslashes($name)."', 
                        rot_dynamic_flag='$dynamicFlag'
                    WHERE rot_id='$reportOverviewID'";
        
        if (!mysqli_query(Database::$connection, $update)) {
            return false;
        } else {
            // return true.
            return true;   
        }
    }
    
    public function getActiveReport() {
        $reportOverviewSQLConstructor = new ReportOverviewSQLConstructor();
        
        $result = "SELECT * FROM report_overview_tbl WHERE rot_report_active='1' "
                . " ORDER BY rot_date_created ASC";
        
        //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {
        
            $array[$row['rot_id']] = $reportOverviewSQLConstructor->createReportOverview($row);

        }

        if(isset($array))
        {
            return $array;
        } else {
            return null;
        }
    }

    
    /**
     * 
     * @return type
     */
    public function getCountActiveReports($userID, $level) {
        if ($level != 'admin') {
            $result = mysqli_query(Database::$connection, "SELECT count(*) FROM report_overview_tbl WHERE rot_report_active='1' "
                . "AND rot_report_name != 'AUTO GENERATED NAME' "
                . "AND user_tbl_usr_id = '".$userID."'");
        } else {
            $result = mysqli_query(Database::$connection, "SELECT count(*) FROM report_overview_tbl WHERE rot_report_active='1' "
                . "AND rot_report_name != 'AUTO GENERATED NAME' ");
        }
        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);

        return $row['count(*)'];
        
    }
}
