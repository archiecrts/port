<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once("Models/Entities/ReportParameters.php");
include_once 'Models/SQLConstructors/ReportParametersSQLConstructor.php';
//include_once ("Models/DB.php");
include_once ('Helpers/charsetFunctions.php');
include_once("Models/Database.php");
/**
 * Description of ReportParametersModel
 *
 * @author Archie
 */
class ReportParametersModel {
    
    
    public function setReportParameter($reportOverviewID, $parameter, $value)
    {
        $report_parameter_insert = "INSERT INTO report_parameters_tbl "
               . "(par_parameter, par_value, report_overview_tbl_rot_id) "
               . "VALUES ('".$parameter."', "
                . "'".utf8_decode($value)."', "
                . "'".$reportOverviewID."')";
       
        //return mysqli_insert_id(Database::$connection);
            
        if (!mysqli_query(Database::$connection, $report_parameter_insert)) {
            return null;
        } else {
            // return true.
            return true;   
        }   
    }
    
    /**
     * 
     * @param type $reportOverviewID
     * @return type
     */
    public function getAllParametersByReportOverviewID($reportOverviewID)
    {
        $reportParametersSQLConstructor = new ReportParametersSQLConstructor();
        
        //execute the SQL query and return records
        $result = mysqli_query(Database::$connection, "SELECT * FROM report_parameters_tbl "
                . "WHERE report_overview_tbl_rot_id='".$reportOverviewID."'");

        //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {
            
            $array[$row['par_id']] = 
                    $reportParametersSQLConstructor->createReportParameter($row);
        }

        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
        
    }
}
