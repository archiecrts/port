<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/Product.php");
include_once 'Models/SQLConstructors/ProductSQLConstructor.php';
include_once("Models/Entities/ProductSummary.php");
include_once 'Models/SQLConstructors/ProductSummarySQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");

/**
 * Description of ProductModel
 *
 * @author Archie
 */
class ProductModel {

    /**
     * getProductByID
     * @param INT $productID
     * @return OBJECT PRODUCT
     */
    public function getProductByID($productID) {
        $productSQLConstructor = new ProductSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM product_tbl WHERE prod_id='" . $productID . "'");

        $row = mysqli_fetch_assoc($result);
        //fetch tha data from the database
        $product = $productSQLConstructor->createProduct($row);


        return $product;
    }

    /**
     * getProductsByInstallationID
     * @param type $installationID
     * @return type
     */
    public function getProductsByInstallationID($installationID) {
        $productSQLConstructor = new ProductSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM product_tbl WHERE installations_tbl_inst_id='" . $installationID . "'");

        //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {

            $array[$row['prod_id']] = $productSQLConstructor->createProduct($row);
        }

        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    /**
     * getProductsByInstallationIDAndProductType
     * @param type $installationID
     * @param type $productType
     * @return type
     */
    public function getProductsByInstallationIDAndProductType($installationID, $productType) {
        $productSQLConstructor = new ProductSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT p.* FROM product_tbl AS p 
                LEFT JOIN version_lookup_tbl AS v ON p.version_lookup_tbl_vlt_id = v.vlt_id
                LEFT JOIN series_lookup_tbl AS s ON v.model_lookup_tbl_mlt_id = s.mlt_id  
                WHERE installations_tbl_inst_id='$installationID' 
                AND s.type_of_product_lookup_tbl_toplt_id = '$productType' AND p.prod_archive_flag = '0' 
                ORDER BY prod_unit_name ASC");

        //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {

            $array[$row['prod_id']] = $productSQLConstructor->createProduct($row);
        }

        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }

    /**
     * countOfProducts
     * @param INT $installationID
     * @return INT count of products
     */
    public function countOfProducts($installationID) {
        $result = mysqli_query(Database::$connection, "SELECT count(prod_id) FROM product_tbl "
                . "WHERE installations_tbl_inst_id='" . $installationID . "'");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);

        $count = $row['count(prod_id)'];

        return $count;
    }

    /**
     * countOfProductsByType
     * @param type $installationID
     * @param type $typeID
     * @return type
     */
    public function countOfProductsByType($installationID, $typeID) {
        $result = mysqli_query(Database::$connection, "SELECT count(prod_id) FROM product_tbl "
                . "WHERE installations_tbl_inst_id='" . $installationID . "' 
                 AND version_lookup_tbl_vlt_id IN 
                (SELECT vlt_id FROM version_lookup_tbl where model_lookup_tbl_mlt_id IN 
                (SELECT mlt_id FROM series_lookup_tbl WHERE type_of_product_lookup_tbl_toplt_id = '" . $typeID . "'))");
        /*debugWriter("debug.txt", "SELECT count(prod_id) FROM product_tbl "
                . "WHERE installations_tbl_inst_id='" . $installationID . "' 
                 AND version_lookup_tbl_vlt_id IN 
                (SELECT vlt_id FROM version_lookup_tbl where model_lookup_tbl_mlt_id IN 
                (SELECT mlt_id FROM series_lookup_tbl WHERE type_of_product_lookup_tbl_toplt_id = '" . $typeID . "'))");*/
        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);

        $count = $row['count(prod_id)'];

        return $count;
    }
    
    /**
     * 
     * @param type $installationID
     * @return type
     */
    public function summaryOfEnginesByInstallationID($installationID) {
        //$productSQLConstructor = new ProductSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT p.prod_id, count(p.prod_id), p.version_lookup_tbl_vlt_id, 
                e.iet_model_description, e.iet_main_aux FROM product_tbl AS p 
                LEFT JOIN version_lookup_tbl AS v ON p.version_lookup_tbl_vlt_id = v.vlt_id
                LEFT JOIN series_lookup_tbl AS s ON v.model_lookup_tbl_mlt_id = s.mlt_id  
                LEFT JOIN installation_engine_tbl AS e ON e.product_tbl_prod_id = p.prod_id
                WHERE installations_tbl_inst_id='$installationID' 
                 GROUP BY v.vlt_id;");

        //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {

            $array[$row['prod_id']] = $row;
        }

        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }

    /**
     * productSummaryByCustomerID
     * @param INT $customerID
     * @return $row array
     */
    public function productSummaryByCustomerID($customerID) {
        $productSummarySQLConstructor = new ProductSummarySQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT COUNT(p.prod_id), p.version_lookup_tbl_vlt_id, 
                                v.vlt_version_description, SUM(v.vlt_cylinder_count),
                                v.model_lookup_tbl_mlt_id, i.iet_model_description  
                                FROM product_tbl AS p
                                LEFT JOIN installation_engine_tbl AS i ON i.product_tbl_prod_id = p.prod_id
                                LEFT JOIN version_lookup_tbl AS v ON v.vlt_id = p.version_lookup_tbl_vlt_id 
                                WHERE p.customer_tbl_cust_id='".$customerID."' 
                                GROUP BY v.model_lookup_tbl_mlt_id ORDER BY SUM(v.vlt_cylinder_count) ASC, i.iet_model_description ASC");
                                //GROUP BY p.version_lookup_tbl_vlt_id
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['model_lookup_tbl_mlt_id']] = $productSummarySQLConstructor->createProductSummary($row);
        }


        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }

    /**
     * setNewProduct
     * @param type $product
     * @return boolean
     */
    public function setNewProduct($product) {

        $insert = "INSERT INTO product_tbl ("
                . "prod_serial_number, "
                . "prod_description, "
                . "prod_drawing_number, "
                . "prod_comments, "
                . "prod_enquiry_only_flag, "
                . "customer_tbl_cust_id, "
                . "installations_tbl_inst_id, "
                . "version_lookup_tbl_vlt_id, "
                . "prod_unit_name, "
                . "prod_source_alias_id) "
                . "VALUES ("
                . "'" . $product->productSerialNumber . "', "
                . "'" . $product->productDescription . "', "
                . "'" . $product->productDrawingNumber . "', "
                . "'" . $product->productComment . "', "
                . "'0', "
                . "'" . $product->customerID . "', "
                . "'" . $product->installationID . "', "
                . "'" . $product->versionLookupID . "', "
                . "'" . $product->productUnitName . "', "
                . "'" . $product->sourceAliasID . "'"
                . ")";

        if (!(mysqli_query(Database::$connection, $insert))) {
            debugWriter("debug.txt", "setNewProduct: " . $insert . " " . mysqli_error(Database::$connection) . "\r\n");
            return false;
        } else {
            return mysqli_insert_id(Database::$connection);
        }
    }

    /**
     * updateProductObject
     * @param type $product
     * @return boolean
     */
    public function updateProductObject($product) {
        $string = "UPDATE product_tbl SET 
            prod_serial_number = '" . addslashes($product->productSerialNumber) . "', 
            prod_description = '" . addslashes($product->productDescription) . "', 
            prod_drawing_number = '" . addslashes($product->productDrawingNumber) . "',
            prod_comments = '" . addslashes($product->productComment) . "',
            prod_enquiry_only_flag = '0',
            customer_tbl_cust_id = '" . $product->customerID . "',
            installations_tbl_inst_id = '" . $product->installationID . "', 
            version_lookup_tbl_vlt_id = '" . $product->versionLookupID . "',
            prod_unit_name = '" . addslashes($product->productUnitName) . "', 
            prod_source_alias_id = '" . $product->sourceAliasID . "'   
            WHERE prod_id = '" . $product->productID . "'";

        if (!(mysqli_query(Database::$connection, $string))) {
            debugWriter("debug.txt", "updateProductObject: " . mysqli_error(Database::$connection) . "\r\n");
            return mysqli_error(Database::$connection);
        } else {
            return true;
        }
    }

}
