<?php

/*
 * Property of S-T Diesel Marine Group.
 */
include_once("Models/Entities/User.php");
include_once ("Models/SQLConstructors/UserSQLConstructor.php");
////include_once ("Models/DB.php");
include_once ("Models/UserHistoryModel.php");
include_once("Models/Database.php");
/**
 * Description of UserModel
 *
 * @author Archie
 */
class UserModel {
    
    /**
     * 
     * @return \User Array
     */
    public function getUsers() 
    {
        $userSQLConstructor = new UserSQLConstructor();
        //execute the SQL query and return records
            $result = mysqli_query(Database::$connection, "SELECT * FROM user_tbl ORDER BY usr_name ASC");
           
            //fetch tha data from the database
            while ($row = mysqli_fetch_assoc($result)) {             
                $array[$row['usr_name']] = $userSQLConstructor->createUser($row);
            }
            
            return $array;
    }
    
    /**
     * 
     * @param type $name
     * @return type User
     */
    public function getUser($name) 
    {
        $allUsers = $this->getUsers();
        
        if (array_key_exists($name, $allUsers))
        {
            return $allUsers[$name];
        }
        else
        {
            return false;
        }
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function getUserByID($id) 
    {
        $userSQLConstructor = new UserSQLConstructor();
        //execute the SQL query and return records
            $result = mysqli_query(Database::$connection, "SELECT * FROM user_tbl WHERE usr_id='".$id."'");
           
            //fetch tha data from the database
            $row = mysqli_fetch_assoc($result);             
            $user = $userSQLConstructor->createUser($row);
            
            
            return $user;
    }
    
    /**
     * 
     * @param type $userLogin
     * @param type $userPassword
     * @return type User
     */
    public function getUserAccessControl($userLogin, $userPassword)
    {
        // To protect MySQL injection (more detail about MySQL injection)
        $myusernameStrip = stripslashes($userLogin);
        $mypasswordStrip = stripslashes($userPassword);
        $myusername = mysqli_real_escape_string(Database::$connection, $myusernameStrip);
        $mypassword = mysqli_real_escape_string(Database::$connection, $mypasswordStrip);
        
        $sql = "SELECT * FROM user_tbl WHERE (usr_login='$myusername' OR usr_email='$myusername') and usr_password=MD5('$mypassword')";
        
        $result=mysqli_query(Database::$connection, $sql);
        $user = mysqli_fetch_assoc($result);

        // Mysql_num_row is counting table row
        $count=mysqli_num_rows($result);

        // If result matched $myusername and $mypassword, table row must be 1 row
        if($count==1){
            $level = mysqli_query(Database::$connection, "SELECT level_name FROM user_level_tbl WHERE level_id='".$user['user_level_tbl_level_id']."'");
            $levelRow = mysqli_fetch_assoc($level);
            $dept = mysqli_query(Database::$connection, "SELECT dept_name FROM user_department_tbl WHERE dept_id='".$user['user_department_tbl_dept_id']."'");
            $deptRow = mysqli_fetch_assoc($dept);
            
            // Register $myusername, $mypassword and redirect to file "login_success.php"
            $_SESSION['name']=$user['usr_name'];
            $_SESSION['level']=$user['user_level_tbl_level_id'];
            $_SESSION['user_id']=$user['usr_id'];
            $_SESSION['department']=$user['user_department_tbl_dept_id'];
            $_SESSION['firstLogin']=$user['usr_initial_login'];
            $_SESSION['limited']=$user['usr_limit_to_territory'];
            $_SESSION['loginDatestamp'] = time();
            $comment = $user['usr_name']." - ". $levelRow['level_name']. " - ". $deptRow['dept_name'] ." - ".$_SERVER['REMOTE_ADDR'];
            $userHistoryModel = new UserHistoryModel();
            $userHistoryModel->setUserHistory($user['usr_id'], date("Y-m-d H:i:s", time()), "login", $comment);
            return "SUCCESS";
        } else {
            $comment = "FAILED ATTEMPT ".$userLogin." - ".$_SERVER['REMOTE_ADDR'];
            $userHistoryModel = new UserHistoryModel();
            $userHistoryModel->setUserHistory(null, date("Y-m-d H:i:s", time()), "failed login", $comment);
            return "Username and Password incorrect";
        }
    }
    
    /**
     * 
     * @return \User Array
     */
    public function getSalesManagers() 
    {
        $userSQLConstructor = new UserSQLConstructor();
        //execute the SQL query and return records
        $result = mysqli_query(Database::$connection, "SELECT * FROM user_tbl WHERE user_level_tbl_level_id='3' AND usr_retire_user_flag = '0'");

        //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {             
            $array[$row['usr_name']] = $userSQLConstructor->createUser($row);
        }

        return $array;
    }
    
    
    
    /**
     * 
     * @param type $login
     * @param type $password
     * @param type $name
     * @param type $level
     * @param type $department
     * @param type $email
     * @param type $flag
     * @return type
     */
    public function setNewUser($login, $password, $name, $level, $department, $email, $flag, $limitToTerritory) 
    {
        $result = "INSERT INTO user_tbl (usr_login, usr_password, "
                . "usr_name, user_level_tbl_level_id, user_department_tbl_dept_id, usr_email, usr_initial_login, usr_limit_to_territory) "
                . "VALUES ("
                . "'".$login."', "
                . "'".md5($password)."', "
                . "'".$name."', "
                . "'".$level."', "
                . "'".$department."', "
                . "'".$email."', "
                . "'".$flag."', "
                . "'".$limitToTerritory."')";
        
        if (!mysqli_query(Database::$connection, $result))
        {
            debugWriter("debug.txt", "setNewUser FAILED $result ".mysqli_error(Database::$connection));
            return null;
        } else {
            return mysqli_insert_id(Database::$connection);
        }
    }
    
    /**
     * updateUser
     * @param User $user
     * @return boolean
     */
    public function updateUser($user) {
       // debugWriter("debug.txt", "userpassword ".$user->password);
        $passwordString = "";
        if (($user->password !== "")) {
            $passwordString = "usr_password = '".md5($user->password)."', ";
        }
        $result = "UPDATE user_tbl SET "
                . "usr_login = '".$user->login."', "
                . "$passwordString"
                . "usr_name = '".$user->name."', "
                . "user_level_tbl_level_id = '".$user->level."', "
                . "user_department_tbl_dept_id = '".$user->department."', "
                . "usr_email = '".$user->email."', "
                . "usr_initial_login = '".$user->initialLogin."', "
                . "usr_retire_user_flag = '".$user->retire."', "
                . "usr_limit_to_territory = '".$user->limitToTerritory."' "
                . "WHERE usr_id = '".$user->id."'";
        
        if (!mysqli_query(Database::$connection, $result))
        {
            debugWriter("debug.txt", "Update User Failed $result ".mysqli_error(Database::$connection) );
            return null;
        } else {
            return true;
        }
    }
    
    /**
     * Reset the users password and their initial login state.
     * @param String $password
     * @param INT $userID
     * @return boolean true/false
     */
    public function resetPasswordFirstLogin($password, $userID) {
        
        $result = "UPDATE user_tbl SET usr_password = md5('".$password."'), "
                . "usr_initial_login = '0' "
                . "WHERE usr_id = '".$userID."'";
        
        if (!mysqli_query(Database::$connection, $result))
        {
            debugWriter("debug.txt", "resetPasswordFirstLogin $result ".mysqli_error(Database::$connection) );
            return false;
        } else {
            $_SESSION['firstLogin'] = 0;
            return true;
        }
        
    }
    
    /**
     * Reset the users password
     * @param String $password
     * @param INT $userID
     * @return boolean true/false
     */
    public function resetPassword($password, $userID) {
        
        $result = "UPDATE user_tbl SET usr_password = md5('".$password."') "
                . "WHERE usr_id = '".$userID."'";
        
        if (!mysqli_query(Database::$connection, $result))
        {
            debugWriter("debug.txt", "resetPassword $result ".mysqli_error(Database::$connection) );
            return false;
        } else {
            $_SESSION['firstLogin'] = 0;
            return true;
        }
        
    }
}
