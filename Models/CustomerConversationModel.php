<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once("Models/Entities/CustomerConversation.php");
include_once 'Models/SQLConstructors/CustomerConversationSQLConstructor.php';
////include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of CustomerConversationModel
 *
 * @author Archie
 */
class CustomerConversationModel {
    
    /**
     * 
     * @param type $customerID
     * @return \CustomerContacts
     */
    public function getConversationsByCustomerID($customerID)
    {
        $customerConversationSQLConstructor = new CustomerConversationSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_conversation_tbl "
                . "WHERE customer_contacts_tbl_ict_id IN (SELECT ict_id FROM customer_contacts_tbl where customer_tbl_cust_id='".$customerID."') "
                . "ORDER BY con_date DESC");

        //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['con_id']] 
                    = $customerConversationSQLConstructor->createCustomerConversation($row);
        }
        if(isset($array))
        {
            return $array;
        } else {
            return null;
        }
    }
    
    
    public function getConversationsByCustomerIDPaged($customerID, $offset, $rowsPerPage)
    {
        $customerConversationSQLConstructor = new CustomerConversationSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_conversation_tbl "
                . "WHERE customer_contacts_tbl_ict_id IN (SELECT ict_id FROM customer_contacts_tbl where customer_tbl_cust_id='".$customerID."') "
                . "ORDER BY con_date DESC LIMIT $offset, $rowsPerPage");

        //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['con_id']] 
                    = $customerConversationSQLConstructor->createCustomerConversation($row);
        }
        if(isset($array))
        {
            return $array;
        } else {
            return null;
        }
    }
    
    
    /**
     * 
     * @param type $userID
     * @param type $entryDate
     * @param type $content
     * @param type $actionDueDate
     * @param type $actionType
     * @param type $contactID
     * @param type $campaignOverviewID
     * @param type $replyToConversationItemID
     * @return type
     */
    public function setConversationEntryFromUser($userID, $entryDate, $content, $actionDueDate, $actionType, 
            $contactID, $campaignOverviewID = 0, $replyToConversationItemID) {
     
        $insert = "INSERT INTO customer_conversation_tbl ("
                . "con_date, "
                . "con_content, "
                . "con_next_action_due_date, "
                . "con_action_type, "
                . "campaign_overview_tbl_cot_id, "
                . "con_reply_to_con_id, "
                . "customer_contacts_tbl_ict_id) "
                . "VALUES ("
                . "'$entryDate', "
                . "'$content', "
                . "'$actionDueDate', "
                . "'$actionType', "
                . "'$campaignOverviewID', "
                . "'$replyToConversationItemID', "
                . "'$contactID')";
        
        
        if (!mysqli_query(Database::$connection, $insert)) {
            return mysqli_error(Database::$connection);
        } else {
            $history = "INSERT INTO customer_conversation_history_tbl ("
                    . "ich_status, "
                    . "customer_conversation_tbl_con_id, "
                    . "user_tbl_usr_id) VALUES ("
                    . "'1', "
                    . "'".  mysqli_insert_id(Database::$connection)."', "
                    . "'".$userID."')";
            
            if (!mysqli_query(Database::$connection, $history)) {
                return null;
            } else {
                return true;
            }
        }
    }
    
    /**
     * 
     * @param type $conversationID
     * @param type $userID
     * @return boolean
     */
    public function closeConversationItem($conversationID, $userID) {
        
        $result = "INSERT INTO customer_conversation_history_tbl ("
                    . "ich_status, "
                    . "customer_conversation_tbl_con_id, "
                    . "user_tbl_usr_id) VALUES ("
                    . "'0', "
                    . "'" .  $conversationID . "', "
                    . "'" . $userID . "')";
            
            if (!mysqli_query(Database::$connection, $result)) {
                return null;
            } else {
                return true;
            }
    }
}