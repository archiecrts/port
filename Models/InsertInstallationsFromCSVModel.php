<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/Installation.php");
//include_once ("Models/DB.php");
include_once("Models/Database.php");

/**
 * Description of InsertInstallationsFromCSVModel
 *
 * @author Archie
 */
class InsertInstallationsFromCSVModel {

    // SET GLOBAL VARIABLES
    private $dbColumnName;
    private $newColumnNameArray;
    private $replace;

    /**
     * createTemporaryTable
     * @param Array $columnNameArray
     */
    private function createTemporaryTable($columnNameArray, $type) {

        if (($type != 'UNSPECIFIED') && ($type != 'inst_type')) {
            $typeString = " inst_type VARCHAR(10), ";
        } else {
            $typeString = "";
        }

        /*
         * Create a temporary table to contain the CSV data in the same order as the CSV.
         */
        $create_table_query = "CREATE TEMPORARY TABLE temporary_table (temp_id SERIAL, ";
        // Go through the column names array
        foreach ($columnNameArray as $p => $q) {
            $p = str_replace($this->replace, "_", $p); // Use the REGEX to replace bad characters with underscore

            $create_table_query .= " " . addslashes($p) . " VARCHAR(255), "; // This adds to the string that creates the table
            $this->dbColumnName .= " " . addslashes($p) . ", "; // This does similar for a select

            $key = str_replace($this->replace, "_", $p); // This shouldnt technically be needed but the code complained without it.
            $this->newColumnNameArray[$key] = $q; // create a new array with the new CSV column names
        }
        $create_table_query .= "$typeString PRIMARY KEY(temp_id))"; // set the primary key of the table


        /*
         * mysqli_query(Database::$connection, ) runs a sql query, putting ! in front of it says "if the query fails"
         */
        if (!mysqli_query(Database::$connection, $create_table_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't create table : " . $create_table_query . " \r\n" . mysqli_error(Database::$connection) . "");
        }
    }

    /**
     * createManualCheckTable
     * @param Array $columnNameArray
     */
    private function createManualCheckTable($columnNameArray, $type) {

        if (($type != 'UNSPECIFIED') && ($type != 'inst_type')) {
            $typeString = " inst_type VARCHAR(10), ";
        } else {
            $typeString = "";
        }

        /*
         * Create a temporary table to contain the CSV data in the same order as the CSV.
         */
        $create_manual_table_query = "CREATE TABLE IF NOT EXISTS manual_check_tbl (temp_id SERIAL, ";
        // Go through the column names array
        foreach ($columnNameArray as $p => $q) {
            $p = str_replace($this->replace, "_", $p); // Use the REGEX to replace bad characters with underscore

            if (($q != "") && ($q != "@dummy")) {
                $create_manual_table_query .= " " . addslashes($q) . " VARCHAR(45), "; // This adds to the string that creates the table
            }
        }
        $create_manual_table_query .= " $typeString matching_customer_id INT(11) UNSIGNED, matching_installation_id INT(11) UNSIGNED, PRIMARY KEY(temp_id))"; // set the primary key of the table

        /*
         * mysqli_query(Database::$connection, ) runs a sql query, putting ! in front of it says "if the query fails"
         */
        if (!mysqli_query(Database::$connection, $create_manual_table_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't create table : " . $create_manual_table_query . " \r\n" . mysqli_error(Database::$connection) . "");
        }
    }

    /**
     * loadFile
     * @param STRING $fileName
     * @param BOOLEAN $headers
     * @param STRING $type
     * @param STRING $dbColumnName
     */
    private function loadFile($fileName, $headers, $type) {
        // Add the CSV data to the temp table.
        // Ignore the first line in the file because these are column headers.
        if ($headers == 1) {
            $ignoreFirstLine = "IGNORE 1 LINES ";
        } else {
            $ignoreFirstLine = "";
        }

        // Location type might be in the csv, if its not then we manually set it for the whole file and that is
        // decided for the SQL statement here.
        if (($type != 'UNSPECIFIED') && ($type != 'inst_type')) {
            $typeString = "SET inst_type = '$type'";
        } else {
            $typeString = "";
        }

        $new_query_string = substr($this->dbColumnName, 0, -2); // removes the last character from the string (its a comma that shouldnt be there)
        // Load the data from the csv into the temporary table, using the column names generated on the verify page.
        $insert_temp_query = "LOAD DATA LOCAL INFILE '" . $fileName . "'
                INTO TABLE temporary_table
                CHARACTER SET 'latin1'
                COLUMNS TERMINATED BY ','
                ENCLOSED BY '\"' LINES TERMINATED BY '\r\n'
                $ignoreFirstLine ($new_query_string) $typeString";


        // DATABASE query generator. This say, if mysqli_query fails then write the first line. otherwise write the second.
        if (!mysqli_query(Database::$connection, $insert_temp_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't insert records into temporary table : " . $new_query_string . "*** " . mysqli_error(Database::$connection));
        } else {
           // debugWriter("loadInstallationDataLogs.txt", $fileName . " Inserted");
        }
    }

    /**
     * cleanTempData
     * @param type $fileName
     * @param type $columnNameArray
     * @param type $headers
     * @param type $type
     * @param type $dbColumnName
     * @param type $newColumnNameArray
     */
    private function cleanTempData() {
        $cleanQuery = "UPDATE temporary_table SET ";
        // clean out leading and trailing whitespaces 
        foreach ($this->newColumnNameArray as $key => $name) {
            $key = str_replace($this->replace, "_", $key);
            $cleanQuery .= " " . $key . "= TRIM(" . addslashes($key) . "), ";
        }
        $cleanedQuery = substr($cleanQuery, 0, -2);
        mysqli_query(Database::$connection, $cleanedQuery);



        // Do a quick count of the new records, This is currently used for debug, but may stay in the code.
        $quickCount = mysqli_query(Database::$connection, "SELECT count(*) FROM temporary_table");
        $countQuick = mysqli_fetch_assoc($quickCount);
        print "<p>Temp table Count: " . $countQuick['count(*)'] . ". Before duplicates are removed</p>";
    }

    /**
     * createCustomers
     * @param type $columnNameArray
     */
    private function createCustomers() {
        // customer name- its important that it is neither a duplicate or blank.
        // The database has a unique attribute attached to customer name.
        // Check all records inserted into the temporary table, if they are blank, assign a unique ID.
        // find everywhere there is no customer name created
        // array_search looks for the KEY in the array where the VALUE is equal to the array_search criteria
        $imo = "";
        $customerName = "";
        $installationName = "";
        if (array_search('inst_imo', $this->newColumnNameArray) != "") {
            $imo = ", " . array_search('inst_imo', $this->newColumnNameArray) . " AS inst_imo ";
        }
        if (array_search('cust_name', $this->newColumnNameArray) != "") {
            $customerName = ", " . array_search('cust_name', $this->newColumnNameArray) . " AS cust_name ";
        }
        if (array_search('inst_installation_name', $this->newColumnNameArray) != "") {
            $installationName = ", " . array_search('inst_installation_name', $this->newColumnNameArray) . " AS inst_installation_name ";
        }
        
        $emptyCustomer = "SELECT temp_id " . $customerName . "  "
                . "" . $installationName . "  "
                . "" . $imo . " "
                . "FROM temporary_table WHERE " . array_search('cust_name', $this->newColumnNameArray) . " = ''";

        $emptyCustomerQuery = mysqli_query(Database::$connection, $emptyCustomer);

        
        while ($emptyCustomerQueryRow = mysqli_fetch_assoc($emptyCustomerQuery)) {
            // Clean the IMO field to remove dashes and #NAME entries.
            if (isset($emptyCustomerQueryRow['inst_imo'])) {
                if ($emptyCustomerQueryRow['inst_imo'] == "-") {
                    $emptyCustomerQueryRow['inst_imo'] = "";
                }
                if ($emptyCustomerQueryRow['inst_imo'] == '#NAME') {
                    $emptyCustomerQueryRow['inst_imo'] = "";
                }
            }

            /*
             * DOES THE INSTALLATION EXIST FOR THE EMPTY CUSTOMER NAME IN THE CUSTOMER HAS INSTALLATION TABLE.
             * IF IT DOES THEN THE CUSTOMER DOES NOT NEED TO BE ADDED AGAIN.
             */
            $checkInstallationsForPreExistance = mysqli_query(Database::$connection, "SELECT cust_name FROM customer_tbl "
                    . "WHERE cust_id = (SELECT customer_tbl_cust_id FROM customer_tbl_has_installations_tbl WHERE "
                    . "installations_tbl_inst_id IN (SELECT inst_id FROM installations_tbl WHERE "
                    . "inst_installation_name = '" . addslashes($emptyCustomerQueryRow['inst_installation_name']) . "' AND "
                    . "inst_imo = '" . addslashes($emptyCustomerQueryRow['inst_imo']) . "'))");

            $checkInstallationsForPreExistanceRow = mysqli_fetch_assoc($checkInstallationsForPreExistance);

            // it exists
            if (isset($checkInstallationsForPreExistanceRow['cust_name'])) {

                // reset the temp customer name to be the one stored in the customer table
                $updateEmptyName = "UPDATE temporary_table SET " . array_search('cust_name', $this->newColumnNameArray) . " = "
                        . "'" . $checkInstallationsForPreExistanceRow['cust_name'] . "' WHERE temp_id = '" . $emptyCustomerQueryRow['temp_id'] . "'";

                if (!mysqli_query(Database::$connection, $updateEmptyName)) {
                    debugWriter("loadInstallationDataLogs.txt", "Cannot update empty customer name " . mysqli_error(Database::$connection));
                }
            } else {
                // It does not exist in the DB so give it a unique name
                $updateEmptyName = "UPDATE temporary_table SET " . array_search('cust_name', $this->newColumnNameArray) . " = "
                        . "'" . uniqid("UNKNOWN.") . "' WHERE temp_id = '" . $emptyCustomerQueryRow['temp_id'] . "'";

                if (!mysqli_query(Database::$connection, $updateEmptyName)) {
                    debugWriter("loadInstallationDataLogs.txt", "Cannot update empty customer name " . mysqli_error(Database::$connection));
                }
            }
        }



        // start SQL statement to get all customer details.
        $customerQuery = "SELECT " . array_search('cust_name', $this->newColumnNameArray) . " AS cust_name ";
        $custOfficeName = "";
        $custAddress = "";
        $custCity = "";
        $custState = "";
        $custRegion = "";
        $custCountry = "";
        $custPostcode = "";

        foreach ($this->newColumnNameArray as $key => $name) {
            $key = str_replace($this->replace, "_", $key);
            if ($name == 'cust_telephone') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cust_fax') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cust_website') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cadt_office_name') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
                $custOfficeName = ", cadt_office_name";
            }
            if ($name == 'cadt_address') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
                $custAddress = ", cadt_address";
            }
            if ($name == 'cadt_city') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
                $custCity = ", cadt_city";
            }
            if ($name == 'cadt_state') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
                $custState = ", cadt_state";
            }
            if ($name == 'cadt_region') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
                $custRegion = ", cadt_region";
            }
            if ($name == 'cadt_country') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
                $custCountry = ", cadt_country";
            }
            if ($name == 'cadt_postcode') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
                $custPostcode = ", cadt_postcode";
            }
            if ($name == 'cust_type') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
        }

        $customerQuery .= " FROM temporary_table GROUP BY cust_name $custOfficeName $custAddress $custCity $custState $custRegion $custCountry $custPostcode";
        
        //debugWriter("debug.txt", "Customer Query ".$customerQuery);
        $customerInsert = mysqli_query(Database::$connection, $customerQuery);

        $newCustomerCount = 0;
        $newAddressCount = 0;

        while ($customerQueryRow = mysqli_fetch_assoc($customerInsert)) {
            //debugWriter("debug.txt", "customer Name ".$customerQueryRow['cust_name']);
            // Check the customer_tbl for entries that match
            $checkCustomerTablequery = mysqli_query(Database::$connection, "SELECT count(cust_name) FROM customer_tbl WHERE "
                    . "cust_name = '" . addslashes($customerQueryRow['cust_name']) . "'");

            $checkCustomerTable = mysqli_fetch_assoc($checkCustomerTablequery);
            //debugWriter("debug.txt", "Line 282 checkCustomerTableQuery".mysqli_error(Database::$connection));
            $customerID = "";

            // If there is no match because the number of entries is less than 1, then proceed.
            if ($checkCustomerTable['count(cust_name)'] < 1) {
                //debugWriter("debug.txt", "new entry for customer");
                /*
                 * Create a new entry.
                 */
                $insertCustomer = "INSERT INTO customer_tbl (cust_name, cust_telephone, cust_fax,"
                        . "cust_website, cust_account_manager, cust_tbh_id, "
                        . "cust_tbh_account_code, cust_ld_id, cust_ld_account_code, cust_header_notes) VALUES ("
                        . "'" . addslashes((isset($customerQueryRow['cust_name']) ? $customerQueryRow['cust_name'] : "")) . "',"
                        . "'" . addslashes((isset($customerQueryRow['cust_telephone']) ? $customerQueryRow['cust_telephone'] : "")) . "', "
                        . "'" . addslashes((isset($customerQueryRow['cust_fax']) ? $customerQueryRow['cust_fax'] : "")) . "', "
                        . "'" . addslashes((isset($customerQueryRow['cust_website']) ? $customerQueryRow['cust_website'] : "")) . "', "
                        . "NULL, "
                        . "'0', "
                        . "'0', "
                        . "'0', "
                        . "'0', "
                        . "'')";

                if (!mysqli_query(Database::$connection, $insertCustomer)) {
                    debugWriter("loadInstallationDataLogs.txt", "Cannot Insert Customer : " . mysqli_error(Database::$connection));
                } else {
                    $newCustomerCount++;
                }
                $customerID = mysqli_insert_id(Database::$connection);

                $customerType = '1';
                if (isset($customerQueryRow['cust_type'])) {

                    $selectStatus = mysqli_query(Database::$connection, "SELECT cslt_id FROM customer_status_lookup_tbl "
                            . "WHERE cslt_status = '" . $customerQueryRow['cust_type'] . "'");
                    $statusRow = mysqli_fetch_assoc($selectStatus);
                    $customerType = $statusRow['cslt_id'];
                }
                // Status is held in a separate table because there can be 1 or many
                $insertCustomerStatus = "INSERT INTO customer_status_tbl "
                        . "(customer_tbl_cust_id, customer_status_lookup_tbl_cslt_id) "
                        . "VALUES "
                        . "('" . $customerID . "', "
                        . "'" . $customerType . "')";
                if (!mysqli_query(Database::$connection, $insertCustomerStatus)) {
                    debugWriter("loadInstallationDataLogs.txt", "Cannot Insert Customer Status : " . mysqli_error(Database::$connection));
                }


                /*
                 * Create an entry into the address table.
                 */

                $insertAddress = "INSERT INTO customer_address_tbl (cadt_office_name, cadt_address, "
                        . "cadt_city, cadt_state, cadt_region, cadt_country, cadt_postcode, cadt_address_verified, "
                        . "customer_tbl_cust_id, cadt_archived, cadt_default_address) "
                        . "VALUES ("
                        . "'" . addslashes((isset($customerQueryRow['cadt_office_name']) ? $customerQueryRow['cadt_office_name'] : "Office")) . "', "
                        . "'" . addslashes((isset($customerQueryRow['cadt_address']) ? $customerQueryRow['cadt_address'] : "")) . "', "
                        . "'" . addslashes((isset($customerQueryRow['cadt_city']) ? $customerQueryRow['cadt_city'] : "")) . "', "
                        . "'" . addslashes((isset($customerQueryRow['cadt_state']) ? $customerQueryRow['cadt_state'] : "")) . "', "
                        . "'" . addslashes((isset($customerQueryRow['cadt_region']) ? $customerQueryRow['cadt_region'] : "")) . "', "
                        . "'" . addslashes((isset($customerQueryRow['cadt_country']) ? $customerQueryRow['cadt_country'] : "")) . "', "
                        . "'" . addslashes((isset($customerQueryRow['cadt_postcode']) ? $customerQueryRow['cadt_postcode'] : "")) . "', "
                        . "'0', "
                        . "'" . $customerID . "', "
                        . "'0', "
                        . "'1')";

                if (!mysqli_query(Database::$connection, $insertAddress)) {
                    debugWriter("loadInstallationDataLogs.txt", "Cannot Insert Customer Address : " . mysqli_error(Database::$connection));
                } else {
                    $newAddressCount++;
                }
            } else {
               // debugWriter("debug.txt", "existing entry for customer");
                // Get the customer ID.
                $getCustomerID = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl WHERE "
                        . "cust_name = '" . addslashes($customerQueryRow['cust_name']) . "'");

                $getCustomerIDRow = mysqli_fetch_assoc($getCustomerID);
                $customerID = $getCustomerIDRow['cust_id'];
                //debugWriter("debug.txt", "customer id: ".$customerID);

                // Get a count of all addresses currently attached to that customer ID
                $customerAddress = (isset($customerQueryRow['cadt_address']) ? $customerQueryRow['cadt_address'] : ""); // This is a Ternary Logic statement it reads as (statement ? true : false). if the statement is true do true, else do false.
                $customerCity = (isset($customerQueryRow['cadt_city']) ? $customerQueryRow['cadt_city'] : "");
                $customerState = (isset($customerQueryRow['cadt_state']) ? $customerQueryRow['cadt_state'] : "");
                $customerRegion = (isset($customerQueryRow['cadt_region']) ? $customerQueryRow['cadt_region'] : "");
                $customerCountry = (isset($customerQueryRow['cadt_country']) ? $customerQueryRow['cadt_country'] : "");
                $customerPostcode = (isset($customerQueryRow['cadt_postcode']) ? $customerQueryRow['cadt_postcode'] : "");

                //debugWriter("debug.txt", $customerAddress." ".$customerCity." ".$customerState." ".$customerCountry." ".$customerPostcode);
                // So the customer exists, but the address might be new. Check for that.
                $checkAddress = mysqli_query(Database::$connection, "SELECT count(cadt_id) FROM customer_address_tbl "
                        . "WHERE customer_tbl_cust_id = '" . $customerID . "' AND "
                        . "cadt_address = '" . addslashes($customerAddress) . "' AND "
                        . "cadt_city = '" . addslashes($customerCity) . "' AND "
                        . "cadt_state = '" . addslashes($customerState) . "' AND "
                        . "cadt_region = '" . addslashes($customerRegion) . "' AND "
                        . "cadt_country = '" . addslashes($customerCountry) . "' AND "
                        . "cadt_postcode = '" . addslashes($customerPostcode) . "'");

                $checkAddressRow = mysqli_fetch_assoc($checkAddress);


                if ($checkAddressRow['count(cadt_id)'] < 1) {
                  //  debugWriter("debug.txt", "address not found");
                    $insertAddress = "INSERT INTO customer_address_tbl (cadt_office_name, cadt_address, "
                            . "cadt_city, cadt_state, cadt_region, cadt_country, cadt_postcode, cadt_address_verified, "
                            . "customer_tbl_cust_id, cadt_archived, cadt_default_address) "
                            . "VALUES ("
                            . "'" . addslashes((isset($customerQueryRow['cadt_office_name']) ? $customerQueryRow['cadt_office_name'] : "Office")) . "', "
                            . "'" . addslashes((isset($customerQueryRow['cadt_address']) ? $customerQueryRow['cadt_address'] : "")) . "', "
                            . "'" . addslashes((isset($customerQueryRow['cadt_city']) ? $customerQueryRow['cadt_city'] : "")) . "', "
                            . "'" . addslashes((isset($customerQueryRow['cadt_state']) ? $customerQueryRow['cadt_state'] : "")) . "', "
                            . "'" . addslashes((isset($customerQueryRow['cadt_region']) ? $customerQueryRow['cadt_region'] : "")) . "', "
                            . "'" . addslashes((isset($customerQueryRow['cadt_country']) ? $customerQueryRow['cadt_country'] : "")) . "', "
                            . "'" . addslashes((isset($customerQueryRow['cadt_postcode']) ? $customerQueryRow['cadt_postcode'] : "")) . "', "
                            . "'0', "
                            . "'" . $customerID . "', "
                            . "'0', "
                            . "'0')";

                    if (!mysqli_query(Database::$connection, $insertAddress)) {
                        debugWriter("loadInstallationDataLogs.txt", "Cannot Insert Customer Address : " . mysqli_error(Database::$connection));
                    } else {
                        $newAddressCount++;
                    }
                }
                
                
                // now check if the status matches and if not add a new status.
                $customerType = "1";
                if (isset($customerQueryRow['cust_type'])) {
                    $selectStatus = mysqli_query(Database::$connection, "SELECT cslt_id FROM customer_status_lookup_tbl "
                    . "WHERE cslt_status = '" . $customerQueryRow['cust_type'] . "'");
                    $statusRow = mysqli_fetch_assoc($selectStatus);
                    $customerType = $statusRow['cslt_id'];
                }

                  

                $checkCustomerStatusInfo = mysqli_query(Database::$connection, "SELECT count(cst_id) FROM customer_status_tbl "
                . "WHERE customer_tbl_cust_id = '" . $customerID . "' "
                . "AND customer_status_lookup_tbl_cslt_id = '" . $customerType . "'");

                $checkCustomerStatusInfoRow = mysqli_fetch_assoc($checkCustomerStatusInfo); 

                if ($checkCustomerStatusInfoRow['count(cst_id)'] == 0) {
                  //  debugWriter("debug.txt", "status count " . $checkCustomerStatusInfoRow['count(cst_id)']);
                  //print "add to manual table";
                    $insertNewStatus = "INSERT INTO customer_status_tbl (customer_tbl_cust_id, "
                            . "customer_status_lookup_tbl_cslt_id) "
                            . "VALUES ('".$customerID."', '".$customerType."')";
                    
                    if (!mysqli_query(Database::$connection, $insertNewStatus)) {
                        debugWriter("debug.txt", "COULD NOT ADD NEW CUSTOMER STATUS TBL ENTRY". mysqli_error(Database::$connection));
                    }

                }
            }
        }

        print "<p>Added " . $newCustomerCount . " new customers</p>";
        print "<p>Added " . $newAddressCount . " new addresses</p>";
    }

    /**
     * createContacts
     */
    private function createContacts() {
        // Create the select statement
        $contactsString = "SELECT " . array_search('cust_name', $this->newColumnNameArray) . " AS cust_name ";


        foreach ($this->newColumnNameArray as $key => $name) {
            $key = str_replace($this->replace, "_", $key);
            // Find all the relevant columns (up to three contacts is possible in uploadInstallationsTest.php)
            // the contact details have a _1, _2 or _3 after each name which is why I use substr()
            if (substr($name, 0, -2) == 'ict_salutation') {
                $contactsString .= ", " . addslashes($key) . " AS '" . $name . "' "; // AS works to create an alias for the column name, to make it easier to handle.
            }
            if (substr($name, 0, -2) == 'ict_first_name') {
                $contactsString .= ", " . addslashes($key) . " AS '" . $name . "' "; // AS works to create an alias for the column name, to make it easier to handle.
            }
            if (substr($name, 0, -2) == 'ict_surname') {
                $contactsString .= ", " . addslashes($key) . " AS '" . $name . "' "; // AS works to create an alias for the column name, to make it easier to handle.
            }
            if (substr($name, 0, -2) == 'ict_job_title') {
                $contactsString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if (substr($name, 0, -2) == 'ict_email') {
                $contactsString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if (substr($name, 0, -2) == 'ict_number') {
                $contactsString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if (substr($name, 0, -2) == 'ict_mobile_number') {
                $contactsString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if (substr($name, 0, -2) == 'ict_info_source') {
                $contactsString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
        }
        $contactsString .= " FROM temporary_table";

        $contactsQuery = mysqli_query(Database::$connection, $contactsString); // get the records from the temporary table.
        // Start count of customers added
        $contactCount = 0;

        // Loop through the resultant records and perform the same actions on each row.
        while ($contactsQueryRow = mysqli_fetch_assoc($contactsQuery)) {
            // Create an array for each customer name and split the possible contacts into the array
            $contact = []; // empty array.
            $customerID = ""; // empty customer ID
            foreach ($contactsQueryRow as $k => $r) {
                if ($k != "cust_name") {
                    $id = substr($k, -1);
                    $contact[$contactsQueryRow['cust_name']][$id][substr($k, 0, -2)] = addslashes($r);
                } else {
                    // Get the customers ID
                    $getCustomerIDQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl WHERE "
                            . "cust_name = '" . addslashes($r) . "'");

                    $customerIDRow = mysqli_fetch_assoc($getCustomerIDQuery);
                    $customerID = $customerIDRow['cust_id'];
                }
            }

            if (isset($contact[$contactsQueryRow['cust_name']])) {
                foreach ($contact[$contactsQueryRow['cust_name']] as $id => $columnName) {

                    $columnsString = "";
                    $valuesString = "";
                    $emptyRecord = 0; // this counts if the record has all empty columns and thus should not be inserted.
                    // check for existing customer contact records that match the new one to be inserted.
                    // Also check the history date to see when it was added.
                    $checkContacts = "SELECT count(*) FROM customer_contacts_tbl "
                            . "WHERE customer_tbl_cust_id = '" . $customerID . "' ";

                    foreach ($columnName as $col => $val) {
                        $columnsString .= "$col, ";
                        if ($val == '-' || $val == '#NAME?') { // if the data is useless then reset $val to be an empty string
                            $val = "";
                        }

                        if ($val != "") {
                            $val = addslashes($val);
                            $emptyRecord++;
                        }
                        $valuesString .= "'" . $val . "', ";
                        $checkContacts .= "AND $col = '$val' ";
                    }

                    if ($emptyRecord > 0) {

                        $checkContactsResult = mysqli_query(Database::$connection, $checkContacts);
                        if (!mysqli_query(Database::$connection, $checkContacts)) {
                            debugWriter("debug.txt", "INSERT FROM CSV FAILED : " . $checkContacts . " " . mysqli_error(Database::$connection));
                        }
                        $rowCC = mysqli_fetch_assoc($checkContactsResult);

                        if ($rowCC['count(*)'] < 1) {
                            // create an insert statement.
                            $sqlString = "INSERT INTO customer_contacts_tbl ($columnsString "
                                    . "ict_inactive, ict_contact_validated, customer_tbl_cust_id, customer_address_tbl_cadt_id) "
                                    . "VALUES ($valuesString "
                                    . "'0', '0', '" . $customerID . "', NULL)";
                            if (!mysqli_query(Database::$connection, $sqlString)) {
                                debugWriter("debug.txt", "INSERT customer_contacts_tbl FAILED : " . $sqlString . " " . mysqli_error(Database::$connection));
                            } else {
                                $contactCount++;
                            }

                            // If the record contains something useful then we add it to the database.

                            $customerContactsID = mysqli_insert_id(Database::$connection);
                            // Insert a record into the history table as well.
                            $history = "INSERT INTO customer_contacts_history_tbl "
                                    . "(customer_contacts_tbl_ict_id, ccht_action, ccht_date, user_tbl_usr_id, ccht_origin) "
                                    . "VALUES ('" . $customerContactsID . "','Created', NOW(), '" . $_SESSION['user_id'] . "', 'CSV')";

                            if (!mysqli_query(Database::$connection, $history)) {
                                debugWriter("debug.txt", "INSERT HISTORY FAILED : " . $history . " " . mysqli_error(Database::$connection));
                            } else {
                                $customerContactsHistoryID = mysqli_insert_id(Database::$connection);
                                // add the address to the contact using the link table.
                                $checkAddress = mysqli_query(Database::$connection, "SELECT cadt_id FROM customer_address_tbl "
                                        . "WHERE customer_tbl_cust_id = '" . $customerID . "' "
                                        . "AND cadt_default_address = '1'");

                                $checkAddressRow = mysqli_fetch_assoc($checkAddress);

                                $addressHasContactsHistory = "INSERT INTO customer_address_tbl_has_customer_contacts_history_tbl "
                                        . "(customer_address_tbl_cadt_id, customer_contacts_history_tbl_ccht_id) "
                                        . "VALUES ('" . $checkAddressRow['cadt_id'] . "', '" . $customerContactsHistoryID . "')";

                                if (!mysqli_query(Database::$connection, $addressHasContactsHistory)) {
                                    debugWriter("debug.txt", "INSERT ADDRESS/HISTORY FAILED : " . $addressHasContactsHistory . " " . mysqli_error(Database::$connection));
                                }
                            }
                        } else {
                            // TODO put it somewhere else to be looked at manually.
                        }
                    }
                }
            }
        }
        print "<p>Added " . $contactCount . " new contacts<p/>";
    }

    /**
     * createInstallations
     */
    private function createInstallations() {
        $installationString = "SELECT " . array_search('cust_name', $this->newColumnNameArray) . " AS cust_name ";


        foreach ($this->newColumnNameArray as $key => $name) {
            $key = str_replace($this->replace, "_", $key);
            // Find all the relevant columns (up to three contacts is possible in uploadInstallationsTest.php)
            if ($name == 'inst_type') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_installation') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_imo') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_installation_name') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_year') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'installation_status_tbl_stat_id') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_owner_parent_company') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_technical_manager_company') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_original_source') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_original_source_date') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_photo_link') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_hull_number') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_hull_type') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_year_built') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_deadweight') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_length_overall') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_beam_extreme') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_beam_moulded') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_built_date') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_draught') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_gross_tonnage') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_length') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_propeller_type') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_propulsion_unit_count') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_ship_builder') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_teu') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_build_year') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            
            // Select this stuff to match the address from the addresses table and link the address to the installation
            if ($name == 'cust_telephone') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cust_fax') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cust_website') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cadt_office_name') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cadt_address') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cadt_city') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cadt_state') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cadt_region') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cadt_country') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cadt_postcode') {
                $installationString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
        }
        $installationString .= ", temp_id FROM temporary_table";

        $installationQuery = mysqli_query(Database::$connection, $installationString); // get the records from the temporary table.
        // Start count of installations
        $installationCount = 0;
        $tempID = '';
        $customerID = "";
        // Loop through the resultant records and perform the same actions on each row.
        while ($installationQueryRow = mysqli_fetch_assoc($installationQuery)) {

            $installation = []; // empty array.
            //$customerID = ""; // empty customer ID
            //$addressID = ''; // empty address ID
            if (isset($installationQueryRow['temp_id'])) {
                $tempID = $installationQueryRow['temp_id'];
            }
            
                    
            /**
             * Set the customer name, or find its ID.
             */
            foreach ($installationQueryRow as $s => $t) {
                if ($s != "temp_id" && $s != 'cust_telephone' && $s != 'cust_fax' 
                        && $s != 'cust_website' 
                        && $s != 'cadt_office_name' 
                        && $s != 'cadt_address' 
                        && $s != 'cadt_city' 
                        && $s != 'cadt_state' 
                        && $s != 'cadt_region' 
                        && $s != 'cadt_country' 
                        && $s != 'cadt_postcode' ) {
                    if (($s != "cust_name") && ($s != "inst_owner_parent_company")
                            && ($s != 'inst_technical_manager_company') && 
                            ($s != 'inst_original_source_date')
                            && ($s != 'inst_built_date') 
                            && ($s != 'inst_build_year')) {
                        $installation[$installationQueryRow['cust_name']][$s] = $t;
                    } else if ($s == "inst_original_source_date") {
                        //debugWriter("debug.txt", "Original Source Date ".$t." - ".$t."-01-01 00:00:00");
                        $installation[$installationQueryRow['cust_name']][$s] = $t."-01-01 00:00:00";
                    } else if ($s == "inst_built_date") {
                        //debugWriter("debug.txt", "inst_built_date ".$t." - ".$t."-01-01 00:00:00");
                        $installation[$installationQueryRow['cust_name']][$s] = $t."-01-01 00:00:00";
                    } else if ($s == "inst_build_year") {
                        //debugWriter("debug.txt", "inst_build_year ".$t." - ".$t."-01-01 00:00:00");
                        $installation[$installationQueryRow['cust_name']][$s] = $t."-01-01 00:00:00";
                    } else if ($s == "cust_name") {
                    
                        // Get the customers ID
                        $getCustomerIDQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl WHERE "
                                . "cust_name = '" . addslashes($t) . "'");

                        $customerIDRow = mysqli_fetch_assoc($getCustomerIDQuery);
                        $customerID = $customerIDRow['cust_id'];
                    } else if ($s == "inst_owner_parent_company") {
                        // get owner ID here and make $t = the ID not the name.
                        //debugWriter("debug.txt", "OWNER PARENT DATA : ". $t);
                        $getOwnerIDQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl WHERE "
                                . "cust_name = '" . addslashes($t) . "'");

                        $ownerIDRow = mysqli_fetch_assoc($getOwnerIDQuery);
                        $ownerID = $ownerIDRow['cust_id'];
                        /// CREATE OWNER PARENT AND TECHNICAL.
                        if ($ownerID == "" || !isset($ownerID) && ($t != "")) {
                            $insertNewOwnerParent = "INSERT INTO customer_tbl (cust_name) VALUES ('".addslashes($t)."')";
                            if (!mysqli_query(Database::$connection, $insertNewOwnerParent)) {
                                debugWriter("debug.txt", "INSERT OWNER PARENT FAILED : " . $insertNewOwnerParent . " " . mysqli_error(Database::$connection));
                            }
                            $ownerID = mysqli_insert_id(Database::$connection);
                            $insertIntoStatusTbl = "INSERT INTO customer_status_tbl (customer_tbl_cust_id, customer_status_lookup_tbl_cslt_id) "
                                    . "VALUES ('$ownerID', '1')";
                            if (!mysqli_query(Database::$connection, $insertIntoStatusTbl)) {
                                debugWriter("debug.txt", "INSERT OWNER PARENT Status FAILED : " . $insertIntoStatusTbl . " " . mysqli_error(Database::$connection));
                            }
                        }
                        $installation[$installationQueryRow['cust_name']][$s] = $ownerID;
                    } else if ($s == "inst_technical_manager_company") {
                        // get owner ID here and make $t = the ID not the name.
                        //debugWriter("debug.txt", "OWNER PARENT DATA : ". $t);
                        $getTechIDQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl WHERE "
                                . "cust_name = '" . addslashes($t) . "'");

                        $techIDRow = mysqli_fetch_assoc($getTechIDQuery);
                        $techID = $techIDRow['cust_id'];
                        if ($techID == "" || !isset($techID) && ($t != "")) {
                            $insertNewOwnerParent = "INSERT INTO customer_tbl (cust_name) VALUES ('".addslashes($t)."')";
                            if (!mysqli_query(Database::$connection, $insertNewOwnerParent)) {
                                debugWriter("debug.txt", "INSERT OWNER PARENT FAILED : " . $insertNewOwnerParent . " " . mysqli_error(Database::$connection));
                            }
                            $techID = mysqli_insert_id(Database::$connection);
                            $insertIntoStatusTbl = "INSERT INTO customer_status_tbl (customer_tbl_cust_id, customer_status_lookup_tbl_cslt_id) "
                                    . "VALUES ('$techID', '1')";
                            if (!mysqli_query(Database::$connection, $insertIntoStatusTbl)) {
                                debugWriter("debug.txt", "INSERT OWNER PARENT Status FAILED : " . $insertIntoStatusTbl . " " . mysqli_error(Database::$connection));
                            }
                        }
                        $installation[$installationQueryRow['cust_name']][$s] = $techID;
                    }
                }
            }
            // get the address
            $selectAddressID = "SELECT cadt_id FROM customer_address_tbl "
                            . "WHERE customer_tbl_cust_id = '".$customerID."' AND ";
            
            foreach ($installationQueryRow as $s => $t) {
                if ($s == 'cust_telephone') {
                        $selectAddressID .= " " . addslashes($s) . " = '" . addslashes($t) . "' AND ";
                    }
                    if ($s == 'cust_fax') {
                        $selectAddressID .= " " . addslashes($s) . " = '" . addslashes($t) . "' AND ";
                    }
                    if ($s == 'cust_website') {
                        $selectAddressID .= " " . addslashes($s) . " = '" . addslashes($t) . "' AND ";
                    }
                    if ($s == 'cadt_office_name') {
                        $selectAddressID .= " " . addslashes($s) . " = '" . addslashes($t) . "' AND ";
                    }
                    if ($s == 'cadt_address') {
                        $selectAddressID .= " " . addslashes($s) . " = '" . addslashes($t) . "' AND ";
                    }
                    if ($s == 'cadt_city') {
                        $selectAddressID .= " " . addslashes($s) . " = '" . addslashes($t) . "' AND ";
                    }
                    if ($s == 'cadt_state') {
                        $selectAddressID .= " " . addslashes($s) . " = '" . addslashes($t) . "' AND ";
                    }
                    if ($s == 'cadt_region') {
                        $selectAddressID .= " " . addslashes($s) . " = '" . addslashes($t) . "' AND ";
                    }
                    if ($s == 'cadt_country') {
                        $selectAddressID .= " " . addslashes($s) . " = '" . addslashes($t) . "' AND ";
                    }
                    if ($s == 'cadt_postcode') {
                        $selectAddressID .= " " . addslashes($s) . " = '" . addslashes($t) . "' AND ";
                    }
            }
            $selectAddressID =  substr($selectAddressID, 0, -4);
           
            $selectAddressQuery = mysqli_query(Database::$connection, $selectAddressID);
            $rowAddr = mysqli_fetch_assoc($selectAddressQuery);
            $addressID = $rowAddr['cadt_id']; 
            if (mysqli_error(Database::$connection)) {
                debugWriter("debug.txt", "$selectAddressID select address error ".mysqli_error(Database::$connection));
            }
            /*
             * check if installation name exists in the database, and check the customer has installation table
             * to see if the record exists there. If the installation exists but not with a customer, then it
             * should be attached to the customer. If it exists but with a different customer, then we need to
             * either swap the customers and add the record change to the history table, or we need to do something
             * manually to decide which customer the installation attaches to.
             */

            // Clean the customer IMO if necessary
            $imoString = "";
            $installationID = "";
            if (isset($installation[$installationQueryRow['cust_name']]['inst_imo'])) {
                if ($installation[$installationQueryRow['cust_name']]['inst_imo'] == "-") {
                    $installation[$installationQueryRow['cust_name']]['inst_imo'] = "";
                }
                if ($installation[$installationQueryRow['cust_name']]['inst_imo'] == '#NAME') {
                    $installation[$installationQueryRow['cust_name']]['inst_imo'] = "";
                }
                if ($installation[$installationQueryRow['cust_name']]['inst_imo'] != "") {
                    $imoString = "AND inst_imo = '" . addslashes($installation[$installationQueryRow['cust_name']]['inst_imo']) . "'";
                }
            }

            /**
             * Search the table by installation name and IMO (as necessary)
             */
            if (isset($installation[$installationQueryRow['cust_name']]['inst_installation_name'])) {
                $checkInstallationExists = mysqli_query(Database::$connection, "SELECT inst_id, inst_imo FROM installations_tbl "
                        . "WHERE inst_installation_name = '" . addslashes($installation[$installationQueryRow['cust_name']]['inst_installation_name']) . "' $imoString");

                $checkInstallationExistsRow = mysqli_fetch_assoc($checkInstallationExists);
                $installationID = $checkInstallationExistsRow['inst_id'];
                //debugWriter("debug.txt", "GETTING INST ID AND CUST ID FOR MANUAL CHECK ". $installationID);
            }

            $columnsString = "";
            $valuesString = "";
            $emptyRecord = 0;
            $locationType = "";

            // the installation doesnt exist.
            if ($installationID == "") {
                $statIDCount = 0;
                if (isset($installation[$installationQueryRow['cust_name']])) {
                    foreach ($installation[$installationQueryRow['cust_name']] as $col => $val) {
                    if ($col == 'inst_type') {
                        $locationType = strtoupper($val);
                    }
                    $columnsString .= "$col, ";
                    if ($val == '-' || $val == '#NAME?') { // if the data is useless then reset $val to be an empty string
                        $val = "";
                    }

                    if ($val != "") {
                        if ($col == 'installation_status_tbl_stat_id') {
                            $statIDCount = 1;
                            // this value needs to be taken from the loop-up table.
                            $statusQuery = mysqli_query(Database::$connection, "SELECT stat_id FROM installation_status_tbl WHERE stat_type = '" . $locationType . "'
                                        AND MATCH (stat_status_description)
                                        AGAINST ('" . addslashes($val) . "' IN NATURAL LANGUAGE MODE)");
                            $statusRow = mysqli_fetch_assoc($statusQuery);

                            if (!isset($statusRow['stat_id'])) {
                                if ($locationType == 'LAND') {
                                    $val = 1;
                                } else {
                                    $val = 5;
                                }
                            } else {
                                $val = $statusRow['stat_id'];
                            }
                        }
                        $val = addslashes($val);
                        $emptyRecord++;
                    }

                    $valuesString .= "'" . $val . "', ";
                }
                }

                if ($emptyRecord > 0) {

                    if ($statIDCount == 0) {
                        $columnsString .= "installation_status_tbl_stat_id, ";
                        $valuesString .= "'1', ";
                    }
                    
                    $addressIDCol = "";
                    $addressIDVal = "";
                    if (isset($addressID) && $addressID != "") {
                        $addressIDCol = ', customer_address_tbl_cadt_id';
                        $addressIDVal = ', '. $addressID;
                    
                        $insertInstallation = "INSERT INTO installations_tbl (" . substr($columnsString, 0, -2) . " ".$addressIDCol.") VALUES (" . substr($valuesString, 0, -2) . " $addressIDVal)";

                        if (!mysqli_query(Database::$connection, $insertInstallation)) {
                            debugWriter("debug.txt", "insertInstallation FAILED : $insertInstallation " . mysqli_error(Database::$connection));
                        } else {
                            $installationCount++;
                        }
                    }
                    $insertCustomerHasInstallation = "INSERT INTO customer_tbl_has_installations_tbl "
                            . "(customer_tbl_cust_id, installations_tbl_inst_id) VALUES ('$customerID', '" . mysqli_insert_id(Database::$connection) . "')";

                    if (!mysqli_query(Database::$connection, $insertCustomerHasInstallation)) {
                        debugWriter("debug.txt", "insertCustomerHasInstallation FAILED : $insertCustomerHasInstallation " . mysqli_error(Database::$connection));
                    }
                }
            } else {
                // the installation exists.
                // BUT it might not have all the data in this new record. Check things like the technical manager and stuff
                // this may be a different engine type.

                $checkCustomerHasInstallation = mysqli_query(Database::$connection, "SELECT customer_tbl_cust_id "
                        . "FROM customer_tbl_has_installations_tbl "
                        . "WHERE installations_tbl_inst_id = '$installationID'");

                $checkCustomerHasInstallationRow = mysqli_fetch_assoc($checkCustomerHasInstallation);

                // The new customer ID and the previous customer ID for this installation do not match
                if ($customerID != $checkCustomerHasInstallationRow['customer_tbl_cust_id']) {

                    // Get the customer name of the new record
                    $select = mysqli_query(Database::$connection, "SELECT cust_name FROM customer_tbl WHERE cust_id = '" . $customerID . "'");
                    $selectRow = mysqli_fetch_assoc($select);

                    // Check if the name contains UNKNOWN (ie an empty customer name)
                    if (strpos($selectRow['cust_name'], 'UNKNOWN') !== false) {

                        // Get the name of the previous customer name.
                        $selectExisting = mysqli_query(Database::$connection, "SELECT cust_name FROM customer_tbl "
                                . "WHERE cust_id = '" . $checkCustomerHasInstallationRow['customer_tbl_cust_id'] . "'");
                        $selectExistingRow = mysqli_fetch_assoc($selectExisting);

                        // See if this one is UNKNOWN as well. If it is, they are likely to be the same installation with two engine types.
                        if (strpos($selectExistingRow['cust_name'], 'UNKNOWN') !== false) {
                            // we want the technical and engine data from this record but we dont want to re-add the installation
                            // Change the name of the customer for the record. This should mean it gets added to the correct installation
                            // during the technical data sweep.
                            $manualCheckQuery = mysqli_query(Database::$connection, "UPDATE temporary_table SET "
                                    . "" . array_search('cust_name', $this->newColumnNameArray) . " = "
                                    . "'" . $selectExistingRow['cust_name'] . "' "
                                    . "WHERE temp_id='" . $tempID . "'");
                        } else {

                            // enter the temp record into the manual check table
                            $manualCheckQuery = "SELECT ";

                            foreach ($this->newColumnNameArray as $key => $name) {
                                $key = str_replace($this->replace, "_", $key);

                                if (($name != "") && ($name != '@dummy')) {
                                    $manualCheckQuery .= " " . addslashes($key) . " AS '" . $name . "', ";
                                }
                            }
                            $manualCheckQuery .= " temp_id FROM temporary_table WHERE temp_id='" . $tempID . "'";
                            //$manualCheckQuery = mysqli_query(Database::$connection, "SELECT * FROM temporary_table WHERE temp_id='" . $tempID . "'");
                            $row = mysqli_fetch_assoc(mysqli_query(Database::$connection, $manualCheckQuery));

                            $contentSQL = "";
                            $valueSQL = "";

                            foreach ($row as $key => $value) {
                                $contentSQL .= "$key, ";
                                $valueSQL .= "'" . addslashes($value) . "', ";
                            }
                            $contentSQLClean = substr($contentSQL, 0, -2);
                            $valueSQLClean = substr($valueSQL, 0, -2);

                            $insertManual = "INSERT INTO manual_check_tbl (" . $contentSQLClean . ", "
                                    . "matching_customer_id, matching_installation_id) "
                                    . "VALUES (" . $valueSQLClean . ", "
                                    . "'" . $checkCustomerHasInstallationRow['customer_tbl_cust_id'] . "', "
                                    . "'" . $installationID . "'"
                                    . ")";

                            if (!mysqli_query(Database::$connection, $insertManual)) {
                                debugWriter("debug.txt", "DUPLICATE INSERT INTO MANUAL CHECK FAILED $insertManual:/r/n " . mysqli_error(Database::$connection));
                            }

                            // remove the UNKNOWN customer from the customer table
                            $deleteRow = "DELETE FROM customer_tbl where cust_id = '" . $customerID . "'";

                            if (!mysqli_query(Database::$connection, $deleteRow)) {
                                debugWriter("debug.txt", "Could not delete customer $deleteRow " . mysqli_error(Database::$connection));
                            } else {
                                // remove the false customer from the temp table.
                                $deleteTempRow = "DELETE FROM temporary_table where temp_id = '" . $tempID . "'";
                                if (!mysqli_query(Database::$connection, $deleteTempRow)) {
                                    debugWriter("debug.txt", "Could not delete temp customer $deleteTempRow " . mysqli_error(Database::$connection));
                                }
                            }
                        }
                    } else {
                        /*
                         * The record is not a duplicate so this needs to be added to the manual check table.
                         */

                        $manualCheckQuery = "SELECT ";

                        foreach ($this->newColumnNameArray as $key => $name) {
                            $key = str_replace($this->replace, "_", $key);

                            if (($name != "") && ($name != '@dummy')) {
                                $manualCheckQuery .= " " . addslashes($key) . " AS '" . $name . "', ";
                            }
                        }
                        $manualCheckQuery .= " temp_id FROM temporary_table WHERE temp_id='" . $tempID . "'";

                        $row = mysqli_fetch_assoc(mysqli_query(Database::$connection, $manualCheckQuery));

                        $contentSQL = "";
                        $valueSQL = "";

                        foreach ($row as $key => $value) {
                            $contentSQL .= "$key, ";
                            $valueSQL .= "'" . addslashes($value) . "', ";
                        }
                        $contentSQLClean = substr($contentSQL, 0, -2);
                        $valueSQLClean = substr($valueSQL, 0, -2);

                        $insertManual = "INSERT INTO manual_check_tbl (" . $contentSQLClean . ", "
                                    . "matching_customer_id, matching_installation_id) "
                                    . "VALUES (" . $valueSQLClean . ", "
                                    . "'" . $checkCustomerHasInstallationRow['customer_tbl_cust_id'] . "', "
                                    . "'" . $installationID . "'"
                                    . ")";

                        if (!mysqli_query(Database::$connection, $insertManual)) {
                            debugWriter("debug.txt", "NOT DUPLICATE INSERT INTO MANUAL CHECK FAILED $insertManual: " . mysqli_error(Database::$connection));
                        }
                       // debugWriter("debug.txt", "Installation INSERT INTO MANUAL CHECK TBL ". $insertManual);
                    }
                }
            }
        }
        print "<p>Added " . $installationCount . " new installations</p>";
    }

    
    
    /**
     * createTechnicalData
     */
    private function createTechnicalData() {
        // Select all the engine records from the temporary table
        $technicalString = "SELECT " . array_search('cust_name', $this->newColumnNameArray) . " AS cust_name ";
        

        foreach ($this->newColumnNameArray as $key => $name) {
            $key = str_replace($this->replace, "_", $key);

            if ($name == 'inst_installation_name') {
                $technicalString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'inst_imo') {
                $technicalString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'iet_main_aux') {
                $technicalString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'iet_engine_manufacturer') {
                $technicalString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'iet_engine_model') {
                $technicalString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'iet_series') {
                $technicalString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'iet_engine_cylinder_count') {
                $technicalString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'iet_engine_cylinder_configuration') {
                $technicalString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'iet_bore_size') {
                $technicalString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'iet_stroke') {
                $technicalString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'engine_count') {
                $technicalString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            
            // technical
            if ($name == 'iet_fuel_type') {
                $technicalString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'iet_output') {
                $technicalString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'iet_unit_name') {
                $technicalString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'iet_serial_number') {
                $technicalString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'iet_release_date') {
                $technicalString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'iet_comment') {
                $technicalString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'load_priority_lookup_tbl_load_id') {
                $technicalString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
        }
        $technicalString .= " , temp_id FROM temporary_table WHERE temp_id NOT IN (SELECT temp_id FROM manual_check_tbl)";

        $technicalQuery = mysqli_query(Database::$connection, $technicalString); // get the records from the temporary table.
       // debugWriter("debug.txt", "Line 970 ". $technicalString . " " . mysqli_error(Database::$connection));
        
        $engineCount = 0;
        $productCount = 0;
        $tempID = "";
        
        while ($technicalRow = mysqli_fetch_assoc($technicalQuery)) {
            if (isset($technicalRow['temp_id'])) {
                $tempID = $technicalRow['temp_id'];
            }
            
            // foreach record, get the customer ID for that engine.
            $getCustomerIDQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl WHERE "
                    . "cust_name = '" . addslashes($technicalRow['cust_name']) . "'");

            $customerIDRow = mysqli_fetch_assoc($getCustomerIDQuery);
            $customerID = $customerIDRow['cust_id'];
            

            $imoString = "";
            if (isset($technicalRow['inst_imo'])) {
                if ($technicalRow['inst_imo'] == "-") {
                    $technicalRow['inst_imo'] = "";
                }
                if ($technicalRow['inst_imo'] == '#NAME') {
                    $technicalRow['inst_imo'] = "";
                }
                if ($technicalRow['inst_imo'] != "") {
                    $imoString = "AND inst_imo = '" . addslashes($technicalRow['inst_imo']) . "'";
                }
            }
            
            // get the installation ID if it exists.
            if (isset($technicalRow['inst_installation_name'])) {

                $getInstallationIDQuery = mysqli_query(Database::$connection, "SELECT inst_id FROM installations_tbl WHERE "
                        . "inst_installation_name = '" . addslashes($technicalRow['inst_installation_name']) . "' $imoString");

                $installationIDRow = mysqli_fetch_assoc($getInstallationIDQuery);
                $installationID = $installationIDRow['inst_id'];
            }

            $cylinderCount = "0";
            $configuration = "A";
            if (isset($technicalRow['iet_engine_cylinder_count']) && ($technicalRow['iet_engine_cylinder_count'] != "")) {
                $cylinderCount = $technicalRow['iet_engine_cylinder_count'];
            }
            
            if (isset($technicalRow['iet_engine_cylinder_configuration']) && $technicalRow['iet_engine_cylinder_configuration'] != "") {
                $configuration = $technicalRow['iet_engine_cylinder_configuration'];
            }
            
            
            $designerLookupID = "";
            $seriesLookupID = "";
            $versionLookupID = "NULL";
            $typeOfProductID = "";
            $TOProductDescription = "";
            // The product needs to be created first. Then if the product is an engine, do the engine stuff.
            if (isset($technicalRow['iet_engine_manufacturer']) && ($technicalRow['iet_engine_manufacturer'] != "")) {
                
                $designerResult = mysqli_query(Database::$connection, "SELECT * FROM designer_lookup_tbl WHERE "
                        . "dlt_designer_description = '" . addslashes(utf8_decode($technicalRow['iet_engine_manufacturer'])) . "'");
                $rowDesigner = mysqli_fetch_assoc($designerResult);
                $designerLookupID = $rowDesigner['dlt_id'];
                
                if ($designerLookupID == "") {
                    
                    $addDesigner = "INSERT INTO designer_lookup_tbl (dlt_designer_description) VALUES "
                            . "('".addslashes(utf8_decode($technicalRow['iet_engine_manufacturer'])) . "')";
                    
                    if (!mysqli_query(Database::$connection, $addDesigner)) {
                        debugWriter("debug.txt", "insert new manufacturer failed ".mysqli_error(Database::$connection));
                    } else {
                        $designerLookupID = mysqli_insert_id(Database::$connection);
                      //  debugWriter("debug.txt", "Had to add designer to DB ".$designerLookupID);
                    }
                }
                //debugWriter("debug.txt", "ENGINE iet_engine_manufacturer ". $technicalRow['iet_engine_manufacturer']." DLT ".$designerLookupID);
            }
            if (isset($technicalRow['iet_series'])) {
               // debugWriter("debug.txt", "----");
               // debugWriter("debug.txt", "ENGINE SERIES ". $technicalRow['iet_series']);
                $addDesignerLookupSQL  = "";
                if ($designerLookupID != "") {
                    $addDesignerLookupSQL = " AND designer_lookup_tbl_dlt_id = '".$designerLookupID."'";
                }
                if ($technicalRow['iet_series'] == "") {
                    $technicalRow['iet_series'] = 'Unknown';
                    //debugWriter("debug.txt", $addDesignerLookupSQL);
                    //debugWriter("debug.txt", "ENGINE SERIES ". $technicalRow['iet_series']);
                }
                $modelResult = mysqli_query(Database::$connection, "SELECT * FROM series_lookup_tbl "
                        . "WHERE mlt_series = '" . addslashes(utf8_decode($technicalRow['iet_series'])) . "' "
                        . "$addDesignerLookupSQL LIMIT 1");
                $rowModel = mysqli_fetch_assoc($modelResult);
                $seriesLookupID = $rowModel['mlt_id'];
                $typeOfProductID = $rowModel['type_of_product_lookup_tbl_toplt_id'];
                //debugWriter("debug.txt", "----".$seriesLookupID);
                // if its a new series add it here
                if ($seriesLookupID == "") {
                    $addSeries = "INSERT INTO series_lookup_tbl (mlt_series, designer_lookup_tbl_dlt_id, "
                            . "type_of_product_lookup_tbl_toplt_id) VALUES "
                            . "('". addslashes(utf8_decode($technicalRow['iet_series'])) . "', "
                            . "'".$designerLookupID."', '1')";
                    
                    if (!mysqli_query(Database::$connection, $addSeries)) {
                        debugWriter("debug.txt", "insert new series failed ".mysqli_error(Database::$connection));
                    } else {
                        $seriesLookupID = mysqli_insert_id(Database::$connection);
                        $typeOfProductID = '1';
                        //debugWriter("debug.txt", "Had to add series to DB ".$seriesLookupID);
                        
                        $versionInsert = "INSERT INTO version_lookup_tbl (vlt_version_description, "
                                . "model_lookup_tbl_mlt_id, vlt_cylinder_count, vlt_cylinder_configuration) "
                                . "VALUES ('$cylinderCount$configuration', '$seriesLookupID', '$cylinderCount', '$configuration')";
                        
                        if (!mysqli_query(Database::$connection, $versionInsert)) {
                            debugWriter("debug.txt", "insert new version failed ".$versionInsert. " ".mysqli_error(Database::$connection));
                        }
                        
                    }
                }
                
                
                

                if ($typeOfProductID == "") {
                    $typeOfProductID = 1;
                    //debugWriter("debug.txt", "type of product ".$typeOfProductID);
                }
                $typeOfProductResult = mysqli_query(Database::$connection, "SELECT * FROM type_of_product_lookup_tbl WHERE toplt_id='$typeOfProductID'");
                $rowTOP = mysqli_fetch_assoc($typeOfProductResult);
                $TOProductDescription = $rowTOP['toplt_product_description'];
                $versionResult = mysqli_query(Database::$connection, "SELECT * FROM version_lookup_tbl "
                        . "WHERE model_lookup_tbl_mlt_id = '" . $seriesLookupID . "' AND "
                        . "vlt_cylinder_count = '".$cylinderCount."' AND vlt_cylinder_configuration = '".$configuration."'");
                $rowVersion = mysqli_fetch_assoc($versionResult);
                $versionLookupID = "'".$rowVersion['vlt_id']."'";
                //debugWriter("debug.txt", "versionLookupID CHECK *" . $versionLookupID ."");
                if ($versionLookupID == "''") {
                    $versionLookupID = "NULL";
                }
            }
           
            /*
             * Before the product or the engine data is added, we need to check if the product and engine already
             * exists.
             */
            $unitName = "";
            $serialNumber = "";
            if (isset($technicalRow['iet_unit_name']) && ($technicalRow['iet_unit_name'] != "")) {
                $unitName = " AND e.iet_unit_name = '".addslashes($technicalRow['iet_unit_name'])."'";
            }
            if (isset($technicalRow['iet_serial_number']) && ($technicalRow['iet_serial_number'] != "")) {
                $andor = "AND";
                if ($unitName != "") {
                    $andor = "OR";
                }
                $serialNumber = " $andor p.prod_serial_number = '".addslashes($technicalRow['iet_serial_number'])."'";
            }
            
            $checkProductExists = mysqli_query(Database::$connection, "select count(p.prod_id) from product_tbl p, installation_engine_tbl e 
                WHERE p.customer_tbl_cust_id = '$customerID' 
                AND p.installations_tbl_inst_id = '$installationID' 
                AND p.version_lookup_tbl_vlt_id = $versionLookupID 
                AND e.product_tbl_prod_id = p.prod_id 
                $unitName $serialNumber");
            
            $rowCheck = mysqli_fetch_assoc($checkProductExists);
            if ($rowCheck['count(p.prod_id)']) {
                //print "Found an existing product ".$rowCheck['count(p.prod_id)']."<br/>";
                // Add to manual check table.
                if ($unitName == "" && $serialNumber == "") {
                    // only if there was no unit name. Non-engine products do not have a unit name.
                    $manualCheckQuery = "SELECT ";

                        foreach ($this->newColumnNameArray as $key => $name) {
                            $key = str_replace($this->replace, "_", $key);

                            if (($name != "") && ($name != '@dummy')) {
                                $manualCheckQuery .= " " . addslashes($key) . " AS '" . $name . "', ";
                            }
                        }
                        $manualCheckQuery .= " temp_id FROM temporary_table WHERE temp_id='" . $tempID . "'";

                        $row = mysqli_fetch_assoc(mysqli_query(Database::$connection, $manualCheckQuery));

                        $contentSQL = "";
                        $valueSQL = "";

                        foreach ($row as $key => $value) {
                            $contentSQL .= "$key, ";
                            $valueSQL .= "'" . $value . "', ";
                        }
                        $contentSQLClean = substr($contentSQL, 0, -1);
                        $valueSQLClean = substr($valueSQL, 0, -1);

                        $insertManual = "INSERT INTO manual_check_tbl (" . $contentSQLClean . ", "
                                    . "matching_customer_id, matching_installation_id) "
                                    . "VALUES (" . $valueSQLClean . ", "
                                    . "'" . $customerID . "', "
                                    . "'" . $installationID . "'"
                                    . ")";

                        if (!mysqli_query(Database::$connection, $insertManual)) {
                            debugWriter("debug.txt", "INSERT INTO MANUAL CHECK FAILED $insertManual: " . mysqli_error(Database::$connection));
                        }
                       // debugWriter("debug.txt", "LN 1194 INSERT INTO MANUAL CHECK: $insertManual ");
                }
            } else {
            /*
             * Before creating the product the customers product list for each installation
             * needs to be checked over both the product and engine table to see if a matching
             * product already exists. 
             */
            $productID = "";
            // Create the product
            $productInsert = "INSERT INTO product_tbl (prod_serial_number, prod_description, "
                    . "prod_drawing_number, prod_comments, prod_enquiry_only_flag, "
                    . "customer_tbl_cust_id, installations_tbl_inst_id, version_lookup_tbl_vlt_id, prod_unit_name) "
                    . "VALUES ("
                    . "'" . (isset($technicalRow['iet_serial_number']) ? addslashes($technicalRow['iet_serial_number']) : "") . "', "
                    . "'$TOProductDescription', "
                    . "'', "
                    . "'" . (isset($technicalRow['iet_comment']) ? addslashes($technicalRow['iet_comment']) : "") . "', "
                    . "'0', "
                    . "'$customerID', "
                    . "'".(isset($installationID) ? $installationID : "" )."', "
                    . "$versionLookupID, "
                    . "'" . (isset($technicalRow['iet_unit_name']) ? addslashes($technicalRow['iet_unit_name']) : "") . "')";

            if (!mysqli_query(Database::$connection, $productInsert)) {
                debugWriter("loadInstallationDataLogs.txt", "Product insert failed " . $productInsert . " " . mysqli_error(Database::$connection));
            } else {
                $productCount++;
                $productID = mysqli_insert_id(Database::$connection);
                //debugWriter("debug.txt", "PRODUCT INSERTED ".$productCount. " ID IS ". $productID);
            }

            if ($typeOfProductID == 1) { // 1 = engine
            //debugWriter("loadInstallationDataLogs.txt", "inside Engine Stuff ");

                // ENGINE STUFF.
                // make cylinder count and configuration not have n/a in them. this screws stuff up
               /* if (isset($technicalRow['iet_engine_cylinder_count'])) {
                    if (strcasecmp($technicalRow['iet_engine_cylinder_count'], "n/a") == 0) {
                        $technicalRow['iet_engine_cylinder_count'] = "0";
                    }
                }

                if (isset($technicalRow['iet_engine_cylinder_configuration'])) {
                    if (strcasecmp($technicalRow['iet_engine_cylinder_configuration'], "n/a") == 0) {
                        $technicalRow['iet_engine_cylinder_configuration'] = "";
                    }
                }*/

                if (isset($technicalRow['iet_engine_model'])) {
                    if (strcasecmp($technicalRow['iet_engine_model'], "n/a") == 0) {
                        $technicalRow['iet_engine_model'] = "";
                    }
                }

                $doesEngItNeedAnAND = 0;

                
                //$engCylinderCountString = "";
                //$engCylinderConfigString = "";
                $engBoreSizeString = "";
                $engStrokeString = "";
                $engReleaseDateString = "";
                $engLoadPriorityString = "";
                $engCommentString = "";
                $engMainAux = "";




                
               /* if (isset($technicalRow['iet_engine_cylinder_count']) && ($technicalRow['iet_engine_cylinder_count'] != "")) {
                    $and = "";
                    if ($doesEngItNeedAnAND == 1) {
                        $and = " AND ";
                    }
                    $engCylinderCountString = $and . "iet_engine_cylinder_count = '" . addslashes($technicalRow['iet_engine_cylinder_count']) . "'";

                    $doesEngItNeedAnAND = 1;
                }*/

                /*if (isset($technicalRow['iet_engine_cylinder_configuration']) && ($technicalRow['iet_engine_cylinder_configuration'] != "")) {
                    $and = "";
                    if ($doesEngItNeedAnAND == 1) {
                        $and = " AND ";
                    }
                    $engCylinderConfigString = $and . "iet_engine_cylinder_configuration = '" . addslashes($technicalRow['iet_engine_cylinder_configuration']) . "'";

                    $doesEngItNeedAnAND = 1;
                }*/

                if (isset($technicalRow['iet_bore_size']) && ($technicalRow['iet_bore_size'] != "")) {
                    $and = "";
                    if ($doesEngItNeedAnAND == 1) {
                        $and = " AND ";
                    }
                    $engBoreSizeString = $and . "iet_bore_size = '" . addslashes($technicalRow['iet_bore_size']) . "'";

                    $doesEngItNeedAnAND = 1;
                }

                if (isset($technicalRow['iet_stroke']) && ($technicalRow['iet_stroke'] != "")) {
                    $and = "";
                    if ($doesEngItNeedAnAND == 1) {
                        $and = " AND ";
                    }
                    $engStrokeString = $and . "iet_stroke = '" . addslashes($technicalRow['iet_stroke']) . "' ";

                    $doesEngItNeedAnAND = 1;
                }

                if (isset($technicalRow['iet_release_date']) && ($technicalRow['iet_release_date'] != "")) {
                    $and = "";
                    if ($doesEngItNeedAnAND == 1) {
                        $and = " AND ";
                    }
                    $engReleaseDateString = $and . "iet_release_date = '" . addslashes($technicalRow['iet_release_date']);

                    $doesEngItNeedAnAND = 1;
                }

                if (isset($technicalRow['iet_comment']) && ($technicalRow['iet_comment'] != "")) {
                    $and = "";
                    if ($doesEngItNeedAnAND == 1) {
                        $and = " AND ";
                    }
                    $engCommentString = $and . "iet_comment = '" . addslashes($technicalRow['iet_comment']) . "' ";

                    $doesEngItNeedAnAND = 1;
                }

                if (isset($technicalRow['load_priority_lookup_tbl_load_id']) && ($technicalRow['load_priority_lookup_tbl_load_id'] != "")) {
                    $and = "";
                    if ($doesEngItNeedAnAND == 1) {
                        $and = " AND ";
                    }
                    $engLoadPriorityString = $and . "load_priority_lookup_tbl_load_id = '" . addslashes($technicalRow['load_priority_lookup_tbl_load_id']) . "' ";

                    $doesEngItNeedAnAND = 1;
                }
                
                if (isset($technicalRow['iet_main_aux']) && ($technicalRow['iet_main_aux'] != "")) {
                    $and = "";
                    if ($doesEngItNeedAnAND == 1) {
                        $and = " AND ";
                    }
                    $engMainAux = $and . "iet_main_aux = '" . addslashes(ucwords($technicalRow['iet_main_aux'])) . "' ";

                    $doesEngItNeedAnAND = 1;
                }

                $engStringStart = "";
                $engStringFinish = "";
                if ($designerLookupID != ""  || $seriesLookupID != "" /*||
                        $engCylinderCountString != "" || $engCylinderConfigString != ""*/) {
                    $engStringStart = " (";
                    $engStringFinish = " )";
                }

               

                
                if (($designerLookupID != "") || ($seriesLookupID != "")  /*||
                        ($engCylinderCountString != "") || ($engCylinderConfigString != "")*/ ||
                        ($engBoreSizeString != "") || ($engStrokeString != "")) {
                    // check if the engine exists already.// Need to use engine count as well.
                    
                    $mainEngineCheck = "SELECT count(iet_id) FROM installation_engine_tbl "
                            . "WHERE product_tbl_prod_id='$productID'";

                    //debugWriter("debug.txt", $mainEngineCheck);

                    $engineCheckSQL = mysqli_query(Database::$connection, $mainEngineCheck);

                    $engineCheckRow = mysqli_fetch_assoc($engineCheckSQL);
                    $noofExistingEngines = $engineCheckRow['count(iet_id)'];

                    if (!isset($technicalRow['engine_count'])) {
                        $technicalRow['engine_count'] = 1;
                    }

                    // the number of main engines held is less than the number of engines the CSV expects
                    if ($noofExistingEngines < $technicalRow['engine_count']) {
                         //debugWriter("debug.txt", $mainEngineCheck . " | " . $noofExistingEngines . " : " . $technicalRow['engine_count']);
                        for ($i = $noofExistingEngines + 1; $i <= $technicalRow['engine_count']; $i++) {

                            if ((isset($technicalRow['iet_engine_manufacturer'])) && $technicalRow['iet_engine_manufacturer'] != "") {
                                $engine = "INSERT INTO installation_engine_tbl (iet_model_description,  " //iet_engine_cylinder_count, ". "iet_engine_cylinder_configuration,
                                        
                                        . "iet_main_aux, iet_engine_verified, "
                                        . "iet_output, iet_output_unit_of_measure, "
                                        . "iet_fuel_type,  "
                                        . "iet_bore_size, iet_stroke, iet_release_date, iet_enquiry_only, "
                                        . " load_priority_lookup_tbl_load_id, product_tbl_prod_id) "
                                        . "VALUES ("
                                        . "'".(isset($technicalRow['iet_engine_model']) ? addslashes($technicalRow['iet_engine_model']) : "")."', "
                                        //. "'" . (isset($technicalRow['iet_engine_cylinder_count']) ? addslashes($technicalRow['iet_engine_cylinder_count']) : "") . "', "
                                        //. "'" . (isset($technicalRow['iet_engine_cylinder_configuration']) ? addslashes($technicalRow['iet_engine_cylinder_configuration']) : "") . "', "
                                        . "'" . (isset($technicalRow['iet_main_aux']) ? addslashes(ucwords($technicalRow['iet_main_aux'])) : "Main") . "', "
                                        . "'0', "
                                        . "'" . (isset($technicalRow['iet_output']) ? addslashes($technicalRow['iet_output']) : "") . "', "
                                        . "'MW', "
                                        . "'" . (isset($technicalRow['iet_fuel_type']) ? addslashes($technicalRow['iet_fuel_type']) : "") . "', "
                                     
                                        . "'" . (isset($technicalRow['iet_bore_size']) ? addslashes($technicalRow['iet_bore_size']) : "") . "', "
                                        . "'" . (isset($technicalRow['iet_stroke']) ? addslashes($technicalRow['iet_stroke']) : "") . "', "
                                        . "'" . (isset($technicalRow['iet_release_date']) ? addslashes($technicalRow['iet_release_date'] . "-01-01 00:00:00") : "") . "', "
                                        . "'0', "
                                        . "'3', " // " . (isset($technicalRow['load_priority_lookup_tbl_load_id']) ? addslashes($technicalRow['load_priority_lookup_tbl_load_id']) : "3") . "
                                        . "'$productID')";

                                if (!mysqli_query(Database::$connection, $engine)) {
                                    debugWriter("loadInstallationDataLogs.txt", "Engine insert failed " . $engine . " " . mysqli_error(Database::$connection));
                                } else {
                                    $engineCount++;
                                }
                            }
                        }
                    }
                }
            }
        }
        }
        print "<p>Added " . $productCount . " Products</p>";
        print "<p>Added " . $engineCount . " Engines</p>";
    }

    /**
     * getAllFromManualCheckTbl
     * @return row
     */
    public function getAllFromManualCheckTbl() {

        $count = mysqli_query(Database::$connection, "SELECT count(*) FROM manual_check_tbl");
        $countRow = mysqli_fetch_assoc($count);

        if ($countRow['count(*)'] < 1) {
            $this->dropManualCheckTbl();
            return null;
        } else {

            $result = mysqli_query(Database::$connection, "SELECT * FROM manual_check_tbl");

            while ($row = mysqli_fetch_assoc($result)) {
                $array[$row['temp_id']] = $row;
            }

            if (isset($array)) {
                return $array;
            } else {
                debugWriter("debug.txt", "getAllFromManualCheckTbl FAILED " . mysqli_error(Database::$connection));
                return null;
            }
        }
    }

    /**
     * dropManualCheckTbl
     */
    public function dropManualCheckTbl() {
        $drop_table = "DROP TABLE IF EXISTS manual_check_tbl";
        if (!mysqli_query(Database::$connection, $drop_table)) {
            debugWriter("debug.txt", "Can't drop manual_check_tbl - gravity failure : " . mysqli_error(Database::$connection) . "");
        }
    }

    /**
     * importDataFromCSV
     * @param STRING $fileName
     * @param ARRAY $columnNameArray
     * @param BOOLEAN $headers
     * @param STRING $type
     */
    public function importDataFromCSV($fileName, $columnNameArray, $headers, $type) {

        // This writes out to a file the data/time and file name when things are uploaded.
        /*debugWriter("loadInstallationDataLogs.txt", "Log Date: " . date("Y-m-d H:i", time()) . " USER: " . $_SESSION['name'] . "");
        debugWriter("loadInstallationDataLogs.txt", "File Name: " . $fileName . "");
        foreach ($columnNameArray as $p => $q) {
            debugWriter("loadInstallationDataLogs.txt", "Column Name Array: " . $p . " - " . $q . "");
        }
        debugWriter("loadInstallationDataLogs.txt", "Headers Used: " . $headers . "");
        debugWriter("loadInstallationDataLogs.txt", "Type: " . $type . "");*/

        // GLOBAL VARIABLES

        $this->dbColumnName = "";
        $this->newColumnNameArray = [];
        $this->replace = ["/", "-", " ", ".", "#"]; // REGEX (Regular Expression) for clearing out these characters
        // create the temporary table.
        $this->createTemporaryTable($columnNameArray, $type);

        // create a table to load the records we want to check manually.
        $this->createManualCheckTable($columnNameArray, $type);

        $this->loadFile($fileName, $headers, $type);

        $this->cleanTempData();
        /*
         * Do these next sweeps by first pulling out a query of just the necessary data, then do the search.
         */

        /*
         * Look at each database record in turn and see if the distinct Managing Companies (Customers) exist already.
         * If they don't exist in the DB, then a new record needs to be created.
         */
        $this->createCustomers();

        /*
         * Once the new customers have been added, get the contact details for each customer and add them to the
         * contacts database table, if the contact does not already exist.
         */

        /**
         * Dont be fooled into thinking this bit should be easy, there are an unknown number of columns
         * with whatever names were created in the spreadsheets. There could also be up to three contact
         * records on each line. This has given me logic headaches.
         */
        $this->createContacts();

        /**
         * Get the address details for the company and add to the customer.
         */
        /*
         * Once the contacts have been added, go through each installation and check if it already exists, and if it
         * attached to the currently displayed customer. If not, this needs to stay in the temp table and be manually
         * sorted out. Add all new installations to the current customer. If there are engines shown in the record
         * then these need to be added to the installation engine table against the installation at the same time.
         */
        $this->createInstallations();

        /**
         * Next the technical and engine data for each record needs to be added.
         * NEARLY THERE...
         */
        $this->createTechnicalData();
        /**
         * Drop the temporary table.
         */
        $drop_table = "DROP TABLE IF EXISTS temporary_table";
        if (!mysqli_query(Database::$connection, $drop_table)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't drop table - gravity failure : " . mysqli_error(Database::$connection) . "");
        }
    }

}
