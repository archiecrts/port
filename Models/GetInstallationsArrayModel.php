<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/Installation.php");
include_once("Models/Entities/Customer.php");
include_once("Models/Entities/CustomerConversation.php");
include_once("Models/Entities/CustomerConversationHistory.php");
include_once("Models/SQLConstructors/ReportResultsSQLConstructor.php");
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of GetInstallationsArrayModel
 *
 * @author Archie
 */
class GetInstallationsArrayModel {
    
    /**
     * @param type $continent
     * @param type $area
     * @param type $country
     * @param type $salesManager
     * @param type $engineManufacturer
     * @param type $engineModel
     * @param type $actionDueFrom
     * @param type $actionDueUntil
     * @param type $lastCalledFrom
     * @param type $lastCalledUntil
     * @param type $actionType
     * @param type $productType
     * @return \Installation
     */
    public function getInstallationsArray($continent = [], $area = [], $country = [], $salesManager = "", 
            $engineManufacturer = [], $engineModel = [], $mainOrAux = "", 
            $actionDueFrom = "", $actionDueUntil = "", $lastCalledFrom = "", $lastCalledUntil = "",
            $actionType = "", $productType = "", $instSearchName = "", $companySearchName = "", 
            $ssFromDate = "", $ssToDate = "") {

        // Check if we will be using the ActionDue range or the lastCalled range
        $actionDueRange = FALSE;
        $lastCalledRange = FALSE;
        // SQL STATEMENTS TO INJECT.
        $SQLStart = "SELECT (@rownr:=@rownr+1) AS rowNumber, c.cust_id, i.inst_id, a.cadt_id, e.iet_id, c.cust_name, 
            c.cust_account_manager, i.inst_type, i.inst_installation, i.inst_installation_name, 
            i.inst_imo, IFNULL(i.inst_group_owner, i.inst_owner_parent_company) AS 'inst_owner_parent_company', i.inst_technical_manager_company, 
            i.installation_status_tbl_stat_id, i.inst_original_source, 
            a.cadt_city, a.cadt_state, a.cadt_region, a.cadt_country, q.c_area, q.c_continent, 
            d.dlt_designer_description, s.mlt_series, s.type_of_product_lookup_tbl_toplt_id, 
            count(s.mlt_series) AS engine_count, p.prod_source_alias_id, 
            e.iet_model_description, e.iet_main_aux, e.iet_release_date, i.inst_built_date, z.cadt_country AS tm_country, 
            z.cadt_city AS tm_city, x.c_area AS tm_area, x.c_continent AS tm_continent, sdd.sddt_special_survey_date

            FROM customer_tbl AS c 
            LEFT JOIN customer_tbl_has_installations_tbl as ci ON c.cust_id = ci.customer_tbl_cust_id 
            LEFT JOIN installations_tbl AS i ON ci.installations_tbl_inst_id = i.inst_id 
            LEFT JOIN customer_address_tbl AS a ON i.customer_address_tbl_cadt_id = a.cadt_id 
            LEFT JOIN customer_address_tbl AS z ON i.inst_technical_manager_company = z.customer_tbl_cust_id 
            LEFT JOIN product_tbl AS p ON i.inst_id = p.installations_tbl_inst_id 
            LEFT JOIN version_lookup_tbl AS v ON p.version_lookup_tbl_vlt_id = v.vlt_id 
            LEFT JOIN series_lookup_tbl AS s ON v.model_lookup_tbl_mlt_id = s.mlt_id
            LEFT JOIN designer_lookup_tbl AS d ON s.designer_lookup_tbl_dlt_id = d.dlt_id 
            LEFT JOIN installation_engine_tbl AS e ON p.prod_id = e.product_tbl_prod_id 
            LEFT JOIN country_tbl AS q ON a.cadt_country = q.c_name 
            LEFT JOIN country_tbl AS x ON z.cadt_country = x.c_name 
            LEFT JOIN survey_due_date_tbl AS sdd ON i.inst_id = sdd.installations_tbl_inst_id
            CROSS JOIN (SELECT @rownr := 0) AS dummy  ";
        
        $continentSQL = "";
        
        if (isset($continent[0]) && ($continent[0] != 'all')) 
        {
            foreach ($continent as $index => $value) 
            {
                if ($value != "all" && $index == 0)
                {
                    $continentSQL .= "a.cadt_country IN (SELECT c_name FROM country_tbl "
                            . "WHERE c_continent = '" . utf8_decode($value) . "') ";
                    $continentSQL .= " OR z.cadt_country IN (SELECT c_name FROM country_tbl "
                            . "WHERE c_continent = '" . utf8_decode($value) . "') ";
                } 
                else if ($value != "all" && $index > 0) 
                {
                    $continentSQL .= "OR a.cadt_country IN (SELECT c_name FROM country_tbl "
                            . "WHERE c_continent = '" . utf8_decode($value) . "') ";
                    $continentSQL .= "OR z.cadt_country IN (SELECT c_name FROM country_tbl "
                            . "WHERE c_continent = '" . utf8_decode($value) . "') ";
                }
            }
        }
        
        $areaSQL = "";
        
        if (isset($area[0]) && ($area[0] != 'all')) 
        {
            foreach ($area as $index => $value) 
            {
                if ($value != "all" && $index == 0)
                {
                    $areaSQL .= "a.cadt_country IN (SELECT c_name FROM country_tbl "
                            . "WHERE c_area = '" . utf8_decode($value) . "') ";
                    $areaSQL .= " OR z.cadt_country IN (SELECT c_name FROM country_tbl "
                            . "WHERE c_area = '" . utf8_decode($value) . "') ";
                } 
                else if ($value != "all" && $index > 0) 
                {
                    $areaSQL .= "OR a.cadt_country IN (SELECT c_name FROM country_tbl "
                            . "WHERE c_area = '" . utf8_decode($value) . "') ";
                    $areaSQL .= "OR z.cadt_country IN (SELECT c_name FROM country_tbl "
                            . "WHERE c_area = '" . utf8_decode($value) . "') ";
                }
            }
        }

        $countrySQL = "";
        
        if (isset($country[0]) && ($country[0] != 'all')) 
        {
            foreach ($country as $index => $value) 
            {
                if ($value != "all" && $index == 0)
                {
                    $addString = "";
                    if (strpos($value, ' and ') !== false) {
                        $ampValue = str_replace(" and "," & ",$value);
                        $addString = " OR a.cadt_country = '" . utf8_decode($ampValue) . "'  OR z.cadt_country = '" . utf8_decode($ampValue) . "'";
                    }
                    $countrySQL .= "a.cadt_country = '" . utf8_decode($value) . "' OR z.cadt_country = '" . utf8_decode($value) . "' ".$addString;
                } 
                else if ($value != "all" && $index > 0) 
                {
                    $addString = "";
                    if (strpos($value, ' and ') !== false) {
                        $ampValue = str_replace(" and "," & ",$value);
                        $addString = " OR a.cadt_country = '" . utf8_decode($ampValue) . "'  OR z.cadt_country = '" . utf8_decode($ampValue) . "'";
                    }
                    $countrySQL .= "OR a.cadt_country = '" . utf8_decode($value) . "' OR z.cadt_country = '" . utf8_decode($value) . "' ".$addString;
                }
            }
        } else if ($_SESSION['limited'] == 1) {
            //debugWriter("debug.txt", "Limited");
            $result = mysqli_query(Database::$connection, "SELECT DISTINCT(c_name) FROM country_tbl WHERE c_name IN (SELECT ut_territory_name FROM user_territory_tbl WHERE user_tbl_usr_id = '".$_SESSION['user_id']."') ORDER BY c_name ASC");
            while ($row = mysqli_fetch_assoc($result)) {             
                
                $countrySQL .= "OR a.cadt_country = '" . utf8_decode($row['c_name']) . "' OR z.cadt_country = '" . utf8_decode($row['c_name'])."' ";
            }
        }
        if (substr($countrySQL, 0, 2) == "OR") {
            $countrySQL = substr($countrySQL, 3);
        }
        $salesManagerSQL = "";

        if ($salesManager != '') {
            $salesManagerSQL = "(a.cadt_country IN (
                                    SELECT c_name 
                                    FROM country_tbl 
                                    WHERE c_continent IN (
                                        SELECT ut_territory_name 
                                        FROM user_territory_tbl 
                                        WHERE user_tbl_usr_id='" . $salesManager . "')
                                    OR c_area IN (
                                        SELECT ut_territory_name 
                                        FROM user_territory_tbl 
                                        WHERE user_tbl_usr_id='" . $salesManager . "')
                                    OR c_name IN(
                                        SELECT ut_territory_name 
                                        FROM user_territory_tbl 
                                        WHERE user_tbl_usr_id='" . $salesManager . "')
                                    AND c.cust_account_manager IS NULL or c.cust_account_manager = '" . $salesManager . "'))";
            
        }

        $engineManufacturerSQL = "";

        if (isset($engineManufacturer[0]) && ($engineManufacturer[0] != 'all')) 
        {
            foreach ($engineManufacturer as $index => $value) 
            {
                $theID = mysqli_query(Database::$connection, "SELECT dlt_id FROM designer_lookup_tbl WHERE dlt_designer_description = '".utf8_decode($value)."'");
                $idRow = mysqli_fetch_assoc($theID);
                
                
                if ($value != "all" && $index == 0)
                {
                    $engineManufacturerSQL .= "d.dlt_id = '" . $idRow['dlt_id'] . "' OR p.prod_source_alias_id  = '" . $idRow['dlt_id'] . "' ";
                } 
                else if ($value != "all" && $index > 0) 
                {
                    $engineManufacturerSQL .= "OR d.dlt_id = '" . $idRow['dlt_id'] . "' OR p.prod_source_alias_id  = '" . $idRow['dlt_id'] . "' ";
                }
            }
        }

      
        $engineModelSQL = "";

        if (isset($engineModel[0]) && ($engineModel[0] != 'all')) 
        {
            foreach ($engineModel as $index => $value) 
            {
                if ($value != "all" && $index == 0)
                {
                    $engineModelSQL .= "s.mlt_series = '" . utf8_decode($value) . "' ";
                } 
                else if ($value != "all" && $index > 0) 
                {
                    $engineModelSQL .= "OR s.mlt_series = '" . utf8_decode($value) . "' ";
                }
            }
        }
        
        
        
        $mainOrAuxSQL = "";
        
        if (($mainOrAux != '') && ($mainOrAux != 'all')) 
        {
            $mainOrAuxSQL = " e.iet_main_aux = '".$mainOrAux."' ";
        }
        
        $productTypeSQL = "";
        
        if ($productType != "") {
            $productTypeSQL = " s.type_of_product_lookup_tbl_toplt_id = '$productType' ";
        }
        
        $instSearchNameSQL = "";
        
        $companySearchNameSQL = "";
        
        if (($instSearchName != '')) {
            $instSearchNameSQL = " i.inst_installation_name LIKE '%".addslashes($instSearchName)."%' ";
        }
        
        if (($companySearchName != '')) {
            $companySearchNameSQL = " c.cust_name LIKE '%".addslashes($companySearchName)."%' OR 
                    i.inst_technical_manager_company IN (SELECT cust_id FROM customer_tbl 
                    WHERE cust_name LIKE '%".addslashes($companySearchName)."%') OR 
                    i.inst_group_owner IN (SELECT cust_id FROM customer_tbl 
                    WHERE cust_name LIKE '%".addslashes($companySearchName)."%')";
        }
        
        $specialSurveySQL = "";
        // special survey search SQL
        if ($ssFromDate != "" && $ssToDate != "") {
            $specialSurveySQL = " sdd.sddt_special_survey_date BETWEEN '$ssFromDate' AND '$ssToDate' ";
        } else if ($ssFromDate != "" && $ssToDate == "") {
            $specialSurveySQL = " sdd.sddt_special_survey_date >= '$ssFromDate' ";
        } else if ($ssFromDate == "" && $ssToDate != "") {
            $specialSurveySQL = " sdd.sddt_special_survey_date <= '$ssToDate' ";
        }
        

        

        $refArray1["continent"] = "(" . $continentSQL . ")";
        $refArray1["area"] = "(" . $areaSQL . ")";
        $refArray1["country"] = "(" . $countrySQL . ")";
        $refArray1["stmgr"] = $salesManagerSQL;
        $refArray1["engMan"] = "(" . $engineManufacturerSQL . ")";
        $refArray1["engModel"] = "(" . $engineModelSQL . ")";
        $refArray1["mainOrAux"] = $mainOrAuxSQL;
        $refArray1["productType"] = $productTypeSQL;
        $refArray1['instSearchName'] = $instSearchNameSQL;
        $refArray1['companySearchName'] = $companySearchNameSQL;
        $refArray1['specialSurvey'] = $specialSurveySQL;
        
        $ct = 0;
        $string1 = $SQLStart;
        foreach ($refArray1 as $key => $sql) {
            
            if (($sql != '') && ($sql != "()")) {
                if ($ct == 0) {
                    $string1.= " WHERE ";
                    $ct++;
                } else {
                    if (($key == 'continent') || ($key == 'area') || ($key == 'country')) {
                        $string1.= " OR ";
                    } else {
                        $string1.= " AND ";
                    }
                }
                $string1.= $sql;
            }
        }
        $string1 .= " AND (sdd.sddt_special_survey_date = (SELECT MAX(sddt_special_survey_date) FROM survey_due_date_tbl WHERE installations_tbl_inst_id = i.inst_id) OR (i.inst_id NOT IN (SELECT installations_tbl_inst_id FROM survey_due_date_tbl))) AND (z.cadt_default_address = '1' OR z.cadt_country IS NULL) GROUP BY s.mlt_series, e.iet_main_aux, i.inst_installation_name, c.cust_name ORDER BY ISNULL(i.inst_installation_name), i.inst_installation_name, e.iet_main_aux ASC, c.cust_name"; 

        
        debugWriter("debug.txt", $string1);


        //execute the SQL query and return records
        $result = mysqli_query(Database::$connection, $string1);
        
        $reportResultsConstructor = new ReportResultsSQLConstructor();
        
        //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {
            
            $array[$row['rowNumber']] = $reportResultsConstructor->createReportResults($row);
            
        }

        if (isset($array)) {
           // debugWriter("debug.txt", $string1);
            return $array;
        } else {
            debugWriter("debug.txt", "getInstallationsArrayModel FAILED (LN 275) ".mysqli_error(Database::$connection));
            return null;
        }
    }
}
