<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/EnquiryEngine.php");
include_once 'Models/SQLConstructors/EnquiryEngineSQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of EnquiryEngineModel
 *
 * @author Archie
 */
class EnquiryEngineModel {
    
    /**
     * createEnquiryEngine
     * @param Entity $engine
     * @return INT | null
     */
    public function createEnquiryEngine($engine) {
        $result = "INSERT INTO enquiry_engines_tbl (eet_id, enquiry_tbl_enq_id, installation_engine_tbl_iet_id) "
                . "VALUES "
                . "('".$engine->enqEngineID."', "
                . "'".$engine->enqEngineTblID."', "
                . "'".$engine->installationID."')";
        
        if (!mysqli_query(Database::$connection, $result)) {
            debugWriter("debug.txt", "createEnquiryEngine INSERT Failed ".  mysqli_error(Database::$connection));
            return null;
        } else {
            return mysqli_insert_id(Database::$connection);
        }
    }
    
    
    /**
     * getEnquiryEnginesByEnquiryID
     * @param type $addEnquiry
     * @return type
     */
    public function getEnquiryEnginesByEnquiryID($addEnquiry) {
        $enquiryEngineSQLConstructor = new EnquiryEngineSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM enquiry_engines_tbl WHERE enquiry_tbl_enq_id = '".$addEnquiry."'");
        
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['eet_id']] = $enquiryEngineSQLConstructor->createEnquiryEngine($row);
        }
        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
}
