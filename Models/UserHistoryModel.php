<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/UserHistory.php");
include_once("Models/SQLConstructors/UserHistorySQLConstructor.php");
////include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of UserHistoryModel
 *
 * @author Archie
 */
class UserHistoryModel {
    
    /**
     * Creates a new entry into the user history table.
     * @param INT $userID
     * @param DATETIME $datetime
     * @param ENUM $type
     * @param TEXT $comment
     * @return boolean
     */
    public function setUserHistory($userID, $datetime, $type, $comment) {
        if ($userID == null) {
            $result = "INSERT INTO user_history_tbl "
                . "(uht_item_date, uht_item_type, "
                . "uht_comment) "
                . "VALUES ('".$datetime."', '".$type."', '".$comment."')";
        } else {
            $result = "INSERT INTO user_history_tbl "
                . "(uht_item_date, uht_item_type, "
                . "uht_comment, user_tbl_usr_id) "
                . "VALUES ('".$datetime."', '".$type."', '".$comment."', '".$userID."')";
        }
        if (!mysqli_query(Database::$connection, $result)) {
            debugWriter("userHistoryErrors.txt", "MySQL ERROR: User ID: ".$userID.", Datetime ".$datetime.", Type: ".$type.", Comment: ".$comment.", ".mysqli_error(Database::$connection));
            return false;
        } else {
            return true;
        }
    }
    
    
    /**
     * Gets a list of user history items for user.
     * @param INT $userID
     * @return Array of User History Objects
     */
    public function getUserHistory($userID) {
        $userHistorySQLConstructor = new UserHistorySQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM user_history_tbl WHERE user_tbl_usr_id = '".$userID."'");
        
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['uht_id']] = $userHistorySQLConstructor->createUserHistory($row); 
        }
        
        if (isset($array)) {
            return $array;
        } else {
            $logger = fopen("./logs/userHistoryErrors.txt", "a");
            fwrite($logger, "MySQL ERROR: GETTING HISTORY FOR User ID: ".$userID.", ".mysqli_error(Database::$connection). "\r\n");
            fclose($logger);
            return null;
        }

    }
    
    
    /**
     * Gets all entries between the dates set.
     * @param DATE $startDate
     * @param DATE $endDate
     * @return Array of User History Objects
     */
    public function getAllUserHistoryBetweenDates($startDate, $endDate) {
        $userHistorySQLConstructor = new UserHistorySQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM user_history_tbl "
                . "WHERE uht_item_date >= '".$startDate."' "
                . "AND uht_item_date <= '".$endDate."' ORDER BY uht_item_date DESC");
        
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['uht_id']] = $userHistorySQLConstructor->createUserHistory($row); 
        }
        
        if (isset($array)) {
            return $array;
        } else {
            debugWriter("userHistoryErrors.txt", "getAllUserHistoryBetweenDates Failed or array is empty: " . $startDate . " , " . $endDate . ": " . mysqli_error(Database::$connection));
            return null;
        }
        
    }
    
    /**
     * lastSeawebUpdateAt
     * @return User History Object
     */
    public function lastSeawebUpdateAt() {
        $userHistorySQLConstructor = new UserHistorySQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM user_history_tbl WHERE uht_comment LIKE 'SEAWEB%' LIMIT 1");
        $row = mysqli_fetch_assoc($result);
        
        $record =  $userHistorySQLConstructor->createUserHistory($row); 
        
        if (isset($record)) {
            return $record;
        } else {
            return null;
        }
    }
    
}
