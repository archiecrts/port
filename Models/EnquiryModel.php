<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/Enquiry.php");
include_once 'Models/SQLConstructors/EnquirySQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of EnquiryModel
 *
 * @author Archie
 */
class EnquiryModel {
    
    /**
     * createEnquiry
     * @param type $enquiry
     * @return type
     */
    public function createEnquiry($enquiry) {
        $result = "INSERT INTO enquiry_tbl (enq_date, user_tbl_usr_id, enq_success_rate,"
                . "enq_header_note, customer_tbl_cust_id, enquiry_source_tbl_est_id, "
                . "enquiry_type_tbl_et_id, enq_customer_reference, customer_contacts_tbl_ict_id, "
                . "enq_include_installation_in_quote) "
                . "VALUES "
                . "('".$enquiry->enquiryDate."', "
                . "'".$enquiry->userID."', "
                . "'".$enquiry->enquirySuccessRate."', "
                . "'".$enquiry->enquiryHeaderNote."', "
                . "'".$enquiry->customerID."', "
                . "'".$enquiry->enquirySourceID."', "
                . "'".$enquiry->enquiryTypeID."', "
                . "'".$enquiry->enquiryCustomerReference."', "
                . "'".$enquiry->customerContactID."', "
                . "'".$enquiry->includeInstallationInQuote."')";
        
        if (!mysqli_query(Database::$connection, $result)) {
            debugWriter("debug.txt", "createEnquiry INSERT Failed ".  mysqli_error(Database::$connection));
            return null;
        } else {
            return mysqli_insert_id(Database::$connection);
        }
    }
    
    /**
     * addEnquiryOurReference
     * @param INT $enquiryID
     * @param STRING $reference
     * @return boolean
     */
    public function addEnquiryOurReference($enquiryID, $reference) {
        $result = "UPDATE enquiry_tbl SET enq_our_reference = '".$reference."' "
                . "WHERE enq_id = '".$enquiryID."'";
        
        if (!mysqli_query(Database::$connection, $result)) {
            debugWriter("debug.txt", "addEnquiryOurReference FAILED ".mysqli_error(Database::$connection));
            return null;
        } else {
            return true;
        }
    }
    
    /**
     * getEnquiryByID
     * @param ID $addEnquiry
     * @return type
     */
    public function getEnquiryByID($addEnquiry) {
        $enquirySQLConstructor = new EnquirySQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM enquiry_tbl WHERE enq_id = '".$addEnquiry."'");
        
        $row = mysqli_fetch_assoc($result);
        $enquiry = $enquirySQLConstructor->createEnquiry($row);
        return $enquiry;
    }
}
