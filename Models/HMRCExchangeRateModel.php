<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/HMRCExchangeRate.php");
include_once 'Models/SQLConstructors/HMRCExchangeRateSQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of HMRCExchangeRateModel
 *
 * @author Archie
 */
class HMRCExchangeRateModel {
    
    /**
     * getAllExchangeRates
     * @return array of exchange rates
     */
    public function getAllExchangeRates() {
        $hmrcExchangeRateSQLConstructor = new HMRCExchangeRateSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM hmrc_monthly_exchange_rate_tbl ORDER BY hmrc_country ASC");

        //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {             
            $array[$row['hmrc_id']] 
                    = $hmrcExchangeRateSQLConstructor->createHMRCExchnageRate($row);
        }
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    /**
     * getExchangeRateByCountryName
     * @param STRING $countryName
     * @return Exchange Rate
     */
    public function getExchangeRateByCountryName($countryName) {
        $hmrcExchangeRateSQLConstructor = new HMRCExchangeRateSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM hmrc_monthly_exchange_rate_tbl "
                . "WHERE hmrc_country = '".$countryName."'");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);            
        $rate = $hmrcExchangeRateSQLConstructor->createHMRCExchnageRate($row);
            
        return $rate;
    }
    
    /**
     * loadExchangeRateCSV
     * @param Exchange Rate $exchangeRate
     * @return INT | null
     */
    public function loadExchangeRateCSV($fileName) {
        $insert_temp_query = "LOAD DATA LOCAL INFILE '" . $fileName . "'
                INTO TABLE hmrc_monthly_exchange_rate_tbl
                COLUMNS TERMINATED BY ','
                ENCLOSED BY '\"' LINES TERMINATED BY '\r\n'
                IGNORE 1 LINES
                (hmrc_country, hmrc_currency, hmrc_currency_code, hmrc_currency_units_per_pound, 
                @var1, @var2) 
                set hmrc_start_date = STR_TO_DATE(@var1, '%d/%m/%Y'), hmrc_end_date = STR_TO_DATE(@var2, '%d/%m/%Y')";
       
        //debugWriter("debug.txt", $insert_temp_query);
        if (!(mysqli_query(Database::$connection, $insert_temp_query))) {
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * truncateTable
     * This removes all rows and resets the index back to 1.
     * @return boolean
     */
    public function truncateTable() {
        $truncate = "TRUNCATE TABLE hmrc_monthly_exchange_rate_tbl";
        
        if (!(mysqli_query(Database::$connection, $truncate))) {
           return false;
       } else {
           return true;
       }
    }
}
