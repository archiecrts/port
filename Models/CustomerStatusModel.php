<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/CustomerStatus.php");
include_once 'Models/SQLConstructors/CustomerStatusSQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of CustomerStatusModel
 *
 * @author Archie
 */
class CustomerStatusModel {
   
    /**
     * getCustomerStatusByID
     * @param INT $customerStatusID
     * @return Customer Status Object
     */
    public function getCustomerStatusByID($customerStatusID) {
        $customerStatusSQLConstructor = new CustomerStatusSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_status_tbl WHERE cst_id='".$customerStatusID."'");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);

        $customerStatus = $customerStatusSQLConstructor->createCustomerStatus($row);
        
        return $customerStatus;
    }
    
    
    /**
     * getAllCustomerStatusValuesByCustomerID
     * @param INT $customerID
     * @return Array of Customer Status Objects
     */
    public function getAllCustomerStatusValuesByCustomerID($customerID) {
        $customerStatusSQLConstructor = new CustomerStatusSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_status_tbl "
                . "WHERE customer_tbl_cust_id = '".$customerID."' ORDER BY customer_status_lookup_tbl_cslt_id ASC");

        //fetch tha data from the database
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['cst_id']] = $customerStatusSQLConstructor->createCustomerStatus($row);
        }

        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    
    /**
     * setNewCustomerStatusObject
     * @param Customer Status Object $statusObject
     * @return INT
     */
    public function setNewCustomerStatusObject($statusObject) {
        $insert = "INSERT INTO customer_status_tbl (customer_tbl_cust_id, customer_status_lookup_tbl_cslt_id) "
                . "VALUES ('".$statusObject->customerID."', '".$statusObject->custStatusLookupID."')";
        
        if (!mysqli_query(Database::$connection, $insert)) {
            debugWriter("debug.txt", "setNewCustomerStatusObject FAILED ".mysqli_error(Database::$connection));
            return null;
        } else {
            return mysqli_insert_id(Database::$connection);
        }
    }
    
    /**
     * deleteCustomerStatusObjectsByStatusID
     * @param INT $statusID
     * @return boolean
     */
    public function deleteCustomerStatusObjectsByStatusID($statusID) {
        $delete = "DELETE FROM customer_status_tbl WHERE cst_id = '".$statusID."'";
        
        if (!mysqli_query(Database::$connection, $delete)) {
            debugWriter("debug.txt", "deleteCustomerStatusObjectsByStatusID FAILED ".mysqli_error(Database::$connection));
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * deleteCustomerStatusObjectsByCustomerID
     * @param INT $customerID
     * @return boolean
     */
    public function deleteCustomerStatusObjectsByCustomerID($customerID) {
        $delete = "DELETE FROM customer_status_tbl WHERE customer_tbl_cust_id = '".$customerID."'";
        
        if (!mysqli_query(Database::$connection, $delete)) {
            debugWriter("debug.txt", "deleteCustomerStatusObjectsByStatusID FAILED ".mysqli_error(Database::$connection));
            return false;
        } else {
            return true;
        }
    }
}
