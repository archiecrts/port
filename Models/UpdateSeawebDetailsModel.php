<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
//include_once ("Models/DB.php");
include_once("Models/Database.php");
include_once 'Models/Entities/UpdateCustomer.php';
include_once 'Models/Entities/UpdateShipData.php';
include_once 'Models/SQLConstructors/UpdateCustomerSQLConstructor.php';
include_once 'Models/SQLConstructors/UpdateShipDataSQLConstructor.php';

/**
 * Description of UpdateCustomerDetailsModel
 *
 * @author Archie
 */
class UpdateSeawebDetailsModel {

    /**
     * getAllUpdatedCustomerDetails
     * @return array of updateCustomer objects
     */
    public function getAllUpdatedCustomerDetails() {
        $updateCustomerSQLConstructor = new UpdateCustomerSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM update_customer_tbl");
        while ($row = mysqli_fetch_assoc($result)) {
            $array['temp_id'] = $updateCustomerSQLConstructor->createUpdateCustomer($row);
        }

        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }

    /**
     * getNewCustomerRecord
     * @return \Customer
     */
    public function getNewCustomerRecord() {

        $result = mysqli_query(Database::$connection, "SELECT temp_id, owcode, short_company_name, 
                telephone, website, company_status, 
                full_company_name, customer_id FROM update_customer_tbl WHERE customer_duplicate = '0' AND 
                customer_exists = '1'");
        if ($result !== FALSE) {
            while ($row = mysqli_fetch_assoc($result)) {
                $array[$row['temp_id']] = new Customer($row['customer_id'], trim($row['short_company_name']), trim($row['telephone']), "", trim($row['website']), "", "0", "0", "0", "0", "", trim($row['owcode']), NULL, NULL, trim($row['company_status']), trim($row['full_company_name']), "");
            }
            if (isset($array)) {
                return $array;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * getNewCustomerAddressRecord
     * @return \CustomerAddress
     */
    public function getNewCustomerAddressRecord() {

        $result = mysqli_query(Database::$connection, "SELECT temp_id, owcode, country_name, town_name,   
                full_address, customer_id, customer_address_id FROM update_customer_tbl WHERE 
                address_duplicate = '0' AND address_exists = '1'");
        if ($result !== FALSE) {
            while ($row = mysqli_fetch_assoc($result)) {
                $array[$row['temp_id']] = new CustomerAddress($row['customer_address_id'], "Office", "", trim($row['town_name']), "", "", trim($row['country_name']), "", "0", trim($row['customer_id']), "0", "1", trim($row["full_address"]));
            }

            if (isset($array)) {
                return $array;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * getNewCustomerContactRecord
     * @return \CustomerContact
     */
    public function getNewCustomerContactRecord() {

        $result = mysqli_query(Database::$connection, "SELECT temp_id, owcode, email, customer_id, customer_address_id FROM update_customer_tbl "
                . "WHERE contact_duplicate = '0' AND contact_exists = '1'");
        if ($result !== FALSE) {
            while ($row = mysqli_fetch_assoc($result)) {
                $array[$row['temp_id']] = new CustomerContact("", "", "", "", "", trim($row['email']), "", "", "SeaWeb", 0, 0, trim($row['customer_id']), trim($row['customer_address_id']));
            }

            if (isset($array)) {
                return $array;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * getAllUpdatedShipDetails
     * @return array of updated ship detail objects
     */
    public function getAllUpdatedShipDetails() {
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM update_ship_data_tbl WHERE ship_duplicate = '0' AND ship_exists='1' LIMIT 10000");
        if ($result !== FALSE) {
            while ($row = mysqli_fetch_assoc($result)) {
                $shipManagerID = "";
                $technicalManagerID = "";
                $gpoID = ""; 
                $status = ""; 
                $buildDate = ""; 
                $shipManagerAddressID = ""; 
                
                $shipManagerQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl "
                    . "WHERE cust_seaweb_code = '" . $row['ship_manager_company_code'] . "'");
                $shipManagerRow = mysqli_fetch_assoc($shipManagerQuery);
                $shipManagerID = $shipManagerRow['cust_id'];

                $shipManagerAddressQuery = mysqli_query(Database::$connection, "SELECT cadt_id FROM customer_address_tbl WHERE "
                        . "customer_tbl_cust_id = '" . $shipManagerID . "' AND cadt_default_address = '1'");

                $shipManagerAddressRow = mysqli_fetch_assoc($shipManagerAddressQuery);
                $shipManagerAddressID = $shipManagerAddressRow['cadt_id'];

                $technicalManagerQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl "
                        . "WHERE cust_seaweb_code = '" . $row['technical_manager_code'] . "'");
                $technicalManagerRow = mysqli_fetch_assoc($technicalManagerQuery);
                $technicalManagerID = $technicalManagerRow['cust_id'];
                
                $gpoQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl "
                        . "WHERE cust_seaweb_code = '" . $row['group_beneficial_owner_company_code'] . "'");
                $gpoRow = mysqli_fetch_assoc($gpoQuery);
                $gpoID = $gpoRow['cust_id'];
                
                $status = 5;
                if (isset($row['ship_status'])) {
                    // this value needs to be taken from the loop-up table.
                    $statusQuery = mysqli_query(Database::$connection, "SELECT stat_id FROM installation_status_tbl WHERE stat_type = 'MARINE'
                                            AND MATCH (stat_status_description)
                                            AGAINST ('" . addslashes($row['ship_status']) . "' IN NATURAL LANGUAGE MODE)");

                    $statusRow = mysqli_fetch_assoc($statusQuery);

                    if (isset($statusRow['stat_id'])) {
                        $status = $statusRow['stat_id'];
                    }
                }
                
                $buildDate = date("Y-m-d H:i:s", time());
                if (isset($row['date_of_build'])) {
                    $year = substr($row['date_of_build'], 0, 4);
                    $month = substr($row['date_of_build'], 4, 2);
                    if ($month == "00") {
                        $month = "01";
                    }
                    $buildDate = $year . "-" . $month . "-01 00:00:00";
                }
                
                $array[$row['temp_id']] = new Installation($row['installation_id'], "MARINE", $row['ship_type_level_2'], $row['ship_name'], $row['lrimo'], 0, $technicalManagerID, $gpoID, $status, "SeaWeb", date("Y-m-d H:i:s", time()), "", "", "", $row['yard_number'], "", "", "", "", $buildDate, "", "", "", $row['propeller_type'], $row['noof_propulsion_units'], $row['ship_builder'], "", null, 0, "", $shipManagerAddressID, $row['flag_name'], $row['lead_ship_in_series_by_imo'], $row['ship_type_level_4'], $row['classification_society']);
            }

            if (isset($array)) {
                return $array;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * getNewCustomerRecordByID
     * @param type $tempID
     * @return \Customer
     */
    public function getNewCustomerRecordByID($tempID) {

        $result = mysqli_query(Database::$connection, "SELECT temp_id, owcode, short_company_name, 
                telephone, website, company_status, 
                full_company_name, customer_id FROM update_customer_tbl WHERE temp_id = '$tempID'");
        $row = mysqli_fetch_assoc($result);
        $customer = new Customer($row['customer_id'], trim($row['short_company_name']), trim($row['telephone']), "", trim($row['website']), "", "0", "0", "0", "0", "", trim($row['owcode']), NULL, NULL, trim($row['company_status']), trim($row['full_company_name']), "");

        return $customer;
    }

    /**
     * getNewCustomerAddressRecordByID
     * @param type $tempID
     * @return \Customer
     */
    public function getNewCustomerAddressRecordByID($tempID) {

        $result = mysqli_query(Database::$connection, "SELECT temp_id, owcode, country_name, town_name,   
                full_address, customer_id, customer_address_id 
                FROM update_customer_tbl WHERE temp_id = '$tempID'");
        $row = mysqli_fetch_assoc($result);
        $address = new CustomerAddress($row['customer_address_id'], "Office", "", trim($row['town_name']), "", "", trim($row['country_name']), "", 0, $row['customer_id'], 0, 1, trim($row['full_address']));

        return $address;
    }
    
    /**
     * getNewShipRecordByID
     * @param type $tempID
     * @return \Installation
     */
    public function getNewShipRecordByID($tempID) {

        $result = mysqli_query(Database::$connection, "SELECT * FROM update_ship_data_tbl WHERE temp_id = '$tempID'");
        $row = mysqli_fetch_assoc($result);
        $shipManagerQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl "
                    . "WHERE cust_seaweb_code = '" . $row['ship_manager_company_code'] . "'");
                $shipManagerRow = mysqli_fetch_assoc($shipManagerQuery);
                $shipManagerID = $shipManagerRow['cust_id'];

                $shipManagerAddressQuery = mysqli_query(Database::$connection, "SELECT cadt_id FROM customer_address_tbl WHERE "
                        . "customer_tbl_cust_id = '" . $shipManagerID . "' AND cadt_default_address = '1'");

                $shipManagerAddressRow = mysqli_fetch_assoc($shipManagerAddressQuery);
                $shipManagerAddressID = $shipManagerAddressRow['cadt_id'];

                $technicalManagerQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl "
                        . "WHERE cust_seaweb_code = '" . $row['technical_manager_code'] . "'");
                $technicalManagerRow = mysqli_fetch_assoc($technicalManagerQuery);
                $technicalManagerID = $technicalManagerRow['cust_id'];
                
                $gpoQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl "
                        . "WHERE cust_seaweb_code = '" . $row['group_beneficial_owner_company_code'] . "'");
                $gpoRow = mysqli_fetch_assoc($gpoQuery);
                $gpoID = $gpoRow['cust_id'];
                
                
                $status = 5;
                if (isset($row['ship_status'])) {
                    // this value needs to be taken from the loop-up table.
                    $statusQuery = mysqli_query(Database::$connection, "SELECT stat_id FROM installation_status_tbl WHERE stat_type = 'MARINE'
                                            AND MATCH (stat_status_description)
                                            AGAINST ('" . addslashes($row['ship_status']) . "' IN NATURAL LANGUAGE MODE)");

                    $statusRow = mysqli_fetch_assoc($statusQuery);

                    if (isset($statusRow['stat_id'])) {
                        $status = $statusRow['stat_id'];
                    }
                }
                
                $buildDate = date("Y-m-d H:i:s", time());
                if (isset($row['date_of_build'])) {
                    $year = substr($row['date_of_build'], 0, 4);
                    $month = substr($row['date_of_build'], 4, 2);
                    if ($month == "00") {
                        $month = "01";
                    }
                    $buildDate = $year . "-" . $month . "-01 00:00:00";
                }
                
        $ship = new Installation($row['installation_id'], "MARINE", $row['ship_type_level_2'], $row['ship_name'], $row['lrimo'], 0, $technicalManagerID, $gpoID, $status, "SeaWeb", date("Y-m-d H:i:s", time()), "", "", "", $row['yard_number'], "", "", "", "", $buildDate, "", "", "", $row['propeller_type'], $row['noof_propulsion_units'], $row['ship_builder'], "", null, 0, "", $shipManagerAddressID, $row['flag_name'], $row['lead_ship_in_series_by_imo'], $row['ship_type_level_4'], $row['classification_society']);
        
        
        return $ship;
    }

    /**
     * updateduplicateRecordByType
     * @param type $type
     * @param type $record
     * @return boolean
     */
    public function updateduplicateRecordByType($type, $record) {
        $duplicate = "" . $type . "_duplicate";
        $exists = "" . $type . "_exists";

        $update = "UPDATE update_customer_tbl SET $duplicate = '1', $exists = '0' WHERE temp_id = '$record'";

        if (!mysqli_query(Database::$connection, $update)) {
            debugWriter("debug.txt", "Failed to update duplicate records " . mysqli_error(Database::$connection));
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * updateDuplicateShipRecord
     * @param type $record
     * @return boolean
     */
    public function updateDuplicateShipRecord($record) {
        $update = "UPDATE update_ship_data_tbl SET ship_duplicate = '1', ship_exists = '0' WHERE temp_id = '$record'";
        //debugWriter("debug.txt", "updateDuplicateShipRecord " . $update);
        if (!mysqli_query(Database::$connection, $update)) {
            debugWriter("debug.txt", "Failed to update duplicate ship records " . mysqli_error(Database::$connection));
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * removeCompleteDuplicatesFromCustomer
     */
    public function removeCompleteDuplicatesFromCustomer() {
        $delete = "DELETE FROM update_customer_tbl WHERE "
                . "(customer_duplicate = '1' AND address_duplicate = '1' AND contact_duplicate = '1') OR "
                . "(customer_duplicate = '1' AND address_duplicate = '1' AND email = '')";

        if (!mysqli_query(Database::$connection, $delete)) {
            debugWriter("debug.txt", "DELETE FAILED " . mysqli_error(Database::$connection));
        }
    }

    /**
     * removeCompleteDuplicatesFromShipData
     */
    public function removeCompleteDuplicatesFromShipData() {
        $delete = "DELETE FROM update_ship_data_tbl WHERE "
                . "ship_duplicate = '1'";

        if (!mysqli_query(Database::$connection, $delete)) {
            debugWriter("debug.txt", "DELETE FAILED " . mysqli_error(Database::$connection));
        }
    }

}