<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/NotifyAdminCategory.php");
include_once 'Models/SQLConstructors/NotifyAdminCategorySQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of NotifyAdminCategoryModel
 *
 * @author Archie
 */
class NotifyAdminCategoryModel {
    
    /**
     * Get All Categories of Notify Admin.
     * @return \NotifyAdminCategory
     */
    public function getAllCategories() {
        $notifyAdminCategorySQLConstructor = new NotifyAdminCategorySQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM issue_types_tbl");
        
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['itt_id']] = $notifyAdminCategorySQLConstructor->createNotifyAdminCategory($row);
        }
        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
}
