<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/InstallationEngine.php");
include_once("Models/SQLConstructors/InstallationEngineSQLConstructor.php");
include_once("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of InstallationEngineModel
 *
 * @author Archie
 */
class InstallationEngineModel {
    
    /**
     * getEngineByProductID
     * @param type $productID
     * @return Installation Engine Object
     */
    public function getEngineByProductID($productID) {
        $installationEngineSQLConstructor = new InstallationEngineSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM installation_engine_tbl AS i 
                LEFT JOIN product_tbl AS p ON i.product_tbl_prod_id = p.prod_id
                LEFT JOIN version_lookup_tbl AS v ON p.version_lookup_tbl_vlt_id = v.vlt_id
                WHERE i.product_tbl_prod_id = '".$productID."'");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);
            
        return $installationEngineSQLConstructor->createInstallationEngine($row);
       
    }
    /**
     * Get installation engines by installation id
     * @param INT $installationID
     * @return Installation Engine Object Array
     */
    public function getInstallationEnginesByInstallationID($installationID) {
        $installationEngineSQLConstructor = new InstallationEngineSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM installation_engine_tbl AS i 
                LEFT JOIN product_tbl AS p ON i.product_tbl_prod_id = p.prod_id
                LEFT JOIN version_lookup_tbl AS v ON p.version_lookup_tbl_vlt_id = v.vlt_id WHERE i.installations_tbl_inst_id='".$installationID."' "
                . "AND i.iet_enquiry_only = '0'");

        //fetch tha data from the database
        while($row = mysqli_fetch_assoc($result)) {
            
            $array[$row['iet_id']] = $installationEngineSQLConstructor->createInstallationEngine($row);
        }
        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    /**
     * getInstallationEngineByID
     * @param INT $engineID
     * @return Installation Engine Object
     */
    public function getInstallationEngineByID($engineID) {
        $installationEngineSQLConstructor = new InstallationEngineSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM installation_engine_tbl AS i 
                LEFT JOIN product_tbl AS p ON i.product_tbl_prod_id = p.prod_id
                LEFT JOIN version_lookup_tbl AS v ON p.version_lookup_tbl_vlt_id = v.vlt_id WHERE i.iet_id='".$engineID."'");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);
            
        $engine =  $installationEngineSQLConstructor->createInstallationEngine($row);
        
        return $engine;
        
        
    }
    
    /**
     * updateInstallationEngineObject
     * @param type $engineObject
     */
    public function updateInstallationEngineObject($engineObject) {

        $string = "UPDATE installation_engine_tbl SET 
            iet_model_description = '".$engineObject->modelDescription."', 
            
            iet_main_aux = '".$engineObject->mainOrAux."',
            iet_engine_verified = '".$engineObject->engineVerified."',
            iet_output = '".$engineObject->output."', 
            iet_fuel_type = '".$engineObject->fuelType."', 
           
            iet_bore_size = '".$engineObject->boreSize."',
            iet_stroke = '".$engineObject->stroke."',
            iet_release_date = '".$engineObject->releaseDate."',
            iet_enquiry_only = '".$engineObject->enquiryOnly."', 
            load_priority_lookup_tbl_load_id = '".$engineObject->loadPriorityID."'    
            WHERE iet_id = '".$engineObject->installationEngineID."'";
        
        if (!(mysqli_query(Database::$connection, $string))) {
            debugWriter("debug.txt", "updateInstallationEngineObject: ".mysqli_error(Database::$connection)."\r\n");
            return mysqli_error(Database::$connection);
        } else {
            return true;
        } 
    }
    
    /**
     * countOfEngines
     * @param INT $installationID
     * @return INT count of engines
     */
    public function countOfEngines($installationID) {
        $result = mysqli_query(Database::$connection, "SELECT count(iet_id) FROM installation_engine_tbl "
                . "WHERE installations_tbl_inst_id='".$installationID."' AND iet_enquiry_only = '0'");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);
            
        $count =  $row['count(iet_id)'];
        
        return $count;
    }
    
    
    /**
     * setNewEngine
     * @param Entity $engine
     * @return INT Insert ID
     */
    public function setNewEngine($engine) {
        $insert = "INSERT INTO installation_engine_tbl ("
                . "iet_model_description, "
                . "iet_main_aux, "
                . "iet_engine_verified, "
                . "iet_output, "
                . "iet_output_unit_of_measure, "
                . "iet_fuel_type, "
               
                . "iet_bore_size, "
                . "iet_stroke, "
                . "iet_release_date, "
                . "iet_enquiry_only, "
                . "load_priority_lookup_tbl_load_id, "
                . "product_tbl_prod_id) "
                . "VALUES ("
                . "'".$engine->modelDescription."', "
                . "'".$engine->mainOrAux."', "
                . "'".$engine->engineVerified."', "
                . "'".$engine->output."', "
                . "'".$engine->outputUnitOfMeasurement."', "
                . "'".$engine->fuelType."', "
               
                . "'".$engine->boreSize."', "
                . "'".$engine->stroke."', "
                . "'".$engine->releaseDate."', "
                . "'".$engine->enquiryOnly."', "
                . "'".$engine->loadPriorityID."', "
                . "'".$engine->productID."'"
                . ")";
        
        if (!(mysqli_query(Database::$connection, $insert))) {
            debugWriter("debug.txt", "setNewEngine: ".mysqli_error(Database::$connection)."\r\n");
            return false;
        } else {
            return mysqli_insert_id(Database::$connection);
        } 
    }
    
    /**
     * getEnginesByCustomerID
     * @param INT $customerID
     * @return Installation Engine Object Array
     */
    public function getEnginesByCustomerID($customerID) {
        $installationEngineSQLConstructor = new InstallationEngineSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM installation_engine_tbl AS i 
                LEFT JOIN product_tbl AS p ON i.product_tbl_prod_id = p.prod_id
                LEFT JOIN version_lookup_tbl AS v ON p.version_lookup_tbl_vlt_id = v.vlt_id "
                . "WHERE p.customer_tbl_cust_id='".$customerID."' AND i.iet_enquiry_only='0'");

        //fetch tha data from the database
        while($row = mysqli_fetch_assoc($result)) {
            
            $array[$row['iet_id']] = $installationEngineSQLConstructor->createInstallationEngine($row);
        }
        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    /**
     * getEngineBySerialNumberAndInstallation
     * @param STRING $serialNumber
     * @param INT $installationID
     * @return type
     */
    public function getEngineBySerialNumberAndInstallation($serialNumber, $installationID) {
        $result = mysqli_query(Database::$connection, "SELECT iet_id FROM installation_engine_tbl "
                . "WHERE installations_tbl_inst_id = '".$installationID."' "
                . "AND iet_serial_number = '".$serialNumber."'");
        
        $row = mysqli_fetch_assoc($result);
        
        if (isset($row['iet_id'])) {
            return $row['iet_id'];
        } else {
            return null;
        }
    }
    
    /**
     * getEngineByInstallationIDAndPosition
     * @param INT $installationID
     * @param STRING $position
     * @return Installation Engine Object Array
     */
    public function getEngineByInstallationIDAndPosition($installationID, $position) {
        $installationEngineSQLConstructor = new InstallationEngineSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM installation_engine_tbl AS i 
                LEFT JOIN product_tbl AS p ON i.product_tbl_prod_id = p.prod_id
                WHERE p.installations_tbl_inst_id='".$installationID."' 
                AND i.iet_position_if_main = '".$position."' ORDER BY iet_id DESC");

        //fetch tha data from the database
        while($row = mysqli_fetch_assoc($result)) {
            
            $array[$row['iet_id']] = $installationEngineSQLConstructor->createInstallationEngine($row);
        }
        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
}
