<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/UserDepartment.php");
include_once("Models/SQLConstructors/UserDepartmentSQLConstructor.php");
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of UserDepartmentModel
 *
 * @author Archie
 */
class UserDepartmentModel {
    
    
    /**
     * getUserDepartments
     * @return Array UserDepartment
     */
    public function getUserDepartments() {
        $userDepartmentSQLConstructor = new UserDepartmentSQLConstructor();

        //execute the SQL query and return records
        $result = mysqli_query(Database::$connection, "SELECT * FROM user_department_tbl ORDER BY dept_name ASC");

        //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {

            $array[$row['dept_id']] = $userDepartmentSQLConstructor->createUserDepartment($row);
        }

        return $array;
    }

    /**
     * getUserDepartmentByID
     * @param INT $deptID
     * @return UserDepartment
     */
    public function getUserDepartmentByID($deptID) {
        $userDepartmentSQLConstructor = new UserDepartmentSQLConstructor();

        //execute the SQL query and return records
        $result = mysqli_query(Database::$connection, "SELECT * FROM user_department_tbl WHERE dept_id='".$deptID."'");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);

        $dept = $userDepartmentSQLConstructor->createUserDepartment($row);
        
        return $dept;
    }
    
    /**
     * insertUserDepartment
     * @param type $department
     * @return type
     */
    public function insertUserDepartment($department) {
        $insert = "INSERT INTO user_department_tbl (dept_name) VALUES ('$department')";
        
        if (!mysqli_query(Database::$connection, $insert)) {
            debugWriter("debug.txt", "INSERT INTO DEPT $insert ". mysqli_error(Database::$connection));
            return null;
        } else {
            return mysqli_insert_id(Database::$connection);
        }
    }
    
}
