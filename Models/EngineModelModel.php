<?php
include_once("Models/Entities/EngineModel.php"); // Market Research entity
include_once("Models/Entities/EngineDBModel.php"); // Engine DB entity TO BE MERGED.
include_once 'Models/SQLConstructors/EngineModelSQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");

/**
 * Description of EngineModelModel
 * SQL for getting the engine models.
 * @author Archie
 */
class EngineModelModel {
    
    public function getDistinctEngineModels() {
        $engineModelSQLConstructor = new EngineModelSQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT DISTINCT(iet_engine_model) FROM "
                . "installation_engine_tbl "
                . "WHERE iet_engine_model != '' "
                . "ORDER BY iet_engine_model ASC");

            //fetch tha data from the database
            while ($row = mysqli_fetch_assoc($result)) {             
                $array[$row['iet_engine_model']] = $engineModelSQLConstructor->createEngineModel($row);
                        
            }
        return $array;
    }
    
    
  
    
    /**
     * 
     * @param type $modelDescription
     * @return type
     */
    public function setEngineModelEngineDB($modelDescription, $engineBuilderID) {
        $model_insert = "INSERT INTO engine_model_tbl (em_model_description, engine_builder_tbl_eb_id) "
                                . "VALUES ('".$modelDescription."','".$engineBuilderID."')";

        if (!mysqli_query(Database::$connection, $model_insert)) {
            return mysqli_error(Database::$connection);
        } else {
            return mysqli_insert_id(Database::$connection);
        }
    }
    
    
    /**
     * 
     * @param type $dbModelID
     * @param type $newModel
     * @return boolean
     */
    public function updateEngineDBModel($dbModelID, $newModel) {
        $update_model_query = "UPDATE engine_model_tbl "
                        . "SET em_model_description = '".$newModel."' "
                        . "WHERE em_id = '".$dbModelID."'";

        if (!mysqli_query(Database::$connection, $update_model_query)) {
            return mysqli_error(Database::$connection);
        } else {
            return true;
        }
    }
    
    /**
     * 
     * @param type $manufacturerID
     * @return boolean|\EngineDBModel
     */
    public function getModelsByManufacturerID($manufacturerID) {
        
        $engineModelSQLConstructor = new EngineModelSQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT em_id, em_model_description "
                . "FROM engine_model_tbl WHERE engine_builder_tbl_eb_id = '".$manufacturerID."'");

        while($row = mysqli_fetch_assoc($result))
        {
            $array[$row['em_id']] 
                    = $engineModelSQLConstructor->createEngineDBModel($row, $manufacturerID);
                    
        }
        
        if (isset($array)) {
            return $array;
        } else {
            return false;
        }
    }
    
    /**
     * getModelsByModelID
     * @param type $modelID
     * @return boolean
     */
    public function getModelsByModelID($modelID) {
        
        $engineModelSQLConstructor = new EngineModelSQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM engine_model_tbl WHERE em_id = '".$modelID."'");

        $row = mysqli_fetch_assoc($result);
        $model = $engineModelSQLConstructor->createEngineDBModelNoManufacturer($row);
                    
        if (isset($model)) {
            return $model;
        } else {
            return false;
        }
    }
}
