<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/Document.php");
include_once 'Models/SQLConstructors/DocumentsSQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");

/**
 * Description of DocumentModel
 * Contains getters and setters for Document Entities
 * @author Archie
 */
class DocumentsModel {

    /**
     * Add Document adds a document details to the DB after the physical doc has been placed in the folder.
     * @param Document Object $documentObject
     * @return boolean
     */
    public function addDocument($documentObject) {
        //execute the SQL query and return records
        $result = "INSERT INTO documents_tbl (doc_url, doc_loaded_date, doc_read_by_date, doc_destination) "
                . "VALUES ("
                . "'" . $documentObject->url . "', "
                . "'" . $documentObject->loadedDate . "', "
                . "'" . $documentObject->readByDate . "',"
                . "'" . $documentObject->destination . "')";

        if (!mysqli_query(Database::$connection, $result)) {
            return false;
        } else {
            return mysqli_insert_id(Database::$connection);
        }
    }
    
    /**
     * UpdateDocument changes the dates for a document.
     * @param Document $documentObject
     * @return boolean
     */
    public function updateDocument($documentObject) {
        //execute the SQL query and return records
        $result = "UPDATE documents_tbl SET doc_loaded_date = '" . $documentObject->loadedDate . "', "
                . "doc_read_by_date = '" . $documentObject->readByDate . "', "
                . "doc_archived = '0' "
                . "WHERE doc_url = '" . $documentObject->url . "' ";

        
        
        if (!mysqli_query(Database::$connection, $result)) {
            return false;
        } else {
            $res = mysqli_query(Database::$connection, "SELECT doc_id FROM documents_tbl WHERE doc_url = '" . $documentObject->url . "'");
            $row = mysqli_fetch_assoc($res);
            return $row['doc_id'];
        }
    }
    
    
    /**
     * This returns an array of Document Objects.
     * @return boolean| Array of Document objects
     */
    public function getDocuments() {
        $documentsSQLConstructor = new DocumentsSQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM documents_tbl WHERE doc_archived = '0' ORDER BY doc_url ASC");
        
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['doc_id']] = $documentsSQLConstructor->createDocument($row);
        }
        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    /**
     * This returns an array of Document Objects.
     * @return boolean| Array of Document objects
     */
    public function getPolicyDocuments() {
        $documentsSQLConstructor = new DocumentsSQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM documents_tbl WHERE doc_archived = '0' AND "
                . "doc_destination = 'policies' ORDER BY doc_url ASC");
        
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['doc_id']] = $documentsSQLConstructor->createDocument($row);
        }
        
        if (isset($array)) {
            return $array;
        } else {
            debugWriter("debug.txt", "getPolicyDocuments FAILED ".mysqli_error(Database::$connection));
            return null;
        }
    }
    
    /**
     * This returns an array of Document Objects.
     * @return boolean| Array of Document objects
     */
    public function getTrainingDocuments() {
        $documentsSQLConstructor = new DocumentsSQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM documents_tbl WHERE doc_archived = '0' AND "
                . "doc_destination = 'training' ORDER BY doc_url ASC");
        
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['doc_id']] = $documentsSQLConstructor->createDocument($row);
        }
        
        if (isset($array)) {
            return $array;
        } else {
            debugWriter("debug.txt", "getTrainingDocuments FAILED ".mysqli_error(Database::$connection));
            return null;
        }
    }
    
    /**
     * This returns an array of Document Objects.
     * @return boolean| Array of Document objects
     */
    public function getBrochureDocuments() {
        $documentsSQLConstructor = new DocumentsSQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM documents_tbl WHERE doc_archived = '0' AND "
                . "doc_destination = 'brochures' ORDER BY doc_url ASC");
        
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['doc_id']] = $documentsSQLConstructor->createDocument($row);
        }
        
        if (isset($array)) {
            return $array;
        } else {
            debugWriter("debug.txt", "getBrochureDocuments FAILED ".mysqli_error(Database::$connection));
            return null;
        }
    }
    
    
    /**
     * 
     * @param type $documentID
     * @return type
     */
    public function getDocumentByID($documentID) {
        $documentsSQLConstructor = new DocumentsSQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM documents_tbl WHERE doc_id = '".$documentID."'");
        
        $row = mysqli_fetch_assoc($result);
        $document = $documentsSQLConstructor->createDocument($row);
     
        
        if (isset($document)) {
            return $document;
        } else {
            return null;
        }
    }
}