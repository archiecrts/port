<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/SupplierParts.php");
include_once("Models/SQLConstructors/SupplierPartSQLConstructor.php");
include_once("Models/SQLConstructors/VersionLookupSQLConstructor.php");
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of SupplierPartsModel
 *
 * @author Archie
 */
class SupplierPartsModel {
    
    /**
     * getSupplierPartByID
     * @param type $partID
     * @return type
     */
    public function getSupplierPartByID($partID) {
        $supplierPartsSQLConstructor = new SupplierPartSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT *  FROM supplier_parts_tbl WHERE spt_id = '$partID'");
        
        $row = mysqli_fetch_assoc($result);
        $part = $supplierPartsSQLConstructor->createSupplierParts($row);
        return $part;
    }
    
    
    /**
     * getPartsBySupplierID
     * @param type $supplierID
     * @return type
     */
    public function getPartsBySupplierID($supplierID) {
        $supplierPartsSQLConstructor = new SupplierPartSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM suppliers_parts_tbl WHERE customer_tbl_cust_id = '$supplierID'");
        
        while ($row = mysqli_fetch_assoc($result)) {
            $part[$row['spt_id']] = $supplierPartsSQLConstructor->createSupplierParts($row);
        }
        
        if (isset ($part)) {
            return $part;
        } else {
            return null;
        }
    }
    
    /**
     * getAllSuppliersByPartCodeID
     * @param type $partCodeID
     * @return type
     */
    public function getAllSuppliersByPartCodeID($partCodeID) {
        $supplierPartsSQLConstructor = new SupplierPartSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM suppliers_parts_tbl WHERE part_code_tbl_part_id = '$partCodeID'");
        
        while ($row = mysqli_fetch_assoc($result)) {
            $part[$row['spt_id']] = $supplierPartsSQLConstructor->createSupplierParts($row);
        }
        
        if (isset ($part)) {
            return $part;
        } else {
            return null;
        }
    }
    
    /**
     * getAllProductVersionsOfSupplier
     * @param type $supplierID
     * @return type
     */
    public function getAllProductVersionsOfSupplier($supplierID) {
        $versionLookupSQLConstructor = new VersionLookupSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM version_lookup_tbl WHERE vlt_id IN 
                (SELECT version_lookup_tbl_vlt_id FROM product_parts_manifest_tbl WHERE
                part_code_tbl_part_id IN (SELECT part_id FROM part_code_tbl WHERE part_id IN 
                (SELECT part_code_tbl_part_id FROM suppliers_parts_tbl WHERE customer_tbl_cust_id = '$supplierID')))");
        
        while ($row = mysqli_fetch_assoc($result)) {
            $part[$row['vlt_id']] = $versionLookupSQLConstructor->createVersionLookup($row);
        }
        
        if (isset ($part)) {
            return $part;
        } else {
            return null;
        }
    }
    
    /**
     * setSupplierPart
     * @param type $supplierPartObject
     * @return type
     */
    public function setSupplierPart($supplierPartObject) {
        
        // check if part already exists.
        $result = mysqli_query(Database::$connection, "SELECT * FROM suppliers_parts_tbl WHERE customer_tbl_cust_id = '$supplierPartObject->customerID' "
                . "AND part_code_tbl_part_id = '$supplierPartObject->partCodeID'");
        debugWriter("debug.txt", mysqli_error(Database::$connection));
        $row = mysqli_fetch_assoc($result);
        
        if (!isset($row)) {
            $insert = "INSERT INTO suppliers_parts_tbl (customer_tbl_cust_id, spt_stock_level, part_code_tbl_part_id, "
                    . "spt_price, spt_date_price_set, spt_lead_time) VALUES ("
                    . "'$supplierPartObject->customerID', "
                    . "'$supplierPartObject->stockLevel', "
                    . "'$supplierPartObject->partCodeID', "
                    . "'$supplierPartObject->partPrice', "
                    . "'$supplierPartObject->datePriceSet',"
                    . "'$supplierPartObject->leadTime')";

            if(!mysqli_query(Database::$connection, $insert)) {
                debugWriter("debug.txt", "setSupplierPart ".mysqli_error(Database::$connection));
                return null;
            } else {
                return mysqli_insert_id(Database::$connection);
            }
        }
        return null;
    }
    
}
