<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/LoadPriority.php");
include_once 'Models/SQLConstructors/LoadPrioritySQLConstructor.php';
include_once("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of LoadPriorityModel
 *
 * @author Archie
 */
class LoadPriorityModel {
    
    /**
     * getLoadPriorityByID
     * @param INT $loadPriorityID
     * @return type
     */
    public function getLoadPriorityByID($loadPriorityID)
    {
        $loadPrioritySQLConstructor = new LoadPrioritySQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM load_priority_lookup_tbl "
                . "WHERE load_id='".$loadPriorityID."'");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);

        
        $loadPriority = $loadPrioritySQLConstructor->createLoadPriority($row);
        
        
        return $loadPriority;
    }
    
    /**
     * getLoadPriorities
     * @return type
     */
    public function getLoadPriorities()
    {
        $loadPrioritySQLConstructor = new LoadPrioritySQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM load_priority_lookup_tbl");

        //fetch tha data from the database
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['load_id']] = $loadPrioritySQLConstructor->createLoadPriority($row);
        }

        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
}
