<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/EngineModelVersion.php");
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of EngineModelVersionModel
 *
 * @author Archie
 */
class EngineModelVersionModel {
    /**
     * 
     * @param type $version
     * @param type $engineModelId
     * @return boolean
     */
    public function setEngineModelVersion($version, $engineModelId) {
        $version_insert = "INSERT INTO engine_model_version_tbl (mv_description, engine_model_tbl_em_id) "
                            . "VALUES ('".$version."','".$engineModelId."')";

        if (!mysqli_query(Database::$connection, $version_insert)) {
            return mysqli_error(Database::$connection);
        } else {
            return true;
        }
    }
    
    
    /**
     * 
     * @param type $versionID
     * @param type $newVersion
     * @return type
     */
    public function updateEngineModelVersion($versionID, $newVersion) {
        
        $update_version_query = "UPDATE engine_model_version_tbl "
                        . "SET mv_description = '".$newVersion."' "
                        . "WHERE mv_id = '".$versionID."'";

        if (!mysqli_query(Database::$connection, $update_version_query)) {
            return mysqli_error(Database::$connection);
        } else {
            true;
        }
        
    }
}
