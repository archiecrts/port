<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/PartColour.php");
include_once("Models/SQLConstructors/PartColourSQLConstructor.php");
include_once("Models/Database.php");
/**
 * Description of PartColourModel
 *
 * @author Archie
 */
class PartColourModel {
    
    /**
     * getPartColourByID
     * @param type $colourID
     * @return type
     */
    public function getPartColourByID($colourID) {
        $partColourSQLConstructor = new PartColourSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM part_colour_tbl WHERE pct_id='$colourID'");
        $row = mysqli_fetch_assoc($result);
        
        $colour = $partColourSQLConstructor->createPartColour($row);
        
        return $colour;
    }
    
    
    /**
     * getAllPartColours
     * @return Array
     */
    public function getAllPartColours() {
        $partColourSQLConstructor = new PartColourSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM part_colour_tbl");
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['pct_id']] = $partColourSQLConstructor->createPartColour($row);
        }
        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
        
    }
}
