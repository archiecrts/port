<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/Customer.php");
include_once("Models/Entities/VersionLookup.php");
include_once 'Models/SQLConstructors/CustomerSQLConstructor.php';
include_once 'Models/SQLConstructors/VersionLookupSQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of CustomerModel
 *
 * @author Archie
 */
class CustomerModel {
    
    
    /**
     * Get Customer Record by ID.
     * @param INT $customerID
     * @return Customer Object
     */
    public function getCustomerByID($customerID) {
        $customerSQLConstructor = new CustomerSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_tbl WHERE cust_id='".$customerID."'");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);

        $customer = $customerSQLConstructor->createCustomer($row);
        
        return $customer;
    }
    
    /**
     * getCustomerByName
     * @param type $customerName
     * @return type
     */
    public function getCustomerByName($customerName) {
        if ($customerName != "") {
            $customerSQLConstructor = new CustomerSQLConstructor();
            $result = mysqli_query(Database::$connection, "SELECT * FROM customer_tbl WHERE cust_name='".addslashes($customerName)."'");

            //fetch tha data from the database
            $row = mysqli_fetch_assoc($result);

            $customer = $customerSQLConstructor->createCustomer($row);

            return $customer;
        } else {
            return null;
        }
    }
    
    /**
     * getAllCustomers
     * @return array of customer objects
     */
    public function getAllCustomers() {
        $customerSQLConstructor = new CustomerSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_tbl ORDER BY cust_name ASC");

        //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['cust_id']] = $customerSQLConstructor->createCustomer($row);
        }

        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    
    /**
     * getCustomerIDByInstallationID
     * @param INT $installationID
     * @return type
     */
    public function getCustomerIDByInstallationID($installationID) {
        $customerSQLConstructor = new CustomerSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_tbl WHERE cust_id IN (SELECT customer_tbl_cust_id FROM 
                customer_tbl_has_installations_tbl WHERE installations_tbl_inst_id='".$installationID."')");
        
        $row = mysqli_fetch_assoc($result);
        
        $customer = $customerSQLConstructor->createCustomer($row);
        
        return $customer;
    }
    
    
    /**
     * updateCustomerObject
     * @param Entity Object $customerObject
     * @return boolean
     */
    public function updateCustomerObject($customerObject) {
        $accountManager = " NULL ";
        if ($customerObject->accountManager != "") {
            $accountManager = " '". $customerObject->accountManager."'";
        }
        $string = "UPDATE customer_tbl SET 
            cust_name = '".addslashes($customerObject->customerName)."', 
            cust_telephone = '".addslashes($customerObject->mainTelephone)."',
            cust_fax = '".addslashes($customerObject->mainFax)."',
            cust_website = '".addslashes($customerObject->website)."',
            cust_account_manager = ".$accountManager.",
            cust_header_notes = '".addslashes($customerObject->headerNotes)."',
            cust_company_full_name = '".addslashes($customerObject->companyFullName)."', 
            cust_navision_id = '".addslashes($customerObject->navisionID)."'      
            WHERE cust_id = '".$customerObject->customerID."'";
        
        if (!(mysqli_query(Database::$connection, $string))) {
            debugWriter("debug.txt", "updateCustomerObject: ".mysqli_error(Database::$connection)."\r\n");
            return "Customer profile not updated";
        } else {
            return true;
        } 
    }
    
    /**
     * setCustomerObject
     * @param Entity Object $customerObject
     * @return boolean
     */
    public function setCustomerObject($customerObject) {
        $accountManager = " NULL ";
        if ($customerObject->accountManager != "") {
            $accountManager = " '". $customerObject->accountManager."'";
        }
        $string = "INSERT INTO customer_tbl (cust_name, 
                cust_telephone, cust_fax, cust_website, cust_account_manager, 
                cust_header_notes, cust_navision_id) VALUES (
                '".addslashes($customerObject->customerName)."', 
                '".addslashes($customerObject->mainTelephone)."',
                '".addslashes($customerObject->mainFax)."',
                '".addslashes($customerObject->website)."',
                ".$accountManager.",
                '".addslashes($customerObject->headerNotes)."', 
                '".addslashes($customerObject->navisionID)."')";
        
        if (!(mysqli_query(Database::$connection, $string))) {
            debugWriter("debug.txt", "setCustomerObject: ".mysqli_error(Database::$connection)."\r\n");
            return "Customer profile not created";
        } else {
            return mysqli_insert_id(Database::$connection);
        } 
    }
    
    /**
     * getInstallationsWithConversationEntriesForAccountManager
     * @param type $accountManager
     * @return \Installation
     */
    public function getCustomersWithConversationEntriesForAccountManager($accountManager) {

        $customerSQLConstructor = new CustomerSQLConstructor();
        $customerAddressSQLConstructor = new CustomerAddressSQLConstructor();
        $customerConversationSQLConstructor = new CustomerConversationSQLConstructor();
        $customerConversationHistorySQLConstructor = new CustomerConversationHistorySQLConstructor();
        $array =[];
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_conversation_tbl c, customer_contacts_tbl i, 
                                customer_conversation_history_tbl h, customer_tbl q, customer_address_tbl a  
                                WHERE c.customer_contacts_tbl_ict_id = i.ict_id 
                                AND c.con_id = h.customer_conversation_tbl_con_id 
                                AND con_id NOT IN (SELECT customer_conversation_tbl_con_id FROM 
                                                    customer_conversation_history_tbl WHERE 
                                                    ich_status = '0') 
                                AND q.cust_id = i.customer_tbl_cust_id   
                                AND (q.cust_id = a.customer_tbl_cust_id AND a.cadt_default_address = '1')
                                AND (a.cadt_country IN (
                                    SELECT c_name 
                                    FROM country_tbl 
                                    WHERE c_continent IN (
                                        SELECT ut_territory_name 
                                        FROM user_territory_tbl 
                                        WHERE user_tbl_usr_id='" . $accountManager . "')
                                    OR c_area IN (
                                        SELECT ut_territory_name 
                                        FROM user_territory_tbl 
                                        WHERE user_tbl_usr_id='" . $accountManager . "')
                                    OR c_name IN(
                                        SELECT ut_territory_name 
                                        FROM user_territory_tbl 
                                        WHERE user_tbl_usr_id='" . $accountManager . "')
                                    AND q.cust_account_manager IS NULL or q.cust_account_manager = '" . $accountManager . "')
                                    OR h.user_tbl_usr_id = '" . $accountManager . "')
                                ORDER BY con_id DESC;");
        
        while ($row = mysqli_fetch_assoc($result)) {
            
            $array[$row['con_id']]['customer'] = $customerSQLConstructor->createCustomer($row);
            
            $array[$row['con_id']]['address'] = $customerAddressSQLConstructor->createCustomerAddress($row);
            
            $array[$row['con_id']]['conversation'] = $customerConversationSQLConstructor->createCustomerConversation($row);
                        
            $array[$row['con_id']]['history'] = $customerConversationHistorySQLConstructor->createCustomerConversationHistory($row);
                      
        }
        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
        
    }
    
    
    /**
     * getInstallationsWithConversationEntriee
     * @param INT $userID
     * @return \Installation
     */
    public function getCustomersWithConversationEntries($userID) {
        
        $customerSQLConstructor = new CustomerSQLConstructor();
        $customerAddressSQLConstructor = new CustomerAddressSQLConstructor();
        $customerConversationSQLConstructor = new CustomerConversationSQLConstructor();
        $customerConversationHistorySQLConstructor = new CustomerConversationHistorySQLConstructor();
        $array = [];
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_conversation_tbl c, customer_contacts_tbl i, 
                                customer_conversation_history_tbl h, customer_tbl q, customer_address_tbl a  
                                WHERE c.customer_contacts_tbl_ict_id = i.ict_id 
                                AND c.con_id = h.customer_conversation_tbl_con_id 
                                AND con_id NOT IN (SELECT customer_conversation_tbl_con_id FROM 
                                                    customer_conversation_history_tbl WHERE 
                                                    ich_status = '0') 
                                AND q.cust_id = i.customer_tbl_cust_id   
                                AND (q.cust_id = a.customer_tbl_cust_id AND a.cadt_default_address = '1')
                                ORDER BY con_id DESC");
        
        while ($row = mysqli_fetch_assoc($result)) {
            
            $array[$row['con_id']]['customer'] = $customerSQLConstructor->createCustomer($row);
            
            $array[$row['con_id']]['address'] = $customerAddressSQLConstructor->createCustomerAddress($row);
            
            $array[$row['con_id']]['conversation'] = $customerConversationSQLConstructor->createCustomerConversation($row);
                        
            $array[$row['con_id']]['history'] = $customerConversationHistorySQLConstructor->createCustomerConversationHistory($row);
                   
        }
        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    /**
     * getCustomerAddressesByCountryArray
     * @param type $country
     * @param type $area
     * @param type $continent
     * @return type
     */
    public function getCustomerByCountryArray($country, $area, $continent, $customerType) {
        $customerSQLConstructor = new CustomerSQLConstructor();
        
        $continentSQL = "";
        
        if (isset($continent[0]) && ($continent[0] != 'all')) 
        {
            foreach ($continent as $index => $value) 
            {
                if ($value != "all" && $index == 0)
                {
                    $continentSQL .= "a.cadt_country IN (SELECT c_name FROM country_tbl "
                            . "WHERE c_continent = '" . utf8_decode($value) . "') ";
                } 
                else if ($value != "all" && $index > 0) 
                {
                    $continentSQL .= "OR a.cadt_country IN (SELECT c_name FROM country_tbl "
                            . "WHERE c_continent = '" . utf8_decode($value) . "') ";
                }
            }
        }
        
        $areaSQL = "";
        
        if (isset($area[0]) && ($area[0] != 'all')) 
        {
            foreach ($area as $index => $value) 
            {
                if ($value != "all" && $index == 0)
                {
                    $areaSQL .= "a.cadt_country IN (SELECT c_name FROM country_tbl "
                            . "WHERE c_area = '" . utf8_decode($value) . "') ";
                } 
                else if ($value != "all" && $index > 0) 
                {
                    $areaSQL .= "OR a.cadt_country IN (SELECT c_name FROM country_tbl "
                            . "WHERE c_area = '" . utf8_decode($value) . "') ";
                }
            }
        }

        $countrySQL = "";
        
        if (isset($country[0]) && ($country[0] != 'all')) 
        {
            foreach ($country as $index => $value) 
            {
                if ($value != "all" && $index == 0)
                {
                    $countrySQL .= "a.cadt_country = '" . utf8_decode($value) . "' ";
                } 
                else if ($value != "all" && $index > 0) 
                {
                    $countrySQL .= "OR a.cadt_country = '" . utf8_decode($value) . "' ";
                }
            }
        }
        
        $SQLStart = "SELECT c.* FROM customer_tbl AS c "
                . "LEFT JOIN customer_address_tbl AS a ON c.cust_id = a.customer_tbl_cust_id "
                . "LEFT JOIN country_tbl AS q ON a.cadt_country = q.c_name "
                . "LEFT JOIN customer_status_tbl AS s ON c.cust_id = s.customer_tbl_cust_id ";
        $refArray1["continent"] = "(" . $continentSQL . ")";
        $refArray1["area"] = "(" . $areaSQL . ")";
        $refArray1["country"] = "(" . $countrySQL . ")";
        
        $ct = 0;
        $string1 = $SQLStart;
        foreach ($refArray1 as $key => $sql) {
            
            if (($sql != '') && ($sql != "()")) {
                if ($ct == 0) {
                    $string1.= " WHERE ";
                    $ct++;
                } else {
                    $string1.= " AND ";
                }
                $string1.= $sql;
            }
        }
        $string1 .= " AND s.customer_status_lookup_tbl_cslt_id='$customerType' group by c.cust_name ORDER BY c.cust_name"; 
        //debugWriter("debug.txt", "findCustomerByCountry STRING  ".$string1);
        
        $result = mysqli_query(Database::$connection, $string1);

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);

        while ($row = mysqli_fetch_assoc($result)) {
            
            $array[$row['cust_id']] = $customerSQLConstructor->createCustomer($row);
            
        }

        if (isset($array)) {
            return $array;
        } else {
            debugWriter("debug.txt", "findCustomerByCountry FAILED  ".mysqli_error(Database::$connection));
            return null;
        }
    }
    
    /**
     * getCustomerByProductConfiguration
     * @param type $designer
     * @param type $series
     * @param type $cylinderCount
     * @param type $cylinderConfiguration
     */
    public function getCustomerByProductConfiguration($designer = 'all', $series = 'all', $cylinderCount = 0, $cylinderConfiguration = 0) {
        $customerSQLConstructor = new CustomerSQLConstructor();
        $versionLookupSQLConstructor = new VersionLookupSQLConstructor();
        
        $SQLStart = "SELECT * FROM customer_tbl AS c 
            INNER JOIN customer_address_tbl AS a ON c.cust_id = a.customer_tbl_cust_id 
            INNER JOIN suppliers_parts_tbl AS p ON c.cust_id = p.customer_tbl_cust_id
            INNER JOIN part_code_tbl AS q ON q.part_id = p.part_code_tbl_part_id
            INNER JOIN product_parts_manifest_tbl AS m ON m.part_code_tbl_part_id = q.part_id
            INNER JOIN version_lookup_tbl AS v ON m.version_lookup_tbl_vlt_id = v.vlt_id 
            INNER JOIN series_lookup_tbl AS s ON v.model_lookup_tbl_mlt_id = s.mlt_id
            INNER JOIN designer_lookup_tbl AS d ON s.designer_lookup_tbl_dlt_id = d.dlt_id 
            ";
        
        $engineManufacturerSQL = "";

        if (isset($designer) && ($designer != 'all')) 
        {
            $engineManufacturerSQL .= "d.dlt_id = '" . utf8_decode($designer) . "' ";
        }

      
        $engineModelSQL = "";

        if (isset($series) && ($series != 'all')) 
        {
            $engineModelSQL .= "s.mlt_id = '" . utf8_decode($series) . "' ";
        } 
               
        
        $refArray1["engMan"] = "(" . $engineManufacturerSQL . ")";
        $refArray1["engModel"] = "(" . $engineModelSQL . ")";
        
        $ct = 0;
        $string1 = $SQLStart;
        foreach ($refArray1 as $key => $sql) {
            
            if (($sql != '') && ($sql != "()")) {
                if ($ct == 0) {
                    $string1.= " WHERE ";
                    $ct++;
                } else {
                    $string1.= " AND ";
                }
                $string1.= $sql;
            }
        }
        $string1 .= " group by c.cust_name  ORDER BY c.cust_name"; 
        //debugWriter("debug.txt", "Get customer by product String  ".$string1); 
        


        //execute the SQL query and return records
        $result = mysqli_query(Database::$connection, $string1);
        while ($row = mysqli_fetch_assoc($result)) {
            
             $array[$row['cust_id']]['customer'] = $customerSQLConstructor->createCustomer($row);
             $array[$row['cust_id']]['version'] = $versionLookupSQLConstructor->createVersionLookup($row);
            
        }

        if (isset($array)) {
            return $array;
        } else {
            debugWriter("debug.txt", "Get customer by product FAILED  ".mysqli_error(Database::$connection));
            return null;
        }
    }
    
    
    /**
     * getCustomerParentByCode
     * @param type $customerSeawebCode
     * @return type
     */
    public function getCustomerParentByCode($customerSeawebCode) {
        $customerSQLConstructor = new CustomerSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_tbl WHERE cust_seaweb_code='".$customerSeawebCode."'");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);

        $customer = $customerSQLConstructor->createCustomer($row);
        
        return $customer;
    }
    
    /**
     * getAllCustomerChildren
     * @param type $parentCode
     * @return type
     */
    public function getAllCustomerChildren($parentCode) {
        $customerSQLConstructor = new CustomerSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_tbl WHERE cust_parent_company_seaweb_code = '$parentCode'");

        //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['cust_id']] = $customerSQLConstructor->createCustomer($row);
        }

        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
}
