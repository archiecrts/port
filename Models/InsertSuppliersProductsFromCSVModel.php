<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of InsertSuppliersProductsFromCSVModel
 *
 * @author Archie
 */
class InsertSuppliersProductsFromCSVModel {
    // SET GLOBAL VARIABLES
    private $dbColumnName;
    private $newColumnNameArray;
    private $replace;
    
    
    /**
     * createTemporaryTable
     * @param Array $columnNameArray
     */
    private function createTemporaryTable($columnNameArray, $type) {

        if (($type != 'UNSPECIFIED') && ($type != 'inst_type')) {
            $typeString = " inst_type VARCHAR(10), ";
        } else {
            $typeString = "";
        }

        /*
         * Create a temporary table to contain the CSV data in the same order as the CSV.
         */
        $create_table_query = "CREATE TEMPORARY TABLE temporary_suppliers_products_table (temp_id SERIAL, ";
        // Go through the column names array
        foreach ($columnNameArray as $p => $q) {
            $p = str_replace($this->replace, "_", $p); // Use the REGEX to replace bad characters with underscore

            $create_table_query .= " " . addslashes($p) . " VARCHAR(45), "; // This adds to the string that creates the table
            $this->dbColumnName .= " " . addslashes($p) . ", "; // This does similar for a select

            $key = str_replace($this->replace, "_", $p); // This shouldnt technically be needed but the code complained without it.
            $this->newColumnNameArray[$key] = $q; // create a new array with the new CSV column names
        }
        $create_table_query .= "$typeString PRIMARY KEY(temp_id))"; // set the primary key of the table


        /*
         * mysqli_query(Database::$connection, ) runs a sql query, putting ! in front of it says "if the query fails"
         */
        if (!mysqli_query(Database::$connection, $create_table_query)) {
            debugWriter("loadSuppliersProductsDataLogs.txt", "Can't create table : " . $create_table_query . " \r\n" . mysqli_error(Database::$connection) . "");
        }
    }
    
    
    /**
     * loadFile
     * @param STRING $fileName
     * @param BOOLEAN $headers
     * @param STRING $type
     * @param STRING $dbColumnName
     */
    private function loadFile($fileName, $headers, $type) {
        // Add the CSV data to the temp table.
        // Ignore the first line in the file because these are column headers.
        if ($headers == 1) {
            $ignoreFirstLine = "IGNORE 1 LINES ";
        } else {
            $ignoreFirstLine = "";
        }

        // Location type might be in the csv, if its not then we manually set it for the whole file and that is
        // decided for the SQL statement here.
        if (($type != 'UNSPECIFIED') && ($type != 'inst_type')) {
            $typeString = "SET inst_type = '$type'";
        } else {
            $typeString = "";
        }

        $new_query_string = substr($this->dbColumnName, 0, -2); // removes the last character from the string (its a comma that shouldnt be there)
        // Load the data from the csv into the temporary table, using the column names generated on the verify page.
        $insert_temp_query = "LOAD DATA LOCAL INFILE '" . $fileName . "'
                INTO TABLE temporary_suppliers_products_table
                CHARACTER SET 'latin1'
                COLUMNS TERMINATED BY ','
                ENCLOSED BY '\"' LINES TERMINATED BY '\r\n'
                $ignoreFirstLine ($new_query_string) $typeString";


        // DATABASE query generator. This say, if mysqli_query fails then write the first line. otherwise write the second.
        if (!mysqli_query(Database::$connection, $insert_temp_query)) {
            debugWriter("loadSuppliersProductsDataLogs.txt", "Can't insert records into temporary table : " . $new_query_string . "*** " . mysqli_error(Database::$connection));
        }
    }

    /**
     * cleanTempData
     * 
     */
    private function cleanTempData() {
        $cleanQuery = "UPDATE temporary_suppliers_products_table SET ";
        // clean out leading and trailing whitespaces 
        foreach ($this->newColumnNameArray as $key => $name) {
            $key = str_replace($this->replace, "_", $key);
            $cleanQuery .= " " . $key . "= TRIM(" . addslashes($key) . "), ";
        }
        $cleanedQuery = substr($cleanQuery, 0, -2);
        mysqli_query(Database::$connection, $cleanedQuery);

        // Do a quick count of the new records, This is currently used for debug, but may stay in the code.
        $quickCount = mysqli_query(Database::$connection, "SELECT count(*) FROM temporary_suppliers_products_table");
        $countQuick = mysqli_fetch_assoc($quickCount);
        print "<p>Temp table Count: " . $countQuick['count(*)'] . ". Before duplicates are removed</p>";
    }
    
    
    /**
     * matchSupplierToDBSupplier
     */
    /*private function matchProductToSupplier() {
        
        
        $customerQuery = "SELECT temp_id, " . array_search('cust_name', $this->newColumnNameArray) . " AS cust_name ";
        
        foreach ($this->newColumnNameArray as $key => $name) {
            $key = str_replace($this->replace, "_", $key);
            if ($name == 'cust_tbh_id') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cust_tbh_account_code') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cust_ld_id') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cust_ld_account_code') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'dlt_designer_description') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'mlt_series') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'part_code') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'part_description') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
        }
        
        $customerQuery .= " FROM temporary_suppliers_products_table";
        
        //debugWriter("debug.txt", "Customer Query ".$customerQuery);
        $customerInsert = mysqli_query(Database::$connection, $customerQuery);
        while ($customerQueryRow = mysqli_fetch_assoc($customerInsert)) {
            print "<pre>";
            print_r($customerQueryRow);
            print "</pre>";
            
            if (isset($customerQueryRow['cust_name'])) {
                $query1 = mysqli_query(Database::$connection, "SELECT ");
            }
        }
        
    }*/

   
    
    
    
    /**
     * importDataFromCSV
     * @param STRING $fileName
     * @param ARRAY $columnNameArray
     * @param BOOLEAN $headers
     * @param STRING $type
     */
    public function importProductDataFromCSV($fileName, $columnNameArray, $headers, $type) {

        
        // This writes out to a file the data/time and file name when things are uploaded.
        /*debugWriter("loadSuppliersProductsDataLogs.txt", "Log Date: " . date("Y-m-d H:i", time()) . " USER: " . $_SESSION['name'] . "");
        debugWriter("loadSuppliersProductsDataLogs.txt", "File Name: " . $fileName . "");
        foreach ($columnNameArray as $p => $q) {
            debugWriter("loadSuppliersProductsDataLogs.txt", "Column Name Array: " . $p . " - " . $q . "");
        }
        debugWriter("loadSuppliersProductsDataLogs.txt", "Headers Used: " . $headers . "");
        debugWriter("loadSuppliersProductsDataLogs.txt", "Type: " . $type . "");*/

        // GLOBAL VARIABLES

        $this->dbColumnName = "";
        $this->newColumnNameArray = [];
        $this->replace = ["/", "-", " ", ".", "#"]; // REGEX (Regular Expression) for clearing out these characters
        // create the temporary table.
        $this->createTemporaryTable($columnNameArray, $type);

        $this->loadFile($fileName, $headers, $type);

        $this->cleanTempData();
        
        //$this->matchProductToSupplier();
        

        /**
         * Drop the temporary table.
         */
        $drop_table = "DROP TABLE IF EXISTS temporary_suppliers_products_table";
        if (!mysqli_query(Database::$connection, $drop_table)) {
            debugWriter("loadSuppliersProductsDataLogs.txt", "Can't drop table - gravity failure : " . mysqli_error(Database::$connection) . "");
        }
    }
}
