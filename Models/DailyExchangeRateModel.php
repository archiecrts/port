<?php
include_once("Models/Entities/DailyExchangeRate.php");
include_once 'Models/SQLConstructors/DailyExchangeRateSQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of DailyExchangeRateModel
 *
 * @author Archie
 */
class DailyExchangeRateModel {
    
    /**
     * getAllDailyRates
     * @return type
     */
    public function getAllDailyRates() {
        $dailyExchangeRateSQLConstructor = new DailyExchangeRateSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM daily_exchange_rates_tbl");
        
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['daily_id']] = $dailyExchangeRateSQLConstructor->createDailyExchnageRate($row);
        }
        
        if (isset($array)){
            return $array;
        } else {
            return null;
        }
    }
    
    /**
     * updateDailyRate
     * @param INT $rateID
     * @param FLOAT $rate
     * @return boolean
     */
    public function updateDailyRate($rateID, $rate) {
        $update = "UPDATE daily_exchange_rates_tbl SET "
                . "daily_currency_unit_per_pound = '".$rate."', "
                . "daily_last_changed_date = NOW() WHERE daily_id = '".$rateID."'";
        
        if (!mysqli_query(Database::$connection, $update)) {
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * matchCurrencyCodesHMRCToDaily
     * @param type $currencyCode
     * @return type
     */
    public function matchCurrencyCodesHMRCToDaily($currencyCode) {
        $result = mysqli_query(Database::$connection, "SELECT hmrc_country FROM hmrc_monthly_exchange_rate_tbl "
                . "WHERE hmrc_currency_code = '".$currencyCode."'");
        
        $row = mysqli_fetch_assoc($result);
        return $row['hmrc_country'];
    }
}
