<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/TradingZoneLastSeen.php");
include_once("Models/SQLConstructors/TradingZoneLastSeenSQLConstructor.php");
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of TradingZoneLastSeenModel
 *
 * @author Archie
 */
class TradingZoneLastSeenModel {
    
    /**
     * getAllTradingZonesForInstallation
     * @param type $installationID
     * @return type
     */
    public function getAllTradingZonesForInstallation($installationID) {
        $tradingZoneLastSeenSQLConstructor = new TradingZoneLastSeenSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM trading_zone_last_seen_tbl WHERE "
                . "installations_tbl_inst_id = '$installationID' ORDER BY tzlst_last_seen_date DESC");
        
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['tzlst_id']] = $tradingZoneLastSeenSQLConstructor->createTradingZoneLastSeen($row);
        }
        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
        
    }
}
