<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/CustomerStatusLookup.php");
include_once 'Models/SQLConstructors/CustomerStatusLookupSQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of CustomerStatusLookupModel
 *
 * @author Archie
 */
class CustomerStatusLookupModel {
    
    /**
     * getCustomerStatusLookupByID
     * @param INT $customerStatusLookupID
     * @return Customer Status Lookup Object
     */
    public function getCustomerStatusLookupByID($customerStatusLookupID) {
        $customerStatusLookupSQLConstructor = new CustomerStatusLookupSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_status_lookup_tbl WHERE cslt_id='".$customerStatusLookupID."'");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);

        $customerStatusLookup = $customerStatusLookupSQLConstructor->createCustomerStatusLookup($row);
        
        return $customerStatusLookup;
    }
    
    
    /**
     * getAllCustomerStatusLookupValues
     * @return Array of Customer Status Lookup Objects
     */
    public function getAllCustomerStatusLookupValues() {
        $customerStatusLookupSQLConstructor = new CustomerStatusLookupSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_status_lookup_tbl");

        //fetch tha data from the database
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['cslt_id']] = $customerStatusLookupSQLConstructor->createCustomerStatusLookup($row);
        }

        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    /**
     * setNewCustomerStatusLookupObject
     * @param STRING $status
     * @return INT | NULL
     */
    public function setNewCustomerStatusLookupObject($status) {
        $insert = "INSERT INTO customer_status_lookup_tbl (cslt_status) VALUES ('".$status."')";
        
        if (!mysqli_query(Database::$connection, $insert)) {
            debugWriter("debug.txt", "setNewCustomerStatusLookupObject FAILED ".mysqli_error(Database::$connection));
            return null;
        } else {
            return mysqli_insert_id(Database::$connection);
        }
    }
}
