<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/VersionLookup.php");
include_once 'Models/SQLConstructors/VersionLookupSQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of VersionLookupModel
 *
 * @author Archie
 */
class VersionLookupModel {
    
    /**
     * getVersionIDBySeriesID
     * @param type $seriesLookupID
     * @return type
     */
    public function getVersionIDBySeriesID($seriesLookupID) {
        
        $versionLookupSQLConstructor = new VersionLookupSQLConstructor();
        $versionResult = mysqli_query(Database::$connection, "SELECT * FROM version_lookup_tbl "
                . "WHERE model_lookup_tbl_mlt_id = '".$seriesLookupID."' ORDER BY vlt_cylinder_configuration ASC, vlt_cylinder_count ASC");
        while($row = mysqli_fetch_assoc($versionResult)) {
        //debugWriter("debug.txt", "GET VERSION ID BY SERIES ERROR ".  mysqli_error(Database::$connection));
            $version[$row['vlt_id']] = $versionLookupSQLConstructor->createVersionLookup($row);
        }
        
        if (isset($version)) {
            return $version;
        } else {
            return null;
        }
    }
    
    
    /**
     * getVersionByID
     * @param type $versionID
     * @return type
     */
    public function getVersionByID($versionID) {
        $versionLookupSQLConstructor = new VersionLookupSQLConstructor();
        $versionResult = mysqli_query(Database::$connection, "SELECT * FROM version_lookup_tbl "
                . "WHERE vlt_id = '".$versionID."'");
        $row = mysqli_fetch_assoc($versionResult);
        $version = $versionLookupSQLConstructor->createVersionLookup($row);
        
        
        if (isset($version)) {
            return $version;
        } else {
            return null;
        }
    }
}
