<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once("Models/Entities/Training.php");
include_once 'Models/SQLConstructors/TrainingSQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of TrainingModel
 *
 * @author Archie
 */
class TrainingModel {
    
    /**
     * addDocumentToUser adds a recently uploaded document to a user so it can be tracked as read.
     * @param INT $userID
     * @param INT $documentID
     * @return boolean
     */
    public function addDocumentToUser($userID, $documentID) {
        //execute the SQL query and return records
        $result = "INSERT INTO user_training_tbl (tr_has_been_read, tr_hr_confirmation, user_tbl_usr_id, documents_tbl_doc_id) "
                . "VALUES ('0', '0', '".$userID."', '".$documentID."')";

        if (!mysqli_query(Database::$connection, $result)) {
            return false;
        } else {
            return true;
        }
    }
    
    
    /**
     * Returns one record based on user ID and document ID.
     * @param INT $userID
     * @param INT $documentID
     * @return Training Object
     */
    public function getRecordByUserIDandDocID($userID, $documentID) {
        $trainingSQLConstructor = new TrainingSQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM user_training_tbl "
                . "WHERE user_tbl_usr_id='".$userID."' "
                . "AND documents_tbl_doc_id='".$documentID."'");
        
        $row = mysqli_fetch_assoc($result);
        $record = $trainingSQLConstructor->createTraining($row);

        return $record;
    }
    
    
    /**
     * getReadReceiptsByDocumentID Gets all training rows where users 
     * have not clicked to say they have read the form.
     * @param INT $documentID
     * @return boolean|\Training
     */
    public function getReadReceiptsByDocumentID($documentID) {
        $trainingSQLConstructor = new TrainingSQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM user_training_tbl "
                . "WHERE documents_tbl_doc_id='".$documentID."' "
                . "AND tr_has_been_read = '0'");
        
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['tr_id']] = $trainingSQLConstructor->createTraining($row);
        }
        
        if(isset($array)) {
            return $array;
        } else {
            return false;
        }
    }
}
