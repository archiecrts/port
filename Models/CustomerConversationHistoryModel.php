<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/CustomerConversationHistory.php");
include_once 'Models/SQLConstructors/CustomerConversationHistorySQLConstructor.php';
////include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of InstallationConverationHistoryModel
 *
 * @author Archie
 */
class CustomerConversationHistoryModel {
    
    /**
     * 
     * @param type $conversationID
     * @return type
     */
    public function isConversationItemClosed($conversationID) {
        $result = mysqli_query(Database::$connection, "SELECT count(ich_id) FROM customer_conversation_history_tbl "
                . "             WHERE customer_conversation_tbl_con_id = '".$conversationID."'");
        
        $row = mysqli_fetch_assoc($result);
        
        return $row['count(ich_id)'];
    }
    
    /**
     * 
     * @param type $conversationID
     * @return \CustomerConversationHistory
     */
    public function getHistoryByConversationID($conversationID) {
        $customerConversationHistorySQLConstructor = new CustomerConversationHistorySQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_conversation_history_tbl "
                . " WHERE customer_conversation_tbl_con_id = '".$conversationID."' "
                . " ORDER BY ich_id ASC");
        
        while ($row = mysqli_fetch_assoc($result)) {
            $array[] = $customerConversationHistorySQLConstructor->createCustomerConversationHistory($row);
        }
        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
        
    }
    
}
