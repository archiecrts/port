<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once("Models/Entities/NoticeBoard.php");
include_once("Models/Entities/NoticeBoardCategories.php");
include_once 'Models/SQLConstructors/NoticeBoardSQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of NoticeBoardModel
 *
 * @author Archie
 */
class NoticeBoardModel {
    
    /**
     * 
     * @return \NoticeBoard
     */
    public function getTrainingNotices() {
        $noticeBoardSQLConstructor = new NoticeBoardSQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM notice_board_tbl "
                . "WHERE notice_board_categories_tbl_nbtc_id IN (SELECT nbtc_id "
                . "FROM notice_board_categories_tbl WHERE nbtc_category='Training') "
                . "AND nbt_archive = '0' AND nbt_authorised = '1' "
                . "ORDER BY nbt_date DESC LIMIT 3");
        
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['nbt_id']] = $noticeBoardSQLConstructor->createNoticeBoard($row);
                    
        }
        
        if (!isset($array)) {
            return null;
        } else {
            return $array;
        }
    }
    
    /**
     * 
     * @return \NoticeBoard
     */
    public function getAllNoticesDescendingOrder() {
        $noticeBoardSQLConstructor = new NoticeBoardSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM notice_board_tbl WHERE nbt_archive = '0' "
                . "AND nbt_authorised = '1' ORDER BY nbt_date DESC");
        
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['nbt_id']] = $noticeBoardSQLConstructor->createNoticeBoard($row);
        }
        
        if (!isset($array)) {
            return null;
        } else {
            return $array;
        }
    }
    
    /**
     * getAllNoticesByCategoryDescendingOrder gets all notices of the specified category ID.
     * @param INT $categoryID
     * @return array NoticeBoard Objects
     */
    public function getAllNoticesByCategoryDescendingOrder($categoryID) {
        $noticeBoardSQLConstructor = new NoticeBoardSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM notice_board_tbl "
                . "WHERE nbt_archive = '0' AND nbt_authorised = '1' "
                . "AND notice_board_categories_tbl_nbtc_id='".$categoryID."' ORDER BY nbt_date DESC");
        
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['nbt_id']] = $noticeBoardSQLConstructor->createNoticeBoard($row);
        }
        
        if (!isset($array)) {
            return null;
        } else {
            return $array;
        }
    }
    
    /**
     * 
     * @param NoticeBoardObject $noticeBoardObject
     * @return Boolean
     */
    public function setNewNoticeArticle($noticeBoardObject) {
        
        $result = "INSERT INTO notice_board_tbl (nbt_title, nbt_date, nbt_content, "
                . "user_tbl_usr_id, notice_board_categories_tbl_nbtc_id, nbt_archive, nbt_authorised, "
                . "documents_tbl_doc_id) "
                . "VALUES ("
                . "'".addslashes($noticeBoardObject->title)."', "
                . "'$noticeBoardObject->date', "
                . "'".addslashes($noticeBoardObject->content)."', "
                . "'".$noticeBoardObject->userID."', "
                . "'".$noticeBoardObject->categoryID."', "
                . "'".$noticeBoardObject->archive."', "
                . "'".$noticeBoardObject->authorised."', "
                . "'".$noticeBoardObject->documentID."')";
        
        if (!mysqli_query(Database::$connection, $result))
        {
            return mysqli_error(Database::$connection);
        } else {
            return true;
        }
    }
    
    /**
     * 
     * @param NoticeBoardObject $noticeBoardObject
     * @return Boolean
     */
    public function setNewNoticeArticleNoDocID($noticeBoardObject) {
        
        $result = "INSERT INTO notice_board_tbl (nbt_title, nbt_date, nbt_content, "
                . "user_tbl_usr_id, notice_board_categories_tbl_nbtc_id, nbt_archive, nbt_authorised) "
                . "VALUES ("
                . "'".addslashes($noticeBoardObject->title)."', "
                . "'$noticeBoardObject->date', "
                . "'".addslashes($noticeBoardObject->content)."', "
                . "'".$noticeBoardObject->userID."', "
                . "'".$noticeBoardObject->categoryID."', "
                . "'".$noticeBoardObject->archive."', "
                . "'".$noticeBoardObject->authorised."')";
        
        if (!mysqli_query(Database::$connection, $result))
        {
            debugWriter("noticeBoard.txt", mysqli_error(Database::$connection));
            return null;
        } else {
            return mysqli_insert_id(Database::$connection);
        }
    }
    
   
    /**
     * 
     * @param type $articleID
     * @return \NoticeBoard
     */
    public function getArticleByID($articleID) {
        $noticeBoardSQLConstructor = new NoticeBoardSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM notice_board_tbl WHERE nbt_id = '".$articleID."'");
        
        $row = mysqli_fetch_assoc($result);
        $article = $noticeBoardSQLConstructor->createNoticeBoard($row);
        
        
        if (!isset($article)) {
            return null;
        } else {
            return $article;
        }
    }
    
    /**
     * 
     * @param type $month
     * @param type $year
     * @return \NoticeBoard
     */
    public function getArticleByMonthAndYear($month, $year) {
        $noticeBoardSQLConstructor = new NoticeBoardSQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM notice_board_tbl "
                . "WHERE MONTH(nbt_date) = '".$month."' "
                . "AND YEAR(nbt_date) = '".$year."' AND nbt_archive = '0' "
                . "AND nbt_authorised = '1' ORDER BY nbt_date DESC ");
        
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['nbt_id']] = $noticeBoardSQLConstructor->createNoticeBoard($row);
        }
        
        if (!isset($array)) {
            return null;
        } else {
            return $array;
        }
    }
    
    /**
     * getArticleByMonthAndYearAndCategory
     * @param String $month
     * @param String $year
     * @param INT $category
     * @return  Array NoticeBoard Objects
     */
    public function getArticleByMonthAndYearAndCategory($month, $year, $category) {
        $noticeBoardSQLConstructor = new NoticeBoardSQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM notice_board_tbl "
                . "WHERE MONTH(nbt_date) = '".$month."' "
                . "AND YEAR(nbt_date) = '".$year."' "
                . "AND nbt_archive = '0' AND nbt_authorised = '1' "
                . "AND notice_board_categories_tbl_nbtc_id='".$category."' ORDER BY nbt_date DESC ");
        
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['nbt_id']] = $noticeBoardSQLConstructor->createNoticeBoard($row);
        }
        
        if (!isset($array)) {
            return null;
        } else {
            return $array;
        }
    }
    
    /**
     * 
     * @param type $noticeBoardObject
     * @return boolean
     */
    public function updateNoticeArticle($noticeBoardObject) {
        $result = "UPDATE notice_board_tbl SET "
                . "nbt_title = '".addslashes($noticeBoardObject->title)."', "
                . "nbt_date = '$noticeBoardObject->date', "
                . "nbt_content = '".addslashes($noticeBoardObject->content)."', "
                . "notice_board_categories_tbl_nbtc_id = '$noticeBoardObject->categoryID' "
                . "WHERE nbt_id = '$noticeBoardObject->ID'";
        
        if (!mysqli_query(Database::$connection, $result))
        {
            return mysqli_error(Database::$connection);
        } else {
            return true;
        }
    }
    
    
    /**
     * 
     * @return boolean
     */
    public function getHistoryArrayMonthsYears() {
        $result = mysqli_query(Database::$connection, "SELECT DISTINCT YEAR(nbt_date) as 'year' "
                . "FROM notice_board_tbl "
                . "WHERE nbt_archive = '0' AND nbt_authorised = '1' "
                . "ORDER BY nbt_date DESC");
        $array = [];
        
        while($row = mysqli_fetch_assoc($result)) {
            
            $year = [];
            $months = mysqli_query(Database::$connection, "SELECT DISTINCT MONTH(nbt_date) as 'month' "
                    . "FROM notice_board_tbl WHERE nbt_archive = '0' AND nbt_authorised = '1' "
                    . "AND YEAR(nbt_date) = '".$row['year']."' "
                    . "ORDER BY nbt_date DESC");
            
            while($row1 = mysqli_fetch_assoc($months)) {
                array_push($year, $row1['month']);
            }
            $array[$row['year']] = $year;
        }
        
        if (!isset($array)) {
            return null;
        } else {
            return $array;
        }
    }
    
    /**
     * Counts the number of times a category is used.
     * @param INT $categoryID
     * @return INT
     */
    public function getCountOfCategoryUsage($categoryID) {
        $result = mysqli_query(Database::$connection, "SELECT count(*) FROM notice_board_tbl "
                . "WHERE notice_board_categories_tbl_nbtc_id = '".$categoryID."'");
        
        $row = mysqli_fetch_assoc($result);
        return $row['count(*)'];
    }
    
    
    /**
     * This counts the number of articles a user has awaiting authorisation from above.
     * @param INT $userID
     * @return INT
     */
    public function countArticlesAwaitingAuthorisation($userID) {
        
        $result = mysqli_query(Database::$connection, "SELECT count(*) FROM notice_board_tbl 
            WHERE user_tbl_usr_id='".$userID."' AND nbt_archive = '0' AND nbt_authorised='0'");
        
        $row = mysqli_fetch_assoc($result);
        
        return $row['count(*)'];
    }
    
    
    /**
     * This counts the number of all articles awaiting authorisation from above.
     * @return INT
     */
    public function countAllArticlesAwaitingAuthorisation() {
        
        $result = mysqli_query(Database::$connection, "SELECT count(*) FROM notice_board_tbl 
            WHERE nbt_authorised='0' AND nbt_archive ='0'");
        
        $row = mysqli_fetch_assoc($result);
        
        return $row['count(*)'];
    }
    
    /**
     * 
     * @return \NoticeBoard
     */
    public function getAllUnauthorisedNoticesDescendingOrder() {
        $noticeBoardSQLConstructor = new NoticeBoardSQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM notice_board_tbl WHERE nbt_archive = '0' "
                . "AND nbt_authorised = '0' ORDER BY nbt_date DESC");
        
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['nbt_id']] = $noticeBoardSQLConstructor->createNoticeBoard($row);
        }
        
        if (!isset($array)) {
            return null;
        } else {
            return $array;
        }
    }
    
    
    /**
     * Update the notice board article to authorised.
     * @param INT $noticeBoardID
     * @return boolean
     */
    public function updateArticleToAuthorised($noticeBoardID) {
        $result = "UPDATE notice_board_tbl SET nbt_authorised = '1' WHERE "
                . "nbt_id = '".$noticeBoardID."'";
        
        if (!mysqli_query(Database::$connection, $result)) {
            return null;
        } else {
            return true;
        }
    }
}