<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/SpecialSurveyDue.php");
include_once("Models/SQLConstructors/SpecialSurveyDueDateSQLConstructor.php");
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of SpecialSurveyDueDateModel
 *
 * @author Archie
 */
class SpecialSurveyDueDateModel {
    
    
    /**
     * getSpecialSurveyDueDateByInstallationID
     * @param type $installationID
     * @return type
     */        
    public function getSpecialSurveyDueDateByInstallationID($installationID) {
        $specialSurveyDueDateSQLConstructor = new SpecialSurveyDueDateSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM survey_due_date_tbl WHERE "
                . "installations_tbl_inst_id = '$installationID'  AND sddt_special_survey_date IS NOT NULL");
        
        $row = mysqli_fetch_assoc($result);
        $specialSurvey = $specialSurveyDueDateSQLConstructor->createSpecialSurveyDueDate($row);
        
        if (isset($specialSurvey)) {
            return $specialSurvey;
        } else {
            return null;
        }
        
    }
    
    
    /**
     * getSpecialSurveyDueDates
     * @param type $dateOne
     * @param type $dateTwo
     * @return type
     */
    public function getSpecialSurveyDueDates($dateOne, $dateTwo) {
        $specialSurveyDueDateSQLConstructor = new SpecialSurveyDueDateSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM survey_due_date_tbl WHERE 
                sddt_special_survey_date IS NOT NULL AND 
                sddt_special_survey_date BETWEEN '$dateOne' AND '$dateTwo'
                ORDER BY sddt_special_survey_date ASC");
        
        while($row = mysqli_fetch_assoc($result)) {
            $specialSurvey[] = $specialSurveyDueDateSQLConstructor->createSpecialSurveyDueDate($row);
        }
        if (isset($specialSurvey)) {
            return $specialSurvey;
        } else {
            return null;
        }
    }
}
