<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
//include_once ("Models/DB.php");
include_once("Models/Database.php");
include_once("Models/DatabaseHistory.php");
/**
 * Description of InsertSuppliersFromCSVModel
 *
 * @author Archie
 */
class InsertSuppliersFromCSVModel {
    
    
    // SET GLOBAL VARIABLES
    private $dbColumnName;
    private $newColumnNameArray;
    private $replace;
    
    
    /**
     * createTemporaryTable
     * @param Array $columnNameArray
     */
    private function createTemporaryTable($columnNameArray, $type) {

        if (($type != 'UNSPECIFIED') && ($type != 'inst_type')) {
            $typeString = " inst_type VARCHAR(10), ";
        } else {
            $typeString = "";
        }

        /*
         * Create a temporary table to contain the CSV data in the same order as the CSV.
         */
        $create_table_query = "CREATE TEMPORARY TABLE temporary_suppliers_table (temp_id SERIAL, ";
        // Go through the column names array
        foreach ($columnNameArray as $p => $q) {
            $p = str_replace($this->replace, "_", $p); // Use the REGEX to replace bad characters with underscore

            $create_table_query .= " " . addslashes($p) . " VARCHAR(45), "; // This adds to the string that creates the table
            $this->dbColumnName .= " " . addslashes($p) . ", "; // This does similar for a select

            $key = str_replace($this->replace, "_", $p); // This shouldnt technically be needed but the code complained without it.
            $this->newColumnNameArray[$key] = $q; // create a new array with the new CSV column names
        }
        $create_table_query .= "$typeString PRIMARY KEY(temp_id))"; // set the primary key of the table


        /*
         * mysqli_query(Database::$connection, ) runs a sql query, putting ! in front of it says "if the query fails"
         */
        if (!mysqli_query(Database::$connection, $create_table_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't create table : " . $create_table_query . " \r\n" . mysqli_error(Database::$connection) . "");
        }
    }
    
    /**
     * createManualCheckTable
     * @param Array $columnNameArray
     */
    private function createManualCheckTable($columnNameArray, $type) {

        if (($type != 'UNSPECIFIED') && ($type != 'inst_type')) {
            $typeString = " inst_type VARCHAR(10), ";
        } else {
            $typeString = "";
        }

        /*
         * Create a temporary table to contain the CSV data in the same order as the CSV.
         */
        $create_manual_table_query = "CREATE TABLE IF NOT EXISTS manual_check_tbl (temp_id SERIAL, ";
        // Go through the column names array
        foreach ($columnNameArray as $p => $q) {
            $p = str_replace($this->replace, "_", $p); // Use the REGEX to replace bad characters with underscore

            if (($q != "") && ($q != "@dummy")) {
                $create_manual_table_query .= " " . addslashes($q) . " VARCHAR(45), "; // This adds to the string that creates the table
            }
        }
        $create_manual_table_query .= " $typeString matching_customer_id INT(11) UNSIGNED, matching_installation_id INT(11) UNSIGNED, PRIMARY KEY(temp_id))"; // set the primary key of the table

        /*
         * mysqli_query(Database::$connection, ) runs a sql query, putting ! in front of it says "if the query fails"
         */
        if (!mysqli_query(Database::$connection, $create_manual_table_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't create table : " . $create_manual_table_query . " \r\n" . mysqli_error(Database::$connection) . "");
        }
    }
    
    /**
     * loadFile
     * @param STRING $fileName
     * @param BOOLEAN $headers
     * @param STRING $type
     * @param STRING $dbColumnName
     */
    private function loadFile($fileName, $headers, $type) {
        // Add the CSV data to the temp table.
        // Ignore the first line in the file because these are column headers.
        if ($headers == 1) {
            $ignoreFirstLine = "IGNORE 1 LINES ";
        } else {
            $ignoreFirstLine = "";
        }

        // Location type might be in the csv, if its not then we manually set it for the whole file and that is
        // decided for the SQL statement here.
        if (($type != 'UNSPECIFIED') && ($type != 'inst_type')) {
            $typeString = "SET inst_type = '$type'";
        } else {
            $typeString = "";
        }

        $new_query_string = substr($this->dbColumnName, 0, -2); // removes the last character from the string (its a comma that shouldnt be there)
        // Load the data from the csv into the temporary table, using the column names generated on the verify page.
        $insert_temp_query = "LOAD DATA LOCAL INFILE '" . $fileName . "'
                INTO TABLE temporary_suppliers_table
                CHARACTER SET 'latin1'
                COLUMNS TERMINATED BY ','
                ENCLOSED BY '\"' LINES TERMINATED BY '\r\n'
                $ignoreFirstLine ($new_query_string) $typeString";


        // DATABASE query generator. This say, if mysqli_query fails then write the first line. otherwise write the second.
        if (!mysqli_query(Database::$connection, $insert_temp_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't insert records into temporary table : " . $new_query_string . "*** " . mysqli_error(Database::$connection));
        } else {
           // debugWriter("loadInstallationDataLogs.txt", $fileName . " Inserted");
        }
    }

    /**
     * cleanTempData
     * @param type $fileName
     * @param type $columnNameArray
     * @param type $headers
     * @param type $type
     * @param type $dbColumnName
     * @param type $newColumnNameArray
     */
    private function cleanTempData() {
        $cleanQuery = "UPDATE temporary_suppliers_table SET ";
        // clean out leading and trailing whitespaces 
        foreach ($this->newColumnNameArray as $key => $name) {
            $key = str_replace($this->replace, "_", $key);
            $cleanQuery .= " " . $key . "= TRIM(" . addslashes($key) . "), ";
        }
        $cleanedQuery = substr($cleanQuery, 0, -2);
        mysqli_query(Database::$connection, $cleanedQuery);



        // Do a quick count of the new records, This is currently used for debug, but may stay in the code.
        $quickCount = mysqli_query(Database::$connection, "SELECT count(*) FROM temporary_suppliers_table");
        $countQuick = mysqli_fetch_assoc($quickCount);
        print "<p>Temp table Count: " . $countQuick['count(*)'] . ". Before duplicates are removed</p>";
    }

    /**
     * createCustomers
     * @param type $columnNameArray
     */
    private function createCustomers() {
        // customer name- its important that it is neither a duplicate or blank.
        // The database has a unique attribute attached to customer name.
        // Check all records inserted into the temporary table, if they are blank, assign a unique ID.
        // find everywhere there is no customer name created
        // array_search looks for the KEY in the array where the VALUE is equal to the array_search criteria
 
        $customerName = "";
        
       
        if (array_search('cust_name', $this->newColumnNameArray) != "") {
            $customerName = ", " . array_search('cust_name', $this->newColumnNameArray) . " AS cust_name ";
        }
       
        $emptyCustomer = "SELECT temp_id " . $customerName . "  "
                . "FROM temporary_suppliers_table WHERE " . array_search('cust_name', $this->newColumnNameArray) . " = ''";

        $emptyCustomerQuery = mysqli_query(Database::$connection, $emptyCustomer);

        
        while ($emptyCustomerQueryRow = mysqli_fetch_assoc($emptyCustomerQuery)) {
            
                // It does not exist in the DB so give it a unique name
                $updateEmptyName = "UPDATE temporary_suppliers_table SET " . array_search('cust_name', $this->newColumnNameArray) . " = "
                        . "'" . uniqid("UNKNOWN.") . "' WHERE temp_id = '" . $emptyCustomerQueryRow['temp_id'] . "'";

                if (!mysqli_query(Database::$connection, $updateEmptyName)) {
                    debugWriter("loadInstallationDataLogs.txt", "Cannot update empty customer name " . mysqli_error(Database::$connection));
                }
            }
        



        // start SQL statement to get all customer details.
        $customerQuery = "SELECT " . array_search('cust_name', $this->newColumnNameArray) . " AS cust_name ";
        $custOfficeName = "";
        $custAddress = "";
        $custCity = "";
        $custState = "";
        $custCountry = "";
        $custPostcode = "";
        $custDefaultAddress = "";

        foreach ($this->newColumnNameArray as $key => $name) {
            $key = str_replace($this->replace, "_", $key);
            if ($name == 'cust_telephone') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cust_fax') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cust_website') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cadt_office_name') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
                $custOfficeName = ", cadt_office_name";
            }
            if ($name == 'cadt_address') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
                $custAddress = ", cadt_address";
            }
            if ($name == 'cadt_city') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
                $custCity = ", cadt_city";
            }
            if ($name == 'cadt_state') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
                $custState = ", cadt_state";
            }
            if ($name == 'cadt_country') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
                $custCountry = ", cadt_country";
            }
            if ($name == 'cadt_postcode') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
                $custPostcode = ", cadt_postcode";
            }
            if ($name == 'cust_type') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'cadt_default_address') {
                $customerQuery .= ", " . addslashes($key) . " AS '" . $name . "' ";
                $custDefaultAddress = ", cadt_default_address";
            }
        }

        $customerQuery .= " FROM temporary_suppliers_table GROUP BY cust_name $custOfficeName $custAddress $custCity $custState $custCountry $custPostcode";
        
        //debugWriter("debug.txt", "Customer Query ".$customerQuery);
        $customerInsert = mysqli_query(Database::$connection, $customerQuery);

        $newCustomerCount = 0;
        $newAddressCount = 0;

        while ($customerQueryRow = mysqli_fetch_assoc($customerInsert)) {
            //debugWriter("debug.txt", "customer Name ".$customerQueryRow['cust_name']);
            // Check the customer_tbl for entries that match
            $checkCustomerTablequery = mysqli_query(Database::$connection, "SELECT count(cust_name) FROM customer_tbl WHERE "
                    . "cust_name = '" . addslashes($customerQueryRow['cust_name']) . "'");

            $checkCustomerTable = mysqli_fetch_assoc($checkCustomerTablequery);
            //debugWriter("debug.txt", "Line 282 checkCustomerTableQuery".mysqli_error(Database::$connection));
            $customerID = "";

            // If there is no match because the number of entries is less than 1, then proceed.
            if ($checkCustomerTable['count(cust_name)'] < 1) {
                //debugWriter("debug.txt", "new entry for customer");
                /*
                 * Create a new entry.
                 */
                $insertCustomer = "INSERT INTO customer_tbl (cust_name, cust_telephone, cust_fax,"
                        . "cust_website, cust_account_manager, cust_tbh_id, "
                        . "cust_tbh_account_code, cust_ld_id, cust_ld_account_code, cust_header_notes) VALUES ("
                        . "'" . addslashes((isset($customerQueryRow['cust_name']) ? $customerQueryRow['cust_name'] : "")) . "',"
                        . "'" . addslashes((isset($customerQueryRow['cust_telephone']) ? $customerQueryRow['cust_telephone'] : "")) . "', "
                        . "'" . addslashes((isset($customerQueryRow['cust_fax']) ? $customerQueryRow['cust_fax'] : "")) . "', "
                        . "'" . addslashes((isset($customerQueryRow['cust_website']) ? $customerQueryRow['cust_website'] : "")) . "', "
                        . "NULL, "
                        . "'0', "
                        . "'0', "
                        . "'0', "
                        . "'0', "
                        . "'')";

                if (!mysqli_query(Database::$connection, $insertCustomer)) {
                    debugWriter("loadInstallationDataLogs.txt", "Cannot Insert Customer : " . mysqli_error(Database::$connection));
                } else {
                    $newCustomerCount++;
                }
                $customerID = mysqli_insert_id(Database::$connection);

                $customerType = '3';
                if (isset($customerQueryRow['cust_type']) && ($customerQueryRow['cust_type'] != "")) {

                    $selectStatus = mysqli_query(Database::$connection, "SELECT cslt_id FROM customer_status_lookup_tbl "
                            . "WHERE cslt_status = '" . $customerQueryRow['cust_type'] . "'");
                    $statusRow = mysqli_fetch_assoc($selectStatus);
                    $customerType = $statusRow['cslt_id'];
                }
                // Status is held in a separate table because there can be 1 or many
                $insertCustomerStatus = "INSERT INTO customer_status_tbl "
                        . "(customer_tbl_cust_id, customer_status_lookup_tbl_cslt_id) "
                        . "VALUES "
                        . "('" . $customerID . "', "
                        . "'" . $customerType . "')";
                if (!mysqli_query(Database::$connection, $insertCustomerStatus)) {
                    debugWriter("loadSuppliersDataLogs.txt", "Cannot Insert Customer Status : " . mysqli_error(Database::$connection));
                }


                /*
                 * Create an entry into the address table.
                 */

                $insertAddress = "INSERT INTO customer_address_tbl (cadt_office_name, cadt_address, "
                        . "cadt_city, cadt_state, cadt_country, cadt_postcode, cadt_address_verified, "
                        . "customer_tbl_cust_id, cadt_archived, cadt_default_address) "
                        . "VALUES ("
                        . "'" . addslashes((isset($customerQueryRow['cadt_office_name']) ? $customerQueryRow['cadt_office_name'] : "Office")) . "', "
                        . "'" . addslashes((isset($customerQueryRow['cadt_address']) ? $customerQueryRow['cadt_address'] : "")) . "', "
                        . "'" . addslashes((isset($customerQueryRow['cadt_city']) ? $customerQueryRow['cadt_city'] : "")) . "', "
                        . "'" . addslashes((isset($customerQueryRow['cadt_state']) ? $customerQueryRow['cadt_state'] : "")) . "', "
                        . "'" . addslashes((isset($customerQueryRow['cadt_country']) ? $customerQueryRow['cadt_country'] : "")) . "', "
                        . "'" . addslashes((isset($customerQueryRow['cadt_postcode']) ? $customerQueryRow['cadt_postcode'] : "")) . "', "
                        . "'0', "
                        . "'" . $customerID . "', "
                        . "'0', "
                        . "'" . addslashes((isset($customerQueryRow['cadt_default_address']) && 
                        (($customerQueryRow['cadt_default_address'] == "TRUE") 
                        || ($customerQueryRow['cadt_default_address'] == "1"))  ? "1" : "0")) . "')";

                //debugWriter("debug.txt", $insertAddress);
                if (!mysqli_query(Database::$connection, $insertAddress)) {
                    debugWriter("loadSuppliersDataLogs.txt", "Cannot Insert Customer Address : " . mysqli_error(Database::$connection));
                } else {
                    //debugWriter("debug.txt", "INSERT ID ".mysqli_insert_id(Database::$connection));
                    $newAddressCount++;
                }
            } else {
               // debugWriter("debug.txt", "existing entry for customer");
                // Get the customer ID.
                $getCustomerID = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl WHERE "
                        . "cust_name = '" . addslashes($customerQueryRow['cust_name']) . "'");

                $getCustomerIDRow = mysqli_fetch_assoc($getCustomerID);
                $customerID = $getCustomerIDRow['cust_id'];
                //debugWriter("debug.txt", "customer id: ".$customerID);

                // Get a count of all addresses currently attached to that customer ID
                $customerAddress = (isset($customerQueryRow['cadt_address']) ? $customerQueryRow['cadt_address'] : ""); // This is a Ternary Logic statement it reads as (statement ? true : false). if the statement is true do true, else do false.
                $customerCity = (isset($customerQueryRow['cadt_city']) ? $customerQueryRow['cadt_city'] : "");
                $customerState = (isset($customerQueryRow['cadt_state']) ? $customerQueryRow['cadt_state'] : "");
                $customerCountry = (isset($customerQueryRow['cadt_country']) ? $customerQueryRow['cadt_country'] : "");
                $customerPostcode = (isset($customerQueryRow['cadt_postcode']) ? $customerQueryRow['cadt_postcode'] : "");

                //debugWriter("debug.txt", $customerAddress." ".$customerCity." ".$customerState." ".$customerCountry." ".$customerPostcode);
                // So the customer exists, but the address might be new. Check for that.
                $checkAddress = mysqli_query(Database::$connection, "SELECT count(cadt_id) FROM customer_address_tbl "
                        . "WHERE customer_tbl_cust_id = '" . $customerID . "' AND "
                        . "cadt_address = '" . addslashes($customerAddress) . "' AND "
                        . "cadt_city = '" . addslashes($customerCity) . "' AND "
                        . "cadt_state = '" . addslashes($customerState) . "' AND "
                        . "cadt_country = '" . addslashes($customerCountry) . "' AND "
                        . "cadt_postcode = '" . addslashes($customerPostcode) . "'");

                $checkAddressRow = mysqli_fetch_assoc($checkAddress);


                if ($checkAddressRow['count(cadt_id)'] < 1) {
                  //  debugWriter("debug.txt", "address not found");
                    $insertAddress = "INSERT INTO customer_address_tbl (cadt_office_name, cadt_address, "
                            . "cadt_city, cadt_state, cadt_country, cadt_postcode, cadt_address_verified, "
                            . "customer_tbl_cust_id, cadt_archived, cadt_default_address) "
                            . "VALUES ("
                            . "'" . addslashes((isset($customerQueryRow['cadt_office_name']) ? $customerQueryRow['cadt_office_name'] : "Office")) . "', "
                            . "'" . addslashes((isset($customerQueryRow['cadt_address']) ? $customerQueryRow['cadt_address'] : "")) . "', "
                            . "'" . addslashes((isset($customerQueryRow['cadt_city']) ? $customerQueryRow['cadt_city'] : "")) . "', "
                            . "'" . addslashes((isset($customerQueryRow['cadt_state']) ? $customerQueryRow['cadt_state'] : "")) . "', "
                            . "'" . addslashes((isset($customerQueryRow['cadt_country']) ? $customerQueryRow['cadt_country'] : "")) . "', "
                            . "'" . addslashes((isset($customerQueryRow['cadt_postcode']) ? $customerQueryRow['cadt_postcode'] : "")) . "', "
                            . "'0', "
                            . "'" . $customerID . "', "
                            . "'0', "
                            . "'" . addslashes((isset($customerQueryRow['cadt_default_address']) && 
                                (($customerQueryRow['cadt_default_address'] == "TRUE") 
                                || ($customerQueryRow['cadt_default_address'] == "1"))  ? "1" : "0")) . "')";

                    if (!mysqli_query(Database::$connection, $insertAddress)) {
                        debugWriter("loadSuppliersDataLogs.txt", "Cannot Insert Customer Address : " . mysqli_error(Database::$connection));
                    } else {
                        $newAddressCount++;
                    }
                }
                
                // now check if the status matches and if not add a new status.
                $customerType = "3";
                /*if (isset($customerQueryRow['cust_type'])) {
                    $selectStatus = mysqli_query(Database::$connection, "SELECT cslt_id FROM customer_status_lookup_tbl "
                    . "WHERE cslt_status = '" . $customerQueryRow['cust_type'] . "'");
                    $statusRow = mysqli_fetch_assoc($selectStatus);
                    $customerType = $statusRow['cslt_id'];
                    debugWriter("debug.txt", $customerType);
                }*/

                $checkCustomerStatusInfo = mysqli_query(Database::$connection, "SELECT count(cst_id) FROM customer_status_tbl "
                . "WHERE customer_tbl_cust_id = '" . $customerID . "' "
                . "AND customer_status_lookup_tbl_cslt_id = '" . $customerType . "'");
                
                //debugWriter("debug.txt", "SELECT count(cst_id) FROM customer_status_tbl WHERE customer_tbl_cust_id = '" . $customerID . "'AND customer_status_lookup_tbl_cslt_id = '" . $customerType . "'");

                $checkCustomerStatusInfoRow = mysqli_fetch_assoc($checkCustomerStatusInfo); 

                if ($checkCustomerStatusInfoRow['count(cst_id)'] == 0) {
                    //debugWriter("debug.txt", "status count " . $checkCustomerStatusInfoRow['count(cst_id)']);
               
                    $insertNewStatus = "INSERT INTO customer_status_tbl (customer_tbl_cust_id, "
                            . "customer_status_lookup_tbl_cslt_id) "
                            . "VALUES ('".$customerID."', '".$customerType."')";
                    //debugWriter("debug.txt", $insertNewStatus);
                    if (!mysqli_query(Database::$connection, $insertNewStatus)) {
                        debugWriter("debug.txt", "COULD NOT ADD NEW CUSTOMER STATUS TBL ENTRY". mysqli_error(Database::$connection));
                    }

                }
            }
        }

        print "<p>Added " . $newCustomerCount . " new customers</p>";
        print "<p>Added " . $newAddressCount . " new addresses</p>";
    }
    
    
    private function createApprovalData() {
        //debugWriter("debug.txt", "inside approval data");
        // Create the select statement
        $suppliersString = "SELECT " . array_search('cust_name', $this->newColumnNameArray) . " AS cust_name ";
        /*print "<pre>";
        print_r($this->newColumnNameArray);
        print "</pre>";*/

        foreach ($this->newColumnNameArray as $key => $name) {
            /*print $key."---";
            print $name. "<br/>";*/
            //$key = str_replace($this->replace, "_", $key);
            // Find all the relevant columns (up to three contacts is possible in uploadInstallationsTest.php)
            // the contact details have a _1, _2 or _3 after each name which is why I use substr()
            if ($name == 'sat_approved_as_supplier') {
                $suppliersString .= ", " . addslashes($key) . " AS '" . $name . "' "; // AS works to create an alias for the column name, to make it easier to handle.
            }
            if ($name == 'sat_approved_as_trader') {
                $suppliersString .= ", " . addslashes($key) . " AS '" . $name . "' "; // AS works to create an alias for the column name, to make it easier to handle.
            }
            if ($name == 'sat_approved_as_agent') {
                $suppliersString .= ", " . addslashes($key) . " AS '" . $name . "' "; // AS works to create an alias for the column name, to make it easier to handle.
            }
            if ($name == 'supplier_approval_status_lookup_tbl_saslt_id') {
                $suppliersString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'sat_date_approval_status_confirmed') {
                $suppliersString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'sat_status_review_date') {
                $suppliersString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if ($name == 'sat_reason_for_status_change') {
                $suppliersString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            
        }
        $suppliersString .= " FROM temporary_suppliers_table";
        
        //print $suppliersString;

        $suppliersQuery = mysqli_query(Database::$connection, $suppliersString); // get the records from the temporary table.
        // Start count of customers added
        $approvalsCount = 0;

        // Loop through the resultant records and perform the same actions on each row.
        while ($suppliersQueryRow = mysqli_fetch_assoc($suppliersQuery)) {
            //print "<pre>";
            //print "Suppliers query row<br/>";
            //print_r($suppliersQueryRow);
           // print "</pre>";
            $columnsString = "";
            $valuesString = "";
            // Create an array for each customer name and split the possible contacts into the array
          //  $supplier = []; // empty array.
            
            $customerID = ""; // empty customer ID
            foreach ($suppliersQueryRow as $col => $val) {
                if ($col == "cust_name") {
                    // Get the customers ID
                    $getCustomerIDQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl WHERE "
                            . "cust_name = '" . addslashes($val) . "'");

                    $customerIDRow = mysqli_fetch_assoc($getCustomerIDQuery);
                    $customerID = $customerIDRow['cust_id'];
                } else {
                    $columnsString .= "$col, ";
                    
                    if ($val == '-' || $val == '#NAME?') { // if the data is useless then reset $val to be an empty string
                            $val = "";
                        }

                        if ($val != "") {
                            $val = addslashes($val);
                        }
                        if ($col == 'supplier_approval_status_lookup_tbl_saslt_id') {
                           // print "LOOKUP ".substr($val, 0, 1);
                            $appLookup = mysqli_query(Database::$connection, "SELECT * FROM supplier_approval_status_lookup_tbl WHERE "
                                    . "saslt_approval_status LIKE '".substr($val, 0, 1)."%'");
                            $appLookupRow = mysqli_fetch_assoc($appLookup);
                            $val = $appLookupRow['saslt_id'];
                        }
                        if (($col == 'sat_approved_as_supplier') || 
                                ($col == 'sat_approved_as_trader') ||
                                ($col == 'sat_approved_as_agent')) {
                            if ($val != "0") {
                                $val = "1";
                            }
                        }
                        if (($col == 'sat_date_approval_status_confirmed') 
                                || ($col == 'sat_status_review_date')) {
                            
                            $val = date("Y-m-d",strtotime($val));
                        }
                        $valuesString .= "'" . $val . "', ";
                       // $checkSuppliers .= "AND $col = '$val' ";
                }
            }
            $sqlString = "INSERT INTO supplier_approvals_tbl (".substr($columnsString,0,-2)." "
                                    . ", customer_tbl_cust_id) "
                                    . "VALUES (".substr($valuesString,0, -2)." "
                                    . ", '" . $customerID . "')";
            //print $sqlString."<br/>";
            if (!mysqli_query(Database::$connection, $sqlString)) {
                debugWriter("debug.txt", "INSERT supplier_approvals_tbl FAILED : " . $sqlString . " " . mysqli_error(Database::$connection));
            } else {
                $approvalsCount++;
            }
        }
        print "<p>Added " . $approvalsCount . " new suppliers approvals<p/>";
    }

    /**
     * createContacts
     */
    private function createContacts() {
        // Create the select statement
        $contactsString = "SELECT " . array_search('cust_name', $this->newColumnNameArray) . " AS cust_name ";


        foreach ($this->newColumnNameArray as $key => $name) {
            $key = str_replace($this->replace, "_", $key);
            // Find all the relevant columns (up to three contacts is possible in uploadInstallationsTest.php)
            // the contact details have a _1, _2 or _3 after each name which is why I use substr()
            if (substr($name, 0, -2) == 'ict_salutation') {
                $contactsString .= ", " . addslashes($key) . " AS '" . $name . "' "; // AS works to create an alias for the column name, to make it easier to handle.
            }
            if (substr($name, 0, -2) == 'ict_first_name') {
                $contactsString .= ", " . addslashes($key) . " AS '" . $name . "' "; // AS works to create an alias for the column name, to make it easier to handle.
            }
            if (substr($name, 0, -2) == 'ict_surname') {
                $contactsString .= ", " . addslashes($key) . " AS '" . $name . "' "; // AS works to create an alias for the column name, to make it easier to handle.
            }
            if (substr($name, 0, -2) == 'ict_job_title') {
                $contactsString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if (substr($name, 0, -2) == 'ict_email') {
                $contactsString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if (substr($name, 0, -2) == 'ict_number') {
                $contactsString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if (substr($name, 0, -2) == 'ict_mobile_number') {
                $contactsString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
            if (substr($name, 0, -2) == 'ict_info_source') {
                $contactsString .= ", " . addslashes($key) . " AS '" . $name . "' ";
            }
        }
        $contactsString .= " FROM temporary_suppliers_table";

        $contactsQuery = mysqli_query(Database::$connection, $contactsString); // get the records from the temporary table.
        // Start count of customers added
        $contactCount = 0;

        // Loop through the resultant records and perform the same actions on each row.
        while ($contactsQueryRow = mysqli_fetch_assoc($contactsQuery)) {
            // Create an array for each customer name and split the possible contacts into the array
            $contact = []; // empty array.
            $customerID = ""; // empty customer ID
            foreach ($contactsQueryRow as $k => $r) {
                if ($k != "cust_name") {
                    $id = substr($k, -1);
                    $contact[$contactsQueryRow['cust_name']][$id][substr($k, 0, -2)] = addslashes($r);
                } else {
                    // Get the customers ID
                    $getCustomerIDQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl WHERE "
                            . "cust_name = '" . addslashes($r) . "'");

                    $customerIDRow = mysqli_fetch_assoc($getCustomerIDQuery);
                    $customerID = $customerIDRow['cust_id'];
                }
            }

            if (isset($contact[$contactsQueryRow['cust_name']])) {
                foreach ($contact[$contactsQueryRow['cust_name']] as $id => $columnName) {

                    $columnsString = "";
                    $valuesString = "";
                    $emptyRecord = 0; // this counts if the record has all empty columns and thus should not be inserted.
                    // check for existing customer contact records that match the new one to be inserted.
                    // Also check the history date to see when it was added.
                    $checkContacts = "SELECT count(*) FROM customer_contacts_tbl "
                            . "WHERE customer_tbl_cust_id = '" . $customerID . "' ";

                    foreach ($columnName as $col => $val) {
                        $columnsString .= "$col, ";
                        if ($val == '-' || $val == '#NAME?') { // if the data is useless then reset $val to be an empty string
                            $val = "";
                        }

                        if ($val != "") {
                            $val = addslashes($val);
                            $emptyRecord++;
                        }
                        $valuesString .= "'" . $val . "', ";
                        $checkContacts .= "AND $col = '$val' ";
                    }

                    if ($emptyRecord > 0) {

                        $checkContactsResult = mysqli_query(Database::$connection, $checkContacts);
                        if (!mysqli_query(Database::$connection, $checkContacts)) {
                            debugWriter("debug.txt", "INSERT FROM CSV FAILED : " . $checkContacts . " " . mysqli_error(Database::$connection));
                        }
                        $rowCC = mysqli_fetch_assoc($checkContactsResult);

                        if ($rowCC['count(*)'] < 1) {
                            // create an insert statement.
                            $sqlString = "INSERT INTO customer_contacts_tbl ($columnsString "
                                    . "ict_inactive, ict_contact_validated, customer_tbl_cust_id, customer_address_tbl_cadt_id) "
                                    . "VALUES ($valuesString "
                                    . "'0', '0', '" . $customerID . "', NULL)";
                            if (!mysqli_query(Database::$connection, $sqlString)) {
                                debugWriter("debug.txt", "INSERT customer_contacts_tbl FAILED : " . $sqlString . " " . mysqli_error(Database::$connection));
                            } else {
                                $contactCount++;
                            }

                            // If the record contains something useful then we add it to the database.

                            /*$customerContactsID = mysqli_insert_id(Database::$connection);
                            // Insert a record into the history table as well.
                            $history = "INSERT INTO customer_contacts_history_tbl "
                                    . "(ict_id, ccht_action, ccht_date, user_tbl_usr_id, ccht_origin) "
                                    . "VALUES ('" . $customerContactsID . "','Created', NOW(), '" . $_SESSION['user_id'] . "', 'CSV')";

                            if (!mysqli_query(DatabaseHistory::$connection, $history)) {
                                debugWriter("debug.txt", "INSERT HISTORY FAILED : " . $history . " " . mysqli_error(DatabaseHistory::$connection));
                            } else {
                                $customerContactsHistoryID = mysqli_insert_id(DatabaseHistory::$connection);
                                // add the address to the contact using the link table.
                                $checkAddress = mysqli_query(Database::$connection, "SELECT cadt_id FROM customer_address_tbl "
                                        . "WHERE customer_tbl_cust_id = '" . $customerID . "' "
                                        . "AND cadt_default_address = '1'");

                                $checkAddressRow = mysqli_fetch_assoc($checkAddress);

                                $addressHasContactsHistory = "INSERT INTO customer_address_tbl_has_customer_contacts_history_tbl "
                                        . "(customer_address_tbl_cadt_id, customer_contacts_history_tbl_ccht_id) "
                                        . "VALUES ('" . $checkAddressRow['cadt_id'] . "', '" . $customerContactsHistoryID . "')";

                                if (!mysqli_query(Database::$connection, $addressHasContactsHistory)) {
                                    debugWriter("debug.txt", "INSERT ADDRESS/HISTORY FAILED : " . $addressHasContactsHistory . " " . mysqli_error(Database::$connection));
                                }
                            }*/
                        } else {
                            // TODO put it somewhere else to be looked at manually.
                        }
                    }
                }
            }
        }
        print "<p>Added " . $contactCount . " new contacts<p/>";
    }
    
    
    /**
     * getAllFromManualCheckTbl
     * @return row
     */
    public function getAllFromManualCheckTbl() {

        $count = mysqli_query(Database::$connection, "SELECT count(*) FROM manual_check_tbl");
        $countRow = mysqli_fetch_assoc($count);

        if ($countRow['count(*)'] < 1) {
            $this->dropManualCheckTbl();
            return null;
        } else {

            $result = mysqli_query(Database::$connection, "SELECT * FROM manual_check_tbl");

            while ($row = mysqli_fetch_assoc($result)) {
                $array[$row['temp_id']] = $row;
            }

            if (isset($array)) {
                return $array;
            } else {
                debugWriter("debug.txt", "getAllFromManualCheckTbl FAILED " . mysqli_error(Database::$connection));
                return null;
            }
        }
    }

    /**
     * dropManualCheckTbl
     */
    public function dropManualCheckTbl() {
        $drop_table = "DROP TABLE IF EXISTS manual_check_tbl";
        if (!mysqli_query(Database::$connection, $drop_table)) {
            debugWriter("debug.txt", "Can't drop manual_check_tbl - gravity failure : " . mysqli_error(Database::$connection) . "");
        }
    }
    
    
    
    /**
     * importDataFromCSV
     * @param STRING $fileName
     * @param ARRAY $columnNameArray
     * @param BOOLEAN $headers
     * @param STRING $type
     */
    public function importDataFromCSV($fileName, $columnNameArray, $headers, $type) {

        // This writes out to a file the data/time and file name when things are uploaded.
        /*debugWriter("loadSuppliersDataLogs.txt", "Log Date: " . date("Y-m-d H:i", time()) . " USER: " . $_SESSION['name'] . "");
        debugWriter("loadSuppliersDataLogs.txt", "File Name: " . $fileName . "");
        foreach ($columnNameArray as $p => $q) {
            debugWriter("loadSuppliersDataLogs.txt", "Column Name Array: " . $p . " - " . $q . "");
        }
        debugWriter("loadSuppliersDataLogs.txt", "Headers Used: " . $headers . "");
        debugWriter("loadSuppliersDataLogs.txt", "Type: " . $type . "");*/

        // GLOBAL VARIABLES

        $this->dbColumnName = "";
        $this->newColumnNameArray = [];
        $this->replace = ["/", "-", " ", ".", "#"]; // REGEX (Regular Expression) for clearing out these characters
        // create the temporary table.
        $this->createTemporaryTable($columnNameArray, $type);

        // create a table to load the records we want to check manually.
        $this->createManualCheckTable($columnNameArray, $type);

        $this->loadFile($fileName, $headers, $type);

        $this->cleanTempData();
        /*
         * Do these next sweeps by first pulling out a query of just the necessary data, then do the search.
         */

        /*
         * Look at each database record in turn and see if the distinct Managing Companies (Customers) exist already.
         * If they don't exist in the DB, then a new record needs to be created.
         */
        $this->createCustomers();
        
        
        $this->createApprovalData();

        /*
         * Once the new customers have been added, get the contact details for each customer and add them to the
         * contacts database table, if the contact does not already exist.
         */

        /**
         * Dont be fooled into thinking this bit should be easy, there are an unknown number of columns
         * with whatever names were created in the spreadsheets. There could also be up to three contact
         * records on each line. This has given me logic headaches.
         */
        $this->createContacts();

        
        /**
         * Drop the temporary table.
         */
        $drop_table = "DROP TABLE IF EXISTS temporary_suppliers_table";
        if (!mysqli_query(Database::$connection, $drop_table)) {
            debugWriter("loadSuppliersDataLogs.txt", "Can't drop table - gravity failure : " . mysqli_error(Database::$connection) . "");
        }
    }
}
