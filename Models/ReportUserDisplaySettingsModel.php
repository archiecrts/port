<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/ReportUserDisplaySettings.php");
include_once("Models/Entities/ReportUserDisplayColumnName.php");
include_once("Models/SQLConstructors/ReportUserDisplaySettingsSQLConstructor.php");
//include_once ("Models/DB.php");
include_once("Models/Database.php");

/**
 * Description of ReportUserDisplaySettingsModel
 *
 * @author Archie
 */
class ReportUserDisplaySettingsModel {

    /**
     * getSettingsByID
     * @param INT $userID
     * @return ARRAY \ReportUserDisplaySettingsModel
     */
    public function getSettingsByID($userID) {
        $reportUserDisplaySettingsSQLConstructor = new ReportUserDisplaySettingsSQLConstructor();
        $array = [];
        //execute the SQL query and return records
        $result = mysqli_query(Database::$connection, "SELECT * FROM report_user_display_settings_tbl "
                . "WHERE user_tbl_usr_id='" . $userID . "' AND ruds_active_flag = '1'");

        //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {
            $colResult = mysqli_query(Database::$connection, "SELECT * FROM report_user_display_column_names_tbl "
                    . "WHERE rudcn_id = '" . $row['report_user_display_column_names_tbl_rudcn_id'] . "'");

            $row1 = mysqli_fetch_assoc($colResult);

            $array[$row1['rudcn_id']] = $reportUserDisplaySettingsSQLConstructor->createReportUserDisplaySettings($row1);
        }
        if (sizeof($array) == 0) { // this is for accounts that I created in WORKBENCH.
            $result = mysqli_query(Database::$connection, "SELECT * FROM report_user_display_settings_tbl "
                    . "WHERE user_tbl_usr_id='1' AND ruds_active_flag = '1'");

            //fetch tha data from the database
            while ($row = mysqli_fetch_assoc($result)) {
                $colResult = mysqli_query(Database::$connection, "SELECT * FROM report_user_display_column_names_tbl "
                        . "WHERE rudcn_id = '" . $row['report_user_display_column_names_tbl_rudcn_id'] . "'");

                $row1 = mysqli_fetch_assoc($colResult);

                $array[$row1['rudcn_id']] = $reportUserDisplaySettingsSQLConstructor->createReportUserDisplaySettings($row1);
            }
        }

        return $array;
    }
    
    /*
     * getAllSettings
     */
    public function getAllSettings() {
        $reportUserDisplaySettingsSQLConstructor = new ReportUserDisplaySettingsSQLConstructor();
        $colResult = mysqli_query(Database::$connection, "SELECT * FROM report_user_display_column_names_tbl");

        while($row1 = mysqli_fetch_assoc($colResult)) {
            
            $array[$row1['rudcn_id']] = $reportUserDisplaySettingsSQLConstructor->createReportUserDisplaySettings($row1);
        }
        
        return $array;

    }

    /**
     * setSettingsForUser
     * @param ARRAY $settingsArray
     * @return boolean
     */
    public function setSettingsForUser($settingsArray, $userID) {

        foreach ($settingsArray as $columnID => $active) {
            $result = "INSERT INTO report_user_display_settings_tbl "
                    . "(report_user_display_column_names_tbl_rudcn_id, "
                    . "user_tbl_usr_id, ruds_active_flag) VALUES "
                    . "('" . $columnID . "', "
                    . "'" . $userID . "', "
                    . "'" . $active . "')";

            if (!mysqli_query(Database::$connection, $result)) {
                debugWriter("debug.txt", "setSettingsForUser: " . mysqli_error(Database::$connection));
                return false;
            }
        }
        return true;
    }

}
