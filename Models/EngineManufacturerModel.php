<?php
include_once("Models/Entities/EngineManufacturer.php");
include_once("Models/Entities/EngineBuilder.php");
include_once 'Models/SQLConstructors/EngineManufacturerSQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of EngineManufacturerModel
 *
 * @author Archie
 */
class EngineManufacturerModel {
    
    
    /**
     * 
     * @return \EngineManufacturer
     */
    public function getDistinctEngineManufacturers() {
        $engineManufacturerSQLConstructor = new EngineManufacturerSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT DISTINCT(iet_engine_manufacturer) "
                            . "FROM installation_engine_tbl ORDER BY iet_engine_manufacturer ASC");

            //fetch tha data from the database
            while ($row = mysqli_fetch_assoc($result)) {             
                $array[$row['iet_engine_manufacturer']] 
                        = $engineManufacturerSQLConstructor->createEngineManufacturer($row);
            }
        return $array;
    }
    
    /**
     * getDistinctEngineManufacturersByTypeOfProduct
     * @param INT $typeOfProductID
     * @return type
     */
    public function getDistinctEngineManufacturersByTypeOfProduct($typeOfProductID) {
        $engineManufacturerSQLConstructor = new EngineManufacturerSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT DISTINCT(iet_engine_manufacturer) "
                            . "FROM installation_engine_tbl ORDER BY iet_engine_manufacturer ASC");

            //fetch tha data from the database
            while ($row = mysqli_fetch_assoc($result)) {             
                $array[$row['iet_engine_manufacturer']] 
                        = $engineManufacturerSQLConstructor->createEngineManufacturer($row);
            }
        return $array;
    }
    

    
    /**
     * 
     * @return \EngineBuilder
     */
    public function getDistinctEngineBuildersWithIntervals() {
        $engineManufacturerSQLConstructor = new EngineManufacturerSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM engine_builder_tbl WHERE eb_id IN "
                    . "(SELECT engine_builder_tbl_eb_id FROM engine_servicing_intervals_tbl "
                    . "GROUP BY engine_builder_tbl_eb_id)");
            
            
            //fetch tha data from the database
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['eb_id']] 
                    = $engineManufacturerSQLConstructor->createEngineBuilder($row);
        }
        if(isset($array)) {
            return $array;
        } else {
            return null;
        }
        
    }
    
    public function getEngineBuilders() {
        $engineManufacturerSQLConstructor = new EngineManufacturerSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM engine_builder_tbl");

        //fetch tha data from the database
        while ($row = mysqli_fetch_array($result)) {
           $array[$row['eb_id']] = $engineManufacturerSQLConstructor->createEngineBuilder($row);
        }
        
        if(isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    /**
     * 
     * @param type $engineBuilder
     * @return type
     */
    public function setEngineBuilder($engineBuilder) {
        // INSERT ENGINE BUILDER/MAKE
        $builder_insert = "INSERT INTO engine_builder_tbl (eb_engine_builder_name) VALUES ('".$engineBuilder."')";

        if (!mysqli_query(Database::$connection, $builder_insert)) {
            return mysqli_error(Database::$connection);
        } else {
            return mysqli_insert_id(Database::$connection);
        }
    }
    
    
    /**
     * 
     * @param type $manufacturerID
     * @param type $updatedManufacturer
     * @return boolean
     */
    public function updateEngineBuilder($manufacturerID, $updatedManufacturer) {
        $update_make_query = "UPDATE engine_builder_tbl "
                        . "SET eb_engine_builder_name = '".$updatedManufacturer."' "
                        . "WHERE eb_id = '".$manufacturerID."'";

        if (!mysqli_query(Database::$connection, $update_make_query)) {
            return mysqli_error(Database::$connection);
        } else {
            return true;
        }
    }
    
    
    /**
     * getEngineBuilderByID
     * @param type $engineBuilderID
     * @return type
     */
    public function getEngineBuilderByID($engineBuilderID) {
        $engineManufacturerSQLConstructor = new EngineManufacturerSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM engine_builder_tbl WHERE eb_id = '".$engineBuilderID."'");

        //fetch tha data from the database
        $row = mysqli_fetch_array($result);
        $engineBuilder = $engineManufacturerSQLConstructor->createEngineBuilder($row);
        
        
        if(isset($engineBuilder)) {
            return $engineBuilder;
        } else {
            return null;
        }
    }
}
