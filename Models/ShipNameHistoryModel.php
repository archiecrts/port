<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/ShipNameHistory.php");
include_once("Models/SQLConstructors/ShipNameHistorySQLConstructor.php");
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of ShipNameHistoryModel
 *
 * @author Archie
 */
class ShipNameHistoryModel {
   
    
    /**
     * getAllShipNameHistoryForInstallation
     * @param type $installationID
     * @return type
     */
    public function getAllShipNameHistoryForInstallation($installationID) {
        $shipNameHistorySQLConstructor = new ShipNameHistorySQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM ship_name_history_tbl WHERE "
                . "installations_tbl_inst_id = '$installationID' ORDER BY snht_sequence DESC");
        
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['snht_id']] = $shipNameHistorySQLConstructor->createShipNameHistory($row);
        }
        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
        
    }
}
