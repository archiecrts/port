<?php

include_once("Models/Entities/Installation.php");
include_once("Models/Entities/CustomerConversation.php");
include_once("Models/Entities/CustomerConversationHistory.php");
include_once("Models/SQLConstructors/InstallationSQLConstructor.php");
include_once("Models/SQLConstructors/CustomerConversationSQLConstructor.php");
include_once("Models/SQLConstructors/CustomerConversationHistorySQLConstructor.php");
include_once("Models/SQLConstructors/CustomerSQLConstructor.php");
include_once 'Models/SQLConstructors/CustomerAddressSQLConstructor.php';
include_once("Models/DB.php");
include_once("Models/Database.php");

/**
 * The Installation Model provides the preset queries into the Installation
 * Table.
 *
 * @author Archie
 */
class InstallationModel {
    
    /**
     * SINGLE INSTALLATION
     * @param type $installationID
     * @return \Installation
     */
    public function getInstallationByInstallationID($installationID)
    {
        $installationSQLConstructor = new InstallationSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM installations_tbl WHERE inst_id='".$installationID."' ");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);

        
        $installation = $installationSQLConstructor->createInstallation($row);
        
        
        return $installation;
    }
    
    
    /**
     * getInstallationByIMO
     * @param type $imo
     * @return type
     */
    public function getInstallationByIMO($imo)
    {
        $installationSQLConstructor = new InstallationSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM installations_tbl WHERE inst_imo='".$imo."' ");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);

        
        $installation = $installationSQLConstructor->createInstallation($row);
        
        
        return $installation;
    }
    
   

    /**
     * Could be one or several installations listed under the same name (to accommodate different units).
     * @param type $installationName
     * @return \Installation
     */
    public function getInstallationsByInstallationName($installationName)
    {
        $installationSQLConstructor = new InstallationSQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM installations_tbl WHERE"
                . " inst_installation_name = '".$installationName."'");
        
        
        while ($row = mysqli_fetch_assoc($result)) {
            
            
            $array[$row['inst_id']] = $installationSQLConstructor->createInstallation($row);
            
        }

        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    /**
     * getInstallationsByInstallationNameAndCustomerID
     * @param type $installationName
     * @param type $customerID
     * @return type
     */
    public function getInstallationsByInstallationNameAndCustomerID($installationName, $customerID)
    {
        $installationSQLConstructor = new InstallationSQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM installations_tbl WHERE"
                . " inst_installation_name = '".$installationName."' "
                . "AND inst_id IN (SELECT installations_tbl_inst_id FROM "
                . "customer_tbl_has_installations_tbl WHERE customer_tbl_cust_id='".$customerID."')");
        
       // debugWriter("debug.txt", "getInstallationsByInstallationNameAndCustomerID ERROR ".$installationName." ". $customerID." ".mysqli_error(Database::$connection));
        while ($row = mysqli_fetch_assoc($result)) {
            
            
            $array[$row['inst_id']] = $installationSQLConstructor->createInstallation($row);
            
        }

        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    /**
     * 
     * @param type $installationObject
     * @return type
     */
    public function updateInstallationObject($installationObject) {
        
        $string = "UPDATE installations_tbl SET 
            inst_type = '".$installationObject->type."', 
            inst_installation = '".addslashes($installationObject->installation)."', 
            inst_installation_name= '".addslashes($installationObject->installationName)."', 
            inst_imo = '".$installationObject->imo."', 
            installation_status_tbl_stat_id = '".$installationObject->status."', 
            inst_owner_parent_company = '".addslashes($installationObject->operatingCompany)."', 
            inst_technical_manager_company = '".addslashes($installationObject->technicalManager)."', 
            inst_group_owner = '".addslashes($installationObject->groupOwner)."', 
            inst_original_source = '".addslashes($installationObject->originalSource)."',
            inst_original_source_date = '".$installationObject->originalSourceDate."',
            inst_photo_link = '".addslashes($installationObject->photoLink)."',
            inst_hull_number = '".addslashes($installationObject->hullNumber)."', 
            inst_hull_type = '".addslashes($installationObject->hullType)."',
            inst_yard_built = '".addslashes($installationObject->yardBuilt)."',
            inst_deadweight = '".addslashes($installationObject->deadWeight)."',
            inst_length_overall = '".addslashes($installationObject->lengthOverall)."',
            inst_beam_extreme = '".addslashes($installationObject->beamExtreme)."',
            inst_beam_moulded = '".addslashes($installationObject->beamMoulded)."',
            inst_built_date = '".addslashes($installationObject->builtDate)."',
            inst_draught = '".addslashes($installationObject->draught)."',
            inst_gross_tonnage = '".addslashes($installationObject->grossTonnage)."',
            inst_length = '".addslashes($installationObject->length)."',
            inst_propeller_type = '".addslashes($installationObject->propellerType)."',
            inst_propulsion_unit_count = '".addslashes($installationObject->propulsionUnitCount)."',
            inst_ship_builder = '".addslashes($installationObject->shipBuilder)."',
            inst_teu = '".addslashes($installationObject->teu)."',
            inst_build_year = '".$installationObject->buildYear."',
            inst_archive = '".$installationObject->archiveFlag."',
            inst_archive_reason = '".addslashes($installationObject->archiveReason)."',
            customer_address_tbl_cadt_id = '".$installationObject->customerAddressID."',
            inst_flag_name = '".addslashes($installationObject->flagName)."',
            inst_lead_ship_in_series_by_imo = '".$installationObject->leadShipByIMO."',
            inst_ship_type_level_4 = '".$installationObject->shipTypeLevel4."',
            inst_classification_society = '".addslashes($installationObject->classificationSociety)."' 
            WHERE inst_id = '".$installationObject->installationID."'";
        
        if (!(mysqli_query(Database::$connection, $string))) {
            debugWriter("debug.txt", "updateInstallationObject ".mysqli_error(Database::$connection));
            return mysqli_error(Database::$connection);
        } else {
            //debugWriter("debug.txt", "INST ID ".$installationObject->installationID);
            return true;
        } 
    }
    
    /**
     * getAllInstallationsByCustomer
     * @param type $customerID
     * @return \Installation
     */
    public function getAllInstallationsByCustomer($customerID) {
        $installationSQLConstructor = new InstallationSQLConstructor();
        
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM installations_tbl WHERE inst_id IN (
                            SELECT installations_tbl_inst_id FROM customer_tbl_has_installations_tbl WHERE 
                            customer_tbl_cust_id  = '".$customerID."') AND inst_installation_name != 'UNKNOWN INSTALLATION' "
                . "AND inst_archive = '0'");
        
        while ($row = mysqli_fetch_assoc($result)) {
            
            $array[$row['inst_id']] = $installationSQLConstructor->createInstallation($row);
           
        }
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    /**
     * getCombinedFleetByCustomer
     * @param type $customerID
     * @return type
     */
    public function getCombinedFleetByCustomer($customerID) {
        $installationSQLConstructor = new InstallationSQLConstructor();
        
        //OR inst_owner_parent_company = '".$customerID."' 
        $result = mysqli_query(Database::$connection, "SELECT * FROM installations_tbl WHERE inst_id IN (
                            SELECT installations_tbl_inst_id FROM customer_tbl_has_installations_tbl WHERE 
                            customer_tbl_cust_id  = '".$customerID."')
                            
                            OR inst_technical_manager_company = '".$customerID."'
                            OR inst_group_owner = '".$customerID."'
                            AND inst_installation_name != 'UNKNOWN INSTALLATION'
                            AND inst_archive = '0'");
        
        while ($row = mysqli_fetch_assoc($result)) {
            
            $array[$row['inst_id']] = $installationSQLConstructor->createInstallation($row);
           
        }
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    /**
     * getAllArchviedInstallationsByCustomer
     * @param type $customerID
     * @return \Installation
     */
    public function getAllArchviedInstallationsByCustomer($customerID) {
        $installationSQLConstructor = new InstallationSQLConstructor();
        
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM installations_tbl WHERE inst_id IN (
                            SELECT installations_tbl_inst_id FROM customer_tbl_has_installations_tbl WHERE 
                            customer_tbl_cust_id  = '".$customerID."') AND inst_installation_name != 'UNKNOWN INSTALLATION' "
                . "AND inst_archive = '1'");
        
        while ($row = mysqli_fetch_assoc($result)) {
            
            $array[$row['inst_id']] = $installationSQLConstructor->createInstallation($row);
           
        }
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    /**
     * createNewInstallationObject
     * @param type $object
     * @param type $customerID
     * @return type
     */
    public function createNewInstallationObject($object, $customerID) {
        
        $string = "INSERT INTO installations_tbl (
                    inst_type, inst_installation, inst_installation_name, 
                    inst_imo,  installation_status_tbl_stat_id, inst_owner_parent_company, 
                    inst_technical_manager_company, inst_original_source, 
                    inst_original_source_date, inst_photo_link, inst_hull_number, 
                    inst_hull_type, inst_yard_built, inst_deadweight, inst_length_overall, 
                    inst_beam_extreme, inst_beam_moulded, inst_built_date, inst_draught, 
                    inst_gross_tonnage, inst_length, inst_propeller_type, inst_propulsion_unit_count, 
                    inst_ship_builder, inst_teu, inst_build_year, inst_archive, inst_archive_reason, 
                    customer_address_tbl_cadt_id, inst_flag_name, inst_lead_ship_in_series_by_imo, 
                    inst_ship_type_level_4, inst_classification_society)
                        VALUES(
                            '" . addslashes($object->type) . "', 
                            '" . addslashes($object->installation) . "', 
                            '" . addslashes($object->installationName) . "', 
                            '" . addslashes($object->imo) . "', 
                            '" . addslashes($object->status) . "', 
                            '" . addslashes($object->operatingCompany) . "', 
                            '" . addslashes($object->technicalManager) . "', 
                            '" . addslashes($object->originalSource) . "', 
                            '" . addslashes($object->originalSourceDate) . "', 
                            '" . addslashes($object->photoLink) ."', 
                            '" . addslashes($object->hullNumber) ."', "
                            . "'" . addslashes($object->hullType) ."', "
                            . "'" . addslashes($object->yardBuilt) ."', "
                            . "'" . addslashes($object->deadWeight) ."', "
                            . "'" . addslashes($object->lengthOverall) ."', "
                            . "'" . addslashes($object->beamExtreme) ."', "
                            . "'" . addslashes($object->beamMoulded) ."', "
                            . "'" . addslashes($object->builtDate) ."', "
                            . "'" . addslashes($object->draught) ."', "
                            . "'" . addslashes($object->grossTonnage) ."', "
                            . "'" . addslashes($object->length) ."', "
                            . "'" . addslashes($object->propellerType) ."', "
                            . "'" . addslashes($object->propulsionUnitCount) ."', "
                            . "'" . addslashes($object->shipBuilder) ."', "
                            . "'" . addslashes($object->teu) ."', "
                            . "'" . addslashes($object->buildYear) ."',"
                            . "'0', "
                            . "'',"
                            . "'" . addslashes($object->customerAddressID) . "', "
                            . "'" . addslashes($object->flagName)."', "
                            . "'" . addslashes($object->leadShipByIMO)."', "
                            . "'" . addslashes($object->shipTypeLevel4)."', "
                            . "'" . addslashes($object->classificationSociety)."')";
        
        if (!(mysqli_query(Database::$connection, $string))) {
            debugWriter("debug.txt", "createNewInstallationObject Failed $string ".mysqli_error(Database::$connection)."\r\n");
            $return = "Insert has failed";
        } else {
            // add an entry into the linking table.
            $newInstallationID = mysqli_insert_id(Database::$connection);
            $insert = "INSERT INTO customer_tbl_has_installations_tbl "
                    . "(customer_tbl_cust_id, installations_tbl_inst_id) VALUES "
                    . "('".$customerID."', '".$newInstallationID."')";
            if (!(mysqli_query(Database::$connection, $insert))) {
                debugWriter("debug.txt", "createNewInstallationObject Failed (second phase) $insert ".mysqli_error(Database::$connection)."\r\n");
                $return = "Insert has failed";
            } else {
                $return =  $newInstallationID;
            }
        }
        
        return $return;
    }
    
    /**
     * getInstallationsWithConversationEntriesForAccountManager
     * @param type $accountManager
     * @return \Installation
     */
    public function getInstallationsWithConversationEntriesForAccountManager($accountManager) {

        $customerSQLConstructor = new CustomerSQLConstructor();
        $customerAddressSQLConstructor = new CustomerAddressSQLConstructor();
        $customerConversationSQLConstructor = new CustomerConversationSQLConstructor();
        $customerConversationHistorySQLConstructor = new CustomerConversationHistorySQLConstructor();
        $array =[];
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_conversation_tbl c, customer_contacts_tbl i, 
                                customer_conversation_history_tbl h, customer_tbl q, customer_address_tbl a  
                                WHERE c.customer_contacts_tbl_ict_id = i.ict_id 
                                AND c.con_id = h.customer_conversation_tbl_con_id 
                                AND con_id NOT IN (SELECT customer_conversation_tbl_con_id FROM 
                                                    customer_conversation_history_tbl WHERE 
                                                    ich_status = '0') 
                                AND q.cust_id = i.customer_tbl_cust_id   
                                AND (q.cust_id = a.customer_tbl_cust_id AND a.cadt_default_address = '1')
                                AND (a.cadt_country IN (
                                    SELECT c_name 
                                    FROM country_tbl 
                                    WHERE c_continent IN (
                                        SELECT ut_territory_name 
                                        FROM user_territory_tbl 
                                        WHERE user_tbl_usr_id='" . $accountManager . "')
                                    OR c_area IN (
                                        SELECT ut_territory_name 
                                        FROM user_territory_tbl 
                                        WHERE user_tbl_usr_id='" . $accountManager . "')
                                    OR c_name IN(
                                        SELECT ut_territory_name 
                                        FROM user_territory_tbl 
                                        WHERE user_tbl_usr_id='" . $accountManager . "')
                                    AND q.cust_account_manager IS NULL or q.cust_account_manager = '" . $accountManager . "')
                                    OR h.user_tbl_usr_id = '" . $accountManager . "')
                                ORDER BY con_id DESC;");
        
        while ($row = mysqli_fetch_assoc($result)) {
            
            $array[$row['con_id']]['customer'] = $customerSQLConstructor->createCustomer($row);
            
            $array[$row['con_id']]['address'] = $customerAddressSQLConstructor->createCustomerAddress($row);
            
            $array[$row['con_id']]['conversation'] = $customerConversationSQLConstructor->createCustomerConversation($row);
                        
            $array[$row['con_id']]['history'] = $customerConversationHistorySQLConstructor->createCustomerConversationHistory($row);
                      
        }
        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
        
    }
    
    
    /**
     * getInstallationsWithConversationEntriee
     * @param INT $userID
     * @return \Installation
     */
    public function getInstallationsWithConversationEntries($userID) {
        
        $customerSQLConstructor = new CustomerSQLConstructor();
        $customerAddressSQLConstructor = new CustomerAddressSQLConstructor();
        $customerConversationSQLConstructor = new CustomerConversationSQLConstructor();
        $customerConversationHistorySQLConstructor = new CustomerConversationHistorySQLConstructor();
        $array = [];
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_conversation_tbl c, customer_contacts_tbl i, 
                                customer_conversation_history_tbl h, customer_tbl q, customer_address_tbl a  
                                WHERE c.customer_contacts_tbl_ict_id = i.ict_id 
                                AND c.con_id = h.customer_conversation_tbl_con_id 
                                AND con_id NOT IN (SELECT customer_conversation_tbl_con_id FROM 
                                                    customer_conversation_history_tbl WHERE 
                                                    ich_status = '0') 
                                AND q.cust_id = i.customer_tbl_cust_id   
                                AND (q.cust_id = a.customer_tbl_cust_id AND a.cadt_default_address = '1')
                                ORDER BY con_id DESC");
        
        while ($row = mysqli_fetch_assoc($result)) {
            
            $array[$row['con_id']]['customer'] = $customerSQLConstructor->createCustomer($row);
            
            $array[$row['con_id']]['address'] = $customerAddressSQLConstructor->createCustomerAddress($row);
            
            $array[$row['con_id']]['conversation'] = $customerConversationSQLConstructor->createCustomerConversation($row);
                        
            $array[$row['con_id']]['history'] = $customerConversationHistorySQLConstructor->createCustomerConversationHistory($row);
                   
        }
        
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    /**
     * $ownerParentID
     * @param type $ownerParentID
     * @return type
     */
    public function getInstallationsByOwnerParentID($ownerParentID) {
        $installationSQLConstructor = new InstallationSQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM installations_tbl WHERE inst_owner_parent_company='$ownerParentID'");
    
        //debugWriter("debug.txt", "getInstallationsByOwnerParentID ". mysqli_error(Database::$connection));
        while ($row = mysqli_fetch_assoc($result)) {
            
            $array[$row['inst_id']] = $installationSQLConstructor->createInstallation($row);
           
        }
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    /**
     * getInstallationsByTechnicalManagerID
     * @param type $technicalManagerID
     * @return type
     */
    public function getInstallationsByTechnicalManagerID($technicalManagerID) {
        $installationSQLConstructor = new InstallationSQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM installations_tbl WHERE inst_technical_manager_company='$technicalManagerID'");
    //debugWriter("debug.txt", "getInstallationsByTechnicalManagerID ". mysqli_error(Database::$connection));
        while ($row = mysqli_fetch_assoc($result)) {
            
            $array[$row['inst_id']] = $installationSQLConstructor->createInstallation($row);
           
        }
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
    
    /**
     * getSisterShipsByLeadIMO
     * @param type $imo
     * @return type
     */
    public function getSisterShipsByLeadIMO($imo) {
        $installationSQLConstructor = new InstallationSQLConstructor();
        
        $result = mysqli_query(Database::$connection, "SELECT * FROM installations_tbl WHERE "
                . "inst_lead_ship_in_series_by_imo = '$imo' OR inst_imo = '$imo'");
    
        while ($row = mysqli_fetch_assoc($result)) {
            
            $array[$row['inst_id']] = $installationSQLConstructor->createInstallation($row);
           
        }
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }
}