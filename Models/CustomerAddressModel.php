<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/CustomerAddress.php");
include_once("Models/SQLConstructors/CustomerAddressSQLConstructor.php");
include_once("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of CustomerAddress
 *
 * @author Archie
 */
class CustomerAddressModel {
    
    
    /**
     * Get CustomerAddress Record by Customer ID.
     * @param INT $customerID
     * @return CustomerAddress Object
     */
    public function getDefaultCustomerAddressByCustomerID($customerID) {

        $customerAddressSQLConstructor = new CustomerAddressSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_address_tbl WHERE customer_tbl_cust_id='".$customerID."' "
                . "AND cadt_default_address = '1'");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);

        $customerAddress = $customerAddressSQLConstructor->createCustomerAddress($row);
        
        return $customerAddress;
    }
    
    
    /**
     * getCustomerAddressesByCustomerID
     * @param INT $customerID
     * @return Array of Customer Address Objects
     */
    public function getCustomerAddressesByCustomerID($customerID) {

        $customerAddressSQLConstructor = new CustomerAddressSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_address_tbl "
                . "WHERE customer_tbl_cust_id='".$customerID."' "
                . "AND cadt_archived = '0'");

        //fetch tha data from the database
        while($row = mysqli_fetch_assoc($result)) {

            $array[$row['cadt_id']] = $customerAddressSQLConstructor->createCustomerAddress($row);
        }
        if (isset($array)) {
            return $array;
        } else {
            //debugWriter("debug.txt", "getCustomerAddressesByCustomerID: ".date("Y-m-d H:i:s", time())." : ".mysqli_error(Database::$connection)."\r\n");
            return null;
        }
    }
    
    
    /**
     * getCustomerAddressByID
     * @param INT $addressID
     * @return OBJECT
     */
    public function getCustomerAddressByID($addressID) {
        $customerAddressSQLConstructor = new CustomerAddressSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM customer_address_tbl WHERE cadt_id='".$addressID."'");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);

        $customerAddress = $customerAddressSQLConstructor->createCustomerAddress($row);
        
        return $customerAddress;
    }
    
    /**
     * updateCustomerAddress
     * @param Entity Object $customerAddressObject
     * @return boolean
     */
    public function updateCustomerAddress($customerAddressObject) {
        $update = "UPDATE customer_address_tbl SET "
                . "cadt_office_name = '". addslashes($customerAddressObject->officeName). "', "
                . "cadt_address = '". addslashes($customerAddressObject->address). "', "
                . "cadt_city = '". addslashes($customerAddressObject->city). "', "
                . "cadt_state = '". addslashes($customerAddressObject->state). "', "
                . "cadt_region = '". addslashes($customerAddressObject->region). "', "
                . "cadt_country = '". addslashes($customerAddressObject->country). "', "
                . "cadt_postcode = '". addslashes($customerAddressObject->postcode) ."', "
                . "cadt_address_verified = '". $customerAddressObject->addressVerified ."', "
                . "cadt_archived = '". $customerAddressObject->archived ."', "
                . "cadt_default_address = '". $customerAddressObject->defaultAddress ."', "
                . "cadt_full_address = '". addslashes($customerAddressObject->fullAddress) ."' "
                . "WHERE cadt_id = '".$customerAddressObject->addressID."'";
        
        if (!mysqli_query(Database::$connection, $update)) {
            debugWriter("debug.txt", "updateCustomerAddress: ".mysqli_error(Database::$connection)."\r\n");
            return "Address changes not saved";
        } else {
            return true;
        }
    }
    
    /**
     * setCustomerAddress
     * @param Object $customerAddressObject
     * @return string
     */
    public function setCustomerAddress($customerAddressObject) {
        //debugWriter("debug.txt", "setCustomerAddress: ".$customerAddressObject->officeName."\r\n");
        $insert = "INSERT INTO customer_address_tbl ("
                . "cadt_office_name, "
                . "cadt_address, "
                . "cadt_city, "
                . "cadt_state, "
                . "cadt_region, "
                . "cadt_country, "
                . "cadt_postcode, "
                . "cadt_address_verified, "
                . "customer_tbl_cust_id, "
                . "cadt_archived, "
                . "cadt_default_address) "
                . "VALUES ('".$customerAddressObject->officeName. "', "
                . "'". $customerAddressObject->address. "', "
                . "'". $customerAddressObject->city. "', "
                . "'". $customerAddressObject->state. "', "
                . "'". $customerAddressObject->region. "', "
                . "'". $customerAddressObject->country. "', "
                . "'". $customerAddressObject->postcode ."', "
                . "'". $customerAddressObject->addressVerified ."', "
                . "'". $customerAddressObject->customerID."', "
                . "'". $customerAddressObject->archived ."', "
                . "'". $customerAddressObject->defaultAddress ."')";
        
        if (!mysqli_query(Database::$connection, $insert)) {
            debugWriter("debug.txt", "setCustomerAddress: ".$insert. " ".mysqli_error(Database::$connection)."\r\n");
            return "Address not saved";
        } else {
            return mysqli_insert_id(Database::$connection);
        }
    }
}
