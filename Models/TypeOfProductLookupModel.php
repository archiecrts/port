<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/TypeOfProductLookup.php");
include_once 'Models/SQLConstructors/TypeOfProductLookupSQLConstructor.php';
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of TypeOfProductLookupModel
 *
 * @author Archie
 */
class TypeOfProductLookupModel {
    
    
    /**
     * getTypeOfProductByID
     * @param type $toplID
     * @return type
     */
    public function getTypeOfProductByID($toplID) {
        $typeOfProductSQLConstructor = new TypeOfProductLookupSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM type_of_product_lookup_tbl WHERE toplt_id='".$toplID."'");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);
        $type = $typeOfProductSQLConstructor->createTypeOfProductLookup($row);
        
        
        if (isset($type)) {
            return $type;
        } else {
            return null;
        }
    }
    
    /**
     * getAllProductTypes
     * @return type
     */
    public function getAllProductTypes() {
        $typeOfProductSQLConstructor = new TypeOfProductLookupSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM type_of_product_lookup_tbl");

        //fetch tha data from the database
        while ( $row = mysqli_fetch_assoc($result)) {
            $type[$row['toplt_id']] = $typeOfProductSQLConstructor->createTypeOfProductLookup($row);
        }
        
        if (isset($type)) {
            return $type;
        } else {
            return null;
        }
    }
}
