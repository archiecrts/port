<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once 'Models/SQLConstructors/InstallationStatusSQLConstructor.php';
include_once("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of InstallationStatusModel
 *
 * @author Archie
 */
class InstallationStatusModel {
    
    /**
     * getInstallationStatusByID
     * @param type $statusID
     * @return type
     */
    public function getInstallationStatusByID($statusID)
    {
        $installationStatusSQLConstructor = new InstallationStatusSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM installation_status_tbl WHERE stat_id='".$statusID."'");

        //fetch tha data from the database
        $row = mysqli_fetch_assoc($result);

        
        $status = $installationStatusSQLConstructor->createInstallationStatus($row);
        
        
        return $status;
    }
    
    /**
     * getAllIstallationStatusObjects
     * @return type
     */
    public function getAllInstallationStatusObjects() {
        $installationStatusSQLConstructor = new InstallationStatusSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM installation_status_tbl");

        //fetch tha data from the database
        while($row = mysqli_fetch_assoc($result)) {
            $array[$row['stat_id']] = $installationStatusSQLConstructor->createInstallationStatus($row);
        }
        
        if (isset($array)) {
            return $array;
        } else {
            debugWriter("debug.txt", "getAllIstallationStatusObjects FAILED ". mysqli_error(Database::$connection));
            return null;
        }
    }
    
}
