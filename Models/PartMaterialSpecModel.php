<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/Entities/PartMaterialSpec.php");
include_once("Models/SQLConstructors/MaterialSpecSQLConstructor.php");
//include_once ("Models/DB.php");
include_once("Models/Database.php");
/**
 * Description of PartMaterialSpecModel
 *
 * @author Archie
 */
class PartMaterialSpecModel {
    
    /**
     * getMaterialSpecByID
     * @param type $materialID
     * @return type
     */
    public function getMaterialSpecByID ($materialID) {
        $materialSpecSQLConstructor = new MaterialSpecSQLConstructor();
        $result = mysqli_query(Database::$connection, "SELECT * FROM material_spec_tbl WHERE mst_id = '$materialID'");
        $row = mysqli_fetch_assoc($result);
        $spec = $materialSpecSQLConstructor->createMaterialSpec($row);
        
        return $spec;
    }
    
    /**
     * insertMaterialSpec
     * @param Object $spec
     * @return boolean or insert id
     */
    public function insertMaterialSpec ($spec) {
        $insert = "INSERT INTO material_spec_tbl (mst_code, mst_description, mst_hardness, mst_din_standard, mst_material_spec) "
                . "VALUES ('$spec->materialCode', "
                . "'$spec->materialDescription', "
                . "'$spec->materialHardness', "
                . "'$spec->dinStandard', "
                . "'$spec->materialSpec')";
        
        if (!mysqli_query(Database::$connection, $insert)) {
            return false;
        } else {
            return mysqli_insert_id(Database::$connection);
        }
    }
    
    
    /**
     * getMaterialsFromCriteriaOptions
     * @param type $spec
     * @return type
     */
    public function getMaterialsFromCriteriaOptions ($spec) {
        $result = mysqli_query(Database::$connection, "SELECT mst_id FROM material_spec_tbl WHERE "
                . "mst_code = '$spec->materialCode' AND "
                . "mst_description = '$spec->materialDescription' AND "
                . "mst_hardness = '$spec->materialHardness' AND "
                . "mst_din_standard = '$spec->dinStandard' AND "
                . "mst_material_spec = '$spec->materialSpec'");
        $row = mysqli_fetch_assoc($result);
        if (isset($row['mst_id'])) {
            return $row['mst_id'];
        } else {
            return null;
        }
        
    }
}
