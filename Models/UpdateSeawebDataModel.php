<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once 'Models/Entities/Customer.php';
include_once 'Models/Entities/CustomerAddress.php';
include_once 'Models/Entities/CustomerContact.php';
include_once 'Models/Entities/Installation.php';
include_once 'Models/Entities/InstallationEngine.php';
include_once 'Models/Entities/Product.php';
include_once 'Models/Entities/SeaWebMainEngine.php';
include_once 'Models/Entities/ShipNameHistory.php';
include_once 'Models/Entities/SpecialSurveyDue.php';
include_once 'Models/Entities/SpecialSurveyHistoryDates.php';
include_once 'Models/Entities/TradingZoneLastSeen.php';
include_once 'Models/SQLConstructors/CustomerSQLConstructor.php';
include_once 'Models/SQLConstructors/CustomerAddressSQLConstructor.php';
include_once 'Models/SQLConstructors/CustomerContactsSQLConstructor.php';
include_once 'Models/SQLConstructors/InstallationSQLConstructor.php';
include_once 'Models/SQLConstructors/InstallationEngineSQLConstructor.php';
include_once 'Models/SQLConstructors/ProductSQLConstructor.php';
include_once 'Models/SQLConstructors/ShipNameHistorySQLConstructor.php';
include_once 'Models/SQLConstructors/SpecialSurveyDueDateSQLConstructor.php';
include_once 'Models/SQLConstructors/SpecialSurveyHistoryDateSQLConstructor.php';
include_once 'Models/SQLConstructors/TradingZoneLastSeenSQLConstructor.php';
include_once 'Controllers/UserHistoryController.php';
include_once("Models/Database.php");

/**
 * Description of UpdateSeawebData
 *
 * @author Archie
 */
class UpdateSeawebDataModel {

    ///////////////////////
    //
    //   CREATE TABLES
    //
    ///////////////////////

    private $replace;

    /**
     * createCustomerTable
     */
    private function createCustomerTable() {

        /*
         * Create a temporary table to contain the CSV data in the same order as the CSV.
         */
        $create_table_query = "CREATE TABLE IF NOT EXISTS update_customer_tbl (temp_id SERIAL, "
                . "owcode INT(10), short_company_name VARCHAR(255), "
                . "country_name VARCHAR(255), town_name VARCHAR(255), telephone VARCHAR(255), "
                . "email VARCHAR(255), website VARCHAR(255), company_status VARCHAR(255), "
                . "full_company_name TEXT, full_address TEXT, customer_duplicate BOOLEAN DEFAULT '0', "
                . "address_duplicate BOOLEAN DEFAULT '0', contact_duplicate BOOLEAN DEFAULT '0', "
                . "customer_exists BOOLEAN DEFAULT '0', address_exists BOOLEAN DEFAULT '0', contact_exists BOOLEAN DEFAULT '0', "
                . "customer_id INT(10), customer_address_id INT(10), customer_contact_id INT(10),  "
                . " PRIMARY KEY(temp_id))"; // set the primary key of the table



        if (!mysqli_query(Database::$connection, $create_table_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't create table : " . $create_table_query . " \r\n" . mysqli_error(Database::$connection) . "");
        }
        $create_table_query = NULL;
        unset($create_table_query);
    }

    /**
     * createShipTable
     */
    private function createShipTable() {

        /*
         * Create a temporary table to contain the CSV data in the same order as the CSV.
         */
        $create_table_query = "CREATE TABLE IF NOT EXISTS update_ship_data_tbl (temp_id SERIAL, "
                . "lrimo VARCHAR(45), ship_name VARCHAR(255), "
                . "ship_status VARCHAR(255), ship_type_level_2 VARCHAR(255), "
                . "ship_type_level_4 VARCHAR(255), date_of_build VARCHAR(45), "
                . "lead_ship_in_series_by_imo VARCHAR(45), "
                . "flag_name VARCHAR(255), classification_society VARCHAR(255),  "
                . "ship_manager VARCHAR(255), ship_manager_company_code VARCHAR(45), "
                . "technical_manager VARCHAR(255), technical_manager_code VARCHAR(45), "
                . "noof_aux_engines INT, noof_main_engines INT, "
                . "noof_propulsion_units INT, propeller_type  VARCHAR(255), "
                . "ship_builder VARCHAR(255), yard_number VARCHAR(45), group_beneficial_owner VARCHAR(255), "
                . "group_beneficial_owner_company_code VARCHAR(45), ship_duplicate BOOLEAN DEFAULT '0', "
                . "ship_exists BOOLEAN DEFAULT '0', installation_id INT(10), "
                . " PRIMARY KEY(temp_id), INDEX(lrimo), INDEX(ship_name))"; // set the primary key of the table



        if (!mysqli_query(Database::$connection, $create_table_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't create table : " . $create_table_query . " \r\n" . mysqli_error(Database::$connection) . "");
        }
        $create_table_query = NULL;
        unset($create_table_query);
    }

    /**
     * createMainEngineTable
     */
    public function createMainEngineTable() {


        $create_table_query = "CREATE TABLE IF NOT EXISTS update_main_engine_tbl (temp_id SERIAL, "
                . "lrno VARCHAR(45), position VARCHAR(255), "
                . "engine_designer VARCHAR(255), engine_builder VARCHAR(255), "
                . "engine_model VARCHAR(255), noof_cylinders INT, "
                . "bore INT, stroke INT, cylinder_configuration VARCHAR(45), "
                . "main_engine_duplicate BOOLEAN DEFAULT '0', main_engine_exists BOOLEAN DEFAULT '0', "
                . "product_id INT(10), inst_engine_id INT(10), "
                . " PRIMARY KEY(temp_id))"; // set the primary key of the table



        if (!mysqli_query(Database::$connection, $create_table_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't create table : " . $create_table_query . " \r\n" . mysqli_error(Database::$connection) . "");
        }
        $create_table_query = NULL;
        unset($create_table_query);
    }

    /**
     * createAuxEngineTable
     */
    public function createAuxEngineTable() {

        $create_table_query = "CREATE TABLE IF NOT EXISTS update_aux_engine_tbl (temp_id SERIAL, "
                . "lrno VARCHAR(45), engine_designer VARCHAR(255), engine_builder VARCHAR(255), "
                . "engine_model VARCHAR(255), noof_cylinders INT, "
                . "bore INT, stroke INT, engine_sequence INT, "
                . "aux_engine_duplicate BOOLEAN DEFAULT '0', aux_engine_exists BOOLEAN DEFAULT '0', "
                . "product_id INT(10), inst_engine_id INT(10), "
                . " PRIMARY KEY(temp_id))"; // set the primary key of the table



        if (!mysqli_query(Database::$connection, $create_table_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't create table : " . $create_table_query . " \r\n" . mysqli_error(Database::$connection) . "");
        }
        $create_table_query = NULL;
        unset($create_table_query);
    }

    /**
     * createSurveyDateHistoryTable
     */
    public function createSurveyDateHistoryTable() {
        $create_table_query = "CREATE TABLE IF NOT EXISTS update_survey_date_history_tbl (temp_id SERIAL, "
                . "lrno VARCHAR(45), special_survey VARCHAR(16), "
                . "sdh_duplicate BOOLEAN DEFAULT '0', sdh_exists BOOLEAN DEFAULT '0', "
                . "sdh_id INT(10), "
                . " PRIMARY KEY(temp_id))"; // set the primary key of the table



        if (!mysqli_query(Database::$connection, $create_table_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't create table : " . $create_table_query . " \r\n" . mysqli_error(Database::$connection) . "");
        }
        $create_table_query = NULL;
        unset($create_table_query);
    }

    /**
     * createSurveyDateTable
     */
    public function createSurveyDateTable() {
        $create_table_query = "CREATE TABLE IF NOT EXISTS update_survey_date_tbl (temp_id SERIAL, "
                . "lrno VARCHAR(45), special_survey VARCHAR(16), "
                . "sd_duplicate BOOLEAN DEFAULT '0', sd_exists BOOLEAN DEFAULT '0', "
                . "sd_id INT(10), "
                . " PRIMARY KEY(temp_id))"; // set the primary key of the table



        if (!mysqli_query(Database::$connection, $create_table_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't create table : " . $create_table_query . " \r\n" . mysqli_error(Database::$connection) . "");
        }
        $create_table_query = NULL;
        unset($create_table_query);
    }

    /**
     * createTradingZoneTable
     */
    public function createTradingZoneTable() {

        $create_table_query = "CREATE TABLE IF NOT EXISTS update_trading_zone_tbl (temp_id SERIAL, "
                . "lrno VARCHAR(45), trading_zone VARCHAR(255), "
                . "last_seen_date DATETIME, "
                . "tz_id INT(10), "
                . " PRIMARY KEY(temp_id))"; // set the primary key of the table

        if (!mysqli_query(Database::$connection, $create_table_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't create table : " . $create_table_query . " \r\n" . mysqli_error(Database::$connection) . "");
        }
        $create_table_query = NULL;
        unset($create_table_query);
    }

    /**
     * createNameHistoryTable
     */
    public function createNameHistoryTable() {
        $create_table_query = "CREATE TABLE IF NOT EXISTS update_name_history_tbl (temp_id SERIAL, "
                . "lrno VARCHAR(45), sequence INT, "
                . "vessel_name VARCHAR(255), effective_date VARCHAR(45), "
                . "name_history_duplicate BOOLEAN DEFAULT '0', name_history_exists  BOOLEAN DEFAULT '0', "
                . "name_history_id INT(10), "
                . " PRIMARY KEY(temp_id))"; // set the primary key of the table

        if (!mysqli_query(Database::$connection, $create_table_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't create table : " . $create_table_query . " \r\n" . mysqli_error(Database::$connection) . "");
        }
        $create_table_query = NULL;
        unset($create_table_query);
    }

    ///////////////////////
    //
    //   LOAD FILES
    //
    ///////////////////////

    /**
     * loadCustomerFile
     * @param type $fileName
     */
    private function loadCustomerFile($fileName) {

        // Load the data from the csv into the temporary table, using the column names generated on the verify page.
        $insert_temp_query = "LOAD DATA LOCAL INFILE '" . $fileName . "'
                INTO TABLE update_customer_tbl
                CHARACTER SET 'latin1'
                COLUMNS TERMINATED BY ','
                ENCLOSED BY '\"' LINES TERMINATED BY '\r\n'
                IGNORE 1 LINES (owcode, short_company_name, 
                country_name, town_name, telephone, 
                email, website, company_status, 
                full_company_name, full_address)";


        // DATABASE query generator. This say, if mysqli_query fails then write the first line. otherwise write the second.
        if (!mysqli_query(Database::$connection, $insert_temp_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't insert records into temporary table : *** " . mysqli_error(Database::$connection));
        }
        $insert_temp_query = NULL;
        unset($insert_temp_query);
        unlink($fileName);
    }

    /**
     * loadShipFile
     * @param type $fileName
     */
    private function loadShipFile($fileName) {

        // Load the data from the csv into the temporary table, using the column names generated on the verify page.
        $insert_temp_query = "LOAD DATA LOCAL INFILE '" . $fileName . "'
                INTO TABLE update_ship_data_tbl
                CHARACTER SET 'latin1'
                COLUMNS TERMINATED BY ','
                ENCLOSED BY '\"' LINES TERMINATED BY '\r\n'
                IGNORE 1 LINES (lrimo, ship_name, "
                . "ship_status, ship_type_level_2, "
                . "ship_type_level_4, date_of_build, "
                . "lead_ship_in_series_by_imo, "
                . "flag_name, classification_society, ship_manager, ship_manager_company_code, "
                . "technical_manager, technical_manager_code, noof_aux_engines, noof_main_engines, "
                . "noof_propulsion_units, propeller_type, ship_builder, yard_number, group_beneficial_owner, "
                . "group_beneficial_owner_company_code)";


        // DATABASE query generator. This say, if mysqli_query fails then write the first line. otherwise write the second.
        if (!mysqli_query(Database::$connection, $insert_temp_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't insert records into table : *** " . mysqli_error(Database::$connection));
        }
        $insert_temp_query = NULL;
        unset($insert_temp_query);
        unlink($fileName);
    }

    /**
     * loadMainEngineFile
     * @param type $fileName
     */
    private function loadMainEngineFile($fileName) {

        // Load the data from the csv into the temporary table, using the column names generated on the verify page.
        $insert_temp_query = "LOAD DATA LOCAL INFILE '" . $fileName . "'
                INTO TABLE update_main_engine_tbl
                CHARACTER SET 'latin1'
                COLUMNS TERMINATED BY ','
                ENCLOSED BY '\"' LINES TERMINATED BY '\r\n'
                IGNORE 1 LINES (lrno, position, "
                . "engine_designer, engine_builder, "
                . "engine_model, noof_cylinders, "
                . "bore, stroke, cylinder_configuration)";


        // DATABASE query generator. This say, if mysqli_query fails then write the first line. otherwise write the second.
        if (!mysqli_query(Database::$connection, $insert_temp_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't insert main engine records into table : *** " . mysqli_error(Database::$connection));
        }
        $insert_temp_query = NULL;
        unset($insert_temp_query);
        unlink($fileName);
    }

    /**
     * loadAuxEngineFile
     * @param type $fileName
     */
    private function loadAuxEngineFile($fileName) {

        // Load the data from the csv into the temporary table, using the column names generated on the verify page.
        $insert_temp_query = "LOAD DATA LOCAL INFILE '" . $fileName . "'
                INTO TABLE update_aux_engine_tbl
                CHARACTER SET 'latin1'
                COLUMNS TERMINATED BY ','
                ENCLOSED BY '\"' LINES TERMINATED BY '\r\n'
                IGNORE 1 LINES (lrno, engine_sequence, engine_builder, engine_designer, "
                . "engine_model, noof_cylinders, "
                . "bore, stroke)";


        // DATABASE query generator. This say, if mysqli_query fails then write the first line. otherwise write the second.
        if (!mysqli_query(Database::$connection, $insert_temp_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't insert aux records into table : *** " . mysqli_error(Database::$connection));
        }
        $insert_temp_query = NULL;
        unset($insert_temp_query);
        unlink($fileName);
    }

    /**
     * loadSurveyDateHistoryFile
     * @param type $fileName
     */
    private function loadSurveyDateHistoryFile($fileName) {

        // Load the data from the csv into the temporary table, using the column names generated on the verify page.
        $insert_temp_query = "LOAD DATA LOCAL INFILE '" . $fileName . "'
                INTO TABLE update_survey_date_history_tbl
                CHARACTER SET 'latin1'
                COLUMNS TERMINATED BY ','
                ENCLOSED BY '\"' LINES TERMINATED BY '\r\n'
                IGNORE 1 LINES (lrno, special_survey)";


        // DATABASE query generator. This say, if mysqli_query fails then write the first line. otherwise write the second.
        if (!mysqli_query(Database::$connection, $insert_temp_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't insert sdh records into table : *** " . mysqli_error(Database::$connection));
        }
        $insert_temp_query = NULL;
        unset($insert_temp_query);
        unlink($fileName);
    }

    /**
     * loadSurveyDateFile
     * @param type $fileName
     */
    private function loadSurveyDateFile($fileName) {

        // Load the data from the csv into the temporary table, using the column names generated on the verify page.
        $insert_temp_query = "LOAD DATA LOCAL INFILE '" . $fileName . "'
                INTO TABLE update_survey_date_tbl
                CHARACTER SET 'latin1'
                COLUMNS TERMINATED BY ','
                ENCLOSED BY '\"' LINES TERMINATED BY '\r\n'
                IGNORE 1 LINES (lrno, special_survey)";


        // DATABASE query generator. This say, if mysqli_query fails then write the first line. otherwise write the second.
        if (!mysqli_query(Database::$connection, $insert_temp_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't insert SD records into table : *** " . mysqli_error(Database::$connection));
        }
        $insert_temp_query = NULL;
        unset($insert_temp_query);
        unlink($fileName);
    }

    /**
     * loadTradingZoneFile
     * @param type $fileName
     */
    private function loadTradingZoneFile($fileName) {

        // Load the data from the csv into the temporary table, using the column names generated on the verify page.
        $insert_temp_query = "LOAD DATA LOCAL INFILE '" . $fileName . "'
                INTO TABLE update_trading_zone_tbl
                CHARACTER SET 'latin1'
                COLUMNS TERMINATED BY ','
                ENCLOSED BY '\"' LINES TERMINATED BY '\r\n'
                IGNORE 1 LINES (lrno, trading_zone, "
                . "last_seen_date)";


        // DATABASE query generator. This say, if mysqli_query fails then write the first line. otherwise write the second.
        if (!mysqli_query(Database::$connection, $insert_temp_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't insert TZ records into table : *** " . mysqli_error(Database::$connection));
        }
        $insert_temp_query = NULL;
        unset($insert_temp_query);
        unlink($fileName);
    }

    /**
     * loadNameHistoryFile
     * @param type $fileName
     */
    private function loadNameHistoryFile($fileName) {

        // Load the data from the csv into the temporary table, using the column names generated on the verify page.
        $insert_temp_query = "LOAD DATA LOCAL INFILE '" . $fileName . "'
                INTO TABLE update_name_history_tbl
                CHARACTER SET 'latin1'
                COLUMNS TERMINATED BY ','
                ENCLOSED BY '\"' LINES TERMINATED BY '\r\n'
                IGNORE 1 LINES (lrno, sequence, "
                . "vessel_name, effective_date)";


        // DATABASE query generator. This say, if mysqli_query fails then write the first line. otherwise write the second.
        if (!mysqli_query(Database::$connection, $insert_temp_query)) {
            debugWriter("loadInstallationDataLogs.txt", "Can't insert Name History records into table : *** " . mysqli_error(Database::$connection));
        }
        $insert_temp_query = NULL;
        unset($insert_temp_query);
        unlink($fileName);
    }

    ///////////////////////
    //
    //   COUNT DATA
    //
    ///////////////////////

    /**
     * countTempCustomerData
     */
    private function countTempCustomerData() {
        // Do a quick count of the new records, This is currently used for debug, but may stay in the code.
        $quickCount = mysqli_query(Database::$connection, "SELECT count(*) FROM update_customer_tbl");
        $countQuick = mysqli_fetch_assoc($quickCount);
        print "<p>Customer table Count: " . $countQuick['count(*)'] . ". Before duplicates are removed</p>";
        unset($quickCount);
        unset($countQuick);
    }

    /**
     * countTempShipData
     */
    private function countTempShipData() {

        // Do a quick count of the new records, This is currently used for debug, but may stay in the code.
        $quickCount = mysqli_query(Database::$connection, "SELECT count(*) FROM update_ship_data_tbl");
        $countQuick = mysqli_fetch_assoc($quickCount);
        print "<p>Ship table Count: " . $countQuick['count(*)'] . ". Before duplicates are removed</p>";
        unset($quickCount);
        unset($countQuick);
    }

    /**
     * countMainEngineData
     */
    private function countMainEngineProductData() {
        // Do a quick count of the new records, This is currently used for debug, but may stay in the code.
        $quickCount = mysqli_query(Database::$connection, "SELECT count(*) FROM update_main_engine_tbl");
        $countQuick = mysqli_fetch_assoc($quickCount);
        print "<p>Main Engine table Count: " . $countQuick['count(*)'] . ". Before duplicates are removed</p>";
        unset($quickCount);
        unset($countQuick);
    }

    /**
     * countAuxEngineData
     */
    private function countAuxEngineData() {
        // Do a quick count of the new records, This is currently used for debug, but may stay in the code.
        $quickCount = mysqli_query(Database::$connection, "SELECT count(*) FROM update_aux_engine_tbl");
        $countQuick = mysqli_fetch_assoc($quickCount);
        print "<p>Aux Engine table Count: " . $countQuick['count(*)'] . ". Before duplicates are removed</p>";
        unset($quickCount);
        unset($countQuick);
    }

    /**
     * countSurveyDateHistoryData
     */
    private function countSurveyDateHistoryData() {
        // Do a quick count of the new records, This is currently used for debug, but may stay in the code.
        $quickCount = mysqli_query(Database::$connection, "SELECT count(*) FROM update_survey_date_history_tbl");
        $countQuick = mysqli_fetch_assoc($quickCount);
        print "<p>Survey Date History table Count: " . $countQuick['count(*)'] . ". Before duplicates are removed</p>";
        unset($quickCount);
        unset($countQuick);
    }

    /**
     * countSurveyDateData
     */
    private function countSurveyDateData() {
        // Do a quick count of the new records, This is currently used for debug, but may stay in the code.
        $quickCount = mysqli_query(Database::$connection, "SELECT count(*) FROM update_survey_date_tbl");
        $countQuick = mysqli_fetch_assoc($quickCount);
        print mysqli_error(Database::$connection);
        print "<p>Survey Date table Count: " . $countQuick['count(*)'] . ". Before duplicates are removed</p>";
        unset($quickCount);
        unset($countQuick);
    }

    /**
     * countTradingZoneData
     */
    private function countTradingZoneData() {
        // Do a quick count of the new records, This is currently used for debug, but may stay in the code.
        $quickCount = mysqli_query(Database::$connection, "SELECT count(*) FROM update_trading_zone_tbl");
        $countQuick = mysqli_fetch_assoc($quickCount);
        print "<p>Trading Zone table Count: " . $countQuick['count(*)'] . ". Before duplicates are removed</p>";
        unset($quickCount);
        unset($countQuick);
    }

    /**
     * countNameHistoryData
     */
    private function countNameHistoryData() {
        // Do a quick count of the new records, This is currently used for debug, but may stay in the code.
        $quickCount = mysqli_query(Database::$connection, "SELECT count(*) FROM update_name_history_tbl");
        $countQuick = mysqli_fetch_assoc($quickCount);
        print "<p>Name History table Count: " . $countQuick['count(*)'] . ". Before duplicates are removed</p>";
        unset($quickCount);
        unset($countQuick);
    }

    ///////////////////////////
    //
    //   CREATE RECORD ARRAYS
    //
    ///////////////////////////

    /**
     * getNewCustomerRecord
     * @return \Customer
     */
    private function getNewCustomerRecord($rowNum) {

        $result = mysqli_query(Database::$connection, "SELECT temp_id, owcode, short_company_name, 
                telephone, website, company_status, 
                full_company_name FROM update_customer_tbl LIMIT $rowNum, 10000");
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['temp_id']] = new Customer("", trim($row['short_company_name']), preg_replace('/\s+/', '', trim($row['telephone'])), "", trim($row['website']), "", "0", "0", "0", "0", "", trim($row['owcode']), "0", NULL, trim($row['company_status']), trim($row['full_company_name']), "");
        }
        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }

    /**
     * getNewCustomerAddressRecord
     * @return \CustomerAddress
     */
    private function getNewCustomerAddressRecord($rowNum) {

        $result = mysqli_query(Database::$connection, "SELECT temp_id, owcode, country_name, town_name,   
                full_address, customer_id FROM update_customer_tbl LIMIT $rowNum, 10000");
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['temp_id']] = new CustomerAddress("", "Office", "", trim($row['town_name']), "", "", trim($row['country_name']), "", "0", trim($row['customer_id']), "0", "1", trim($row["full_address"]));
        }

        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }

    /**
     * getNewCustomerContactRecord
     * @return \CustomerContact
     */
    private function getNewCustomerContactRecord($rowNum) {

        $result = mysqli_query(Database::$connection, "SELECT temp_id, owcode, email, customer_id, customer_address_id FROM update_customer_tbl LIMIT $rowNum, 10000");
        while ($row = mysqli_fetch_assoc($result)) {
            $array[$row['temp_id']] = new CustomerContact("", "", "", "", "", trim($row['email']), "", "", "SeaWeb", 0, 0, trim($row['customer_id']), trim($row['customer_address_id']));
        }

        if (isset($array)) {
            return $array;
        } else {
            return null;
        }
    }

    /**
     * getShipDataRecords
     */
    private function getShipDataRecords($rowNum) {
        $result = mysqli_query(Database::$connection, "SELECT temp_id, lrimo, ship_name, "
                . "ship_status, ship_type_level_2, "
                . "ship_type_level_4, date_of_build, "
                . "lead_ship_in_series_by_imo, "
                . "flag_name, classification_society, ship_manager_company_code, "
                . " technical_manager_code, "
                . "noof_propulsion_units, propeller_type, ship_builder, yard_number, group_beneficial_owner, "
                . "group_beneficial_owner_company_code FROM update_ship_data_tbl "
                . "LIMIT $rowNum, 10000");
        while ($row = mysqli_fetch_assoc($result)) {
            $dateOfBuild = date("Y-m-d H:i:s", time());
            if (isset($row['date_of_build'])) {
                $year = substr($row['date_of_build'], 0, 4);
                $month = substr($row['date_of_build'], 4, 2);
                if (($month == "00") || (intval($month) > 12)) {
                    $month = "01";
                }
                $dateOfBuild = $year . "-" . $month . "-01 00:00:00";
            }
            $shipManagerID = "";
            if ($row['ship_manager_company_code'] != "") {
                $shipManagerQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl WHERE cust_seaweb_code = '" . addslashes($row['ship_manager_company_code']) . "'");
                $smRow = mysqli_fetch_assoc($shipManagerQuery);
                if (isset($smRow['cust_id'])) {
                    $shipManagerID = $smRow['cust_id'];
                }
            }

            $defaultAddressID = "";
            if ($shipManagerID != "") {
                $defaultAddressQuery = mysqli_query(Database::$connection, "SELECT cadt_id FROM customer_address_tbl "
                        . "WHERE customer_tbl_cust_id = '" . $shipManagerID . "' "
                        . "AND cadt_default_address = '1'");
                $defaultAddressRow = mysqli_fetch_assoc($defaultAddressQuery);
                if (isset($defaultAddressRow['cadt_id'])) {
                    $defaultAddressID = $defaultAddressRow['cadt_id'];
                }
            }

            $technicalManagerID = "";
            if ($row['technical_manager_code'] != "") {
                $techManagerQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl WHERE cust_seaweb_code = '" . addslashes($row['technical_manager_code']) . "'");
                $tmRow = mysqli_fetch_assoc($techManagerQuery);

                if (isset($smRow['cust_id'])) {
                    $technicalManagerID = $tmRow['cust_id'];
                }
            }

            $groupBeneficialOwnerID = "";
            if ($row['group_beneficial_owner_company_code'] != "") {
                $gboQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl WHERE cust_seaweb_code = '" . addslashes($row['group_beneficial_owner_company_code']) . "'");
                $gpRow = mysqli_fetch_assoc($gboQuery);

                if (isset($gpRow['cust_id'])) {
                    $groupBeneficialOwnerID = $gpRow['cust_id'];
                }
            }

            $status = "5";
            if (isset($row['ship_status'])) {
                // this value needs to be taken from the loop-up table.
                $statusQuery = mysqli_query(Database::$connection, "SELECT stat_id FROM installation_status_tbl WHERE stat_type = 'MARINE'
                                        AND MATCH (stat_status_description)
                                        AGAINST ('" . addslashes($row['ship_status']) . "' IN NATURAL LANGUAGE MODE)");

                $statusRow = mysqli_fetch_assoc($statusQuery);

                if (isset($statusRow['stat_id'])) {
                    $status = $statusRow['stat_id'];
                }
            }
            $array[$row['temp_id']] = new Installation("", "MARINE", trim($row['ship_type_level_2']), trim($row['ship_name']), trim($row['lrimo']), 0, $technicalManagerID, $groupBeneficialOwnerID, $status, "SeaWeb", "", "", "", "", trim($row['yard_number']), "", "", "", "", $dateOfBuild, "", "", "", trim($row['propeller_type']), trim($row['noof_propulsion_units']), trim($row['ship_builder']), "", NULL, 0, "", $defaultAddressID, trim($row['flag_name']), trim($row['lead_ship_in_series_by_imo']), trim($row['ship_type_level_4']), trim($row['classification_society']));
        }
        return $array;
    }

    /**
     * getMainEngineRecords
     */
    private function getMainEngineRecords() {


        $result = mysqli_query(Database::$connection, "SELECT * FROM update_main_engine_tbl");
        while ($row = mysqli_fetch_assoc($result)) {
            // get installation ID
            $instQuery = mysqli_query(Database::$connection, "SELECT inst_id FROM installations_tbl WHERE inst_imo = '" . $row['lrno'] . "'");
            $instRow = mysqli_fetch_assoc($instQuery);

            $engineModel = "Not Specified";
            if ($row['engine_model'] != "") {
                $engineModel = trim($row['engine_model']);
            }

            $engineDesigner = strtoupper($row['engine_designer']);
            if ($engineDesigner == "MAN-B&W") {
                $engineDesigner = "MAN B&W";
            }

            if ($engineDesigner == "") {
                $engineDesigner = "UNKNOWN";
            }


            $array[$row['temp_id']] = new SeaWebMainEngine($row['lrno'], $row['position'], $engineDesigner, trim($row['engine_builder']), $engineModel, $row['noof_cylinders'], $row['bore'], $row['stroke'], '', $instRow['inst_id']); //$row['cylinder_configuration'],
        }

        return $array;
    }

    /**
     * getVersionIDOfEngine
     * @param type $engineDesigner
     * @param type $engineModel
     * @param type $noofCylinders
     * @param type $config
     */
    private function getVersionIDOfEngine($engineDesigner, $engineModel, $noofCylinders, $config, $aux) {
        $versionID = "";
        $seriesID = "";
        $sourceDesignerID = ""; // this is the original designer ID as found from the seaweb file, just in case there is no direct series match and it is found in the alias table.
        // MAN-B&W is a pain, rather than having to change the differences in the LAND or MARINE files I just clean it here.
        if ($engineDesigner == "MAN-B&W") {
            $engineDesigner = str_replace("-", " ", $engineDesigner);
        }
        if ($engineDesigner == "") {
            $engineDesigner = "Unknown";
        }
        // search designer table for a match.
        $designerQueryString = "SELECT dlt_id FROM designer_lookup_tbl WHERE "
                . "dlt_designer_description = '" . addslashes($engineDesigner) . "'";

        $designerQuery = mysqli_query(Database::$connection, $designerQueryString);
        $designerRow = mysqli_fetch_assoc($designerQuery);

        $designerID = $designerRow['dlt_id'];



        // there was no designer clean the designer name and try again.
        if ($designerID == "") {

            $cleanedDesigner = str_replace($this->replace, "", $engineDesigner);

            $designerQueryString2 = "SELECT dlt_id FROM designer_lookup_tbl WHERE "
                    . "dlt_designer_description = '" . $cleanedDesigner . "'";
            $designerQuery2 = mysqli_query(Database::$connection, $designerQueryString2);
            $designerRow2 = mysqli_fetch_assoc($designerQuery2);

            $designerID = $designerRow2['dlt_id'];
        }



        // if the model is empty then mark it as not specified.
        if (($engineModel == "") || (substr($engineModel, 0, 2) == "..")) {
            $engineModel = "Not Specified";
        }


        // something has been found for the designer
        if ($designerID != "") { // could still be a brand new designer. SEE } ELSE { BELOW
            $sourceDesignerID = $designerID; // this is set so that there is a historic record
            // find out if the source designer exists in the alias table.
            $aliasMatchString = "SELECT * FROM alias_tbl WHERE ali_designer_id = '" . $sourceDesignerID . "'";
            $aliasMatch = mysqli_query(Database::$connection, $aliasMatchString);
            $aliasRow = mysqli_fetch_assoc($aliasMatch);

            if (isset($aliasRow['designer_lookup_tbl_dlt_id'])) {
                // the alias is now the designer.
                $designerID = $aliasRow['designer_lookup_tbl_dlt_id'];
            }

            // debugWriter("debug.txt", "DESIGNER ID ".$designerID);
            // debugWriter("debug.txt", "ORIGINAL DESIGNER ID ".$sourceDesignerID);
            // find any series that match the engine designer. There are aliasses for designers so check these as well.
            $seriesMatchString = "SELECT mlt_id, mlt_series FROM series_lookup_tbl AS s  
                WHERE s.designer_lookup_tbl_dlt_id = '$designerID' 
                AND '" . addslashes($engineModel) . "' 
                LIKE CONCAT ('%',s.mlt_series,'%')";



            $seriesMatch = mysqli_query(Database::$connection, $seriesMatchString);

            // going to check the percentage of match for each series so we get the right one.
            $greatestMatchSeriesID = 0;
            $greatestPercentage = 0;
            while ($seriesRow = mysqli_fetch_assoc($seriesMatch)) {
                // this works out the percentage match
                similar_text(addslashes($engineModel), $seriesRow['mlt_series'], $percent);
                //   debugWriter("debug.txt", "DES: ".$designerID." Desc: ".addslashes($mainEngineQueryRow['iet_model_description'])." SERIES: ". $seriesRow['mlt_series']." PERCENT: ".$percent);
                // mark the best match here.
                if ($percent >= $greatestPercentage) {
                    $greatestPercentage = $percent;
                    $greatestMatchSeriesID = $seriesRow['mlt_id'];
                }
            }

            // check if any match was found, if not return to the original designer and get their models
            if ($greatestMatchSeriesID == 0) {
                //      debugWriter("debug.txt", "original designer is required. ".$sourceDesignerID);
                $designerID = $sourceDesignerID;
                $seriesMatchString = "SELECT mlt_id, mlt_series FROM series_lookup_tbl AS s  
                    WHERE s.designer_lookup_tbl_dlt_id = '$designerID' 
                    AND '" . addslashes($engineModel) . "' 
                    LIKE CONCAT ('%',s.mlt_series,'%')";

                $seriesMatch = mysqli_query(Database::$connection, $seriesMatchString);

                // going to check the percentage of match for each series so we get the right one.
                $greatestMatchSeriesID = 0;
                $greatestPercentage = 0;
                while ($seriesRow1 = mysqli_fetch_assoc($seriesMatch)) {
                    similar_text(addslashes($engineModel), $seriesRow1['mlt_series'], $percent);
                    //debugWriter("debug.txt", "DES: ".$sourceDesignerID." Desc: ".addslashes($mainEngineQueryRow['iet_model_description'])." SERIES: ". $seriesRow1['mlt_series']." PERCENT: ".$percent);
                    // mark the best match here.
                    if ($percent >= $greatestPercentage) {
                        $greatestPercentage = $percent;
                        $greatestMatchSeriesID = $seriesRow1['mlt_id'];
                    }
                }
            }
            $configString = "";
            if ($aux == false) {
                $configString = "AND vlt_cylinder_configuration = '" . $config . "'";
            }
            //   debugWriter("debug.txt", "The Winner is: " . $greatestMatchSeriesID . " with " . $greatestPercentage);
            //while ($seriesRow = mysqli_fetch_assoc($seriesMatch)) {
            // We have some series IDs and we need to check which version to use.
            // Look at the version table to see if there is a match for the cylinder count and config for that match.
            if ($greatestMatchSeriesID != 0) {
                $versionQuery = mysqli_query(Database::$connection, "SELECT vlt_id FROM version_lookup_tbl WHERE "
                        . "model_lookup_tbl_mlt_id = '" . $greatestMatchSeriesID . "' AND "
                        . "vlt_cylinder_count = '" . $noofCylinders . "' "
                        . "$configString");

                $versionRow = mysqli_fetch_assoc($versionQuery);
                $versionID = $versionRow['vlt_id'];

                // debugWriter("debug.txt", "version ID ".$versionID);
                // a version ID has been chosen and it is not blank so set the series ID to whichever
                // one caused the version to be found.
                if (isset($versionID) && ($versionID != "")) {
                    $seriesID = $greatestMatchSeriesID;
                } else { // no version ID was found. Do I have enough information to make a new one?
                    // check if the cylinder count and config are present. If they are then we can make a new version.
                    if (($noofCylinders != '0') && ($config != '0')) {
                        $versionID = $this->createNewVersion($greatestMatchSeriesID, $noofCylinders, $config);
                        if (isset($versionID) && $versionID != "") {
                            $seriesID = $greatestMatchSeriesID;
                        }
                    } else { // couldnt make a new record out of the count and config so look for an N/A version. 
                        // If this doesnt exist then please make one.
                        $versionQuery = mysqli_query(Database::$connection, "SELECT vlt_id FROM version_lookup_tbl WHERE "
                                . "model_lookup_tbl_mlt_id = '" . $greatestMatchSeriesID . "' AND "
                                . "vlt_cylinder_count = '0' "
                                . "AND vlt_cylinder_configuration = 'A'");

                        $versionRow = mysqli_fetch_assoc($versionQuery);
                        $versionID = $versionRow['vlt_id'];

                        if (isset($versionID) && ($versionID != "")) {
                            $seriesID = $greatestMatchSeriesID;
                        } else {

                            $versionID = $this->createNewVersion($greatestMatchSeriesID, "", "");
                            if (isset($versionID) && $versionID != "") {
                                $seriesID = $greatestMatchSeriesID;
                            }
                        }
                    }
                }
            } else {

                // No series ID was found for the given model
                // find the generic series ID.
                $seriesMatchTwo = mysqli_query(Database::$connection, "SELECT mlt_id FROM series_lookup_tbl "
                        . "WHERE designer_lookup_tbl_dlt_id = '$designerID' "
                        . "AND mlt_series = 'Not Specified'");


                $seriesRowTwo = mysqli_fetch_assoc($seriesMatchTwo);


                if (isset($seriesRowTwo) && $seriesRowTwo['mlt_id'] != "") {

                    if (($noofCylinders != '0') && ($config != '0')) {
                        $versionQueryOne = mysqli_query(Database::$connection, "SELECT * FROM version_lookup_tbl WHERE "
                                . "model_lookup_tbl_mlt_id = '" . $seriesRowTwo['mlt_id'] . "' AND "
                                . "vlt_cylinder_count = '" . $noofCylinders . "' "
                                . "AND vlt_cylinder_configuration = '" . $config . "'");

                        $versionRowOne = mysqli_fetch_assoc($versionQueryOne);
                        $versionID = $versionRowOne['vlt_id'];

                        if (empty($versionID)) { // make one because its important to have the values.
                            $versionID = $this->createNewVersion($seriesRowTwo['mlt_id'], $noofCylinders, $config);
                            if (isset($versionID) && $versionID != "") {
                                $seriesID = $seriesRowTwo['mlt_id'];
                            }
                        }
                    } else {

                        $versionQueryOne = mysqli_query(Database::$connection, "SELECT vlt_id FROM version_lookup_tbl WHERE "
                                . "model_lookup_tbl_mlt_id = '" . $seriesRowTwo['mlt_id'] . "' AND "
                                . "vlt_cylinder_count = '0' "
                                . "AND vlt_cylinder_configuration = 'A'");

                        $versionRowOne = mysqli_fetch_assoc($versionQueryOne);
                        $versionID = $versionRowOne['vlt_id'];


                        $seriesID = $seriesRowTwo['mlt_id'];
                    }
                }


                if ($versionID == "" && $seriesID != "") {

                    $versionID = $this->createNewVersion($seriesID, $noofCylinders, $config);
                } else if ($versionID == "" && $seriesID == "") {
                    $insertSeries = $this->createNewSeries($designerID, "Not Specified"); // create a new series

                    if (isset($insertSeries) && $insertSeries != "") { // create a new version
                        $seriesID = $insertSeries;
                        $versionID = $this->createNewVersion($seriesID, $noofCylinders, $config);
                    }
                }
            }
        } else { // else there is no designer and a new one needs adding.
            // debugWriter("debug.txt", "MAKING A NEW DESIGNER " . $mainEngineQueryRow['dlt_designer_description']);
            $insertDesigner = $this->createNewDesigner(addslashes($engineDesigner));

            if (isset($insertDesigner) && $insertDesigner != "") {
                $designerID = $insertDesigner; // designer created

                $insertSeries = $this->createNewSeries($designerID, addslashes($engineModel));

                if (isset($insertSeries) && $insertSeries != "") {
                    $seriesID = $insertSeries;
                    $versionID = $this->createNewVersion($seriesID, $noofCylinders, $config);
                }
            }
        }
        $array['versionID'] = $versionID;
        $array['aliasID'] = $sourceDesignerID;
        return $array;
    }

    /**
     * createNewDesigner REUSABLE CODE
     * @param type $designerDescription
     * @return type
     */
    private function createNewDesigner($designerDescription) {
        if ($designerDescription != "") {
            $insertDesigner = "INSERT INTO designer_lookup_tbl (dlt_designer_description) "
                    . "VALUES ('" . addslashes($designerDescription) . "')";

            if (!(mysqli_query(Database::$connection, $insertDesigner))) {
                debugWriter("debug.txt", "create new designer Failed " . mysqli_error(Database::$connection) . "\r\n");
                return null;
            } else {
                $designerID = mysqli_insert_id(Database::$connection); // designer created
                return $designerID;
            }
        } else {
            // debugWriter("debug.txt", "create new designer Failed : Designer was empty\r\n");
            return null;
        }
    }

    /**
     * createNewSeries
     * @param type $designerID
     * @param type $modelDescription
     * @return type
     */
    private function createNewSeries($designerID, $modelDescription) {
        $seriesName = "Not Specified";
        if ($modelDescription != "") {
            $seriesName = $modelDescription;
        }
        // create the series
        $insertSeries = "INSERT INTO series_lookup_tbl (mlt_series, "
                . "designer_lookup_tbl_dlt_id, type_of_product_lookup_tbl_toplt_id) VALUES "
                . "('" . addslashes($seriesName) . "', '" . $designerID . "', '1')";

        //debugWriter("debug.txt", "series string " . $insertSeries);
        if (!mysqli_query(Database::$connection, $insertSeries)) {
            debugWriter("debug.txt", "could not add new series ID " . mysqli_error(Database::$connection));
            print "<span style='color:purple;'>Error creating a new 'Not Specified' series record for designer ID " . $designerID . "</span><br/>";
            return null;
        } else {
            $seriesID = mysqli_insert_id(Database::$connection);
            return $seriesID;
        }
    }

    /**
     * createNewVersion
     * @param type $seriesID
     * @param type $cylCount
     * @param type $cylConfiguration
     */
    private function createNewVersion($seriesID, $cylCount, $cylConfiguration) {
        $versionDescription = 'N/A';
        $cylinderCount = '0';
        $configuration = 'A';

        if (($cylCount != "") && ($cylCount != "0")) {
            $cylinderCount = $cylCount;
        }
        if (($cylConfiguration != "") && ($cylConfiguration != "0")) {
            $configuration = $cylConfiguration;
        }
        if (($cylCount != "") && ($cylCount != "0") && ($cylConfiguration != "") && ($cylConfiguration != "0")) {
            $versionDescription = $cylCount . "" . $cylConfiguration;
        }

        $insertVersion = "INSERT INTO version_lookup_tbl (vlt_version_description, "
                . "model_lookup_tbl_mlt_id, vlt_cylinder_count, vlt_cylinder_configuration) VALUES "
                . "('" . addslashes($versionDescription) . "', '" . $seriesID . "', '$cylinderCount', '$configuration')";

        if (!mysqli_query(Database::$connection, $insertVersion)) {
            debugWriter("debug.txt", "could not add new version ID - $insertVersion - " . mysqli_error(Database::$connection));
            print "<span style='color:red;'>Error creating a new 'N/A' version record for series ID " . $seriesID . "</span><br/>";
            return null;
        } else {
            $versionID = mysqli_insert_id(Database::$connection);
            return $versionID;
        }
    }

    /**
     * getAuxEngineRecords
     */
    private function getAuxProductRecords() {
        $result = mysqli_query(Database::$connection, "SELECT * FROM update_aux_engine_tbl");
        while ($row = mysqli_fetch_assoc($result)) {
            // get installation ID
            $instQuery = mysqli_query(Database::$connection, "SELECT inst_id FROM installations_tbl WHERE inst_imo = '" . $row['lrno'] . "'");
            $instRow = mysqli_fetch_assoc($instQuery);

            $engineModel = "Not Specified";
            if ($row['engine_model'] != "") {
                $engineModel = trim($row['engine_model']);
            }
            if ($engineModel == "") {
                $engineModel = "Not Specified";
            }

            $engineDesigner = strtoupper($row['engine_designer']);
            if (strcasecmp($engineDesigner, "MAN-B&W") == 0) {
                $engineDesigner = "MAN B&W";
            }

            if ($engineDesigner == "") {
                $engineDesigner = "UNKNOWN";
            }


            $array[$row['temp_id']] = new SeaWebMainEngine($row['lrno'], $row['engine_sequence'], $engineDesigner, trim($row['engine_builder']), $engineModel, $row['noof_cylinders'], $row['bore'], $row['stroke'], '', $instRow['inst_id']); //'-', 
        }

        return $array;
    }

    /**
     * getSurveyDateHistoryRecords
     */
    private function getSurveyDateHistoryRecords() {
        $result = mysqli_query(Database::$connection, "SELECT * FROM update_survey_date_history_tbl");
        while ($row = mysqli_fetch_array($result)) {
            $instQuery = mysqli_query(Database::$connection, "SELECT inst_id FROM installations_tbl WHERE inst_imo = '" . $row['lrno'] . "'");
            $instRow = mysqli_fetch_assoc($instQuery);

            $array[$row['temp_id']] = new SpecialSurveyHistoryDates("", $row['special_survey'], $instRow['inst_id'], $row['lrno']);
        }

        return $array;
    }

    /**
     * getSurveyDateRecords
     */
    private function getSurveyDateRecords() {
        $result = mysqli_query(Database::$connection, "SELECT * FROM update_survey_date_tbl");
        while ($row = mysqli_fetch_array($result)) {
            $instQuery = mysqli_query(Database::$connection, "SELECT inst_id FROM installations_tbl WHERE inst_imo = '" . $row['lrno'] . "'");
            $instRow = mysqli_fetch_assoc($instQuery);

            $array[$row['temp_id']] = new SpecialSurveyDue("", $row['special_survey'], $instRow['inst_id'], $row['lrno']);
        }

        return $array;
    }

    /**
     * getTradingZoneRecords
     */
    private function getTradingZoneRecords() {
        $result = mysqli_query(Database::$connection, "SELECT * FROM update_trading_zone_tbl");
        while ($row = mysqli_fetch_array($result)) {
            $instQuery = mysqli_query(Database::$connection, "SELECT inst_id FROM installations_tbl WHERE inst_imo = '" . $row['lrno'] . "'");
            $instRow = mysqli_fetch_assoc($instQuery);

            $array[$row['temp_id']] = new TradingZoneLastSeen("", $row['trading_zone'], $row['last_seen_date'], $instRow['inst_id'], $row['lrno']);
        }

        return $array;
    }

    /**
     * getNameHistoryRecords
     */
    private function getNameHistoryRecords() {
        $result = mysqli_query(Database::$connection, "SELECT * FROM update_name_history_tbl");
        while ($row = mysqli_fetch_array($result)) {
            $instQuery = mysqli_query(Database::$connection, "SELECT inst_id FROM installations_tbl WHERE inst_imo = '" . $row['lrno'] . "'");
            $instRow = mysqli_fetch_assoc($instQuery);

            $array[$row['temp_id']] = new ShipNameHistory("", $row['lrno'], $row['sequence'], $row['vessel_name'], $row['effective_date'], $instRow['inst_id']);
        }

        return $array;
    }

    ///////////////////////////
    //
    //   CHECK AGAINST DB
    //
    ///////////////////////////

    /**
     * checkNewRecordAgainstDB
     * @param type $array
     */
    private function checkCustomerRecordAgainstDB($array) {
        $customerSQLConstructor = new CustomerSQLConstructor();

        $counta = 0;
        $countb = 0;

        foreach ($array['customer'] as $key => $customer) {
            $result = mysqli_query(Database::$connection, "SELECT * FROM customer_tbl WHERE cust_seaweb_code = '" . $customer->seawebCode . "'");
            $row = mysqli_fetch_assoc($result);
            //print "size of array ".sizeof($row);
            if (isset($row['cust_seaweb_code']) && ($row['cust_seaweb_code'] != "")) {
                $currentCustomer = $customerSQLConstructor->createCustomer($row);
                $currentCustomer->mainTelephone = preg_replace('/\s+/', '', trim($currentCustomer->mainTelephone));
                $customer->customerID = $currentCustomer->customerID;
                $customer->navisionID = $currentCustomer->navisionID;
                $customer->parentSeawebCode = "0";
                $currentCustomer->parentSeawebCode = "0";
                $currentCustomer->companyFullName = trim($currentCustomer->companyFullName);

                if (compareObjects($customer, $currentCustomer) == 'TRUE') {
                    $update = "UPDATE update_customer_tbl SET customer_duplicate = '1', "
                            . "customer_id = '" . $currentCustomer->customerID . "'  WHERE temp_id = '$key'";
                    $counta++;
                    if (!mysqli_query(Database::$connection, $update)) {
                        debugWriter("debug.txt", "UPDATE AS DUPLICATE FAILED " . mysqli_error(Database::$connection));
                    }
                } else {
                    /*print "<pre>Customer<br/>";
                    print_r($customer);
                    print "<br/>Current Customer<br/>";
                    print_r($currentCustomer);
                    print "</pre>";*/
                    $update = "UPDATE update_customer_tbl SET customer_exists = '1', "
                            . "customer_id = '" . $currentCustomer->customerID . "'  WHERE temp_id = '$key'";
                    $countb++;

                    if (!mysqli_query(Database::$connection, $update)) {
                        debugWriter("debug.txt", "UPDATE AS EXISTS FAILED " . mysqli_error(Database::$connection));
                    }
                }
            }
        }
        $count = array("duplicate" => $counta, "exists" => $countb);
        return $count;
    }

    /**
     * checkAddressAgainstDB
     * @param type $array
     */
    private function checkAddressAgainstDB($array) {
        $customerAddressSQLConstructor = new CustomerAddressSQLConstructor();
        $counta = 0;
        $countb = 0;
        foreach ($array['address'] as $key => $address) {
            if ($address->country == "China, People's Republic of") {
                $address->country = "China";
            }

            if (($address->country == "Eire") || ($address->country == "Republic of Ireland" ) || ($address->country == "Irish Republic") || ($address->country == "Ireland")) {
                $address->country = "Eire / Republic of Ireland";
                
            }

            $result = mysqli_query(Database::$connection, "SELECT * FROM customer_address_tbl "
                    . "WHERE customer_tbl_cust_id = '" . $address->customerID . "'");
            $row = mysqli_fetch_assoc($result);
            if (isset($row['customer_tbl_cust_id']) && ($row['customer_tbl_cust_id'] != "")) {
                $currentCustomerAddress = $customerAddressSQLConstructor->createCustomerAddress($row);
                $address->addressID = $currentCustomerAddress->addressID;

                if (compareObjects($address, $currentCustomerAddress) == 'TRUE') {
                    $update = "UPDATE update_customer_tbl SET address_duplicate = '1', "
                            . "customer_address_id = '" . $currentCustomerAddress->addressID . "'  WHERE temp_id = '$key'";
                    $counta++;
                    if (!mysqli_query(Database::$connection, $update)) {
                        debugWriter("debug.txt", "UPDATE AS DUPLICATE FAILED " . mysqli_error(Database::$connection));
                    }
                } else {
                    /*print "<pre>Customer<br/>";
                    print_r($address);
                    print "<br/>Current Customer<br/>";
                    print_r($currentCustomerAddress);
                    print "</pre>";*/
                    $update = "UPDATE update_customer_tbl SET address_exists = '1', "
                            . "customer_address_id = '" . $currentCustomerAddress->addressID . "'  WHERE temp_id = '$key'";
                    $countb++;
                    if (!mysqli_query(Database::$connection, $update)) {
                        debugWriter("debug.txt", "UPDATE ADDRESS AS EXISTS FAILED " . mysqli_error(Database::$connection));
                    }
                }
            }
        }
        $count = array("duplicate" => $counta, "exists" => $countb);
        return $count;
    }

    /**
     * checkContactAgainstDB
     * @param type $array
     */
    private function checkContactAgainstDB($array) {
        $customerContactsSQLConstructor = new CustomerContactsSQLConstructor();
        $counta = 0; // duplicate
        $countb = 0; // exists
        foreach ($array['contact'] as $key => $contact) {

            $result = mysqli_query(Database::$connection, "SELECT * FROM customer_contacts_tbl "
                    . "WHERE customer_tbl_cust_id = '" . $contact->customerID . "' "
                    . "AND ict_email = '" . $contact->email . "'");
            $row = mysqli_fetch_assoc($result);
            if (isset($row['customer_tbl_cust_id']) && ($row['customer_tbl_cust_id'] != "")) {
                $currentCustomerContact = $customerContactsSQLConstructor->createCustomerContact($row);
                $contact->customerContactID = $currentCustomerContact->customerContactID;

                if (compareObjects($contact, $currentCustomerContact) == 'TRUE') {
                    $update = "UPDATE update_customer_tbl SET contact_duplicate = '1', "
                            . "customer_contact_id='" . $currentCustomerContact->customerContactID . "' "
                            . "WHERE temp_id = '$key'";
                    $counta++;

                    if (!mysqli_query(Database::$connection, $update)) {
                        debugWriter("debug.txt", "UPDATE AS DUPLICATE FAILED " . mysqli_error(Database::$connection));
                    }
                } else {
                    $update = "UPDATE update_customer_tbl SET contact_exists = '1', "
                            . "customer_contact_id='" . $currentCustomerContact->customerContactID . "' "
                            . "WHERE temp_id = '$key'";
                    $countb++;

                    if (!mysqli_query(Database::$connection, $update)) {
                        debugWriter("debug.txt", "UPDATE CONTACT AS EXISTS FAILED " . mysqli_error(Database::$connection));
                    }
                }
            }
        }
        $count = array("duplicate" => $counta, "exists" => $countb);
        return $count;
    }

    /**
     * checkShipRecordsAgainstDB
     * @param type $array
     * @return int
     */
    private function checkShipRecordsAgainstDB($array) {
        $installationSQLConstructor = new InstallationSQLConstructor();
        $counta = 0;
        $countb = 0;
        foreach ($array as $key => $ship) {
            //print "<pre>";

            $result = mysqli_query(Database::$connection, "SELECT * FROM installations_tbl "
                    . "WHERE inst_imo = '" . $ship->imo . "'");
            $row = mysqli_fetch_assoc($result);



            if (isset($row['inst_imo']) && ($row['inst_imo'] != "")) {
                $currentShip = $installationSQLConstructor->createInstallation($row);

                $ship->installationID = $currentShip->installationID;
                $ship->originalSourceDate = $currentShip->originalSourceDate; // because this only changes if the new record is added.
                debugWriter("debug.txt", "New ".$ship->groupOwner." OLD ".$currentShip->groupOwner);
                $ship->buildYear = $currentShip->buildYear;


                if (compareObjects($ship, $currentShip) == 'TRUE') {
                    $update = "UPDATE update_ship_data_tbl SET ship_duplicate = '1', "
                            . "installation_id='" . $currentShip->installationID . "' "
                            . "WHERE temp_id = '$key'";
                    $counta++;
                    if (!mysqli_query(Database::$connection, $update)) {
                        debugWriter("debug.txt", "UPDATE AS DUPLICATE FAILED " . mysqli_error(Database::$connection));
                    }
                } else {
                    /* print "<pre>";
                      print_r($ship);
                      print_r($currentShip); */

                    $update = "UPDATE update_ship_data_tbl SET ship_exists = '1', "
                            . "installation_id='" . $currentShip->installationID . "' "
                            . "WHERE temp_id = '$key'";
                    $countb++;
                    if (!mysqli_query(Database::$connection, $update)) {
                        debugWriter("debug.txt", "UPDATE CONTACT AS EXISTS FAILED " . mysqli_error(Database::$connection));
                    }
                }
            }
        }
        //print "</pre>";
        $count = array("duplicate" => $counta, "exists" => $countb);
        return $count;
    }

    /**
     * checkMainEngineAgainstDB
     * @param type $array
     */
    private function checkMainEngineAgainstDB($array) {
        // $array is the new data of main engines
        $counta = 0;
        $countb = 0;

        foreach ($array as $tempID => $seawebObject) {

            //debugWriter("debug.txt", "Check main engine seaweb INST ID ".$seawebObject->installationID);
            $result = mysqli_query(Database::$connection, "SELECT i.inst_imo, e.iet_id, e.iet_position_if_main, 
                    e.iet_model_description, e.iet_main_aux, e.iet_bore_size, 
                    e.iet_stroke, e.iet_engine_builder, p.*, v.*, d.dlt_designer_description, 
                    f.dlt_designer_description AS sourcealias
                    FROM product_tbl AS p 
                    LEFT JOIN installation_engine_tbl AS e ON p.prod_id = e.product_tbl_prod_id 
                    LEFT JOIN installations_tbl AS i ON i.inst_id = p.installations_tbl_inst_id 
                    LEFT JOIN version_lookup_tbl AS v ON p.version_lookup_tbl_vlt_id = v.vlt_id
                    LEFT JOIN series_lookup_tbl AS s ON v.model_lookup_tbl_mlt_id = s.mlt_id 
                    LEFT JOIN designer_lookup_tbl AS d ON s.designer_lookup_tbl_dlt_id = d.dlt_id
                    LEFT JOIN designer_lookup_tbl AS f ON p.prod_source_alias_id = f.dlt_id
                    WHERE p.installations_tbl_inst_id = '" . $seawebObject->installationID . "' AND 
                    e.iet_position_if_main = '" . $seawebObject->position . "' AND 
                    s.type_of_product_lookup_tbl_toplt_id = '1'");
            $row = mysqli_fetch_assoc($result);
            if ($row['sourcealias'] != "") {
                $engineDesigner = strtoupper($row['sourcealias']);
            } else {
                $engineDesigner = strtoupper($row['d.dlt_designer_description']);
            }
            if ($engineDesigner == "MAN-B&W") {
                $engineDesigner = str_replace("-", " ", $engineDesigner);
            }
            if ($engineDesigner == "") {
                $engineDesigner = "UNKNOWN";
            }


            $dbSeawebObject = new SeaWebMainEngine(
                    $row['inst_imo'], $row['iet_position_if_main'], $engineDesigner, $row['iet_engine_builder'], $row['iet_model_description'], $row['vlt_cylinder_count'], $row['iet_bore_size'], $row['iet_stroke'], '', $seawebObject->installationID); //$row['vlt_cylinder_configuration'], 



            if (compareObjects($seawebObject, $dbSeawebObject) == 'TRUE') {
                $update = "UPDATE update_main_engine_tbl SET main_engine_duplicate = '1', "
                        . "product_id = '" . $row['prod_id'] . "', inst_engine_id= '" . $row['iet_id'] . "'  "
                        . "WHERE temp_id = '$tempID'";
                $counta++;

                if (!mysqli_query(Database::$connection, $update)) {
                    debugWriter("debug.txt", "UPDATE AS DUPLICATE FAILED " . mysqli_error(Database::$connection));
                }
            } else {
                /* print "<pre>";
                  print_r($seawebObject);
                  print_r($dbSeawebObject);
                  print "</pre>"; */
                $update = "UPDATE update_main_engine_tbl SET main_engine_exists = '1', "
                        . "product_id = '" . $row['prod_id'] . "', inst_engine_id= '" . $row['iet_id'] . "'  "
                        . "WHERE temp_id = '$tempID'";
                $countb++;

                if (!mysqli_query(Database::$connection, $update)) {
                    debugWriter("debug.txt", "UPDATE AS EXISTS FAILED " . mysqli_error(Database::$connection));
                }
            }
            // }
        }
        $count = array("duplicate" => $counta, "exists" => $countb);
        return $count;
    }

    /**
     * checkAuxEngineAgainstDB
     * @param type $array
     * @return int
     */
    private function checkAuxEngineAgainstDB($array) {
        $counta = 0;
        $countb = 0;

        foreach ($array as $tempID => $seawebObject) {

            $result = mysqli_query(Database::$connection, "SELECT i.inst_imo, e.iet_id, e.iet_position_if_main, 
                    e.iet_model_description, e.iet_main_aux, e.iet_bore_size, 
                    e.iet_stroke, e.iet_engine_builder AS builder, p.*, v.*, d.dlt_designer_description AS designer, 
                    f.dlt_designer_description AS sourcealias
                    FROM product_tbl AS p 
                    LEFT JOIN installation_engine_tbl AS e ON p.prod_id = e.product_tbl_prod_id 
                    LEFT JOIN installations_tbl AS i ON i.inst_id = p.installations_tbl_inst_id 
                    LEFT JOIN version_lookup_tbl AS v ON p.version_lookup_tbl_vlt_id = v.vlt_id
                    LEFT JOIN series_lookup_tbl AS s ON v.model_lookup_tbl_mlt_id = s.mlt_id 
                    LEFT JOIN designer_lookup_tbl AS d ON s.designer_lookup_tbl_dlt_id = d.dlt_id
                    LEFT JOIN designer_lookup_tbl AS f ON p.prod_source_alias_id = f.dlt_id
                        WHERE p.installations_tbl_inst_id = '" . $seawebObject->installationID . "' AND "
                    . "e.iet_position_if_main = '" . $seawebObject->position . "'");


            $row = mysqli_fetch_assoc($result);
            if ($row['sourcealias'] != "") {
                $engineDesigner = strtoupper($row['sourcealias']);
            } else {
                $engineDesigner = strtoupper($row['designer']);
            }

            if (strcasecmp($engineDesigner, "MAN-B&W") == 0) {
                $engineDesigner = "MAN B&W";
            }


            $dbSeawebObject = new SeaWebMainEngine(
                    $row['inst_imo'], $row['iet_position_if_main'], $engineDesigner, $row['builder'], $row['iet_model_description'], $row['vlt_cylinder_count'], $row['iet_bore_size'], $row['iet_stroke'], '', $seawebObject->installationID); //"-",

            if (compareObjects($seawebObject, $dbSeawebObject) == 'TRUE') {
                $update = "UPDATE update_aux_engine_tbl SET aux_engine_duplicate = '1', "
                        . "product_id = '" . $row['prod_id'] . "', inst_engine_id= '" . $row['iet_id'] . "'  "
                        . "WHERE temp_id = '$tempID'";
                $counta++;

                if (!mysqli_query(Database::$connection, $update)) {
                    debugWriter("debug.txt", "UPDATE AS DUPLICATE FAILED " . mysqli_error(Database::$connection));
                }
            } else {

                $update = "UPDATE update_aux_engine_tbl SET aux_engine_exists = '1', "
                        . "product_id = '" . $row['prod_id'] . "', inst_engine_id= '" . $row['iet_id'] . "'  "
                        . "WHERE temp_id = '$tempID'";
                $countb++;

                if (!mysqli_query(Database::$connection, $update)) {
                    debugWriter("debug.txt", "UPDATE AS EXISTS FAILED " . mysqli_error(Database::$connection));
                }
            }
        }
        $count = array("duplicate" => $counta, "exists" => $countb);
        return $count;
    }

    //////////////////
    //
    // UPDATE DIRECT TO DB AS ITS JUST REPLACING HISTORIC RECORDS.
    // 
    //////////////////

    /**
     * updateSpecialSurveyDateHistoryAgainstDB
     * This does not require checking against old records, 
     * the old records can just be removed and the new history added.
     * @param type $array
     * @return int
     */
    private function updateSpecialSurveyDateHistoryInDB($array) {
        $count = 0;

        foreach ($array as $key => $ssdh) {

            $deleteOldRecords = "DELETE FROM survey_date_history_tbl "
                    . "WHERE installations_tbl_inst_id = '" . $ssdh->installationID . "'";
            if (!mysqli_query(Database::$connection, $deleteOldRecords)) {
                debugWriter("debug.txt", "Failed to delete old SSDH records " . mysqli_error(Database::$connection));
            } else {
                $date = "";
                if (($ssdh->specialSurveyHistoryDate != "") && ($ssdh->specialSurveyHistoryDate != 'Not recorded')) {


                    if (strpos($ssdh->specialSurveyHistoryDate, "/") !== false) {
                        $exp = explode("/", $ssdh->specialSurveyHistoryDate);
                        $date = $exp[2] . "-" . $exp[1] . "-" . $exp[0];
                    } else {
                        $date = $ssdh->specialSurveyHistoryDate;
                    }

                    if (substr($date, 0, -2) == '00') {
                        $date = substr($date, 0, 8) . "01";
                    }
                }
                $insert = "INSERT INTO survey_date_history_tbl (sdht_special_survey_date, "
                        . "installations_tbl_inst_id, sdht_lrno) VALUES ("
                        . "'" . $date . "', "
                        . "'" . $ssdh->installationID . "', "
                        . "'" . $ssdh->imo . "')";
                if (!mysqli_query(Database::$connection, $insert)) {
                    debugWriter("debug.txt", "Failed to insert new SSDH record (1) $insert " . mysqli_error(Database::$connection));
                } else {
                    $count++;
                }
            }
        }
        return $count;
    }

    /**
     * updateSpecialSurveyDateInDB
     * @param type $array
     * @return int
     */
    private function updateSpecialSurveyDateInDB($array) {
        $count = 0;

        foreach ($array as $key => $ssd) {

            $deleteOldRecords = "DELETE FROM survey_due_date_tbl "
                    . "WHERE installations_tbl_inst_id = '" . $ssd->installationID . "'";
            if (!mysqli_query(Database::$connection, $deleteOldRecords)) {
                debugWriter("debug.txt", "Failed to delete old SSDH records " . mysqli_error(Database::$connection));
            } else {
                $date = "";
                if (($ssd->specialSurveyDueDate != "") && ($ssd->specialSurveyDueDate != 'Not recorded')) {

                    if (strpos($ssd->specialSurveyDueDate, "/") !== false) {
                        $exp = explode("/", $ssd->specialSurveyDueDate);
                        $date = $exp[2] . "-" . $exp[1] . "-" . $exp[0];
                    } else {
                        $date = $ssd->specialSurveyDueDate;
                    }
                    //$exp = explode("/", $ssd->specialSurveyDueDate);
                    //$date = $exp[2]."-".$exp[1]."-".$exp[0];
                    if (substr($date, 0, -2) == '00') {
                        $date = substr($date, 0, 8) . "01";
                    }
                }
                $insert = "INSERT INTO survey_due_date_tbl (sddt_special_survey_date, "
                        . "installations_tbl_inst_id, sddt_lrno) VALUES ("
                        . "'" . $date . "', "
                        . "'" . $ssd->installationID . "', "
                        . "'" . $ssd->imo . "')";
                if (!mysqli_query(Database::$connection, $insert)) {
                    debugWriter("debug.txt", "Failed to insert new SSDH record (2) " . mysqli_error(Database::$connection));
                } else {
                    $count++;
                }
            }
        }
        return $count;
    }

    /**
     * updateTradingZoneInDB
     * @param type $array
     * @return int
     */
    private function updateTradingZoneInDB($array) {
        $count = 0;

        foreach ($array as $key => $tz) {

            $deleteOldRecords = "DELETE FROM trading_zone_last_seen_tbl "
                    . "WHERE installations_tbl_inst_id = '" . $tz->installationID . "'";
            if (!mysqli_query(Database::$connection, $deleteOldRecords)) {
                debugWriter("debug.txt", "Failed to delete old TZ records " . mysqli_error(Database::$connection));
            } else {
                $date = "";
                if (($tz->lastSeenDate != "") && ($tz->lastSeenDate != 'Not recorded')) {
                    /* $exp = explode("/", $tz->lastSeenDate);
                      print "<pre>";
                      print_r($exp);
                      $date = $exp[2]."-".$exp[1]."-".$exp[0]." ".$exp[3]; */
                    $date = $tz->lastSeenDate;
                }
                $insert = "INSERT INTO trading_zone_last_seen_tbl (tzlst_trading_zone, "
                        . "tzlst_last_seen_date, "
                        . "installations_tbl_inst_id, tzlst_lrno) VALUES ("
                        . "'" . addslashes($tz->tradingZone) . "', "
                        . "'" . $date . "', "
                        . "'" . $tz->installationID . "', "
                        . "'" . $tz->LRNO . "')";
                if (!mysqli_query(Database::$connection, $insert)) {
                    debugWriter("debug.txt", "Failed to insert new SSDH record (3) " . mysqli_error(Database::$connection));
                } else {
                    $count++;
                }
            }
        }
        return $count;
    }

    /**
     * updateNameHistoryInDB
     * @param type $array
     * @return int
     */
    private function updateNameHistoryInDB($array) {
        $count = 0;

        foreach ($array as $snh) {

            $deleteOldRecords = "DELETE FROM ship_name_history_tbl "
                    . "WHERE installations_tbl_inst_id = '" . $snh->installationID . "'";
            if (!mysqli_query(Database::$connection, $deleteOldRecords)) {
                debugWriter("debug.txt", "Failed to delete old SNH records " . mysqli_error(Database::$connection));
            } else {
                $date = "";
                if (($snh->shipNameHistoryEffectiveDate != "") && ($snh->shipNameHistoryEffectiveDate != 'Not recorded')) {
                    $year = substr($snh->shipNameHistoryEffectiveDate, 0, 4);
                    $month = substr($snh->shipNameHistoryEffectiveDate, 5, 2);
                    if ($month == "00") {
                        $month = "01";
                    }
                    $date = $year . "-" . $month . "-01 01:00:00";
                }
                $insert = "INSERT INTO ship_name_history_tbl (snht_lrno, "
                        . "snht_sequence, snht_vessel_name, snht_effective_date, "
                        . "installations_tbl_inst_id) VALUES ("
                        . "'" . addslashes($snh->shipNameHistoryIMO) . "', "
                        . "'" . addslashes($snh->shipNameHistorySequence) . "', "
                        . "'" . addslashes($snh->shipNameHistoryVesselName) . "', "
                        . "'" . $date . "', "
                        . "'" . $snh->installationID . "')";

                if (!mysqli_query(Database::$connection, $insert)) {
                    debugWriter("debug.txt", "Failed to insert new SNH record $insert " . mysqli_error(Database::$connection));
                } else {
                    $count++;
                }
            }
        }
        return $count;
    }

    ///////////////////////////
    //
    //   REMOVE DUPLICATES
    //
    ///////////////////////////

    /**
     * removeCompleteDuplicatesFromCustomer
     */
    private function removeCompleteDuplicatesFromCustomer() {
        $delete = "DELETE FROM update_customer_tbl WHERE "
                . "(customer_duplicate = '1' AND address_duplicate = '1' AND contact_duplicate = '1') OR "
                . "(customer_duplicate = '1' AND address_duplicate = '1' AND email = '')";

        if (!mysqli_query(Database::$connection, $delete)) {
            debugWriter("debug.txt", "DELETE FAILED " . mysqli_error(Database::$connection));
        }
    }

    /**
     * removeCompleteDuplicatesFromShipData
     */
    private function removeCompleteDuplicatesFromShipData() {
        $delete = "DELETE FROM update_ship_data_tbl WHERE "
                . "ship_duplicate = '1'";

        if (!mysqli_query(Database::$connection, $delete)) {
            debugWriter("debug.txt", "DELETE FAILED " . mysqli_error(Database::$connection));
        }
    }

    /**
     * removeCompleteDuplicatesFromMainEngine
     */
    private function removeCompleteDuplicatesFromMainEngine() {
        $delete = "DELETE FROM update_main_engine_tbl WHERE "
                . "main_engine_duplicate = '1'";

        if (!mysqli_query(Database::$connection, $delete)) {
            debugWriter("debug.txt", "DELETE FAILED " . mysqli_error(Database::$connection));
        }
    }

    /**
     * removeCompleteDuplicatesFromAuxEngine
     */
    private function removeCompleteDuplicatesFromAuxEngine() {
        $delete = "DELETE FROM update_aux_engine_tbl WHERE "
                . "aux_engine_duplicate = '1'";

        if (!mysqli_query(Database::$connection, $delete)) {
            debugWriter("debug.txt", "DELETE FAILED " . mysqli_error(Database::$connection));
        }
    }

    /**
     * removeCompleteDuplicatesFromSDH
     */
    private function removeCompleteDuplicatesFromSDH() {
        $delete = "DELETE FROM update_survey_date_history_tbl WHERE "
                . "sdh_duplicate = '1'";

        if (!mysqli_query(Database::$connection, $delete)) {
            debugWriter("debug.txt", "DELETE FAILED " . mysqli_error(Database::$connection));
        }
    }

    /**
     * removeCompleteDuplicatesFromSD
     */
    private function removeCompleteDuplicatesFromSD() {
        $delete = "DELETE FROM update_survey_date_tbl WHERE "
                . "sd_duplicate = '1'";

        if (!mysqli_query(Database::$connection, $delete)) {
            debugWriter("debug.txt", "DELETE FAILED " . mysqli_error(Database::$connection));
        }
    }

    /**
     * removeCompleteDuplicatesFromTZ
     */
    private function removeCompleteDuplicatesFromTZ() {
        $delete = "DELETE FROM update_trading_zone_tbl WHERE "
                . "tz_duplicate = '1'";

        if (!mysqli_query(Database::$connection, $delete)) {
            debugWriter("debug.txt", "DELETE FAILED " . mysqli_error(Database::$connection));
        }
    }

    /**
     * removeCompleteDuplicatesFromNameHistory
     */
    private function removeCompleteDuplicatesFromNameHistory() {
        $delete = "DELETE FROM update_name_history_tbl WHERE "
                . "name_history_duplicate = '1'";

        if (!mysqli_query(Database::$connection, $delete)) {
            debugWriter("debug.txt", "DELETE FAILED " . mysqli_error(Database::$connection));
        }
    }

    ///////////////////////////
    //
    //   ADD RECORDS TO DB
    //
    ///////////////////////////

    /**
     * addNewCustomers
     * @return int
     */
    private function addNewCustomers() {
        $newRecords = 0;
        $result = mysqli_query(Database::$connection, "SELECT temp_id, owcode, short_company_name, 
                 telephone, website, company_status, 
                full_company_name FROM update_customer_tbl WHERE customer_exists = '0' AND customer_duplicate = '0'");
        while ($row = mysqli_fetch_assoc($result)) {

            $insertCustomer = "INSERT INTO customer_tbl (cust_name, cust_telephone,"
                    . "cust_website, cust_account_manager, cust_tbh_id, "
                    . "cust_tbh_account_code, cust_ld_id, cust_ld_account_code, "
                    . "cust_seaweb_code, "
                    . "cust_company_status, cust_company_full_name) VALUES ("
                    . "'" . addslashes($row['short_company_name']) . "',"
                    . "'" . addslashes($row['telephone']) . "', "
                    . "'" . addslashes($row['website']) . "', "
                    . "NULL, "
                    . "'0', "
                    . "'0', "
                    . "'0', "
                    . "'0', "
                    . "'" . addslashes($row['owcode']) . "', "
                    . "'" . addslashes($row['company_status']) . "', "
                    . "'" . addslashes($row['full_company_name']) . "')";

            //debugWriter("debug.txt", "INSERT NEW CUSTOMER STRING " . $insertCustomer);

            if (!mysqli_query(Database::$connection, $insertCustomer)) {
                debugWriter("loadInstallationDataLogs.txt", "Cannot Insert Customer : " . mysqli_error(Database::$connection));
            } else {
                $customerID = mysqli_insert_id(Database::$connection);
                $insertCustomerStatus = "INSERT INTO customer_status_tbl "
                        . "(customer_tbl_cust_id, customer_status_lookup_tbl_cslt_id) "
                        . "VALUES ('" . $customerID . "', '1')";

                //debugWriter("debug.txt", "INSERT CUSTOMER STATUS " . $insertCustomerStatus);
                if (!mysqli_query(Database::$connection, $insertCustomerStatus)) {
                    debugWriter("debug.txt", "Cannot Insert Customer Status : " . mysqli_error(Database::$connection));
                }

                $update = "UPDATE update_customer_tbl SET customer_duplicate = '1', "
                        . "customer_id='" . $customerID . "' "
                        . "WHERE temp_id = '" . $row['temp_id'] . "'";
                //debugWriter("debug.txt", "UPDATE CUSTOMER TO DUPLICATE " . $update);
                if (!mysqli_query(Database::$connection, $update)) {
                    debugWriter("debug.txt", "UPDATE CONTACT AS EXISTS FAILED " . mysqli_error(Database::$connection));
                }
                $newRecords++;
            }
        }
        return $newRecords;
    }

    /**
     * addNewAddresses
     * @return int
     */
    private function addNewAddresses() {
        $newRecords = 0;
        $result = mysqli_query(Database::$connection, "SELECT temp_id, owcode, town_name, country_name,  
                full_address, customer_id FROM update_customer_tbl WHERE address_exists = '0' AND address_duplicate = '0'");
        while ($row = mysqli_fetch_assoc($result)) {
            $insertAddress = "INSERT INTO customer_address_tbl (cadt_office_name, cadt_city, "
                    . "cadt_country, cadt_address_verified, "
                    . "customer_tbl_cust_id, cadt_archived, cadt_default_address, cadt_full_address) "
                    . "VALUES ("
                    . "'Office', "
                    . "'" . addslashes($row['town_name']) . "', "
                    . "'" . addslashes($row['country_name']) . "', "
                    . "'0', "
                    . "'" . $row['customer_id'] . "', "
                    . "'0', "
                    . "'1', "
                    . "'" . addslashes($row["full_address"]) . "')";
            
            //debugWriter("debug.txt", "ADD NEW ADDRESS ".$insertAddress."\r\n");

            if (!mysqli_query(Database::$connection, $insertAddress)) {
                debugWriter("loadInstallationDataLogs.txt", "Cannot Insert Customer Address : " . mysqli_error(Database::$connection));
            } else {
                $addressID = mysqli_insert_id(Database::$connection);
                $update = "UPDATE update_customer_tbl SET address_duplicate = '1', "
                        . "customer_address_id = '" . $addressID . "'  WHERE temp_id = '" . $row['temp_id'] . "'";
                //$count++;
                if (!mysqli_query(Database::$connection, $update)) {
                    debugWriter("debug.txt", "UPDATE ADDRESS AS EXISTS FAILED " . mysqli_error(Database::$connection));
                }
                $newRecords++;
            }
        }
        return $newRecords;
    }

    /**
     * addNewContacts
     * @return int
     */
    private function addNewContacts() {
        $newRecords = 0;
        $result = mysqli_query(Database::$connection, "SELECT temp_id, owcode, email, customer_id, "
                . "customer_address_id FROM update_customer_tbl WHERE contact_exists = '0' AND contact_duplicate = '0'");
        while ($row = mysqli_fetch_assoc($result)) {

            if (($row['customer_id'] != "") && ($row['customer_address_id'] != "")) {
                $sqlString = "INSERT INTO customer_contacts_tbl (ict_email, "
                        . "ict_inactive, ict_contact_validated, customer_tbl_cust_id, "
                        . "customer_address_tbl_cadt_id, ict_info_source) "
                        . "VALUES ('" . $row['email'] . "', "
                        . "'0', '0', '" . $row['customer_id'] . "', '" . $row['customer_address_id'] . "', 'SeaWeb')";
                if (!mysqli_query(Database::$connection, $sqlString)) {
                    debugWriter("debug.txt", "INSERT customer_contacts_tbl FAILED : " . $sqlString . " " . mysqli_error(Database::$connection));
                } else {
                    $contactID = mysqli_insert_id(Database::$connection);
                    $update = "UPDATE update_customer_tbl SET contact_duplicate = '1', "
                            . "customer_contact_id = '" . $contactID . "'  WHERE temp_id = '" . $row['temp_id'] . "'";

                    if (!mysqli_query(Database::$connection, $update)) {
                        debugWriter("debug.txt", "UPDATE ADDRESS AS EXISTS FAILED " . mysqli_error(Database::$connection));
                    }
                    $newRecords++;
                }
            }
        }
        return $newRecords;
    }

    /**
     * addNewShips
     * @return int
     */
    private function addNewShips() {
        $newRecords = 0;
        $result = mysqli_query(Database::$connection, "SELECT * FROM update_ship_data_tbl WHERE ship_exists = '0' AND ship_duplicate = '0'");
        while ($row = mysqli_fetch_assoc($result)) {

            $dateOfBuild = date("Y-m-d H:i:s", time());
            if (isset($row['date_of_build'])) {
                $year = substr($row['date_of_build'], 0, 4);
                $month = substr($row['date_of_build'], 4, 2);
                if ($month == "00") {
                    $month = "01";
                }
                $dateOfBuild = $year . "-" . $month . "-01 00:00:00";
            }
            $shipManagerID = "";
            if ($row['ship_manager_company_code'] != "") {
                $shipManagerQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl WHERE cust_seaweb_code = '" . addslashes($row['ship_manager_company_code']) . "'");
                $smRow = mysqli_fetch_assoc($shipManagerQuery);
                if (isset($smRow['cust_id'])) {
                    $shipManagerID = $smRow['cust_id'];
                }
            }
            $defaultAddressID = "";
            if ($shipManagerID != "") {
                $defaultAddressQuery = mysqli_query(Database::$connection, "SELECT cadt_id FROM customer_address_tbl "
                        . "WHERE customer_tbl_cust_id = '" . $shipManagerID . "' "
                        . "AND cadt_default_address = '1'");
                $defaultAddressRow = mysqli_fetch_assoc($defaultAddressQuery);
                if (isset($defaultAddressRow['cadt_id'])) {
                    $defaultAddressID = $defaultAddressRow['cadt_id'];
                }
            }


            $technicalManagerID = "";
            if ($row['technical_manager_code'] != "") {
                $techManagerQuery = mysqli_query(Database::$connection, "SELECT cust_id FROM customer_tbl WHERE cust_seaweb_code = '" . addslashes($row['technical_manager_code']) . "'");
                $tmRow = mysqli_fetch_assoc($techManagerQuery);
                //debugWriter("debug.txt", "technical_manager_code " . $row['technical_manager_code'] . " " . mysqli_error(Database::$connection));
                if (isset($smRow['cust_id'])) {
                    $technicalManagerID = $tmRow['cust_id'];
                }
            }

            $status = "5";
            if (isset($row['ship_status'])) {
                // this value needs to be taken from the loop-up table.
                $statusQuery = mysqli_query(Database::$connection, "SELECT stat_id FROM installation_status_tbl WHERE stat_type = 'MARINE'
                                        AND MATCH (stat_status_description)
                                        AGAINST ('" . addslashes($row['ship_status']) . "' IN NATURAL LANGUAGE MODE)");

                $statusRow = mysqli_fetch_assoc($statusQuery);

                if (isset($statusRow['stat_id'])) {
                    $status = $statusRow['stat_id'];
                }
            }

            $sqlString = "INSERT INTO installations_tbl (inst_type, "
                    . "inst_installation, inst_installation_name, inst_imo, "
                    . "installation_status_tbl_stat_id, inst_owner_parent_company, "
                    . "inst_technical_manager_company, inst_original_source, "
                    . "inst_original_source_date, inst_yard_built, inst_built_date, "
                    . "inst_propeller_type, inst_propulsion_unit_count, inst_ship_builder, inst_build_year, inst_archive, "
                    . "customer_address_tbl_cadt_id, inst_flag_name, inst_lead_ship_in_series_by_imo, "
                    . "inst_ship_type_level_4, inst_classification_society) "
                    . "VALUES ('MARINE', "
                    . "'" . addslashes($row['ship_type_level_2']) . "', "
                    . "'" . addslashes($row['ship_name']) . "', "
                    . "'" . addslashes($row['lrimo']) . "', "
                    . "'" . $status . "', "
                    . "'0', "
                    . "'" . $technicalManagerID . "', "
                    . "'SeaWeb', "
                    . "'" . date("Y-m-d H:i:s", time()) . "', "
                    . "'" . addslashes($row['yard_number']) . "', "
                    . "'" . $dateOfBuild . "', "
                    . "'" . addslashes($row['propeller_type']) . "', "
                    . "'" . addslashes($row['noof_propulsion_units']) . "', "
                    . "'" . addslashes($row['ship_builder']) . "', "
                    . "'0000-00-00 00:00:00', "
                    . "'0', "
                    . "'" . $defaultAddressID . "', "
                    . "'" . addslashes($row['flag_name']) . "', "
                    . "'" . addslashes($row['lead_ship_in_series_by_imo']) . "', "
                    . "'" . addslashes($row['ship_type_level_4']) . "', "
                    . "'" . addslashes($row['classification_society']) . "')";
            if (!mysqli_query(Database::$connection, $sqlString)) {
                debugWriter("debug.txt", "INSERT INTO installations_tbl FAILED : " . $sqlString . " " . mysqli_error(Database::$connection));
            } else {
                $installationID = mysqli_insert_id(Database::$connection);
                $update = "UPDATE update_ship_data_tbl SET ship_duplicate = '1', "
                        . "installation_id = '" . $installationID . "'  WHERE temp_id = '" . $row['temp_id'] . "'";

                if (!mysqli_query(Database::$connection, $update)) {
                    debugWriter("debug.txt", "UPDATE SHIP AS DUPLICATE FAILED " . mysqli_error(Database::$connection));
                }

                // add ship into customer_tbl_has_installations_tbl
                $insertHas = "INSERT INTO customer_tbl_has_installations_tbl (customer_tbl_cust_id, installations_tbl_inst_id) "
                        . "VALUES ('$shipManagerID', '$installationID')";

                if (!mysqli_query(Database::$connection, $insertHas)) {
                    debugWriter("debug.txt", "insert into customer has installations failed " . mysqli_error(Database::$connection));
                } //else {
                   // debugWriter("debug.txt", "C has I inserted " . mysqli_insert_id(Database::$connection));
                //}
                $newRecords++;
            }
        }
        return $newRecords;
    }

    /**
     * addNewMainProducts
     */
    private function addNewMainProducts() {

        $count = 0;
        $archived = 0;

        $result = mysqli_query(Database::$connection, "SELECT * FROM update_main_engine_tbl "
                . "WHERE main_engine_duplicate = '0'");
        while ($row = mysqli_fetch_assoc($result)) {
            // select installation from installations table and customer data from customer table.
            $installationDataQuery = mysqli_query(Database::$connection, "SELECT i.inst_id, i.inst_installation_name "
                    . "FROM installations_tbl AS i "
                    . "WHERE i.inst_imo = '" . $row['lrno'] . "'");
            $instRow = mysqli_fetch_assoc($installationDataQuery);

            // count the products
            $countProducts = mysqli_query(Database::$connection, "SELECT count(prod_id) FROM product_tbl "
                    . "WHERE prod_unit_name LIKE '" . addslashes($instRow['inst_installation_name']) . " IC%'");
            $countProdRow = mysqli_fetch_assoc($countProducts);

            // get the customer ID
            $customerDataQuery = mysqli_query(Database::$connection, "SELECT customer_tbl_cust_id "
                    . "FROM customer_tbl_has_installations_tbl "
                    . "WHERE installations_tbl_inst_id = '" . $instRow['inst_id'] . "'");
            $custRow = mysqli_fetch_assoc($customerDataQuery);
            //debugWriter("debug.txt", "cust ID ".$custRow['customer_tbl_cust_id']);
            // get engine version
            $version = $this->getVersionIDOfEngine(addslashes($row['engine_designer']), addslashes($row['engine_model']), addslashes($row['noof_cylinders']), addslashes($row['cylinder_configuration']), false);

            // This is a catch all in case this is a brand new engine
            $unitName = $instRow['inst_installation_name'] . " IC " . ($countProdRow['count(prod_id)'] + 1) . " (POSSIBLY REPLACEMENT)";
            $productID = "";
            // Check that a main engine exists
            if ($row['main_engine_exists'] == '1') {
                $unitNameQuery = mysqli_query(Database::$connection, "SELECT prod_id, prod_unit_name FROM product_tbl WHERE prod_id = '" . $row['product_id'] . "'");
                $unitNameRow = mysqli_fetch_assoc($unitNameQuery);
                // it gets updated here to the pre-existing engine.
                if (strpos($unitNameRow['prod_unit_name'], "(UPDATED") !== false) {
                    $unitNameExp = explode(" (", $unitNameRow['prod_unit_name']);
                    $unitName = $unitNameExp[0] . " (UPDATED " . date("Y-m-d", time()) . ")";
                } else {
                    $unitName = $unitNameRow['prod_unit_name'] . " (UPDATED " . date("Y-m-d", time()) . ")";
                }
                $productID = $unitNameRow['prod_id'];
            }

            // Product TBL
            $sqlString = "INSERT INTO product_tbl (
                    prod_enquiry_only_flag, customer_tbl_cust_id, 
                    installations_tbl_inst_id, version_lookup_tbl_vlt_id, 
                    prod_unit_name, prod_source_alias_id) 
                    VALUES (
                    '0',  
                    '" . $custRow['customer_tbl_cust_id'] . "', 
                    '" . $instRow['inst_id'] . "', 
                    '" . $version['versionID'] . "', 
                    '" . addslashes($unitName) . "', 
                    '" . $version['aliasID'] . "')";
            if (!mysqli_query(Database::$connection, $sqlString)) {
                debugWriter("debug.txt", "INSERT INTO product_tbl FAILED (Main) : " . $sqlString . " " . mysqli_error(Database::$connection));
            } else {
                $newProductID = mysqli_insert_id(Database::$connection);
                $update = "UPDATE update_main_engine_tbl SET main_engine_duplicate = '1', "
                        . "product_id = '" . $newProductID . "'  WHERE temp_id = '" . $row['temp_id'] . "'";

                if (!mysqli_query(Database::$connection, $update)) {
                    debugWriter("debug.txt", "UPDATE MAIN ENGINE AS DUPLICATE FAILED " . mysqli_error(Database::$connection));
                } else {
                    $engSqlString = "INSERT INTO installation_engine_tbl (iet_model_description, "
                            . "iet_main_aux, iet_engine_verified, iet_output, iet_output_unit_of_measure, "
                            . "iet_fuel_type, iet_unit_name, iet_bore_size, iet_stroke, "
                            . "iet_release_date, iet_enquiry_only, load_priority_lookup_tbl_load_id, "
                            . "product_tbl_prod_id, iet_position_if_main, iet_engine_builder) "
                            . "VALUES ("
                            . "'" . addslashes($row['engine_model']) . "', "
                            . "'Main', "
                            . "'0', "
                            . "NULL, "
                            . "'MW', "
                            . "NULL, "
                            . "'" . addslashes($unitName) . "', "
                            . "'" . addslashes($row['bore']) . "', "
                            . "'" . addslashes($row['stroke']) . "', "
                            . "NULL, "
                            . "'0', "
                            . "'3', "
                            . "'$newProductID', "
                            . "'" . addslashes($row['position']) . "', "
                            . "'" . addslashes($row['engine_builder']) . "')";

                    if (!mysqli_query(Database::$connection, $engSqlString)) {
                        debugWriter("debug.txt", "INSERT INTO inst_eng_tbl FAILED : " . $engSqlString . " " . mysqli_error(Database::$connection));
                    } else {
                        $instEngineID = mysqli_insert_id(Database::$connection);
                        $update = "UPDATE update_main_engine_tbl SET  "
                                . "inst_engine_id = '" . $instEngineID . "'  WHERE temp_id = '" . $row['temp_id'] . "'";

                        if (!mysqli_query(Database::$connection, $update)) {
                            debugWriter("debug.txt", "UPDATE MAIN ENGINE eng_id FAILED : " . $engSqlString . " " . mysqli_error(Database::$connection));
                        } else {
                            // mark the old engine as archived as necessary

                            if ($productID != "") {
                                // there is an old product ID that needs archiving.
                                $archiveProduct = "UPDATE product_tbl SET prod_archive_flag = '1' WHERE prod_id = '" . $productID . "'";
                                if (!mysqli_query(Database::$connection, $archiveProduct)) {
                                    debugWriter("debug.txt", "Could not archive product " . mysqli_error(Database::$connection));
                                } else {
                                    $archived++;
                                }
                            }
                            $count++;
                        }
                    }
                }
            }
        }

        print $count . " new products added<br/>";
        print $archived . " old products archived<br/>";
    }

    /**
     * addNewAuxProducts
     * @return int
     */
    private function addNewAuxProducts() {

        $count = 0;
        $archived = 0;

        $result = mysqli_query(Database::$connection, "SELECT * FROM update_aux_engine_tbl "
                . "WHERE aux_engine_duplicate = '0'");
        while ($row = mysqli_fetch_assoc($result)) {
            // select installation from installations table and customer data from customer table.
            $installationDataQuery = mysqli_query(Database::$connection, "SELECT i.inst_id, i.inst_installation_name "
                    . "FROM installations_tbl AS i "
                    . "WHERE i.inst_imo = '" . $row['lrno'] . "'");
            $instRow = mysqli_fetch_assoc($installationDataQuery);

            // count the products
            $countProducts = mysqli_query(Database::$connection, "SELECT count(prod_id) FROM product_tbl "
                    . "WHERE prod_unit_name LIKE '" . $instRow['inst_installation_name'] . " IC%'");
            $countProdRow = mysqli_fetch_assoc($countProducts);

            // get the customer ID
            $customerDataQuery = mysqli_query(Database::$connection, "SELECT customer_tbl_cust_id "
                    . "FROM customer_tbl_has_installations_tbl "
                    . "WHERE installations_tbl_inst_id = '" . $instRow['inst_id'] . "'");
            $custRow = mysqli_fetch_assoc($customerDataQuery);

            // get engine version
            $version = $this->getVersionIDOfEngine($row['engine_designer'], $row['engine_model'], $row['noof_cylinders'], '-', true);

            // This is a catch all in case this is a brand new engine
            $unitName = $instRow['inst_installation_name'] . " IC " . ($countProdRow['count(prod_id)'] + 1) . " (POSSIBLY REPLACEMENT)";
            $productID = "";
            // Check that a main engine exists
            if ($row['aux_engine_exists'] == '1') {
                $unitNameQuery = mysqli_query(Database::$connection, "SELECT prod_id, prod_unit_name FROM product_tbl WHERE prod_id = '" . $row['product_id'] . "'");
                $unitNameRow = mysqli_fetch_assoc($unitNameQuery);
                // it gets updated here to the pre-existing engine.
                if (strpos($unitNameRow['prod_unit_name'], "(UPDATED") !== false) {
                    $unitNameExp = explode(" (", $unitNameRow['prod_unit_name']);
                    $unitName = $unitNameExp[0] . " (UPDATED " . date("Y-m-d", time()) . ")";
                } else {
                    $unitName = $unitNameRow['prod_unit_name'] . " (UPDATED " . date("Y-m-d", time()) . ")";
                }
                $productID = $unitNameRow['prod_id'];
            }

            // Product TBL
            $sqlString = "INSERT INTO product_tbl (
                    prod_enquiry_only_flag, customer_tbl_cust_id, 
                    installations_tbl_inst_id, version_lookup_tbl_vlt_id, 
                    prod_unit_name, prod_source_alias_id) 
                    VALUES (
                    '0',  
                    '" . $custRow['customer_tbl_cust_id'] . "', 
                    '" . $instRow['inst_id'] . "', 
                    '" . $version['versionID'] . "', 
                    '" . addslashes($unitName) . "', 
                    '" . $version['aliasID'] . "')";
            if (!mysqli_query(Database::$connection, $sqlString)) {
                debugWriter("debug.txt", "INSERT INTO product_tbl FAILED (AUX) : " . $sqlString . " " . mysqli_error(Database::$connection));
            } else {
                $newProductID = mysqli_insert_id(Database::$connection);
                $update = "UPDATE update_aux_engine_tbl SET aux_engine_duplicate = '1', "
                        . "product_id = '" . $newProductID . "'  WHERE temp_id = '" . $row['temp_id'] . "'";

                if (!mysqli_query(Database::$connection, $update)) {
                    debugWriter("debug.txt", "UPDATE AUX ENGINE AS DUPLICATE FAILED " . mysqli_error(Database::$connection));
                } else {
                    $engSqlString = "INSERT INTO installation_engine_tbl (iet_model_description, "
                            . "iet_main_aux, iet_engine_verified, iet_output, iet_output_unit_of_measure, "
                            . "iet_fuel_type, iet_unit_name, iet_bore_size, iet_stroke, "
                            . "iet_release_date, iet_enquiry_only, load_priority_lookup_tbl_load_id, "
                            . "product_tbl_prod_id, iet_position_if_main, iet_engine_builder) "
                            . "VALUES ("
                            . "'" . addslashes($row['engine_model']) . "', "
                            . "'Aux', "
                            . "'0', "
                            . "NULL, "
                            . "'MW', "
                            . "NULL, "
                            . "'" . addslashes($unitName) . "', "
                            . "'" . addslashes($row['bore']) . "', "
                            . "'" . addslashes($row['stroke']) . "', "
                            . "NULL, "
                            . "'0', "
                            . "'3', "
                            . "'$newProductID', "
                            . "'" . addslashes($row['engine_sequence']) . "', "
                            . "'" . addslashes($row['engine_builder']) . "')";

                    if (!mysqli_query(Database::$connection, $engSqlString)) {
                        debugWriter("debug.txt", "INSERT INTO inst_eng_tbl FAILED : " . $engSqlString . " " . mysqli_error(Database::$connection));
                    } else {
                        $instEngineID = mysqli_insert_id(Database::$connection);
                        $update = "UPDATE update_aux_engine_tbl SET  "
                                . "inst_engine_id = '" . $instEngineID . "'  WHERE temp_id = '" . $row['temp_id'] . "'";

                        if (!mysqli_query(Database::$connection, $update)) {
                            debugWriter("debug.txt", "UPDATE AUX ENGINE eng_id FAILED : " . $engSqlString . " " . mysqli_error(Database::$connection));
                        } else {
                            // mark the old engine as archived as necessary

                            if ($productID != "") {
                                // there is an old product ID that needs archiving.
                                $archiveProduct = "UPDATE product_tbl SET prod_archive_flag = '1' WHERE prod_id = '" . $productID . "'";
                                if (!mysqli_query(Database::$connection, $archiveProduct)) {
                                    debugWriter("debug.txt", "Could not archive product " . mysqli_error(Database::$connection));
                                } else {
                                    $archived++;
                                }
                            }
                            $count++;
                        }
                    }
                }
            }
        }

        print $count . " New Aux products added<br/>";
        print $archived . " Old Aux products archived<br/>";
    }

    ///////////////////////////
    //
    //   DROP TABLES
    //
    ///////////////////////////

    /**
     * dropCustomerTbl
     */
    private function dropCustomerTbl() {
        $count = mysqli_query(Database::$connection, "SELECT count(*) FROM update_customer_tbl");
        if (isset($count)){
            $row = mysqli_fetch_assoc($count);
            if ($row['count(*)'] == 0) {
                $drop_table = "DROP TABLE IF EXISTS update_customer_tbl";
                if (!mysqli_query(Database::$connection, $drop_table)) {
                    debugWriter("loadSuppliersDataLogs.txt", "Can't drop table - gravity failure : " . mysqli_error(Database::$connection) . "");
                }
            }
        }
    }

    /**
     * dropInstallationsTbl
     */
    private function dropInstallationsTbl() {
        $count = mysqli_query(Database::$connection, "SELECT count(*) FROM update_ship_data_tbl");
        if (isset($count)){
            $row = mysqli_fetch_assoc($count);
            if ($row['count(*)'] == 0) {
                $drop_table = "DROP TABLE IF EXISTS update_ship_data_tbl";
                if (!mysqli_query(Database::$connection, $drop_table)) {
                    debugWriter("loadSuppliersDataLogs.txt", "Can't drop table - gravity failure : " . mysqli_error(Database::$connection) . "");
                }
            }
        }
    }

    /**
     * dropMainEngineTbl
     */
    private function dropMainEngineTbl() {
        $count = mysqli_query(Database::$connection, "SELECT count(*) FROM update_main_engine_tbl");
        if (isset($count)){
            $row = mysqli_fetch_assoc($count);
            if ($row['count(*)'] == 0) {
                $drop_table = "DROP TABLE IF EXISTS update_main_engine_tbl";
                if (!mysqli_query(Database::$connection, $drop_table)) {
                    debugWriter("loadSuppliersDataLogs.txt", "Can't drop table - gravity failure : " . mysqli_error(Database::$connection) . "");
                }
            }
        }
    }

    /**
     * dropAuxEngineTbl
     */
    private function dropAuxEngineTbl() {
        $count = mysqli_query(Database::$connection, "SELECT count(*) FROM update_aux_engine_tbl");
        if (isset($count)){
            $row = mysqli_fetch_assoc($count);
            if ($row['count(*)'] == 0) {
                $drop_table = "DROP TABLE IF EXISTS update_aux_engine_tbl";
                if (!mysqli_query(Database::$connection, $drop_table)) {
                    debugWriter("loadSuppliersDataLogs.txt", "Can't drop table - gravity failure : " . mysqli_error(Database::$connection) . "");
                }
            }
        }
    }

    /**
     * dropSSDHEngineTbl
     */
    private function dropSSDHEngineTbl() {
        $drop_table = "DROP TABLE IF EXISTS update_survey_date_history_tbl";
        if (!mysqli_query(Database::$connection, $drop_table)) {
            debugWriter("loadSuppliersDataLogs.txt", "Can't drop table - gravity failure : " . mysqli_error(Database::$connection) . "");
        }
    }

    /**
     * dropSSDEngineTbl
     */
    private function dropSSDEngineTbl() {
        $drop_table = "DROP TABLE IF EXISTS update_survey_date_tbl";
        if (!mysqli_query(Database::$connection, $drop_table)) {
            debugWriter("loadSuppliersDataLogs.txt", "Can't drop table - gravity failure : " . mysqli_error(Database::$connection) . "");
        }
    }

    /**
     * dropTZTbl
     */
    private function dropTZTbl() {
        $drop_table = "DROP TABLE IF EXISTS update_trading_zone_tbl";
        if (!mysqli_query(Database::$connection, $drop_table)) {
            debugWriter("loadSuppliersDataLogs.txt", "Can't drop table - gravity failure : " . mysqli_error(Database::$connection) . "");
        }
    }

    private function dropNameHistoryTbl() {
        $drop_table = "DROP TABLE IF EXISTS update_name_history_tbl";
        if (!mysqli_query(Database::$connection, $drop_table)) {
            debugWriter("loadSuppliersDataLogs.txt", "Can't drop table - gravity failure : " . mysqli_error(Database::$connection) . "");
        }
    }

    public function updateCustomerTable() {
        $userHistoryController = new UserHistoryController();
        $this->dropCustomerTbl();
        /*
         * CUSTOMER FILE
         */
        if (file_exists('./upload/tblCompanyFullDetailsAndParentCode.CSV')) {
            print "<strong>tblCompanyFullDetailsAndParentCode FILE EXISTS</strong>";

            $this->createCustomerTable();
            $this->loadCustomerFile("./upload/tblCompanyFullDetailsAndParentCode.CSV");
            $this->countTempCustomerData();
            
            // these countries cause a problem so I have to update the database before anything else happens.
            $updateChina = "UPDATE update_customer_tbl SET country_name = 'China' "
                    . "WHERE country_name = '".addslashes("China, People's Republic of")."'";
            
            if (!mysqli_query(Database::$connection, $updateChina)) {
                debugWriter("debug.txt", "UPDATING CHINA $updateChina ".mysqli_error(Database::$connection));
            }
            
            $updateEire = "UPDATE update_customer_tbl SET country_name = 'Eire / Republic of Ireland' "
                    . "WHERE country_name = 'Eire' OR country_name = 'Republic of Ireland' OR "
                    . "country_name = 'Irish Republic' OR country_name = 'Ireland'  ";
            
            if (!mysqli_query(Database::$connection, $updateEire)) {
                debugWriter("debug.txt", "UPDATING EIRE $updateEire ".mysqli_error(Database::$connection));
            }
           
            $rowNum = 0;

            do {
               // debugWriter("debug.txt", $rowNum);
                $array['customer'] = $this->getNewCustomerRecord($rowNum);
                $rowsFound = sizeOf($array['customer']);
                print "Size of array: $rowsFound<br/>";
                $countCustArray = $this->checkCustomerRecordAgainstDB($array);

                $array['address'] = $this->getNewCustomerAddressRecord($rowNum);
                $rowsFound = sizeOf($array['address']);
                print "Size of array: $rowsFound<br/>";
                $countAddrArray = $this->checkAddressAgainstDB($array);

                $array['contact'] = $this->getNewCustomerContactRecord($rowNum);
                $rowsFound = sizeOf($array['contact']);
                print "Size of array: $rowsFound<br/>";
                $countContactArray = $this->checkContactAgainstDB($array);

                print "customers duplicated: " . $countCustArray['duplicate'] . "<br/>";
                print "customers exist: " . $countCustArray['exists'] . "<br/>";

                print "addresses duplicated: " . $countAddrArray['duplicate'] . "<br/>";
                print "addresses exists: " . $countAddrArray['exists'] . "<br/>";

                print "contacts duplicated: " . $countContactArray['duplicate'] . "<br/>";
                print "contacts exists: " . $countContactArray['exists'] . "<br/>";

                unset($array);
                $rowNum = $rowNum + 10000;
            } while ($rowsFound == 10000);

            $this->removeCompleteDuplicatesFromCustomer();

            print "New Companies added: " . $this->addNewCustomers() . "<br/>";
            print "New Addresses added: " . $this->addNewAddresses() . "<br/>";
            print "New Contacts added: " . $this->addNewContacts() . "<br/>";

            $this->removeCompleteDuplicatesFromCustomer();
            print "<br/>";
            $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "UPDATE", "SEAWEB Company details updated automatically");


            $array['customer'] = NULL;
            $array['address'] = NULL;
            $array['contact'] = NULL;
            unset($array);
            $countCustArray['duplicate'] = NULL;
            $countAddrArray['duplicate'] = NULL;
            $countContactArray['duplicate'] = NULL;
            unset($countCustArray);
            unset($countAddrArray);
            unset($countContactArray);

            $this->dropCustomerTbl();
            $gcCollection = gc_collect_cycles();
            print $gcCollection . "<br/>";
        } else {
            print "NO tblCompanyFullDetailsAndParentCode.CSV FILE FOUND<br/>";
        }
    }
    
    public function updateShipDataTable() {
        $this->dropInstallationsTbl();
        $userHistoryController = new UserHistoryController();
        /*
         * SHIP DATA
         */
        if (file_exists('./upload/ShipData.CSV')) {
            print "<strong>ShipData FILE EXISTS</strong>";

            $this->createShipTable();
            $this->loadShipFile("./upload/ShipData.CSV");
            $this->countTempShipData();

            $rowNum = 0;

            do {
                $array = $this->getShipDataRecords($rowNum);
                $rowsFound = sizeOf($array);
                print "Size of array: $rowsFound<br/>";
                $countShipArray = $this->checkShipRecordsAgainstDB($array);
                unset($array);
                print "Ships duplicated: " . $countShipArray['duplicate'] . "<br/>";
                print "Ships exist: " . $countShipArray['exists'] . "<br/>";
                print "<br/>";
                $rowNum = $rowNum + 10000;
            } while ($rowsFound == 10000);

            $this->removeCompleteDuplicatesFromShipData();
            print "New Ships Added: " . $this->addNewShips() . "<br/>";
            $this->removeCompleteDuplicatesFromShipData();
            $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "UPDATE", "SEAWEB Ship details updated automatically");


            $countShipArray['duplicate'] = NULL;
            $countShipArray['exists'] = NULL;
            unset($array);
            unset($countShipArray);

            $this->dropInstallationsTbl();
        } else {
            print "NO ShipData.CSV FILE FOUND<br/>";
        }

    }
    
    public function updateMainEngineTable() {
        $userHistoryController = new UserHistoryController();
        $this->dropMainEngineTbl();
        /*
         * MAIN ENGINE
         */
        if (file_exists('./upload/tblMainEngines.CSV')) {
            print "<strong>tblMainEngines FILE EXISTS</strong>";
            $this->createMainEngineTable();
            $this->loadMainEngineFile("./upload/tblMainEngines.CSV");
            $this->countMainEngineProductData();

            $array = ($this->getMainEngineRecords());
            $countMainArray = $this->checkMainEngineAgainstDB($array);
            unset($array);
            print "Main engines duplicates: " . $countMainArray['duplicate'] . "<br/>";
            print "Main engines exist: " . $countMainArray['exists'] . "<br/>";
            $this->removeCompleteDuplicatesFromMainEngine();
            $this->addNewMainProducts() . " <br/>";
            $this->removeCompleteDuplicatesFromMainEngine();
            print "<br/>";
            $this->dropMainEngineTbl();
            $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "UPDATE", "SEAWEB Main Engine details updated automatically");
            
        } else {
            print "NO tblMainEngines.CSV FILE FOUND<br/>";
        }
    }
    
    public function updateAuxEngineTable() {
        $userHistoryController = new UserHistoryController();
        $this->dropAuxEngineTbl();
        /*
         * AUX ENGINE
         */
        if (file_exists('./upload/tblAuxEngines.CSV')) {
            print "<strong>tblAuxEngines FILE EXISTS</strong>";
            $this->createAuxEngineTable();
            $this->loadAuxEngineFile("./upload/tblAuxEngines.CSV");
            $this->countAuxEngineData();

            $array = ($this->getAuxProductRecords());
            $countAuxArray = $this->checkAuxEngineAgainstDB($array);
            unset($array);
            print "Aux engine duplicates: " . $countAuxArray['duplicate'] . "<br/>";
            print "Aux engine exist: " . $countAuxArray['exists'] . "<br/>";
            $this->removeCompleteDuplicatesFromAuxEngine();
            $this->addNewAuxProducts() . "<br/>";
            $this->removeCompleteDuplicatesFromAuxEngine();
            print "<br/>";
            $this->dropAuxEngineTbl();
            $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "UPDATE", "SEAWEB Aux Engine details updated automatically");

            
        } else {
            print "NO tblAuxEngines.CSV FILE FOUND<br/>";
        }
    }
    
    public function updateSurveyDateHistoryTable() {
        $userHistoryController = new UserHistoryController();
        $this->dropSSDHEngineTbl();
        /*
         * SURVEY DATE HISTORY
         */
        if (file_exists('./upload/tblSurveyDateHistory.CSV')) {
            print "<strong>tblSurveyDateHistory FILE EXISTS</strong>";
            $this->createSurveyDateHistoryTable();
            $this->loadSurveyDateHistoryFile("./upload/tblSurveyDateHistory.CSV");
            $this->countSurveyDateHistoryData();

            $array = ($this->getSurveyDateHistoryRecords());
            print "SDH replaced: " . $this->updateSpecialSurveyDateHistoryInDB($array);
            unset($array);
            $this->dropSSDHEngineTbl();
            print "<br/>";
            $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "UPDATE", "SEAWEB Survey Date Hitory details updated automatically");
        } else {
            print "NO tblSurveyDateHistory.CSV FILE FOUND<br/>";
        }
    }
    
    public function updateSurveyDueDateTable() {
        $userHistoryController = new UserHistoryController();
        /*
         * SURVEY DATE DUE
         */
        if (file_exists('./upload/tblSurveyDates.CSV')) {
            print "<strong>tblSurveyDates FILE EXISTS</strong>";
            $this->createSurveyDateTable();
            $this->loadSurveyDateFile("./upload/tblSurveyDates.CSV");
            $this->countSurveyDateData();

            $array = ($this->getSurveyDateRecords());
            print "SDD replaced: " . $this->updateSpecialSurveyDateInDB($array);
            unset($array);
            $this->dropSSDEngineTbl();
            print "<br/>";
            $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "UPDATE", "SEAWEB Survey Due Date details updated automatically");
        } else {
            print "NO tblSurveyDates.CSV FILE FOUND<br/>";
        }
    }
    
    public function updateTradingZoneTable() {
        $userHistoryController = new UserHistoryController();
        $this->dropTZTbl();
        /*
         * TRADING ZONE
         */
        if (file_exists('./upload/tblTradingZoneLastSeen.CSV')) {
            print "<strong>tblTradingZoneLastSeen FILE EXISTS</strong>";
            $this->createTradingZoneTable();
            $this->loadTradingZoneFile("./upload/tblTradingZoneLastSeen.CSV");
            $this->countTradingZoneData();

            $array = $this->getTradingZoneRecords();
            print "TZ replaced: " . $this->updateTradingZoneInDB($array);
            unset($array);
            $this->dropTZTbl();
            print "<br/>";
            $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "UPDATE", "SEAWEB Trading Zone details updated automatically");
        } else {
            print "NO tblTradingZoneLastSeen.CSV FILE FOUND<br/>";
        }
    }
    
    public function updateNameHistoryTable() {
        $userHistoryController = new UserHistoryController();
        $this->dropNameHistoryTbl();
        /*
         *  NAME HISTORY
         */
        if (file_exists('./upload/tblNameHistory.CSV')) {
            print "<strong>tblNameHistory FILE EXISTS</strong>";
            $this->createNameHistoryTable();
            $this->loadNameHistoryFile("./upload/tblNameHistory.CSV");
            $this->countNameHistoryData();

            $array = $this->getNameHistoryRecords();
            print "Name History replaced: " . $this->updateNameHistoryInDB($array);
            unset($array);
            $this->dropNameHistoryTbl();
            print "<br/>";
            $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "UPDATE", "SEAWEB Ship name history details updated automatically");
        } else {
            print "NO tblNameHistory.CSV FILE FOUND<br/>";
        }
    }
   
    /**
     * updateSeawebData
     * @param type $folder
     */
    public function updateSeawebData($folder) {

        $this->updateCustomerTable();

        $this->updateShipDataTable();
        
        $this->updateMainEngineTable();
        
        $this->updateAuxEngineTable();

        $this->updateSurveyDateHistoryTable();
        
        $this->updateSurveyDueDateTable();
        
        $this->updateTradingZoneTable();
        
        $this->updateNameHistoryTable();

    }
    
}
