<?php
$optGroups = ['Company Data', 'Ship Data', 'Main Engines', 'Aux Engines', 'Company Fleet Counts',  
    'Survey Date History', 'Survey Date', 'Trading Zone Last Seen', 'Ship Name History'];

$dbInstallationArray = [
    'inst_imo' => ['LRIMOShipNo' => 'Ship Data'],
    'inst_installation_name' => ['ShipName' => 'Ship Data'],
    'installation_status_tbl_stat_id' => ['ShipStatus' => 'Ship Data'],
    'inst_installation' => ['ShiptypeLevel2' => 'Ship Data'],
    'inst_built_date' => ['DateOfBuild' => 'Ship Data'],
    'inst_lead_ship_in_series_by_imo' => ['LeadshipinseriesbyIMONumber' => 'Ship Data'],
    'inst_flag_name' => ['FlagName' => 'Ship Data'],
    'gb_owner' => ['GroupBeneficialOwner' => 'Ship Data'],
    'gb_owner_company_code' => ['GroupBeneficialOwnerCompanyCode' => 'Ship Data'],
    'gb_owner_country' => ['GroupBeneficialOwnerCountryofDomicile' => 'Ship Data'],
    /*'reg_owner' => ['RegisteredOwner' => 'Ship Data'],
    'reg_owner_code' => ['RegisteredOwnerCode' => 'Ship Data'],
    'reg_owner_country' => ['RegisteredOwnerCountryOfDomicile' => 'Ship Data'],*/
    'shipmanager' => ['ShipManager' => 'Ship Data'],
    'shipmanager_code' => ['ShipManagerCompanyCode' => 'Ship Data'],
    'shipmanager_country' => ['ShipManagerCountryofDomicileName' => 'Ship Data'],
    'inst_technical_manager' => ['TechnicalManager' => 'Ship Data'],
    'inst_technical_manager_code' => ['TechnicalManagerCode' => 'Ship Data'],
    'inst_technical_manager_country' => ['TechnicalManagerCountryOfDomicile' => 'Ship Data'],
    'noof_aux_engines' => ['NumberOfAuxiliaryEngines' => 'Ship Data'],
    'noof_main_engines' => ['NumberOfMainEngines' => 'Ship Data'],
    'inst_propeller_type' => ['PropellerType' => 'Ship Data'],
    'inst_propulsion_unit_count' => ['NumberOfPropulsionUnits' => 'Ship Data'],
    'inst_yard_built' => ['YardNumber' => 'Ship Data'],
    'inst_ship_builder' => ['ShipBuilder' => 'Ship Data'],
    'inst_ship_type_level_4' => ['ShiptypeLevel4' => 'Ship Data'],
    'inst_classification_society' => ['ClassificationSociety' => 'Ship Data'],
    // COMPANY DATA
    'cust_seaweb_code' => ['OWCODE' => 'Company Data'],
    'cust_name' => ['ShortCompanyName' => 'Company Data'],
    'cadt_country' => ['CountryName' => 'Company Data'],
    'cadt_city' => ['TownName' => 'Company Data'],
    'cust_telephone' => ['Telephone' => 'Company Data'],
    'ict_email' => ['Emailaddress' => 'Company Data'],
    'cust_website' => ['Website' => 'Company Data'],
    'po_box' => ['POBox' => 'Company Data'],
    'cadt_address' => ['StreetNumber' => 'Company Data'],
    'cadt_address1' => ['Street' => 'Company Data'],
    'cadt_postcode' => ['PrePostcode' => 'Company Data'],
    'cadt_postcode1' => ['PostPostcode' => 'Company Data'],
    'cust_parent_company_seaweb_code' => ['parentCompany' => 'Company Data'],
    'cust_parent_company_nationality_of_control' => ['NationalityOfControl' => 'Company Data'],
    'cust_company_status' => ['CompanyStatus' => 'Company Data'],
    'cust_company_full_name' => ['FullCompanyName' => 'Company Data'],
    'cadt_full_address' => ['FullAddress' => 'Company Data'],
    'cust_fax' => ['Facsimile' => 'Company Data'],
    // FLEET COUNTS
    'cfct_owcode' => ['OWCODE (FLEET)' => 'Company Fleet Counts'],
    'cfct_cust_name' => ['ShortCompanyName (FLEET)' => 'Company Fleet Counts'],
    'cfct_registered_owner_count' => ['RegisteredOwnerCount' => 'Company Fleet Counts'],
    'cfct_shipmanager_count' => ['ShipManagerCount' => 'Company Fleet Counts'],
    'cfct_operator_count' => ['OperatorCount' => 'Company Fleet Counts'],
    'cfct_group_owner_count' => ['GroupOwnerCount' => 'Company Fleet Counts'],
    'cfct_doc_count' => ['DOCCount' => 'Company Fleet Counts'],
    'cfct_fleet_size' => ['FleetSize' => 'Company Fleet Counts'],
    'cfct_in_service_count' => ['InServiceCount' => 'Company Fleet Counts'],
    // MAIN ENGINES
    'main_imo' => ['LRNO (Main)' => 'Main Engines'],
    'iet_position_if_main' => ['Position' => 'Main Engines'],
    'dlt_designer_description' => ['EngineDesigner' => 'Main Engines'],
    'iet_engine_builder' => ['EngineBuilder' => 'Main Engines'],
    'iet_model_description' => ['EngineModel' => 'Main Engines'],
    'vlt_cylinder_count' => ['NumberOfCylinders' => 'Main Engines'],
    'iet_bore_size' => ['Bore' => 'Main Engines'],
    'iet_stroke' => ['Stroke' => 'Main Engines'],
    'vlt_cylinder_configuration' => ['CylinderArrangementCode' => 'Main Engines'],
    // AUX ENGINES
    'aux_imo' => ['LRNO (Aux)' => 'Aux Engines'],
    'aux_sequence' => ['EngineSequence (Aux)' => 'Aux Engines'],
    'aux_engine_builder' => ['EngineBuilder (Aux)' => 'Aux Engines'],
    'aux_engine_designer' => ['EngineDesigner (Aux)' => 'Aux Engines'],
    'aux_engine_model' => ['EngineModel (Aux)' => 'Aux Engines'],
    'aux_noof_cylinders' => ['NumberOfCylinders (Aux)' => 'Aux Engines'],
    'aux_bore' => ['Bore (Aux)' => 'Aux Engines'],
    'aux_stroke' => ['Stroke (Aux)' => 'Aux Engines'],
    // SURVEY DATE HISTORY
    'sdht_lrno' => ['LRNO (SDH)' => 'Survey Date History'],
    'sdht_special_survey_date' => ['SpecialSurvey (SDH)' => 'Survey Date History'],
    // SURVEY DATE
    'sddt_lrno' => ['LRNO (SD)' => 'Survey Date'],
    'sddt_special_survey_date' => ['SpecialSurvey (SD)' => 'Survey Date'],
    // TRADE ZONE
    'tzlst_lrno' => ['LRNO (TZ)' => 'Trading Zone Last Seen'],
    'tzlst_trading_zone' => ['TradingZone' => 'Trading Zone Last Seen'],
    'tzlst_last_seen_date' => ['LastSeenDate' => 'Trading Zone Last Seen'],
    // Ship name history since 2000
    'snht_lrno' => ['LRNO (SNHT)' => 'Ship Name History'],
    'snht_sequence' => ['Sequence' => 'Ship Name History'],
    'snht_vessel_name' => ['VesselName' => 'Ship Name History'],
    'snht_effective_date' => ['EffectiveDate' => 'Ship Name History']
    
];
?>
<script>
    /* This function is given an array and returns an array of duplicated values if they exist */
    function identifyDuplicatesFromArray(arr) {
        var i;
        var len = arr.length;
        var obj = {};
        var duplicates = [];

        for (i = 0; i < len; i++) {

            if (!obj[arr[i]]) {

                obj[arr[i]] = {};

            }

            else
            {
                duplicates.push(arr[i]);
            }

        }
        return duplicates;
    }
    /* This function gathers all the selected options and puts them in an array to check for duplicates */
    $(function () {
        $('#form').on('submit', function (e) {
            e.preventDefault(); // prevent the usual submit
            var checkArray = [];

            $("select").each(function (index) {
                var selected = $(this).find(":selected").text();

                if (selected !== "MANUALLY SELECT") {
                    checkArray.push(selected);
                }
            });

            var result = identifyDuplicatesFromArray(checkArray);
            if (result.length < 1) {
                this.submit();
            } else {
                alert("You have used the same column name twice.\r\n Option(s) used " + result);
            }
        });
    });
</script>

<h3>FILE: <?php echo $name; ?></h3>
<form id="form" name="form" action="index.php?portal=mr&page=uploadSeaweb&action=upload" method="post" enctype="multipart/form-data">
    <input type="hidden" name="fileName" value="<?php echo $fileName; ?>"/>
    <input type="hidden" name="fileType" value="<?php echo $_POST['fileType']; ?>"/>
    Column Names are included in file <input type="checkbox" name="headers" value="1" checked/>
    <table>
        <tr>

<?php
$typeExists = 0;
if (is_array($array) || $array instanceof Traversable) {
    foreach ($array as $key => $fileColumnName) {
        if ((strcasecmp($fileColumnName, "TYPE") == 0)) {
            $typeExists = 1;
        }
        if ($fileColumnName != " ") {
            print "<td><strong>" . $fileColumnName . "</strong><br/>[<span class='tinyText'>Example: " . $lineOfText[$key] . "</span>]</td>";
        }
    }
}
if (($typeExists == 0) && ($_POST['fileType'] == 'shipData')) {
    print "<td><strong>Type</strong></td>";
}
?>
        </tr>
        <tr>

            <?php
            if (is_array($array) || $array instanceof Traversable) {
                foreach ($array as $key => $fileColumnName) {

                    print "<td><select name='" . $key . "_" . $fileColumnName . "'>";
                    print "<option value=''>MANUALLY SELECT</option>";
                    foreach ($optGroups as $optgroup) {
                        print "<optgroup label='" . $optgroup . "'>";
                        $group = $optgroup;
                        foreach ($dbInstallationArray as $key => $array1) {
                            foreach ($array1 as $name => $type) {
                                 if ($type === $group) {
                                    // match the file column names to the database values.
                                    if (strcasecmp($fileColumnName, $name) == 0) {
                                        print "<option value='$key' selected>$name</option>";
                                    } else {
                                        print "<option value='$key'>$name</option>";
                                    }
                                }
                            }
                        }
                        print "</optgroup>";
                    }

                    print "</select></td>";
                }
            }
           if (($typeExists == 0) && ($_POST['fileType'] == 'shipData')) {
                print "<td><select name='type'>
                <option value='marine'>Marine</option>
                </select>
                </td>";
            }
            ?>
        </tr>
    </table>


    <input type="submit" name="submitVerify" value="Import Data"/>
</form>