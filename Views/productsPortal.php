<?php

include_once 'Controllers/DesignerLookupController.php';
include_once 'Controllers/SeriesLookupController.php';
include_once 'Controllers/VersionLookupController.php';
include_once 'Controllers/TypeOfProductLookupController.php';

$designerLookupController = new DesignerLookupController();
$seriesLookupController = new SeriesLookupController();
$versionLookupController = new VersionLookupController();
$typeOfProductLookupController = new TypeOfProductLookupController();

include_once('Views/Products/products.php');