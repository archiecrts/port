<h3>Accounts</h3>
<?php
/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once 'Controllers/HMRCExchangeRateController.php';
include_once 'Controllers/DailyExchangeRateController.php';
$hmrcExchangeRateController = new HMRCExchangeRateController();
$dailyExchangeRateController = new DailyExchangeRateController();

// history controllers
include_once 'ControllersHistory/DailyExchangeRateHistoryController.php';
include_once 'ControllersHistory/HMRCExchangeRateHistoryController.php';
$dailyExchangeRateHistoryController = new DailyExchangeRateHistoryController();
$hmrcExchangeRateHistoryController = new HMRCExchangeRateHistoryController();
// helper
include 'Helpers/ForeignExchange.php';
$foreignExchange = new ForeignExchange();


$page = "";
if (isset($_GET['page'])) {
    $page = $_GET['page'];
}
switch ($page) {
    case "hmrc":
        $allRates = $hmrcExchangeRateController->getAllExchangeRates();
        include 'Views/Accounts/uploadHMRCExchangeRateCSV.php';
        break;
    case "uploadHMRCRate":

        // update history first
        $allRatesHistory = $hmrcExchangeRateController->getAllExchangeRates();
        if (isset($allRatesHistory)) {
            foreach ($allRatesHistory as $index => $rate) {
                $hmrcExchangeRateHistory = new HistoryHMRCExchangeRate(
                        "", 
                        $rate->hmrcID, 
                        $rate->hmrcCountry, 
                        $rate->hmrcCurrency,
                        $rate->hmrcCurrencyCode, 
                        $rate->hmrcCurrencyUnitsPerPound, 
                        $rate->hmrcStartDate, 
                        $rate->hmrcEndDate, 
                        $_SESSION['user_id'], 
                        date("Y-m-d H:i:s", time()));
                $historyHMRC = $hmrcExchangeRateHistoryController->setHMRCExchangeRateHistory($hmrcExchangeRateHistory);
            }
        }

        $fileExt = explode('.', $_FILES["rateCSV"]["name"]);

                    if ($fileExt[count($fileExt) - 1] != 'csv') {
                        echo "<p span='red'>Sorry, you must use a CSV</p>";
                    } else {

                        // Check the file for errors, then save into the upload directory
                        $name = '';
                        if ($_FILES["rateCSV"]["error"] == 0) {
                            $tmp_name = $_FILES["rateCSV"]["tmp_name"];
                            $name = $_FILES["rateCSV"]["name"];
                            move_uploaded_file($tmp_name, "upload/$name");
                        }

                        $fileName = 'upload/' . $_FILES['rateCSV']['name'];

                        $truncateTable = $hmrcExchangeRateController->truncateTable();

                        if ($truncateTable === true) {
                            $loadData = $hmrcExchangeRateController->loadExchangeRateCSV($fileName);

                            if ($loadData === true) {
                                print "<p>Data successfully loaded</p>";
                            } else {
                                print "<p>Upload failed and table is empty</p>";
                            }
                        }
                    }



        $allRates = $hmrcExchangeRateController->getAllExchangeRates();
        include 'Views/Accounts/uploadHMRCExchangeRateCSV.php';
        break;

    case "updateDailyRates":
        $dailyRates = $dailyExchangeRateController->getAllDailyRates();
        if (isset($dailyRates)) {
            foreach ($dailyRates as $index => $rate) {
                // update history first
                $dailyExchangeRateHistory = new HistoryDailyExchangeRate(
                        "", 
                        $rate->dailyEXCID, 
                        $rate->dailyEXCCurrencyCode, 
                        $rate->dailyEXCCurrencyUnitPerPound, 
                        $rate->dailyEXCDateSet, 
                        $_SESSION['user_id'], 
                        date("Y-m-d H:i:s", time()));
                $historyDaily = $dailyExchangeRateHistoryController->setDailyExchangeRateHistory($dailyExchangeRateHistory);

                if ($historyDaily != null) {
                    $fxDownload = $foreignExchange->downloadAndUpdate('GBP', $rate->dailyEXCCurrencyCode);
                    $dailyExchangeRateController->updateDailyRate($rate->dailyEXCID, $fxDownload);
                }
            }
        }
    // then go into Daily
    case "daily":
        $dailyRates = $dailyExchangeRateController->getAllDailyRates();
        include 'Views/Accounts/generateDailyRateDownload.php';
        break;
    default:
        $allRates = $hmrcExchangeRateController->getAllExchangeRates();
        include 'Views/Accounts/uploadHMRCExchangeRateCSV.php';
        break;
}