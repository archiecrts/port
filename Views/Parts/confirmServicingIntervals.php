        <h3>Confirm Servicing Interval List</h3>
        <form name="form" action="index.php?portal=parts&page=confirmServiceList" enctype="multipart/form-data" method="post">
        <?php
        // TODO check engine builder is set.
        /*print "<pre>";
        print_r($_POST);
        print "</pre>";*/
        $interval = 0;
        $i=0;
        $addMinorInterval = 0;
        $major = 0;
        $countRecordsInserted = 0;
        
        // Create a list of intervals in loop.
        while ($i <($_POST['noOfColumns']+1)) {
            // foreach column, if there is a minor interval then add it and loop.
            // If there is no minor, use interval and loop.
            // If interval is %10K=0 then mark as MAJOR.
            
            foreach ($_POST['min'] as $key => $value) {

                if (($interval >= $value) && ($interval < $_POST['max'][$key])) {
                    $addMinorInterval = 1;
                    break; // get out of the foreach so that later min and max values dont overwrite it.
                } else {
                    $addMinorInterval = 0;
                   
                }
            }
            // $i = 0 ends up showing a 0 hour service as a Major. So we ignore this line. 
            // The number of columns above also gets 1 added to it to take account that the
            // user doesnt know of its existence.
            if ($i != 0) {
                

                
                if ($interval % $_POST['markMajor'] === 0) {
                    if (!isset($_POST['confirm'])) {
                        print "<span class='blue'>".$interval." MAJOR</span><br />";
                    }
                    $major = 1;
                } else {
                    if (!isset($_POST['confirm'])) {
                        print $interval."<br />";
                    }
                    $major = 0;
                }
            

                if (isset($_POST['confirm'])) {

                    $insert_query = "INSERT INTO engine_servicing_intervals_tbl "
                            . "(engine_builder_tbl_eb_id, esi_interval, esi_major, esi_related_column_number) "
                            . "VALUES ('".$_POST['make']."', '".$interval."', '".$major."', '".$i."')";

                    if (!mysqli_query($insert_query)) {
                        echo "Can't insert records : " . mysqli_error(). "<br/>";
                    } else {
                        $countRecordsInserted++;
                    }
                }
            }
            
            if ($addMinorInterval === 1) {
                $interval = $interval+$_POST['minorInterval'];
            } else {
                $interval = $interval+$_POST['intervalToSet'];
            }
            $i++;
        }
        
        // This recreates the $_POST variables to be re-passed to the page.
        foreach($_POST as $key => $value ){
            
            // Arrays need to be handled differently.
            if (is_array($value)) {
                foreach($value as $j => $k) {
                    print '<input type="hidden" name="'.$key.'['.$j.']" value="'.$k.'"/>';
                }     
            } else {
                print '<input type="hidden" name="'.$key.'" value="'.$value.'"/>';
            }
            
        }
        
        if (!isset($_POST['confirm'])) {
            
            echo '<input type="submit" name="confirm" value="Confirm"/>';
        } else {
            echo "You have successfully added ".$countRecordsInserted." records to the database";
        }
        ?>
            
            
        </form>
    </body>
</html>
