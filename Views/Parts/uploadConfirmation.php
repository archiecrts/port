


<h2>Import Screen</h2>
<?php
/**
 * Check that all the post values have a value attached to them / not null.
 * Otherwise send them back to the previous page with an error.
 */
foreach ($_POST as $key => $value) {
    if ($value == "") {
        header("location:index.php?portal=parts&page=upload&error=error");
    }
}


// Check that file is .csv
$fileExt = explode('.', $_FILES["file"]["name"]);

if ($fileExt[count($fileExt) - 1] != 'csv') {
    echo "Sorry, you must use a CSV, please go <a href='import.php'>back</a>";
    header("location:import.php?fileerror=fileerror");
} else {

    // Check the file for errors, then save into the upload directory

    if ($_FILES["file"]["error"] == 0) {
        $tmp_name = $_FILES["file"]["tmp_name"];
        $name = $_FILES["file"]["name"];
        move_uploaded_file($tmp_name, "upload/$name");
    }

    $fileName = 'upload/' . $_FILES['file']['name'];
    $modelVersionId = $_POST['version'];
}
?>

<script>


    $('#loading_spinner').show();

    var post_data = "fileName=" + <?php echo json_encode($fileName); ?> 
    + "& modelVersionId=" + <?php echo json_encode($modelVersionId); ?>
    + "& make=" + <?php echo json_encode($_POST['make']); ?>;

    $.ajax({
        url: 'Views/Parts/importAjaxPhpCode.php',
        type: 'POST',
        data: post_data,
        dataType: 'html',
        success: function (data) {
            $('.my_update_panel').html(data);
            //Moved the hide event so it waits to run until the prior event completes
            //It hid the spinner immediately, without waiting, until I moved it here
            $('#loading_spinner').hide();
        },
        error: function () {
            alert("Something went wrong!");
        }
    });


</script>
<img id="loading_spinner" src="icons/ajax-loader.gif"/>

<div class="my_update_panel">Please wait while the file is imported into the database. Do not press the back
button or navigate from this page.</div>

</body>
</html>