<?php
//$edit = filter_input(INPUT_GET, 'edit');
if (isset($edit)) {
    $editMode = 1;
} else {
    $editMode = 0;
}

include_once 'Controllers/EngineMakeModelVersionController.php';
$engineMakeModelVersionController = new EngineMakeModelVersionController();

// TODO: DOES THIS NEED A SEARCH FEATURE?
?>
<table class="borderedTable">
    <tr><th align="left">Parts Manifest</th><th align="left">Manufacturer</th><th align="left">Model</th><th align="left">Version</th></tr>
    <?php
    //execute the SQL query and return records
    $allEnginesArray = $engineMakeModelVersionController->getAllEngines();


    //fetch tha data from the database
    if (isset($allEnginesArray)) {
        foreach ($allEnginesArray as $key => $object) {

            print '<tr><td width="150px">';
            $countManifestRow = $engineMakeModelVersionController->getCountOfAssociatedServiceIntervals($object->engineVersionID );
            

            $manifestAttached = "icons/cross.png";
            $countOfRows = "";
            if ($countManifestRow > 0) {
                $manifestAttached = "icons/tick.png";
                $countOfRows = "(" . $countManifestRow . " Parts)";
            }

            print '<img src="' . $manifestAttached . '" alt="Manifest Attached" height="16" width="16">';
            print $countOfRows;
            print '</td><td>';

            if ($editMode == 1) {
                print "<a href='index.php?portal=parts&page=editEngine&chosenEngineVersion=" . $object->engineVersionID . "' target='_parent'>" . $object->engineManufacturer . "</a>";
            } else {
                print $object->engineManufacturer;
            }
            print '</td>'
                    . '<td>' . $object->engineModel . '</td>'
                    . '<td>' . $object->engineVersion . '</td><tr>';
        }
    }
    ?>
</table>