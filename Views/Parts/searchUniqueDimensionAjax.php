<?php
include_once("../../Models/Database.php");
require_once '../../Helpers/charsetFunctions.php';

$dimOD = '';
$dimPD = '';
$dimLength = '';
$dimDIN = '';
$dimHeadType = '';
$dimAngle = '';
$dimID = '';

if (isset($_GET['dimOD']) && ($_GET['dimOD'] != "")) {
    $dimOD = " dim_outer_diameter = '".$_GET['dimOD']."' AND ";
}
if (isset($_GET['dimPD']) && ($_GET['dimPD'] != "")) {
    $dimPD = "dim_profile_diameter = '".$_GET['dimPD']."' AND ";
}
if (isset($_GET['dimLength']) && ($_GET['dimLength'] != "")) {
    $dimLength = "dim_length = '".$_GET['dimLength']."' AND ";
}
if (isset($_GET['dimDIN']) && ($_GET['dimDIN'] != "")) {
    $dimDIN = "dim_din_number = '".$_GET['dimDIN']."' AND ";
}
if (isset($_GET['dimHeadType']) && ($_GET['dimHeadType'] != "")) {
    $dimHeadType = "dim_head_type = '".$_GET['dimHeadType']."' AND ";
}
if (isset($_GET['dimAngle']) && ($_GET['dimAngle'] != "")) {
    $dimAngle = "dim_valve_or_seat_angle_in_degrees = '".$_GET['dimAngle']."' AND ";
}
if (isset($_GET['dimID']) && ($_GET['dimID'] != "")) {
    $dimID = "dim_inner_diameter = '".$_GET['dimID']."' AND ";
}


    $query ="SELECT * FROM dimensions_tbl WHERE $dimOD $dimPD $dimLength $dimDIN $dimHeadType $dimAngle $dimID";
    $query = rtrim($query);
    $queryOne = substr($query, 0, (strlen($query)-4));


    $result = mysqli_query(Database::$connection, $queryOne);
    while($row = mysqli_fetch_assoc($result)) {
        print "<p><button class='useThis' type='button' id='".$row['dim_id']."' onclick='useDim(this);' data-id='".$row['dim_outer_diameter'].", ".$row['dim_profile_diameter'].", ".$row['dim_length'].", ".$row['dim_din_number'].", ".$row['dim_head_type'].", ".$row['dim_valve_or_seat_angle_in_degrees'].", ".$row['dim_inner_diameter']."' >Use this</button><br/>";
        print "Dim OD: ".$row['dim_outer_diameter']."mm<br/>";
        print "Dim ID: ".$row['dim_inner_diameter']." mm<br/>";
        print "Dim PD: ".$row['dim_profile_diameter']."mm<br/>";
        print "Dim Length :".$row['dim_length']."mm<br/>";
        print "Dim DIN :".$row['dim_din_number']."<br/>";
        print "Dim Head Type :".$row['dim_head_type']."<br/>";
        print "Dim Angle: ".$row['dim_valve_or_seat_angle_in_degrees']." Degrees<br/>";
        print "</p>";
    }
        
    