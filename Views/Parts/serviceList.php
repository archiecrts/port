<script>
$(document).ready(function(){
    $("#advanced").hide();
    $("#hideAdvanced").hide();

    $("#showAdvanced").click(function(){
        $("#advanced").show();
        $("#showAdvanced").hide();
        $("#hideAdvanced").show();
    });
    
    $("#hideAdvanced").click(function(){
        $("#advanced").hide();
        $("#hideAdvanced").hide();
        $("#showAdvanced").show();
    });
});
</script>

        <h3>Create A Servicing Interval List For Chosen Engine</h3>
        <p>Each engine make requires a set of servicing intervals. These can be regular, major and minor intervals.</p>
        <p>Number of intervals should be the total number including any minor intervals you wish to add from the advanced settings.</p>
        <form name="form" action="index.php?portal=parts&page=confirmServiceList" method="post" enctype="multipart/form-data">
        <table>
            
            <tr>
                <td>Select an Engine Builder</td>
                <td>
                    <select name="make" id="list-select">
                            <option>Select One</option>
                            <?php
                            //execute the SQL query and return records
            $result = mysqli_query("SELECT * FROM engine_builder_tbl");
            
            
            //fetch tha data from the database
            while ($row = mysqli_fetch_array($result)) {
               echo '<option value="'.$row{'eb_id'}.'">'.$row{'eb_engine_builder_name'}."</option>";
            }
                            ?>
                        </select> 
                </td>
            </tr>
            <tr>
                <td>Total Number of Intervals</td>
                <td><input type="text" name="noOfColumns" value="50" /></td>
            </tr>
            <tr>
                <td>Initial Interval to Set</td>
                <td><input type="text" name="intervalToSet" value='1000' /></td>
            </tr>
            <tr>
                <td>Mark Major Service Every</td>
                <td><input type="text" name="markMajor" value="10000"/></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" name="submit" value="Create Intervals"/></td>
            </tr>
        </table>
        
        <h3>Advanced Settings</h3>
        <p>If the servicing schedule has randomly placed minor services, enter them here</p>
        <p>
        <input type="button" name="showAdvanced" id="showAdvanced" value="Show" />
        <input type="button" name="hideAdvanced" id="hideAdvanced" value="Hide" />
        </p>
        <div id="advanced">
        
        <p>Example: MINOR INTERVAL: 500</p>
        <p>Minor Interval <input type="text" name="minorInterval" value="" /></p>
        <input type="button" id="button" onlick="duplicate()" value="Add New Range"/>
        <p>Example Values: MIN: 0 MAX: 10000 </p>
        <div id="duplicator"> 
        <table>
            <tr>
                <td>Range To Include</td>
                <td>Min <input type='text' name="min[]" value=""/> Max <input type='text' name="max[]" value=""/></td>
            </tr>
            
        </table>
        </div>
        
        <script type='text/javascript'>//<![CDATA[ 

document.getElementById('button').onclick = duplicate;


var i = 0;
var original = document.getElementById('duplicator');

function duplicate() {
    var clone = original.cloneNode(true); // "deep" clone
    clone.id = "duplicator" + ++i; // there can only be one element with an ID
    original.parentNode.appendChild(clone);
}
//]]>  

</script>
        
        </div>
        </form>
    </body>
</html>
