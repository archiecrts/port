<h2>ADMIN - Add Engines List</h2>

<?php

if ($_FILES) {
    
    // SELF REFERENCING PAGE, BECAUSE THE ORIGINAL FORM AT THE BOTTOM DOES SO LITTLE.
    
    // Check that file is .csv
    $fileExt = explode('.', $_FILES["file"]["name"]);
        
    if ($fileExt[count($fileExt)-1] != 'csv') {
        echo "Sorry, you must use a CSV";
    } else {
        
        // Check the file for errors, then save into the upload directory
        
        if ($_FILES["file"]["error"] == 0) {
            $tmp_name = $_FILES["file"]["tmp_name"];
            $name = $_FILES["file"]["name"];
            move_uploaded_file($tmp_name, "upload/$name");
        }
        
        $fileName = 'upload/'.$_FILES['file']['name'];
        

// Read a file into an array.-------------------------------

function csv_in_array($url,$delm=",",$encl="\"",$head=false) { 
    
    $csvxrow = file($url);   // ---- csv rows to array ----
    
    $csvxrow[0] = chop($csvxrow[0]); 
    $csvxrow[0] = str_replace($encl,'',$csvxrow[0]); 
    $keydata = explode($delm,$csvxrow[0]); 
    $keynumb = count($keydata); 
    
    if ($head === true) { 
    $anzdata = count($csvxrow); 
    $z=0; 
    for($x=1; $x<$anzdata; $x++) { 
        $csvxrow[$x] = chop($csvxrow[$x]); 
        $csvxrow[$x] = str_replace($encl,'',$csvxrow[$x]); 
        $csv_data[$x] = explode($delm,$csvxrow[$x]); 
        $i=0; 
        foreach($keydata as $key) { 
            $out[$z][$key] = $csv_data[$x][$i]; 
            $i++;
            }    
        $z++;
        }
    }
    else { 
        $i=0;
        foreach($csvxrow as $item) { 
            $item = chop($item); 
            $item = str_replace($encl,'',$item); 
            $csv_data = explode($delm,$item); 
            for ($y=0; $y<$keynumb; $y++) { 
               $out[$i][$y] = $csv_data[$y]; 
            }
        $i++;
        }
    }

return $out; 
}

$csvdata = csv_in_array( $fileName, ",", "\"", false ); 

// --------------------------------------------------------------

        $engineCount=0;
        $engineModelCount=0;
        $modelVersionCount=0;
        
        foreach ($csvdata as $key => $value) {
            
            
            /* 
             * first check if the engine in column 1 already exists in the engine builder table
             * if it exists then get its ID, otherwise enter it into the table and get the new ID.
             */
            $resultMake = mysqli_query("SELECT * FROM engine_builder_tbl "
                    . "WHERE eb_engine_builder_name='".addslashes($value[0])."'");
            
            $rowMake = mysqli_fetch_row($resultMake);
            
            
            //fetch tha data from the database
            $engineBuilderId;
            if ($rowMake == NULL) {
                // Does not exist already
                
                // Make sure engine name isnt blank.
                if ($value[0] != ""){
                    $builder_insert = "INSERT INTO engine_builder_tbl (eb_engine_builder_name) "
                            . "VALUES ('".addslashes($value[0])."')";

                    if (!mysqli_query($builder_insert)) {
                        echo "MAKE ".$value[0]." Can't insert records : " . mysqli_error() . "<br/><br/>";
                    }
                    $engineBuilderId=mysqli_insert_id();

                    $engineCount++;
                } else {
                    // The engine hasnt been created so make sure the ID is null.
                    $engineBuilderId=NULL;
                }
            } else {
                // Use the ID in the table for the model insert below.
                $engineBuilderId = $rowMake[0];
            }

            
            
            
            
            /* 
             * Check if the model exists in the table already, if it does then get its ID, otherwise
             * add it to the table and get the new ID.
             */
            
            // First check that an engine exists for this row.
            if ($engineBuilderId != NULL) {
                $engineModelId;
                $resultModel = mysqli_query("SELECT * FROM engine_model_tbl "
                        . "WHERE em_model_description='".addslashes($value[1])."' "
                        . "OR em_model_description='MODEL NOT GIVEN' "
                        . "AND engine_builder_tbl_eb_id='".$engineBuilderId."'");

                $rowModel = mysqli_fetch_row($resultModel);
                //fetch tha data from the database

                if ($rowModel == NULL) {
                    // Does not exist already
                    // make sure the model name isnt empty, set to obviously not set name.
                    if ($value[1] == "") {
                        $value[1] = "MODEL NOT GIVEN";
                    }
                    $model_insert = "INSERT INTO engine_model_tbl (em_model_description, engine_builder_tbl_eb_id) "
                        . "VALUES ('".addslashes($value[1])."','".$engineBuilderId."')";

                    if (!mysqli_query($model_insert)) {
                        echo "MODEL ".$value[0].":".$value[1]." Can't insert records : " . mysqli_error() . "<br/><br/>";
                    }


                    // INSERT VERSION
                    $engineModelId = mysqli_insert_id();

                    $engineModelCount++;
                } else {
                    // Use the ID in the table.
                    $engineModelId = $rowModel[0];
                }
            }
            
            
            
            
            /* 
             * Check if the version (if one is present) already exists for the model ID given. If it exists ignore it, if 
             * it doesnt then create it.
             */
            if (array_key_exists(2, $value)){
                
                if ($value[2] == "") {
                    $value[2] = 'VERSION NOT GIVEN';
                }
                
                $resultVersion = mysqli_query("SELECT * FROM engine_model_version_tbl "
                        . "WHERE mv_description='".addslashes($value[2])."' "
                        . "AND engine_model_tbl_em_id='".$engineModelId."'");

                $rowVersion = mysqli_fetch_row($resultVersion);
                //fetch tha data from the database

                if ($rowVersion == NULL) {
                    // Does not exist already
                    $version_insert = "INSERT INTO engine_model_version_tbl (mv_description, engine_model_tbl_em_id) "
                            . "VALUES ('".addslashes($value[2])."','".$engineModelId."')";

                    if (!mysqli_query($version_insert)) {
                        echo "VERSION ".$value[0].":".$value[1].":".$value[2]." Can't insert records : " . mysqli_error() . "<br/><br/>";
                    }
                    
                    $modelVersionCount++;
                }
            } else {
                // ENTER A DEFAULT VALUE
                $version_insert = "INSERT INTO engine_model_version_tbl (mv_description, engine_model_tbl_em_id) "
                            . "VALUES ('VERSION NOT GIVEN','".$engineModelId."')";

                if (!mysqli_query($version_insert)) {
                    echo "VERSION ".$value[0].":".$value[1].":".$value[2]." Can't insert records : " . mysqli_error();
                }
                
                $modelVersionCount++;
            }
        }
        print "Inserted ".$engineCount." new Engine Builders/Makes<br/>";
        print "Inserted ".$engineModelCount." new Engine Models<br/>";
        print "Inserted ".$modelVersionCount." new Model Versions<br/>";
    }
} else {
?>
<form name="form" action="index.php?portal=parts&page=addMultipleEngines&action=upload" method="post" enctype="multipart/form-data">
    
    
    <p>Choose File <input type="file" name="file" id="file" /></p>
    
    <p><input type="submit" name="submit" value="Save" /></p>
</form>
<?php

}
?>
    </body>
</html>
