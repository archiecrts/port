<?php

// catch if fields have not been filled in.
foreach($_POST as $key => $value) {
    if ($value == "") {
        header("location:index.php?portal=parts&page=generate&error=error");
    }
}

        /**
         * $modelVersionId: The version ID of the engine model.
         * $configuration: The configuration of the engine V or Inline.
         * $cylinders: The number of cylinders to search for.
         */
        $modelVersionId=$_POST['version'];
        $configuration=$_POST['configuration'];
        $cylinders=$_POST['cylinders'];
        
        
        /**
         * Query the engine servicing intervals table for the POST service variable.
         */
        $db1 = mysqli_query("SELECT esi_interval FROM engine_servicing_intervals_tbl "
                . "WHERE esi_id='".$_POST['service']."'");
        $rowDB = mysqli_fetch_assoc($db1);
        $service=$rowDB['esi_interval'];  
        
        /**
         * Query the description of the model version to display to the user.
         */
        $result1 = mysqli_query("SELECT mv_description FROM engine_model_version_tbl "
                . "WHERE mv_id='".$modelVersionId."'");
        $row1 = mysqli_fetch_assoc($result1);
        $modelVersionDescription=$row1['mv_description'];  
        
        /**
         * Query the description of the engine model to display to the user.
         */
        $result2 = mysqli_query("SELECT em_model_description FROM engine_model_tbl "
                . "WHERE em_id='".$_POST['model']."'");
        $row2 = mysqli_fetch_assoc($result2);
        $engineModelName=$row2['em_model_description'];
         
        /**
         * Query the engine builder/make name to display to the user.
         */
        $result3 = mysqli_query("SELECT eb_engine_builder_name FROM engine_builder_tbl "
                . "WHERE eb_id='".$_POST['make']."'");
        $row3 = mysqli_fetch_assoc($result3);
        $engineBuilderName= $row3['eb_engine_builder_name'];
        
        //execute the SQL query and return records
        $result4 = mysqli_query("SELECT * FROM engine_parts_manifest_tbl "
                . "WHERE engine_model_version_tbl_mv_id='".$modelVersionId."' "
                . "AND epm_v_inline='".$configuration."' "
                . "AND epm_".$cylinders."cyl_quantity >'0' "
                . "AND epm_id IN (SELECT engine_parts_manifest_tbl_epm_id FROM service_tbl"
                . " WHERE engine_servicing_intervals_tbl_esi_id='".$_POST['service']."')"
                );

        // Start a new line count - PHP starts counting at 0.
        $lineCount = 0;
        // Time is used as a unique identifier to stop files being overwritten.
        $engineName = preg_replace("/[^[:alnum:]]/ui", '_', $engineBuilderName.$engineModelName.$modelVersionDescription);
        $time = time()."".$engineName;
        // Create the new parts manifest file.
        $manifest = fopen("download/".$time."_.csv", "w") or die("Unable to open file!");
        
        /**
         * The file requires an Order Number (order of lines) and a Line number (users Order of lines).
         * currently order number is supplied for both because it not being used.
         */
        $orderNumber=1;
        $text = "Order, LineNumber, CustomerNotes, Quantity, Description\r\n";
        fwrite($manifest, $text);
        while ($row = mysqli_fetch_array($result4)) {

                $text = $orderNumber.",".$orderNumber.",".$row{'epm_stc_code'}.",".$row{'epm_'.$cylinders.'cyl_quantity'}.",".str_replace(',', '' , $row{'epm_description'})."\r\n";
                fwrite($manifest, $text);
                $lineCount++;
                $orderNumber++;
        }
        fclose($manifest);
       
  
?>
   
        <h2>Download Parts List Screen</h2>
        
        <p>The Servicing Parts List for:</p>
        <table>
            <tr>
                <td>Engine:</td>
                <td><?php echo $engineBuilderName; ?></td>
            </tr>
            <tr>
                <td>Model:</td>
                <td><?php echo $engineModelName; ?></td>
            </tr>
            <tr>
                <td>Version:</td>
                <td><?php echo $modelVersionDescription; ?></td>
            </tr>
            <tr>
                <td>Configuration:</td>
                <td><?php echo $configuration; ?> / <?php echo $cylinders; ?> Cylinders</td>
            </tr>
            <tr>
                <td>Service</td>
                <td><?php echo $service; ?> Hours</td>
            </tr>
        </table>
        
        <?php
        
        // Reads the file into an array
        $lines = file("download/".$time."_.csv");
        // Cuts off everything after the first three
        $the_rest = array_splice($lines, 6);
        // leaving the first three in the original array $lines
        $first_three = $lines;
        ?>
        <h3>First 5 lines of generated file</h3>
        <table class="borderedTable">
            
        
        <?php
        foreach($first_three as $key => $value)
        {
            $line = explode(",", $value);
           echo "<tr>";
           echo "<td>".$line[0]."</td>";
           echo "<td>".$line[1]."</td>";
           echo "<td>".$line[2]."</td>";
           echo "<td>".$line[3]."</td>";
           echo "<td>".$line[4]."</td>";
           echo "</tr>";
        }


       
        ?>
        </table>
        <p>Contains <?php echo $lineCount; ?> lines </p>
        
        <p><a href="<?php echo "download/".$time."_.csv"; ?>">Save list to desktop</a></p>
    </body>
</html>