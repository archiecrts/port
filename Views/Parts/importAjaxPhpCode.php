<?php
if (isset($_SESSION['server'])) {
    $server = $_SESSION['server'];
} else if (isset($_GET['server'])) {
    $server = $_GET['server'];
}
include_once("../../Models/Database.php");



$fileName = "../../".$_POST['fileName'];
$modelVersionId = $_POST['modelVersionId'];
// INSERT INTO MANIFEST TABLE
        $insert_query = "LOAD DATA LOCAL INFILE '".$fileName."' 
                INTO TABLE engine_parts_manifest_tbl
                COLUMNS TERMINATED BY ','
                ENCLOSED BY '\"'
                LINES TERMINATED BY '\r\n'
                (epm_v_inline, epm_stc_code, epm_oe_number,
                epm_alt_number, epm_description, epm_weight, epm_price, epm_price_year, 
                epm_wear_item, epm_in_stock,
                epm_min_stock_level, epm_main_supplier, epm_4cyl_quantity, epm_5cyl_quantity, epm_6cyl_quantity,
                epm_7cyl_quantity, epm_8cyl_quantity, epm_9cyl_quantity, epm_10cyl_quantity,
                epm_12cyl_quantity, epm_14cyl_quantity, epm_16cyl_quantity,
                epm_18cyl_quantity, epm_20cyl_quantity)
                SET engine_model_version_tbl_mv_id=".$modelVersionId."
                ";
        
		if (!mysqli_query(Database::$connection, $insert_query)) {
			echo "Can't insert records : " . mysqli_error(Database::$connection);
		} else {
			echo "You have successfully inserted records into parts manifest table";
		}
        

        // Delete the duplicate entries (just in case someone uploads the same one twice.
        // This also means updated lists can be added and former data wont be altered.
        // The x is on the end of the sub query, it serves as an alias. DO NOT DELETE IT.
        $delete_duplicates_query = "DELETE FROM engine_parts_manifest_tbl WHERE epm_id NOT IN 
                                    (SELECT * FROM 
                                    (SELECT MIN(n.epm_id) FROM engine_parts_manifest_tbl n
                                    GROUP BY n.engine_model_version_tbl_mv_id, n.epm_oe_number) x)";
        
        if (!mysqli_query(Database::$connection, $delete_duplicates_query)) {
                echo "Can't delete records : " . mysqli_error(Database::$connection);
        } else {
                echo "<br/>Duplicate records have been removed.";
        }
        
        
        // SERVICING INTERVAL.
        include("importServiceIntervalData.php");
        
        
        // Clean up the inserted records by removing blank lines. Sadly this cannot be done in
        // the above queries.
 
        // Clean up Engine Parts Manifest Table
        $delete_query_pm = "delete from engine_parts_manifest_tbl where epm_oe_number='0'";
        if (!mysqli_query(Database::$connection, $delete_query_pm)) {
                echo "Can't delete records : " . mysqli_error(Database::$connection);
        } else {
                echo "<br/>Data has been processed and empty records removed.";
                echo "<p><a href='index.php'>Finish</a></p>";
        }
