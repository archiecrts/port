<script type="text/javascript">
    // MODELS drop down
   $(document).ready(function($) {
  var list_target_id = 'list-target'; //first select list ID (MODELS)
  var list_select_id = 'list-select'; //second select list ID (MAKES)
  var initial_target_html = '<option value="">Please select a make...</option>'; //Initial prompt for target select
 
  $('#'+list_target_id).html(initial_target_html); //Give the target select the prompt option
 
  $('#'+list_select_id).change(function(e) {
    //Grab the chosen value on first select list change
    var selectvalue = $(this).val();
 
    //Display 'loading' status in the target select list
    $('#'+list_target_id).html('<option value="">Loading...</option>');
 
    if (selectvalue == "") {
        //Display initial prompt in target select if blank value selected
       $('#'+list_target_id).html(initial_target_html);
    } else {
      //Make AJAX request, using the selected value as the GET
      var server = '<?php print $_SESSION['server']; ?>';
      $.ajax({url: 'Views/Parts/generateDynamicDropDown.php?svalue='+selectvalue + '&server=' + server,
             success: function(output) {
                //alert(output);
                $('#'+list_target_id).html(output);
            },
          error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status + " "+ thrownError);
          }});
        }
    });
});
// Version Drop Down Function
$(document).ready(function($) {
  var list_target_id = 'list-version'; //first select list ID (VERSIONS)
  var list_select_id = 'list-target'; //second select list ID (MODELS)
  var initial_target_html = '<option value="">Please select a model...</option>'; //Initial prompt for target select
 
  $('#'+list_target_id).html(initial_target_html); //Give the target select the prompt option
 
  $('#'+list_select_id).change(function(e) {
    //Grab the chosen value on first select list change
    var selectvalue = $(this).val();
 
    //Display 'loading' status in the target select list
    $('#'+list_target_id).html('<option value="">Loading...</option>');
 
    if (selectvalue == "") {
        //Display initial prompt in target select if blank value selected
       $('#'+list_target_id).html(initial_target_html);
    } else {
      //Make AJAX request, using the selected value as the GET
      var server = '<?php print $_SESSION['server']; ?>';
      $.ajax({url: 'Views/Parts/generateVersionDropDown.php?vvalue='+selectvalue + '&server=' + server,
             success: function(output) {
                //alert(output);
                $('#'+list_target_id).html(output);
            },
          error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status + " "+ thrownError);
          }});
        }
    });
});
</script>
<div id="leftColumn">
        <h2>Add Engine Screen</h2>
        
        <?php
        $error;
        $action = "";
        if (isset($_GET['action'])) {
            $action = $_GET['action'];
        }
        switch($action) {
            case "confirmAddEngine":

                if ((($_POST['engineBuilder'] == "" || $_POST['engineBuilder'] == "Select One") && $_POST['make'] == "") 
                        || (($_POST['chooseModel'] == "" || $_POST['chooseModel'] == "Select One") && $_POST['model'] == ""
                                || ($_POST['version'] == ""))) {

                    $error = "error";
                } else {
                    $engineBuilderId;
                    if ($_POST['engineBuilder'] != NULL && $_POST['engineBuilder'] != "Select One") {
                        $engineBuilderId = $_POST['engineBuilder'];
                    } else {    
                        // INSERT ENGINE BUILDER/MAKE
                        $engineBuilderId = $engineManufacturerController->setEngineBuilder(addslashes($_POST['make']));
                    }
                    
                    //INSERT MODEL
                    $engineModelId;
                    if ($_POST['chooseModel'] != NULL) {
                        $engineModelId = $_POST['chooseModel'];
                    } else {
                        // INSERT VERSION
                        $engineModelId = $engineModelController->setEngineModelEngineDB(addslashes($_POST['model']), $engineBuilderId);
                    }
                    
                    
                    if (isset($_POST['version'])) {
                        $versionInsert = $engineModelVersionController->setEngineModelVersion(addslashes($_POST['version']), $engineModelId);

                        if ($versionInsert == true) {
                            print "<p><strong>Engine successfully entered</strong></p>";
                        } else {
                            print "Error inserting engine<br/>";
                        }
                    }
                }
                break;
            default:
                
                break;
        }
        

        // catch and display the error that all fields were not selected before attempting an import.
        if(isset($error)) {
            print "<span class='red'>Please select an option from each drop down or fill in the relevant fields.</span>";
        }
        
        ?>
        <form name="loginForm" action="index.php?portal=parts&page=addEngine&action=confirmAddEngine" method="post">
            <table>
                <tr>
                    <td>Engine Manufacturer</td>
                    <td><select name="engineBuilder" id="list-select">
                        <option>Select One</option>
                            <?php
                            //execute the SQL query and return records
                           $engineBuildersArray = $engineManufacturerController->getEngineBuilders();
                           //fetch tha data from the database
                            foreach ($engineBuildersArray as $key => $object) {
                               echo '<option value="'.$key.'">'.$object->engineBuilder."</option>";
                            }
                            ?>
                        </select>
                        
                        </td>
                </tr>
                <tr>
                    <td>Or add new manufacturer name</td>
                    <td><input type="text" name="make" /></td>
                </tr>
                <tr>
                    <td>Model</td>
                    <td><select name="chooseModel" id="list-target"><!-- AJAX GENERATED --></select></td>
                </tr>
                <tr>
                    <td>Or add new model </td>
                    <td><input type="text" name="model" /></td>
                </tr>
                <tr>
                    <td>Version</td>
                    <td><input type="text" name="version" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" name="submit" value="Add New Engine" /></td>
                </tr>
            </table>
        </form>
</div>
<div id="rightColumn">
        <h4>Current Engines</h4>
        <!--<iframe src="Views/Parts/enginesList.php" frameborder='0' width='500' height="300"></iframe>-->
        <div><?php  
        include_once 'Views/Parts/enginesList.php';
        
        ?></div>
</div

    </body>
</html>
