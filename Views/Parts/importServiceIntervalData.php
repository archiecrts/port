<?php
    
    
//print "SERVICE INTERVAL";
    
// the purpose of this section is to read each line as of the servicing columns.
// match those columns to the servicing table and create an entry every time a Y is found.

// Set known variables.
$make = $_POST['make'];
// read each line of the file.
//explode each line into an array and discard the unwanted columns.
// for each array item give it a number key from 1 to however many intervals exist.
// if an array item value is 'Y' then create an entry in the table for that item.
$row = 1;
if (($handle = fopen($fileName, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 4000, ",")) !== FALSE) {
        $num = count($data);
        //echo "<p> $num fields in line $row: <br /></p>\n";
        $row++;
        for ($c=0; $c < $num; $c++) {
            $epm_result = mysqli_query("SELECT epm_id FROM engine_parts_manifest_tbl WHERE "
                    . "engine_model_version_tbl_mv_id='".$modelVersionId."' "
                    . "AND epm_oe_number='$data[2]' ");//AND epm_alt_number='$data[3]]'
            echo "<strong>".$data[$c]."</strong> | ";
            $idRow = mysqli_fetch_assoc($epm_result);
            echo "<strong>".$data[$c]."</strong> | ";
            if ($c >= 24) {
                $col = $c-23;
                if (($data[$c] === "Y") || ($data[$c] === "y")) {
                    
                    $esi_result = mysqli_query("SELECT esi_id, esi_interval FROM engine_servicing_intervals_tbl WHERE "
                            . "engine_builder_tbl_eb_id='".$make."' "
                            . "AND esi_related_column_number='".$col."'");
            
                    $esiRow = mysqli_fetch_assoc($esi_result);
                    
                    // print to logs
                    //echo $c." " .$idRow['epm_id']." ".$esiRow['esi_id']." ".$esiRow['esi_interval']."<br />\n";
                    $insert = "INSERT INTO service_tbl (engine_parts_manifest_tbl_epm_id, "
                            . "engine_servicing_intervals_tbl_esi_id) "
                            . "VALUES ('".$idRow['epm_id']."', '".$esiRow['esi_id']."')";

                    if (!mysqli_query($insert)) {
                        echo "Can't insert records : " . mysqli_error() . "<br/>";
                    } else {
                        //echo "Service Scheduled<br />";
                        // perhaps print to logs
                    }
                }
            }
        }
    }
    fclose($handle);
}

?>