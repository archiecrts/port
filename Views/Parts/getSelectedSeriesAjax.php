<?php
include_once("../../Models/Database.php");
require_once '../../Helpers/charsetFunctions.php';

$designerStringArray = [];
$league = "";
if (isset($_GET['designerArray'])) {
    $_GET['designerArray'] = str_replace("-amp-", "&", $_GET['designerArray']);
    $designerStringArray = explode(",", $_GET['designerArray']);
}
if (isset($_GET['league']) && ($_GET['league'] == '1')) {
    $league = " AND mlt_stc_league_engine = '1' ";
}
$designerArray = [];
$string = "";

foreach ($designerStringArray as $value) {
    $des = mysqli_query(Database::$connection, "SELECT dlt_id FROM designer_lookup_tbl WHERE dlt_designer_description = '".addslashes($value)."'");
    $rowDes = mysqli_fetch_assoc($des);
    $designerID = $rowDes['dlt_id'];
    $result1 = mysqli_query(Database::$connection, "SELECT DISTINCT(mlt_series), mlt_id FROM series_lookup_tbl WHERE 
        designer_lookup_tbl_dlt_id = '$designerID' 
       GROUP BY mlt_series ORDER BY mlt_series ASC");
    while($row = mysqli_fetch_assoc($result1)) {
            if ($row['mlt_series'] == 'Not Specified') {
                $row['mlt_series'] = $row['mlt_series']." ($value)";
            }
            echo '<option value="' . htmlXentities($row['mlt_id']) . '">' . htmlXentities($row['mlt_series']) . '</option>';
        }
    
    
}