<h3>Product Manifest</h3>
    

<table>
    <tr>
    <th>Designer</th>
    <th>Series</th>
    <th>Cylinder Count</th>
    <th>Configuration</th>
    <th>View Manifest</th>
<?php

/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
$designerList = $designerLookupController->getDistinctDesigners();

// products list with add to manifest button.
if (isset($designerList)) {
    foreach ($designerList as $id => $designer) {
        print "<tr><td colspan='2' style='font-weight:bold; background:lightgrey;'>";
        print $designer->designerDescription;
        print "</td><td style='font-weight:bold; background:lightgrey;' colspan='3'></td></tr>";
        $productSeriesList = $seriesLookupController->getDistinctModelSeriesByDesigner($designer->designerID);
       //print_r($productSeriesList);    
        if (isset($productSeriesList)){
            foreach ($productSeriesList as $key => $series) {
                $versionList = $versionLookupController->getVersionIDBySeriesID($series->seriesID);
                print "<tr><td>&nbsp;</td>";
                print "<td style='font-weight:bold; background:lightgreen;' colspan='3'>".$series->seriesDescription." </td>"; //".$series->seriesID."
                print "<td style='font-weight:bold; background:lightgreen;'><a href='index.php?portal=parts&page=addToManifest&action=createGroup&seriesID=$series->seriesID'>Group Create</a></td>";
                if (isset($versionList)) {
                    //print "<pre>";
                    //print_r($versionList);
                    foreach ($versionList as $verID => $version) {
                        print "<tr><td></td><td></td>";
                        print "<td>".(($version->cylinderCount != '0') ? $version->cylinderCount : 'N/A' )."</td>";
                        print "<td>";
                            if ($version->cylinderConfiguration == 'A' && $version->cylinderCount == '0') {
                                print "N/A";
                            } else if ($version->cylinderConfiguration == 'A' && $version->cylinderCount != '0') {
                                print "Unknown";
                            } else {
                                print $version->cylinderConfiguration; 
                            }
                        print "</td>";
                        $manifest = $partManifestController->getManifestByVersionID($version->versionID);
                        print "<td>";
                        if (isset($manifest)) {
                            print "<a href=''>View</a>";
                        } else {
                            print "<a href=''>Create</a>";
                        }
                        print "</td>";
                        print "</tr>";
                    }
                    
                }
                print "</tr>";
            }
        }
    }
}    
?>
</table>