<?php


$chooseEngineVersion = filter_input(INPUT_GET, 'chosenEngineVersion');
if (isset($chooseEngineVersion)) {
    $mv_id = $chooseEngineVersion;
}

?>

<div id="leftColumn">
        <h2>Edit Engine Screen</h2>
        
        <?php
        
        if (isset($_POST['submit'])) {
            if (($_POST['submit'] == 'Save') 
                    && ($_POST['make'] != '')
                    && ($_POST['makeId'] != '')) {
                
                if ($_POST['model'] == '' ) {
                    $_POST['model'] = 'MODEL NOT GIVEN';
                }
                
                if ($_POST['version'] == '' ) {
                    $_POST['version'] = 'VERSION NOT GIVEN';
                }
            
                $updateEngineManufacturer = $engineManufacturerController->updateEngineBuilder($_POST['makeId'], $_POST['make']);
                $updateEngineModel = $engineModelController->updateEngineDBModel($_POST['modelId'], $_POST['model']);
                $updateEngineVersion = $engineModelVersionController->updateEngineModelVersion($_POST['versionId'], $_POST['version']);
                

                print "<h3>Engine has been updated</h3>";
            
            }
        }
        ?>
        <form name="loginForm" action="index.php?portal=parts&page=editEngine&action=edit" method="post">
            <?php
            $make = "";
            $makeId = "";
            $model = "";
            $modelId = "";
            $version = "";
            $versionId = "";
            
            if(isset($mv_id)) {
                
                // Select the engine from the database that was selected from the side list.
                $engine = $engineMakeModelVersionController->getEngineByVersionID($mv_id);
                
                $make = $engine['make'];
                $makeId = $engine['makeID'];
                $model = $engine['model'];
                $modelId = $engine['modelID'];
                $version = $engine['version'];
                $versionId = $mv_id;
            }
            
            
            ?>
            <p>This is for correcting spelling mistakes or adding missing information.
            Do not try to change the version or model of the engine
            unless you are sure otherwise the attached manifest will be invalid.</p>
            
            <?php
            
            if(isset($mv_id)) {
            // Check to see if a parts manifest exists in any way. Let the user know.
            
                        
                $countManifestRow = $engineMakeModelVersionController->getCountOfAssociatedServiceIntervals($mv_id);
                
                
                if ($countManifestRow > 0) {
                    echo '<h4>WARNING: There is a parts manifest attached to this engine.</h4>';
                }
                
                
            }
            ?>
            
            <table>
                <tr>
                    <td>Manufacturer name</td>
                    <td><input type="text" name="make" value="<?php echo $make; ?>" /> [Changing this will change all engines]</td>
                    <input type="hidden" name="makeId" value="<?php echo $makeId; ?>" />
                </tr>
                <tr>
                    <td>Model</td>
                    <td><input type="text" name="model" value="<?php echo $model; ?>" /></td>
                    <input type="hidden" name="modelId" value="<?php echo $modelId; ?>" />
                </tr>
                <tr>
                    <td>Version</td>
                    <td><input type="text" name="version" value="<?php echo $version; ?>" /></td>
                    <input type="hidden" name="versionId" value="<?php echo $versionId; ?>" />
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" name="submit" value="Save" /></td>
                </tr>
            </table>
        </form>
        </div>


        <div id="rightColumn">
        <h4>Current Engines</h4>
        <div><?php  
        $edit = 1;
        include_once 'Views/Parts/enginesList.php';
        
        ?></div>
        </div
        
        
    </body>
</html>
