

<script type="text/javascript">
    // MODELS drop down
   $(document).ready(function($) {
  var list_target_id = 'list-target'; //first select list ID (MODELS)
  var list_select_id = 'list-select'; //second select list ID (MAKES)
  var initial_target_html = '<option value="">Please select a make...</option>'; //Initial prompt for target select
 
  $('#'+list_target_id).html(initial_target_html); //Give the target select the prompt option
 
  $('#'+list_select_id).change(function(e) {
    //Grab the chosen value on first select list change
    var selectvalue = $(this).val();
 
    //Display 'loading' status in the target select list
    $('#'+list_target_id).html('<option value="">Loading...</option>');
 
    if (selectvalue == "") {
        //Display initial prompt in target select if blank value selected
       $('#'+list_target_id).html(initial_target_html);
    } else {
      //Make AJAX request, using the selected value as the GET
      var server = '<?php print $_SESSION['server']; ?>';
      $.ajax({url: 'Views/Parts/generateDynamicDropDown.php?svalue='+selectvalue + '&server=' + server,
             success: function(output) {
                //alert(output);
                $('#'+list_target_id).html(output);
            },
          error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status + " "+ thrownError);
          }});
        }
    });
});
// Version Drop Down Function
$(document).ready(function($) {
  var list_target_id = 'list-version'; //first select list ID (VERSIONS)
  var list_select_id = 'list-target'; //second select list ID (MODELS)
  var initial_target_html = '<option value="">Please select a model...</option>'; //Initial prompt for target select
 
  $('#'+list_target_id).html(initial_target_html); //Give the target select the prompt option
 
  $('#'+list_select_id).change(function(e) {
    //Grab the chosen value on first select list change
    var selectvalue = $(this).val();
 
    //Display 'loading' status in the target select list
    $('#'+list_target_id).html('<option value="">Loading...</option>');
 
    if (selectvalue == "") {
        //Display initial prompt in target select if blank value selected
       $('#'+list_target_id).html(initial_target_html);
    } else {
      //Make AJAX request, using the selected value as the GET
      var server = '<?php print $_SESSION['server']; ?>';
      $.ajax({url: 'Views/Parts/generateVersionDropDown.php?vvalue='+selectvalue + '&server=' + server,
             success: function(output) {
                //alert(output);
                $('#'+list_target_id).html(output);
            },
          error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status + " "+ thrownError);
          }});
        }
    });
});
</script>
        <h2>Import Screen</h2>
        <?php
        
        // catch and display the error that all fields were not selected before attempting an import.
        if(isset($_GET['error'])) {
            print "<span class='red'>Please select an option from each drop down.</span>";
        }
        if(isset($_GET['fileerror'])) {
            echo "<span class='red'>Sorry, you must include a CSV.</span>";
        }
        $engineBuildersArray = $engineManufacturerController->getDistinctEngineBuildersWithIntervals();
        ?>
        <p>Please use a CSV file. 'Save As' your excel file as CSV. CSV files contain no extra formatting information which could cause errors.</p>
        <p>Please remove any column headers before saving your file and uploading.</p>
        <p>Only completed engines (including a servicing schedule) will be presented. If your engine is missing, it may not
        have a servicing schedule created for it, speak to the administrator.</p>
        
        <form name=loginForm" action="index.php?portal=parts&page=uploadConfirmation" method="post" enctype="multipart/form-data">
            <table>
                <tr>
                    <td>Make</td>
                    <td><select name="make" id="list-select">
                            <option>Select One</option>
                            <?php
                            
            
                            //fetch tha data from the database
                            foreach ($engineBuildersArray as $key => $object) {
                               echo '<option value="'.$key.'">'.$object->engineBuilder."</option>";
                            }
                            ?>
                        </select> 
                    </td>
                </tr>
                <tr>
                    <td>Model</td>
                    <td><select name="model" id="list-target"><!-- AJAX GENERATED --></select></td>
                </tr>
                <tr>
                    <td>Version</td>
                    <td><select name="version" id="list-version"><!-- AJAX GENERATED --></select></td>
                </tr>
                <tr>
                    <td>Choose CSV/Excel Sheet</td>
                    <td><input type="file" name="file" id="file" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="Import CSV" name="submit" /></td>
                </tr>
            </table>
            
            
        </form>
    </body>
</html>
