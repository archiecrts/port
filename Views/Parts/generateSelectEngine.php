<h3>Generate New Parts List</h3>


<script type="text/javascript">
    $(function () {
        var list_target_id = {};
        var list_select_id = {};
        var list_version_id = {};



        list_target_id['list-target'] = 'list-target'; //second select list ID (SERIES)
        list_select_id['list-select'] = 'list-select'; //first select list ID (MANUFACTURER)
        list_version_id['list_version'] = 'list_version'; // third select list ID (VERISON)

        var initialTypeValue = $("#type").val();

        var initialManufacturer = "<?php echo $designerLookupController->getDesignerByVersionLookupID($product->versionLookupID)->designerID; ?>";
        var initialIDValue = "<?php echo $seriesLookupController->getSeriesByVersionLookupID($product->versionLookupID)->seriesID; ?>";
        


        $.ajax({url: 'Views/AddInstallation/dynamicSeriesDropDown.php?svalue=' + initialManufacturer + '&tvalue=' + initialTypeValue + '&selected=' + initialIDValue,
            success: function (output) {
                // alert(output);
                $('#' + list_target_id['list-target']).html(output);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status + " " + thrownError);
            }});




        $('#' + list_select_id['list-select']).change(function (e) {
           // var changedID = this.id;
            // alert("change " + this.id);
            //Grab the chosen value on first select list change
            var selectvalue = $(this).val();
            var typeValue = $('#type').val();

            //Display 'loading' status in the target select list
            $('#' + list_target_id['list-target']).html('<option value="">Loading...</option>');

            if (selectvalue === "") {
                //Display initial prompt in target select if blank value selected
                $('#' + list_target_id['list-target'] + ' option[value=' + initialIDValue + ']').prop('selected', 'true');
            } else {
                //Make AJAX request, using the selected value as the GET

                $.ajax({url: 'Views/AddInstallation/dynamicSeriesDropDown.php?svalue=' + selectvalue + '&tvalue=' + typeValue,
                    success: function (output) {
                        //  alert(output);
                        $('#' + list_target_id['list-target']).html(output);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status + " " + thrownError);
                    }});
            }
        });

        $('#' + list_target_id['list-target']).change(function (e) {
            //var changedID = this.id;
            //var target = "list-target_" + changedID[1];
            // alert("change " + this.id);
            //Grab the chosen value on first select list change
            var selectvalue = $(this).val();
            //var typeValue = $('#type').val();

            //Display 'loading' status in the target select list
            $('#' + list_version_id['list-version']).html('<option value="">Loading...</option>');

            if (selectvalue === "") {
                //Display initial prompt in target select if blank value selected
                $('#' + list_version_id['list-version'] + ' option[value=' + initialIDValue + ']').prop('selected', 'true');
            } else {
                //Make AJAX request, using the selected value as the GET

                $.ajax({url: 'Views/AddInstallation/dynamicVersionDropDown.php?svalue=' + selectvalue,
                    success: function (output) {
                        //alert(output);
                        $('#list-version').html(output);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status + " " + thrownError);
                    }
                });
            }
        });

    });
// Servicing Interval drop down
$(document).ready(function($) {
  var list_target_id = 'list-service'; //first select list ID (SERVICE)
  var list_select_id = 'list-version'; //second select list ID (MAKES)
  var initial_target_html = '<option value="">Please select a make...</option>'; //Initial prompt for target select
 
  $('#'+list_target_id).html(initial_target_html); //Give the target select the prompt option
 
  $('#'+list_select_id).change(function(e) {
    //Grab the chosen value on first select list change
    var selectvalue = $(this).val();
 
    //Display 'loading' status in the target select list
    $('#'+list_target_id).html('<option value="">Loading...</option>');
 
    if (selectvalue == "") {
        //Display initial prompt in target select if blank value selected
       $('#'+list_target_id).html(initial_target_html);
    } else {
      //Make AJAX request, using the selected value as the GET
      var server = '<?php print $_SESSION['server']; ?>';
      $.ajax({url: 'Views/Parts/generateServiceDropDown.php?dvalue='+selectvalue + '&server=' + server,
             success: function(output) {
                //alert(output);
                $('#'+list_target_id).html(output);
            },
          error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status + " "+ thrownError);
          }});
        }
    });
});
</script>
        <h4>Select Engine screen</h4>
        
        <?php
        
        // catch the posted back error for the user.
        if (isset($_GET['error'])) {
            print "<span class='red'>Select an option from every drop down please</span>";
        }
        
        $engineBuildersArray = $designerLookupController->getDistinctDesignersWithIntervals();
        
        ?>
        <p>Only completed engines (with service intervals) are shown.</p>
        <form name="loginForm" action="index.php?portal=parts&page=downloadPartsList" method="post">
            <table>
                <tr>
                    <td>Make</td>
                    <td><select name="engineManufacturer" id="list-select">
                            <option>Select One</option>
                            <?php
                            //fetch tha data from the database
                            foreach ($engineBuildersArray as $key => $object) {
                               echo '<option value="'.$key.'">'.$object->designerDescription."</option>";
                            }
                            ?>
                        </select> 
                        
                    </td>
                </tr>
                <tr>
                <td class="title">Series</td>
                    <td><select name="series" id="list-target" >
                            <option value="">Please Select a Make</option>
                        </select>     
                    </td>
            </tr>
                <tr>
                    <td class="title">Version</td>
                        <td>
                            <select name="versionID" id="list-version">
                                <option>Please Select Series...</option>
                            </select>
                        </td>
                </tr>
                <tr>
                    <td>Servicing Schedule</td>
                    <td>
                        <select name="service" id="list-service"><!--AJAX GENERATED--></select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="Create Parts List" name="submit" /></td>
                </tr>
            </table>
        </form>
    </body>
</html>