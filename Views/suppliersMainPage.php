<h3>Suppliers</h3>
<?php
require_once 'Helpers/charsetFunctions.php';

include_once 'Controllers/CustomerController.php';
include_once 'Controllers/InsertSuppliersFromCSVController.php';
include_once 'Controllers/SupplierApprovalStatusLookupController.php';
include_once 'Controllers/SupplierApprovalController.php';
include_once 'Controllers/SupplierPartsController.php';
include_once 'Controllers/PartDescriptionLookupController.php';
include_once 'Controllers/CustomerContactsController.php';
include_once 'Controllers/CustomerAddressController.php';
include_once 'Controllers/CustomerStatusLookupController.php';
include_once 'Controllers/CustomerStatusController.php';
include_once 'Controllers/CountryController.php';
include_once 'Controllers/UserTerritoryController.php';
include_once 'Controllers/UserController.php';
include_once 'Controllers/SeriesLookupController.php';
include_once 'Controllers/DesignerLookupController.php';
include_once 'Controllers/TypeOfProductLookupController.php';
include_once 'Controllers/InsertSuppliersProductsFromCSVController.php';
include_once 'Controllers/VersionLookupController.php';
include_once 'Controllers/PartCodeController.php';
include_once 'Controllers/PartDimensionsController.php';
include_once 'Controllers/PartColourController.php';
include_once 'Controllers/PartMaterialSpecController.php';
include_once 'Controllers/UserHistoryController.php';
include_once 'Controllers/PartManifestController.php';

include_once 'ControllersHistory/CustomerHistoryController.php';
include_once 'ControllersHistory/CustomerStatusHistoryController.php';
include_once 'ControllersHistory/CustomerAddressHistoryController.php';
include_once 'ControllersHistory/CustomerContactHistoryController.php';


$insertSuppliersFromCSVController = new InsertSuppliersFromCSVController();
$supplierApprovalStatusLookupController = new SupplierApprovalLookupModel();
$supplierApprovalController = new SupplierApprovalController();
$supplierPartsController = new SupplierPartsController();
$partDescriptionLookupController = new PartDescriptionLookupController();
$customerController = new CustomerController();
$countryController = new CountryController();
$customerAddressController = new CustomerAddressController();
$customerContactsController = new CustomerContactsController();
$customerStatusLookupController = new CustomerStatusLookupController();
$customerStatusController = new CustomerStatusController();
$userTerritoryController = new UserTerritoryController();
$userController = new UserController();
$seriesLookupController = new SeriesLookupController();
$designerLookupController = new DesignerLookupController();
$typeOfProductLookupController = new TypeOfProductLookupController();
$insertSuppliersProductsFromCSVController = new InsertSuppliersProductsFromCSVController();
$versionLookupController = new VersionLookupController();
$partCodeController = new PartCodeController();
$partDimensionsController = new PartDimensionsController();
$partColourController = new PartColourController();
$partMaterialSpecController = new PartMaterialSpecController();
$userHistoryController = new UserHistoryController();
$partManifestController = new PartManifestController();

$customerHistoryController = new CustomerHistoryController();
$customerStatusHistoryController = new CustomerStatusHistoryController();
$customerAddressHistoryController = new CustomerAddressHistoryController();
$customerContactHistoryController = new CustomerContactHistoryController();

switch ($_GET['page']) {
    case "manifest":
        include 'Views/Suppliers/partManifestList.php';
        break;
    case "report":
        include 'Views/Suppliers/searchSuppliers.php';
        break;
    case "addNewSupplier":
        include 'Views/Suppliers/addNewSupplier.php';
        break;
    case "upload":
        include 'Views/Uploads/uploadSupplierBulk.php';
        break;
    case "addProducts":
        include 'Views/Uploads/uploadSupplierProducts.php';
        break;
    case "supplierTable":

        $partCodeID = $_GET['partCodeID'];
        $partCodeRecords = $supplierPartsController->getAllSuppliersByPartCodeID($partCodeID);
        include 'Views/Suppliers/supplierTablePartCode.php';
        break;
    case "supplierTableCountry":

        
        print "<p>Default address displayed may not match country search but the company has one address in that country</p>";
        $country = [];
        $area = [];
        $continent = [];
        if (isset($_POST['country'])) {
            foreach ($_POST['country'] as $ckey => $cval) {
                $country[$ckey] = $cval;
            }
            
        }
        if (isset($_POST['allAreas'])) {
            $area = $_POST['allAreas'];
        }
        if (isset($_POST['continent'])) {
            $continent = $_POST['continent'];
        }


        $customerArray = $customerController->getCustomerByCountryArray($country, $area, $continent, 3);

        //print_r($customerArray);
        include 'Views/Suppliers/supplierTableCountry.php';
        break;
    case "supplierTableProduct":


        
        $typeOfProduct = "";
        $designer = "";
        $series = "";
        $count = 0;
        $configuration = '';
        
        if (isset($_POST['typeProduct'])) {
            $typeOfProduct = $_POST['typeProduct'];
        }
        
        if (isset($_POST['allProducts'])) {
            $designer = $_POST['allProducts'];
        }
        
        if (isset($_POST['allSeries'])) {
            $series = $_POST['allSeries'];
        }
        
        
        $customerArray = $customerController->getCustomerByProductConfiguration($designer, $series, $count, $configuration);
        
        include 'Views/Suppliers/supplierTableProduct.php';
        break;
    case "supplierRecord":
        $customerID = $_GET['customerID'];
        $customer = $customerController->getCustomerByID($customerID);
        $customerAddresses = $customerAddressController->getCustomerAddressesByCustomerID($customerID);

        if (($_GET['action'] == 'edit') || ($_GET['action'] == 'add')) {
            if ($_GET['tab'] == 'profile') {
                include 'Views/Suppliers/editSupplierProfile.php';
            }
            if ($_GET['tab'] == 'address') {
                if ($_GET['action'] == 'edit') {
                    include 'Views/Suppliers/editSupplierAddress.php';
                } else if ($_GET['action'] == 'add') {
                    include 'Views/Suppliers/editSupplierAddress.php';
                }
            }
            if ($_GET['tab'] == 'contacts') {
                include 'Views/Suppliers/editSupplierContacts.php';
            }
            if ($_GET['tab'] == 'product') {
                include 'Views/Suppliers/editProduct.php';
            }
            if ($_GET['tab'] == 'component') {
                
                include 'Views/Suppliers/editComponent.php';
            }
        } else if ($_GET['action'] == 'save') {
            if ($_GET['tab'] == 'profile') {

                $oldCustomerRecord = $customerController->getCustomerByID($customerID);
                $oldCustomerStatusArray = $customerStatusController->getAllCustomerStatusValuesByCustomerID($customerID);


                $saveCustomer = $customerController->getCustomerByID($customerID);
                $saveCustomer->customerName = addslashes($_POST['customerName']);
                $saveCustomer->mainTelephone = addslashes($_POST['mainTelephone']);
                $saveCustomer->mainFax = addslashes($_POST['mainFax']);
                $saveCustomer->website = addslashes($_POST['website']);
                $saveCustomer->headerNotes = addslashes($_POST['headerNotes']);
                $accManager = null;
                if ($_POST['accountManager'] != "") {
                    $accManager = $_POST['accountManager'];
                }
                $saveCustomer->accountManager = addslashes($accManager);


                $updateCustomer = $customerController->updateCustomerObject($saveCustomer);
                $customer = $customerController->getCustomerByID($customerID);
                $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "update", "Customer Updated: Customer ID " . $customerID);

                // History DB entry
                $customerHistory = new HistoryCustomer("", $oldCustomerRecord->customerID, $oldCustomerRecord->customerName, $oldCustomerRecord->mainTelephone, $oldCustomerRecord->mainFax, $oldCustomerRecord->website, $oldCustomerRecord->accountManager, $oldCustomerRecord->tbhID, $oldCustomerRecord->tbhAccountCode, $oldCustomerRecord->ldID, $oldCustomerRecord->ldAccountCode, $oldCustomerRecord->headerNotes, $_SESSION['user_id'], date("Y-m-d H:i:s", time()));
                $setCustomerHistory = $customerHistoryController->setCustomerHistory($customerHistory);


                // Set Status
                $customerStatusController->deleteCustomerStatusObjectsByCustomerID($customerID);

                foreach ($_POST as $key => $value) {
                    if (substr($key, 0, -2) == "status") {

                        $saveStatus = new CustomerStatus("", $customerID, $value);
                        $newStatus = $customerStatusController->setNewCustomerStatusObject($saveStatus);
                    }
                }


                // Set Status History
                foreach ($oldCustomerStatusArray as $key => $statusObject) {
                    $oldStatus = new HistoryCustomerStatus(
                            "", $statusObject->custStatusID, $statusObject->customerID, $statusObject->custStatusLookupID, $_SESSION['user_id'], date("Y-m-d H:i:s", time()));

                    $setCustomerStatusHistory = $customerStatusHistoryController->setCustomerStatusHistory($oldStatus);
                }
            }
            if ($_GET['tab'] == 'address') {

                if ($_GET['addressID'] != "") {
                    $customerID = $_GET['customerID'];


                    $oldAddressRecord = $customerAddressController->getCustomerAddressByID($_GET['addressID']);

                    $saveAddress = $customerAddressController->getCustomerAddressByID($_GET['addressID']);

                    if ($saveAddress->addressVerified == '0') {
                        if (isset($_POST['addressVerified'])) {
                            $newVerification = 1;
                        }
                    }
                    $saveAddress->officeName = $_POST['officeName'];
                    $saveAddress->address = $_POST['address'];
                    $saveAddress->city = $_POST['city'];
                    $saveAddress->state = $_POST['state'];
                    $saveAddress->country = $_POST['country'];
                    $saveAddress->postcode = $_POST['postcode'];
                    $saveAddress->addressVerified = 0;
                    if (isset($_POST['addressVerified'])) {
                        $saveAddress->addressVerified = $_POST['addressVerified'];
                    }
                    $saveAddress->defaultAddress = 0;
                    if (isset($_POST['defaultAddress'])) {
                        $saveAddress->defaultAddress = $_POST['defaultAddress'];
                    }
                    $saveAddress->archived = 0;
                    $update = "Updated";
                    if (isset($_POST['archived'])) {
                        $saveAddress->archived = $_POST['archived'];
                        $update = "Archived";
                    }

                    $updateCustomerAddress = $customerAddressController->updateCustomerAddress($saveAddress);
                    $customerAddresses = $customerAddressController->getCustomerAddressesByCustomerID($customerID);
                    // Log in the DB


                    $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "update", "Customer Address $update: Address ID " . $_GET['addressID']);
                    if (isset($newVerification)) {
                        $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "update", "Customer Address Verified: Address ID " . $_GET['addressID']);
                    }
                    // History DB entry
                    $addressHistory = new HistoryCustomerAddress("", $oldAddressRecord->addressID, $oldAddressRecord->officeName, $oldAddressRecord->address, $oldAddressRecord->city, $oldAddressRecord->state, $oldAddressRecord->country, $oldAddressRecord->postcode, $oldAddressRecord->addressVerified, $oldAddressRecord->customerID, $oldAddressRecord->archived, $oldAddressRecord->defaultAddress, $oldAddressRecord->fullAddress, $_SESSION['user_id'], date("Y-m-d H:i:s", time()));

                    $setCustomerAddressHistory = $customerAddressHistoryController->setCustomerAddressHistory($addressHistory);
                } else if ($_GET['addressID'] == "") {


                    $addressVerified = 0;
                    if (isset($_POST['addressVerified'])) {
                        $addressVerified = $_POST['addressVerified'];
                    }
                    $defaultAddress = 0;
                    if (isset($_POST['defaultAddress'])) {
                        $defaultAddress = $_POST['defaultAddress'];
                    }
                    $archived = 0;
                    if (isset($_POST['archived'])) {
                        $archived = $_POST['archived'];
                    }

                    $saveAddress = new CustomerAddress(
                            '', addslashes($_POST['officeName']), addslashes($_POST['address']), addslashes($_POST['city']), addslashes($_POST['state']), addslashes($_POST['country']), addslashes($_POST['postcode']), $addressVerified, $_GET['customerID'], $archived, $defaultAddress, "");


                    $setCustomerAddress = $customerAddressController->setCustomerAddress($saveAddress);
                    $customerAddresses = $customerAddressController->getCustomerAddressesByCustomerID($customerID);
                    // Log in the DB
                    $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "create", "Customer Address Added: Address ID " . $setCustomerAddress);
                }
            }
            if ($_GET['tab'] == 'contacts') {



                $inactive = 0;
                if (isset($_POST['inactive'])) {
                    $inactive = 1;
                }
                $contactValidated = 0;

                if (isset($_POST['contactValidated'])) {
                    $contactValidated = 1;
                }

                $customerAddressID = "";
                if (isset($_POST['customerAddressID'])) {
                    $customerAddressID = $_POST['customerAddressID'];
                }

                if (($_POST['contactID'] != "")) {

                    $oldContactRecord = $customerContactsController->getContactByID($_POST['contactID']);
                    $saveContact = new CustomerContact(
                            $_POST['contactID'], addslashes($_POST['salutation']), addslashes($_POST['firstName']), addslashes($_POST['surname']), addslashes($_POST['jobTitle']), addslashes($_POST['email']), addslashes($_POST['number']), addslashes($_POST['mobile']), addslashes($_POST['infoSource']), $inactive, $contactValidated, $customerID, $customerAddressID);



                    $updateObject = $customerContactsController->updateContactDetails($saveContact);

                    // Log in the DB
                    $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "update", "Customer Contact updated: ID " . $_POST['contactID']);


                    $contactHistory = new HistoryCustomerContact("", $oldContactRecord->customerContactID, $oldContactRecord->salutation, $oldContactRecord->firstName, $oldContactRecord->surname, $oldContactRecord->jobTitle, $oldContactRecord->email, $oldContactRecord->number, $oldContactRecord->mobile, $oldContactRecord->infoSource, $oldContactRecord->inactive, $oldContactRecord->contactValidated, $oldContactRecord->customerID, $oldContactRecord->customerAddressID, $_SESSION['user_id'], date("Y-m-d H:i:s", time()));

                    $setCustomerContactHistory = $customerContactHistoryController->setCustomerContactHistory($contactHistory);
                } else if (($_POST['contactID'] == "")) {
                    $saveContact = new CustomerContact(
                            $dummy = "", addslashes($_POST['salutation']), addslashes($_POST['firstName']), addslashes($_POST['surname']), addslashes($_POST['jobTitle']), addslashes($_POST['email']), addslashes($_POST['number']), addslashes($_POST['mobile']), addslashes($_POST['infoSource']), $inactive, $contactValidated, $customerID, $customerAddressID);

                    $setObject = $customerContactsController->setContact($saveContact);

                    // Log in the DB
                    $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "create", "Customer Contact Created: ID " . $setObject);
                }
            }
            if ($_GET['tab'] == 'product') {
                /*print "<pre>";
                print_r($_POST);
                print_r($_GET);
                print "</pre>";*/
            }
            if ($_GET['tab'] == 'component') {
               /* print "<pre>";
                print_r($_POST);
                print_r($_GET);
                print "</pre>";*/
                
                $supplierPart = new SupplierParts("", $_GET['customerID'], 0, $_POST['partCodeID'], 0, date("Y-m-d", time()), 0);
                
                $addedPart = $supplierPartsController->setSupplierPart($supplierPart);
                if ($addedPart != NULL) {
                print "ADDED PART ID ".$addedPart;
                }
            }
            if (isset($_POST['submitAddMore'])) {
                print "<p>Component Added</p>";
                include 'Views/Suppliers/editComponent.php';
            } else {
                include 'Views/Suppliers/supplierRecord.php';
            }
        } else if ($_GET['action'] == 'view') {
            include 'Views/Suppliers/supplierRecord.php';
        }
        break;
    default:
        include 'Views/Suppliers/searchSuppliers.php';
        break;
}

