<?php
if (isset($_POST['submit'])) {
    
    /*
     * I am putting in a file sweep/cleanup here that should only run once a day, but it makes sense that
     * it runs when the first person logs in.
     */
    include_once('Helpers/uploadSweeper.php');
    
    include_once('Controllers/UserController.php');

    $controller = new UserController();
    $accessControl = $controller->getUserAccessControl($_POST['username'], $_POST['password']);
    if ($accessControl == "SUCCESS") {
        print '<meta http-equiv="refresh" content="0;url=index.php?portal=mr"/>';
    } else {
        print $accessControl;
    }
}
?>

<h2>Login</h2>

<form name="loginForm" action="index.php?page=accessControl" method="post" enctype="multipart/form-data">
    <table>
        <tr>
            <td>Username</td>
            <td><input type="text" name="username" id="username" /></td>
        </tr>
        <tr>
            <td>Password</td>
            <td><input type="password" name="password" id="password" /></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" name="submit" value="Login" /></td>
        </tr>
        <!--<tr>
            <td colspan="2"><a href="index.php">forgotten password?</a></td>
        </tr>-->
    </table>
</form>
</body>
</html>