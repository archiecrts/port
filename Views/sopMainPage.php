<h3>Sales Order Processing</h3>


<?php
include_once 'Controllers/EnquirySourceController.php';
include_once 'Controllers/EnquiryTypeController.php';
include_once 'Controllers/UserController.php';
include_once 'Controllers/InstallationEngineController.php';
include_once 'Controllers/InstallationController.php';
include_once 'Controllers/EnquiryController.php';
include_once 'Controllers/EnquiryEngineController.php';
include_once 'Controllers/EngineManufacturerController.php';
include_once 'Controllers/EngineModelController.php';
include_once 'Controllers/CustomerContactsController.php';

include_once 'ControllersHistory/UserHistoryDBController.php';

$enquirySourceController = new EnquirySourceController();
$enquiryTypeController = new EnquiryTypeController();
$userController = new UserController();
$installationEngineController = new InstallationEngineController();
$installationController = new InstallationController();
$enquiryController = new EnquiryController();
$enquiryEngineController = new EnquiryEngineController();
$engineManufacturerController = new EngineManufacturerController();
$engineModelController = new EngineModelController();
$customerContactsController = new CustomerContactsController();

$userHistoryController = new UserHistoryController();

$page = "";
if (isset($_GET['page'])) {
    $page = $_GET['page'];
}

switch ($page) {
    case "stepOne": /* Choose a customer and enquiry type */
        include 'Views/Enquiries/enquiryChooseCustomer.php';
        break;

    case "stepTwo": /* Choose the engines that we need to quote on */
        include 'Views/Enquiries/enquiryStepTwo.php';
        break;

    case "addParts": /* Add the parts to each engine */

        /*print "<pre>";
        print_r($_POST);
        print "</pre>";*/

        $enquiryDate = "";
        $userID = "";
        $enquiryHeaderNote = "";
        $customerID = "";
        $enquirySourceID = "";
        $enquiryTypeID = "";
        $enquiryCustomerReference = "";
        $customerContactID = "";
        $includeInstallationInQuote = "";

        if (isset($_POST['calendar'])) {
            $enquiryDate = $_POST['calendar'];
        }
        if (isset($_POST['pointOfContact'])) {
            $userID = $_POST['pointOfContact'];
        }

        $enquiryHeaderNote;
        if (isset($_POST['customerID'])) {
            $customerID = $_POST['customerID'];
        }
        if (isset($_POST['enquirySource'])) {
            $enquirySourceID = $_POST['enquirySource'];
        }
        if (isset($_POST['enquiryType'])) {
            $enquiryTypeID = $_POST['enquiryType'];
        }
        if (isset($_POST['customerRef'])) {
            $enquiryCustomerReference = $_POST['customerRef'];
        }
        if (isset($_POST['customerContact'])) {
            $customerContactID = $_POST['customerContact'];
        }

        if (isset($_POST['useInst'])) {
            $includeInstallationInQuote = $_POST['useInst'];
        }

        /*
         * First make an enquiry object.
         */

        $enquiry = new Enquiry(
                "", $enquiryDate, $userID, 0, $enquiryHeaderNote, $customerID, $enquirySourceID, $enquiryTypeID, $enquiryCustomerReference, $customerContactID, $includeInstallationInQuote, "");

        $addEnquiry = $enquiryController->createEnquiry($enquiry);

        // The reference cannot be made until the insert has happened. Now we need to add the reference
        $ourReference = $enquiryTypeController->getEnquiryTypeByID($_POST['enquiryType'])->prefix.""
        . "".date('y', strtotime($_POST['calendar']))
        ."".str_pad($addEnquiry, 5, "0", STR_PAD_LEFT);
        $addReference = $enquiryController->addEnquiryOurReference($addEnquiry, $ourReference);
        /*
         * First decisions need to be made about what we do with the engine data that has been provided.
         * If the end user has a verified engine (one with a serial number) then we allow the user
         * to select the engine(s) from the side table. They can also add new ones as required.
         */
        if (isset($_POST['selectEngine'])) {
            foreach ($_POST['selectEngine'] as $key => $engineID) {
                $knownEngine = new EnquiryEngine("", $addEnquiry, $engineID);
                $addKnownEngine = $enquiryEngineController->createEnquiryEngine($knownEngine);
            }
        }

        // for data entered engines
        if (isset($_POST['engine'])) {
            foreach ($_POST['engine'] as $identity => $engine) {

                if (isset($engine['inst']) && ($engine['inst'] != "") && (isset($engine['sn'])) && ($engine['sn'] != "")) {

                    // first double check that the SN is new and isnt already in existence.
                    $instID = $installationController->getInstallationsByInstallationName($engine['inst']);

                    foreach ($instID as $key => $value) {
                        $installation = $value->installationID;
                    }


                    $doesItExist = $installationEngineController->getEngineBySerialNumberAndInstallation($engine['sn'], $installation);
                    if ($doesItExist != null) {
                        $knownEngine = new EnquiryEngine("", $addEnquiry, $doesItExist);
                        $addKnownEngine = $enquiryEngineController->createEnquiryEngine($knownEngine);
                    } else {
                        // get the installation ID


                        /*
                         * If the installation exists WITHOUT a matching equivalent engine (e.g. there are no attached engines
                         * or the engine makes and/or models dont match; BUT we are given a SN by the end user then we assume that
                         * the customers data is correct and we ADD a new engine object to the DB, attached to that installation.
                         */
                        $engineManufacturer = $engine['designer'];
                        $engineModel = $engine['model'];
                        $engineCylinderCount = $engine['cyl'];
                        $engineCylinderConfiguration = "";
                        $engineVerified = "1";
                        $serialNumber = $engine['sn'];
                        $enquiryOnly = "1";

                        $makeEngine = new InstallationEngine("", $engineManufacturer, $engineModel, $engineCylinderCount, $engineCylinderConfiguration, "", $installation, $engineVerified, $customerID, "", "MW", "", "", $serialNumber, "", "", "", $enquiryOnly, "Created for enquiry", "", "3", "", "");
                        $setNewEngine = $installationEngineController->setNewEngine($makeEngine);


                        if ($setNewEngine != null) {
                            $knownEngine = new EnquiryEngine("", $addEnquiry, $setNewEngine);
                            $addKnownEngine = $enquiryEngineController->createEnquiryEngine($knownEngine);
                        }
                    }
                } else if (isset($engine['inst']) && ($engine['inst'] != "") && (isset($engine['sn'])) && ($engine['sn'] == "")) {
                    // get the installation ID
                    /*
                     * If the installation exists WITHOUT a matching equivalent engine (e.g. there are no attached engines
                     * or the engine makes and/or models dont match; then the engine only exists in the enquiry but can be searched
                     * as part of the enquiry history for that installation. We add the engine as enquiry only TRUE because 
                     * we arent sure if it exists
                     * or if the customer is giving us a duff installation name.
                     */
                    $instID = $installationController->getInstallationsByInstallationName($engine['inst']);

                    foreach ($instID as $key => $value) {
                        $installationID = $value->installationID;
                    }
                    $engineManufacturer = $engine['designer'];
                    $engineModel = $engine['model'];
                    $engineCylinderCount = $engine['cyl'];
                    $engineCylinderConfiguration = "";
                    $engineVerified = "0";
                    $serialNumber = "";
                    $enquiryOnly = $engine['enqOnly'];

                    $makeEngine = new InstallationEngine("", $engineManufacturer, $engineModel, $engineCylinderCount, $engineCylinderConfiguration, "", $installationID, $engineVerified, $_POST['customerID'], "", "MW", "", "", $serialNumber, "", "", "", $enquiryOnly, "Created for enquiry", "", "3", "", "");
                    $setNewEngine = $installationEngineController->setNewEngine($makeEngine);


                    if ($setNewEngine != null) {
                        $knownEngine = new EnquiryEngine("", $addEnquiry, $setNewEngine);
                        $addKnownEngine = $enquiryEngineController->createEnquiryEngine($knownEngine);
                    }
                } else if ((!isset($engine['inst']) || ($engine['inst'] == "")) && ((!isset($engine['sn'])) || ($engine['sn'] == ""))) {
                    /*
                     * If the installation is not known (USUALLY A REQUREST FROM A TRADER) and there is no SN then the engine only
                     * exists on the enquiry but is connected to the customer creating the enquiry and can be searched for that way.
                     */

                    // make an unknown installation
                    $newInstallation = new Installation(
                            "", "", "", "UNKNOWN INSTALLATION", "", $customerID, $customerID, "1", "ENQUIRY", "", "", "");

                    $installationID = $installationController->createNewInstallationObject($newInstallation, $customerID);
                    $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "create", "New installation created ID: " . $installationID . " Unknown installation created for enquiry");



                    $engineManufacturer = $engine['designer'];
                    $engineModel = $engine['model'];
                    $engineCylinderCount = $engine['cyl'];
                    $engineCylinderConfiguration = "";
                    $engineVerified = "0";
                    $serialNumber = "";
                    $enquiryOnly = '1';

                    $makeEngine = new InstallationEngine("", $engineManufacturer, $engineModel, $engineCylinderCount, $engineCylinderConfiguration, "", $installationID, $engineVerified, $customerID, "", "MW", "", "", $serialNumber, "", "", "", $enquiryOnly, "Created for enquiry", "", "3", "", "");
                    $setNewEngine = $installationEngineController->setNewEngine($makeEngine);


                    if ($setNewEngine != null) {
                        $knownEngine = new EnquiryEngine("", $addEnquiry, $setNewEngine);
                        $addKnownEngine = $enquiryEngineController->createEnquiryEngine($knownEngine);
                    }
                } else if ((!isset($engine['inst']) || ($engine['inst'] == "")) && ((isset($engine['sn'])) || ($engine['sn'] != ""))) {
                    /*
                     * If the installation is not known but the engine SN is given then this is a verified engine for an unknown 
                     * installation/end user. We create a new UNKNOWN customer record, a new UNKNOWN installation record and a known
                     * engine record. This will be vaguely conntected to the TRADER via the enquiry/quote and will exist for 
                     * future use.
                     */
                    // make an unknown installation
                    $newInstallation = new Installation(
                            "", "", "", "UNKNOWN INSTALLATION", "", $customerID, $customerID, "1", "ENQUIRY", "", "", "");

                    $installationID = $installationController->createNewInstallationObject($newInstallation, $customerID);
                    $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "create", "New installation created ID: " . $installationID . " Unknown installation created for enquiry");



                    $engineManufacturer = $engine['designer'];
                    $engineModel = $engine['model'];
                    $engineCylinderCount = $engine['cyl'];
                    $engineCylinderConfiguration = "";
                    $engineVerified = "0";
                    $serialNumber = $engine['sn'];
                    $enquiryOnly = $engine['enqOnly'];

                    $makeEngine = new InstallationEngine("", $engineManufacturer, $engineModel, $engineCylinderCount, $engineCylinderConfiguration, "", $installationID, $engineVerified, $_POST['customerID'], "", "MW", "", "", $serialNumber, "", "", "", $enquiryOnly, "Created for enquiry", "", "3", "", "");
                    $setNewEngine = $installationEngineController->setNewEngine($makeEngine);
                    print $setNewEngine . "<br/>";

                    if ($setNewEngine != null) {
                        $knownEngine = new EnquiryEngine("", $addEnquiry, $setNewEngine);
                        $addKnownEngine = $enquiryEngineController->createEnquiryEngine($knownEngine);
                    }
                }
            }
        }
        /*
         * Make enquiry engine objects for each engine.
         */
        $theEnquiry = $enquiryController->getEnquiryByID($addEnquiry);
        $theEngines = $enquiryEngineController->getEnquiryEnginesByEnquiryID($addEnquiry);
        $theCustomerContact = $customerContactsController->getContactByID($_POST['customerContact']);
        include 'Views/Enquiries/enquiryAddParts.php';
        break;

    case "addNewInstallation": 
        include 'Views/Enquiries/enquiryAddNewInstallation.php';
        break;
    
    case "addNewVessel":
        include 'Views/Enquiries/enquiryAddNewVessel.php';
        break;
    
    
    case "quote":
        include 'Views/Enquiries/newPages.php';
        break;
    
    case "orders":
        include 'Views/Enquiries/newPages.php';
        break;
    
    case "invoices":
        include 'Views/Enquiries/newPages.php';
        break;
    
    case "dashboard":
        include 'Views/Enquiries/customerSOPDashboard.php';
        break;
    
    case "editEnquiryHeader":
        include 'Views/Enquiries/editEnquiryHeader.php';
        break;
        
    default :
        include 'Views/SOP/sopMainDashboard.php';
}