<?php

/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

include 'Views/Menus/subMenuIntranet.php';
include_once('Controllers/IntranetController.php');

        
$controller = new IntranetController();
$controller->invoke();
