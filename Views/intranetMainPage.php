<style>
    body {
        background-image: url("./icons/Engine_header_image.png");
        background-repeat: no-repeat;
        background-position-y: 60px;
    }
</style>
<?php
/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

include_once('Controllers/NoticeBoardController.php');
include_once('Controllers/NoticeBoardCategoriesController.php');
include_once('Controllers/UserController.php');
include_once('Controllers/DocumentsController.php');
include_once('Controllers/TrainingController.php');
include_once("Controllers/UserHistoryController.php");
include_once 'ControllersHistory/NoticeBoardHistoryController.php';

$userHistoryController = new UserHistoryController();
$noticeBoardController = new NoticeBoardController();
$noticeBoardCategoriesController = new NoticeBoardCategoriesController();
$userController = new UserController();
$documentsController = new DocumentsController();
$trainingController = new TrainingController();
$noticeBoardHistoryController = new NoticeBoardHistoryController();

$page = "notice";
// This is checking that the users session has been created.
// if not we force the redirect to access control.
if (!isset($_SESSION['name'])) {
    $page = "accessControl";
} else {
    if (isset($_GET['page'])) {
        $page = $_GET['page'];
    }
}
switch ($page) {
    case "upload":
        include 'Views/Uploads/uploadFiles.php';
        break;
    case "training":
        include ('Views/Training/trainingDocuments.php');
        break;
    case "brochures":
        include ('Views/Brochures/brochures.php');
        break;
    case "policies":
        include ('Views/Policies/policies.php');
        break;
    case "addArticle":
        include ('Views/NoticeBoard/addArticleForm.php');
        break;
    case "editArticle":
    case "editUnauthedArticle":
        include ('Views/NoticeBoard/editArticleForm.php');
        break;
    case "showArticle":
        include ('Views/NoticeBoard/showArticle.php');
        break;
    case "showHistory":
        include ('Views/NoticeBoard/showHistory.php');
        break;
    case "notice":
        include ('Views/NoticeBoard/noticeBoard.php');
        break;
    case "policyReport":
        include ('Views/Policies/reportPoliciesNotRead.php');
        break;
    case "emailReadReport":
        include ('Views/Policies/emailReadReminder.php');
        break;
    case "addCategories":
        include ('Views/NoticeBoard/addCategories.php');
        break;
    case "authorise":
        include ('Views/NoticeBoard/authoriseArticles.php');
        break;
    default:
        include ('Views/NoticeBoard/noticeBoard.php');
}  