<?php

/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

$article = $noticeBoardController->getArticleByID($_GET['article']);

if (isset($article)) {
        print "<div class='notice'>";
        if (($_SESSION['level'] == "2") || ($_SESSION['level'] == '1')) {
            print "<div class='editTools'>";
            print "<a href='index.php?portal=intranet&page=editArticle&article=$article->ID'><img src='./icons/editSpanner.png' alt='edit article' width='20px height='20px' border='0'></a> ";
            print "<a href='javascript:;' onclick='deleteArticle($article->ID)'><img src='./icons/removeArticle.png' alt='remove article' width='20px height='20px' border='0'></a> ";
            print "</div>";
        }
        print "<p class='noticeDate'>" . date("d M Y", strtotime($article->date)) . "</p>";
        $author = $userController->getUserByID($article->userID);
        print "<p class='noticeDate'>Author: ".$author->name."</p>";
        print "<span class='noticeTitle'>".$article->title."</span>";
        $category = $noticeBoardCategoriesController->getCategoryByID($article->categoryID);
        print "<p class='noticeDate'><strong>".$category->category."</strong> </p>";
        print "<div class='item'>";
        print "<p>".$article->content."</p>";
        print "</div>";
}

