<?php

include_once("../../Models/Database.php");
include_once("../../Models/DatabaseHistory.php");

$article = $_GET['article'];
$userID = $_GET['userID'];
// get old record before update
$originalArticle = mysqli_query("SELECT * FROM notice_board_tbl WHERE nbt_id='".$article."'");
$originalRow = mysqli_fetch_assoc($originalArticle);

$result = "UPDATE notice_board_tbl SET nbt_archive = '1' WHERE nbt_id='".$article."'";

if (!mysqli_query(Database::$connection, $result)) {
    echo mysqli_error(Database::$connection);
} else {
    $history = "INSERT INTO user_history_tbl "
                . "(uht_item_date, uht_item_type, "
                . "uht_comment, user_tbl_usr_id) "
                . "VALUES ('".date("Y-m-d H:i:s", time())."', 'delete', 'Article ".$article." Archived', '".$userID."')";
    
    if (!mysqli_query(Database::$connection, $history)) {
        echo mysqli_error(Database::$connection);
    }
    
    $articleHistory = "INSERT INTO ".$databaseHistory.".notice_board_edit_history_tbl (nbt_id, nbt_title, nbt_date, "
                . "nbt_content, nbt_archive, nbt_authorised, nbt_user_id, nbt_category_id, nbt_document_id, "
                . "notice_history_changed_by_id, notice_history_date_changed) "
                . "VALUES ("
                . "'".$originalRow['nbt_id']."', "
                . "'".$originalRow['nbt_title']."', "
                . "'".$originalRow['nbt_date']."', "
                . "'".$originalRow['nbt_content']."', "
                . "'".$originalRow['nbt_archive']."', "
                . "'".$originalRow['nbt_authorised']."', "
                . "'".$originalRow['user_tbl_usr_id']."', "
                . "'".$originalRow['notice_board_categories_tbl_nbtc_id']."', "
                . "'".$originalRow['documents_tbl_doc_id']."',"
                . "'".$userID."', "
                . "'".date("Y-m-d", time())."')";
    
    if (!mysqli_query(DatabaseHistory::$connection, $articleHistory)) {
        echo mysqli_error(DatabaseHistory::$connection);
    }
}