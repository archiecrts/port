<h3>Add a New Article</h3>
<link rel="stylesheet" href="TinyEditor/style.css" />
<script type="text/javascript" src="TinyEditor/tinyeditor.js"></script>
<script type="text/javascript" src="js/validateArticleForms.js"></script>

<!-- CSS/JS for DatePicker  -->
<link rel="stylesheet" href="./css/jquery-ui.css">
<link rel="stylesheet" href="./css/jquery-ui.min.css">
<link rel="stylesheet" href="./css/jquery-ui.structure.css">
<link rel="stylesheet" href="./css/jquery-ui.structure.min.css">
<link rel="stylesheet" href="./css/jquery-ui.theme.css">
<link rel="stylesheet" href="./css/jquery-ui.theme.min.css">
<script>
    $(function () {
        $("#datepicker").datepicker();
    });
</script>
<?php

if (isset($_POST['submit'])) {
    
    $title = addslashes($_POST['title']);
    $date = $_POST['date'];
    $content = addslashes($_POST['content']);
    $userID = $_SESSION['user_id'];
    $category = $_POST['category'];
    $authorised = '0';
    
    if (($_SESSION['level'] === '1') || $_SESSION['level'] === '2') {
        $authorised = '1';
    }
    
    $newArticle = new NoticeBoard('', $title, $date, $content, $userID, $category, 0, $authorised, '');
    
    $addArticle = $noticeBoardController->setNewNoticeArticleNoDocID($newArticle);
    
   // debugWriter("noticeBoardLogs.txt", $title."\r\nDate: ".$date."\r\nContent: ".$content."\r\nUser ID: ".$userID."\r\nCategory: ".$category."\r\nDoc ID: ".$addArticle."\r\nSUCCESS: ".$addArticle."\r\n\r\n");
    
    if ($addArticle != null) {
        // Log in the DB
        $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "create", "Notice Board Article Added: ".$addArticle);
     
        print '<meta http-equiv="refresh" content="0;url=index.php?portal=intranet&page=notice">';
    } else if ($addArticle == null) {
        print "<p class='red'>Something went wrong, I am sorry but your article was not saved</p>";
    }
    
}

function ae_detect_ie() {
    if ((isset($_SERVER['HTTP_USER_AGENT']) &&
            (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident') !== false ||
            strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false))) {

        return true;
    } else {
        return false;
    }
}

if (ae_detect_ie()) {
    ?>
<p class="red">It seems, that your are using MSIE.
    Why not to switch to standard-complaint brower, like 
    <a href="http://www.google.com/chrome/">Chrome</a> or
    <a href="http://www.mozilla.com/firefox/">Firefox</a>?</p>
<p>This browser does not support the editor below.</p>
<?php } ?>
<form name="form" onsubmit="editor.post(); return validateArticle();" action="index.php?portal=intranet&page=addArticle" method="post" enctype="multipart/form-data">
    
    <table>
        <tr>
            <td>Title</td>
            <td><input type="text" name="title" value='' required /></td>
        </tr>
        <tr>
            <td>Date</td>
            <td><input type="text" name="date"  id="datepicker"  value='<?php echo date("Y-m-d", time()); ?>' required /></td>
        </tr>
        <tr>
            <td>Category</td>
            <td><select name='category' required>
                    <option value="">Select One</option>
                    <?php
                    $categories = $noticeBoardCategoriesController->getCategories();

                    foreach ($categories as $key => $object) {
                        print "<option value='".$key."'>".$object->category."</option>";
                    }
                    
                    ?>
                    
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2"><p id="demo" class="red"></p>
            <textarea id="input" name="content" style="width:400px; height:200px"></textarea>
<script type="text/javascript">
new TINY.editor.edit('editor',{
	id:'input',
	width:584,
	height:175,
	cssclass:'te',
	controlclass:'tecontrol',
	rowclass:'teheader',
	dividerclass:'tedivider',
	controls:['bold','italic','underline','strikethrough','|','subscript','superscript','|',
			  'orderedlist','unorderedlist','|','outdent','indent','|','leftalign',
			  'centeralign','rightalign','blockjustify','|','unformat','|','undo','redo','n',
			  'size','style','|','image','hr','link','unlink','|','cut','copy','paste','print'],
	footer:true,
	fonts:['Helvetica LT Com Light'],
	xhtml:true,
	cssfile:'TinyEditor/style.css',
	bodyid:'editor',
	footerclass:'tefooter',
	toggle:{text:'show source',activetext:'show wysiwyg',cssclass:'toggle'},
	resize:{cssclass:'resize'}
});
</script>
                </td>
        </tr>
        <tr>
            <td></td>
            <td style="text-align: right;"><input type="submit" name="submit" value="Save"/></td>
        </tr>
    </table>
</form>