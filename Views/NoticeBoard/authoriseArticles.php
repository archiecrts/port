<h3>Authorise Articles</h3>
<script>
    $(function() {
    $( "#dialog-confirm" ).dialog({
        autoOpen: false,
        resizable: false,
        height:220,
        modal: true,
        buttons: {
          "Confirm": function() {
            $( this ).dialog( "close" );
            var articleId = $(this).data("articleId");
            var server = '<?php print $_SESSION['server']; ?>';
            $.ajax({url: 'Views/NoticeBoard/archiveArticle.php?article=' + articleId + '&server=' + server});
            window.location.reload(true);
          },
          Cancel: function() {
            $( this ).dialog( "close" );
          }
        },
        create:function () {
            $(this).closest(".ui-dialog").find(".ui-button").addClass("custom");
        }
    });
  });
  
  function deleteArticle(articleId) {
      $( "#dialog-confirm" ).data('articleId', articleId).dialog( "open" );
  };
  
    // Jump / filter menu
    function jump() {
        var jumpmenu = document.getElementById('jump');
        if (jumpmenu.options[jumpmenu.selectedIndex].value !== '') {
            // Redirect
            location.href = 'index.php?portal=intranet&jump=' + jumpmenu.options[jumpmenu.selectedIndex].value;
        } else {
            location.href = 'index.php?portal=intranet';
        }

    }
</script>
<div id="dialog-confirm" title="Are you sure?">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This will archive the article.<br/>Are you sure?</p>
</div>
    <?php
/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

// check if article is set in GET, update the article to be authorised.
if (isset($_GET['article']) && ($_GET['article'] != "")) {
    $updateArticle = $noticeBoardController->updateArticleToAuthorised($_GET['article']);
    if (!isset($updateArticle)) {
        print "<span class='red'>Authorisation Failed, sorry!</span>";
    } else {
        // Log in the DB
        $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "other", "Notice Board Article authorised");
     
    }
}

    $notices = $noticeBoardController->getAllUnauthorisedNoticesDescendingOrder();
    
    
print "<div id='notices'>";
// DISPLAY NOTICES
if (isset($notices)) {
    foreach ($notices as $key => $object) {
        print "<div class='notice'>";
        if (($_SESSION['level'] == "2") || ($_SESSION['level'] == '1')) {
            print "<div class='editTools'>";
            print "<a href='index.php?portal=intranet&page=authorise&article=$object->ID'><img src='./icons/tick.png' alt='authorise article' width='20px height='20px' border='0'></a> ";
            print "<a href='index.php?portal=intranet&page=editUnauthedArticle&article=$object->ID'><img src='./icons/editSpanner.png' alt='edit article' width='20px height='20px' border='0'></a> ";
            print "<a href='javascript:;' onclick='deleteArticle($object->ID)'><img src='./icons/removeArticle.png' alt='remove article' width='20px height='20px' border='0'></a> ";
            print "</div>";
        }

        print "<p class='noticeDate'>" . date("d M Y H:i", strtotime($object->date)) . "</p>";

        $author = $userController->getUserByID($object->userID);
        print "<p class='noticeDate'>Author: " . $author->name . "</p>";

        print "<span class='noticeTitle'>" . stripslashes($object->title) . "</span>";

        $category = $noticeBoardCategoriesController->getCategoryByID($object->categoryID);
        print "<p class='noticeDate'><strong>" . $category->category . "</strong> </p>";

        print "<div class='item'>";
        print "<p>" . stripslashes($object->content) . "</p>";
        print "</div>";
        print "</div>";
    }
} else {
    print "No notices awaiting approval";
}
print "</div>";