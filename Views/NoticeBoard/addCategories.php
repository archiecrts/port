<h3>Add Categories</h3>
<script>
function editCategory(categoryID) {
    //alert(categoryID);
    var div = document.getElementById("categoryDiv" + categoryID); 
    var form = document.getElementById("categoryDivForm" + categoryID); 

    div.style.display = 'none';
    form.style.display = 'table-row';
        
}

function saveForm(categoryID) {
    var div = document.getElementById("categoryDiv" + categoryID); 
    var form = document.getElementById("categoryDivForm" + categoryID); 
    var text = document.getElementById("newName" + categoryID).value;
    var server = '<?php print $_SESSION['server']; ?>';
   
    //alert(categoryID + " " + text);    
    $.ajax({url: 'Views/NoticeBoard/changeCategoryName.php?id=' + categoryID + '&name=' + text + '&server=' + server,
             success: function(output) {
                //alert(output);
                $('#categoryName' + categoryID).html(output);
            },
          error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status + " "+ thrownError);
          }});



    div.style.display = 'table-row';
    form.style.display = 'none';
}


function deleteCategory(categoryID) {
    var div = document.getElementById("categoryDiv" + categoryID); 
    var server = '<?php print $_SESSION['server']; ?>';
   
    //alert(categoryID);    
    $.ajax({url: 'Views/NoticeBoard/deleteCategory.php?id=' + categoryID + '&server=' + server,
             success: function(output) {
                div.style.display = 'none';
            },
          error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status + " "+ thrownError);
          }});



    
}
</script>
<h4>Current Categories</h4>
<p>These cannot be deleted if they are attached to articles but new categories may be added.</p>
<?php
if(isset($_POST['submit'])) {
    if((isset($_POST['newCategory'])) && ($_POST['newCategory'] != "")) {
        $locked = '0';
        if (isset($_POST['locked'])) {
            $locked = '1';
        }
        $add = $noticeBoardCategoriesController->setNoticeBoardCategory(addslashes($_POST['newCategory']), $locked);
        // Log in the DB
        $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "create", "New Notice Board Category created: ".addslashes($_POST['newCategory']));
     
        
    }
}
/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

$categories = $noticeBoardCategoriesController->getCategories();

print "<table>";

if (isset($categories)) {
    
    foreach ($categories as $key => $object) {
        print "<tr id='categoryDiv".$object->categoryID."'>";

        // show new category as blue.
        if (isset($_POST['newCategory']) && ($object->category == $_POST['newCategory'])) {
            print "<td id='categoryName".$object->categoryID."' class='newCategory'>".$object->category."</td>";
        } else {
            print "<td id='categoryName".$object->categoryID."'>".$object->category."</td>";
        }
        
        // check if the category can be edited.
        if ($object->notEditableFlag == '0') {
            
            print "<td><button onclick='editCategory(".$object->categoryID.")'><img src='./icons/editSpanner.png' alt='edit' height='10px' /></button></td>";
            
            $countUsage = $noticeBoardController->getCountOfCategoryUsage($object->categoryID);
            
            if ($countUsage == 0) {
                print "<td><button onclick='deleteCategory(".$object->categoryID.")'><img src='./icons/removeArticle.png' alt='edit' height='10px' /></button></td>";
            }
            
        } else {
            print "<td colspan='2'>[<em>locked</em>]</td>";
        }
        print "</tr>";
        
        print "<tr id='categoryDivForm".$object->categoryID."' style='display:none;'>";

            print "<td><input type='text' id='newName".$object->categoryID."' name='newName' value='".$object->category."' />"
            . "<button onclick='saveForm(".$object->categoryID.")'>Save</button></td>";

        print "</tr>";
    }
} else {
    print "<tr><td>No Categories set</td></tr>";
}
print "</table>";

?>

<form name="form" action="index.php?portal=intranet&page=addCategories" method="post" enctype="multipart/form-data">
    
    <p>New category <input type="text" name="newCategory" value="" /> 
        <label for="locked">Locked</label> <input type="checkbox" id="locked" name="locked" value="1" /> 
        <input type="submit" name="submit" value="Add Category"/></p>
</form>