<h3>Edit Article</h3>
<link rel="stylesheet" href="TinyEditor/style.css" />
<script type="text/javascript" src="TinyEditor/tinyeditor.js"></script>
<script type="text/javascript" src="js/validateForms.js"></script>
<?php
if (isset($_POST['submit'])) {
    $id = $_GET['article'];
    $title = addslashes($_POST['title']);
    $date = $_POST['date'];
    $content = addslashes($_POST['content']);
    $userID = $_SESSION['user_id'];
    $category = $_POST['category'];
    $authorised = $_POST['authorised'];

    // Create history
    $oldArticle = $noticeBoardController->getArticleByID($id);
    $noticeBoardHistory = new HistoryNoticeBoard(
            "", 
            $oldArticle->ID, 
            $oldArticle->title, 
            $oldArticle->date, 
            $oldArticle->content, 
            $oldArticle->archive, 
            $oldArticle->authorised, 
            $oldArticle->userID, 
            $oldArticle->categoryID, 
            $oldArticle->documentID, 
            $_SESSION['user_id'], 
            date("Y-m-d H:i:s", time()));
    $setNoticeBoardHistory = $noticeBoardHistoryController->setNewNoticeBoard($noticeBoardHistory);
    
    $newArticle = new NoticeBoard($id, $title, $date, $content, $userID, $category, 0, $authorised, '');

    $addArticle = $noticeBoardController->updateNoticeArticle($newArticle);
    if ($addArticle) {
        // Log in the DB
        $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "update", "Notice Board Article Updated: ".$id);
     
        if (isset($_GET['page']) && ($_GET['page'] == "editUnauthedArticle")) {
            print '<meta http-equiv="refresh" content="0;url=index.php?portal=intranet&page=authorise">';
        } else {
            print '<meta http-equiv="refresh" content="0;url=index.php?portal=intranet&page=notice">';
        }
    } else {
        print "<p class='red'>Something went wrong, I am sorry but your article was not updated " . $addArticle . "</p>";
    }
}
$articleID = "";
if (isset($_GET['article'])) {
    $articleID = $_GET['article'];
    $article = $noticeBoardController->getArticleByID($articleID);
}

function ae_detect_ie() {
    if ((isset($_SERVER['HTTP_USER_AGENT']) &&
            (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident') !== false ||
            strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false))) {

        return true;
    } else {
        return false;
    }
}

if (ae_detect_ie()) {
    ?>
<p class="red">It seems, that your are using MSIE.
    Why not to switch to standard-complaint brower, like 
    <a href="http://www.google.com/chrome/">Chrome</a> or
    <a href="http://www.mozilla.com/firefox/">Firefox</a>?</p>
<p>This browser does not support the editor below.</p>
<?php } ?>
<form name="form" onsubmit="editor.post();
        return validateArticle();" action="index.php?portal=intranet&page=<?php echo $_GET['page']; ?>&article=<?php echo $articleID; ?>" method="post" enctype="multipart/form-data">
    <input type='hidden' name="authorised" value="<?php print $article->authorised; ?>"/>
    <table>
        <tr>
            <td>Title</td>
            <td><input type="text" name="title" value='<?php print $article->title; ?>' required /></td>
        </tr>
        <tr>
            <td>Date</td>
            <td><input type="text" name="date" id="date" value='<?php print $article->date; ?>' required /></td>
        </tr>
        <tr>
            <td>Category</td>
            <td>
<?php
$search = "$article->categoryID'";
$replace = "$article->categoryID' Selected";
$categories = $noticeBoardCategoriesController->getCategories();
$subject = "<select name='category' required>";
foreach ($categories as $key => $object) {
    $subject .= "<option value='" . $key . "'>" . $object->category . "</option>";
}
$subject .="</select>";
$subject1 = str_replace($search, $replace, $subject);
echo $subject1;
?>

            </td>
        </tr>
        <tr>
            <td colspan="2"><p id="demo" class="red"></p>
                <textarea id="input" name="content" style="width:400px; height:200px"><?php echo $article->content; ?></textarea>
                <script type="text/javascript">
                    new TINY.editor.edit('editor', {
                        id: 'input',
                        width: 584,
                        height: 175,
                        cssclass: 'te',
                        controlclass: 'tecontrol',
                        rowclass: 'teheader',
                        dividerclass: 'tedivider',
                        controls: ['bold', 'italic', 'underline', 'strikethrough', '|', 'subscript', 'superscript', '|',
                            'orderedlist', 'unorderedlist', '|', 'outdent', 'indent', '|', 'leftalign',
                            'centeralign', 'rightalign', 'blockjustify', '|', 'unformat', '|', 'undo', 'redo', 'n',
                            'size', 'style', '|', 'image', 'hr', 'link', 'unlink', '|', 'cut', 'copy', 'paste', 'print'],
                        footer: true,
                        fonts: ['Helvetica LT Com Light'],
                        xhtml: true,
                        cssfile: 'TinyEditor/style.css',
                        bodyid: 'editor',
                        footerclass: 'tefooter',
                        toggle: {text: 'show source', activetext: 'show wysiwyg', cssclass: 'toggle'},
                        resize: {cssclass: 'resize'}
                    });
                </script>
            </td>
        </tr>
        <tr>
            <td></td>
            <td style="text-align: right"><input type="submit" name="submit" value="Save"/></td>
        </tr>
    </table>
</form>