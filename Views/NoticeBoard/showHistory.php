<h3>Notice Board</h3>
 <script src="./js/collapseMenu.js" type="text/javascript"></script>
 <script>
     $(function() {
    $( "#dialog-confirm" ).dialog({
        autoOpen: false,
        resizable: false,
        height:220,
        modal: true,
        buttons: {
          "Confirm": function() {
            $( this ).dialog( "close" );
            var articleId = $(this).data("articleId");
            var server = '<?php print $_SESSION['server']; ?>';
            $.ajax({url: 'Views/NoticeBoard/archiveArticle.php?article=' + articleId + '&server=' + server});
            window.location.reload(true);
          },
          Cancel: function() {
            $( this ).dialog( "close" );
          }
        },
        create:function () {
            $(this).closest(".ui-dialog").find(".ui-button").addClass("custom");
        }
    });
  });
  
  function deleteArticle(articleId) {
      $( "#dialog-confirm" ).data('articleId', articleId).dialog( "open" );
  };
 
// Jump / filter menu
    function jump(month, year) {
        var jumpmenu = document.getElementById('jump');
        
        if (jumpmenu.options[jumpmenu.selectedIndex].value !== '') {
            //alert("here2");
            // Redirect
            location.href = 'index.php?portal=intranet&page=showHistory&month=' + month + '&year=' + year + '&jump=' + jumpmenu.options[jumpmenu.selectedIndex].value;
        } else {
            location.href = 'index.php?portal=intranet&page=showHistory&month=' + month + '&year=' + year;
        }

    }
 </script>
 <div id="dialog-confirm" title="Are you sure?">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This will archive the article.<br/>Are you sure?</p>
</div>
<?php

print "<div id='navBar'>";
if (($_SESSION['level'] == "2") || ($_SESSION['level'] == '1')) {
    print "<div id='articleHref'><a href='index.php?portal=intranet&page=addArticle'>Add Article</a></div>";
}
// filter drop down.
print "<div id='filter'>Filter articles by ";
print "<select id='jump' name='filter' onchange='jump(".$_GET['month'].",".$_GET['year'].")'>";
print "<option value=''>All</option>";
$categories = $noticeBoardCategoriesController->getCategories();

foreach ($categories as $key => $object) {
    if ($_GET['jump'] == $key) {
        $selected = 'selected';
    } else {
        $selected = "";
    }
    print "<option value='".$key."' $selected>".$object->category."</option>";
}

print "</select>";
print "</div>";
print "</div>";


$month;
$year;
if(isset($_GET['month'])) {
    $month = $_GET['month'];
}
if(isset($_GET['year'])) {
    $year = $_GET['year'];
}

// GENERATE ARRAY OF NOTICES.
$notices;
if ((!isset($_GET['jump'])) || ($_GET['jump'] == '')) {
    $notices = $noticeBoardController->getArticleByMonthAndYear($month, $year);
} else {
    $notices = $noticeBoardController->getArticleByMonthAndYearAndCategory($month, $year, $_GET['jump']);
}

if (isset($notices)) {
    foreach ($notices as $key => $object) {
        print "<div class='notice'>";
        if (($_SESSION['level'] == "2") || ($_SESSION['level'] == '1')) {
        print "<div class='editTools'>";
        print "<a href='index.php?portal=intranet&page=editArticle&article=$object->ID'><img src='./icons/editSpanner.png' alt='edit article' width='20px height='20px' border='0'></a> ";
        print "<a href='javascript:;' onclick='deleteArticle($object->ID)'><img src='./icons/removeArticle.png' alt='remove article' width='20px height='20px' border='0'></a> ";
        print "</div>";
        }
        print "<p class='noticeDate'>".date("d M Y", strtotime($object->date))."</p>";
        $author = $userController->getUserByID($object->userID);
        print "<p class='noticeDate'>Author: ".$author->name."</p>";
        print "<span class='noticeTitle'>".$object->title."</span>";
        $category = $noticeBoardCategoriesController->getCategoryByID($object->categoryID);
        print "<p class='noticeDate'><strong>".$category->category."</strong> </p>";
        print "<div class='item'>";
        print "<p>".$object->content."</p>";
        print "</div>";
        print "</div>";
    }
}

?>
 <div id="historyColumn">
     <h4>History</h4>
     
             <ul id="expList">
                 <?php
                    $historyArray = $noticeBoardController->getHistoryArrayMonthsYears();
                    
                    if (isset($historyArray)) {
                        foreach ($historyArray as $key => $object) {
                            print "<li>".$key."<ul>";
                            foreach ($object as $k => $month) {
                                
                                $dateObj   = DateTime::createFromFormat('!m', $month);
                                $monthName = $dateObj->format('F');
                                print "<li><a href='index.php?portal=intranet&page=showHistory&month=".$month."&year=".$key."'>$monthName</a></li>";
                            }
                            
                            print "</ul></li>";
                        }
                    }
                    ?>
             
 </div>