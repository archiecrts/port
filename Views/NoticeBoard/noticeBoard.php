<h3>Notice Board</h3>
<script src="./js/collapseMenu.js" type="text/javascript"></script>
<script>
    $(function () {
        $("#dialog-confirm").dialog({
            autoOpen: false,
            resizable: false,
            height: 220,
            modal: true,
            buttons: {
                "Confirm": function () {
                    $(this).dialog("close");
                    var articleId = $(this).data("articleId");

                    var userID = '<?php print $_SESSION['user_id']; ?>';
                    //$.ajax({url: 'Views/NoticeBoard/archiveArticle.php?article=' + articleId + '&userID=' + userID});
                    $.ajax({url: 'Views/NoticeBoard/archiveArticle.php?article=' + articleId + '&userID=' + userID,
                        success: function (output) {
                            //alert(output);
                            window.location.reload(true);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.status + " " + thrownError);
                        }});

                   //window.location.reload(true);
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            },
            create: function () {
                $(this).closest(".ui-dialog").find(".ui-button").addClass("custom");
            }
        });
    });

    function deleteArticle(articleId) {
        $("#dialog-confirm").data('articleId', articleId).dialog("open");
    }
    ;

    // Jump / filter menu
    function jump() {
        var jumpmenu = document.getElementById('jump');
        if (jumpmenu.options[jumpmenu.selectedIndex].value !== '') {
            // Redirect
            location.href = 'index.php?portal=intranet&jump=' + jumpmenu.options[jumpmenu.selectedIndex].value;
        } else {
            location.href = 'index.php?portal=intranet';
        }

    }
</script>
<div id="dialog-confirm" title="Are you sure?">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This will archive the article.<br/>Are you sure?</p>
</div>
<?php
print "<div id='navBar'>";
$countUnauthorisedArticles = $noticeBoardController->countArticlesAwaitingAuthorisation($_SESSION['user_id']);
if ($countUnauthorisedArticles > 0) {
    print "<div id='awaitingAuth'>You currently have " . $countUnauthorisedArticles . " articles awaiting authorisation</div>";
}
/**
 * All users can see the add article button but the contents is not authorised until admin or group say so.
 */
print "<div id='articleHref'><a href='index.php?portal=intranet&page=addArticle'>Add Article</a></div>";

// filter drop down.
print "<div id='filter'>Filter articles by ";
print "<select id='jump' name='filter' onchange='jump()'>";
print "<option value=''>All</option>";
$categories = $noticeBoardCategoriesController->getCategories();
$jump = '';
if (isset($_GET['jump'])) {
    $jump = $_GET['jump'];
}
foreach ($categories as $key => $object) {
    if ($jump == $key) {
        $selected = 'selected';
    } else {
        $selected = "";
    }
    print "<option value='" . $key . "' $selected>" . $object->category . "</option>";
}

print "</select>";
print "</div>"; // end of filter.
print "</div>"; // end of navbar.
print "<div id='pushDownSection'></div>";

// GENERATE ARRAY OF NOTICES.
$notices;
if ((!isset($_GET['jump'])) || ($_GET['jump'] == '')) {
    $notices = $noticeBoardController->getAllNoticesDescendingOrder();
} else {
    $notices = $noticeBoardController->getAllNoticesByCategoryDescendingOrder($_GET['jump']);
}
include_once('Controllers/DocumentsController.php');
$documentsController = new DocumentsController();

// DISPLAY NOTICES
if (isset($notices)) {
    foreach ($notices as $key => $object) {
        print "<div class='notice'>";
        if (($_SESSION['level'] == "2") || ($_SESSION['level'] == '1')) {
            print "<div class='editTools'>";
            print "<a href='index.php?portal=intranet&page=editArticle&article=$object->ID'><img src='./icons/editSpanner.png' alt='edit article' width='20px height='20px' border='0'></a> ";
            print "<a href='javascript:;' onclick='deleteArticle($object->ID)'><img src='./icons/removeArticle.png' alt='remove article' width='20px height='20px' border='0'></a> ";
            print "</div>";
        }

        print "<p class='noticeDate'>" . date("d M Y", strtotime($object->date)) . "</p>";
        $category = $noticeBoardCategoriesController->getCategoryByID($object->categoryID);
        $author = $userController->getUserByID($object->userID);
        print "<p class='noticeDate'>Author: " . $author->name . "</p>";

        if ($category->category == 'Documents') {
            print "<span class='noticeTitle'><a href='" . $documentsController->getDocumentByID($object->documentID)->url . "' target='_blank'>" . stripslashes($object->title) . "</a></span>";
        } else {
            print "<span class='noticeTitle'>" . stripslashes($object->title) . "</span>";
        }


        print "<p class='noticeDate'><strong>" . $category->category . "</strong> </p>";

        print "<div class='item'>";
        print "<p>" . stripslashes($object->content) . "</p>";
        print "</div>";
        print "</div>";
    }
}
?>
<div id="historyColumn">
    <h4>History</h4>

    <ul id="expList">
        <?php
        $historyArray = $noticeBoardController->getHistoryArrayMonthsYears();

        if (isset($historyArray)) {
            foreach ($historyArray as $key => $object) {
                print "<li>" . $key . "<ul>";
                foreach ($object as $k => $month) {

                    $dateObj = DateTime::createFromFormat('!m', $month);
                    $monthName = $dateObj->format('F');
                    print "<li><a href='index.php?portal=intranet&page=showHistory&month=" . $month . "&year=" . $key . "'>$monthName</a></li>";
                }

                print "</ul></li>";
            }
        }
        ?>
    </ul>
</div>