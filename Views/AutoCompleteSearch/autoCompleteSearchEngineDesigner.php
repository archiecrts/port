<script>
// autocomplet : this function will be executed every time we change the text
    function autocomplet() {
        var min_length = 1; // min caracters to display the autocomplete
        var keyword = $('#engineMake').val();
        if (keyword.length >= min_length) {
            $.ajax({
                url: './Views/AutoCompleteSearch/autoCompleteSQLEngineDesigner.php',
                type: 'POST',
                data: {keyword: keyword},
                success: function (data) {
                    if (keyword.length === $('#engineMake').val().length) {
                        $('#searchEngineList').show();
                        $('#searchEngineList').html(data);
                    }
                }
            });
        } else {
            $('#searchEngineList').hide();
        }
    }

// set_item : this function will be executed when we select an item
    function set_item(item) {
        // change input value
        $('#engineMake').val(item);
        $('#engineModel').val("");
        // hide proposition list
        $('#searchEngineList').hide();
    }


    function set_engine_designer_id (item) {
        $('#designerID').val(item);
    }
    
    
</script>
<style>
    #searchList {
        display: none;
    }
</style>
