<?php

include_once("../../Models/Database.php");

$keyword = addslashes($_POST['keyword']);
$resultInst = mysqli_query(Database::$connection, "SELECT i.inst_id, i.inst_installation_name, i.inst_imo 
                    FROM installations_tbl AS i 
                    WHERE i.inst_archive = '0'  
                    AND
                    (MATCH (i.inst_installation_name) AGAINST ('".$keyword."%*' IN NATURAL LANGUAGE MODE))
                    AND i.inst_installation_name LIKE '%".$keyword."%'
                    ORDER BY i.inst_installation_name ASC
                    ");

while ($row = mysqli_fetch_assoc($resultInst)) {
    // add new option
    echo '<li onclick="set_itemInst(\'' . str_replace("'", "\'", $row['inst_installation_name'].' - '.$row['inst_imo']) .'\');set_installation_idInst(\''.$row['inst_id'].'\');">' . $row['inst_installation_name']." - ". $row['inst_imo'] .'</li>';
} 