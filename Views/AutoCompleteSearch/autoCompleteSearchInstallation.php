<script>
// autocomplet : this function will be executed every time we change the text
    function autocompletInst() {
        var min_length = 1; // min caracters to display the autocomplete
        var keyword = $('#searchBoxInst').val();

        if (keyword.length >= min_length) {
            $.ajax({
                url: './Views/AutoCompleteSearch/autoCompleteSQLInstallation.php',
                type: 'POST',
                data: {keyword: keyword},
                success: function (data) {
                    if (keyword.length === $('#searchBoxInst').val().length) {
                        $('#searchListInst').show();
                        $('#searchListInst').html(data);
                    }
                }
            });
        } else {
            $('#searchListInst').hide();
        }


    }

// set_item : this function will be executed when we select an item
    function set_itemInst(item) {
        // change input value
        $('#searchBoxInst').val(item);
        // hide proposition list
        $('#searchListInst').hide();// WARNING this is the same function as in autoCompleteSearchPastInstallationName.php
        $('#searchListPast').hide();
    }

    // ID of the installation.
    function set_installation_idInst(id) {
        // change the hidden value
        $('#installationIDInst').val(id);
    }

    // managing company is the customer
    function set_customer_idInst(id) {
        // change the hidden value
        $('#customerIDInst').val(id);
    }

    $(document).on('keyup keypress', 'form input[type="text"]', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            return false;
        }
    });
</script>
<style>

    #searchListInst {
        display: none;
        overflow:hidden; 
        overflow-y:scroll;
        height:200px;
    }
</style>

<h4>Search Installation Name</h4>
<p>
<form name="searchFormInst" action="index.php" method="GET" enctype="multipart/form-data">
    <div id="listingHolder">
        <input type="text" id="searchBoxInst" onkeyup="autocompletInst()" placeholder="Begin typing an installation/ship name"  autocomplete="off">
        <input type="hidden" name="portal" value="mr"/>
        <input type="hidden" name="page" value="report" />
        <input type="hidden" name="action" value="showInstallation" />
        <input type="hidden" id="installationIDInst" name="installationID" value=""/>
        <input type="hidden" id="customerIDInst" name="customerID" value=""/>
        <input type="submit" name="submit" value="Go"/> 
        <ul id="searchListInst" multiple size="10"></ul>
    </div>
</form>
</p>