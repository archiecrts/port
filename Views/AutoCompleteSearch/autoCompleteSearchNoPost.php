<script>
// autocomplet : this function will be executed every time we change the text
    function autocomplet() {
        var min_length = 1; // min caracters to display the autocomplete
        var keyword = $('#searchBox').val();
        if (keyword.length >= min_length) {
            $.ajax({
                url: './Views/AutoCompleteSearch/autoCompleteSQLCustomerOnly.php',
                type: 'POST',
                data: {keyword: keyword},
                success: function (data) {
                    if (keyword.length === $('#searchBox').val().length) {
                        $('#searchList').show();
                        $('#searchList').html(data);
                    }
                }
            });
        } else {
            $('#searchList').hide();
        }
    }

// set_item : this function will be executed when we select an item
    function set_item(item) {
        // change input value
        $('#searchBox').val(item);
        // hide proposition list
        $('#searchList').hide();
    }

    // ID of the installation.
    function set_installation_id(id) {
        // change the hidden value
        $('#installationID').val(id);
    }

    // managing company is the customer
    function set_customer_id(id) {
        // change the hidden value
        var customerIDValue = id;
        $('#customerID').val(id);
        

        $.ajax({url: 'Views/AddInstallation/customerAddressDropDownAjax.php?customerID=' + customerIDValue,
            success: function (output) {
                //  alert(output);
                $('#list-select').html(output);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status + " " + thrownError);
            }});
    }
</script>
<style>

    #searchList {
        display: none;
    }
</style>


<div> Type a customer name 
    <input type="text" id="searchBox" onkeyup="autocomplet()"  autocomplete="off">
    <input type="hidden" name="portal" value="mr"/>
    <input type="hidden" name="page" value="report" />
    <input type="hidden" name="action" value="showInstallation" />
    <input type="hidden" id="installationID" name="installationID" value=""/>
    <input type="hidden" id="customerID" name="customerID" value=""/>
    <p></p>
    <ul id="searchList"></ul>
</div>


<strong>Installation Address</strong> <select name="customerAddressID" id='list-select'>
    <option value=''>Select a customer...</option>
</select>