<script>
// autocomplet : this function will be executed every time we change the text
    function autocompletIMO() {
        var min_length = 1; // min caracters to display the autocomplete
        var keyword = $('#searchBoxIMO').val();
        if (keyword.length >= min_length) {
            $.ajax({
                url: './Views/AutoCompleteSearch/autoCompleteSQLIMO.php',
                type: 'POST',
                data: {keyword: keyword},
                success: function (data) {
                    if (keyword.length === $('#searchBoxIMO').val().length) {
                        $('#searchListIMO').show();
                        $('#searchListIMO').html(data);
                    }
                }
            });
        } else {
            $('#searchListIMO').hide();
        }
    }

// set_item : this function will be executed when we select an item
    function set_itemIMO(item) {
        // change input value
        $('#searchBoxIMO').val(item);
        // hide proposition list
        $('#searchListIMO').hide();
    }
    
    // ID of the installation.
    function set_installation_idIMO(id) {
        // change the hidden value
        $('#installationIDIMO').val(id);
    }
    
    // managing company is the customer
    function set_customer_idIMO(id) {
        // change the hidden value
        $('#customerIDIMO').val(id);
    }
    
    $(document).on('keyup keypress', 'form input[type="text"]', function(e) {
        if(e.which === 13) {
          e.preventDefault();
          return false;
        }
    });
</script>
<style>

    #searchListIMO {
        display: none;
        overflow:hidden; 
        overflow-y:scroll;
        height:200px;
    }
</style>

<h4>Search IMO</h4>
<p>
<form name="searchFormIMO" action="index.php" method="GET" enctype="multipart/form-data">
    <div>
        <input type="text" id="searchBoxIMO" onkeyup="autocompletIMO()" placeholder="Begin typing an IMO No."  autocomplete="off">
        <input type="hidden" name="portal" value="mr"/>
        <input type="hidden" name="page" value="report" />
        <input type="hidden" name="action" value="showInstallation" />
        <input type="hidden" id="installationIDIMO" name="installationID" value=""/>
        <input type="hidden" id="customerIDIMO" name="customerID" value=""/>
        <input type="submit" name="submit" value="Go"/> 
        <ul id="searchListIMO" multiple size="15"></ul>
    </div>
</form>
</p>
