<?php

include_once("../../Models/Database.php");

$keyword = "%" . addslashes($_POST['keyword']) . "%";
$resultInst = mysqli_query(Database::$connection, "SELECT c.cust_id, c.cust_name, c.cust_tbh_account_code, c.cust_header_notes,
                    c.cust_account_manager, a.cadt_country, u.usr_name 
                    FROM customer_tbl AS c LEFT JOIN 
                    (SELECT * FROM customer_address_tbl WHERE cadt_default_address='1') a
                    ON c.cust_id = a.customer_tbl_cust_id
                    LEFT JOIN (SELECT * FROM user_tbl) u ON c.cust_account_manager = u.usr_id 
                    WHERE c.cust_name LIKE '%".$keyword."%' OR c.cust_tbh_account_code LIKE '%".$keyword."%'
                    LIMIT 0, 10");


while ($row = mysqli_fetch_assoc($resultInst)) {
    $accountManagerDetails = $row['cust_account_manager'].'_'.$row['usr_name'];
    if ($row['cust_account_manager'] == "") {
        $resultTer = mysqli_query(Database::$connection, "SELECT t.user_tbl_usr_id, b.usr_name FROM user_territory_tbl AS t 
                LEFT JOIN (SELECT * FROM user_tbl) b ON t.user_tbl_usr_id = b.usr_id
                WHERE 
                ut_territory_name IN (select c_name from country_tbl where c_name = '".addslashes($row['cadt_country'])."')
                OR ut_territory_name IN (select c_area from country_tbl where c_name = '".addslashes($row['cadt_country'])."')
                OR ut_territory_name IN (select c_continent from country_tbl where c_name = '".addslashes($row['cadt_country'])."')");

        $rowTerritory = mysqli_fetch_assoc($resultTer);
        $accountManagerDetails = $rowTerritory['user_tbl_usr_id']."_".$rowTerritory['usr_name'];
    }
    // add new option
    echo '<li onclick="set_item(\'' . str_replace("'", "\'", $row['cust_tbh_account_code']) . ' - ' . str_replace("'", "\'", $row['cust_name']) . '\');set_customer_id(\''.$row['cust_id'].'\');set_account_manager(\''.$accountManagerDetails.'\');set_customer_notes(\''.$row['cust_header_notes'].'\');"> ' . $row['cust_tbh_account_code'] . ' - ' . $row['cust_name'] . ' - ' . $row['cadt_country'] . ' </li>';
} 