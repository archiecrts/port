<script>
// autocomplet : this function will be executed every time we change the text
    function autocomplet() {
        var min_length = 1; // min caracters to display the autocomplete
        var keyword = $('#searchBox').val();
        if (keyword.length >= min_length) {
            $.ajax({
                url: './Views/AutoCompleteSearch/autoCompleteSQL.php',
                type: 'POST',
                data: {keyword: keyword},
                success: function (data) {
                    if (keyword.length === $('#searchBox').val().length) {
                        $('#searchList').show();
                        $('#searchList').html(data);
                    }
                }
            });
        } else {
            $('#searchList').hide();
        }
    }

// set_item : this function will be executed when we select an item
    function set_item(item) {
        // change input value
        $('#searchBox').val(item);
        // hide proposition list
        $('#searchList').hide();
    }
    
    // ID of the installation.
    function set_installation_id(id) {
        // change the hidden value
        $('#installationID').val(id);
    }
    
    // managing company is the customer
    function set_customer_id(id) {
        // change the hidden value
        $('#customerID').val(id);
    }
    
    $(document).on('keyup keypress', 'form input[type="text"]', function(e) {
        if(e.which === 13) {
          e.preventDefault();
          return false;
        }
    });
</script>
<style>

    #searchList {
        display: none;
        overflow:hidden; 
        overflow-y:scroll;
        height:200px;
    }
</style>

<h4>Search Customer Name</h4>
<p>
<form name="searchForm" action="index.php" method="GET" enctype="multipart/form-data">
    <p>You must select an option from the drop down before continuing.</p>
    <div>
        <input type="text" id="searchBox" onkeyup="autocomplet()" placeholder="Begin typing a company name"  autocomplete="off">
        <input type="hidden" name="portal" value="mr"/>
        <input type="hidden" name="page" value="report" />
        <input type="hidden" name="action" value="showCustomer" />
        <input type="hidden" id="installationID" name="installationID" value=""/>
        <input type="hidden" id="customerID" name="customerID" value=""/>
        <input type="submit" name="submit" value="Go"/> 
        <ul id="searchList" multiple size="15"></ul>
    </div>
</form>
</p>