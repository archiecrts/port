<script>
// autocomplet : this function will be executed every time we change the text
    function autocomplet() {
        var min_length = 1; // min caracters to display the autocomplete
        var keyword = $('#searchBox').val();
        if (keyword.length >= min_length) {
            $.ajax({
                url: './Views/AutoCompleteSearch/autoCompleteSQLEnquiry.php',
                type: 'POST',
                data: {keyword: keyword},
                success: function (data) {
                    if (keyword.length === $('#searchBox').val().length) {
                        $('#searchList').show();
                        $('#searchList').html(data);
                    }
                }
            });
        } else {
            $('#searchList').hide();
        }
    }

// set_item : this function will be executed when we select an item
    function set_item(item) {
        // change input value
        $('#searchBox').val(item);
        // hide proposition list
        $('#searchList').hide();
    }

    // Header Note
    function set_customer_notes(note) {
        // change the hidden value
        var html = "<h4>Warning</h4>" + note;
        $('#customerNotesContainer').html(html);
        $('#customerNotesContainer').css("display", "block");
    }

    // managing company is the customer
    function set_customer_id(id) {
        // change the hidden value
        $('#customerID').val(id).change();
        $('#customerContact').html('<option value="">Loading...' + id + '</option>');

        //Make AJAX request, using the selected value as the GET
        $.ajax({
            url: 'Views/enquiries/generateCustomerContactDropDown.php',
            type: 'POST',
            data: {customerID: id},
            success: function (data) {
               // alert(data);
                $('#customerContact').html(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status + " " + thrownError);
            }
        });
        
        set_add_contact(id);
    }
    
    //set Account Manager ID
    function set_account_manager(id) {
        var val = id.split('_');
        var html = val[1]+" <input type='hidden' name='accManagerID' value='"+val[0]+"' />";
        $('#accManagerDisplay').html(html);
    }
    
    function set_add_contact(id) {
        $('#addContact').click(function() {
            window.open ('Views/Enquiries/enquiryAddCustomerContact.php?customerID='+id, 'newwindow', config='height=400, width=600, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no');

        });
    }
</script>
<style>
    #searchList {
        display: none;
    }
</style>
