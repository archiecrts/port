<?php

include_once("../../Models/Database.php");

$keyword = addslashes($_POST['keyword']);
$resultInst = mysqli_query(Database::$connection, "SELECT installations_tbl_inst_id, snht_vessel_name, snht_lrno 
                    FROM ship_name_history_tbl 
                    WHERE 
                    (MATCH (snht_vessel_name) AGAINST ('".$keyword."%*' IN NATURAL LANGUAGE MODE))
                        AND snht_vessel_name LIKE '%".$keyword."%'
                        ORDER BY snht_vessel_name ASC
                    ");

while ($row = mysqli_fetch_assoc($resultInst)) {
    // add new option
    echo '<li onclick="set_itemInst(\'' . str_replace("'", "\'", $row['snht_vessel_name'].' - '.$row['snht_lrno']) .'\');set_installation_idInst(\''.$row['installations_tbl_inst_id'].'\');">' . $row['snht_vessel_name']." - ". $row['snht_lrno'] .'</li>';
} 