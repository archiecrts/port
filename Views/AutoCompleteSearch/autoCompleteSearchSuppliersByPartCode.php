<script>
// autocomplet : this function will be executed every time we change the text
    function autocompletePartCode() {
        var min_length = 1; // min caracters to display the autocomplete
        var keyword = $('#searchBoxOne').val();
        if (keyword.length >= min_length) {
            $.ajax({
                url: './Views/AutoCompleteSearch/autoCompleteSQLPartCode.php',
                type: 'POST',
                data: {keyword: keyword},
                success: function (data) {
                    if (keyword.length === $('#searchBoxOne').val().length) {
                        $('#searchListOne').show();
                        $('#searchListOne').html(data);
                    }
                }
            });
        } else {
            $('#searchListOne').hide();
        }
    }

// set_item : this function will be executed when we select an item
    function set_part(item) {
        // change input value
        $('#searchBoxOne').val(item);
        // hide proposition list
        $('#searchListOne').hide();
    }
    
  
    
    // managing company is the customer
    function set_partcode_id(id) {
        // change the hidden value
        $('#partCodeID').val(id);
    }
    
    $(document).on('keyup keypress', 'form input[type="text"]', function(e) {
        if(e.which === 13) {
          e.preventDefault();
          return false;
        }
    });
</script>
<style>

    #searchListOne {
        display: none;
    }
</style>
<hr/>
<h4>Search</h4>
<p>
<form name="searchFormOne" action="index.php" method="GET" enctype="multipart/form-data">
    <p>Search on Product Part Code (displays first 15 matching records)</p>
    <div>
        <input type="text" id="searchBoxOne" onkeyup="autocompletePartCode()"  autocomplete="off" placeholder="Type a part code or description">
        <input type="hidden" name="portal" value="suppliers"/>
        <input type="hidden" name="page" value="supplierTable" />
        <input type="hidden" name="action" value="view" />
        <input type="hidden" id="partCodeID" name="partCodeID" value=""/>
        <input type="submit" name="submit" value="Go"/> 
        <ul id="searchListOne" multiple size="15"></ul>
    </div>
</form>
</p>
<hr/>