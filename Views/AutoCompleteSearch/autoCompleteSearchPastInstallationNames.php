<script>
// autocomplet : this function will be executed every time we change the text
    function autocompletPast() {
        var min_length = 1; // min caracters to display the autocomplete
        var keyword = $('#searchBoxPast').val();
        if (keyword.length >= min_length) {
            $.ajax({
                url: './Views/AutoCompleteSearch/autoCompleteSQLPastInstallationNames.php',
                type: 'POST',
                data: {keyword: keyword},
                success: function (data) {
                    if (keyword.length === $('#searchBoxPast').val().length) {
                        $('#searchListPast').show();
                        $('#searchListPast').html(data);
                    }
                }
            });
        } else {
            $('#searchListPast').hide();
        }
    }

// set_item : this function will be executed when we select an item
    function set_itemInst(item) {
        // change input value
        $('#searchBoxInst').val(item);
        $('#searchBoxPast').val(item);
        // hide proposition list
        $('#searchListPast').hide(); // WARNING this is the same function as in autoCompleteSearchInstallation.php
        $('#searchListInst').hide();
    }
    
    // ID of the installation.
    function set_installation_idPast(id) {
        // change the hidden value
        $('#installationIDPast').val(id);
    }
    
    // managing company is the customer
    function set_customer_idPast(id) {
        // change the hidden value
        $('#customerIDPast').val(id);
    }
    
    $(document).on('keyup keypress', 'form input[type="text"]', function(e) {
        if(e.which === 13) {
          e.preventDefault();
          return false;
        }
    });
</script>
<style>

    #searchListPast {
        display: none;
        overflow:hidden; 
        overflow-y:scroll;
        height:200px;
    }
</style>

<h4>Search Past Installation Names</h4>
<p>
<form name="searchFormPast" action="index.php" method="GET" enctype="multipart/form-data">
    <div>
        <input type="text" id="searchBoxPast" onkeyup="autocompletPast()" placeholder="Begin typing Past Name."  autocomplete="off">
       <!-- <input type="hidden" name="portal" value="mr"/>
        <input type="hidden" name="page" value="report" />
        <input type="hidden" name="action" value="showInstallation" />
        <input type="hidden" id="installationIDPast" name="installationID" value=""/>
        <input type="hidden" id="customerIDPast" name="customerID" value=""/>
        <input type="submit" name="submit" value="Go"/> -->
        <ul id="searchListPast" multiple size="15"></ul>
    </div>
</form>
</p>
