<script>
// autocomplet : this function will be executed every time we change the text
    function autocompletModel() {
        var min_length = 1; // min caracters to display the autocomplete
        var keyword = $('#engineModel').val();

        var designerID = $('#designerID').val();
        if (keyword.length >= min_length) {
            $.ajax({
                url: './Views/AutoCompleteSearch/autoCompleteSQLEngineModel.php',
                type: 'POST',
                data: {keyword: keyword, designer: designerID},
                success: function (data) {
                    if (keyword.length === $('#engineModel').val().length) {
                        $('#searchEngineModelList').show();
                        $('#searchEngineModelList').html(data);
                    }
                }
            });
        } else {
            $('#searchEngineModelList').hide();
        }
    }

// set_item : this function will be executed when we select an item
    function set_item_model(item) {
        // change input value
        $('#engineModel').val(item);
        // hide proposition list
        $('#searchEngineModelList').hide();
    }


    
    
</script>
<style>
    #searchList {
        display: none;
    }
</style>
