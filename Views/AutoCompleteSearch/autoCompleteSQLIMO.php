<?php

include_once("../../Models/Database.php");

$keyword = "%" . addslashes($_POST['keyword']) . "%";
$resultInst = mysqli_query(Database::$connection, "SELECT c.cust_id, c.cust_name, i.inst_id, i.inst_installation_name, i.inst_imo 
                    FROM customer_tbl AS c LEFT JOIN `customer_tbl_has_installations_tbl` as ci 
                    ON c.cust_id = ci.customer_tbl_cust_id 
                    LEFT JOIN installations_tbl AS i ON ci.installations_tbl_inst_id = i.inst_id 
                    WHERE i.inst_archive = '0'  AND
                    (MATCH (i.inst_imo) AGAINST ('".$keyword."*' IN BOOLEAN MODE))
                     ");


while ($row = mysqli_fetch_assoc($resultInst)) {
    // add new option
    echo '<li onclick="set_itemIMO(\'' . $row['inst_imo']. ' - '.str_replace("'", "\'", $row['inst_installation_name']." - ".$row['cust_name']) .'\');set_installation_idIMO(\''.$row['inst_id'].'\');set_customer_idIMO(\''.$row['cust_id'].'\');">' . $row['inst_imo']." - ".$row['inst_installation_name']  .' - '.$row['cust_name'].'</li>';
} 