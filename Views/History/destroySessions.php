<?php
include_once("Controllers/UserHistoryController.php");
$userHistoryController = new UserHistoryController();
/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

// set timeout period in seconds
$inactive = 54000; //54000 seconds = 15 hours
// Check, if user is already login, then jump to secured page
if (isset($_SESSION['loginDatestamp'])) {
    $session_life = time() - $_SESSION['loginDatestamp'];
    // Jump to secured page
    
    if($session_life > $inactive) {
        // Log in the DB
        $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "logout", "User was logged out by Session Timeout");
        
        // Destroy sessions;
        session_destroy();
        
        // Jump to Logout page
        print '<meta http-equiv="refresh" content="0;url=index.php?portal=logout" />';
    }
}