<?php
include_once("Controllers/UserHistoryController.php");
$userHistoryController = new UserHistoryController();
/* 
 * Log Out Page.
 */
// Log in the DB
$userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "logout", "User logged out manually");
        
session_destroy();
print '<meta http-equiv="refresh" content="0;url=index.php?portal=accessControl">';