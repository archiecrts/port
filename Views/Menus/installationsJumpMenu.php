<?php
$statusArray = [ 'conversation' => 'Conversations']; //,'customer' => 'Customer',, 'installation' => 'Installation' 'contacts' => 'Contacts', 'technical' => 'Technical Data
?>
<ul id="installationsJumpMenu">
    <?php
    foreach ($statusArray as $index => $val) {
        $border = "border";
        if ($index == "conversation") {
            $border = "";
        }
        $css = "class='unselectedButton $border'";
        $imgSrc = './icons/whiteForwardButton.png';

        if (!isset($_GET['tab'])) {
            $_GET['tab'] = 'customer';
        }
        if ($index === $_GET['tab']) {
            $css = "class='selectedButton $border'";
            $imgSrc = './icons/forward_button.png';
        }

        print '<li ><a href="index.php?portal=mr&page=report&action=showInstallation&customerID=' . $customerID . '&tab=' . $index . '&reportID=' . $reportOverviewID . '">' . $val . '</a></li>';
    }
    ?>
</ul>