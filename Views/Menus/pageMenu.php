<?php
if (!isset($_GET['portal'])) {
    $_GET['portal'] = 'intranet';
}
include_once 'Controllers/UserHistoryController.php';
$userHistoryController = new UserHistoryController();
?>
<script>
    $(function () {
        var targetParam = "<?php echo $_GET['portal']; ?>";
        $("#" + targetParam).addClass("selected");
        
    });
</script>
<div id="menuBar" class="menuBar">


    <?php
    if (isset($_SESSION['name'])) {
        ?>
        <ul id="menu" class="menu">
            <li id='dashboard'>
                <a href="index.php?portal=dashboard">
                    <span class="menuText">Dashboard</span></a> 
            </li>   
            <li id="mr">
                <a href="index.php?portal=mr">
                    <span class="menuText">Installations</span>
                </a> 
            </li>
            <!--<li id="suppliers" class="dropdown">
                    <a class="dropbtn" href="index.php?portal=suppliers&page=search">
                        <span class="menuText">Suppliers</span>
                    </a>
                    
                    <ul class="dropdown-content">
                        <li><a href="index.php?portal=suppliers&page=search"><span class="menuText">Search Suppliers</span></a></li>
                        <li><a href="index.php?portal=suppliers&page=upload">Bulk CSV Upload</a></li>
                        <li><a href='index.php?portal=suppliers&page=addNewSupplier'>Add New Supplier Record</a></li>
                        <li><a href="index.php?portal=suppliers&page=addProducts">Add Products to Supplier</a></li>
                    </ul>
                </li>-->
            
            <?php

            if (($_SESSION['level'] == '1') || ($_SESSION['level'] == '2') || ($_SESSION['department'] == '6')) {
                ?>
                <li id='parts' class="dropdown">
                    <a class="dropbtn" href="index.php?portal=parts">
                       <span class="menuText">Parts</span>
                    </a>
                    <ul class="dropdown-content">
                        <li><a href="index.php?portal=parts&page=manifest">Add to Product Manifest</a></li>
                        <li><a href="index.php?portal=parts&page=addPart">Add a new product</a></li>
                    </ul>
                </li>
                <li id='parts' class="dropdown">
                    <a class="dropbtn" href="index.php?portal=products">
                       <span class="menuText">Products</span>
                    </a>
                    <!--<ul class="dropdown-content">
                        <li><a href="index.php?portal=products">Add to Product Manifest</a></li>
                        <li><a href="index.php?portal=products">Add a new product</a></li>
                    </ul>-->
                </li>
                <?php
            }
            if (($_SESSION['level'] == '1') || ($_SESSION['level'] == '2')) {
                ?>
                <li id="users" class="dropdown">
                    <a href="index.php?portal=users" class="dropbtn">
                        <span class="menuText">Users</span>
                    </a> 
                    
                    <ul class="dropdown-content">
                        <li><a href="index.php?portal=users&action=dept">Add new department</a></li>
                    </ul>
                    
                </li>
                <li id="uploads" class="dropdown">
                    <a class="dropbtn" href="index.php?portal=uploads">
                        <span class="menuText">Uploads</span>
                    </a>
                    
                    <ul class="dropdown-content">
                        <li><a href="index.php?portal=uploads&page=upload">Bulk LAND CSV Upload</a></li>
                        <li><a href="index.php?portal=uploads&page=uploadSeals">Bulk SEALS CSV Upload</a></li>
                        <li><a href="index.php?portal=uploads&page=uploadOWS">Bulk OWS CSV Upload</a></li>
                        <li><a href="index.php?portal=uploads&page=uploadSeaweb">Bulk SeaWeb CSV Upload</a></li>
                        <!--<li><a href="index.php?portal=uploads&page=updateSeaweb">Bulk SeaWeb CSV UPDATE From Local Folder</a></li>-->
                        <li><a href="index.php?portal=mr&page=uploadFiles">AUTO SEAWEB UPDATE</a></li>
                    </ul>
                </li>
                
                <li><a href="index.php?portal=uploads&page=checkSeawebChanges"><span style='color:limegreen;'>Last SeaWeb Update At: <?php echo $userHistoryController->lastSeawebUpdateAt()->itemDate; ?></span></a></li>
                <?php
            }
            ?>
            
            
            


            
            <?php
            /*if (($_SESSION['level'] == '1') || ($_SESSION['level'] == '2') || ($_SESSION['department'] == '3')) {
                ?>
                <li id='accounts'>
                    <a href="index.php?portal=accounts">
                       <!-- <img id="invoicesImage" src="icons/invoices_white.png" alt="Accounts" class="menuIcon"  height='30px'/>-->
                        <span class="menuText">Accounts</span></a> 
                </li>
                <?php
            }*/
            ?>

            
        </ul>

    <?php
}
?>
</div>