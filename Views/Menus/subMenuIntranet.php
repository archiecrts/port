
<div id="subMenuBar">
    <script>
        $(function () {
            var targetParam = "<?php 
            
                if (!isset($_GET['page'])) { $_GET['page'] = "notice";}
                echo $_GET['page']; 

                ?>";
                        
            if ((targetParam == "addArticle") || (targetParam == "editArticle")) {
                targetParam = "notice";
            } else if (targetParam == "editUnauthedArticle") {
                targetParam = "authorise";
            }
            if (targetParam == "upload") {
                var destinationParam = "<?php 
            
                if (!isset($_GET['destination'])) { $_GET['destination'] = "brochures";}
                echo $_GET['destination']; 
            
                ?>";
                targetParam = destinationParam;
            }
            $( "#"+targetParam).addClass("selected");
            
        });
    </script>
    <?php
    if (isset($_SESSION['name'])) {
        ?>
        <!--<span id='subMenuTitle'>Intranet</span>-->
        <ul id="menu">
            <li id="notice"><a href="index.php?portal=intranet&page=notice">Notice Board</a></li>
            <li id="policies"><a href="index.php?portal=intranet&page=policies">Policies</a></li>
            <li id="training"><a href="index.php?portal=intranet&page=training">Training</a></li>
            <li id="brochures"><a href="index.php?portal=intranet&page=brochures">Brochures &amp; Sales Presentations</a></li>
            
            <?php
                include_once 'Controllers/NoticeBoardController.php';
                $noticeBoardController = new NoticeBoardController();
            
            if(($_SESSION['level'] == '1') || ($_SESSION['level'] == '2')) {
                print '<li id="addCategories"><a href="index.php?portal=intranet&page=addCategories">Add Categories</a></li>';
            
                $countAllUnauthorisedArticles = $noticeBoardController->countAllArticlesAwaitingAuthorisation();
                if ($countAllUnauthorisedArticles > 0) {
                    print '<li id="authorise"><a href="index.php?portal=intranet&page=authorise">Awaiting Authorisation</a></li>';
            
                }
            }
            ?>
            <li id="trello"><a href="https://trello.com/b/p6SV28wF/james-blackboard" target="_blank">Trello Blackboard</a></li>
        </ul>
        
        <?php
    }
    ?>

</div>
<?php
include('Views/Menus/notifyAdminStub.php');
?>
<div id="clearBoth"></div>