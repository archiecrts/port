<?php

/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

print "<h3>Welcome ".$_SESSION['name']."</h3>";

include_once 'Controllers/UserController.php';
$userController = new UserController();
if (isset($_POST['submit'])) {
    if (isset($_POST['password'])) {
        $resetPassword = $userController->resetPassword($_POST['password'], $_SESSION['user_id']);
        
        if ($resetPassword) {
            print "<h3 class='green'>Your password has been changed</h3>";
            
            include_once('Helpers/PHPMailer/PHPMailerAutoload.php');
            $getUser = $userController->getUser($_SESSION['name']);

                // EMAIL VIA POP3
                $mail = new PHPMailer;

                //$mail->SMTPDebug = 3;                               // Enable verbose debug output

                $mail->isSMTP();                                      // Set mailer to use SMTP
                $mail->Host = 'mail.uk2.net';                         // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                $mail->Username = 'acurtis@simplexturbulo.es';        // SMTP username
                $mail->Password = 'GreatThing22';                     // SMTP password
                $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                $mail->Port = 587;                                    // TCP port to connect to

                $mail->From = 'acurtis@dieselmarinegroup.com';
                $mail->FromName = 'PORT : S-T Diesel Marine Group';
                $mail->addAddress($getUser->email, $_SESSION['name']);     // Add a recipient
                $mail->addReplyTo('acurtis@dieselmarinegroup.com', 'Information');
                //$mail->addCC('cc@example.com');
                //$mail->addBCC('bcc@example.com');

                //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
                //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
                $mail->isHTML(true);                                  // Set email format to HTML

                $mail->Subject = 'Password Changed';
                $mail->Body    = "<p>Your password on PORT has just been changed.<br/>"
                            . "If you did not make / authorise this change please contact me immediately. <br/>"
                            . "Yours Sincerely - Webmaster</p>";
                $mail->AltBody = "Your password on PORT has just been changed. \r\n"
                            . "If you did not make / authorise this change please contact me immediately.\r\n"
                            . "Yours Sincerely - Webmaster";

                if(!$mail->send()) {
                    echo 'Message could not be sent.';
                    echo 'Mailer Error: ' . $mail->ErrorInfo;
                }
            }
        }
    }

?>

<p>Change your password</p>

<script type="text/javascript" src="js/validateFirstLoginForm.js"></script>
<form name="form" onsubmit="return validateFirstLogin();" action="index.php?portal=myAccount" method="post" enctype="multipart/form-data">
    
    <p id="error"></p>
    <table>
        <tr>
            <td>New Password</td>
            <td><input type="password" name="password" required /></td>
        </tr>
        <tr>
            <td>Confirm New Password</td>
            <td><input type="password" name="confirmPassword" required /></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" name="submit" value="Set New Password" /></td>
        </tr>
    </table>
    
</form>
