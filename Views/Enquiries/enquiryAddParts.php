<!-- Include this on each page-->
        <table>
            <tr>
                <td class="blue">Account Manager<td>
                <td class="blue" ><?php if(isset($_POST['accManagerID'])) { echo $userController->getUserByID($_POST['accManagerID'])->name;   ?>
                    <input type="hidden" name="accManagerID" value="<?php echo $_POST['accManagerID']; ?>"/><?php } ?></td>
            </tr>
        </table>
        <!-- END -->
            <?php
$customerCode = explode("-", $_POST['customerName']);
print "<table>";
print "<tr>";
print "<td class='tdLabel'>Reference</td>";
print "<td>" . $theEnquiry->ourReference . "</td>";
print "<td class='tdLabel'>Date</td>";
print "<td>" . $_POST['calendar'] . "</td>";
print "<td class='tdLabel'>Client Ref</td>";
print "<td>" . $_POST['customerRef'] . "</td>";
print "</tr>";
print "<tr>";
print "<td class='tdLabel'>POC</td>";
print "<td>" . $userController->getUserByID($_POST['pointOfContact'])->name . "</td>";
print "<td class='tdLabel'>Type</td>";
print "<td>" . $enquiryTypeController->getEnquiryTypeByID($_POST['enquiryType'])->type . "</td>";
print "<td></td>";
print "<td></td>";
print "</tr>";
print "<tr>";
print "<td class='tdLabel'>Company</td>";
print "<td>" . $_POST['customerName'] . "</td>";
print "<td></td>";
print "<td></td>";
print "<td class='tdLabel'>Client Code</td>";
print "<td>" . $customerCode[0] . "</td>";
print "</tr>";
print "<tr>";
print "<td class='tdLabel'>Customer Contact</td>";
print "<td>" . $theCustomerContact->salutation . " " . $theCustomerContact->firstName . " " . $theCustomerContact->surname . "</td>";
print "<td></td><td></td>";
print "<td class='tdLabel'>Show Details in Quote?</td>";
print "<td>" . ($_POST['useInst'] ? "Yes" : "No") . "</td>";
print "</tr>";
print "<tr>";
print "<td class='tdLabel'>Email</td>";
print "<td>" . $theCustomerContact->email . "</td>";
print "<td class='tdLabel'>Phone</td>";
print "<td>" . $theCustomerContact->number . "</td>";
print "<td class='tdLabel'>Mobile</td>";
print "<td>" . $theCustomerContact->mobile . "</td>";
print "</tr>";

print "</table>";
?>

<h3>Engines</h3>

<?php
if (isset($_POST['selectEngine'])) {
    foreach ($_POST['selectEngine'] as $tab => $engine) {
        $designerModel = $installationEngineController->getInstallationEngineByID($engine);
        print '<a href="index.php?portal=quotes&page=addParts&engine">' . $designerModel->engineManufacturer . '-' . $designerModel->engineModel . '</a>&nbsp;';
    }
}
if (isset($_POST['engine'])) {
    foreach ($_POST['engine'] as $unique => $engine) {
        print '<a href="#">' . $engine['designer'] . '-' . $engine['model']. ' [' . $engine['cyl']. ' Cyl]</a>&nbsp;';
        
    }
}
?>
<table>
    <tr>
        <th>Line</th>
        <th>Customer Ref</th>
        <th>Quantity</th>
        <th>Part Description</th>
    </tr>
    <tr>
        <td><input type="text" name="line" size="5" /></td>
        <td><input type="text" name="customerRef" /></td>
        <td><input type="text" name="quantity" /></td>
        <td><input type="text" name="description" /></td>
    </tr>
    <tr>
        <td></td>
        <td>Add Line after</td>
        <td><input type="text" name="lineAfter" size="5" /></td>
        <td><input type="submit" name="submit" value="Add Part"/></td>
    </tr>
</table>

<?php

$numberOfField = 10;
print "<table class='borderedTable'>";
print "<tr><th>Line</th>"
        . "<th>Qty</th>"
        . "<th>Cust. Ref.</th>"
        . "<th>Description</th>"
        . "<th>Actions</th></tr>";
for ($i = 1; $i <= 10; $i++) {
    print "<tr><td>" . $i . "</td>"
            . "<td>7</td>"
            . "<td>NSN-34563-shD</td>"
            . "<td>o-ring rubber bore 13mm</td>"
            . "<td><input type='button' name='delete' value='Delete' /> "
            . "<input type='button' name='altItem' value='Alt Item'/> "
            . "<input type='button' name='edit' value='Edit'/></td></tr>";
}
print "<tr>"
. "<td></td><td></td><td></td><td></td><td><button>Delete All Parts</button></td></tr>"; 
/* TODO: Discuss do we want to delete all on paginated page or ALL items in parts list.*/

print "</table>";

