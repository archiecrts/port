<?php

/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

?>
<h4>Add New Vessel</h4>
<br/>
<form id="addNewVessel" action="" method="post" enctype="multipart/form-data">
    <input type="hidden" id="vesselID"/>
    <table>
        <tr> 
            <td>Lloyds #</td> <!-- Save in Installations table, inst_imo field -->
            <td><input type="text" id="imo" required class="addVesselFields"/></td> 
        </tr>
        <tr>
            <td>Vessel Name</td> <!-- Save in Installations table, inst_installation_name field -->
            <td><input type="text" id="vesselName" required class="addVesselFields"/></td>  
        </tr>
       
        <tr>
            <td>Year Built</td> 
            <td><input type="Text" required id="year" class="addVesselFields" /> 
            </td>
        </tr>

        <tr>
            <td>Yard Built</td> 
            <td><input type="Text" required id="yeardBuilt" class="addVesselFields"/>
            </td>
        </tr>

        <tr>
            <td>Vessel Owner</td> 
            <td><input type="Text" required id="vesselOwner" class="addVesselFields"/>
            </td>
        </tr>
        
        <tr>
            <td>Vessel Owner</td> 
            <td><input type="Text" required id="vesselOwner" class="addVesselFields"/>
            </td>
            <td>Owner Country</td> 
            <td><input type="Text" required id="vesselOwnerCountry" class="addVesselFields"/>
            </td>
        </tr>
        
        <tr>
            <td>Vessel Operator</td> 
            <td><input type="Text" required id="vesselOperator" class="addVesselFields"/>
            </td>
            <td>Operator Country</td> 
            <td><input type="Text" required id="vesselOperatorCountry" class="addVesselFields"/>
            </td>
        </tr>
        
        <tr>
            <td>Vessel Manager</td> 
            <td><input type="Text" required id="vesselManager" class="addVesselFields"/>
            </td>
        <br/>
            <td>Manager Country</td> 
            <td><input type="Text" required id="vesselManagerCountry" class="addVesselFields"/>
            </td>
        </tr>    
    </table> 
    <br/>
    <hr/>
    <br/>

    
    <input type="submit" name="submit" id="submitAddVessel" value="Save Vessel" />

    <input type="button" name="close" onclick="window.close()" id="cancelAddVessel" value="Cancel Add Vessel"/>
</form>
<div id="mainContainer">
<div id="container">