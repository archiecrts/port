<?php
/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Alexandra Carrico
 */
include 'views/AutoCompleteSearch/autoCompleteSearchEnquiry.php';
?>
<script>
    $(function () {
        $("#datepicker").datepicker();
    });
</script>



<div id="newEnquiryContainer">
    <div id="selectEnquiryOptions">
    <h4>Please Select Core Enquiry Options</h4>
        <br/>
        <form name="enquiryStepOne" action="index.php?portal=quotes&page=stepTwo" method="post" enctype="multipart/form-data">
        <table id="enquiryTbl">
            <tr>
                <td class="blue">Account Manager<td>
                <td class="blue" id="accManagerDisplay"></td><!--When customer selected, display customer's account manager-->
            </tr>
            
            <tr>
                <td>Customer</td>
                <td><input type="text" id="searchBox" onkeyup="autocomplet()"  name="customerName" placeholder="Type Customer's Name or Code" autocomplete="off">               
                    <li id="searchList"></li>   
                    <input type="hidden" id="customerID" name="customerID" value="0"/></td> <!--Search box with lookup for a customer's name and code, get data from customer's database. In the box should appear both code and name of the company-->  
            </tr>
            
            <tr>
                <td>Customer's Contact</td>
                <td><select id="customerContact" name="customerContact" class="selectCustomerContact"> <!--Select list box. Get data from customer's contact table. Set contacts by forename ascending order-->
                        <option value="" default selected >First choose a customer</option> 
                    </select> 
                    <input type="button" id="addContact" name="addContact" value="Add Contact"> <!--If contact not listed then add contact pop-up window-->
                    </td>
            </tr>
            
            <tr> 
                <td>Received Date</td> 
                <td><input type="text"  value="<?php print(date("Y-m-d")); ?>" name="calendar" class="enquiryFields" id="datepicker" style='width:85px;'/> <!--Pop-up calendar to select the enquiry date-->
                </td>
            </tr>

            <tr>
                <td>Customer's Ref</td> 
                <td><input type="Text" id="customerRef" name="customerRef" class="enquiryFields" placeholder="Customer's Enquiry Ref."/> <!-- Text box to type customer's enquiry reference-->
                </td>
            </tr>
            
            <tr>
                <td>Enquiry Type</td> 
                <td><select name="enquiryType" class="enquiryselectOptions"> <!-- List option box. Get data from the enquiry type table. Default to the user's department-->
                        <option value="" default selected hidden>Select</option>
                            <?php
                            $enquiryType = $enquiryTypeController->getAllEnquiryTypes();
                            if (isset($enquiryType)){
                                // TODO add user level and department level to show options as required.
                                foreach ($enquiryType as $index => $type) {
                                    print "<option value='" . $type->typeID . "'>";
                                    print $type->type;
                                    print "</option>";
                                }
                            }
                            ?>
                    </select>
                </td>
            </tr>
            
            <tr>
                <td>Quote Responsible</td> 
                <td><select name="pointOfContact" id="pointOfContact" class="enquiryselectOptions"> <!-- List option box. Get data from company's users table-->
                        <option value="" default selected hidden>Select</option>    
                            <?php
                            $quoteContact = $userController->getUsers();
                            if (isset($quoteContact)) {
                                foreach ($quoteContact as $index => $user) {
                                    print "<option value='" . $user->id . "'>";
                                    print $user->name;
                                    print "</option>";
                                }
                            }
                            ?>
                    </select></td> 
            </tr>
        </table>
        <br>
        <hr></hr>
        <br>
         
        <input type="submit" name="submit" id="submitEnquiryButton" value="Next" />
 
        <input type="reset" name="reset" id="cancelEnquiryButton" value="Cancel Enquiry Creation"/>
        
        </form>
    </div> <!-- end enquiry container -->
    
    <div id="customerNotesContainer"> <!--Warning box, displaying customer warning notes-->
    </div> <!-- end customer notes container -->
    
</div> <!-- end new enquiry container-->
