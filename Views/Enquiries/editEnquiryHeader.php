<div id="editEnquiryHeader">
<h2> Edit Header For Enquiry...</h2>

<h4>Print Enquiry Header</h4>

<form id="editEnqHeader" action="" method="post" enctype="multipart/form-data">
    <table id="enquiryTbl">
    
            <tr>
                <td>Customer</td>
                <td><input type="text" id="searchBox" onkeyup="autocomplet()"  name="customerName" placeholder="Type Customer's Name or Code" autocomplete="off">               
                    <li id="searchList"></li>   
                    <input type="hidden" id="customerID" name="customerID" value="0"/></td><!--Search box with lookup for a customer's name and code, get data from customer's database. In the box should appear both code and name of the company.-->  
            </tr>
            
            <tr>
                <td>Customer's Contact</td>
                <td><select id="customerContact" name="customerContact" class="selectCustomerContact"> <!--Select option box. Get data from customer contacts table in customer database. Set contacts by forename ascending order.  -->
                        <option value="" default selected >First choose a customer</option> 
                    </select> 
                    <input type="button" id="addContact" name="addContact" value="Add Contact"><!--If contact not listed then pop-up  window add contact-->
                    </td>
            </tr>
            
            <tr> 
                <td>Received Date</td> 
                <td><input type="Text"  name="calendar" class="enquiryFields" id="datepicker" placeholder="Select Date"/> <!--Pop up calendar to select the enquiry date-->
                </td>
            </tr>

            <tr>
                <td>Customer's Ref</td> 
                <td><input type="Text" id="customerRef" name="customerRef" class="enquiryFields" placeholder="Customer's Enquiry Ref."/> <!-- Text box to type customer's reference-->
                </td>
            </tr>

            <tr>
                <td>Enquiry Type</td> 
                <td><select name="enquiryType"  class="enquiryselectOptions"> <!-- Select option box. Get data from the enquiry type table-->
                        <option value="" default selected hidden>Select</option>
                            <?php
                            $enquiryType = $enquiryTypeController->getAllEnquiryTypes();
                            if (isset($enquiryType)){
                                // TODO add user level and department level to show options as required.
                                foreach ($enquiryType as $index => $type) {
                                    print "<option value='" . $type->typeID . "'>";
                                    print $type->type;
                                    print "</option>";
                                }
                            }
                            ?>
                    </select></td>
            </tr>
            
            <tr>
                <td>Quote Responsible</td> 
                <td><select name="pointOfContact" id="pointOfContact"  class="enquiryselectOptions"> <!-- Select option box. Get data from company's users table-->
                        <option value="" default selected hidden>Select</option>    
                            <?php
                            $quoteContact = $userController->getUsers();
                            if (isset($quoteContact)) {
                                foreach ($quoteContact as $index => $user) {
                                    print "<option value='" . $user->id . "'>";
                                    print $user->name;
                                    print "</option>";
                                }
                            }
                            ?>
                    </select></td> 
            </tr>
        </table>
        <br>
        <hr></hr>
        <br>
    <input type="submit" name="submit" id="submitUpdateEnqHeader" value="Update Enquiry Header" />

    <input type="button" name="close" onclick="window.close()" id="cancelEditEnqHeader" value="Cancel Edit Header"/>
    
    </form>
</div>
