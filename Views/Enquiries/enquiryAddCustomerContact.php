<link href="../../css/EnquiriesScreen.css" rel="stylesheet" type="text/css"/>
<link href="../../css/main.css" rel="stylesheet" type="text/css"/>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<?php
/*
 * There needs to be some inline SQL here because this ajax page is decoupled from the MVC Framework.
 */

include_once("../../Models/Database.php");

function getCustomerName($customerID) {
    $result = mysqli_query(Database::$connection, "SELECT c.cust_name FROM customer_tbl AS c WHERE cust_id = '" . $customerID . "'");
    $row = mysqli_fetch_assoc($result);
    $array["cust_name"] = $row['cust_name'];
    return $array;
}

function getCustomerOffices($customerID) {
    $offices = mysqli_query(Database::$connection, "SELECT a.cadt_id, a.cadt_office_name FROM customer_address_tbl AS a WHERE customer_tbl_cust_id = '" . $customerID . "'");
    while ($row1 = mysqli_fetch_assoc($offices)) {

        $array[$row1['cadt_id']] = $row1['cadt_office_name'];
    }
    if (isset($array)) {
        return $array;
    } else {
        return null;
    }
}

// END OF INLINE SQL


$custID = "";
if (isset($_GET['customerID'])) {
    $custID = $_GET['customerID'];
}
$cust = getCustomerName($custID);
$offices = getCustomerOffices($custID);
?>
<script type="text/javascript">
    $(function () {
        $("form").submit(function (event) {
           // alert("Handler for .submit() called.");
            event.preventDefault(); // this stops the traditional submit
            // variables
            var customerID = $('#customerIDField').val();
            var officeName = $('#officeName').val();
            var salutation = $('#salutation').val();
            var firstName = $('#firstName').val();
            var surname = $('#surname').val();
            var email = $('#email').val();
            var number = $('#number').val();
            var mobile = $('#mobile').val();
            var jobTitle = $('#jobTitle').val();
            $.ajax({
                url: '../../Views/Enquiries/enquiryAddCustomerContactAjax.php',
                type: 'POST',
                data: {customerID: customerID,
                        officeName: officeName,
                        salutation: salutation,
                        firstName: firstName,
                        surname: surname,
                        email: email,
                        number: number,
                        mobile: mobile,
                        jobTitle: jobTitle},
                success: function (data) {
                   
                    if (window.opener !== null && !window.opener.closed) {
                        var select = window.opener.document.getElementById("customerContact");
                        
                        if (isIE()) {
                            var option = window.opener.document.createElement("option");
                            option.text = salutation + " " + firstName + " " + surname;
                            option.value = data;
                            select.add(option, data);
                            select.value = data;
                        } else {
                            select.options[select.options.length] = new Option(salutation + " " + firstName + " " + surname, data);
                            select.value = data;
                        }
                    }
                    window.close();
                    
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + " " + thrownError);
                }
            });
        });
    });
    
    function isIE(userAgent) {
        userAgent = userAgent || navigator.userAgent;
        return userAgent.indexOf("MSIE ") > -1 || userAgent.indexOf("Trident/") > -1;
      }

</script>

<div id="addNewContactContainer">
<h4>Add New Customer Contact to <?php echo $cust['cust_name']; ?></h4>
<br/>
<form id="addNewContact" action="" method="post" enctype="multipart/form-data">
    <input type="hidden" id="customerIDField" value="<?php echo $custID; ?>"/>
    <table id="addContactTbl">
        <tr>
            <td>Office</td> 
            <td><select id="officeName" name="officeName" required class="addContactFields"> <!-- Select option box. Get the data from the office's customer table in customer database-->
                    <option value="" default selected hidden>Select</option>
                    <?php
                    if (isset($offices)) {
                        foreach ($offices as $index => $value) {
                            if ($value == "") {
                                $value = "Main Office";
                            }
                            print "<option value='" . $index . "'>";
                            print $value;
                            print "</option>";
                        }
                    } else {
                        print "<option value=''>None Available<option>";
                    }
                    ?> 
                </select></td> 
        </tr>

        <tr>
            <td>Position</td> 
            <td><input type="text" id="jobTitle" class="addContactFields"/></td> 
        </tr>
        
        <tr>
            <td>Title</td> 
            <td><select name="salutation" id="salutation" class="addContactFields"> <!-- Select option box. Get data from company's users table-->
                    <option value="" default selected>Please Select Title</option>
                    <option value="Miss">Miss</option>
                    <option value="Mr">Mr</option>
                    <option value="Mrs">Mrs</option>
                    <option value="Dr">Dr.</option>
                    </select></td> 
        </tr>

        <tr>
            <td>First name</td> 
            <td><input type="Text" id="firstName" class="addContactFields"/></td> <!-- Tex box to type customer's contact forename. To be add in the customer's point of contact table in customer database-->
        </tr>
        
        <tr>    
            <td>Surname</td> 
            <td><input type="Text" required id="surname" class="addContactFields"/></td>
        </tr>

        <tr>
            <td>Email</td> 
            <td><input type="Text"  id="email" class="addContactFields"/> <!-- Tex box to type customer's contact surname. To be add in the customer's point of contact table in customer database-->
            </td>
        </tr>

        <tr>
            <td>Direct Telephone</td> 
            <td><input type="Text" id="number" class="addContactFields"/> <!-- Tex box to type customer's contact surname. To be add in the customer's point of contact table in customer database-->
            </td>
        </tr>
        <tr>
            <td>Mobile</td> 
            <td><input type="Text" id="mobile" class="addContactFields"/> <!-- Tex box to type customer's contact surname. To be add in the customer's point of contact table in customer database-->
            </td>
        </tr>
    </table> 
    <br/>
    <hr/>
    <br/>

    <input type="submit" name="submit" id="submitAddContact" value="Save Contact" />

    <input type="button" name="close" onclick="window.close()" id="cancelAddContact" value="Cancel Add Contact"/>
</form>
</div>