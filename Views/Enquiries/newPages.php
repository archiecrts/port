<?php

/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
?>

<br/>
<h1 style="color: blue;">This process will be created soon, please be a little more patient...</h1>
<br/>
<h2>In the meantime... and if you are bored...</h2>
<h2>why not learn a little more of other languages...</h2>
                    
<br/>
<table>
    <tr>
        <th style="width: 125px; border: 2px solid green;">English</th>
        <th style="width: 125px; border: 2px solid green;">Portuguese</th>
        <th style="width: 125px; border: 2px solid green;">Spanish</th>
        <th style="width: 125px; border: 2px solid green;">French</th>
        <th style="width: 125px; border: 2px solid green;">Deutsch</th>
        <th style="width: 125px; border: 2px solid green;">Italian</th>
        <th style="width: 125px; border: 2px solid green;">Polish</th>
        <th style="width: 125px; border: 2px solid green;">Greek</th>
        <th style="width: 125px; border: 2px solid green;">Indonesian</th>
        <th style="width: 125px; border: 2px solid green;">Japanese</th>
    </tr>
    <tr>
        <th>Hello</th>
        <th>Olá</th>
        <th>Hola</th>
        <th>Salut</th>
        <th>Hallo</th>
        <th>Ciao</th>
        <th>Cześć!</th>
        <th>Chaírete</th>
        <th>Salam!</th>
        <th>Kon'nichiwa</th>
    </tr>
    
    <tr>
        <th>Thanks</th>
        <th>Obrigado/a</th>
        <th>Gracias</th>
        <th>Merci</th>
        <th>Dank</th>
        <th>Grazie</th>
        <th>Dzięki</th>
        <th>Efcharistó</th>
        <th>Terima kasih</th>
        <th>Kansha</th>
    </tr>
    
    <tr>
        <th>Goodbye</th>
        <th>Tchau</th>
        <th>Adiós</th>
        <th>Au Revoir</th>
        <th>Auf Wiedersehen</th>
        <th>Addio</th>
        <th>Do Widzenia</th>
        <th>Antío</th>
        <th>Selamat Tinggal</th>
        <th>Sayōnara</th>
    </tr>
    
</table>
