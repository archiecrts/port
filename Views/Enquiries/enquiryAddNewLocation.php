<?php

/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

?>
<script>
    $(function () {
        $("#datepicker").datepicker();
    });

</script>

<h4>Add New Location</h4>
<br/>
<form id="addNewLocation" action="" method="post" enctype="multipart/form-data">
    <input type="hidden" id="locationID"/>
    <table>
        <tr>
            <td>Location Name</td> <!-- Save in Installations table, inst_installation_name field -->
            <td><input type="text" id="locationName" required class="addLocationFields"/></td>  
        </tr>
        
        <tr>
            <td>Installation Type</td> <!-- Save in Installations table, inst_installation_name field -->
            <td><select name="installationType" id="title" class="addLocationFields"> <!-- Select option box. Get data from company's users table-->
                    <option value="" default selected>Please Select Type</option>
                    <option value="factory">Factory</option>
                    <option value="powerStation">Power Station</option>
                    <option value="mine">Mine</option>
                </select></td>
        </tr>
        
        <tr>
            <td>Year Built</td> <!-- Save in Installations table, inst_installation_name field -->
            <td><input type="date" id="yearBuilt" required class="addLocationFields"/></td>  
        </tr>
 
        <tr>
            <td>Fuel Type</td> <!-- Save in Installations table, inst_installation_name field -->
            <td><select name="fuelType" id="title" class="addLocationFields"> <!-- Select option box. Get data from company's users table-->
                    <option value="" default selected>Please Select Type</option>
                    <option value="gas">Gas</option>
                    <option value="hfo">HFO</option>
                    <option value="mdo">MDO</option>
                </select></td>  
        </tr>
        
        <tr>
            <td>Next Top O/H</td> 
            <td><input type="text" name="calendar" class="addLocationFields" id="datepicker"/> 
            </td>
        </tr>

        <tr>
            <td>Next Major O/H</td> 
            <td><input type="text" name="calendar" class="addLocationFields" id="datepicker"/> 
            </td>
        </tr>
        
        <tr>
            <td>Owner</td> 
            <td><input type="Text" required id="ownerName" class="addLocationFields"/>
            </td>
        </tr>
         
        <tr>
            <td>Owner Address</td> 
            <td><input type="Text" required id="ownerAddress" class="addLocationFields"/>
            </td>
        </tr>
        
        <tr>
            <td>Owner Country</td> 
            <td><input type="Text" required id="ownerCountry" class="addLocationFields"/>
            </td>
        </tr> 
        
        <tr>
            <td>Operator</td> 
            <td><input type="Text" required id="operatorName" class="addLocationFields"/>
            </td>
        </tr>
         
        <tr>
            <td>Operator Address</td> 
            <td><input type="Text" required id="operatorAddress" class="addLocationFields"/>
            </td>
        </tr>
        
        <tr>
            <td>Operator Country</td> 
            <td><input type="Text" required id="operatorCountry" class="addLocationFields"/>
            </td>
        </tr> 
        
        <tr>
            <td>Manager</td> 
            <td><input type="Text" required id="managerName" class="addLocationFields"/>
            </td>
        </tr>
         
        <tr>
            <td>Manager Address</td> 
            <td><input type="Text" required id="managerAddress" class="addLocationFields"/>
            </td>
        </tr>
        
        <tr>
            <td>Manager Country</td> 
            <td><input type="Text" required id="managerCountry" class="addLocationFields"/>
            </td>
        </tr> 
        
    </table> 
    <br/>
    <hr/>
    <br/>

    
    <input type="submit" name="submit" id="submitAddLocation" value="Save Location" />

    <input type="button" name="close" onclick="window.close()" id="cancelAddLocation" value="Cancel Add Location"/>
</form>