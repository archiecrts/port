<?php

/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */


$enquiryType="";
if (isset($_POST['enquiryType']) && ($_POST['enquiryType'] != "")) {
    $enquiryType = $_POST['enquiryType'];
}

switch ($enquiryType) {
    case "1": 
    case "4":
    case "5": 
        include ("Views/Enquiries/enquiryDiesel.php");
        break;
    
    case "2": 
        include ("Views/Enquiries/enquirySeals.php");
        break;

    case "6":
    case "7":
    case "2":
        include ("Views/Enquiries/enquiryMarine.php");
        break;
     
    case "3":
        include ("Views/Enquiries/enquiryIncomeCommissions.php");
        break;
    
    default:
        include ("Views/SOP/sopMainDashboard.php");
        break;
}
