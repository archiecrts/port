<link href="../../css/EnquiriesScreen.css" rel="stylesheet" type="text/css"/>
<script>

$(function () {
        $('#searchIMO').click(function () {
         
            var min_length = 0; // min caracters to display the autocomplete
            var imo = $('#vesselIMO').val();
            if (imo.length >= min_length) {
                $.ajax({
                    url: './Views/Enquiries/AjaxSearchForIMO.php',
                    type: 'POST',
                    data: {vesselIMO: imo},
                    success: function (data) {
                      
                        $('#vesselName').val(data);
                       
                    }
                });
            }
        });
    });
</script>
<?php

/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
?>
<div id="selectMarineVesselContainer">
    <div id="selectEnquiryOptions">
        <!-- Include this on each page-->
        <table>
            <tr>
                <td class="blue">Account Manager<td>
                <td class="blue" ><?php if(isset($_POST['accManagerID'])) { echo $userController->getUserByID($_POST['accManagerID'])->name;   ?>
                    <input type="hidden" name="accManagerID" value="<?php echo $_POST['accManagerID']; ?>"/><?php } ?></td>
            </tr>
        </table>
        <!-- END -->
        <h4>Please Add Vessel Details</h4>
        <br/>
        <form name="enquiryMarine" action="index.php?portal=quotes&page=addParts" method="post" enctype="multipart/form-data">
            <table id="vesselIMOTbl"> <!-- Installation table. -->
                <tr>
                    <td>IMO</td> <!-- Input IMO no. -->
                    <td><input type="text" name="imo" id="vesselIMO" placeholder="Type IMO Number" /></td>
                    <td><input type="button" name="searchIMO" id="searchIMO" value="Search IMO" /></td> <!-- Search IMO number from the installation table -->
                    <td><input type="button" id="addNewVessel" value="Add New Vessel"/></td>
                </tr>
            </table>
            
            <table id="vesselNameTbl"> <!-- Installation table. -->
                <tr>
                    <td>Vessel Name</td> <!-- If IMO known then populate the vessel name. If not known then add vessel button appears. -->
                    <td><input type="text" name="vessel" id="vesselName"/></td>         
                </tr>
            </table>
            
        <br/>
        <hr/>
            <input type="submit" name="submit" id="submitEnquiryButton" value="Save and Next"/>
            <input type="reset" name="reset" id="cancelEnquiryButton" value="Cancel Enquiry Creation"/>
        </form>
        <br/>
    </div> <!-- end Select Enquiry Options -->
</div> <!-- end select Marine Vessel Container -->