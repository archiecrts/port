<?php

/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include 'views/AutoCompleteSearch/autoCompleteSearchEnquiry.php';
?>
<script>
    $(function () {
        $("#datepicker").datepicker();
    });
</script>
    <div id="selectCommissionContainer">
    <h4>Please Select Income Commission Options</h4>
        <br/>
        <table id="commissionTbl">   
            <tr>
                <td>Commission Supplier</td>
                <td><input type="text" id="searchBox" onkeyup="autocomplet()"  name="customerName" placeholder="Type Customer's Name or Code" autocomplete="off">               
                    <li id="searchList"></li>   
                    <input type="hidden" id="customerID" name="customerID" value="0"/></td> <!--Search box with lookup for a customer's name and code, get data from customer's database. In the box should appear both code and name of the company-->  
            </tr>
            
            <tr>
                <td>Supplier's Contact</td>
                <td><select id="customerContact" name="customerContact" class="selectCustomerContact"> <!--Select list box. Get data from customer's contact table. Set contacts by forename ascending order-->
                        <option value="" default selected >Choose a commission supplier</option> 
                    </select> 
                    <input type="button" id="addContact" name="addContact" value="Add Contact"></td> <!--If contact not listed then add contact pop-up window--> 
            </tr>

            <tr></td>
                <td>Supplier's Ref</td> 
                <td><input type="Text" id="customerRef" name="customerRef" class="enquiryFields" placeholder="Supplier's Reference"/> <!-- Text box to type supplier's reference-->
                </td>
            </tr>
            
            <tr>
                <td>Installation</td> <!-- Search box with lookup for both IMO and installation name, get data from installations table-->
                <td><input type="text" id="installation" name="installation" onkeyup="autocompletInstallation()" class="engineFields" autocomplete="off" style="width:350px;"/></td>
                <td><input type="button" onClick="window.location='Views/Enquiries/enquiryAddNewInstallation.php';" value="Add New Installation"/> <!--Pop-up window to add new installation-->
                    <li id="searchInstallationList"></li>  
                </td>
            </tr>

            <tr>
                <td>Commission Value</td> 
                <td><input type="number" id="commissionValue" name="commissionValue" class="enquiryFields" style="width:85px;"/><!-- Text box to input commission value-->
                    <select name="currency" id="title" class="commissionCurrency"style="width:175px; margin-left: 15px; float:left; height:22px;"> <!-- Select option box. Get data from company's users table-->
                    <option value="" default selected>Please Select Currency</option>
                    <option value="eur">EUR</option>
                    <option value="gbp">GBP</option>
                    </select>
                </td>
            </tr>
            
            <tr>
                <td>Date Commission Due</td> 
                <td><input type="text"  name="calendar" class="enquiryFields" id="datepicker" style='width:85px;'/> <!--Pop-up calendar to select the enquiry date-->
                </td>
                <td></td>
            </tr>
            
            <tr>
                <td>Date Commission Paid</td> 
                <td><input type="text"  name="calendar" class="enquiryFields" id="datepicker" style='width:85px;'/> <!--Pop-up calendar to select the enquiry date-->
                </td>
            </tr>
        </table>
        <hr/>
        <input type="submit" name="submit" id="submitEnquiryButton" value="Save" />
 
        <input type="reset" name="reset" id="cancelEnquiryButton" value="Cancel Enquiry Creation"/>
    </div>