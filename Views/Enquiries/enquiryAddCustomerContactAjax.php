<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
$customerID = "";
$customerAddressID = "";
$salutation = "";
$firstName = "";
$surname = "";
$jobTitle = "";
$email = "";
$number = "";
$mobile = "";

if (isset($_POST['customerID'])) {
    $customerID = $_POST['customerID'];
    $customerAddressID = $_POST['officeName'];
    $salutation = $_POST['salutation'];
    $firstName = $_POST['firstName'];
    $surname = $_POST['surname'];
    $jobTitle = $_POST['jobTitle'];
    $email = $_POST['email'];
    $number = $_POST['number'];
    $mobile = $_POST['mobile'];
}

include_once("../../Models/Database.php");


$result = "INSERT INTO customer_contacts_tbl (ict_salutation, ict_first_name, ict_surname, ict_job_title, ict_email, "
        . "ict_number, ict_mobile_number, ict_info_source, ict_inactive, ict_contact_validated, "
        . "customer_tbl_cust_id, customer_address_tbl_cadt_id) VALUES "
        . "('".$salutation."', "
        . "'".$firstName."', "
        . "'".$surname."', "
        . "'".$jobTitle."', "
        . "'".$email."', "
        . "'".$number."', "
        . "'".$mobile."', "
        . "'Manually Added', "
        . "'0', "
        . "'1', "
        . "'".$customerID."', "
        . "'".$customerAddressID."')";


if(mysqli_query(Database::$connection, $result)) {
    echo mysqli_insert_id(Database::$connection);
} else {
    echo mysqli_error(Database::$connection);
}
