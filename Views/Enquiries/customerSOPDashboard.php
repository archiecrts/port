
<?php
/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
?>

<h1 align="center">Customer's SOP Dashboard</h1>
<!--<table id="customerSOPHistoryTbl">
        <tr>
            <td>Enquiry Ref</td>
            <td>D16XXXXX</td>
            <td>Customer Quotes</td>
            <td>D16XXXX.1</td>
            <td>Customer Order</td>
            <td>Customer Deliveries</td>
            <td>Not Available</td>
            <td>Customer Invoices</td>
            <td>Not Available</td>
        </tr>
        <tr>
            <td>Supplier Quote Requests</td>
            <td>Not Available</td>
            <td>Supplier Orders</td>
            <td>Not Available</td>
            <td>Supplier Deliveries</td>
            <td>Not Available</td>
            <td>Supplier Invoices</td>
            <td>Not Available</td>
        </tr>

        <tr>
            <td>Corrective Actions</td>
        </tr>
    </table>-->
<br/>

    <table id="enquiryDashboardTbl">
        <tr>
            <th>Enquiry Core</th>
            <th>Enquiry Items</th>
            <th>Quote</th>
            <th>Order</th>
            <th>Despatch</th>
            <th>Invoice</th>
            <th>Corrective Actions</th>
        </tr>
        
        <tr>
            <td><a href="/port/index.php?portal=quotes&page=editEnquiryHeader"><img src="/port/icons/edit_pencil.png" alt="" height='20px'>Edit Header</a></td>
             <td><a href="/port/index.php?portal=quotes&page=addParts"><img src="/port/AlexIcons/add_icon.png" alt="" height='20px'>Add Parts To Enquiry</a></td>
             <td><a href="/port/index.php?portal=quotes&page=stepOne"><img src="/port/AlexIcons/add_icon.png" alt="" height='20px'>Generate Quote</a></td>
        </tr>
        
        <tr>
            <td><a href="/port/index.php?portal=quotes&page=stepOne"><img src="/port/icons/edit_pencil.png" alt="" height='20px'>Edit Installations & Engines</a></td>
            <td><a href="/port/index.php?portal=quotes&page=stepOne"><img src="/port/AlexIcons/add_icon.png" alt="" height='20px'>Bulk Add Lines To Enquiry</a></td>
            <td><a href="/port/index.php?portal=quotes&page=stepOne"><img src="/port/AlexIcons/magnifying_glass.png" alt="" height='20px' >View/Generate Quote</a></td>
        </tr>
        
        <tr>
            <td><a href="/port/index.php?portal=quotes&page=stepOne"><img src="/port/AlexIcons/commissions_icon.png" alt="" height='20px'>Process Commissions</a></td>
            <td><a href="/port/index.php?portal=quotes&page=stepOne"><img src="/port/AlexIcons/magnifying_glass.png" alt="" height='20px' >View Enquiry Items</a></td>
            <td><a href="/port/index.php?portal=quotes&page=stepOne"><img src="/port/AlexIcons/process_icon.png" alt="" height='20px'>Auto Validate Enquiry Items</a></td>
        </tr>
        
        <tr>
        <td><a href="/port/index.php?portal=quotes&page=stepOne"><img src="/port/AlexIcons/process_icon.png"alt="" height='20px' >Close Enquiry</a></td>
        <td><a href="/port/index.php?portal=quotes&page=stepOne"><img src="/port/icons/edit_pencil.png" alt="" height='20px' >Edit Enquiry Items</a></td>     
        </tr>

        
        <tr>
            <td></td>
            <td><a href="/port/index.php?portal=quotes&page=stepOne"><img src="/port/AlexIcons/add_icon.png" alt="" height='20px'>Add Service Component</a></td>  
        </tr>
        
        <tr>
            <td></td>
            <td><a href="/port/index.php?portal=quotes&page=stepOne"><img src="/port/AlexIcons/sort_icon.png" alt="" height='20px'>Sort Enquiry Items</a></td>
        </tr>
        
        <tr>
            <td></td>
            <td><a href="/port/index.php?portal=quotes&page=stepOne"><img src="/port/AlexIcons/import_icon.png" alt="" height='20px'>Import Enquiry Items</a></td>
        </tr>
        
        <tr>
            <td></td>
            <td><a href="/port/index.php?portal=quotes&page=stepOne"><img src="/port/AlexIcons/export_icon.png" alt="" height='20px'>Export Enquiry Items</a></td>
        </tr>
    </table>
        

<br>
        <hr></hr>
<a href="http://localhost/port/index.php?portal=quotes"><img src="/port/icons/returnArrow.png" alt="" height='20px'>Return To Main View</a>