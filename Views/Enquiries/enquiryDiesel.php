<script>
    function deleteEngineRow(event) {
        var id = event.id;
        var split = id.split('_');
        $('#selectedEngines tr#engine' + split[1]).remove();
    }
    
    $(function () {
        $('#serialNoCheck').click(function () {

            var min_length = 0; // min caracters to display the autocomplete
            var serialNo = $('#serialNo').val();
            if (serialNo.length >= min_length) {
                $.ajax({
                    url: './Views/Enquiries/AjaxSearchForSerialNumber.php',
                    type: 'POST',
                    data: {serialNumber: serialNo},
                    success: function (data) {
                        var engine = data.split('|');
                        $('#engineMake').val(engine[0]);
                        $('#engineModel').val(engine[1]);
                        $('#installation').val(engine[4]);
                        
                        $('#cylinders').attr('value', engine[2]);
                        $('#installationHidden').attr('value', engine[3]);
                    }
                });
            }
        });

        $('.selectEngine').click(function () {
            var id = $(this).attr('data-id');
            var sn = $(this).attr('data-sn');
            var designer = $(this).attr('data-designer');
            var model = $(this).attr('data-model');
            var cyl = $(this).attr('data-cyl');
            var inst = $(this).attr('data-inst');
            if (this.checked) {
                var newRow = "<tr id='engine" + id + "'><td>" + sn + "</td><td>" + designer + "</td><td>" + model + "</td><td>" + cyl + "</td><td>" + inst + "</td><td></td></tr>";
                $('#selectedEngines').append(newRow);
            } else {
                $('#selectedEngines tr#engine' + id).remove();
            }
        });

        $('#addEngine').click(function () {
            var id = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5);

            var sn = $('#serialNo').val();
            var designer = $('#engineMake').val();
            var model = $('#engineModel').val();
            var cyl = $('#cylinders').val();
            var inst = $('#installation').val();
            //var inc = $("input:radio[name='useInst']:checked").val();

            var newRow = "<tr id='engine" + id + "'><td>" + sn + "</td><td>" + designer + "</td><td>" + model + "</td><td>" + cyl + "</td><td>" + inst + "</td><td align='center'><input type='button' name='deleteEngine' id='button_" + id + "' value='remove' onclick='deleteEngineRow(this)'/></td></tr>";
            $('#selectedEngines').append(newRow);

            $('#serialNo').val("");
            $('#engineMake').val("");
            $('#engineModel').val("");
            $('#cylinders').val("");
            $('#installation').val("");

            var hiddenValueSN = "<input type='hidden' name='engine[" + id + "][sn]' value='" + sn + "'/>";
            var hiddenValueDesigner = "<input type='hidden' name='engine[" + id + "][designer]' value='" + designer + "'/>";
            var hiddenValueModel = "<input type='hidden' name='engine[" + id + "][model]' value='" + model + "'/>";
            var hiddenValueCyl = "<input type='hidden' name='engine[" + id + "][cyl]' value='" + cyl + "'/>";
            var hiddenValueInst = "<input type='hidden' name='engine[" + id + "][inst]' value='" + inst + "'/>";
            var hiddenValueEnqOnly = "<input type='hidden' name='engine[" + id + "][enqOnly]' value='1'/>";
            $('#hiddenValues').append(hiddenValueSN);
            $('#hiddenValues').append(hiddenValueDesigner);
            $('#hiddenValues').append(hiddenValueModel);
            $('#hiddenValues').append(hiddenValueCyl);
            $('#hiddenValues').append(hiddenValueInst);
            $('#hiddenValues').append(hiddenValueEnqOnly);

        });
    });


</script>
<?php

include 'Views/AutoCompleteSearch/autoCompleteSearchEngineDesigner.php';
include 'Views/AutoCompleteSearch/autoCompleteSearchEngineModel.php';
include 'Views/AutoCompleteSearch/autoCompleteSearchInstallation.php';
?>
 <form name="enquiryDieselSpares" action="index.php?portal=quotes&page=addParts" method="post" enctype="multipart/form-data">

<div id="selectEngineInstallationContainer">
    <div id="selectEnquiryOptions">
        <h3>Enquiry for <?php echo $_POST['customerName']; ?></h3>
        <!-- Include this on each page-->
        <table>
            <tr>
                <td class="blue">Account Manager<td>
                <td class="blue" ><?php if(isset($_POST['accManagerID'])) { echo $userController->getUserByID($_POST['accManagerID'])->name;   ?>
                    <input type="hidden" name="accManagerID" value="<?php echo $_POST['accManagerID']; ?>"/><?php } ?></td>
            </tr>
        </table>
        <!-- END -->
        <h4>Please Select Engine and Installation</h4>
        <br/>
            <div id="hiddenValues">
                <?php
                foreach ($_POST as $key => $value) {
                    print "<input type='hidden' name='" . $key . "' value='" . $value . "'/>";
                }
                ?>
            </div>

            <table id="engineDetailsTbl"> 
                <tr>
                    <td>Serial Number</td> <!--Input serial number and search if it exists in the system. If it does populate the fields below automatically-->
                    <td><input type="text" id="serialNo" name="serialNo" class="engineFields"/></td>
                    <td><input type="button" id="serialNoCheck" value="Search"/></td>
                </tr>
                
                <tr>
                    <td>Engine Make</td> <!-- If the serial number doesn't exist or we do not have a serial number to input in the field above, then search box with lookup for an engine make, get data from engine make table-->
                    <td><input type="text" id="engineMake" onkeyup="autocomplet()" name="engineMake" class="engineFields" autocomplete="off"/> 
                    <li id="searchEngineList"></li>  
                    <input type="hidden" id="designerID" name="designerID" value=""/>
                </td>
                </tr>
                
                <tr>
                    <td>Engine Model</td> <!-- Field able to search after engine make is field in. Search box with lookup for an engine model, get data from engine model table. Restrict content to the engine make selected previously-->
                    <td><input type="text" id="engineModel" name="engineModel" onkeyup="autocompletModel()" class="engineFields" autocomplete="off"/>
                    <li id="searchEngineModelList"></li>  
                </td>
                </tr>
                
                <tr>
                    <td>Cylinders</td> <!--Input number of cylinder, if not populated-->
                    <td><input type="number" id="cylinders" name="cylinders" class="engineFields"/></td>
                </tr>
         </table>
        
        <!--If installation name populated automatically then installation type populated automatically according to the type in the installations table-->
        <!--
        <table id="installationTypeTbl">
            <tr>
                <td style="text-align: right;">Installation Type:</td> 
                <td style="width: 50px;"> <input type="radio" style="vertical-align: middle; position: relative; top:-2px" name="instType" value="0" checked="checked"/>Sea <input  type="radio" style="vertical-align: middle; position: relative; top:-2px" name="instType" value="1"/>Land </td>
            </tr>
        </table> 
       -->
        
       
        <table id="installationTbl">
            <tr>
                <td>Installation</td> <!--Installation name. If serial number is linked with Installation, the field will be populated automatically. If there is no serial number but we have received the installation then search box with lookup for both IMO and installation name, get data from installations table-->
                <td><input type="text" id="installation" name="installation" onkeyup="autocompletInstallation()" class="engineFields" autocomplete="off"/>
                    <td><input type="button" onClick="window.location='Views/Enquiries/enquiryAddNewInstallation.php';" value="Add New Installation"/></td> <!--Pop-up window to add new installation-->
                    <li id="searchInstallationList"></li>  
                </td>
             </tr>
        </table>
       
        <table id="showInstallationTbl">
            <tr>
                <td style="text-align: right;">Show Installation In Quote:</td>
                <td style="width: 50px;"> <input type="radio" style="vertical-align: middle; position: relative; top:-2px" name="useInst" value="1"/>Yes <input  type="radio" style="vertical-align: middle; position: relative; top:-2px" name="useInst" value="0"  checked="checked"/>No</td>
            </tr>
        </table>
        
            <br/>
            <input type="button" id="addEngine" name="addEngine" value="Add To Enquiry"/>
            <br/>
            <hr/>
            <br/>
            <input type="submit" name="submit" id="submitEnquiryButton" value="Save and Next" />
            <input type="reset" name="reset" id="cancelEnquiryButton" value="Cancel Enquiry Creation"/>

    </div> <!-- end Select Enquiry Options container -->
    
    <div id="availableInstallationsContainer">
        <h4>Available Installations/Engines For <?php echo $_POST['customerName']; ?></h4>
        <table class="borderedTable">
            <tr>
                <th>Select/Edit</th>
                <th>SN</th>
                <th>Designer</th>
                <th>Model</th>
                <th>Cyl #</th>
                <th>Installation</th>
            </tr>
            <?php
            $installationEnginesArray = $installationEngineController->getEnginesByCustomerID($_POST['customerID']);
            if (isset($installationEnginesArray)) {
                foreach ($installationEnginesArray as $index => $engine) {
                    $installation = $installationController->getInstallationByInstallationID($engine->installationID);
                    print "<tr>";
                    if ($engine->engineVerified == 1) {
                        print "<td align='center'><input type='checkbox' class='selectEngine' data-id='" . $engine->installationEngineID . "'"
                                . "data-sn='" . $engine->serialNumber . "' data-designer='" . $engine->engineManufacturer . "' "
                                . "data-model='" . $engine->engineModel . "' data-cyl='" . $engine->engineCylinderCount . "' "
                                . "data-inst='" . $installation->installationName . "' name='selectEngine[" . $engine->installationEngineID . "]' value='" . $engine->installationEngineID . "' /></td>";
                    } else {
                        
        /* If the engines in the side table are not verified but the user has been given a serial number
         * and is SURE that the unverified engine is the same engine as the one with the SN given to the user,
         * then the user can EDIT the record to change the unknown serial number to the new SN. Once this is saved
         * the user can select that engines checkbox.
         * 
         */
                        print "<td align='center'><a href='#'>Edit S/N</a></td>";
                    }
                    print "<td>" . $engine->serialNumber . "</td>"
                            . "<td>" . $engine->engineManufacturer . "</td>"
                            . "<td>" . $engine->engineModel . "</td>"
                            . "<td>" . $engine->engineCylinderCount . "</td>"
                            . "<td>" . $installation->installationName . "</td>
                    </tr>";
                }
            }
            ?>
        </table>
        <br/>
        <h4>Selected Engines</h4>
        <table id="selectedEngines" class="borderedTable">
            <tr>
                <th>SN</th>
                <th>Designer</th>
                <th>Model</th>
                <th>Cyl #</th>
                <th>Installation</th>
                <th>Remove</th>
            </tr>
        </table>
    </div>
</div> <!-- end Select Engine Installation Container-->
</form>