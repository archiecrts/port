<h3>Training</h3>
<script>

    function removeDocument(docID, userID) {
        var server = '<?php print $_SESSION['server']; ?>';
        $.ajax({url: 'Views/Policies/removeDocument.php?file=' + docID + '&action=1&userID=' + userID + '&server=' + server,
            success: function (output) {
                location.href = 'index.php?portal=intranet&page=training';

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status + " " + thrownError);
            }
        });
    }
</script>
<div id="leftColumn">
    <?php
    if (($_SESSION['level'] == '2') || ($_SESSION['level'] == '1')) {
        print '<p><a href="index.php?portal=intranet&page=upload&destination=training">Add More Files</a></p>';
    }

    print '<p>(Right click and <strong>Save As</strong> to save files to your computer)</p>';
    $fileArray = $documentsController->getTrainingDocuments();

    if (isset($fileArray)) {
        print '<table>';
        print '<tr>';
        if (($_SESSION['level'] == '2') || ($_SESSION['level'] == '1')) {
            print '<th></th><th>Remove</th>';
        }
        print '</tr>';

        foreach ($fileArray as $docObject) {

            $explodeUrl = explode("/", $docObject->url);
            print '<tr>';
            print '<td><a href="' . $docObject->url . '" target="blank">' . $explodeUrl[2] . '</a></td>';
            print '<td class="removeDoc"><button class="removeDocButton" onclick="removeDocument(' . $docObject->documentID . ', ' . $_SESSION['user_id'] . ');"><img src="./icons/removeArticle.png" height="15px" border="0"  style="vertical-align: middle;"/></button></td>';
            print '</tr>';

        }

        print '</table>';
    } else {
        print "<p>No Files Available</p>";
    }
    print "</div>";
    print "<div id='rightColumn'>";
    include("noticeBoardFeed.php");
    print "</div>";
    