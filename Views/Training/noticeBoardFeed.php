<h3>Latest Training News</h3>

 <script src="./js/readMoreFeed.js" type="text/javascript"></script>
<?php

$trainingNotices = $noticeBoardController->getTrainingNotices();
if (isset($trainingNotices)) {
    foreach ($trainingNotices as $key => $object) {
        print "<div class='feed'>";
        print "<h4>".$object->title."</h4>";
        print "<div class='item'>";
        print "<p>".substr($object->content, 0, 200)."</p>";
        print "<p><a href='index.php?portal=notice&page=showArticle&article=$object->ID'>go to article</a></p>";
        print "</div>";
        print "<p>".date("d M Y", strtotime($object->date))."</p>";
        print "</div>";
    }
}