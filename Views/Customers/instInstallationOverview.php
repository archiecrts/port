<script>
    
    $(function(){
        $('#managedFleetTbl').tablesorter(); 
   });
   
    $(function () {
        var scrolled = 0;
        $(".selectorDropDownBarManaged").click(function () {
            
                if ($('#managedFleetTbl').css('display') == 'none')
                {
                    $('#managedFleetTbl').removeClass('hiddenBlock').addClass('showTable');
                } else {
                    $('#managedFleetTbl').removeClass('showTable').addClass('hiddenBlock');
                }

            
        });
    });
 </script>
 <style>
    /* Tooltip container */
    .tooltip {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black; /* If you want dots under the hoverable text */
    }

    /* Tooltip text */
    .tooltip .tooltiptext {
        visibility: hidden;
        width: 120px;
        background-color: black;
        color: #fff;
        text-align: center;
        padding: 5px 0;
        border-radius: 6px;

        /* Position the tooltip text - see examples below! */
        position: absolute;
        z-index: 1;
    }

    /* Show the tooltip text when you mouse over the tooltip container */
    .tooltip:hover .tooltiptext {
        visibility: visible;
    }

    .countryFlag {
        margin-right:10px;
        float:right;
        position: relative;
    }

    .countryFlag img {
        margin-top: -12px;
        margin-left: 10px;
        vertical-align: top;
        height:40px;
    }
    
    /* SORTER */
#managedFleetTbl thead tr th.headerSortUp span { 
        /* background: #1D6191;*/
         background-image: url('./icons/arrow-up.png');
 
}

#managedFleetTbl thead tr th.headerSortDown span {
   /* background: #9eafec;*/
  background-image: url('./icons/arrow-down.png');
  
}

/*#managedFleetTbl thead tr th.headerSortUp{
  //background: #9eafec;
}
#managedFleetTbl thead tr th.headerSortDown {
  //background: #1D6191;
}*/

#managedFleetTbl thead {
  cursor: pointer;
  background: #c9dff0;
}
</style>
<div id='installationContainerTechnical' style="padding-top: 110px; margin-top: -110px;">
    <div id="installationHeader">
        <div id="officeLocationsHeader" class="selectorDropDownBarManaged">
            <img src="./icons/gear37.png" alt="" height='20px'/> Managed Fleet (with Engine Count):  [Ship Count <?php echo ($installationsArray > 0 ?  sizeof($installationsArray) : "0"); ?>]
        </div>
    </div>



    <?php
    if (isset($installationsArray)) {
        print "<table style='width:100%;' id='managedFleetTbl'  class='hiddenBlock'>";
        print "<thead>";
        print "<tr>";
        print "<th class='headerSortDown'>IMO <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>";
        print "<th>Installation <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>";
        print "<th>Type <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>";
        print "<th>Main/Aux <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>";
        print "<th>Eng. Designer <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>";
        print "<th>Eng. Model <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>";
        print "<th>Eng. # <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>";
        print "<th>Status <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>";
        print "<th>T.Mgr. <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>";
        print "</tr>";
        print "</thead><tbody>";
        $countForIMO = 1;
        foreach ($installationsArray as $key => $installationObject) {

            $productList = $productController->summaryOfEnginesByInstallationID($installationObject->installationID);
            $rowCol = "";
            if (($countForIMO % 2) == 0) {
                $rowCol = "style='background-color:lightsteelblue;'";
            }

            if (isset($productList)) {
                foreach ($productList as $key => $product) {

                    print "<tr $rowCol>";
                    print "<td>" . $installationObject->imo . "</td>";
                    print "<td><a href='index.php?portal=mr&page=report&action=showInstallation&customerID=" . $customerID . "&installationID=" . $installationObject->installationID . "&tab=installation&reportID=" . $reportOverviewID . "' target='_blank''>" . $installationObject->installationName . "</a></td>";
                    print "<td>" . $installationObject->shipTypeLevel4 . "</td>";
                    print "<td>" . $product['iet_main_aux'] . "</td>";
                    print "<td>" . $designerLookupController->getDesignerByVersionLookupID($product['version_lookup_tbl_vlt_id'])->designerDescription . "</td>";
                    print "<td>" . $product['iet_model_description'] . "</td>";
                    print "<td>" . $product['count(p.prod_id)'] . "</td>";
                    print "<td>" . $installationStatusController->getInstallationStatusByID($installationObject->status)->statusDescription . "</td>";
                    print "<td><a href='index.php?portal=mr&page=report&action=showCustomer&customerID=" . $installationObject->technicalManager . "&reportID=' target='_blank'>" . $customerController->getCustomerByID($installationObject->technicalManager)->customerName . "</a></td>";
                }
            }
            $countForIMO++;
            print "</tr>";
        }
        print "</tbody>";
        print "</table>";
    } else {
        print "<table>";
        print "<tr>";
        print "<td colspan='10'>You have not created any installations. <a href='index.php?portal=mr&page=addNewInstallation&step=installation&customerID=$customerID'>You can create one here</a></td>";
        print "</tr>";
        print "</table>";
    }
    ?>

</div>