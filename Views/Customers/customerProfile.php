<script>
    $(function () {
        var scrolled = 0;
        $(".selectorDropDownBar").click(function () {
            var blockID = $(this).attr('id');



            var block = blockID.split('_');
            if (block[0] === "address") {
                if ($('#customerAddressBlock_' + block[1]).css('display') == 'none')
                {
                    $('#customerAddressBlock_' + block[1]).removeClass('hiddenBlock').addClass('showBlock');
                } else {
                    $('#customerAddressBlock_' + block[1]).removeClass('showBlock').addClass('hiddenBlock');
                }

            }
            if (block[0] === "technical") {
                if ($('#technicalDataBlock_' + block[1]).css('display') == 'none') {
                    $('#technicalDataBlock_' + block[1]).removeClass('hiddenBlock').addClass('showBlock');

                } else {
                    $('#technicalDataBlock_' + block[1]).removeClass('showBlock').addClass('hiddenBlock');

                }
            }
        });
    });

</script>
<div id='installationContainerCustomer' style="padding-top: 110px; margin-top: -110px;">
    <div id="installationSubHeader">
        <h3><?php
            if ($customer->customerName == "") {
                print "Unknown Customer";
            } else {
                print $customer->customerName;
            }
            ?></h3>
    </div>

    <?php
//include './Views/Menus/installationsJumpMenu.php';
    ?>

    <table  style="width:100%;">
        <tr>
            <th colspan="3">
                <img src="./icons/crm_grey.png" alt="" height="20px"/>
                [Ship] Manager</th>
            <th style="text-align: right;"><!--<img src="./icons/edit_pencil.png" alt="edit" height='15px'/> <a href="index.php?portal=mr&page=report&action=showInstallation&tab=customer&reportID=<?php echo $reportOverviewID; ?>&edit=edit&customerID=<?php echo $customerID; ?>">Edit</a>--></th>
        </tr>
        <tr>
            <td class="title">Name</td>
            <td><?php
                if ($customer->customerName == "") {
                    print "Unknown Customer";
                } else {
                    print $customer->customerName;
                }
                ?>
            </td>
            <td class="title">Main Fax</td>
            <td><?php print $customer->mainFax; ?></td>
        </tr>
        <tr>
            <td class="title">Account Type(s)</td>
            <td><?php
                $custStatusArray = $customerStatusController->getAllCustomerStatusValuesByCustomerID($customer->customerID);

                if (isset($custStatusArray)) {
                    foreach ($custStatusArray as $key => $status) {
                        print $customerStatusLookupController->getCustomerStatusLookupByID($status->custStatusLookupID)->custStatusDescription;

                        if ($status->custStatusLookupID != "1") {
                            print " <a href='index.php?portal=suppliers&page=supplierRecord&action=view&customerID=" . $customer->customerID . "&submit=Go'>View</a>";
                        }

                        print "<br/>";
                    }
                }
                ?></td>
            <td class="title">Account Manager</td>
            <td>
                <?php
                if ($customer->accountManager != '') {
                    echo $userController->getUserByID($customer->accountManager)->name;
                } else {
                    echo "Set by territory";
                }
                ?>
            </td>
        </tr>
        <tr>
            <td class="title">Website</td>
            <td><a href="
                <?php
                if (substr($customer->website, 0, 7) == "http://") {
                    print $customer->website;
                } else {
                    print "http://" . $customer->website;
                }
                ?>" target="_blank"><?php print $customer->website; ?></a></td>
            <td class="title">Territory Assigned to</td>
            <td><?php
                $defaultAddressCountry = $customerAddressController->getDefaultCustomerAddressByCustomerID($customer->customerID);

                $territoryAccountManager = $userTerritoryController->getUserIDByTerritory($defaultAddressCountry->country);

                echo $userController->getUserByID($territoryAccountManager)->name;
                ?>
            </td>
        </tr>
        <tr>
            <td class="title">Main Telephone</td>
            <td><?php print $customer->mainTelephone; ?></td>
            <td class="title">Header Notes</td>
            <td><?php echo $customer->headerNotes; ?></td>
        </tr>
        <tr>
            <td class="title"></td>
            <td></td>
            <td class="title">NAVISION ID</td>
            <td><?php echo $customer->navisionID; ?></td>
        </tr>
        <tr>
            <td class="title">Full Name</td>
            <td colspan="3"><?php echo $customer->companyFullName; ?></td>
            
        </tr>

<?php
/* $parentSeawebQuery = $customerController->getCustomerParentByCode($customer->seawebCode);
  $parentOfQuery = $customerController->getAllCustomerChildren($customer->seawebCode); */

/* if ($parentSeawebQuery != '') {
  ?>
  <tr>
  <td class="title">Parent Company</td>
  <td><a href='index.php?portal=mr&page=report&action=showInstallation&installationID=&customerID=<?php echo $parentSeawebQuery->customerID; ?>&submit=Go' target='_blank'><?php print $parentSeawebQuery->customerName; ?></a></td>
  <td class="title"></td>
  <td></td>
  </tr>
  <?php
  }

  if ($parentOfQuery != '') {
  ?>
  <tr>
  <td class="title">Child Companies</td>
  <td class="title">Main phone number</td>
  <td class="title">Website</td>
  <td class="title">Country</td>
  </tr>
  <?php
  foreach ($parentOfQuery as $key => $parent) {
  ?>
  <tr>
  <td class="title"><a href='index.php?portal=mr&page=report&action=showInstallation&installationID=&customerID=<?php echo $parent->customerID; ?>&submit=Go' target='_blank'><?php print $parent->customerName; ?></a></td>
  <td><?php print $parent->mainTelephone; ?></td>
  <td><?php print $parent->website; ?></td>
  <td><?php echo $customerAddressController->getDefaultCustomerAddressByCustomerID($parent->customerID)->country; ?></td>
  </tr>
  <?php
  }
  }
 */
?>
    </table>
</div>

<div id="officeLocations" style="padding-top: 110px; margin-top: -110px;">
    <div id="officeLocationsHeader"><img src="./icons/office.png" alt="" height='20px'/> Office Locations:
        <!--<img src="./icons/edit_pencil.png" alt="edit" height='15px'/> &nbsp; <a href="index.php?portal=mr&page=report&action=showInstallation&customerID=<?php echo $customerID; ?>&tab=customerAddress&reportID=<?php echo $reportOverviewID; ?>&edit=add">Add Address</a>-->
    </div>
<?php
if (isset($customerAddresses)) {
    foreach ($customerAddresses as $key => $address) {
        
        ?>
            <div id="address_<?php echo $address->addressID; ?>" class="selectorDropDownBar">
            <?php
            echo $address->country; //$address->city . ", " . 
            if ($address->defaultAddress === '1') {
                print " (default address)";
            }
            ?>
            </div>
            <div id="customerAddressBlock_<?php echo $address->addressID; ?>" class="hiddenBlock">
                <?php
                print '<table>
                    <tr>
                        <td rowspan="8"><img src="./icons/office.png" alt="edit" height="40px"/></td>
                        <td colspan="4"><!--<a href="index.php?portal=mr&page=report&action=showInstallation&customerID=' . $customerID . '&tab=customerAddress&reportID=' . $reportOverviewID . '&edit=edit&addressID=' . $address->addressID . '">Edit</a>--></td>
                    </tr>
                    <tr>
                        <td class="title">Office Name</td>
                        <td>' . $address->officeName . '</td>
                        <td class="title">Country</td>
                        <td>' . $address->country . '</td>
                         
                    </tr>';
                $formattedFullAddress = str_replace(",", "<br/>", $address->fullAddress);
                print '<tr>
                        <td class="title">Address</td>
                        <td rowspan="5" style="vertical-align:top;">' . $formattedFullAddress . '</td>
                        <td class="title">Area</td>
                        <td>' . $countryController->getTimeZoneByCountryName($address->country)->geoArea . '</td>   
                    </tr>
                    <tr>
                        <td></td>
                        <td class="title">Continent</td>
                        <td>' . $countryController->getTimeZoneByCountryName($address->country)->continent . '</td>
                        
                    </tr>
                    <tr>
                        <td></td>
                        <td class="title">Default Address</td>
                        <td>';
                if ($address->defaultAddress === '1') {
                    print "Yes";
                } else {
                    print "No";
                }
                print '</td>
                    </tr>
                        <td></td>
                        <td class="title">Address Verified</td>
                                <td>';
                if ($address->addressVerified === '1') {
                    print "Yes";
                } else {
                    print "No";
                }
                print '</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="title">Address Archived</td>
                                <td>';
                if ($address->archived === '1') {
                    print "Yes";
                } else {
                    print "No";
                }
                print '</td>
                    </tr>
                    <tr>
                    <td class="title">City</td>
                    <td>'.$address->city.'</td>
                    <td></td>
                    <td></td>
                    </tr>
                    <tr>
                    <td></td>
                    <td class="title">State</td>
                    <td>'.$address->state.'</td>
                    <td></td>
                    <td></td>
                    </tr>
                    <tr>
                    <td></td>
                    <td class="title">Region</td>
                    <td>'.$address->region.'</td>
                    <td></td>
                    <td></td>
                    </tr>
                    </table></div>';
            }
        }
        ?>

    </div>