<?php
if (isset($reportOverviewID) && ($reportOverviewID != "")) {
    /*print '<span id="returnLink"><a href="index.php?portal=mr&page=report&action=showReport&reportOverviewID=' . $reportOverviewID . '">Return to Report</a>'
            . ' <img src="./icons/returnArrow.png" alt="" height="15px" />'
            . '</span>';*/
} else {
    $reportOverviewID = "";
}
?>

<?php
require_once 'Helpers/charsetFunctions.php';

if ((isset($tab)) && (!isset($edit)) && (!isset($move))) {
    switch ($tab) {
        case "customer":
            if (isset($save)) {
                
                $oldCustomerRecord = $customerController->getCustomerByID($customerID);
                $oldCustomerStatusArray = $customerStatusController->getAllCustomerStatusValuesByCustomerID($customerID);
                
                $saveCustomer = $customerController->getCustomerByID($customerID);
                $saveCustomer->customerName = addslashes($_POST['customerName']);
                $saveCustomer->mainTelephone = addslashes($_POST['mainTelephone']);
                $saveCustomer->mainFax = addslashes($_POST['mainFax']);
                $saveCustomer->website = addslashes($_POST['website']);
                $saveCustomer->headerNotes = addslashes($_POST['headerNotes']);
                $accManager = null;
                if ($_POST['accountManager'] != "") {
                    $accManager = $_POST['accountManager'];
                }
                $saveCustomer->accountManager = addslashes($accManager);
                
                
                $updateCustomer = $customerController->updateCustomerObject($saveCustomer);
                $customer = $customerController->getCustomerByID($customerID);
                $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "update", "Customer Updated: Customer ID " . $customerID);

                // History DB entry
                $customerHistory = new HistoryCustomer("", $oldCustomerRecord->customerID, $oldCustomerRecord->customerName, $oldCustomerRecord->mainTelephone, $oldCustomerRecord->mainFax, $oldCustomerRecord->website,  $oldCustomerRecord->accountManager, $oldCustomerRecord->tbhID, $oldCustomerRecord->tbhAccountCode, $oldCustomerRecord->ldID, $oldCustomerRecord->ldAccountCode, $oldCustomerRecord->headerNotes, $_SESSION['user_id'], date("Y-m-d H:i:s", time()));
                $setCustomerHistory = $customerHistoryController->setCustomerHistory($customerHistory);
            

                // Set Status
                $customerStatusController->deleteCustomerStatusObjectsByCustomerID($customerID);
                
                foreach ($_POST as $key => $value) {
                    if ( substr($key, 0, -2) == "status") {
                       
                        $saveStatus = new CustomerStatus("", $customerID, $value);
                        $newStatus = $customerStatusController->setNewCustomerStatusObject($saveStatus);
                    }
                }
                    
                
                // Set Status History
                foreach ($oldCustomerStatusArray as $key => $statusObject) {
                    $oldStatus = new HistoryCustomerStatus(
                            "", 
                            $statusObject->custStatusID, 
                            $statusObject->customerID, 
                            $statusObject->custStatusLookupID, 
                            $_SESSION['user_id'], 
                            date("Y-m-d H:i:s", time()));
                    
                    $setCustomerStatusHistory = $customerStatusHistoryController->setCustomerStatusHistory($oldStatus);
                }
                
            }
            include("Views/Customers/installationLayout.php");
            
            break;
        case "customerAddress":
            if (isset($save) && ($_GET['addressID'] != "")) {
                $customerID = $_GET['customerID'];
                
                
                $oldAddressRecord = $customerAddressController->getCustomerAddressByID($_GET['addressID']);
               
                $saveAddress = $customerAddressController->getCustomerAddressByID($_GET['addressID']);
               
                if ($saveAddress->addressVerified == '0') {
                    if (isset($_POST['addressVerified'])) {
                        $newVerification = 1;
                    }
                }
                $saveAddress->officeName = $_POST['officeName'];
                $saveAddress->address = $_POST['address'];
                $saveAddress->city = $_POST['city'];
                $saveAddress->state = $_POST['state'];
                $saveAddress->country = $_POST['country'];
                $saveAddress->postcode = $_POST['postcode'];
                $saveAddress->addressVerified = 0;
                if (isset($_POST['addressVerified'])) {
                    $saveAddress->addressVerified = $_POST['addressVerified'];
                }
                $saveAddress->defaultAddress = 0;
                if (isset($_POST['defaultAddress'])) {
                    $saveAddress->defaultAddress = $_POST['defaultAddress'];
                }
                $saveAddress->archived = 0;
                $update = "Updated";
                if (isset($_POST['archived'])) {
                    $saveAddress->archived = $_POST['archived'];
                    $update = "Archived";
                }

                $updateCustomerAddress = $customerAddressController->updateCustomerAddress($saveAddress);
                $customerAddresses = $customerAddressController->getCustomerAddressesByCustomerID($customerID);
                // Log in the DB


                $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "update", "Customer Address $update: Address ID " . $_GET['addressID']);
                if (isset($newVerification)) {
                    $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "update", "Customer Address Verified: Address ID " . $_GET['addressID']);
                }
                // History DB entry
                $addressHistory = new HistoryCustomerAddress("", $oldAddressRecord->addressID, $oldAddressRecord->officeName, $oldAddressRecord->address, $oldAddressRecord->city, $oldAddressRecord->state, $oldAddressRecord->country, $oldAddressRecord->postcode, $oldAddressRecord->addressVerified, $oldAddressRecord->customerID, $oldAddressRecord->archived, $oldAddressRecord->defaultAddress, $oldAddressRecord->fullAddress, $_SESSION['user_id'], date("Y-m-d H:i:s", time()));

                $setCustomerAddressHistory = $customerAddressHistoryController->setCustomerAddressHistory($addressHistory);
            } else if (isset($save) && ($_GET['addressID'] == "")) {


                $addressVerified = 0;
                if (isset($_POST['addressVerified'])) {
                    $addressVerified = $_POST['addressVerified'];
                }
                $defaultAddress = 0;
                if (isset($_POST['defaultAddress'])) {
                    $defaultAddress = $_POST['defaultAddress'];
                }
                $archived = 0;
                if (isset($_POST['archived'])) {
                    $archived = $_POST['archived'];
                }

                $saveAddress = new CustomerAddress(
                        '', addslashes($_POST['officeName']), addslashes($_POST['address']), addslashes($_POST['city']), addslashes($_POST['state']), addslashes($_POST['country']), addslashes($_POST['postcode']), $addressVerified, $_GET['customerID'], $archived, $defaultAddress, "");


                $setCustomerAddress = $customerAddressController->setCustomerAddress($saveAddress);
                $customerAddresses = $customerAddressController->getCustomerAddressesByCustomerID($customerID);
                // Log in the DB
                $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "create", "Customer Address Added: Address ID " . $setCustomerAddress);
            }

            include("Views/Customers/installationLayout.php");
            break;
        case "installation":
            if (isset($installationsArray) && (count($installationsArray) > 0)) {
                if (isset($_GET['installationID']) && ($_GET['installationID'] != "")) {
                    $installationID = $_GET['installationID'];
                    $installation = $installationController->getInstallationByInstallationID($_GET['installationID']);
                } else {
                    $installationID = key($installationsArray);
                    $installation = $installationsArray[key($installationsArray)];
                }
                if (isset($save) && ($save === "save")) {
                    $oldInstallationRecord = $installationController->getInstallationByInstallationID($installationID);
                    $saveInstallation = $installationController->getInstallationByInstallationID($installationID);
                    $saveInstallation->type = addslashes($_POST['type']);
                    $saveInstallation->installationName = addslashes($_POST['installationName']);
                    $saveInstallation->installation = addslashes($_POST['installation']);
                    $saveInstallation->imo = addslashes($_POST['imo']);
                    $saveInstallation->originalSource = addslashes($_POST['source']);
                    $saveInstallation->originalSourceDate = addslashes($_POST['sourceDate']."-01-01 00:00:00");
                    $saveInstallation->status = addslashes($_POST['status']);
                    $saveInstallation->operatingCompany = addslashes($_POST['operatingCompany']);
                    $saveInstallation->technicalManager = addslashes($_POST['technicalManager']);
                    //marine
                    $saveInstallation->hullNumber = addslashes($_POST['hullNumber']);
                    $saveInstallation->hullType = addslashes($_POST['hullType']);
                    $saveInstallation->yardBuilt = addslashes($_POST['yardBuilt']);
                    $saveInstallation->deadWeight = addslashes($_POST['deadweight']);
                    $saveInstallation->lengthOverall = addslashes($_POST['lengthOverall']);
                    $saveInstallation->beamExtreme = addslashes($_POST['beamExtreme']);
                    $saveInstallation->beamMoulded = addslashes($_POST['beamMoulded']);
                    $saveInstallation->builtDate = addslashes($_POST['builtDate'])."-01-01 00:00:00";
                    $saveInstallation->draught = addslashes($_POST['draught']);
                    $saveInstallation->grossTonnage = addslashes($_POST['grossTonnage']);
                    $saveInstallation->length = addslashes($_POST['length']);
                    $saveInstallation->propellerType = addslashes($_POST['propellerType']);
                    $saveInstallation->propulsionUnitCount = addslashes($_POST['propulsionUnitCount']);
                    $saveInstallation->shipBuilder = addslashes($_POST['shipBuilder']);
                    $saveInstallation->teu = addslashes($_POST['teu']);
                    $saveInstallation->buildYear = addslashes($_POST['buildYear'])."-01-01 00:00:00";
                    
                    
                    if (isset($_POST['archiveFlag'])) {
                        $saveInstallation->archiveFlag = $_POST['archiveFlag'];
                        $saveInstallation->archiveReason = addslashes($_POST['archiveReason']);
                    }


                    $updateInstallation = $installationController->updateInstallationObject($saveInstallation);

                    $installation = $installationController->getInstallationByInstallationID($installationID);

                    // Log in the DB
                    $userHistoryController->setUserHistory(
                            $_SESSION['user_id'], date("Y-m-d H:i:s", time()), "update", "Installation updated: ID " . $installationID);

                    // History DB entry
                    $installationHistory = new HistoryInstallation("", 
                            $oldInstallationRecord->installationID, 
                            $oldInstallationRecord->type, 
                            $oldInstallationRecord->installation, 
                            $oldInstallationRecord->installationName, 
                            $oldInstallationRecord->imo, 
                            $oldInstallationRecord->status, 
                            $oldInstallationRecord->operatingCompany, 
                            $oldInstallationRecord->technicalManager, 
                            $oldInstallationRecord->originalSource,
                            $oldInstallationRecord->originalSourceDate,
                            $oldInstallationRecord->photoLink,
                            $oldInstallationRecord->hullNumber,
                            $oldInstallationRecord->hullType,
                            $oldInstallationRecord->yardBuilt,
                            $oldInstallationRecord->deadWeight,
                            $oldInstallationRecord->lengthOverall,
                            $oldInstallationRecord->beamExtreme,
                            $oldInstallationRecord->beamMoulded,
                            $oldInstallationRecord->builtDate,
                            $oldInstallationRecord->draught,
                            $oldInstallationRecord->grossTonnage,
                            $oldInstallationRecord->length,
                            $oldInstallationRecord->propellerType,
                            $oldInstallationRecord->propulsionUnitCount,
                            $oldInstallationRecord->shipBuilder,
                            $oldInstallationRecord->teu,
                            $oldInstallationRecord->buildYear,
                            $oldInstallationRecord->archiveFlag,
                            $oldInstallationRecord->archiveReason,
                            $oldInstallationRecord->customerAddressID,
                            $_SESSION['user_id'], 
                            date("Y-m-d H:i:s", time()));

                    $setInstallationHistory = $installationHistoryController->setInstallationHistory($installationHistory);
                
                    /* If the installation name has changed then the behaviour is that the UNIT NAME of all the 
                    associated engines should also change*/
                    
                    if ($_POST['installationName'] != $oldInstallationRecord->installationName) {
                       // $productsList;
                    }
                    
                }
                if (isset($save) && ($save === "saveTechnical")) {
                    
                    // changes to the product and engine data need to be saved.

                    if ($_GET['productID'] == "") {
                        $saveProduct = new Product("", "", "", "", "", "", $customerID, $installationID, "", "", "");
                        $saveEngine = new InstallationEngine("", "", "", "", "", "", "", "", "MW", "", "", "", "", "0", "3", "", "");
                    } else {
                        /*
                         * Get the original engine and unit details.
                         */
                        $productID = $_GET['productID'];
                        $oldProductRecord = $productController->getProductByID($productID);
                        $saveProduct = $productController->getProductByID($productID);
                        $oldEngineRecord = $installationEngineController->getEngineByProductID($productID);
                        $saveEngine = $installationEngineController->getEngineByProductID($productID);
                      //  print_r($saveEngine);
                    }
                    /*
                     * Update the posted details
                     */
                    // product
                    //print_r($_POST);
                    $versionLookupID = "";
                    if (!isset($_POST['versionID']) || ($_POST['versionID'] == "")) {
                        $versionLookupID = reset($versionLookupController->getVersionIDBySeriesID($_POST['series']))->versionID;
                        //print "NO VERSION ID";
                    } else {
                        $versionLookupID = $_POST['versionID'];
                        //print "VERSION ID";
                    }
                    
                    $saveProduct->versionLookupID = $versionLookupID;
                    $saveProduct->productSerialNumber = addslashes($_POST['serialNumber']);
                    $saveProduct->productComment = addslashes($_POST['comment']);
                    $saveProduct->productDrawingNumber = addslashes($_POST['drawingNumber']);
                    $saveProduct->productDescription = addslashes($_POST['description']);
        
                    // engine
                    if ($_POST['productType'] == '1') {
                        $saveEngine->mainOrAux = $_POST['mainOrAux'];
                        $verified = 0;
                        if (isset($_POST['engineVerified'])) {
                            $verified = 1;
                        }
                        $saveEngine->engineVerified = $verified;
                        $saveEngine->output = addslashes($_POST['output']);
                        $saveEngine->fuelType = addslashes($_POST['fuelType']);
                        $saveEngine->productID = addslashes($saveEngine->productID);
                        $saveEngine->modelDescription = addslashes($_POST['engineModel']);
                        

                        $saveEngine->boreSize = addslashes($_POST['boreSize']);
                        $saveEngine->stroke = addslashes($_POST['stroke']);
                        $saveEngine->releaseDate = addslashes($_POST['releaseDate']."");
                        $loadPriority = '3';
                        if (isset($_POST['loadPriority'])) {
                            $loadPriority = $_POST['loadPriority'];
                        }
                        $saveEngine->loadPriorityID = addslashes($loadPriority);

                    } else {
                        $saveEngine = new InstallationEngine("", "",  "", "", "", "", "MW", "", "", "", "", "0", "3", "", "");
                    }
                   // print_r($saveEngine);
                    /*
                     * Save the updated information
                     */
                    if ($_GET['productID'] == "") {
                        $addProduct = $productController->setNewProduct($saveProduct);
                        if (isset($addProduct)) {
                            $saveEngine->productID = $addProduct;
                            $addEngine = $installationEngineController->setNewEngine($saveEngine);
                            if ($addEngine == true) {
                                $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "update", "Installation Product Created: ID " . $addProduct);
                                $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "update", "Installation Engine (Product Component ". $addProduct .") Created: ID " . $addEngine);
                            }
                        }
                    } else {
                        $updateProductObject = $productController->updateProductObject($saveProduct);
                        
                        
                        if ($saveEngine->installationEngineID == '') {
                            // the engine wasnt created properly the first time around and needs recreating.
                            $saveEngine->productID = $saveProduct->productID;
                            $addEngine = $installationEngineController->setNewEngine($saveEngine);
                        } else {
                            $updateEngineObject = $installationEngineController->updateInstallationEngineObject($saveEngine);
                        }
                        $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "update", "Installation Product Details Updated: ID " . $saveProduct->productID);
                        $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "update", "Installation Engine Details Updated: ID " . $saveEngine->installationEngineID);
                    
                        $productHistory = new HistoryProduct("", 
                                $oldProductRecord->productID, 
                                $oldProductRecord->productSerialNumber, 
                                $oldProductRecord->productDescription, 
                                $oldProductRecord->productDrawingNumber, 
                                $oldProductRecord->productComment, 
                                $oldProductRecord->productEnquiryOnlyFlag, 
                                $oldProductRecord->customerID, 
                                $oldProductRecord->installationID, 
                                $oldProductRecord->versionLookupID, 
                                $oldProductRecord->productUnitName,
                                $oldProductRecord->sourceAliasID,
                                $_SESSION['user_id'], 
                                date("Y-m-d H:i:s", time()));
                        
                        $engineHistory = new HistoryInstallationEngine(
                                "", 
                                $oldEngineRecord->installationEngineID, 
                                $oldEngineRecord->modelDescription, 
                                $oldEngineRecord->mainOrAux, 
                                $oldEngineRecord->engineVerified, 
                                $oldEngineRecord->output, 
                                $oldEngineRecord->outputUnitOfMeasurement, 
                                $oldEngineRecord->fuelType, 
                                $oldEngineRecord->unitName,
                                $oldEngineRecord->boreSize, 
                                $oldEngineRecord->stroke, 
                                $oldEngineRecord->releaseDate,
                                $oldEngineRecord->enquiryOnly,
                                $oldEngineRecord->loadPriorityID,
                                $oldEngineRecord->productID,
                                $oldEngineRecord->positionOfMain,
                                $oldEngineRecord->engineBuilder,
                                $_SESSION['user_id'], 
                                date("Y-m-d H:i:s", time()));
                        
                        $setInstallationEngineHistory = $installationEngineHistoryController->setInstallationEngineHistory($engineHistory);
                        $setProductHistory = $productHistoryController->setProductHistory($productHistory);
                    }
                   
                }
                if (isset($save) && ($save === "move")) {
                   
                    // change the record in the customer_tbl_has_installations_tbl.
                    $oldCustomerHasInstallationsRecord = $customerHasInstallationsController->getCustomerIDFromInstallationID(addslashes($_GET['installationID']));
                    $newCustomerHasInstallationRecord = $customerHasInstallationsController->getCustomerIDFromInstallationID(addslashes($_GET['installationID']));
                    //print_r($oldCustomerHasInstallationsRecord);
                    
                    $newCustomerHasInstallationRecord->customerID = $_POST['newCustomer'];
                    $customerHasInstallationsController->updateCustomerHasInstallationsRecord($oldCustomerHasInstallationsRecord->customerID, $oldCustomerHasInstallationsRecord->installationID, $newCustomerHasInstallationRecord);
                    
                    // show as a change in the history table.
                    $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "update", "Installation has been moved between customers. Installation ID:" . $_GET['installationID'] . " Old Customer ID: ".$oldCustomerHasInstallationsRecord->customerID . " New Customer ID: ".$_POST['newCustomer']);
                    $customerHasInstallationHistory = new HistoryCustomerHasInstallations("", 
                            $oldCustomerHasInstallationsRecord->customerID, 
                            $oldCustomerHasInstallationsRecord->installationID, 
                            $_SESSION['user_id'], 
                            date("Y-m-d H:i:s", time()));
                    // make an entry in the customer has inst history table.
                    $customerHasInstallationsHistoryController->setCustomerHasInstallationsHistory($customerHasInstallationHistory);
                    
                    
                }
                if (isset($save) && ($save === "moveTechnical")) {
                   // change the record in the product tbl to show the new installation ID.
                    $oldProductData = $productController->getProductByID($_GET['productID']);
                    $newProductData = $productController->getProductByID($_GET['productID']);
        
                    $newProductData->installationID = $_POST['installationID'];
                    
                    $saveProduct = $productController->updateProductObject($newProductData);
                    
                    $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "update", "Product has been moved between installations. Product ID:" . $_GET['productID'] . " Old Installation ID: ".$oldProductData->installationID . " New Installation ID: ".$_POST['installationID']);
                    $productHistory = new HistoryProduct("", 
                            $oldProductData->productID, 
                            $oldProductData->productSerialNumber, 
                            $oldProductData->productDescription, 
                            $oldProductData->productDrawingNumber, 
                            $oldProductData->productComment, 
                            $oldProductData->productEnquiryOnlyFlag, 
                            $oldProductData->customerID, 
                            $oldProductData->installationID, 
                            $oldProductData->versionLookupID, 
                            $oldProductData->productUnitName,
                            $_SESSION['user_id'], 
                            date("Y-m-d H:i:s", time()));
                    $productHistoryController->setProductHistory($productHistory);
                    
                    
                }

                
            }
           // $installationsArray = $installationController->getAllInstallationsByCustomer($customerID);
            include("Views/Customers/installationLayout.php");
            break;
        case "contacts":


            $inactive = 0;
            if (isset($_POST['inactive'])) {
                $inactive = 1;
            }
            $contactValidated = 0;

            if (isset($_POST['contactValidated'])) {
                $contactValidated = 1;
            }

            $customerAddressID = "";
            if (isset($_POST['customerAddressID'])) {
                $customerAddressID = $_POST['customerAddressID'];
            }

            if (isset($save) && ($_POST['contactID'] != "")) {

                $oldContactRecord = $customerContactsController->getContactByID($_POST['contactID']);
                $saveContact = new CustomerContact(
                        $_POST['contactID'], addslashes($_POST['salutation']), addslashes($_POST['firstName']), addslashes($_POST['surname']), addslashes($_POST['jobTitle']), addslashes($_POST['email']), addslashes($_POST['number']), addslashes($_POST['mobile']), addslashes($_POST['infoSource']), $inactive, $contactValidated, $customerID, $customerAddressID);



                $updateObject = $customerContactsController->updateContactDetails($saveContact);

                // Log in the DB
                $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "update", "Customer Contact updated: ID " . $_POST['contactID']);
            
                
                $contactHistory = new HistoryCustomerContact("", 
                        $oldContactRecord->customerContactID, 
                        $oldContactRecord->salutation,
                        $oldContactRecord->firstName,
                        $oldContactRecord->surname, 
                        $oldContactRecord->jobTitle, 
                        $oldContactRecord->email, 
                        $oldContactRecord->number, 
                        $oldContactRecord->mobile, 
                        $oldContactRecord->infoSource, 
                        $oldContactRecord->inactive, 
                        $oldContactRecord->contactValidated, 
                        $oldContactRecord->customerID, 
                        $oldContactRecord->customerAddressID, 
                        $_SESSION['user_id'], 
                        date("Y-m-d H:i:s", time()));
                        
                        $setCustomerContactHistory = $customerContactHistoryController->setCustomerContactHistory($contactHistory);
                
                
            } else if (isset($save) && ($_POST['contactID'] == "")) {
                $saveContact = new CustomerContact(
                        $dummy = "", addslashes($_POST['salutation']), addslashes($_POST['firstName']), addslashes($_POST['surname']), addslashes($_POST['jobTitle']), addslashes($_POST['email']), addslashes($_POST['number']), addslashes($_POST['mobile']), addslashes($_POST['infoSource']), $inactive, $contactValidated, $customerID, $customerAddressID);

                $setObject = $customerContactsController->setContact($saveContact);

                // Log in the DB
                $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "create", "Customer Contact Created: ID " . $setObject);
            }



            include("Views/Customers/installationLayout.php");
            break;

        case "conversation":

            include("Views/Customers/instConversations.php");
            break;
        default:
            include("Views/Customers/installationLayout.php");
           

            break;
    }
} else if ((isset($tab)) && (isset($edit)) && (!isset($move))) {
    switch ($tab) {
        case "customer":
            include("Views/Customers/editCustomerProfile.php");
            break;
        case "customerAddress":
            include("Views/Customers/editCustomerAddress.php");
            break;
        case "installation":
            $installation = $installationController->getInstallationByInstallationID($_GET['installationID']);
            include("Views/Installations/instEditInstallation.php");
            break;
        case "contacts":
            include("Views/Customers/instEditContact.php");
            break;
        case "technical":
            include("Views/Installations/instEditTechnicalData.php");
            break;
        case "conversation":
            include("Views/Customers/instEditConversations.php");
            break;
        default:
            include("Views/Customers/customerProfile.php");
            break;
    }
} else if ((isset($tab)) && (!isset($edit)) && (isset($move))) {
    switch ($tab) {
        case "customer":
            include("Views/Customers/editCustomerProfile.php");
            break;
        case "customerAddress":
            include("Views/Customers/editCustomerAddress.php");
            break;
        case "installation":
            $installation = $installationController->getInstallationByInstallationID($_GET['installationID']);
            include("Views/Installations/instMoveInstallation.php");
            break;
        case "contacts":
            include("Views/Customers/instEditContact.php");
            break;
        case "technical":
            include("Views/Installations/instMoveProduct.php");
            break;
        case "conversation":
            include("Views/Customers/instEditConversations.php");
            break;
        default:
            include("Views/Customers/customerProfile.php");
            break;
    }
}


            