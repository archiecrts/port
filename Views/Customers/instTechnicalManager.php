<script>
    $(function(){
        $('#technicalTbl').tablesorter(); 
   }); 
   
    $(function () {
        //var scrolled = 0;
        $(".selectorDropDownBarTech").click(function () {
            
                if ($('#technicalTbl').css('display') == 'none')
                {
                    $('#technicalTbl').removeClass('hiddenBlock').addClass('showTable');
                } else {
                    $('#technicalTbl').removeClass('showTable').addClass('hiddenBlock');
                }

            
        });
    });
 </script>
 
<style>
     /* SORTER */
#technicalTbl thead tr th.headerSortUp span { 
        /* background: #1D6191;*/
         background-image: url('./icons/arrow-up.png');
 
}

#technicalTbl thead tr th.headerSortDown span {
   /* background: #9eafec;*/
  background-image: url('./icons/arrow-down.png');
  
}

/*#combinedFleetTbl thead tr th.headerSortUp{
  //background: #9eafec;
}
#combinedFleetTbl thead tr th.headerSortDown {
  //background: #1D6191;
}*/

#technicalTbl thead {
  cursor: pointer;
  background: #c9dff0;
}
</style>
 
 <div id="installationContainerTechnicalManager" style="padding-top: 110px; margin-top: -110px;">
    <div id="installationHeader">
        
        <div id="officeLocationsHeader" class="selectorDropDownBarTech">
            <img src="./icons/crm_grey.png" alt="" height='20px'/> Technical Manager for: [Ship Count <?php echo sizeof($technicalManagerList); ?>]
             </div>
    </div>

<table style="width:100%;" id='technicalTbl'  class='hiddenBlock'>
    <thead>
    <tr>
        <th class='headerSortDown'>IMO <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>
        <th>Installation <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>
        <th>Type <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>
        <th>Location Type <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>
        <th>Country <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>
        <th>[Ship] Manager <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>
    </tr>
    </thead><tbody>
<?php
if (isset($technicalManagerList)) {
    $countForRow = 1;
    foreach ($technicalManagerList as $k => $inst) {
        $rowCol = "";
        if (($countForRow % 2) == 0) {
            $rowCol = "style='background-color:lightsteelblue;'";
        }
        print "<tr $rowCol>";
        print "<td>$inst->imo</td>";
        print "<td><a href='index.php?portal=mr&page=report&action=showInstallation&customerID=" . $customerID . "&installationID=" . $inst->installationID . "&tab=installation&reportID=" . $reportOverviewID . "' target='_blank''>$inst->installationName</a></td>";
        print "<td>$inst->installation</td>";
        print "<td>$inst->type</td>";
        $customerAddress = $customerAddressController->getCustomerAddressByID($inst->customerAddressID);
        $customer = $customerController->getCustomerByID($customerAddress->customerID);
        print "<td>$customerAddress->country</td>";
        print "<td><a href='index.php?portal=mr&page=report&action=showCustomer&installationID=&customerID=$customerAddress->customerID&submit=Go' target='_blank'>$customer->customerName</a></td>";
        print "</tr>";
        $countForRow++;
    }
} else {
    print "<tr><td colspan='5'>Nothing to display</td></tr>";
}
?>
    </tbody>
</table>
</div>