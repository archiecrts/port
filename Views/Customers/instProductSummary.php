<script>
    
    $(function(){
        $('#productSummaryTbl').tablesorter(); 
   });
   
   
    $(function () {
        //var scrolled = 0;
        $(".selectorDropDownBarProd").click(function () {
            
                if ($('#productSummaryTbl').css('display') == 'none')
                {
                    $('#productSummaryTbl').removeClass('hiddenBlock').addClass('showTable');
                } else {
                    $('#productSummaryTbl').removeClass('showTable').addClass('hiddenBlock');
                }

            
        });
    });
 </script>
 <style>
      /* SORTER */
#productSummaryTbl thead tr th.headerSortUp span { 
        /* background: #1D6191;*/
         background-image: url('./icons/arrow-up.png');
 
}

#productSummaryTbl thead tr th.headerSortDown span {
   /* background: #9eafec;*/
  background-image: url('./icons/arrow-down.png');
  
}

/*#productSummaryTbl thead tr th.headerSortUp{
  //background: #9eafec;
}
#productSummaryTbl thead tr th.headerSortDown {
  //background: #1D6191;
}*/

#productSummaryTbl thead {
  cursor: pointer;
  background: #c9dff0;
}
 </style>

<div id="installationContainerProductSummary" style="padding-top: 110px; margin-top: -110px;">
    <div id="installationHeader">
        <div id="officeLocationsHeader" class="selectorDropDownBarProd">
            <img src="./icons/technicalDataIcon.png" alt="" height='20px'/> Product Summary For Managed Fleet: [Product Count <?php echo ($productSummaryList > 0 ?  sizeof($productSummaryList) : "0");  ?>]
        </div>
    </div>
    <table style="width:100%;" id='productSummaryTbl'  class='hiddenBlock'>
        <thead>
        <tr>
            <th class='headerSortDown'>Product Type <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>
            <th>Designer / Manufacturer <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>
            <th>Series <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>
            <th>Model <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>
            <th>Version Description <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>
            <th>Total Cylinder Count <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>
            <th>Total Products <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>
        </tr>
        </thead><tbody>
        <?php
        if (isset($productSummaryList)) {
            $countForRow = 1;
            foreach ($productSummaryList as $psKey => $psObject) {
                $rowCol = "";
                if (($countForRow % 2) == 0) {
                    $rowCol = "style='background-color:lightsteelblue;'";
                }
                print "<tr $rowCol>
                    <td>" . $typeOfProductLookupController->getTypeOfProductByID($seriesLookupController->getSeriesByID($psObject->seriesID)->typeOfProductLookupID)->toplProductDescription . "</td>
                    <td>" . $designerLookupController->getDesignerByVersionLookupID($psObject->versionID)->designerDescription . "</td>
                    <td>" . $seriesLookupController->getSeriesByID($psObject->seriesID)->seriesDescription . " </td>
                    <td>" . $psObject->modelDescription . "</td>
                    <td>" . $psObject->versionDescription . "</td>
                    <td>" . $psObject->sumOfCylinders . "</td>
                    <td>" . $psObject->countOfProducts . "</td>
                </tr>";
                $countForRow++;
            }
        }
        ?>
        </tbody>
    </table>
</div>