<script>
    
    $(function(){
        $('#ownerParentTbl').tablesorter(); 
   });
   
    $(function () {
        //var scrolled = 0;
        $(".selectorDropDownBarOwner").click(function () {
            
                if ($('#ownerParentTbl').css('display') == 'none')
                {
                    $('#ownerParentTbl').removeClass('hiddenBlock').addClass('showTable');
                } else {
                    $('#ownerParentTbl').removeClass('showTable').addClass('hiddenBlock');
                }

            
        });
    });
 </script>
  <style>
     /* SORTER */
#ownerParentTbl thead tr th.headerSortUp span { 
        /* background: #1D6191;*/
         background-image: url('./icons/arrow-up.png');
 
}

#ownerParentTbl thead tr th.headerSortDown span {
   /* background: #9eafec;*/
  background-image: url('./icons/arrow-down.png');
  
}

/*#ownerParentTbl thead tr th.headerSortUp{
  //background: #9eafec;
}
#ownerParentTbl thead tr th.headerSortDown {
  //background: #1D6191;
}*/

#ownerParentTbl thead {
  cursor: pointer;
  background: #c9dff0;
}
     </style>
 <div id="installationContainerOwnerParent" style="padding-top: 110px; margin-top: -110px;">
    <div id="installationHeader">

        <div id="officeLocationsHeader" class="selectorDropDownBarOwner">
            <img src="./icons/crm_grey.png" alt="" height='20px'/> Owner / Parent of Installations: [Ship Count <?php echo ($ownerParentList > 0 ?  sizeof($ownerParentList) : "0"); ?>]
        </div>
    </div>

    <table style="width:100%;" id='ownerParentTbl'  class='hiddenBlock'>
        <thead>
        <tr>
            <th class='headerSortDown'>IMO <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>
            <th>Installation <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>
            <th>Type <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>
            <th>Location Type <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>
            <th>Country <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>
            <th>[Ship] Manager <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>
        </tr>
        </thead></tbody>
        <?php
        if (isset($ownerParentList)) {
            $countForRow = 1;
            foreach ($ownerParentList as $k => $inst) {
                $rowCol = "";
                if (($countForRow % 2) == 0) {
                    $rowCol = "style='background-color:lightsteelblue;'";
                }
                print "<tr $rowCol>";
                $customerAddress = $customerAddressController->getCustomerAddressByID($inst->customerAddressID);
                $customer = $customerController->getCustomerByID($customerAddress->customerID);
                print "<td>$inst->imo</td>";
                print "<td><a href='index.php?portal=mr&page=report&action=showInstallation&customerID=$customerAddress->customerID&installationID=$inst->installationID&tab=installation' target='_blank'>$inst->installationName</a></td>";
                print "<td>$inst->installation</td>";
                print "<td>$inst->type</td>";
                print "<td>$customerAddress->country</td>";
                print "<td><a href='index.php?portal=mr&page=report&action=showCustomer&customerID=" . $customerAddress->customerID . "&reportID=' target='_blank'>$customer->customerName</a></td>";
                print "</tr>";
                $countForRow++;
            }
        } else {
            print "<tr><td colspan='5'>Nothing to display</td></tr>";
        }
        ?>
    </tbody>
    </table>
</div>