<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
<script>
    $(function(){
     $('#combinedFleetTbl').tablesorter(); 
   }); 
   
   $(function () {
        //var scrolled = 0;
        $(".selectorDropDownBar").click(function () {
            
                if ($('#combinedFleetTbl').css('display') == 'none')
                {
                    $('#combinedFleetTbl').removeClass('hiddenBlock').addClass('showTable');
                } else {
                    $('#combinedFleetTbl').removeClass('showTable').addClass('hiddenBlock');
                }

            
        });
    });
 </script>
 <style>
    /* Tooltip container */
    .tooltip {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black; /* If you want dots under the hoverable text */
    }

    /* Tooltip text */
    .tooltip .tooltiptext {
        visibility: hidden;
        width: 120px;
        background-color: black;
        color: #fff;
        text-align: center;
        padding: 5px 0;
        border-radius: 6px;

        /* Position the tooltip text - see examples below! */
        position: absolute;
        z-index: 1;
    }

    /* Show the tooltip text when you mouse over the tooltip container */
    .tooltip:hover .tooltiptext {
        visibility: visible;
    }

    .countryFlag {
        margin-right:10px;
        float:right;
        position: relative;
    }

    .countryFlag img {
        margin-top: -12px;
        margin-left: 10px;
        vertical-align: top;
        height:40px;
    }
    
    /* SORTER */
#combinedFleetTbl thead tr th.headerSortUp span { 
        /* background: #1D6191;*/
         background-image: url('./icons/arrow-up.png');
 
}

#combinedFleetTbl thead tr th.headerSortDown span {
   /* background: #9eafec;*/
  background-image: url('./icons/arrow-down.png');
  
}

/*#combinedFleetTbl thead tr th.headerSortUp{
  //background: #9eafec;
}
#combinedFleetTbl thead tr th.headerSortDown {
  //background: #1D6191;
}*/

#combinedFleetTbl thead {
  cursor: pointer;
  background: #c9dff0;
}

</style>
<div id='installationContainerCombinedFleet'>
    <div id="installationHeader">
        <div id="officeLocationsHeader" class="selectorDropDownBar">
            <img src="./icons/gear37.png" alt="" height='20px'/> Combined Fleet:  [Ship Count <?php echo ($combinedFleet > 0 ?  sizeof($combinedFleet) : "0"); ?>]
        </div>
    </div>


    <?php
    if (isset($combinedFleet)) {
        print "<table  style='width:100%;' id='combinedFleetTbl' class='hiddenBlock'>";
        print "<thead>";
        print "<tr>";
        print "<th class='headerSortDown'>IMO <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>"; // the span and spaces make enough space for the down and up arrows
        print "<th>Installation <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>";
        print "<th>Type <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>";
        print "<th>Main/Aux <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>";
        print "<th>Eng. Designer <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>";
        print "<th>Eng. Model <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>";
        print "<th>Eng. # <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>";
        print "<th>Status <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>";
        print "<th>Relationship <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>";
        print "</tr>";
        print "</thead><tbody>";
        $countForIMO = 1;
        foreach ($combinedFleet as $key => $installationObject) {

            $productList = $productController->summaryOfEnginesByInstallationID($installationObject->installationID);
            $rowCol = "";
            if (($countForIMO % 2) == 0) {
                $rowCol = "style='background-color:lightsteelblue;'";
            }

            if (isset($productList)) {
                foreach ($productList as $key => $product) {

                    print "<tr $rowCol>";
                    print "<td>" . $installationObject->imo . "</td>";
                    print "<td><a href='index.php?portal=mr&page=report&action=showInstallation&customerID=" . $customerID . "&installationID=" . $installationObject->installationID . "&tab=installation&reportID=" . $reportOverviewID . "' target='_blank''>" . $installationObject->installationName . "</a></td>";
                    print "<td>" . $installationObject->shipTypeLevel4 . "</td>";
                    print "<td>" . $product['iet_main_aux'] . "</td>";
                    print "<td>" . $designerLookupController->getDesignerByVersionLookupID($product['version_lookup_tbl_vlt_id'])->designerDescription . "</td>";
                    print "<td>" . $product['iet_model_description'] . "</td>";
                    print "<td>" . $product['count(p.prod_id)'] . "</td>";
                    print "<td>" . $installationStatusController->getInstallationStatusByID($installationObject->status)->statusDescription . "</td>";
                    $relationshipString = "";
                    $shipManagerID = $customerAddressController->getCustomerAddressByID($installationObject->customerAddressID)->customerID;
                    if (($shipManagerID == $customerID)) {
                        $relationshipString .= "SM ";
                    }
                    if (($installationObject->technicalManager != "") && ($installationObject->technicalManager == $customerID)) {
                        $relationshipString .= "TM ";
                    }
                    if (($installationObject->groupOwner != "") && ($installationObject->groupOwner == $customerID)) {
                        $relationshipString .= "GP ";
                    }
                    print "<td>$relationshipString</td>";
                }
            }
            $countForIMO++;
            print "</tr>";
        }
        print "</tbody></table>";
    } else {
        print "<table>";
        print "<tr>";
        print "<td colspan='10'>No installations to show</td>";
        print "</tr>";
        print "</table>";
    }
    ?>

</div>