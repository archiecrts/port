<div id="installationContainerFleetCount" style="padding-top: 110px; margin-top: -110px;">
    <div id="installationHeader">

        <div id="officeLocationsHeader"><img src="./icons/technicalDataIcon.png" alt="" height='20px'/> Fleet Counts:
        </div>
    </div>

    <table style="width:100%;">

        <tr>
            <th>Registered Owner</th>
            <th>Ship Manager</th>
            <th>Operator</th>
            <th>Group Owner</th>
            <th>DOC Count</th>
            <th>Fleet Size</th>
            <th>In Service Count</th>
        </tr>

        <?php
        if (isset($customerFleetCountsList)) {
            $countForRow = 1;
            foreach ($customerFleetCountsList as $k => $fc) {
                $rowCol = "";
                if (($countForRow % 2) == 0) {
                    $rowCol = "style='background-color:#dcffcc;'";
                }
                print "<tr $rowCol>";
                print "<td>$fc->fleetCountRegisteredOwnerCount</td>";
                print "<td>$fc->fleetCountShipManagerCount</td>";
                print "<td>$fc->fleetCountOperatorCount</td>";
                print "<td>$fc->fleetCountGroupOwnerCount</td>";
                print "<td>$fc->fleetCountDocCount</td>";
                print "<td>$fc->fleetCountFleetSize</td>";
                print "<td>$fc->fleetCountInServiceCount</td>";
                print "</tr>";
                $countForRow++;
            }
        } else {
            print "<tr>";
            print "<td colspan='4'>Nothing to display</td>";
            print "</tr>";
        }
        ?>
    </table>
</div>