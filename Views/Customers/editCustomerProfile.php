<script src="./js/jquery.are-you-sure.js"></script>
<script src="./js/ays-beforeunload-shim.js"></script>

<script>
    $(function () {

        $('#customerAddressForm').areYouSure();

    });
</script>
<?php

$customer = $customerController->getCustomerByID($_GET['customerID']);


?>
<div id='installationContainer'>
    <div id="installationHeader">
        <img src="./icons/PhoneIcon50x50.png" alt="" height='40px'/>
        <h3>Customer Profile</h3>
    </div>
    <div id='installationSubHeader'>

    </div>

    <div id='installationContentWide'> 
        <form name="customerAddressForm" id="customerAddressForm" action="index.php?portal=mr&page=report&action=showInstallation&customerID=<?php echo $customer->customerID; ?>&tab=customer&reportID=<?php echo $reportOverviewID; ?>&save=save" method="post" enctype="multipart/form-data">
            <table>
                <tr>
                    <td class="title">Customer Name</td>
                    <td><input type="text" name="customerName" value="<?php print $customer->customerName; ?>" /></td>
                </tr>
                <tr>
                    <td class="title">Account Type</td>
                    <td><table><?php
                        $accountTypes = $customerStatusLookupController->getAllCustomerStatusLookupValues();
                        $custStatusArray = $customerStatusController->getAllCustomerStatusValuesByCustomerID($customer->customerID);
                        
                        // This gets just the status lookup values from the customer status objects.
                        $func = function($custStatusArray) {
                            return $custStatusArray->custStatusLookupID;
                        };

                        // this maps the objects into an array so they can be searched with the in_array function
                        $statusIDs = array_map($func, $custStatusArray);
                        
                        foreach ($accountTypes as $type => $status) {
                            $selected = "";

                            if (in_array ( $type , $statusIDs )) {
                                $selected = 'checked="checked"';
                                
                            }
                            print "<tr><td>".$status->custStatusDescription
                                    . "</td><td><input type='checkbox' name='status_".$type."' value='$type' $selected /></td></tr>";
                             
                        }
                        ?>
                        </table>                
                    </td>
                    </tr>
                    <tr>
                        <td class="title">Website</td>
                        <td><input type="text" name="website" value="<?php print $customer->website; ?>" /></td>
                    </tr>
                    <tr>
                        <td class="title">Main Telephone</td>
                        <td><input type="text" name="mainTelephone" value="<?php print $customer->mainTelephone; ?>" /></td>
                    </tr>
                    <tr>
                        <td class="title">Main Fax</td>
                        <td><input type="text" name="mainFax" value="<?php print $customer->mainFax; ?>" /></td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="title">Account Manager</td>
                        <td><select name="accountManager">
                                <option value="">Set by territory</option>
                            <?php
                                $accountManagers = $userController->getSalesManagers();
                                foreach ($accountManagers as $key => $manager) {
                                    $selected = "";
                                    if ($customer->accountManager == $manager->id) {
                                        $selected = "selected";
                                    }
                                    print "<option value='".$manager->id."' $selected>".$manager->name."</option>";
                                }
                            ?>
                            </select>
                            
                        </td>
                    </tr>
                    <tr>
                        
                        <td class="title">Territory Assigned to</td>
                        <td><?php 

                            $territoryAccountManager = $userTerritoryController->getUserIDByTerritory($customerAddress->country);

                            echo $userController->getUserByID($territoryAccountManager)->name; ?>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    
                    <tr>
                        <td class="title">Header Notes</td>
                        <td><textarea name="headerNotes"><?php echo $customer->headerNotes; ?></textarea></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right"><input type="submit" name="submit" value="save"/></td>
                    </tr>
                </table>
            
        </form>

    </div>
</div>