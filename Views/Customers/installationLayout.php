<style>
    #menuScroll {
        float: left;
        position: fixed;
        top: 100px;
        width: 15%;
        padding-right: 5px;
        margin-left: -15px;
        font-size: 0.9em;
        line-height: 2.5em;
    }
    
    #menuScroll ul {
        list-style-type: none;
        margin: 0;
        padding: 10px;
    }
    
    #installationLayout {
        float: right;
        position: relative;
        width: 84%;
    }
    
    #topMenu {
        background: #d3d3d4;
        padding: 3px;
        border-radius: 15px;
        margin-bottom: 20px;
        box-shadow: 2px 2px 2px #888888;
    }
    
    #gotoMenu {
        background: #d3d3d4;
        padding: 3px;
        border-radius: 15px;
        box-shadow: 2px 2px 2px #888888;
    }
    
    #gotoMenu h4 {
        margin-left: 10px;
        margin-bottom: 0px;
    }
</style>
<div id="menuScroll">
    <div id="topMenu">
        <ul>
            <li><a href='index.php?portal=mr&page=report'>Back</a></li>
            <!--<li><a href=''>Add new record</a></li>    -->
        </ul>
    </div>
    <div id="gotoMenu">
        <h4>Go To</h4>
        <ul>
            <li><a href='#installationContainerCustomer'>Customer Information</a></li>
            <li><a href='#officeLocations'>Office Locations</a></li>
            <li><a href='#installationContainerContacts'>Contacts</a></li>
            <li><a href='#installationContainerCombinedFleet'>Combined Fleet</a></li>
            <li><a href='#installationContainerTechnical'>Managed Fleet</a></li>
            <li><a href='#installationContainerOwnerParent'>Owner/Parent For</a></li>
            <li><a href='#installationContainerTechnicalManager'>Technical Manager For</a></li>
            <li><a href='#installationContainerProductSummary'>Product Summary</a></li>
            <!--<li><a href='#installationContainerFleetCount'>Fleet Counts</a></li>-->
        </ul>
    </div>
</div>
<div id="installationLayout">

    <?php
    $installationsArray = $installationController->getAllInstallationsByCustomer($customerID);
    include("Views/Customers/customerProfile.php");
    include("Views/Customers/instContacts.php");
    
    $combinedFleet = $installationController->getCombinedFleetByCustomer($customerID);
    if (isset($combinedFleet)) {
        include("Views/Customers/instCombinedFleet.php");
    }
    
    include("Views/Customers/instInstallationOverview.php");
    
    $ownerParentList = $installationController->getInstallationsByOwnerParentID($customerID);
    $technicalManagerList = $installationController->getInstallationsByTechnicalManagerID($customerID);
    
    
    if (isset($ownerParentList)) {
        include("Views/Customers/instOwnerParent.php");
    }
    if (isset($technicalManagerList)) {
        include("Views/Customers/instTechnicalManager.php");
    }

    $productSummaryList = $productController->productSummaryByCustomerID($customerID);
    include 'Views/Customers/instProductSummary.php';
    $customerFleetCountsList = $customerFleetCountsController->getCustomerFleetCountsByCustomerID($customerID);
    if (isset($customerFleetCountsList)) {
        include("Views/Customers/instFleetCounts.php");
    }
    
    ?>

</div>