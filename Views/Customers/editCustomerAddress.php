<script src="./js/jquery.are-you-sure.js"></script>
<script src="./js/ays-beforeunload-shim.js"></script>

<script>
    $(function () {

        $('#customerAddressForm').areYouSure();

    });
</script>
<?php
$officeName = "";
$address = "";
$city = "";
$state = "";
$country = "";
$postcode = "";
$addressVerified = "";
$customerID = $_GET['customerID'];
$archived = "";
$defaultAddress = ""; // if this is set to 1 then it needs to reset other address entries to 0.
$addressID = "";
$reportOverviewID = $_GET['reportID'];

if (isset($_GET['addressID'])) {
    $addressID = $_GET['addressID'];
    $addressObject = $customerAddressController->getCustomerAddressByID($_GET['addressID']);
    
    $officeName = $addressObject->officeName;
    $address = $addressObject->address;
    $city = $addressObject->city;
    $state = $addressObject->state;
    $country = $addressObject->country;
    $postcode = $addressObject->postcode;
    $addressVerified = $addressObject->addressVerified;
    $customerID = $addressObject->customerID;
    $archived = $addressObject->archived;
    $defaultAddress = $addressObject->defaultAddress;
}


?>
<div id='installationContainer'>
    <div id="installationHeader">
        <img src="./icons/PhoneIcon50x50.png" alt="" height='40px'/>
        <h3>Customer Address</h3>
    </div>
    <div id='installationSubHeader'>

    </div>

    <div id='installationContentWide'> 
        <form name="customerAddressForm" id="customerAddressForm" action="index.php?portal=mr&page=report&action=showInstallation&customerID=<?php echo $_GET['customerID']; ?>&tab=customerAddress&reportID=<?php echo $_GET['reportID']; ?>&save=save&addressID=<?php echo $addressID; ?>" method="post" enctype="multipart/form-data">
            <table>
                
                <tr>
                    <td class="title">Office Name</td>
                    <td><input type="text" name="officeName" value="<?php echo $officeName; ?>"/></td>
                </tr>
                <tr>
                    <td class="title">Address</td>
                    <td><input type="text" name="address" value="<?php echo $address; ?>"/></td>
                </tr>
                <tr>
                    <td class="title">City</td>
                    <td><input type="text" name="city" value="<?php echo $city; ?>"/></td>
                </tr>
                <tr>
                    <td class="title">State</td>
                    <td><input type="text" name="state" value="<?php echo $state; ?>"/></td>
                </tr>
                <tr>
                    <td class="title">Post Code</td>
                    <td><input type="text" name="postcode" value="<?php echo $postcode; ?>"/></td>
                </tr>
                <tr>
                    <td class="title">Country</td>
                    <td><select name="country">
<?php
$countryArray = $countryController->getCountries();

foreach ($countryArray as $countryName => $cname) {
    $selected = "";
    if ($cname != "") {
        if ($cname->countryName == $country) {
            $selected = "selected";
        }
        echo '<option value="' . $cname->countryName . '" '.$selected.'>' . $cname->countryName . '</option>';
    }
}
?>      

                            </select>
                    </td>
                </tr>
                <tr>
                    <td class="title">Default Address</td>
                    <?php
                    $checkedDA = "";
                    if ($defaultAddress == 1) {
                        $checkedDA = "checked";
                    }
                    ?>
                    <td><input type="checkbox" name="defaultAddress" value="1" <?php echo $checkedDA; ?> /></td>
                </tr>
                <tr>
                    <td class="title">Address Verified</td>
                    <?php
                    $checkedAV = "";
                    if ($addressVerified == 1) {
                        $checkedAV = "checked";
                    }
                    ?>
                    <td><input type="checkbox" name="addressVerified" value="1" <?php echo $checkedAV; ?> /></td>
                </tr>
                <tr>
                    <td class="title">Archive Address</td>
                    <?php
                    $checked = "";
                    if ($archived == 1) {
                        $checked = "checked";
                    }
                    
                    $enabled = 'disabled';
                    
                    if ($_SESSION['level']=='1') {
                        $enabled = '';
                    }
                    ?>
                    <td><input type="checkbox" name="archived" value="1" <?php echo $checked; ?> <?php echo $enabled; ?> /></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" name="submit" value="save"/></td>
                </tr>
            </table>
        </form>

    </div>
</div>