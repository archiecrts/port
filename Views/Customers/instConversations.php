<div id='installationContainer'>

    <div id="installationHeader">
        <img src="./icons/conversationsIcon.png" alt="" height='40px'/>
        <h2>Conversations</h2>
    </div>

    <script>
        $(function () {
            $("#dialog-confirm").dialog({
                autoOpen: false,
                resizable: false,
                height: 220,
                modal: true,
                buttons: {
                    "Confirm": function () {
                        $(this).dialog("close");
                        var userID = $(this).data("userID");
                        var conversationID = $(this).data("conversationID");
                        var server = '<?php print $_SESSION['server']; ?>';
                        $.ajax({url: 'Views/Installations/closeActionSnippet.php?conversationID=' + conversationID + '&userID=' + userID + '&server=' + server,
                            success: function (output) {
                                window.location.reload();
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(xhr.status + " " + thrownError);
                            }});
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                },
                create: function () {
                    $(this).closest(".ui-dialog").find(".ui-button").addClass("custom");
                }
            });
        });
        function closeAction(conversationID, userID) {
            $("#dialog-confirm").data('userID', userID).data('conversationID', conversationID).dialog("open");
        }
        ;
    </script>





    <div id='installationSubHeader'>
        <div id="dialog-confirm" title="Are you sure?">
            <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
                This will complete the action.<br/>Are you sure?</p>
        </div>
        <h4><?php echo $customer->customerName; ?></h4>
    </div>

    <?php
    include_once ('Controllers/CountryController.php');
    include_once ('Controllers/CustomerConversationHistoryController.php');
    include_once ('Controllers/UserController.php');
    $countryController = new CountryController();
    $customerConversationHistoryController = new CustomerConversationHistoryController();
    $userController = new UserController();
// form submitted

    //print_r($_GET);
    
    
    if (isset($_POST['submit'])) {
        

        $actionDueDate = date('Y-m-d', time());
        if (isset($_POST['actionDueDate'])) {
            $actionDueDate = $_POST['actionDueDate'];
        }

        $replyToConversationItemID = 0;
        if (isset($_POST['replyToConversationItemID'])) {
            $replyToConversationItemID = $_POST['replyToConversationItemID'];
        }
        
        $conversationItem = $customerConversationController->setConversationEntryFromUser(
                $_SESSION['user_id'], date('Y-m-d H:i:s', time()), addslashes($_POST['content']), $actionDueDate, $_POST['typeAction'], $_POST['contactID'], null, $replyToConversationItemID);

        //print $conversationItem;
        
        // We dont want the post to be accidentally entered twice 
        // so I unset the post data and redirect the page to clear the data as well.
        // header(location:url) doesn't work on this because it is called half way through the index page.
        
        
        unset($_POST);
        print '<meta http-equiv="refresh" content="0;url=index.php?portal=mr&page=report&action=showInstallation&customerID=' . $customerID . '&tab=conversation&reportID='.$_GET['reportID'].'" />';
    }

    $installation = $installationController->getInstallationByInstallationID($installationID);
    $timezone = $countryController->getTimeZoneByCountryName($customerAddress->country);

    function formatNum($num) {
        return ($num > 0) ? '+' . $num : $num;
    }
    ?>
    <div id="installationContentWide">
        <p class="red">REMINDER: Installation is <?php echo formatNum($timezone->timezone); ?> hours from GMT [<?php echo date("H:i", strtotime($timezone->timezone." hours")); ?>]</p> 


<?php
print '<div id="replyFormDiv">';
include ('Views/Installations/conversationReplyForm.php');
print '</div>';

print '<div id="pagination">';
include('Views/Installations/pagination.php');
print '</div>';

$conversationsArray = $customerConversationController->getConversationsByCustomerIDPaged($customerID, $offset, $numberOfRows);

if (isset($conversationsArray)) {
    $historyArray = [];
    foreach ($conversationsArray as $key => $conversation) {
        
        $contact = $customerContactsController->getContactByID($conversation->contactID);
        $reportOverview = $reportOverviewController->getSingleReportByReportOverviewID($conversation->campaignOverviewID);
        $closed = 0;
        $history = $customerConversationHistoryController->getHistoryByConversationID($conversation->conversationID);

        if (sizeof($history) == 1) {
            $closed = 1;
        }
        print '<table class="conversationBox">
                    <tr>
                    <td><strong>' . $contact->jobTitle . '</strong></td>
                    <td>' . $contact->salutation . ' ' . $contact->firstName . ' ' . $contact->surname . '</td>
                    <td>' . date('Y-m-d', strtotime($conversation->date)) . '</td>
                    
                    <td>Reporter:</td>
                    <td>' . $userController->getUserByID($history[0]->userID)->name . '</td>
                </tr>
                <tr> 
                    <td>' . $reportOverview->reportName . '</td>
                    <td colspan="2" class="activeConversationContentBox">' . wordwrap($conversation->content, 60, '<br/>') . '</td>
                    <td>Next Action Due:</td><td>' . date('Y-m-d', strtotime($conversation->nextActionDueDate)) . '</td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2"></td>
                    <td>Action Type:</td><td>' . $conversation->actionType . '</td>
                </tr>';

        if ($closed == 1) {
            print '<tr>
                        <td></td>
                        <td colspan="2"></td>
                        <td colspan="2"><button onclick="closeAction(' . $conversation->conversationID . ', ' . $_SESSION['user_id'] . ');">Mark Action Completed</button></td>
                    </tr>';
        }
        print '</table>';

        // trying to make an array of Years and Dates for right hand side of screen.
        $string = date('Y-m-d', time());
        $date = DateTime::createFromFormat("Y-m-d", $string);
    }
}
?>
    </div>
</div>