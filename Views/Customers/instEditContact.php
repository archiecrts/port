<script src="./js/jquery.are-you-sure.js"></script>
<script src="./js/ays-beforeunload-shim.js"></script>

<script>
    $(function () {

        $('#contactForm').areYouSure();

    });
</script>

<div id='installationContainer'>
    <div id="installationHeader">
        <img src="./icons/PhoneIcon50x50.png" alt="" height='40px'/>
        <h3>CONTACTS</h3>
    </div>
    <div id='installationSubHeader'>

    </div>
    <?php
    $contactID = "";
    $jobTitle = "";
    $salutation = "";
    $firstName = "";
    $surname = "";
    $email = "";
    $number = "";
    $mobile = "";
    $infoSource = "";
    $inactive = "";
    $contactValidated = "";
    $customerID = "";
    $customerAddressID = "";

    if (isset($_GET['contactID'])) {
        $contact = $customerContactsController->getContactByID($_GET['contactID']);

        $contactID = $contact->customerContactID;
        $jobTitle = $contact->jobTitle;
        $salutation = $contact->salutation;
        $firstName = $contact->firstName;
        $surname = $contact->surname;
        $email = $contact->email;
        $number = $contact->number;
        $mobile = $contact->mobile;
        $infoSource = $contact->infoSource;
        $inactive = $contact->inactive;
        $contactValidated = $contact->contactValidated;
        $customerID = $contact->customerID;
        $customerAddressID = $contact->customerAddressID;
    }
    ?>
    <div id='installationContentWide'> 
        
        <form name="contactForm" id="contactForm" action="index.php?portal=mr&page=report&action=showInstallation&customerID=<?php echo $_GET['customerID']; ?>&tab=contacts&reportID=<?php echo $reportOverviewID; ?>&save=save" method="post" enctype="multipart/form-data">
            <table>
                <input type="hidden" name="contactID" value="<?php echo $contactID; ?>"/>
                <tr>
                    <td class="title">Job Title</td>
                    <td><input type="text" name="jobTitle" value="<?php echo $jobTitle; ?>"/></td>
                </tr>
                <tr>
                    <td class="title">Contact Name</td>
                    <td><input type="text" name="salutation" value="<?php echo $salutation; ?>" size="10"/> <input type="text" name="firstName" value="<?php echo $firstName; ?>"/> <input type="text" name="surname" value="<?php echo $surname; ?>"/></td>
                </tr>
                <tr>
                    <td class="title">Email</td>
                    <td><input type="text" name="email" value="<?php echo $email; ?>"/></td>
                </tr>
                <tr>
                    <td class="title">Number</td>
                    <td><input type="text" name="number" value="<?php echo $number; ?>"/></td>
                </tr>
                <tr>
                    <td class="title">Mobile</td>
                    <td><input type="text" name="mobile" value="<?php echo $mobile; ?>"/></td>
                </tr>
                <tr>
                    <td class="title">Associated Address</td>
                    <td><select name="customerAddressID">
                            <option value=""></option>
                            <?php
 
                            foreach ($customerAddresses as $key => $address) {
                                $selected = "";
                                if ($address->addressID == $customerAddressID) {
                                    $selected = "selected";
                                }
                                print "<option value='" . $address->addressID . "' " . $selected . ">" . $address->officeName . "</option>";
                            }
                            ?>

                        </select></td>
                </tr>
                <tr>
                    <td class="title">Info Source</td>
                    <td><input type="text" name="infoSource" value="<?php echo $infoSource; ?>"/></td>
                </tr>
                <tr>
                    <td class="title">Contact Validated</td>
                    <?php
                    $checked = "";
                    if ($contactValidated == 1) {
                        $checked = "checked";
                    }
                    ?>
                    <td><input type="checkbox" name="contactValidated" value="1" <?php echo $checked; ?> /></td>
                </tr>
                <tr>
                    <td class="title">Inactive</td>
                    <?php
                    $checked = "";
                    if ($inactive == 1) {
                        $checked = "checked";
                    }
                    ?>
                    <td><input type="checkbox" name="inactive" value="1" <?php echo $checked; ?> /></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" name="submit" value="save"/></td>
                </tr>
            </table>
        </form>

    </div>
</div>