<?php

include_once 'Controllers/EngineManufacturerController.php';
include_once 'Controllers/EngineModelController.php';
include_once 'Controllers/EngineModelVersionController.php';
include_once 'Controllers/EngineMakeModelVersionController.php';
include_once 'Controllers/PartManifestController.php';
include_once 'Controllers/PartCodeController.php';
include_once 'Controllers/PartDescriptionLookupController.php';
include_once 'Controllers/PartColourController.php';
include_once 'Controllers/PartDimensionsController.php';
include_once 'Controllers/PartMaterialSpecController.php';
$partColourController = new PartColourController();
$engineManufacturerController = new EngineManufacturerController();
$engineMakeModelVersionController = new EngineMakeModelVersionController();
$engineModelController = new EngineModelController();
$engineModelVersionController = new EngineModelVersionController();
$partManifestController = new PartManifestController();
$partCodeController = new PartCodeController();
$partDescriptionLookupController = new PartDescriptionLookupController();
$partDimensionsController = new PartDimensionsController();
$partMaterialSpecController = new PartMaterialSpecController();

include_once 'Controllers/DesignerLookupController.php';
include_once 'Controllers/SeriesLookupController.php';
include_once 'Controllers/VersionLookupController.php';
$designerLookupController = new DesignerLookupController();
$seriesLookupController = new SeriesLookupController();
$versionLookupController = new VersionLookupController();

$page = "generate";
// This is checking that the users session has been created.
// if not we force the redirect to access control.
if (!isset($_SESSION['name'])) {
    $page = "accessControl";
} else {
    if (isset($_GET['page'])) {
        $page = $_GET['page'];
    }
}
switch ($page) {
    case "addPart":
        if (isset($_GET['action']) && $_GET['action'] == 'save' ) {
            
            $dimension = new PartDimensions("", $_POST['dimOD'], $_POST['dimPD'], $_POST['dimLength'], $_POST['dimDIN'], 
                    $_POST['dimHeadType'], $_POST['dimAngle'], $_POST['dimID']);
            
            $materialSpec = new PartMaterialSpec("", $_POST['materialCode'], $_POST['materialDesc'], $_POST['materialHardness'], 
                    $_POST['materialDIN'], $_POST['materialSpec']);
            
            // first check if the object is a duplicate, add if its new.
            $dimensionCheck = $partDimensionsController->getDimensionByObjectCriteria($dimension);
            // insert dimensions and get ID
            if ($dimensionCheck == null) {
                $dimensionID = $partDimensionsController->insertDimensions($dimension);
            } else {
                $dimensionID = $dimensionCheck;
                print "<span class='blue'>Dimension already existed</span><br/>";
            }
            // insert material spec and get ID
            $materialsCheck = $partMaterialSpecController->getMaterialsFromCriteriaOptions($materialSpec);
            // insert dimensions and get ID
            if ($materialsCheck == null) {
                $materialID = $partMaterialSpecController->insertMaterialSpec($materialSpec);
            } else {
                $materialID = $materialsCheck;
                print "<span class='blue'>Material already existed</span><br/>";
            }
            // insert product code object.
            
            $partCode = (isset($_POST['partCode'])) ? $_POST['partCode'] : ""; 
            $partDescriptionLookupID = (isset($_POST['minorDescription'])) ? $_POST['minorDescription'] : ""; 
            $partWeight = (isset($_POST['weight'])) ? $_POST['weight'] : ""; 
            $partTotalStock = (isset($_POST['totalStock'])) ? $_POST['totalStock'] : "";
            $partMinimumStock = (isset($_POST['minStock'])) ? $_POST['minStock'] : "";
            $partReservedStock = (isset($_POST['reservedStock'])) ? $_POST['reservedStock'] : "";
            $partFreeStockQty = (isset($_POST['freeStockQty'])) ? $_POST['freeStockQty'] : "";
            $partOnOrderQty = (isset($_POST['onOrderQty'])) ? $_POST['onOrderQty'] : "";
            $partReservedForKits = (isset($_POST['reservedForKits'])) ? $_POST['reservedForKits'] : "";
            $partPreferredSupplierID = 0;
            $partCostPrice = (isset($_POST['costPrice'])) ? $_POST['costPrice'] : "";
            $partCostPriceSetDate = (isset($_POST['costPriceSetDate'])) ? $_POST['costPriceSetDate'] : "";
            $partWearItem = (isset($_POST['wearItem'])) ? $_POST['wearItem'] : "";
            $colourID = (isset($_POST['colour'])) ? $_POST['colour'] : "";
            $ldPartNumber = (isset($_POST['ldPartNumber'])) ? $_POST['ldPartNumber'] : "";
            $description = (isset($_POST['description'])) ? $_POST['description'] : "";
            $stockItem = (isset($_POST['stockItem'])) ? $_POST['stockItem'] : "";
            $quantityOnEngine = (isset($_POST['quantity'])) ? $_POST['quantity'] : "1";
            
            $partCodeObj = new PartCode("", $partCode, $partDescriptionLookupID, $partWeight, $partTotalStock, 
                    $partMinimumStock, $partReservedStock, $partFreeStockQty, $partOnOrderQty, $partReservedForKits, 
                    $partPreferredSupplierID, $partCostPrice, $partCostPriceSetDate, $partWearItem, $dimensionID, $materialID, $colourID,
                    $ldPartNumber, $description, $stockItem);
            
            $partCheck = $partCodeController->getPartCodeByCriteriaOptions($partCodeObj);
            
            if ($partCheck == null) {
                $insertPart = $partCodeController->insertPartCode($partCodeObj);
            } else {
                $insertPart = $partCheck;
                print "<span class='blue'>Part already exists</span><br/>";
            }
            
            // attach to engines if they have been selected.
            
            // get parts manifest list for these version IDs
            if (isset($_POST['engineSeries']) && sizeof($_POST['engineSeries']) > 0) {
                foreach ($_POST['engineSeries'] as $seriesID) {
                    // get all versions for that series
                    $versions = $versionLookupController->getVersionIDBySeriesID($seriesID);
                    foreach ($versions as $vkey => $version ) {
                        $countEntries = $partManifestController->matchManifestEntry($insertPart, $vkey, $quantityOnEngine);

                        if ($countEntries < 1) {
                            $manifest = new PartManifest("", $insertPart, 0, 0, $quantityOnEngine, 0, $vkey, "");

                            $insert = $partManifestController->insertManifestObject($manifest);
                            if ($insert != false) {
                                print "<span class='blue'>Part added to manifest</span><br/>";
                            }
                        } else {
                            print "<span class='blue'>Part already on manifest ($vkey)</span><br/>";
                        }
                    }
                    
                }
            }
            
        }
        include 'Views/Parts/addPart.php';
        break;
    case "manifest":
        include ('Views/Parts/manifest.php');
        break;
    case "addToManifest":
        if ($_GET['action'] == 'createGroupSave') {
            // save the part to all versions as required
            
            if (isset($_POST['addTo'])) {
                if ($_POST['addTo'] == 'all') {
                    $result = $partManifestController->insertIntoManifestBySeriesID($_GET['seriesID'], $_POST['partCodeID'], $_POST['quantity']);

                    if ($result != false) {
                        print "Component added to all versions";
                    } else {
                        print "Add failed";
                    }
                } else {
                    $result = $partManifestController->insertIntoManifestByVersionID($_GET['seriesID'], $_POST['addTo'], $_POST['partCodeID'], $_POST['quantity']);

                    if ($result != false) {
                        print "Component added to all versions";
                    } else {
                        print "Add failed";
                    }
                }
            }
            
        }
        include ('Views/Parts/addToManifestGroup.php');
        break;
   /* case "generate":
        include 'Views/Parts/generateSelectEngine.php';
        break;
    case "downloadPartsList":
        include 'Views/Parts/downloadPartsList.php';
        break;
    case "upload":
        include ('Views/Parts/uploadCSV.php');
        break;
    case "uploadConfirmation":
        include 'Views/Parts/uploadConfirmation.php';
        break;
    case "addEngine":
        include ('Views/Parts/addEngine.php');
        break;
    case "editEngine":
        include ('Views/Parts/editEngine.php');
        break;
    case "addMultipleEngines":
        include ('Views/Parts/addMultipleEngines.php');
        break;
    case "serviceList":
        include ('Views/Parts/serviceList.php');
        break;
    case "confirmServiceList":
        include ('Views/Parts/confirmServicingIntervals.php');
        break;
    case "intervals":
        include ('Views/Parts/intervals.php');
        break;*/
    default:
        include ('Views/Parts/manifest.php');
}  
