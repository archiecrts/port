<h3>Upload HMRC Exchange Rate CSV</h3>

<p>CSV downloaded from <a href='https://www.gov.uk/government/collections/exchange-rates-for-customs-and-vat' target="_blank">HMRC</a></p>
<p>
<form name="uploadCSV" action="index.php?portal=accounts&page=uploadHMRCRate" method="post" enctype="multipart/form-data">
    <input type="file" name="rateCSV"/>
    <input type="submit" name="submit" value="Submit"/>
</form>
</p>

<table class="borderedTable">
    <tr>
        <th>Country</th>
        <th>Currency</th>
        <th>Currency Code</th>
        <th>Currency Units Per Pound</th>
        <th>Start Date</th>
        <th>End Date</th>
    </tr>
    <?php
    if (isset($allRates)) {
        foreach ($allRates as $index => $rate) {
            $css = "";
            if(new DateTime("today") > new DateTime($rate->hmrcEndDate)){
                $css = "class='red'";
            }
            print "<tr $css>";
            print "<td>" . $rate->hmrcCountry . "</td>";
            print "<td>" . $rate->hmrcCurrency . "</td>";
            print "<td>" . $rate->hmrcCurrencyCode . "</td>";
            print "<td>" . $rate->hmrcCurrencyUnitsPerPound . "</td>";
            print "<td>" . date("d-m-Y", strtotime($rate->hmrcStartDate)) . "</td>";
            print "<td>" . date("d-m-Y", strtotime($rate->hmrcEndDate)) . "</td>";
            print "</tr>";
        }
    }
    ?>
</table>