<h3>Daily Exchange Rates</h3>
    
<p>These will be downloaded from Google's Exchange Rate page.</p>    

<a href="index.php?portal=accounts&page=updateDailyRates">Force Rate Update</a>
<h3>Daily Rates Table</h3>
<table class="borderedTable">
    <tr>
        <th>Country</th>
        <th>Currency Code</th>
        <th>Rate</th>
        <th>Date Set</th>
    </tr>
<?php

if (isset($dailyRates)) {
    foreach($dailyRates as $index => $rate) {
        print "<tr>";
        print "<td>".$dailyExchangeRateController->matchCurrencyCodesHMRCToDaily($rate->dailyEXCCurrencyCode)."</td>";
        print "<td>$rate->dailyEXCCurrencyCode</td>";
        print "<td>$rate->dailyEXCCurrencyUnitPerPound</td>";
        print "<td>$rate->dailyEXCDateSet</td>";
        print "</tr>";
    }
} else {
    print "<tr><td colspan='4'>No rates set</td></tr>";
}
?>
</table>