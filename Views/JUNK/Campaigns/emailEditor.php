<span class='headerH2'>Edit Email Content</span>
<span id="returnLink"><a href="index.php?portal=mr&page=campaign&action=details&campaignID=1">Return to Campaign Details</a>'
<img src="./icons/returnArrow.png" alt="" height="15px" /></span>

<link rel="stylesheet" href="TinyEditor/style.css" />
<script type="text/javascript" src="TinyEditor/tinyeditor.js"></script>
<script type="text/javascript" src="js/validateArticleForms.js"></script>

<p>&nbsp;</p>
<?php
function ae_detect_ie() {
    if ((isset($_SERVER['HTTP_USER_AGENT']) &&
            (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident') !== false ||
            strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false))) {

        return true;
    } else {
        return false;
    }
}

if (ae_detect_ie()) {
    ?>
<p class="red">It seems, that your are using MSIE.
    Why not to switch to standard-complaint brower, like 
    <a href="http://www.google.com/chrome/">Chrome</a> or
    <a href="http://www.mozilla.com/firefox/">Firefox</a>?</p>
<p>This browser does not support the editor below.</p>
<?php 

} 
?>
<table>
    <tr>
        <td>Subject</td>
        <td><input type="text" name="subject" size="40px"/></td>
    </tr>
    <tr>
        <td>From Address</td>
        <td><input type="text" name="fromAddress" size="40px"/></td>
    </tr>
    <tr>
        <td>Reply-to Address</td>
        <td><input type="text" name="replyToAddress" size="40px"/></td>
    </tr>
    <tr>
        <td>Header & Footer</td>
        <td><select name="headerFooter">
                <option value="">Group</option>
                <option value="">Lincoln</option>
                <option value="">Simplex</option>
                <option value="">MPS/MPR</option>
            </select></td>
    </tr>
    <tr>
        <td colspan="2"><p id="demo" class="red"></p>
            <textarea id="input" name="content" style="width:400px; height:200px"></textarea>
<script type="text/javascript">
new TINY.editor.edit('editor',{
	id:'input',
	width:584,
	height:175,
	cssclass:'te',
	controlclass:'tecontrol',
	rowclass:'teheader',
	dividerclass:'tedivider',
	controls:['bold','italic','underline','strikethrough','|','subscript','superscript','|',
			  'orderedlist','unorderedlist','|','outdent','indent','|','leftalign',
			  'centeralign','rightalign','blockjustify','|','unformat','|','undo','redo','n',
			  'size','style','|','image','hr','link','unlink','|','cut','copy','paste','print'],
	footer:true,
	fonts:['Helvetica LT Com Light'],
	xhtml:true,
	cssfile:'TinyEditor/style.css',
	bodyid:'editor',
	footerclass:'tefooter',
	toggle:{text:'show source',activetext:'show wysiwyg',cssclass:'toggle'},
	resize:{cssclass:'resize'}
});
</script></td>
    </tr>
    <tr>
        <td colspan="2" align="right"><input type="submit" name="submit" value="Preview"/><input type="submit" name="submit" value="Save"/></td>
    </tr>
</table>