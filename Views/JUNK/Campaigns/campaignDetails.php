<style>
    #campaignDetails {
        float: left;
        position: relative;
        width:40%;
    } 
    
    #emailContent {
        border: 1px solid grey;
        width:50%;
        float: right;
        position: relative;
        margin-top:50px;
    }
    
    #emailHeader {
        padding:5px;
        font-size: 1.5em;
        font-weight: bold;
        color: blue;
    }
    
    #emailBody {
        padding:5px;
    }
    
    #emailFooter {
        padding:5px;
        font-size: 0.9em;
        color: green;
    }
    

</style>
<span class='headerH2'>Campaign Details</span>
<p>&nbsp;</p>
<?php

/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

?>
<div id="campaignDetails">
<h4>Details</h4>
<table>
    <tr>
        <td>Campaign Name</td>
        <td>Hong Kong</td>
    </tr>
    <tr>
        <td>Status</td>
        <td>Mail shot sent</td>
    </tr>
    <tr>
        <td># Records</td>
        <td>159</td>
    </tr>
    <tr>
        <td>Date/Start Time</td>
        <td>2015-11-21 15:22:02</td>
    </tr>
    <tr>
        <td>Owner</td>
        <td>Clare Hogg</td>
    </tr>
</table>
<h4>Settings</h4>

<table>
    <tr>
        <td>Broadcast Date/Time</td>
        <td>2015-11-21 15:22:02</td>
    </tr>
    <tr>
        <td>Message Type</td>
        <td>Bulk Email [or personal]</td>
    </tr>
    <tr>
        <td>Broadcast Speed</td>
        <td>[bulk, 10 per day]</td>
    </tr>

    <tr>
        <td>Email</td>
        <td><a href="index.php?portal=mr&page=campaign&action=editEmailContent">Edit Email Content</a></td>
    </tr>
    
    <tr>
        <td>Subject</td>
        <td>All The Squirrels</td>
    </tr>
    <tr>
        <td>Reply-to address</td>
        <td>chogg@stdieselmarinegroup.com</td>
    </tr>
    <tr>
        <td>Header & Footer</td>
        <td>Group</td>
    </tr>
</table>

<h4>Attached Documents</h4>
<ul>
    <li><a href="">Welcome_Brochure.pdf</a></li>
</ul>
<h4>Associated Documents</h4>
<ul>
    <li><a href="">Welcome_BrochureV1.pdf</a></li>
</ul>
<h4>Collaborators</h4>
<table>
    <tr>
        <td>Tom Syckelmoore </td>
        <td><strong>[new note]</strong></td>
        <td><img src="./icons/edit_pencil.png" alt="" height="16px"/></td>
    </tr>
    <tr>
        <td colspan="3">
            <table>
                <tr>
                    <td style="vertical-align:top">2015-12-24 12:14:00</td>
                    <td>
                        <div style="width:200px">This is a note, please read the brochure and 
                            check for spelling mistakes.</div>
                    </td>
                    <td>Action Taken <input type="checkbox" /></td>
                </tr>
            </table>  
        </td>
    </tr>
    <tr>
        <td>Clare Hogg</td>
        <td></td>
        <td><img src="./icons/edit_pencil.png" alt="" height="16px"/></td>
    </tr>
    <tr>
        <td>Caterina Zucca</td>
        <td><strong>[new note]</strong></td>
        <td><img src="./icons/edit_pencil.png" alt="" height="16px"/></td>
    </tr>
</table>

<h4>Results</h4>
<p><a href=""><img src="./icons/graph.png" alt="" height='16px'/> View Graphs </a></p>

<table>
    <tr>
        <td>Replies</td>
        <td align='center'>4</td>
    </tr>
    <tr>
        <td>Orders Generated</td>
        <td align='center'>1</td>
    </tr>
    <tr>
        <td>Bounce Backs</td>
        <td align='center'>43</td>
    </tr>
    <tr>
        <td>Opt-outs</td>
        <td align='center'>3</td>
    </tr>
</table>
<p><a href=""><img src="./icons/graph.png" alt="" height='16px'/> Compare Campaigns </a></p>
<h4>Notes</h4>

<p>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."
"There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain..."</p>
</div>
<div id="emailContent">
    <div id="emailHeader">S-T Diesel and Marine Group</div>
    <div id="emailBody"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
        Cras malesuada eros lorem, vitae bibendum risus vulputate eget. Mauris 
        eros nulla, tempus sit amet blandit a, vehicula eu lectus. Pellentesque 
        maximus, eros vitae lobortis vulputate, orci magna lacinia nunc, eget 
        maximus dui lorem sit amet augue. Sed ut luctus nisi, ac vulputate enim. 
        Cras euismod diam dolor, sed aliquet urna aliquet vel.</p>
        <p>Proin convallis 
        arcu arcu, ac accumsan orci efficitur ut. Vivamus at suscipit dui. Vivamus 
        id laoreet felis. Duis bibendum condimentum felis, a venenatis sem vulputate 
        vel. Quisque malesuada eleifend tortor, a efficitur orci commodo vel. 
        Curabitur ornare hendrerit neque, at volutpat metus scelerisque eget. 
        Duis blandit ac enim in efficitur. Nam imperdiet neque sed lacinia eleifend. 
        Fusce rutrum neque ac dolor bibendum mollis.</p></div>
    <div id="emailFooter">VAT No. 123456789<br/>Tel: 01264 860186</div>
</div>