<span class='headerH2'>Campaigns</span>


<?php
/**
 * SET CONTROLLERS.
 */
/*include_once ('Controllers/InstallationController.php');
include_once ('Controllers/GetInstallationsArrayController.php');
include_once ('Controllers/CustomerContactsController.php');
include_once ('Controllers/InstallationUnitController.php');
include_once ('Controllers/CustomerConversationController.php');
include_once ('Controllers/CampaignOverviewController.php');
include_once ('Controllers/CampaignParametersController.php');
include_once ('Controllers/CampaignController.php');
include_once 'Controllers/CampaignUserDisplaySettingsController.php';
include_once ('Controllers/UserController.php');
include_once 'Controllers/UserTerritoryController.php';
include_once 'Controllers/CountryController.php';
include_once("Controllers/UserHistoryController.php");*/

/*$userHistoryController = new UserHistoryController();
$installationController = new InstallationController();
$getInstallationsArrayController = new GetInstallationsArrayController();
$customerContactsController = new CustomerContactsController();
$installationUnitController = new InstallationUnitController();
$installationConversationController = new CustomerConversationController();
$campaignOverviewController = new CampaignOverviewController();
$campaignParametersController = new CampaignParametersController();
$campaignController = new CampaignController();
$campaignUserDisplaySettingsController = new CampaignUserDisplaySettingsController();
$userController = new UserController();
$userTerritoryController = new UserTerritoryController();
$countryController = new CountryController();*/

require_once 'Helpers/charsetFunctions.php';
?>
<style>
    .notFinished {
        background: orangered;
    }

    .finished {
        background: greenyellow;
    }

</style>
<h3>Campaign Dashboard</h3>

<h4>My Notes and Tasks</h4>
<table>
    <tr>
        <th>Campaign Name</th>
        <th>Task</th>
        <th>Date</th>
        <th>Action Taken</th>
    </tr>
    <tr>
        <td>Hong Kong</td>
        <td>Change the brochure page 2 to have more blue borders</td>
        <td>2015-12-24 12:00:14</td>
        <td align='center'><input type="checkbox"/></td>
    </tr>
    <tr>
        <td>Hong Kong</td>
        <td>New Document Added: Test_Brochure.pdf</td>
        <td>2015-12-24 12:00:14</td>
        <td align='center'><input type="checkbox"/></td>
    </tr>
</table>
<h4>Developing Campaigns</h4>
<table>
    <tr>
        <th>Campaign Name</th>
        <th>Status</th>
        <th>Date Created</th>
        <th># Records</th>
        <th>Recipients</th>
        <th>Documents</th>
        <th>Collaborators Added</th>
        <th>Settings</th>
        <th>Approval</th>
        <th>Broadcast</th>
    </tr>
    <tr class="notFinished">
        <td><a href="index.php?portal=mr&page=campaign&action=details&campaignID=1">American Nohab</a></td>
        <td>80%</td>
        <td>2015-12-14 12:45:12</td>
        <td align='center'>34</td>
        <td align='center'><img src="./icons/tick.png" /></td>
        <td align='center'><img src="./icons/cross.png" height='17px' /></td>
        <td align='center'><img src="./icons/cross.png" height='17px' /></td>
        <td align='center'><img src="./icons/tick.png" /></td>
        <td align='center'><img src="./icons/cross.png" height='17px' /></td>
        <td></td>
    </tr>
    <tr class="finished">
        <td><a href="index.php?portal=mr&page=campaign&action=details&campaignID=1">Asia Pacific</a></td>
        <td>100%</td>
        
        <td>2015-11-21 15-22:02</td>
        <td align='center'>68</td>
        <td align='center'><img src="./icons/tick.png" /></td>
        <td align='center'><img src="./icons/tick.png" /></td>
        <td align='center'><img src="./icons/tick.png" /></td>
        <td align='center'><img src="./icons/tick.png" /></td>
        <td align='center'><img src="./icons/tick.png" /></td>
        <td><button>Broadcast</button></td>
    </tr>
</table>

<h4>Running Campaigns Summary (auto refresh)</h4>
<table>
    <tr>
        <th></th>
        <th>Campaign Name</th>
        <th>Status</th>
        <th># Records</th>
        <th>Replies</th>
        <th>Bounce Backs</th>
        <th>Opt-outs</th>
        <th>Date/Start Time</th>
        <th>Settings</th>
    </tr>
    <tr>
        <td align='center'><img src="./icons/forward_button.png" alt="" height="16px"/></td>
        <td>Hong Kong</td>
        <td>Mail shot Sent</td>
        <td align='center'>134</td>
        <td align='center'>5</td>
        <td align='center'>43</td>
        <td align='center'>3</td>
        <td>2015-11-21 15:22:02</td>
        <td align='center'><img src="./icons/gear37.png" alt="settings" height="16px"/></td>
    </tr>
    <tr>
        <td align='center'><img src="./icons/forward_button.png" alt="" height="16px"/></td>
        <td>Asia Pacific Test Run</td>
        <td>Receiving Replies</td>
        <td align='center'>34</td>
        <td align='center'>5</td>
        <td align='center'>10</td>
        <td align='center'>0</td>
        <td>2015-11-21 15:22:02</td>
        <td align='center'><img src="./icons/gear37.png" alt="settings" height="16px"/></td>
    
    </tr>
    <tr>
        <td align='center'><img src="./icons/clock.png" alt="" height="16px"/></td>
        <td>NZ Navy</td>
        <td>Delayed Start</td>
        <td align='center'>114</td>
        <td align='center'>0</td>
        <td align='center'>0</td>
        <td align='center'>0</td>
        <td>2015-11-21 22:22:02</td>
        <td align='center'><img src="./icons/gear37.png" alt="settings" height="16px"/></td>
    </tr>
</table>

<h4>Closed Campaigns</h4>
<table>
    <tr>
        <th>Campaign Name</th>
        <th>Status</th>
        <th># Records</th>
        <th>Replies</th>
        <th>Bounce Backs</th>
        <th>Opt-outs</th>
        <th>Start Date</th>
        <th>Stop Date</th>
        <th># New Customers</th>
        <th>Success Rate</th>
    </tr>
    <tr>
        <td>Hong Kong</td>
        <td>Complete</td>
        <td align='center'>134</td>
        <td align='center'>53</td>
        <td align='center'>43</td>
        <td align='center'>3</td>
        <td>2015-11-21 15:22:02</td>
        <td>2015-11-30 09:12:14</td>
        <td align='center'>8</td>
        <td align='center'>5%</td>
    </tr>
</table>

<p>&nbsp;</p>