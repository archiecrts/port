<?php

/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

$action = "";

if (isset($_GET['action'])) {
    $action = $_GET['action'];
}

switch ($action) {
    case "new":
        include 'Views/Campaigns/createNewCampaign.php';

        break;
    case "details":
        include 'Views/Campaigns/campaignDetails.php';

        break;
    case "editEmailContent":
        include 'Views/Campaigns/emailEditor.php';
        break;
    default:
        include 'Views/Campaigns/campaignDashboard.php';
        break;
}