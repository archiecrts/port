<h3>Notify Admin Requests</h3>
<?php
$statusArray = ['All', 'Open', 'Closed', 'On Hold', 'Resolved', 'Duplicate', 'Invalid', 'Wont Fix'];

?>
<ul id="notifyAdminInlineMenu">
    <?php
        $len = count($statusArray);
        foreach ($statusArray as $index => $val) {
            $border = "border";
            if( $index == $len - 1 ) {
                $border = "";
            }
            $css = "class='unselectedButton $border'";
            $imgSrc = './icons/whiteForwardButton.png';
            
            if (!isset($_GET['statusType'])) {
                $_GET['statusType'] = 'All';
            }
            if ($val === $_GET['statusType']) {
                $css = "class='selectedButton $border'";
                $imgSrc = './icons/forward_button.png';
            }
            
            print '<li '.$css.'><a href="index.php?portal=dashboard&statusType='.$val.'">'.$val.'</a></li>';
        }
    ?>
</ul>
<?php

include_once 'Controllers/NotifyAdminController.php';
include_once 'Controllers/NotifyAdminCategoryController.php';
include_once 'Controllers/UserController.php';
$notifyAdminController = new NotifyAdminController();
$notifyAdminCategoryController = new NotifyAdminCategoryController();
$userController = new UserController();


$categories = $notifyAdminCategoryController->getAllCategories();
if (isset($categories)) {
    foreach ($categories as $key => $object) {
        print '<h4>' . $object->category . '</h4>';

        print "<div>";
        print "<div class='categoryBlockDiv'>"
                . "<div class='categoryBlockHeader'  id='rounded-corners-left-top-col' style='width:140px; padding-left:10px;'>Date</div>"
                . "<div class='categoryBlockHeader' style='width:400px'>Enquiry</div>"
                . "<div class='categoryBlockHeader' style='width:140px'>User</div>"
                . "<div class='categoryBlockHeader'  id='rounded-corners-right-top-col' style='width:100px'>Status</div></div>";

        print "<div class='categoryBlockClearBoth'>&nbsp;</div>";
        
        $statusType = 'open';
        if(isset($_GET['statusType'])) {
            $statusType = $_GET['statusType'];
        }
        
        $enquiries = $notifyAdminController->getAllEnquiriesByCategoryForUser($key, $statusType, $_SESSION['user_id']);
        if (isset($enquiries)) {
            foreach ($enquiries as $enq => $enqObject) {
                print "<div class='row'>";
                print "<div  class='col' style='width:140px; padding-left:10px;'>" . $enqObject->dateLogged . "</div>";
                print "<div  class='col' style='width:400px'>" . $enqObject->issueDescription . "</div>";
                $user = $userController->getUserByID($enqObject->userID);
                print "<div  class='col' style='width:140px'>" . $user->name . "</div>";

                // Choose which option is selected.
                $status = ['Open', 'Resolved', 'On Hold', 'Duplicate',
                    'Invalid', 'Wont Fix', 'Closed'];
                
                print "<div  class='col' style='width:100px'>".$enqObject->status."</div>";
                print "</div>";
                print "<div class='categoryBlockClearBoth'>&nbsp;</div>";
            }
                
            print "</div>";
        }
        print "<div class='row'>";
                print "<div  class='col'  id='rounded-corners-left-bottom-col' style='width:140px; padding-left:10px;'>&nbsp;</div>";
                print "<div  class='col' style='width:400px'></div>";
                print "<div  class='col' style='width:140px'></div>";
                print "<div  class='col' id='rounded-corners-right-bottom-col' style='width:100px'></div>";
                print "</div>";
    }
}

