<h3>Campaigns/Reports Actions Required</h3>
<script>

    $(function () {
        $("#dialog-confirm").dialog({
            autoOpen: false,
            resizable: false,
            height: 220,
            modal: true,
            buttons: {
                "Confirm": function () {
                    $(this).dialog("close");
                    var userID = $(this).data("userID");
                    var conversationID = $(this).data("conversationID");
                    var server = '<?php print $_SESSION['server']; ?>';
                    $.ajax({url: 'Views/Installations/closeActionSnippet.php?conversationID=' + conversationID + '&userID=' + userID + '&server=' + server,
                        success: function (output) {
                            window.location.reload();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.status + " " + thrownError);
                        }});
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            },
            create: function () {
                $(this).closest(".ui-dialog").find(".ui-button").addClass("custom");
            }
        });
    });

    function closeAction(conversationID, userID) {
        $("#dialog-confirm").data('userID', userID).data('conversationID', conversationID).dialog("open");
    }
    ;
</script>

<div id="dialog-confirm" title="Are you sure?">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This will complete the action.<br/>Are you sure?</p>
</div>
<?php
/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once 'Controllers/InstallationController.php';
include_once 'Controllers/UserController.php';
include_once 'Controllers/ReportOverviewController.php';
$installationController = new InstallationController();
$userController = new UserController();
$reportOverviewController = new ReportOverviewController();

if ($_SESSION['level'] == '3') {
    $installationsArray = $installationController->
            getInstallationsWithConversationEntriesForAccountManager($_SESSION['user_id']);
} else {
    $installationsArray = $installationController->getInstallationsWithConversationEntries($_SESSION['user_id']);
}

if (isset($installationsArray)) {
    print "<div style='width:1200px;'>";
    print "<div class='categoryBlockDiv'>"
            . "<div class='categoryBlockHeader' id='rounded-corners-left-top-col' style='width:120px; padding-left:10px;'>Campaign</div>"
            . "<div class='categoryBlockHeader' style='width:200px'>Customer</div>"
            . "<div class='categoryBlockHeader' style='width:100px'>Country</div>"
            . "<div class='categoryBlockHeader' style='width:100px'>Action Type</div>"
            . "<div class='categoryBlockHeader' style='width:100px'>Deadline</div>"
            . "<div class='categoryBlockHeader' style='width:300px'>Comment</div>"
            . "<div class='categoryBlockHeader' style='width:140px'>Reporter</div>"
            . "<div class='categoryBlockHeader' id='rounded-corners-right-top-col' style='width:100px'>Close</div>"
            . "</div>";

    print "<div class='categoryBlockClearBoth'>&nbsp;</div>";

    foreach ($installationsArray as $key => $array) {

        print "<div class='row'>";
        print "<div  class='col' style='width:120px; padding-left:10px;'>" . $reportOverviewController->getSingleReportByReportOverviewID($array['conversation']->campaignOverviewID)->reportName . "</div>";
        print "<div  class='col' style='width:200px'><a href='index.php?portal=mr&page=report&action=showInstallation&customerID=" . $array['customer']->customerID . "'>" . $array['customer']->customerName . "<a/></div>";
        print "<div  class='col' style='width:100px'>" . $array['address']->country . "</div>";
        print "<div  class='col' style='width:100px'>" . $array['conversation']->actionType . "</div>";
        print "<div  class='col' style='width:100px'>" . date("d-m-Y", strtotime($array['conversation']->nextActionDueDate)) . "</div>";
        print "<div  class='col' style='width:300px'>" . substr($array['conversation']->content, 0, 100) . "...</div>";
        print "<div  class='col' style='width:140px'>" . $userController->getUserByID($array['history']->userID)->name . "</div>";
        print "<div  class='col' style='width:100px'><button onclick='closeAction(" . $array['conversation']->conversationID . ", " . $_SESSION['user_id'] . ");'>Completed</button></div>";
        print "</div>";


        print "<div class='categoryBlockClearBoth'>&nbsp;</div>";
    }
    print "<div class='row'>";
        print "<div  class='col' id='rounded-corners-left-bottom-col' style='width:120px; padding-left:10px;'>&nbsp;</div>";
        print "<div  class='col' style='width:200px'></div>";
        print "<div  class='col' style='width:100px'></div>";
        print "<div  class='col' style='width:100px'></div>";
        print "<div  class='col' style='width:100px'></div>";
        print "<div  class='col' style='width:300px'></div>";
        print "<div  class='col' style='width:140px'></div>";
        print "<div  class='col' id='rounded-corners-right-bottom-col' style='width:100px'></div>";
        print "</div>";
    print "</div>";
} else {
    print "No current actions to display.";
}