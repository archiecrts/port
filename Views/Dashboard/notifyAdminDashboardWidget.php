<h3>Admin Enquiries</h3>
<script>
    function updateStatus(statusID, status) {
        var server = '<?php print $_SESSION['server']; ?>';
        $.ajax({url: 'Views/Dashboard/updateNotifyEnquiry.php?id=' + statusID + '&status=' + status + '&server=' + server,
            success: function (output) {
                window.location.reload();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status + " " + thrownError);
            }});
    }
</script>
<?php
    
    if ($_SESSION['level'] == '1') {
        if (isset($_SESSION['maintenance'])) {
            print "<h3 class='red'>PORT IS LOCKED</h3>";
            print "<form name='maintenanceForm' action='index.php?portal=maintenance&action=unset' method='post' enctype='multipart/form-data'>";
            print "<input type='submit' name='submit' class='red' value='UNLOCK PORT from Maintenance'/>";
            print "</form>";
        } else {
            print "<form name='maintenanceForm' action='index.php?portal=maintenance&action=set' method='post' enctype='multipart/form-data'>";
            print "<input type='submit' name='submit' class='blue' value='LOCK PORT for Maintenance'/>";
            print "</form>";
        }
        
        
        
    }
    
    ?>
<?php
$statusArray = ['All', 'Open', 'Closed', 'On Hold', 'Resolved', 'Duplicate', 'Invalid', 'Wont Fix'];

?>
<ul id="notifyAdminInlineMenu">
    <?php
        $len = count($statusArray);
        foreach ($statusArray as $index => $val) {
            $border = "border";
            if( $index == $len - 1 ) {
                $border = "";
            }
            $css = "class='unselectedButton $border'";
            $imgSrc = './icons/whiteForwardButton.png';
            
            if (!isset($_GET['statusType'])) {
                $_GET['statusType'] = 'Open';
            }
            if ($val === $_GET['statusType']) {
                $css = "class='selectedButton $border'";
                $imgSrc = './icons/forward_button.png';
            }
            
            print '<li '.$css.'><a href="index.php?portal=dashboard&statusType='.$val.'">'.$val.'</a></li>';
        }//<img src="'.$imgSrc.'" height="15px"/>
    ?>
</ul>
<?php
/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

include_once 'Controllers/NotifyAdminController.php';
include_once 'Controllers/NotifyAdminCategoryController.php';
include_once 'Controllers/UserController.php';
$notifyAdminController = new NotifyAdminController();
$notifyAdminCategoryController = new NotifyAdminCategoryController();
$userController = new UserController();


$categories = $notifyAdminCategoryController->getAllCategories();
if (isset($categories)) {
    foreach ($categories as $key => $object) {
        print '<h4>' . $object->category . '</h4>';

        print "<div>";
        print "<div class='categoryBlockDiv'>"
                . "<div class='categoryBlockHeader'  id='rounded-corners-left-top-col' style='width:140px; padding-left:10px;'>Date</div>"
                . "<div class='categoryBlockHeader' style='width:400px'>Enquiry</div>"
                . "<div class='categoryBlockHeader' style='width:140px'>User</div>"
                . "<div class='categoryBlockHeader'  id='rounded-corners-right-top-col' style='width:100px'>Status</div></div>";

        print "<div class='categoryBlockClearBoth'>&nbsp;</div>";
        
        $statusType = 'open';
        if(isset($_GET['statusType'])) {
            $statusType = $_GET['statusType'];
        }
        
        $enquiries = $notifyAdminController->getAllEnquiriesByCategory($key, $statusType);
        if (isset($enquiries)) {
            foreach ($enquiries as $enq => $enqObject) {
                print "<div class='row'>";
                print "<div  class='col' style='width:140px; padding-left:10px;'>" . $enqObject->dateLogged . "</div>";
                print "<div  class='col' style='width:400px'>" . $enqObject->issueDescription . "</div>";
                $user = $userController->getUserByID($enqObject->userID);
                print "<div  class='col' style='width:140px'>" . $user->name . "</div>";

                // Choose which option is selected.
                $status = ['Open', 'Resolved', 'On Hold', 'Duplicate',
                    'Invalid', 'Wont Fix', 'Closed'];
                
                print "<div  class='col' style='width:100px'>"
                        . "<select name='resolve' onchange='updateStatus(" . $enqObject->notifyAdminID . ", this.value)'>";
                foreach ($status as $value) {
                    if ($enqObject->status === $value) {
                        print "<option value='$value' selected='selected'>$value</option>";
                    } else {
                        print "<option value='$value'>$value</option>";
                    }
                }

                print "</select></div>";
                print "</div>";
                print "<div class='categoryBlockClearBoth'>&nbsp;</div>";
            }
                
            print "</div>";
        }
        print "<div class='row'>";
                print "<div  class='col'  id='rounded-corners-left-bottom-col' style='width:140px; padding-left:10px;'>&nbsp;</div>";
                print "<div  class='col' style='width:400px'></div>";
                print "<div  class='col' style='width:140px'></div>";
                print "<div  class='col' id='rounded-corners-right-bottom-col' style='width:100px'></div>";
                print "</div>";
    }
}