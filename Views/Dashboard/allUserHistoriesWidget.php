<h3>User History Logs</h3>
<?php

/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

include_once 'Controllers/UserHistoryController.php';
include_once 'Controllers/UserController.php';
$userHistoryController = new UserHistoryController();
$userController = new UserController();

$startDate = date("Y-m-d H:i:s", strtotime("-2 Days"));
$endDate = date("Y-m-d H:i:s", time());

$userHistoryRecords = $userHistoryController->getAllUserHistoryBetweenDates($startDate, $endDate);

print "<p>Showing Dates: <strong>". $startDate . "</strong> to <strong>". $endDate. "</strong></p>";
if (isset($userHistoryRecords)) {
    print "<div style='width:1200px;'>";
    print "<div class='categoryBlockDiv'>"
            . "<div class='categoryBlockHeader' id='rounded-corners-left-top-col' style='width:180px; padding-left:10px;'>Date</div>"
            . "<div class='categoryBlockHeader' style='width:80px'>Type</div>"
            . "<div class='categoryBlockHeader' style='width:160px'>User</div>"
            . "<div class='categoryBlockHeader' id='rounded-corners-right-top-col' style='width:350px'>Comment</div>"
            . "</div>";

    print "<div class='categoryBlockClearBoth'>&nbsp;</div>";

    foreach ($userHistoryRecords as $key => $object) {

        print "<div class='row'>";
        $warning = "";
        if ($object->itemType == "failed login") {
            $warning = "color:red; font-weight: bold;";
        }
        print "<div  class='col' style='width:180px; padding-left:10px; ".$warning."'>" . $object->itemDate. "</div>";
        print "<div  class='col' style='width:80px; ".$warning."'>" . $object->itemType . "</div>";
        print "<div  class='col' style='width:160px; ".$warning."'>" . $userController->getUserByID($object->userID)->name . "</div>";
        print "<div  class='col' style='width:350px; ".$warning."'>" . $object->comment . "</div>";
        print "</div>";


        print "<div class='categoryBlockClearBoth'>&nbsp;</div>";
    }
    print "<div class='row'>";
        print "<div  class='col' id='rounded-corners-left-bottom-col' style='width:180px; padding-left:10px;'>&nbsp;</div>";
        print "<div  class='col' style='width:80px'></div>";
        print "<div  class='col' style='width:160px'></div>";
        print "<div  class='col' id='rounded-corners-right-bottom-col' style='width:350px'></div>";
        print "</div>";
    print "</div>";
} else {
    print "No current actions to display.";
}