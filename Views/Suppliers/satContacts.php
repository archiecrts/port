<div id='installationContainer'>
    <div id="installationHeader">
        
        <div id="officeLocationsHeader"><img src="./icons/PhoneIcon50x50.png" alt="" height='20px'/> Contacts:
            <img src="./icons/edit_pencil.png" alt="edit" height='15px'/> &nbsp; <a href="index.php?portal=suppliers&page=supplierRecord&action=add&customerID=<?php echo $customerID; ?>&tab=contacts">Add Contact</a>
        </div>
    </div>

&nbsp;
    <div id='installationContentWide'>
        <table style="width:100%;">
            <tr>
                <td></td>
                <td><strong>Office</strong></td>
                <td><strong>Name</strong></td>
                <td><strong>Job Title</strong></td>
                <td><strong>Email</strong></td>
                <td><strong>Number</strong></td>
                <td><strong>Mobile</strong></td>
                <td><strong>Inactive</strong></td>
                <td><strong>Validated</strong></td>
                <!--<td><strong>History</strong></td>-->
            </tr>
            <?php
                        $contactsArray = $customerContactsController->getContactsByCustomerID($customerID);
                        if (isset($contactsArray)) {
                            foreach ($contactsArray as $key => $value) {
                                print "<tr>";
                                print '<td><a href="index.php?portal=suppliers&page=supplierRecord&action=edit&customerID='.$customerID.'&tab=contacts&contactID='.$value->customerContactID.'">Edit</a></td>';
                                $address = $customerAddressController->getCustomerAddressByID($value->customerAddressID);
                                print "<td>$address->officeName</td>";
                                print "<td>$value->salutation $value->firstName $value->surname</td>
                                       <td>$value->jobTitle</td>
                                       <td>$value->email</td>
                                       <td>$value->number</td>
                                       <td>$value->mobile</td>
                                       <td>";
                                    if ($value->inactive == 0) {
                                        print "No";
                                    } else {
                                        print "Yes";
                                    }
                                    print "</td>
                                        <td>";
                                    if ($value->contactValidated == 1) {
                                        print "Yes";
                                    } else {
                                        print "No";
                                    }
                                    print "<!--</td>
                                        <td>View</td>-->
                                       </tr>";
                            }
                        }
            ?>
        </table>
    </div>
</div>
