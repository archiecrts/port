<?php

//include_once 'Controllers/PartManifestController.php';
//$partManifestController = new PartManifestController();

$versionID = "";
if (isset($_GET['versionID'])) {
    $versionID = $_GET['versionID'];
}

$manifest = $partManifestController->getManifestByVersionID($versionID);

$series = $seriesLookupController->getSeriesByVersionLookupID($versionID);
$typeOfProduct = $typeOfProductLookupController->getTypeOfProductByID($series->typeOfProductLookupID);
$designer = $designerLookupController->getDesignerByDesignerID($series->designerLookupID);
$version = $versionLookupController->getVersionIDBySeriesID($series->seriesID);
//print_r($version);                

print "<h3>Part Manifest for " . $designer->designerDescription . " : " . $series->seriesDescription . " : ";

foreach ($version as $id => $versionObject) {
    print $versionObject->versionProductDescription . " ";
}
print "</h3>";
print '<table style="width:100%;">
        <tr>
                <th>Part Code</th>
                <th class="colColoured">Description</th>
                <th>Weight</th>
                <th class="colColoured">Preferred Supplier</th>
                <th>Wear Item</th>
                <th class="colColoured">Dimensions</th>
                <th>Material Spec</th>
                <th class="colColoured">Colour</th>';
foreach ($version as $id => $versionObject) {
    print "<th>" . $versionObject->versionProductDescription . "<br/>QTY</th>";
}

print '</tr>';
foreach ($manifest as $key => $partObject) {

    $part = $partCodeController->getPartCodeByID($partObject->partCode);
    $partDescription = $partDescriptionLookupController->getDescriptionByID($part->partDescriptionLookupID);
    $dimensions = $partDimensionsController->getDimensionsByID($part->dimensionID);
    $materialSpec = $partMaterialSpecController->getMaterialSpecByID($part->materialID);
    $partColour = $partColourController->getPartColourByID($part->colourID);
    $partManifest = $partManifestController->getMainfestByPartID($partObject->partCode);
    

    print "<tr>";
    print "<td>" . $part->partCode . "</td>";
    print "<td class='colColoured'>$partDescription->majorType -> $partDescription->minorType </td>";
    print "<td style='text-align: center;'>$part->partWeight</td>";
    print "<td style='text-align: center;' class='colColoured'>" . ($part->partPreferredSupplierID == $_GET['customerID'] ? "Yes" : "No") . "</td>";
    print "<td style='text-align: center;'>$part->partWearItem</td>";
    print "<td class='colColoured'>$dimensions->dimOuterDiameter OD<br/> $dimensions->dimProfileDiameter PD<br/> $dimensions->dimLength Length</td>";
    print "<td style='text-align: center;'>$materialSpec->materialDescription</td>";
    print "<td style='text-align: center;' class='colColoured'>$partColour->partColour</td>";
    foreach ($version as $id => $versionObject) {
        
        print "<td>";
       
        foreach ($partManifest as $manID => $manifestObject) {
       
            if ($versionObject->versionID == $manifestObject->versionLookupID) {
          
                print $manifestObject->quantityOnEngine;
            } 
        }
        print "</td>";
    }
    print "<td>";
}
print '</table>';
