<h3>Suppliers By Product</h3>

<style>
    .approvedSupplier {
        font-weight: bold;
        color: green;
        background-color: greenyellow;
    }
</style>
<?php
require_once 'Helpers/charsetFunctions.php';
?>
<div id="installationsTable">
    <?php
    $masterHeaderArray = [
        "CUSTOMER NAME" => "SUPPLIER / COMPANY DATA",
        "TELEPHONE" => "SUPPLIER / COMPANY DATA",
        "TYPE" => "SUPPLIER / COMPANY DATA",
        "CITY" => "SUPPLIER / COMPANY DATA",
        "STATE" => "SUPPLIER / COMPANY DATA",
        "COUNTRY" => "SUPPLIER / COMPANY DATA",
        "AREA" => "SUPPLIER / COMPANY DATA",
        "CONTINENT" => "SUPPLIER / COMPANY DATA",
        "STATUS" => "APPROVAL",
        "ACCOUNT MANAGER" => "STDMG INTERNAL DATA",
        "PRODUCT MANUFACTURER" => "PRODUCT DATA",
        "PRODUCT SERIES" => "PRODUCT DATA",
        "CYLINDER COUNT & " => "ENGINES ONLY",
        "CYLINDER CONFIGURATION" => "ENGINES ONLY"
        ];

    /*
     * This below uses the above master header array to make a shortened header array.
     */



    $dataGroupsArray = array_unique($masterHeaderArray, SORT_REGULAR); // this makes the top row by getting unique entries from the above array.
    ?>
    <table class='borderedTable' id='reportTbl'>
        <thead>
            <tr>
                <!-- DATA GROUPS -->
                <?php
                $colspan = array_count_values($masterHeaderArray);

                $roundedCornerLeft = '';
                $isFirst = true;
                foreach ($dataGroupsArray as $value) {

                    if ($isFirst == true) {
                        $roundedCornerLeft = 'class="rounded-corners-left-top"';
                    } else {
                        $roundedCornerLeft = '';
                    }
                    print "<th colspan='" . $colspan[$value] . "' " . $roundedCornerLeft . ">" . $value . "</th>";
                    $isFirst = false;
                }
                ?>
                <th class="rounded-corners-right-top">&nbsp;</th>
            </tr>
            <!-- / END DATA GROUPS -->
            <tr class="header-border">
                <?php
                /*
                 * This is the second header row.
                 */
                foreach ($masterHeaderArray as $key => $value) {
                    print "<th>" . $key . "</th>";
                }
                ?>
                <th></th>
            </tr>
            <!-- END SECOND HEADER ROW -->
        </thead>
        <!-- DISPLAY RECORDS -->
        <tbody>
            <?php
            if (isset($customerArray)) {
                
                foreach ($customerArray as $recordKey => $supplierPart) {
                    
                    $customer = $customerController->getCustomerByID($supplierPart['customer']->customerID);
                    $customerAddress = $customerAddressController->getDefaultCustomerAddressByCustomerID($supplierPart['customer']->customerID);
                    $country = $countryController->getTimeZoneByCountryName($customerAddress->country);
                    $customerTypeArray = $customerStatusController->getAllCustomerStatusValuesByCustomerID($supplierPart['customer']->customerID);
                    $supplierApproval = $supplierApprovalController->getSupplierApprovalBySupplierID($supplierPart['customer']->customerID);
                    $approvalLookup = $supplierApprovalStatusLookupController->getSupplierApprovalLookupByID($supplierApproval->approvalStatusID);
                    $versionLookup = $supplierPart['version'];


                    // account manager
                    $accountManager = "";
                    if ($customer->accountManager != '') {
                        $accountManager = $userController->getUserByID($customer->accountManager)->name;
                    } else {
                        if ($customerAddress->country !== "") {
                            $territoryAccountManager = $userTerritoryController->getUserIDByTerritory($customerAddress->country);
                            $accountManager = $userController->getUserByID($territoryAccountManager)->name;
                        }
                        
                    }

                    print "<tr>";
                    print "<td><a href='index.php?portal=suppliers&page=supplierRecord&action=view&customerID=".$supplierPart['customer']->customerID."' target='_blank'>" . $customer->customerName . "</a></td>";
                    print "<td>" . $customer->mainTelephone . "</td>";
                    print "<td>"; 
                    foreach ($customerTypeArray as $typeID => $type) {
                        print $customerStatusLookupController->getCustomerStatusLookupByID($type->custStatusLookupID)->custStatusDescription;
                        print "<br/>";
                    }
                            
                    print "</td>";
                    print "<td>" . $customerAddress->city . "</td>";
                    print "<td>" . $customerAddress->state . "</td>";
                    print "<td>" . $customerAddress->country . "</td>";
                    print "<td>" . $country->geoArea . "</td>";
                    print "<td>" . $country->continent . "</td>";
                    print "<td>$approvalLookup->approvalStatus</td>";
                    print "<td>" . $accountManager . "</td>";
                    // product
                    print "<td>".$designerLookupController->getDesignerByVersionLookupID($versionLookup->versionID)->designerDescription."</td>";
                    print "<td>".$seriesLookupController->getSeriesByID($versionLookup->seriesLookupID)->seriesDescription."</td>";
                    $version = $versionLookupController->getVersionIDBySeriesID($versionLookup->seriesLookupID);
                    print "<td colspan='2'>";
                   // print "<td>$versionLookup->cylinderCount</td>";
                   // print "<td>$versionLookup->cylinderConfiguration</td>";
                    foreach ($version as $id => $versionObject) {
                        print "".$versionObject->versionProductDescription . ", ";
                    }
                    print "</td></tr>";
                }
            } else {
                print "<tr><td><span class='red'>No Records to display</span></td></tr>";
            }
            ?>

    </table>
        
</div>