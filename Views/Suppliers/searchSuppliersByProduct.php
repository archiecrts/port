<h3>Search supplier by product details</h3>
<script type="text/javascript" src="./js/campaignParametersForm.js"></script> 
<form name="form2" action="index.php?portal=suppliers&page=supplierTableProduct&action=view" method="POST" enctype="multipart/form-data">

    <table>
        <tr>
            <td>Type of Product</td>
            <td> 
                <select name="typeProduct">
                    <option value="all">All</option>
                    <?php
                        $topList = $typeOfProductLookupController->getAllProductTypes();
                        foreach ($topList as $topID => $top) {
                            print '<option value="'.$top->toplProductDescription.'">'.$top->toplProductDescription.'</option>';
                        }
                    ?>
                    
                </select>
            </td>
        </tr>
        <tr>
            <td style="vertical-align:top;">Manufacturer / Product Name</td>
            <td><select name="allProducts"  size="6" class='multiSelect' id='select1'>
                    <option value="all" selected="selected">ALL</option>
                                <?php
                                $engineManufacturers = $designerLookupController->getDistinctDesigners();

                                foreach ($engineManufacturers as $name => $engine) {
                                    if ($engine->designerDescription != "") {
                                        echo '<option value="' . htmlXentities($engine->designerID) . '">' . htmlXentities($engine->designerDescription) . '</option>';
                                    }
                                }
                                ?>      

                            </select>
                
                </div>
            </td>
        </tr>
        <tr>
            <td style="vertical-align:top;">Series</td>
            <td><select name="allSeries"  size="6" class='multiSelect' id='model1'>
                    <option value="all" selected="selected">ALL</option>
                                <?php
                                $engineSeries = $seriesLookupController->getDistinctModelSeries();
                                if (isset($engineSeries)) {
                                    foreach ($engineSeries as $name => $series) {
                                        if ($series->seriesDescription != "") {
                                            echo '<option value="' . htmlXentities($series->seriesID) . '">' . htmlXentities($series->seriesDescription) . '</option>';
                                        }
                                    }
                                }
                                ?>      

                            </select>
                
            </td>
        </tr>
        <tr>
            <td>Number of Cylinders</td>
            <td><input type="text" name="cylinderCount"/></td>
        </tr>
        <tr>
            <td>Cylinders Configuration</td>
            <td><select name="cylinderConfiguration">
                    <option value=""></option>
                    <option value="L">L</option>
                    <option value="V">V</option>
                </select>
            </td>
        </tr>
    </table>

    <input type="submit" name="submit" value="Go"/>
</form>