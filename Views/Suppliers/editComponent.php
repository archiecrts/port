<script>
// autocomplet : this function will be executed every time we change the text
    function autocompletePartCode() {
        var min_length = 1; // min caracters to display the autocomplete
        var keyword = $('#searchBoxOne').val();
        if (keyword.length >= min_length) {
            $.ajax({
                url: './Views/AutoCompleteSearch/autoCompleteSQLPartCode.php',
                type: 'POST',
                data: {keyword: keyword},
                success: function (data) {
                    $('#searchListOne').show();
                    $('#searchListOne').html(data);
                }
            });
        } else {
            $('#searchListOne').hide();
        }
    }

// set_item : this function will be executed when we select an item
    function set_part(item) {
        // change input value
        $('#searchBoxOne').val(item);
        // hide proposition list
        $('#searchListOne').hide();
    }



    // managing company is the customer
    function set_partcode_id(id) {
        // change the hidden value
        $('#partCodeID').val(id);
    }

    $(document).on('keyup keypress', 'form input[type="text"]', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            return false;
        }
    });
</script>

<style>

    #searchListOne {
        display: none;
    }
</style>


<h3>Components</h3>
<form name="form1" action="index.php?portal=suppliers&page=supplierRecord&action=save&customerID=1&tab=component" method="post" enctype="multipart/form-data">

    <?php
    /*
     * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
     * Author: Archie Curtis
     */

// parts list to search.
    ?>
    
    <div>
        <input type="text" id="searchBoxOne" onkeyup="autocompletePartCode()"  autocomplete="off" placeholder="Type a part code or description">
        <input type="hidden" id="partCodeID" name="partCodeID" value=""/>
        <input type="submit" name="submit" value="Save"/>
        <input type="submit" name="submitAddMore" value="Save and add more"/>
        <ul id="searchListOne" multiple size="15"></ul>
    </div>
    
</form>
