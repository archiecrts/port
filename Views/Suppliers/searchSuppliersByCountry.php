<?php
include_once('Controllers/CountryController.php');
$countryController = new CountryController();
?>
<script type="text/javascript" src="./js/campaignParametersForm.js"></script> 
<h3>Search supplier by country</h3>
<form name="form1" action="index.php?portal=suppliers&page=supplierTableCountry&action=view" method="post" enctype="multipart/form-data">
<table>
    <tr>
            <td style="vertical-align:top;">Country</td>
            <td>
                <Table>
                    <tr>
                        <td><select name="allCountries[]" multiple size="6" class='multiSelect' id='selectCountry1'>
                                <?php
                                $countryArray = $countryController->getCountries();

                                foreach ($countryArray as $countryName => $country) {
                                    if ($country != "") {
                                        echo '<option value="' . $country->countryName . '">' . $country->countryName . '</option>';
                                    }
                                }
                                ?>      

                            </select>
                        </td>
                        <td><div class='multiSelectButtons'>
                                <table>
                                    <tr>
                                        <td><button id="addCountry" type="button"> > </button></td>
                                    </tr>
                                    <tr>
                                        <td><button id="removeCountry" type="button"> < </button></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td><select name="country[]" multiple size='6' class='multiSelect' id="selectCountry2">

                            </select></td>
                    </tr>
                </Table>



                </div>
            </td>
        </tr>
    
        <tr>
            <td style="vertical-align:top;">Area</td>
            <td>
                <Table>
                    <tr>
                        <td><select name="allAreas[]" multiple size="6" class='multiSelect' id='selectArea1'>
                                <?php
                                $areaArray = $countryController->getDistinctAreas();

                                foreach ($areaArray as $id => $area) {
                                    if ($area != "") {
                                        echo '<option value="' . htmlXentities($area) . '">' . htmlXentities($area) . '</option>';
                                    }
                                }
                                ?>      

                            </select>
                        </td>
                        <td><div class='multiSelectButtons'>
                                <table>
                                    <tr>
                                        <td><button id="addArea" type="button"> > </button></td>
                                    </tr>
                                    <tr>
                                        <td><button id="removeArea" type="button"> < </button></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td><select name="area[]" multiple size='6' class='multiSelect' id="selectArea2">

                            </select></td>
                    </tr>
                </Table>

                </div>
            </td>
        </tr>
        <tr>
            <td style="vertical-align:top;" >Continent</td>
            <td>
                <Table>
                    <tr>
                        <td><select name="allContinents[]" multiple size="6" class='multiSelect' id='selectContinent1'>
                                <?php
                                $continentArray = $countryController->getDistinctContinents();

                                foreach ($continentArray as $id => $continent) {
                                    if ($continent != "") {
                                        echo '<option value="' . htmlXentities($continent) . '">' . htmlXentities($continent) . '</option>';
                                    }
                                }
                                ?>      

                            </select>
                        </td>
                        <td><div class='multiSelectButtons'>
                                <table>
                                    <tr>
                                        <td><button id="addContinent" type="button"> > </button></td>
                                    </tr>
                                    <tr>
                                        <td><button id="removeContinent" type="button"> < </button></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td><select name="continent[]" multiple size='6' class='multiSelect' id="selectContinent2">

                            </select></td>
                    </tr>
                </Table>



                </div>
            </td>
        </tr>
</table>
    <input type="submit" name="submit" value="Go"/>
</form>