<style>
    .colColoured {
        background: lightblue;
    }
    .colListColoured {
        background: lightgray;
    }
</style>
<div id='installationContainer'>
    <div id="installationHeader">

        <div id="officeLocationsHeader"><img src="./icons/technicalDataIcon.png" alt="" height='20px'/> Components:
            <img src="./icons/edit_pencil.png" alt="edit" height='15px'/> &nbsp; <a href="index.php?portal=suppliers&page=supplierRecord&action=add&customerID=<?php echo $customerID; ?>&tab=component">Add Component</a>
        </div>
    </div>

    &nbsp;
    <div id='installationContentWide'>
        <table style="width:100%;">
            <tr>
                <th colspan="8">Component</th>
                <th colspan="3">In Product Manifest</th>
            </tr>
            <tr>
                <th>Part Code</th>
                <th class="colColoured">Description</th>
                <th>Weight</th>
                <th class="colColoured">Preferred Supplier</th>
                <th>Wear Item</th>
                <th class="colColoured">Dimensions</th>
                <th>Material Spec</th>
                <th class="colColoured">Colour</th>
                <th class='colListColoured'>Designer</th>
                <th>Series</th>
                <th class='colListColoured'>Version</th>
                <th>Type of Product</th>
            </tr>
            <?php
            $supplierPartsArray = $supplierPartsController->getPartsBySupplierID($customerID);

            if (isset($supplierPartsArray)) {
                foreach ($supplierPartsArray as $key => $value) {
                    $part = $partCodeController->getPartCodeByID($value->partCodeID);
                    $partDescription = $partDescriptionLookupController->getDescriptionByID($part->partDescriptionLookupID);
                    $dimensions = $partDimensionsController->getDimensionsByID($part->dimensionID);
                    $materialSpec = $partMaterialSpecController->getMaterialSpecByID($part->materialID);
                    $partColour = $partColourController->getPartColourByID($part->colourID);
                    $partManifest = $partManifestController->getMainfestByPartID($value->partCodeID);



                    print "<tr style='vertical-align: top;'>";
                    print "<td>" . $part->partCode . "</td>";
                    print "<td class='colColoured'>$partDescription->majorType -> $partDescription->minorType </td>";
                    print "<td style='text-align: center;'>$part->partWeight</td>";
                    print "<td style='text-align: center;' class='colColoured'>" . ($part->partPreferredSupplierID == $customerID ? "Yes" : "No") . "</td>";
                    print "<td style='text-align: center;'>$part->partWearItem</td>";
                    print "<td class='colColoured'>$dimensions->dimOuterDiameter OD<br/> $dimensions->dimProfileDiameter PD<br/> $dimensions->dimLength Length</td>";
                    print "<td style='text-align: center;'>$materialSpec->materialDescription</td>";
                    print "<td style='text-align: center;' class='colColoured'>$partColour->partColour</td>";
                    print "<td  class='colListColoured'>";
                    // Part can exist on several product types so all possible types will be shown on each row.
                    if (isset($partManifest)) {
                        $designerArray = [];
                        

                        foreach ($partManifest as $arr => $manifest) {
                            $series = $seriesLookupController->getSeriesByVersionLookupID($manifest->versionLookupID);
                            $designer = $designerLookupController->getDesignerByDesignerID($series->designerLookupID)->designerDescription;
                           
                            if (!in_array($designer, $designerArray)) {
                                array_push($designerArray, $designer);
                            }
                            
                        }
                        foreach ($designerArray as $designerDesc) {
                            print $designerDesc . "<br/><br/>";
                        }
                    }
                    print "</td><td>";
                    if (isset($partManifest)) {
                        $seriesArray = [];
                        foreach ($partManifest as $arr => $manifest) {
                            
                            $series = $seriesLookupController->getSeriesByVersionLookupID($manifest->versionLookupID)->seriesDescription;
                           // print $series;
                            if (!in_array($series, $seriesArray)) {
                                array_push($seriesArray, $series);
                            }
                        }
                        foreach ($seriesArray as $seriesDesc) {
                            print $seriesDesc . "<br/><br/>";
                        }
                    }
                    
                    print "</td><td class='colListColoured'>";
                    
                    if (isset($partManifest)) {
                        $versionArray = [];
                        foreach ($partManifest as $arr => $manifest) {
                            $series = $seriesLookupController->getSeriesByVersionLookupID($manifest->versionLookupID);
                            $versionArray[$series->seriesID] = [];
                            $version = $versionLookupController->getVersionIDBySeriesID($series->seriesID);
                            //print_r($series->seriesID);
                            foreach ($version as $verID => $v) {
                                if (!in_array($v->versionProductDescription, $versionArray[$series->seriesID])) {
                                    array_push($versionArray[$series->seriesID], $v->versionProductDescription);
                                }
                            }
                            
                        }
                        foreach ($versionArray as $seriesID => $versionArr) {
                            foreach ($versionArr as $id => $versionDesc) {
                                print $versionDesc . ", ";
                            }
                            print "<br/><br/>";
                        }
                    }
                    print "</td><td>";
                    if (isset($partManifest)) {
                        $topArray = [];
                        foreach ($partManifest as $arr => $manifest) {
                            $series = $seriesLookupController->getSeriesByVersionLookupID($manifest->versionLookupID);
                            $typeOfProduct = $typeOfProductLookupController->getTypeOfProductByID($series->typeOfProductLookupID)->toplProductDescription;

                            if (!in_array($typeOfProduct, $topArray)) {
                                array_push($topArray, $typeOfProduct);
                            }
                            //print $typeOfProduct->toplProductDescription . "<br/><br/>";
                        }
                        foreach ($topArray as $topDesc) {
                            print $topDesc . "<br/><br/>";
                        }
                    }
                    print "</td></tr>";
                }
            }
            ?>
        </table>
    </div>
</div>