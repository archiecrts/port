<script>
    $(function () {
        var scrolled=0;
        $(".selectorDropDownBar").click(function () {
            var blockID = $(this).attr('id');



            var block = blockID.split('_');
            if (block[0] === "address") {
                if ($('#customerAddressBlock_' + block[1]).css('display') == 'none')
                {
                    $('#customerAddressBlock_' + block[1]).removeClass('hiddenBlock').addClass('showBlock');
                } else {
                    $('#customerAddressBlock_' + block[1]).removeClass('showBlock').addClass('hiddenBlock');
                }

            }
            if (block[0] === "technical") {
                if ($('#technicalDataBlock_' + block[1]).css('display') == 'none'){
                    $('#technicalDataBlock_' + block[1]).removeClass('hiddenBlock').addClass('showBlock');
                    
                } else {
                    $('#technicalDataBlock_' + block[1]).removeClass('showBlock').addClass('hiddenBlock');
                    
                }
            }
        });
    });

</script>
<div id="installationSubHeader">
    <h3><?php
        if ($customer->customerName == "") {
            print "Unknown Customer";
        } else {
            print $customer->customerName;
        }
        ?></h3>
</div>



<table style="width:100%;">
    <tr>
        <th colspan="3">
            <img src="./icons/crm_grey.png" alt="" height="20px"/>
            Customer Information</th>
        <th style="text-align: right;"><img src="./icons/edit_pencil.png" alt="edit" height='15px'/> <a href="index.php?portal=suppliers&page=supplierRecord&action=edit&tab=profile&customerID=<?php echo $customerID; ?>">Edit</a></th>
    </tr>
    <tr>
        <td class="title">Name</td>
        <td><?php
            if ($customer->customerName == "") {
                print "Unknown Customer";
            } else {
                print $customer->customerName;
            }
            ?>
        </td>
        <td class="title">Main Fax</td>
        <td><?php print $customer->mainFax; ?></td>
    </tr>
    <tr>
        <td class="title">Account Type(s)</td>
        <td><?php
        
            $custStatusArray = $customerStatusController->getAllCustomerStatusValuesByCustomerID($customer->customerID);
            
            if (isset($custStatusArray)) {
                foreach ($custStatusArray as $key => $status) {
                    print $customerStatusLookupController->getCustomerStatusLookupByID($status->custStatusLookupID)->custStatusDescription;
                
                    if ($status->custStatusLookupID == "1") {
                        print " <a href='index.php?portal=mr&page=report&action=showInstallation&installationID=&customerID=".$customer->customerID."&submit=Go'>View</a>";
                    }
                    
                    print "<br/>";
                }
            }
            ?></td>
        <td class="title">Account Manager</td>
        <td>
            <?php
            if ($customer->accountManager != '') {
                echo $userController->getUserByID($customer->accountManager)->name;
            } else {
                echo "Set by territory";
            }
            ?>
        </td>
    </tr>
    <tr>
        <td class="title">Website</td>
        <td><a href="
            <?php
            if (substr($customer->website, 0, 7) == "http://") {
                print $customer->website;
            } else {
                print "http://" . $customer->website;
            }
            ?>" target="_blank"><?php print $customer->website; ?></a></td>
        <td class="title">Territory Assigned to</td>
        <td><?php
        
            $defaultAddressCountry = $customerAddressController->getDefaultCustomerAddressByCustomerID($customer->customerID);
            
            $territoryAccountManager = $userTerritoryController->getUserIDByTerritory($defaultAddressCountry->country);
            
            echo $userController->getUserByID($territoryAccountManager)->name;
            ?>
        </td>
    </tr>
    <tr>
        <td class="title">Main Telephone</td>
        <td><?php print $customer->mainTelephone; ?></td>
        <td class="title">Header Notes</td>
        <td><?php echo $customer->headerNotes; ?></td>
    </tr>

</table>


<div id="officeLocations">
    <div id="officeLocationsHeader"><img src="./icons/office.png" alt="" height='20px'/> Office Locations:
        <img src="./icons/edit_pencil.png" alt="edit" height='15px'/> &nbsp; <a href="index.php?portal=suppliers&page=supplierRecord&action=add&customerID=<?php echo $customerID; ?>&tab=address">Add Address</a>
    </div>
    <?php
    if (isset($customerAddresses)) {
        foreach ($customerAddresses as $key => $address) {
            ?>
            <div id="address_<?php echo $address->addressID; ?>" class="selectorDropDownBar">
                <?php
                echo $address->city . ", " . $address->country;
                if ($address->defaultAddress === '1') {
                    print " (default address)";
                }
                ?>
            </div>
            <div id="customerAddressBlock_<?php echo $address->addressID; ?>" class="hiddenBlock">
                <?php
                print '<table>
                    <tr>
                        <td rowspan="7"><img src="./icons/office.png" alt="edit" height="40px"/></td>
                        <td colspan="4"><a href="index.php?portal=suppliers&page=supplierRecord&action=edit&customerID=' . $customerID . '&tab=address&addressID=' . $address->addressID . '">Edit</a></td>
                    </tr>
                    <tr>
                        <td class="title">Office Name</td>
                        <td>' . $address->officeName . '</td>
                        <td class="title">Area</td>
                        <td>' . $countryController->getTimeZoneByCountryName($address->country)->geoArea . '</td>    
                    </tr>
                    <tr>
                        <td class="title">Address</td>
                        <td>' . $address->address . '</td>
                        <td class="title">Continent</td>
                        <td>' . $countryController->getTimeZoneByCountryName($address->country)->continent . '</td>
                    </tr>
                    <tr>
                        <td class="title">City</td>
                        <td>' . $address->city . '</td>
                        <td class="title">Default Address</td>
                        <td>';
                if ($address->defaultAddress === '1') {
                    print "Yes";
                } else {
                    print "No";
                }
                print '</td>
                    </tr>
                        <td class="title">State</td>
                        <td>' . $address->state . '</td>
                        <td class="title">Address Verified</td>
                                <td>';
                if ($address->addressVerified === '1') {
                    print "Yes";
                } else {
                    print "No";
                }
                print '</td>
                    </tr>
                    <tr>
                        <td class="title">Post Code</td>
                        <td>' . $address->postcode . '</td>
                        <td class="title">Address Archived</td>
                                <td>';
                if ($address->archived === '1') {
                    print "Yes";
                } else {
                    print "No";
                }
                print '</td>
                    </tr>
                    <tr>
                        <td class="title">Country</td>
                        <td>' . $address->country . '</td>
                        <td colspan="2"></td>
                    </tr>
                            
                        </table></div>';
            }
        }
        ?>

    </div>
