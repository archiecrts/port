<div id='installationContainer'>
    <div id="installationHeader">

        <div id="officeLocationsHeader"><img src="./icons/technicalDataIcon.png" alt="" height='20px'/> Products Supplied For (Click to see current parts manifest in new tab):
           <!-- <img src="./icons/edit_pencil.png" alt="edit" height='15px'/> &nbsp; <a href="index.php?portal=suppliers&page=supplierRecord&action=add&customerID=<?php echo $customerID; ?>&tab=product">Add Product</a>-->
        </div>
    </div>

    &nbsp;
    <div id='installationContentWide'>
        <table>
            <tr>
                <th>Designer</th>
                <th>Series</th>
                <th>Version</th>
                <th>Type of Product</th>
                <th>Parts Manifest</th>
            </tr>
            <?php
            $series = $seriesLookupController->getDistinctSeriesByCustomerID($customerID);
            if (isset($series)) {
            foreach ($series as $sid => $seriesObject) {
                $typeOfProduct = $typeOfProductLookupController->getTypeOfProductByID($seriesObject->typeOfProductLookupID);
                $designer = $designerLookupController->getDesignerByDesignerID($seriesObject->designerLookupID);
                $version = $versionLookupController->getVersionIDBySeriesID($seriesObject->seriesID);

                print "<tr><td>" . $designer->designerDescription . "</td>";
                print "<td>" . $seriesObject->seriesDescription . "</td>";
                print "<td>";
                $versionID = '';
                foreach ($version as $id => $versionObject) {
                    print $versionObject->versionProductDescription . " ";
                    $versionID = $id;
                }
                print "</td>";
                print "<td>" . $typeOfProduct->toplProductDescription . "</td>";
                print "<td><a href='index.php?portal=suppliers&page=manifest&customerID=$customerID&versionID=$versionID' target='_blank'>Parts Manifest</a></td>";
                print "</tr>";
                
                }
            }
            ?>
        </table>
    </div>
</div>