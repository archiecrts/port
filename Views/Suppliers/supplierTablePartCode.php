<h3>Suppliers By Part Code <?php echo $partCodeController->getPartCodeByID($_GET['partCodeID'])->partCode; 
    echo " - ". $partDescriptionLookupController->getDescriptionByID($partCodeController->getPartCodeByID($_GET['partCodeID'])->partDescriptionLookupID)->majorType;
    echo " - ".$partDescriptionLookupController->getDescriptionByID($partCodeController->getPartCodeByID($_GET['partCodeID'])->partDescriptionLookupID)->minorType;
    
?>
</h3>
<?php
$part = $partCodeController->getPartCodeByID($_GET['partCodeID']);
    $colour = $partColourController->getPartColourByID($part->colourID);
    $dimension = $partDimensionsController->getDimensionsByID($part->dimensionID);
    $material = $partMaterialSpecController->getMaterialSpecByID ($part->materialID);
    print "<p>Colour: $colour->partColour <br/>";
    print "OD: $dimension->dimOuterDiameter  <br/>Profile: $dimension->dimProfileDiameter  <br/>Length: $dimension->dimLength  <br/>DIN: $dimension->dimDINNumber <br/>Head Type: $dimension->dimHeadType <br/>Angle (deg): $dimension->dimAngleInDegrees<br/>";
    print "Material: $material->materialDescription</p>";

?>
<style>
    .approvedSupplier {
        font-weight: bold;
        color: green;
        background-color: greenyellow;
    }
</style>
<?php
require_once 'Helpers/charsetFunctions.php';
?>
<div id="installationsTable">
    <?php
    $masterHeaderArray = [
        "CUSTOMER NAME" => "SUPPLIER / COMPANY DATA",
        "CITY" => "SUPPLIER / COMPANY DATA",
        "STATE" => "SUPPLIER / COMPANY DATA",
        "COUNTRY" => "SUPPLIER / COMPANY DATA",
        "AREA" => "SUPPLIER / COMPANY DATA",
        "CONTINENT" => "SUPPLIER / COMPANY DATA",
        "STATUS" => "APPROVAL",
        "PREFERRED SUPPLIER" => "APPROVAL",
        "ACCOUNT MANAGER" => "STDMG INTERNAL DATA",
        "STOCK LEVEL" => "PART DETAILS",
        "PART PRICE" => "PART DETAILS",
        "DATE PRICE SET" => "PART DETAILS",
        "LEAD TIME" => "PART DETAILS"];
    /*,
        "COLOUR" => "PART DETAILS",
        "DIMENSIONS" => "PART DETAILS",
        "MATERIAL" => "PART DETAILS"
*/
    /*
     * This below uses the above master header array to make a shortened header array.
     */



    $dataGroupsArray = array_unique($masterHeaderArray, SORT_REGULAR); // this makes the top row by getting unique entries from the above array.
    ?>
    <table class='borderedTable' id='reportTbl'>
        <thead>
            <tr>
                <!-- DATA GROUPS -->
                <?php
                $colspan = array_count_values($masterHeaderArray);

                $roundedCornerLeft = '';
                $isFirst = true;
                foreach ($dataGroupsArray as $value) {

                    if ($isFirst == true) {
                        $roundedCornerLeft = 'class="rounded-corners-left-top"';
                    } else {
                        $roundedCornerLeft = '';
                    }
                    print "<th colspan='" . $colspan[$value] . "' " . $roundedCornerLeft . ">" . $value . "</th>";
                    $isFirst = false;
                }
                ?>
                <th class="rounded-corners-right-top">&nbsp;</th>
            </tr>
            <!-- / END DATA GROUPS -->
            <tr class="header-border">
                <?php
                /*
                 * This is the second header row.
                 */
                foreach ($masterHeaderArray as $key => $value) {
                    print "<th>" . $key . "</th>";
                }
                ?>
                <th></th>
            </tr>
            <!-- END SECOND HEADER ROW -->
        </thead>
        <!-- DISPLAY RECORDS -->
        <tbody>
            <?php
            if (isset($partCodeRecords)) {

                foreach ($partCodeRecords as $recordKey => $supplierPart) {
                    $customer = $customerController->getCustomerByID($supplierPart->customerID);
                    $customerAddress = $customerAddressController->getDefaultCustomerAddressByCustomerID($supplierPart->customerID);
                    $country = $countryController->getTimeZoneByCountryName($customerAddress->country);
                    $part = $partCodeController->getPartCodeByID($_GET['partCodeID']);
                    $supplierApproval = $supplierApprovalController->getSupplierApprovalBySupplierID($supplierPart->customerID);
                    $approvalLookup = $supplierApprovalStatusLookupController->getSupplierApprovalLookupByID($supplierApproval->approvalStatusID);
                    //$colour = $partColourController->getPartColourByID($part->colourID);
                    //$dimension = $partDimensionsController->getDimensionsByID($part->dimensionID);
                    //$material = $partMaterialSpecController->getMaterialSpecByID ($part->materialID);
                    // account manager
                    $accountManager = "";
                    if ($customer->accountManager != '') {
                        $accountManager = $userController->getUserByID($customer->accountManager)->name;
                    } else {
                        if ($customerAddress->country !== "") {
                            $territoryAccountManager = $userTerritoryController->getUserIDByTerritory($customerAddress->country);
                            $accountManager = $userController->getUserByID($territoryAccountManager)->name;
                        }
                        
                    }

                    print "<tr ".(($part->partPreferredSupplierID == $supplierPart->customerID) ? "class='approvedSupplier'" : "").">";
                    //print_r($supplierPart);
                    print "<td><a href='index.php?portal=suppliers&page=supplierRecord&action=view&customerID=".$supplierPart->customerID."' target='_blank'>" . $customer->customerName . "</a></td>";
                    print "<td>" . $customerAddress->city . "</td>";
                    print "<td>" . $customerAddress->state . "</td>";
                    print "<td>" . $customerAddress->country . "</td>";
                    print "<td>" . $country->geoArea . "</td>";
                    print "<td>" . $country->continent . "</td>";
                    print "<td>$approvalLookup->approvalStatus</td>";
                    print "<td>" . (($part->partPreferredSupplierID == $supplierPart->customerID) ? "Yes" : "No") . "</td>";
                    print "<td>" . $accountManager . "</td>";
                    print "<td>$supplierPart->stockLevel</td>";
                    print "<td>$supplierPart->partPrice</td>";
                    print "<td>$supplierPart->datePriceSet</td>";
                    print "<td>$supplierPart->leadTime</td>";
                   // print "<td>$colour->partColour</td>";
                   // print "<td>OD: $dimension->dimOuterDiameter  <br/>Profile: $dimension->dimProfileDiameter  <br/>Length: $dimension->dimLength  <br/>DIN: $dimension->dimDINNumber <br/>Head Type: $dimension->dimHeadType <br/>Angle (deg): $dimension->dimAngleInDegrees</td>";
                   // print "<td>$material->materialDescription</td>";
                    print "</tr>";
                }
            } else {
                print "<tr><td><span class='red'>No Records to display</span></td></tr>";
            }
            ?>

    </table>

</div>