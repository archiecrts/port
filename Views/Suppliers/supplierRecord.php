<h3>Supplier Details</h3>
<?php
/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

// supplier details // address
include ("./Views/Suppliers/satProfile.php");

// contacts
include ("./Views/Suppliers/satContacts.php");

// components // other suppliers league table
include ("./Views/Suppliers/satComponents.php");

// products // other suppliers league table
include ("./Views/Suppliers/satProducts.php");
