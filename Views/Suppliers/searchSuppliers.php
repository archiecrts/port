<script>
    $(function () {

        $('#searchBy').on('change', function() {
           
         
            $('.byDiv').hide();
            $('#'+this.value).show();
        });

    });
</script>
    

<h3>Search Suppliers</h3>
<p>Search by...
<select name="searchBy" id="searchBy">
    <option value="byName">Supplier Name</option>
    <option value="byPartCode">Part Code / Part Description</option>
    <option value="byCountry">Country</option>
    <option value="byEngine">Product Details (Main product designer)</option>
</select></p>
<div id="byName" class="byDiv">
    <?php
    include './Views/AutoCompleteSearch/autoCompleteSearchSuppliersByName.php';
    ?>
</div>
<div id="byPartCode" class="byDiv" style="display:none;">
    <?php
    include './Views/AutoCompleteSearch/autoCompleteSearchSuppliersByPartCode.php';
    ?>
</div>
<div id="byCountry" class="byDiv" style="display:none;">
    <?php
    include './Views/Suppliers/searchSuppliersByCountry.php';
    ?>
</div>
<div id="byEngine" class="byDiv" style="display:none;">
    <?php
    include './Views/Suppliers/searchSuppliersByProduct.php';
    ?>
</div

