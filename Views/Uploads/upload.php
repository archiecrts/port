<h3>Upload</h3>

<?php
include_once('Controllers/InstallationController.php');
include_once('Controllers/InsertInstallationsFromCSVController.php');
include_once('Controllers/CustomerContactsController.php');
include_once('Controllers/CustomerController.php');
include_once('Controllers/ManualImportController.php');
$installationController = new InstallationController();
$insertInstallationsFromCSVController = new InsertInstallationsFromCSVController();
$customerContactsController = new CustomerContactsController();
$customerController = new CustomerController();
$manualImportController = new ManualImportController();

if (isset($_GET['action'])) {
    $action = $_GET['action'];
} else {
    $action = "";
}
switch ($action) {
    case "verify":
        print "Verify Column Data";

        // load the file into the upload directory and read the first line to get
        // any column headers.
        // Check that file is .csv
        $fileExt = explode('.', $_FILES["file"]["name"]);

        if ($fileExt[count($fileExt) - 1] != 'csv') {
            echo "<p span='red'>Sorry, you must use a CSV</p>";
        } else {

            // Check the file for errors, then save into the upload directory
            $name = '';
            if ($_FILES["file"]["error"] == 0) {
                $tmp_name = $_FILES["file"]["tmp_name"];
                $name = $_FILES["file"]["name"];
                move_uploaded_file($tmp_name, "upload/$name");
            }

            $initialfileName = 'upload/' . $_FILES['file']['name'];

            include("Helpers/utf8UploadedFile.php");
            $fileName = covertToUTF8($initialfileName);



            /**
             * This checks the first 20 lines of the file and fills in any gaps in an
             * array of the second line of the file (the actual data). This will be
             * used to check against the required datatypes of each column and will throw
             * a red error message to the user if the wrong column is selected.
             */
            $handle = fopen($fileName, "r");
            $array = fgetcsv($handle, 0, ',', '"');
            $lineOfText = fgetcsv($handle, 0, ',', '"');
            $i = 0;
            do {

                $select = fgetcsv($handle, 0, ',', '"');
                if (is_array($lineOfText) || $lineOfText instanceof Traversable) {
                    foreach ($lineOfText as $key => $value) {
                        if ($value == "") {
                            $lineOfText[$key] = $select[$key];
                        }
                    }
                }
                $i++;
            } while ($i < 20);
            

            include('Views/Uploads/uploadInstallationsTEST.php');
        }
        break;

    case "upload":

        $fileName = $_POST['fileName'];
        $columnNameArray;

        foreach ($_POST as $key => $value) {
            if (($key != 'submitVerify') && ($key != 'fileName') && ($key != 'headers')) {
                if (($key == 'type') && ($value == 'inst_type')) {
                    $columnNameArray[$key] = $value;
                } else if (($key == 'type') && ($value != 'inst_type')) {
                    // do nothing
                } else {
                    $columnNameArray[$key] = $value;
                }
            }
            if ($value == "") {
                $columnNameArray[$key] = "@dummy";
            }
        }

        if (isset($_POST['headers'])) {
            $headers = "1";
        } else {
            $headers = "0";
        }

        if ((isset($_POST['type']) && ($_POST['type'] != 'inst_type'))) {
            $type = $_POST['type'];
        } else {
            $type = "UNSPECIFIED";
        }

        print $uploadData = $insertInstallationsFromCSVController->importDataFromCSV($fileName, $columnNameArray, $headers, $type);


        // Show the installations we are unsure of and give options to add or delete them.
        $manualRow = $insertInstallationsFromCSVController->getAllFromManualCheckTbl();
        if (isset($manualRow)) {
            $first_key = key($manualRow);


            print "<h3>Homeless Installations</h3>";
            print "<form name='manualCheckForm' action='index.php?portal=mr&page=upload&action=update' method='post' enctype='multipart/form-data'>";
            print "<table class='borderedTable'>";

            print "<tr>";
            print "<th>Delete ALL</th>";
            print "<th>Add Installation To Original Customer ALL</th>";
            print "<th>Keep Engines Only - Original Customer ALL</th>";
            print "<th>Keep Engines Only - Matching Customer ALL</th>";
            print "<th>Create New Customer/Installation ALL</th>";
            print "</tr>";
            print "<tr>";
            print "<th>Delete</th>";
            print "<th>Add Installation To Original Customer</th>";
            print "<th>Keep Engines Only - Original Customer</th>";
            print "<th>Keep Engines Only - Matching Customer</th>";
            print "<th>Create New Customer/Installation</th>";
            foreach ($manualRow[$first_key] as $key => $value) {
                print "<th>" . $key . "</th>";
            }
            print "</tr>";

            foreach ($manualRow as $manualID => $manualCheck) {
                print "<tr>";

                foreach ($manualCheck as $key => $value) {
                    // if its the temp ID then this is where we set the radio buttons
                    if ($key == 'temp_id') {
                        $originalCustomer = $customerController->getCustomerByName($manualCheck['cust_name']); // The original customer
                        if (isset($originalCustomer)) {
                            $originalInstallation = $installationController->getInstallationsByInstallationNameAndCustomerID($manualCheck['inst_installation_name'], $originalCustomer->customerID); // original installation
                        }
                        $matchingInstallation = $installationController->getInstallationByInstallationID($manualCheck['matching_installation_id']); // The original installation
                        
                        // Delete the record - dump it
                        print "<td><input type='radio' name='decisiongroup_" . $value . "' value='delete'/></td>";
                        
                        /*
                         * If the matching installation is not set or is null but the original customer is available 
                         * then show Add Installation to customer radio button.
                         */
                        if ((!isset($originalInstallation) || ($originalInstallation == NULL)) && 
                                (isset($originalCustomer) && $originalCustomer->customerID != $manualCheck['matching_customer_id'])) {
                            print "<td><input type='radio' name='decisiongroup_" . $value . "' value='installation' data-id='$originalCustomer->customerID'  checked='checked' /></td>";
                        
                        } else {
                            print "<td></td>";
                        }
                        
                        // If the original installation exists in the DB then we can add engines to it
                        if (isset($originalInstallation) && ($originalInstallation != NULL)) {
                            print "<td><input type='radio' name='decisiongroup_" . $value . "' value='technical_original'/></td>";
                        } else {
                            print "<td></td>";
                        }
                        // If the matching installation exists in the DB then we can add the engines to that instead.
                        if (isset($matchingInstallation) && ($matchingInstallation != NULL)) {
                            print "<td><input type='radio' name='decisiongroup_" . $value . "' value='technical_matching'/></td>";
                        } else {
                            print "<td></td>";
                        }
                        /*
                         * If the customer does not exist then its OK to create a new customer and its installations.
                         */
                        if (!isset($originalCustomer) || ($originalCustomer == NULL)) {
                            print "<td><input type='radio' name='decisiongroup_" . $value . "' value='new'/></td>";
                        } else {
                            print "<td></td>";
                        }
                        print "<td>" . $value . "</td>"; // this sets the temp ID in its own cell to be viewed.
                    // All other values get set below here.
                    } else if ($key == 'matching_customer_id') {
                        $customer = $customerController->getCustomerByID($value);
                        print "<td>" . $customer->customerName . " (".$value.")</td>";
                    } else if ($key == 'matching_installation_id') {
                        $installation = $installationController->getInstallationByInstallationID($value);
                        print "<td>" . $installation->installationName . " (".$value.")</td>";
                    } else if ($key == 'cust_name') {
                        $customer = $customerController->getCustomerByName($value);
                        print "<td>" . $value . " (".$customer->customerID.")</td>";
                    } else {
                        print "<td>" . $value . "</td>";
                    }
                }
                print "</tr>";
            }
            print "</table>";
            print "<p><input type='submit' name='submit' value='Update Changes'/></p>";
            print "</form>";
        }
        break;
        
    case "update":
        // homeless installations and technical data need to be added to the database.

        foreach ($_POST as $key => $value) {
            if ($key != 'submit') {
                $expRadioGroup = explode("_", $key);
                $tempID = $expRadioGroup[1];
                $record = $manualImportController->getFromManualCheckTblByTempID($tempID);
                print $record['cust_name']. " ". $record['inst_installation_name']."<br/>";
                
                $originalCustomer = $customerController->getCustomerByName($record['cust_name']); // The original customer
                $originalInstallation = $installationController->getInstallationsByInstallationNameAndCustomerID($record['inst_installation_name'], $originalCustomer->customerID); // original installation
                $matchingInstallation = $installationController->getInstallationByInstallationID($record['matching_installation_id']); // The original installation
                        
                
                if ($value == 'delete') {
                    $delete = $manualImportController->deleteFromManualCheckTblByTempID($tempID);
                } else if ($value == 'technical_original') {
                    $newTechnical = $manualImportController->createManualTechnicalData($tempID, $originalInstallation->installationID, $originalCustomer->customerID);
                    $delete = $manualImportController->deleteFromManualCheckTblByTempID($tempID);
                   // debugWriter("debug.txt", $newTechnical);
                } else if ($value == 'technical_matching') {
                    $newTechnical = $manualImportController->createManualTechnicalData($tempID, $record['matching_installation_id'], $record['matching_customer_id']);
                    $delete = $manualImportController->deleteFromManualCheckTblByTempID($tempID);
                    //debugWriter("debug.txt", $newTechnical);
                } else if ($value == 'installation') {
                    /*
                     * If the installation has been inserted on a previous update, then we need to check this and just add the
                     * engines.
                     */
                    if (!isset($originalInstallation)) {
                        $newInstallation = $manualImportController->createManualInstallations($tempID, $originalCustomer->customerID);
                    } else {
                        reset($originalInstallation);
                        $first_key = key($originalInstallation);
                        $newInstallation = $originalInstallation[$first_key]->installationID;
                    }
                    $newTechnical = $manualImportController->createManualTechnicalData($tempID, $newInstallation, $originalCustomer->customerID);
                    $delete = $manualImportController->deleteFromManualCheckTblByTempID($tempID);
                   // debugWriter("debug.txt", $newInstallation);
                  //  debugWriter("debug.txt", $newTechnical);
                } else if ($value == 'new') {
                    $newCustomer = $manualImportController->createManualCustomers($tempID);
                    $newContacts = $manualImportController->createManualContacts($tempID, $newCustomer);
                    if (!isset($originalInstallation)) {
                        $newInstallation = $manualImportController->createManualInstallations($tempID, $newCustomer);
                    } else {
                        reset($originalInstallation);
                        $first_key = key($originalInstallation);
                        $newInstallation = $originalInstallation[$first_key]->installationID;
                    }
                    
                    $newTechnical = $manualImportController->createManualTechnicalData($tempID, $newInstallation, $newCustomer);
                    $delete = $manualImportController->deleteFromManualCheckTblByTempID($tempID);

                }
            }
            print "---";
        }

        $manualImportController->dropManualCheckTbl();

        break;
    default:
        ?>
        <p>Upload a CSV file with column headers. In the next section you can verify the column data.</p>
        <form name="importForm" action="index.php?portal=mr&page=upload&action=verify" method="post" enctype="multipart/form-data" >

            <input type="file" name="file" />
            <input type="submit" name="submit" value="Submit"/>


        </form>

        <?php
        break;
}
