<?php
include_once "Controllers/UpdateSeawebDetailsController.php";
include_once "Controllers/CustomerController.php";
include_once "Controllers/CustomerAddressController.php";
include_once "Controllers/CustomerContactsController.php";
include_once "Controllers/InstallationController.php";
include_once "Controllers/InstallationStatusController.php";
$updateSeawebDetailsController = new UpdateSeawebDetailsController();
$customerController = new CustomerController();
$customerAddressController = new CustomerAddressController();
$customerContactsController = new CustomerContactsController();
$installationController = new InstallationController();
$installationStatusController = new InstallationStatusController();

?>
<style>
    .changesTbl th, td {
        border: 1px solid black;
        font-size: 0.9em;
    }
</style>
<h3>SeaWeb Changes After Last Update</h3>

<p>The changes on this page require verification.</p> 
<p>To keep the original record, press delete to remove the new update.<br/>
To keep the new update, press save and the original record shall be archived.</p>

<h4>Customer Details</h4>
<form name="customerForm" action="index.php?portal=uploads&page=checkSeawebChanges" method="post" enctype="multipart/form-data">
<?php

$customerArray = $updateSeawebDetailsController->getNewCustomerRecord();
$addressArray = $updateSeawebDetailsController->getNewCustomerAddressRecord();
$contactArray = $updateSeawebDetailsController->getNewCustomerContactRecord();
    
    if (isset($customerArray)) {
        foreach($customerArray as $key => $updatedCustomer) {

            print "<h4>Customer Details</h4>";
            $customer = $customerController->getCustomerByID($updatedCustomer->customerID);

            $headers = [];
            foreach ($customer as $cuKey => $field) {
               /* if (($cuKey != 'customerID') && 
                    ($cuKey != 'tbhID') && ($cuKey != 'tbhAccountCode') &&	
                        ($cuKey != 'ldID') && ($cuKey != 'ldAccountCode') &&
                        ($cuKey != 'navisionID') && ($cuKey != 'seawebCode')
                        && ($cuKey != 'parentSeawebCode') && ($cuKey != 'parentNationalityOfControl')){*/
                    array_push($headers, $cuKey);
                //}
            }
            print "<table class='changesTbl'><tr>";
            foreach ($headers as $head) {
                print "<th>";
                print $head;
                print "</th>";
            }
            print "<th>Action</th>";
            print "</tr><tr>";   
            foreach ($customer as $cuKey => $field) {
                /*if (($cuKey != 'customerID') && 
                    ($cuKey != 'tbhID') && ($cuKey != 'tbhAccountCode') &&	
                        ($cuKey != 'ldID') && ($cuKey != 'ldAccountCode') &&
                        ($cuKey != 'navisionID') && ($cuKey != 'seawebCode')
                        && ($cuKey != 'parentSeawebCode') && ($cuKey != 'parentNationalityOfControl')){*/
                    print "<td>";
                    print $field;
                    print "</td>";
                //}
            }
            print "<td></td>";
            print "</tr>";

            print "<tr>";
            foreach ($updatedCustomer as $upKey => $update) {
                /*if (($upKey != 'customerID') && 
                    ($upKey != 'tbhID') && ($upKey != 'tbhAccountCode') &&	
                        ($upKey != 'ldID') && ($upKey != 'ldAccountCode') &&
                        ($upKey != 'navisionID') && ($upKey != 'seawebCode')
                        && ($upKey != 'parentSeawebCode') && ($upKey != 'parentNationalityOfControl')){*/

                if ($update != $customer->$upKey) {
                    print "<td class='red'>";
                } else {
                    print "<td>";
                }
                print $update;
                print "</td>";
                //}
            }
            print "<td>Save <input type='radio' name='customer_$key' value='savecustomer' checked='checked'/> Delete <input type='radio' name='customer_$key' value='deletecustomer'/><!--<a href='index.php?portal=uploads&page=checkSeawebChanges&action=savecustomer&record=$key'>SAVE</a> / <a href='index.php?portal=uploads&page=checkSeawebChanges&action=deletecustomer&record=$key'>DELETE</a>--></td>";
            print "</tr>";
            print "</table>";


        }
    } else {
        print "<p>No updated customer details to display</p>";
    }
    if (isset($addressArray)) {
        foreach($addressArray as $key => $updatedCustomer) {
            
            print "<h4>Address Details</h4>";
            $address = $customerAddressController->getCustomerAddressByID($updatedCustomer->addressID);
           
            $headers = [];
            foreach ($address as $adKey => $field) {
                /*if (($adKey != 'addressID') && 
                    ($adKey != 'archived') && ($adKey != 'addressVerified') &&	
                        ($adKey != 'defaultAddress')){*/
                    array_push($headers, $adKey);
                //}
            }
            print "<table class='changesTbl'><tr>";
            foreach ($headers as $head) {
                print "<th>";
                print $head;
                print "</th>";
            }
            print "<th>Action</th>";
            print "</tr><tr>";    
            foreach ($address as $adKey => $field) {
                /*if (($adKey != 'addressID') && 
                    ($adKey != 'archived') && ($adKey != 'addressVerified') &&	
                        ($adKey != 'defaultAddress')){*/
                    print "<td>";
                    print $field;
                    print "</td>";
               // }
            }
            print "<td></td>";
            print "</tr>";

            print "<tr>";
            foreach ($updatedCustomer as $upKey => $update) {
                /*if (($upKey != 'addressID') && 
                    ($upKey != 'archived') && ($upKey != 'addressVerified') &&	
                        ($upKey != 'defaultAddress')){*/

                if ($update != $address->$upKey) {
                    print "<td class='red'>";
                } else {
                    print "<td>";
                }
                print $update;
                print "</td>";
               // }
            }
            print "<td>Save <input type='radio' name='address_$key' value='saveaddress' checked/> Delete <input type='radio' name='address_$key' value='deleteaddress'/><!--<a href='index.php?portal=uploads&page=checkSeawebChanges&action=saveaddress&record=$key'>SAVE</a> / <a href='index.php?portal=uploads&page=checkSeawebChanges&action=deleteaddress&record=$key'>DELETE</a>--></td>";
            print "</tr></table>";
        }
    } else {
        print "<p>No updated customer address details to display</p>";
    }
    if (isset($contactArray)) {
        foreach($contactArray as $key => $updatedCustomer) {    
            print "<h4>Contact Details</h4>";
            $contact = $customerContactsController->getContactByID($updatedCustomer->customerContactID);

            $headers = [];
            foreach ($contact as $coKey => $field) {
                if (($coKey != 'customerContactID') && ($coKey != 'customerID') && 
                    ($coKey != 'contactValidated') && ($coKey != 'inactive') ){
                array_push($headers, $coKey);
                    }
            }
            print "<table class='changesTbl'><tr>";
            foreach ($headers as $head) {
                print "<th>";
                print $head;
                print "</th>";
            }
            print "<th>Action</th>";
            print "</tr><tr>";   
            foreach ($contact as $coKey => $field) {
                if (($coKey != 'customerContactID') && ($coKey != 'customerID') && 
                    ($coKey != 'contactValidated') && ($coKey != 'inactive') ){
                print "<td>";
                print $field;
                print "</td>";
                    }
            }
            print "<td></td>";
            print "</tr>";

            print "<tr>";
            foreach ($updatedCustomer as $upKey => $update) {
                if (($upKey != 'customerContactID') && ($upKey != 'customerID') && 
                    ($upKey != 'contactValidated') && ($upKey != 'inactive') ){

                if ($update != $contact->$upKey) {
                    print "<td class='red'>";
                } else {
                    print "<td>";
                }
                print $update;
                print "</td>";
                }
            }
            print "<td>Save <input type='radio' name='contact_$key' value='savecontacts' checked/> Delete <input type='radio' name='contact_$key' value='deletecontacts'/></td>";
            print "</tr></table>";
        }
    } else {
        print "<p>No updated customer contact details to display</p>";
    }
?>
    <input type="submit" name="submitCustomer" value="Save Customer Details"/>
</form>
<h4>Ship Details</h4>
<form name="shipForm" action="index.php?portal=uploads&page=checkSeawebChanges" method="post" enctype="multipart/form-data">
    <input type="hidden" name="postShip" value="postShip"/>
<?php

    $shipArray = $updateSeawebDetailsController->getAllUpdatedShipDetails();
    

    if (isset($shipArray)) {
        /*print "<pre>";
        print_r($shipArray);
        print "</pre>";*/
        foreach($shipArray as $key => $updatedShip) {
            
            $ship = $installationController->getInstallationByIMO($updatedShip->imo);

            $headers = [];
            foreach ($ship as $coKey => $field) {
                /*if (($coKey != 'installationID') && ($coKey != 'operatingCompany') && 
                    ($coKey != 'photoLink') && ($coKey != 'hullNumber') &&
                        ($coKey != 'hullType') && ($coKey != 'deadWeight') &&
                        ($coKey != 'lengthOverall') && ($coKey != 'beamExtreme') &&
                        ($coKey != 'beamMoulded') && ($coKey != 'draught') && 
                        ($coKey != 'grossTonnage') && ($coKey != 'length') && 
                        ($coKey != 'teu') && ($coKey != 'buildYear') &&
                        ($coKey != 'archiveFlag') && ($coKey != 'archiveReason') &&
                        ($coKey != 'customerAddressID') && ($coKey != 'originalSourceDate')){*/
                array_push($headers, $coKey);
                //    }
            }
            print "<table class='changesTbl'><tr>";
            foreach ($headers as $head) {
                print "<th>";
                print $head;
                print "</th>";
            }
            print "<th>Action</th>";
            print "</tr><tr>";   
            foreach ($ship as $coKey => $field) {
               /* if (($coKey != 'installationID') && ($coKey != 'operatingCompany') && 
                    ($coKey != 'photoLink') && ($coKey != 'hullNumber') &&
                        ($coKey != 'hullType') && ($coKey != 'deadWeight') &&
                        ($coKey != 'lengthOverall') && ($coKey != 'beamExtreme') &&
                        ($coKey != 'beamMoulded') && ($coKey != 'draught') && 
                        ($coKey != 'grossTonnage') && ($coKey != 'length') && 
                        ($coKey != 'teu') && ($coKey != 'buildYear') &&
                        ($coKey != 'archiveFlag') && ($coKey != 'archiveReason') &&
                        ($coKey != 'customerAddressID') && ($coKey != 'originalSourceDate')){*/
                print "<td>";
                if ($coKey == "status") {
                    $status = $installationStatusController->getInstallationStatusByID($field);
                    print $status->statusDescription;
                } else {
                    print $field;
                }
                print "</td>";
                   // }
            }
            print "<td></td>";
            print "</tr>";

            print "<tr>";
            foreach ($updatedShip as $upKey => $update) {
               /* if (($upKey != 'installationID') && ($upKey != 'operatingCompany') && 
                    ($upKey != 'photoLink') && ($upKey != 'hullNumber') &&
                        ($upKey != 'hullType') && ($upKey != 'deadWeight') &&
                        ($upKey != 'lengthOverall') && ($upKey != 'beamExtreme') &&
                        ($upKey != 'beamMoulded') && ($upKey != 'draught') && 
                        ($upKey != 'grossTonnage') && ($upKey != 'length') && 
                        ($upKey != 'teu') && ($upKey != 'buildYear') &&
                        ($upKey != 'archiveFlag') && ($upKey != 'archiveReason') &&
                        ($upKey != 'customerAddressID') && ($upKey != 'originalSourceDate')){*/

                if ($update != $ship->$upKey) {
                    print "<td class='red'>";
                } else {
                    print "<td>";
                }
                if ($upKey == "status") {
                    $status = $installationStatusController->getInstallationStatusByID($update);
                    print $status->statusDescription;
                } else {
                    print $update;
                }
                print "</td>";
                //}
            }
            print "<td>Save <input type='radio' name='ship_$key' value='saveship' checked/> Delete <input type='radio' name='ship_$key' value='deleteship'/><!--<a href='index.php?portal=uploads&page=checkSeawebChanges&action=saveship&record=$key'>SAVE</a> / <a href='index.php?portal=uploads&page=checkSeawebChanges&action=deleteship&record=$key'>DELETE</a>--></td>";
            
            
            print "</tr></table>";
        }
    } else {
        print "<p>No updated ship records to display</p>";
    }
?>
<input type="submit" name="submitShip" value="Save Ship Details"/>
</form>