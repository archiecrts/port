<?php
$optGroups = ['Customer', 'Installation', 'Address', 'Owner Parent', 'Technical Manager', 'Engine', 'Surveys', 'Contact 1',
    'Contact 2', 'Contact 3'];

$dbInstallationArray = [
    'cust_name' => ['Managing Company (Customer)' => 'Customer'],
    'cust_telephone' => ['Main Telephone Number' => 'Customer'],
    'cust_fax' => ['Main Fax Number' => 'Customer'],
    'cust_website' => ['Website' => 'Customer'],
    'cust_account_manager' => ['Account Manager' => 'Customer'],
    'cust_type' => ['Customer Type' => 'Customer'],
    'cust_tbh_id' => ['TBH ID' => 'Customer'],
    'cust_tbh_account_code' => ['TBH Account Code' => 'Customer'],
    'cust_ld_id' => ['LINCOLN ID' => 'Customer'],
    'cust_ld_account_code' => ['LINCOLN Account Code' => 'Customer'],
    'cust_header_notes' => ['Header Notes' => 'Customer'],
    
    'inst_type' => ['Location Type' => 'Installation'], // land or sea
    'inst_installation' => ['Vessel or Plant Type (Installation)' => 'Installation'], // type of vessel or plant type (eg. power station / train
    'inst_installation_name' => ['Installation Name' => 'Installation'], // Plant name or ship name
    'inst_imo' => ['IMO Ship Number' => 'Installation'],
    'installation_status_tbl_stat_id' => ['Status' => 'Installation'],
    
    'inst_original_source' => ['Source' => 'Installation'],
    'inst_original_source_date' => ['Source Date' => 'Installation'],
    'inst_photo_link' => ['Photo Link' => 'Installation'],
    'inst_hull_number' => ['Hull Number' => 'Installation'],
    'inst_hull_type' => ['Hull Type' => 'Installation'],
    'inst_yard_built' => ['Yard Built' => 'Installation'],
    'inst_deadweight' => ['Deadweight' => 'Installation'],
    'inst_length_overall' => ['Length Overall' => 'Installation'],
    'inst_beam_extreme' => ['Beam (Extreme)' => 'Installation'],
    'inst_beam_moulded' => ['Beam (Moulded)' => 'Installation'],
    'inst_built_date' => ['Built Date' => 'Installation'],
    'inst_draught' => ['Draught' => 'Installation'],
    'inst_gross_tonnage' => ['Gross Tonnage' => 'Installation'],
    'inst_length' => ['Length' => 'Installation'],
    'inst_propeller_type' => ['Propeller Type' => 'Installation'],
    'inst_propulsion_unit_count' => ['Propulsion Unit Count' => 'Installation'],
    'inst_ship_builder' => ['Ship Builder' => 'Installation'],
    'inst_teu' => ['TEU' => 'Installation'],
    'inst_build_year' => ['Build Year' => 'Installation'],
    
    'cadt_office_name' => ['Office Name' => 'Address'],
    'cadt_address' => ['Address' => 'Address'],
    'cadt_city' => ['City' => 'Address'],
    'cadt_state' => ['State' => 'Address'],
    'cadt_region' => ['Region' => 'Address'],
    'cadt_country' => ['Country' => 'Address'],
    'cadt_postcode' => ['Postcode' => 'Address'],
    
    'inst_owner_parent_company' => ['Owner Parent Company' => 'Owner Parent'],
    /*'cadt_owner_parent_code' => ['Owner Parent Company Code' => 'Owner Parent'],
    'cadt_owner_parent_country' => ['Owner Parent Company Country' => 'Owner Parent'],
    'cadt_owner_parent_office_name' => ['Owner Parent Office Name' => 'Owner Parent'],
    'cadt_owner_parent_address' => ['Owner Parent Address' => 'Owner Parent'],
    'cadt_owner_parent_city' => ['Owner Parent City' => 'Owner Parent'],
    'cadt_owner_parent_state' => ['Owner Parent State' => 'Owner Parent'],
    'cadt_owner_parent_country' => ['Owner Parent Country' => 'Owner Parent'],
    'cadt_owner_parent_postcode' => ['Owner Parent Postcode' => 'Owner Parent'],*/
    
    'inst_technical_manager_company' => ['Technical Manager Company' => 'Technical Manager'],
    //'cadt_technical_manager_code' => ['Technical Manager Company Code' => 'Technical Manager'],
    //'cadt_technical_manager_country' => ['Technical_manager_company_country' => 'Technical Manager'],
    
    'iet_main_aux' => ['Eng Main/Aux' => 'Engine'],
    'iet_engine_manufacturer' => ['Engine Manufacturer' => 'Engine'],
    'iet_engine_model' => ['Engine Model' => 'Engine'],
    'iet_series' => ['Engine Series' => 'Engine'],
    'iet_engine_cylinder_count' => ['Cylinder Count' => 'Engine'],
    'iet_engine_cylinder_configuration' => ['Cylinder Configuration' => 'Engine'],
    'iet_bore_size' => ['Engine Bore Size' => 'Engine'],
    'iet_stroke' => ['Engine Stroke' => 'Engine'],
    'iet_fuel_type' => ['Fuel Type' => 'Engine'],
    'iet_output' => ['Output' => 'Engine'],
    'iet_unit_name' => ['Unit Name' => 'Engine'],
    'iet_serial_number' => ['Serial Number' => 'Engine'],
    'iet_release_date' => ['Engine Release Date' => 'Engine'],
    'iet_comment' => ['Comment on Engine' => 'Engine'],
    'load_priority_lookup_tbl_load_id' => ['Load Priority' => 'Engine'],
    
    'sdht_special_survey_date' => ['Special Survey HISTORY' => 'Surveys'],
    'sdht_docking_survey_date' => ['Docking Survey HISTORY' => 'Surveys'],
    'sdht_tail_shaft_survey_date' => ['Tail Shaft Survey HISTORY' => 'Surveys'],
    'sddt_special_survey_date' => ['Special Survey DUE' => 'Surveys'],
    'sddt_docking_survey_date' => ['Docking Survey DUE' => 'Surveys'],
    'sddt_tail_shaft_survey_date' => ['Tail Shaft Survey DUE' => 'Surveys'],
    'tzlst_trading_zone' => ['Trading Zone' => 'Surveys'],
    'txlst_last_seen_date' => ['TZ Last Seen Date' => 'Surveys'],
    
    
    'ict_salutation_1' => ['Contact Salutation 1' => 'Contact 1'],
    'ict_first_name_1' => ['First Name 1' => 'Contact 1'],
    'ict_surname_1' => ['Surname 1' => 'Contact 1'],
    'ict_job_title_1' => ['Job Title 1' => 'Contact 1'],
    'ict_email_1' => ['Contact Email 1' => 'Contact 1'],
    'ict_number_1' => ['Contact Telephone 1' => 'Contact 1'],
    'ict_mobile_number_1' => ['Contact Mobile No. 1' => 'Contact 1'],
    'ict_info_source_1' => ['Details From 1' => 'Contact 1'],
    
    'ict_salutation_2' => ['Contact Salutation 2' => 'Contact 2'],
    'ict_first_name_2' => ['First Name 2' => 'Contact 2'],
    'ict_surname_2' => ['Surname 2' => 'Contact 2'],
    'ict_job_title_2' => ['Job Title 2' => 'Contact 2'],
    'ict_email_2' => ['Contact Email 2' => 'Contact 2'],
    'ict_number_2' => ['Contact Telephone 2' => 'Contact 2'],
    'ict_mobile_number_2' => ['Contact Mobile No. 2' => 'Contact 2'],
    'ict_info_source_2' => ['Details From 2' => 'Contact 2'],
    
    'ict_salutation_3' => ['Contact Salutation 3' => 'Contact 3'],
    'ict_first_name_3' => ['First Name 3' => 'Contact 3'],
    'ict_surname_3' => ['Surname 3' => 'Contact 3'],
    'ict_job_title_3' => ['Job Title 3' => 'Contact 3'],
    'ict_email_3' => ['Contact Email 3' => 'Contact 3'],
    'ict_number_3' => ['Contact Telephone 3' => 'Contact 3'],
    'ict_mobile_number_3' => ['Contact Mobile No. 3' => 'Contact 3'],
    'ict_info_source_3' => ['Details From 3' => 'Contact 3'],
];
?>
<script>
    /* This function is given an array and returns an array of duplicated values if they exist */
    function identifyDuplicatesFromArray(arr) {
        var i;
        var len = arr.length;
        var obj = {};
        var duplicates = [];

        for (i = 0; i < len; i++) {

            if (!obj[arr[i]]) {

                obj[arr[i]] = {};

            }

            else
            {
                duplicates.push(arr[i]);
            }

        }
        return duplicates;
    }
    /* This function gathers all the selected options and puts them in an array to check for duplicates */
    $(function () {
        $('#form').on('submit', function (e) {
            e.preventDefault(); // prevent the usual submit
            var checkArray = [];

            $("select").each(function (index) {
                var selected = $(this).find(":selected").text();

                if (selected !== "Not Required") {
                    checkArray.push(selected);
                }
            });

            var result = identifyDuplicatesFromArray(checkArray);
            if (result.length < 1) {
                this.submit();
            } else {
                alert("You have used the same column name twice.\r\n Option(s) used " + result);
            }
        });
    });
</script>

<h3>FILE: <?php echo $name; ?></h3>
<form id="form" name="form" action="index.php?portal=mr&page=upload&action=upload" method="post" enctype="multipart/form-data">
    <input type="hidden" name="fileName" value="<?php echo $fileName; ?>"/>
    Column Names are included in file <input type="checkbox" name="headers" value="1" checked/>
    <table>
        <tr>

<?php
$typeExists = 0;
if (is_array($array) || $array instanceof Traversable) {
    foreach ($array as $key => $fileColumnName) {
        if ((strcasecmp($fileColumnName, "TYPE") == 0)) {
            $typeExists = 1;
        }
        if ($fileColumnName != " ") {
            print "<td><strong>" . $fileColumnName . "</strong><br/>[<span class='tinyText'>Example: " . $lineOfText[$key] . "</span>]</td>";
        }
    }
}
if ($typeExists == 0) {
    print "<td><strong>Type</strong></td>";
}
?>
        </tr>
        <tr>

            <?php
            if (is_array($array) || $array instanceof Traversable) {
                foreach ($array as $key => $fileColumnName) {

                    print "<td><select name='" . $key . "_" . $fileColumnName . "'>";
                    print "<option value=''>Not Required</option>";
                    foreach ($optGroups as $optgroup) {
                        print "<optgroup label='" . $optgroup . "'>";
                        $group = $optgroup;
                        foreach ($dbInstallationArray as $key => $array1) {
                            foreach ($array1 as $name => $type) {
                                 if ($type === $group) {
                                    // match the file column names to the database values.
                                    if (strcasecmp($fileColumnName, $name) == 0) {
                                        print "<option value='$key' selected>$name</option>";
                                    } else {
                                        print "<option value='$key'>$name</option>";
                                    }
                                }
                            }
                        }
                        print "</optgroup>";
                    }

                    print "</select></td>";
                }
            }
            if ($typeExists == 0) {
                print "<td><select name='type'>
                <option value='land'>Land</option>
                <option value='marine'>Marine</option>
                </select>
                </td>";
            }
            ?>
        </tr>
    </table>


    <input type="submit" name="submitVerify" value="Import Data"/>
</form>