<h3>Upload Seals from Sales Force</h3>
<?php
include_once 'Controllers/InsertSealsFromCSVController.php';
$insertSealsFromCSVController = new InsertSealsFromCSVController();
if (isset($_POST['submit'])) {



    $fileExt = explode('.', $_FILES["file"]["name"]);

    if (strcasecmp($fileExt[count($fileExt) - 1], 'csv') != 0) {
        echo "<p span='red'>Sorry, you must use a CSV</p>";
    } else {

        // Check the file for errors, then save into the upload directory
        $name = '';
        if ($_FILES["file"]["error"] == 0) {
            $tmp_name = $_FILES["file"]["tmp_name"];
            $name = $_FILES["file"]["name"];
            move_uploaded_file($tmp_name, "upload/$name");
        }

        $initialfileName = 'upload/' . $_FILES['file']['name'];

        include("Helpers/utf8UploadedFile.php");
        $fileName = covertToUTF8($initialfileName);
        
        $result = $insertSealsFromCSVController->insertDataFromCSV($fileName);
        print $result;
    }
} else {
    ?>

    <form name="form" action="index.php?portal=uploads&page=uploadSeals" method="post" enctype="multipart/form-data">
        <input type="file" name="file" value=""/>
        <input type="submit" name="submit" value="Go"/>

    </form>
<p>The file needs the following columns in this order</p>
<ol>
    <li>IMO Number</li>
    <li>Ship Name</li>
    <li>Make - Model</li>
    <li>Drawing Number</li>
    <li>Serial Number</li>   
    <li>Comments</li>
</ol>
    <?php
}
