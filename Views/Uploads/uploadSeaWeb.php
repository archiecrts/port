<h3>Upload</h3>

<?php
include_once('Controllers/InstallationController.php');
include_once('Controllers/InsertSeaWebDataFromCSVController.php');
include_once('Controllers/CustomerContactsController.php');
include_once('Controllers/CustomerController.php');
include_once('Controllers/ManualImportController.php');
$installationController = new InstallationController();
$insertSeaWebDataFromCSVController = new InsertSeaWebDataFromCSVController();
$customerContactsController = new CustomerContactsController();
$customerController = new CustomerController();
$manualImportController = new ManualImportController();

if (isset($_GET['action'])) {
    $action = $_GET['action'];
} else {
    $action = "";
}
switch ($action) {
    case "verify":
        
        print "Verify Column Data";

        // load the file into the upload directory and read the first line to get
        // any column headers.
        // Check that file is .csv
        $fileExt = explode('.', $_FILES["file"]["name"]);
        
        if (strcasecmp($fileExt[count($fileExt) - 1], 'csv') != 0) {
            echo "<p span='red'>Sorry, you must use a CSV</p>";
        } else {

            // Check the file for errors, then save into the upload directory
            $name = '';
            if ($_FILES["file"]["error"] == 0) {
                $tmp_name = $_FILES["file"]["tmp_name"];
                $name = $_FILES["file"]["name"];
                move_uploaded_file($tmp_name, "upload/$name");
            }

            $initialfileName = 'upload/' . $_FILES['file']['name'];

            include("Helpers/utf8UploadedFile.php");
            $fileName = covertToUTF8($initialfileName);



            /**
             * This checks the first 20 lines of the file and fills in any gaps in an
             * array of the second line of the file (the actual data). This will be
             * used to check against the required datatypes of each column and will throw
             * a red error message to the user if the wrong column is selected.
             */
            $handle = fopen($fileName, "r");
            $array = fgetcsv($handle, 0, ',', '"');
            $lineOfText = fgetcsv($handle, 0, ',', '"');
            $i = 0;
            do {

                $select = fgetcsv($handle, 0, ',', '"');
                if (is_array($lineOfText) || $lineOfText instanceof Traversable) {
                    foreach ($lineOfText as $key => $value) {
                        if ($value == "") {
                            $lineOfText[$key] = $select[$key];
                        }
                    }
                }
                $i++;
            } while ($i < 20);
            

            include('Views/Uploads/uploadInstallationsSeaWeb.php');
        }
        break;

    case "upload":
        
        $fileName = $_POST['fileName'];
        $columnNameArray;

        foreach ($_POST as $key => $value) {
            if (($key != 'submitVerify') && ($key != 'fileName') && ($key != 'fileType') && ($key != 'headers')) {
                if (($key == 'type') && ($value == 'inst_type')) {
                    $columnNameArray[$key] = $value;
                } else if (($key == 'type') && ($value != 'inst_type')) {
                    // do nothing
                } else {
                    $columnNameArray[$key] = $value;
                }
            }
            if ($value == "") {
                $columnNameArray[$key] = "@dummy";
            }
        }

        if (isset($_POST['headers'])) {
            $headers = "1";
        } else {
            $headers = "0";
        }

        if ((isset($_POST['type']) && ($_POST['type'] != 'inst_type'))) {
            $type = $_POST['type'];
        } else {
            $type = "UNSPECIFIED";
        }

        print $uploadData = $insertSeaWebDataFromCSVController->importSeaWebDataFromCSV($fileName, $columnNameArray, $headers, $_POST['fileType']);


        break;
        
    
    default:
        ?>
        <p>Upload a CSV file with column headers. In the next section you can verify the column data.</p>
        <h4>Upload Order</h4>
        <ol>
            <li>Company Data</li>
            <li>Ship Data</li>
            <li>Main Engines</li>
            <li>Aux Engines</li>
            <li>Company Fleet</li>
            <li>Survey Date History</li>
            <li>Survey Date (due)</li>
            <li>Trading Zone Last Seen</li>
            <li>Ship Name History</li>
        </ol>
        <form name="importForm" action="index.php?portal=mr&page=uploadSeaweb&action=verify" method="post" enctype="multipart/form-data" >

            <input type="file" name="file" />
            <select name="fileType">
                <option value="companyData">Company Data</option>
                <option value="shipData">Ship Data</option>
                <option value="mainEngines">Main Engines</option>
                <option value="auxEngines">Aux Engines</option>
                <option value="companyFleetCounts">Company Fleet Counts</option>
                <option value="surveyDateHistory">Survey Date History</option>
                <option value="surveyDates">Survey Dates</option>
                <option value="tradingZone">Trading Zone Last Seen</option>
                <option value="shipNameHistory">Ship Name History</option>
            </select>
            <input type="submit" name="submit" value="Submit"/>


        </form>

        <?php
        break;
}
