<?php
$optGroups = ['Supplier', 'Product', 'Component'];

$dbInstallationArray = [
    'cust_name' => ['Supplier Name' => 'Supplier'],
    'cust_tbh_id' => ['TBH ID' => 'Supplier'],
    'cust_tbh_account_code' => ['TBH Account Code' => 'Supplier'],
    'cust_ld_id' => ['LINCOLN ID' => 'Supplier'],
    'cust_ld_account_code' => ['LINCOLN Account Code' => 'Supplier'],
    
    'dlt_designer_description' => ['Product Manufacturer' => 'Product'],
    'mlt_series' => ['Product Series' => 'Product'],
    
    'part_code' => ['Part/STC code' => 'Component'],
    'part_description' => ['Part Description' => 'Component']
];
?>
<script>
    /* This function is given an array and returns an array of duplicated values if they exist */
    function identifyDuplicatesFromArray(arr) {
        var i;
        var len = arr.length;
        var obj = {};
        var duplicates = [];

        for (i = 0; i < len; i++) {

            if (!obj[arr[i]]) {

                obj[arr[i]] = {};

            }

            else
            {
                duplicates.push(arr[i]);
            }

        }
        return duplicates;
    }
    /* This function gathers all the selected options and puts them in an array to check for duplicates */
    $(function () {
        $('#form').on('submit', function (e) {
            e.preventDefault(); // prevent the usual submit
            var checkArray = [];

            $("select").each(function (index) {
                var selected = $(this).find(":selected").text();

                if (selected !== "Not Required") {
                    checkArray.push(selected);
                }
            });

            var result = identifyDuplicatesFromArray(checkArray);
            if (result.length < 1) {
                this.submit();
            } else {
                alert("You have used the same column name twice.\r\n Option(s) used " + result);
            }
        });
    });
</script>

<h3>FILE: <?php echo $name; ?></h3>
<form id="form" name="form" action="index.php?portal=suppliers&page=addProducts&action=upload" method="post" enctype="multipart/form-data">
    <input type="hidden" name="fileName" value="<?php echo $fileName; ?>"/>
    Column Names are included in file <input type="checkbox" name="headers" value="1" checked/>
    <table>
        <tr>

<?php

if (is_array($array) || $array instanceof Traversable) {
    foreach ($array as $key => $fileColumnName) {
        
        if ($fileColumnName != " ") {
            print "<td><strong>" . $fileColumnName . "</strong><br/>[<span class='tinyText'>Example: " . $lineOfText[$key] . "</span>]</td>";
        }
    }
}

?>
        </tr>
        <tr>

            <?php
            if (is_array($array) || $array instanceof Traversable) {
                foreach ($array as $key => $fileColumnName) {

                    print "<td><select name='" . $key . "_" . $fileColumnName . "'>";
                    print "<option value=''>Not Required</option>";
                    foreach ($optGroups as $optgroup) {
                        print "<optgroup label='" . $optgroup . "'>";
                        $group = $optgroup;
                        foreach ($dbInstallationArray as $key => $array1) {
                            foreach ($array1 as $name => $type) {
                                 if ($type === $group) {
                                    // match the file column names to the database values.
                                    if (strcasecmp($fileColumnName, $name) == 0) {
                                        print "<option value='$key' selected>$name</option>";
                                    } else {
                                        print "<option value='$key'>$name</option>";
                                    }
                                }
                            }
                        }
                        print "</optgroup>";
                    }

                    print "</select></td>";
                }
            }
            
            ?>
        </tr>
    </table>


    <input type="submit" name="submitVerify" value="Import Data"/>
</form>