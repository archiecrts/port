<h3>Upload Supplier</h3>

<?php



if (isset($_GET['action'])) {
    $action = $_GET['action'];
} else {
    $action = "";
}
switch ($action) {
    case "verify":
        print "Verify Column Data";

        // load the file into the upload directory and read the first line to get
        // any column headers.
        // Check that file is .csv
        $fileExt = explode('.', $_FILES["file"]["name"]);

        if ($fileExt[count($fileExt) - 1] != 'csv') {
            echo "<p span='red'>Sorry, you must use a CSV</p>";
        } else {

            // Check the file for errors, then save into the upload directory
            $name = '';
            if ($_FILES["file"]["error"] == 0) {
                $tmp_name = $_FILES["file"]["tmp_name"];
                $name = $_FILES["file"]["name"];
                move_uploaded_file($tmp_name, "upload/$name");
            }

            $initialfileName = 'upload/' . $_FILES['file']['name'];

            include("Helpers/utf8UploadedFile.php");
            $fileName = covertToUTF8($initialfileName);



            /**
             * This checks the first 20 lines of the file and fills in any gaps in an
             * array of the second line of the file (the actual data). This will be
             * used to check against the required datatypes of each column and will throw
             * a red error message to the user if the wrong column is selected.
             */
            $handle = fopen($fileName, "r");
            $array = fgetcsv($handle, 0, ',', '"');
            $lineOfText = fgetcsv($handle, 0, ',', '"');
            $i = 0;
            do {

                $select = fgetcsv($handle, 0, ',', '"');
                if (is_array($lineOfText) || $lineOfText instanceof Traversable) {
                    foreach ($lineOfText as $key => $value) {
                        if ($value == "") {
                            $lineOfText[$key] = $select[$key];
                        }
                    }
                }
                $i++;
            } while ($i < 20);
            

            include('Views/Uploads/uploadSuppliersVerification.php');
        }
        break;

    case "upload":

        $fileName = $_POST['fileName'];
        $columnNameArray;

        foreach ($_POST as $key => $value) {
            if (($key != 'submitVerify') && ($key != 'fileName') && ($key != 'headers')) {
                if (($key == 'type') && ($value == 'inst_type')) {
                    $columnNameArray[$key] = $value;
                } else if (($key == 'type') && ($value != 'inst_type')) {
                    // do nothing
                } else {
                    $columnNameArray[$key] = $value;
                }
            }
            if ($value == "") {
                $columnNameArray[$key] = "@dummy";
            }
        }

        if (isset($_POST['headers'])) {
            $headers = "1";
        } else {
            $headers = "0";
        }

        if ((isset($_POST['type']) && ($_POST['type'] != 'inst_type'))) {
            $type = $_POST['type'];
        } else {
            $type = "UNSPECIFIED";
        }

        print $uploadData = $insertSuppliersFromCSVController->importDataFromCSV($fileName, $columnNameArray, $headers, $type);
        break;

    case "update":
        // homeless installations and technical data need to be added to the database.

        foreach ($_POST as $key => $value) {
            if ($key != 'submit') {
                $expRadioGroup = explode("_", $key);
                $tempID = $expRadioGroup[1];
                $record = $manualImportController->getFromManualCheckTblByTempID($tempID);
                print $record['cust_name']. " ". $record['inst_installation_name']."<br/>";
                
                $originalCustomer = $customerController->getCustomerByName($record['cust_name']); // The original customer
                $originalInstallation = $installationController->getInstallationsByInstallationNameAndCustomerID($record['inst_installation_name'], $originalCustomer->customerID); // original installation
                $matchingInstallation = $installationController->getInstallationByInstallationID($record['matching_installation_id']); // The original installation
                        
                
                if ($value == 'delete') {
                    $delete = $manualImportController->deleteFromManualCheckTblByTempID($tempID);
                } else if ($value == 'technical_original') {
                    $newTechnical = $manualImportController->createManualTechnicalData($tempID, $originalInstallation->installationID, $originalCustomer->customerID);
                    $delete = $manualImportController->deleteFromManualCheckTblByTempID($tempID);
                   // debugWriter("debug.txt", $newTechnical);
                } else if ($value == 'technical_matching') {
                    $newTechnical = $manualImportController->createManualTechnicalData($tempID, $record['matching_installation_id'], $record['matching_customer_id']);
                    $delete = $manualImportController->deleteFromManualCheckTblByTempID($tempID);
                    //debugWriter("debug.txt", $newTechnical);
                } else if ($value == 'installation') {
                    /*
                     * If the installation has been inserted on a previous update, then we need to check this and just add the
                     * engines.
                     */
                    if (!isset($originalInstallation)) {
                        $newInstallation = $manualImportController->createManualInstallations($tempID, $originalCustomer->customerID);
                    } else {
                        reset($originalInstallation);
                        $first_key = key($originalInstallation);
                        $newInstallation = $originalInstallation[$first_key]->installationID;
                    }
                    $newTechnical = $manualImportController->createManualTechnicalData($tempID, $newInstallation, $originalCustomer->customerID);
                    $delete = $manualImportController->deleteFromManualCheckTblByTempID($tempID);
                   // debugWriter("debug.txt", $newInstallation);
                  //  debugWriter("debug.txt", $newTechnical);
                } else if ($value == 'new') {
                    $newCustomer = $manualImportController->createManualCustomers($tempID);
                    $newContacts = $manualImportController->createManualContacts($tempID, $newCustomer);
                    if (!isset($originalInstallation)) {
                        $newInstallation = $manualImportController->createManualInstallations($tempID, $newCustomer);
                    } else {
                        reset($originalInstallation);
                        $first_key = key($originalInstallation);
                        $newInstallation = $originalInstallation[$first_key]->installationID;
                    }
                    
                    $newTechnical = $manualImportController->createManualTechnicalData($tempID, $newInstallation, $newCustomer);
                    $delete = $manualImportController->deleteFromManualCheckTblByTempID($tempID);

                }
            }
            print "---";
        }

        $manualImportController->dropManualCheckTbl();

        break;
    default:
        ?>
        <p>Upload a CSV file with column headers. In the next section you can verify the column data.</p>
        <form name="importForm" action="index.php?portal=suppliers&page=upload&action=verify" method="post" enctype="multipart/form-data" >

            <input type="file" name="file" />
            <input type="submit" name="submit" value="Submit"/>


        </form>

        <?php
        break;
}
