<?php
include_once "Controllers/UpdateSeawebDetailsController.php";
include_once "Controllers/CustomerController.php";
include_once "Controllers/CustomerAddressController.php";
include_once "Controllers/CustomerContactsController.php";
include_once "Controllers/InstallationController.php";
$updateSeawebDetailsController = new UpdateSeawebDetailsController();
$customerController = new CustomerController();
$customerAddressController = new CustomerAddressController();
$customerContactsController = new CustomerContactsController();
$installationController = new InstallationController();


include_once "ControllersHistory/CustomerHistoryController.php";
include_once "ControllersHistory/CustomerAddressHistoryController.php";
include_once "ControllersHistory/CustomerContactHistoryController.php";
include_once "ControllersHistory/InstallationHistoryController.php";
$customerHistoryController = new CustomerHistoryController();
$customerAddressHistoryController = new CustomerAddressHistoryController();
$customerContactHistoryController = new CustomerContactHistoryController();
$installationHistoryController = new InstallationHistoryController();



if ($action == "savecustomer") {

    $newCustomer = $updateSeawebDetailsController->getNewCustomerRecordByID($recordID);

    // archive the original customer and update the new one.
    $oldCustomerRecord = $customerController->getCustomerByID($newCustomer->customerID);
    
    $customerHistory = new HistoryCustomer(
            "", 
            $oldCustomerRecord->customerID, 
            $oldCustomerRecord->customerName, 
            $oldCustomerRecord->mainTelephone, 
            $oldCustomerRecord->mainFax, 
            $oldCustomerRecord->website, 
            $oldCustomerRecord->accountManager, 
            $oldCustomerRecord->tbhID, 
            $oldCustomerRecord->tbhAccountCode, 
            $oldCustomerRecord->ldID, 
            $oldCustomerRecord->ldAccountCode, 
            $oldCustomerRecord->headerNotes, 
            $oldCustomerRecord->seawebCode, 
            $oldCustomerRecord->parentSeawebCode, 
            $oldCustomerRecord->parentNationalityOfControl, 
            $oldCustomerRecord->companyStatus, 
            $oldCustomerRecord->companyFullName, 
            $oldCustomerRecord->navisionID, 
            $_SESSION['user_id'], 
            date("Y-m-d H:i:s", time()));
    
   
    $setCustomerHistory = $customerHistoryController->setCustomerHistory($customerHistory);
    
    // update current customer with new details.
    if ($customerController->updateCustomerObject($newCustomer)) {
        print "<p class='grean'>Customer ".$newCustomer->customerName." updated</p>";
        // mark duplicate in the update table.
        $updateCustomer = $updateSeawebDetailsController->updateduplicateRecordByType("customer", $recordID);
        if ($updateCustomer == true) {
            print "<p>Record status updated</p>";
        }
    }
    
} else if ($action == "deletecustomer") {
    
    // archive the original customer and update the new one.
    $oldCustomerRecord = $updateSeawebDetailsController->getNewCustomerRecordByID($recordID);
    
    $newCustomer = $customerController->getCustomerByID($oldCustomerRecord->customerID);
    
    
    
    $customerHistory = new HistoryCustomer(
            "", 
            $oldCustomerRecord->customerID, 
            $oldCustomerRecord->customerName, 
            $oldCustomerRecord->mainTelephone, 
            $oldCustomerRecord->mainFax, 
            $oldCustomerRecord->website, 
            $oldCustomerRecord->accountManager, 
            $oldCustomerRecord->tbhID, 
            $oldCustomerRecord->tbhAccountCode, 
            $oldCustomerRecord->ldID, 
            $oldCustomerRecord->ldAccountCode, 
            "DISCARDED change from SeaWeb update", 
            $oldCustomerRecord->seawebCode, 
            $oldCustomerRecord->parentSeawebCode, 
            $oldCustomerRecord->parentNationalityOfControl, 
            $oldCustomerRecord->companyStatus, 
            $oldCustomerRecord->companyFullName, 
            $oldCustomerRecord->navisionID, 
            $_SESSION['user_id'], 
            date("Y-m-d H:i:s", time()));
    
   
    $setCustomerHistory = $customerHistoryController->setCustomerHistory($customerHistory);
    
    // update current customer with new details.
    print "<p class='red'>Customer ".$oldCustomerRecord->customerName." discarded</p>";
    // mark duplicate in the update table.
    $updateCustomer = $updateSeawebDetailsController->updateduplicateRecordByType("customer", $recordID);
    if ($updateCustomer == true) {
        print "<p>Record status updated</p>";
    }
    
} else if ($action == "saveaddress") {
    
    $newAddress = $updateSeawebDetailsController->getNewCustomerAddressRecordByID($recordID);
    // archive the original customer and update the new one.
    $oldCustomerAddressRecord = $customerAddressController->getCustomerAddressByID($newAddress->addressID);
    
    $customerAddressHistory = new HistoryCustomerAddress(
            "", 
            $oldCustomerAddressRecord->addressID, 
            $oldCustomerAddressRecord->officeName, 
            $oldCustomerAddressRecord->address, 
            $oldCustomerAddressRecord->city, 
            $oldCustomerAddressRecord->state, 
            $oldCustomerAddressRecord->country, 
            $oldCustomerAddressRecord->postcode, 
            $oldCustomerAddressRecord->addressVerified, 
            $oldCustomerAddressRecord->customerID, 
            1, 
            $oldCustomerAddressRecord->defaultAddress, 
            $oldCustomerAddressRecord->fullAddress, 
            $_SESSION['user_id'], 
            date("Y-m-d H:i:s", time()));
    

    $setCustomerAddressHistory = $customerAddressHistoryController->setCustomerAddressHistory($customerAddressHistory);
    
    // update current customer address with new details.
    if ($customerAddressController->updateCustomerAddress($newAddress)) {
        print "<p class='grean'>Customer Address updated</p>";
        // mark duplicate in the update table.
        $updateCustomerAddress = $updateSeawebDetailsController->updateduplicateRecordByType("address", $recordID);
        if ($updateCustomerAddress == true) {
            print "<p>Record status updated</p>";
        }
    }
} else if ($action == "deleteaddress") {
    // archive the original customer and update the new one.
    $oldCustomerAddressRecord = $updateSeawebDetailsController->getNewCustomerAddressRecordByID($recordID);
    
    $newCustomerAddress = $customerAddressController->getCustomerAddressByID($oldCustomerAddressRecord->addressID);
    
    
    
    $customerAddressHistory = new HistoryCustomerAddress("", 
            $oldCustomerAddressRecord->addressID, 
            $oldCustomerAddressRecord->officeName." (DISCARDED change from SeaWeb update)", 
            $oldCustomerAddressRecord->address, 
            $oldCustomerAddressRecord->city, 
            $oldCustomerAddressRecord->state, 
            $oldCustomerAddressRecord->country, 
            $oldCustomerAddressRecord->postcode, 
            0, 
            $oldCustomerAddressRecord->customerID, 
            1, 
            $oldCustomerAddressRecord->defaultAddress, 
            $oldCustomerAddressRecord->fullAddress, 
            $_SESSION['user_id'], 
            date("Y-m-d H:i:s", time()));
  
    $setCustomerAddressHistory = $customerAddressHistoryController->setCustomerAddressHistory($customerAddressHistory);
    
    // update current customer address with new details.
    print "<p class='red'>Customer address discarded</p>";
    // mark duplicate in the update table.
    $updateCustomerAddress = $updateSeawebDetailsController->updateduplicateRecordByType("address", $recordID);
    if ($updateCustomerAddress == true) {
        print "<p>Record status updated</p>";
    }
    // save and delete contact not really relevant right now because any change in email address generates a new email address record regardless.
} else if ($action == "saveship") {
    //debugWriter("debug.txt", "Inside Saveship");
    $newShip = $updateSeawebDetailsController->getNewShipRecordByID($recordID);
       
    $oldShipRecord = $installationController->getInstallationByInstallationID($newShip->installationID);

    
    $shipHistory = new HistoryInstallation(
            "", 
            $newShip->installationID, 
            $oldShipRecord -> type, 
            $oldShipRecord -> installation, 
            $oldShipRecord -> installationName, 
            $oldShipRecord -> imo, 
            $oldShipRecord -> status, 
            $oldShipRecord -> operatingCompany, 
            $oldShipRecord -> technicalManager, 
            $oldShipRecord -> originalSource, 
            $oldShipRecord -> originalSourceDate, 
            $oldShipRecord -> photoLink, 
            $oldShipRecord -> hullNumber, 
            $oldShipRecord -> hullType, 
            $oldShipRecord -> yardBuilt, 
            $oldShipRecord -> deadWeight, 
            $oldShipRecord -> lengthOverall, 
            $oldShipRecord -> beamExtreme, 
            $oldShipRecord -> beamMoulded, 
            $oldShipRecord -> builtDate, 
            $oldShipRecord -> draught, 
            $oldShipRecord -> grossTonnage, 
            $oldShipRecord -> length, 
            $oldShipRecord -> propellerType, 
            $oldShipRecord -> propulsionUnitCount, 
            $oldShipRecord -> shipBuilder, 
            $oldShipRecord -> teu, 
            $oldShipRecord -> buildYear, 
            1, 
            "Replaced by new SeaWeb record", 
            $oldShipRecord -> customerAddressID, 
            $oldShipRecord -> flagName,
            $oldShipRecord -> leadShipByIMO,
            $oldShipRecord -> shipTypeLevel4,
            $oldShipRecord -> classificationSociety,
            $_SESSION['user_id'], 
            date("Y-m-d H:i:s", time()));
    
   
    $setShipHistory = $installationHistoryController->setInstallationHistory($shipHistory);
    
    // update current ship with new details.
    if ($setShipHistory != null) {
        print "<p class='blue'>Ship history ".$oldShipRecord -> installationName." updated. New History Record ".$setShipHistory."</p>";
    } else {
        print "<p class='red'>Ship history".$oldShipRecord -> installationName." FAILED</p>";
    }
    // update the old ship.
    if ($installationController->updateInstallationObject($newShip) == true) {
        // mark duplicate in the update table.
        $updateShip = $updateSeawebDetailsController->updateDuplicateShipRecord($recordID);
        if ($updateShip == true) {
            print "<p>Record status updated</p>";
        }
    } else {
       print "<p>Record status Failed to update</p>";
    }
    
    
} else if ($action == "deleteship") {
    $oldShipRecord = $installationController->getInstallationByInstallationID($recordID);
    
    $newShip = $updateSeawebDetailsController->getNewShipRecordByID($oldShipRecord->installationID);
    
    
    
    $shipHistory = new HistoryInstallation(
            "", 
            $newShip->installationID, 
            $newShip -> type, 
            $newShip -> installation, 
            $newShip -> installationName, 
            $newShip -> imo, 
            $newShip -> status, 
            $newShip -> operatingCompany, 
            $newShip -> technicalManager, 
            $newShip -> originalSource, 
            $newShip -> originalSourceDate, 
            $newShip -> photoLink, 
            $newShip -> hullNumber, 
            $newShip -> hullType, 
            $newShip -> yardBuilt, 
            $newShip -> deadWeight, 
            $newShip -> lengthOverall, 
            $newShip -> beamExtreme, 
            $newShip -> beamMoulded, 
            $newShip -> builtDate, 
            $newShip -> draught, 
            $newShip -> grossTonnage, 
            $newShip -> length, 
            $newShip -> propellerType, 
            $newShip -> propulsionUnitCount, 
            $newShip -> shipBuilder, 
            $newShip -> teu, 
            $newShip -> buildYear, 
            1, 
            "DISCARDED change from SeaWeb record", 
            $newShip -> customerAddressID, 
            $newShip -> flagName,
            $newShip -> leadShipByIMO,
            $_SESSION['user_id'], 
            date("Y-m-d H:i:s", time()));
  
    $setShipHistory = $installationHistoryController->setInstallationHistory($shipHistory);
    
    // update current customer address with new details.
    print "<p class='red'>Ship record discarded</p>";
    // mark duplicate in the update table.
    $updateShip = $updateSeawebDetailsController->updateDuplicateShipRecord($recordID);
    if ($updateShip == true) {
        print "<p>Record status updated</p>";
    }
}
// Remove those old records from the update tables.
$updateSeawebDetailsController->removeCompleteDuplicatesFromCustomer();
$updateSeawebDetailsController->removeCompleteDuplicatesFromShipData();