<?php
include_once 'Controllers/UserHistoryController.php';
$userHistoryController = new UserHistoryController();

if (isset($_POST['submit'])) {
    if (count($_FILES['upload']['name']) > 0) {
        //Loop through each file
        for ($i = 0; $i < count($_FILES['upload']['name']); $i++) {
            //Get the temp file path
            $tmpFilePath = $_FILES['upload']['tmp_name'][$i];

            //Make sure we have a filepath
            if ($tmpFilePath != "") {

                //save the filename
                $shortname = $_FILES['upload']['name'][$i];

                //save the url and the file
                $filePath = "upload/" . $_FILES['upload']['name'][$i];

                //Upload the file into the temp dir
                if (move_uploaded_file($tmpFilePath, $filePath)) {

                    $files[] = $shortname;
                    //insert into db 
                    //use $shortname for the filename
                    //use $filePath for the relative url to the file
                } else {
                    print "error";
                }
            }
        }
    } else {
        print "No files found";
    }

    //show success message
    echo "<h1>Uploaded:</h1>";
    if (is_array($files)) {
        echo "<ul>";
        foreach ($files as $file) {
            echo "<li>$file</li>";
        }
        echo "</ul>";
    }
    $_FILES = NULL;
    $files = NULL;
    unset($files);
    unset ($_FILES);
}

?>

<!--<form action="index.php?portal=mr&page=uploadFiles" enctype="multipart/form-data" method="post">

    <div>
        <label for='upload'>Add Attachments:</label>
        <input id='upload' name="upload[]" type="file" multiple="multiple" />
    </div>

    <p><input type="submit" name="submit" value="Submit"></p>

</form>-->


<!--<h3>Files in FTP folder</h3>-->
<?php

// temp place to check FPT folder for new data.

include ("./Helpers/ftp/ftpListFiles.php");
//print $server_file;
$expFileName = explode("_", $server_file);
$expDate = explode(".", $expFileName[1]);

$format = 'Ymd';
$date = DateTime::createFromFormat($format, $expDate[0]);
echo "Format:  " . $date->format('Y-m-d') . "<br/>";

$proceed = false;

if ($userHistoryController->lastSeawebUpdateAt()->itemDate != "") {
    if ($userHistoryController->lastSeawebUpdateAt()->itemDate < $date) {
        $proceed = true;
    }
} else {
    $proceed = true;
}

if ($proceed == true) {
    // ftp seaweb download

    include ("./Helpers/ftp/ftpSeaWebDownload.php");

    // unzip the folder

    include ("./Helpers/ftp/unzipFile.php");
    
    
    if ($unzip == true) {
        // run the update script.
        include 'Views/Uploads/updateSeaweb.php';
    }
    
    unlink($local_file);
    
}
