<?php
include_once 'Controllers/CustomerController.php';
include_once 'Controllers/UpdateSeawebDataController.php';
$customerController = new CustomerController();
$updateSeawebDataController = new UpdateSeawebDataController();
/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

//$customerOne = $customerController->getCustomerByID(1);
//$customerTwo = $customerController->getCustomerByID(1);
if (isset($_POST['button'])) {
    switch($_POST['button']) {
        case "customer":
            print "customer<br/>";
            $updateSeawebDataController->updateCustomerTable();
            print "<br/>";
            break;
        case "ship":
            print "ship<br/>";
            $updateSeawebDataController->updateShipDataTable();
            print "<br/>";
            break;
        case "main":
            print "Main Engines<br/>";
            $updateSeawebDataController->updateMainEngineTable();
            print "<br/>";
            break;
        case "aux":
            print "Aux Engines<br/>";
            $updateSeawebDataController->updateAuxEngineTable();
            print "<br/>";
            break;
        case "sdh":
            print "Survey History<br/>";
            $updateSeawebDataController->updateSurveyDateHistoryTable();
            print "<br/>";
            break;
        case "add":
            print "Survey Due Date<br/>";
            $updateSeawebDataController->updateSurveyDueDateTable();
            print "<br/>";
            break;
        case "tz":
            print "Trading Zone<br/>";
            $updateSeawebDataController->updateTradingZoneTable();
            print "<br/>";
            break;
        case "nameHistory":
            print "Ship Name History<br/>";
            $updateSeawebDataController->updateNameHistoryTable();
            print "<br/>";
            break;
    }
}

function bool2str($bool)
{
    if ($bool === false) {
        return 'FALSE';
    } else {
        return 'TRUE';
    }
}

function compareObjects(&$o1, &$o2)
{
    /*print "<pre>OBJ1: ";
    print_r($o1);
    print "OBJ 2: ";
    print_r($o2);
    print "</pre>";*/
    //debugWriter("debug.txt", 'o1'.$o1->customerName.' == o2 : '.$o2->customerName.' ' . bool2str($o1 == $o2));
    //echo 'o1 == o2 : ' . bool2str($o1 == $o2) . "<br/>";
    //debugWriter("debug.txt", 'o1'.$o1->customerName.' != o2 : '.$o2->customerName.' '. bool2str($o1 != $o2));
    //echo 'o1 != o2 : ' . bool2str($o1 != $o2) . "<br/>";
    //debugWriter("debug.txt", 'o1'.$o1->customerName.' === o2 : '.$o2->customerName.' '. bool2str($o1 === $o2));
    //debugWriter("debug.txt", 'o1'.$o1->customerName.' !== o2 : '.$o2->customerName.' ' . bool2str($o1 !== $o2));
    //echo 'o1 === o2 : ' . bool2str($o1 === $o2) . "<br/>";
    //echo 'o1 !== o2 : ' . bool2str($o1 !== $o2) . "<br/>";
    return bool2str($o1 == $o2);
}


//compareObjects($customerOne, $customerTwo);

//$updateSeawebDataController->updateSeawebData("/upload/");
print "NOW UPLOAD INDIVIDUAL FILES";



?>
<form name="form" action="index.php?portal=uploads&page=updateSeaweb" enctype="multipart/form-data" method="post">
    <button name="button" value="customer">Customer</button><br />
    <button name="button" value="ship">Ship</button><br />
    <button name="button" value="main">Main Engines</button><br />
    <button name="button" value="aux">Aux Engines</button><br />
    <button name="button" value="sdh">Survey History</button><br />
    <button name="button" value="sdd">Survey Due Date</button><br />
    <button name="button" value="tz">Trading Zone</button><br />
    <button name="button" value="nameHistory">Name History</button><br />
</form>

