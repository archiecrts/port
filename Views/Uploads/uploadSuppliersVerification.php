<?php
$optGroups = ['Supplier', 'Approval Status', 'Address', 'Contact 1',
    'Contact 2', 'Contact 3'];

$dbInstallationArray = [
    'cust_name' => ['Managing Company (Customer)' => 'Supplier'],
    'cust_telephone' => ['Main Telephone Number' => 'Supplier'],
    'cust_fax' => ['Main Fax Number' => 'Supplier'],
    'cust_website' => ['Website' => 'Supplier'],
    'cust_account_manager' => ['Account Manager' => 'Supplier'],
    'cust_type' => ['Customer Type' => 'Supplier'],
    'cust_tbh_id' => ['TBH ID' => 'Supplier'],
    'cust_tbh_account_code' => ['TBH Account Code' => 'Supplier'],
    'cust_ld_id' => ['LINCOLN ID' => 'Supplier'],
    'cust_ld_account_code' => ['LINCOLN Account Code' => 'Supplier'],
    'cust_header_notes' => ['Header Notes' => 'Supplier'],
    
    'sat_approved_as_supplier' => ['Approved As Supplier' => 'Approval Status'],
    'sat_approved_as_trader' => ['Approved As Trader' => 'Approval Status'],
    'sat_approved_as_agent' => ['Approved As Agent' => 'Approval Status'],
    'supplier_approval_status_lookup_tbl_saslt_id' => ['Approval Level' => 'Approval Status'],
    'sat_date_approval_status_confirmed' => ['Date Status Confirmed' => 'Approval Status'],
    'sat_status_review_date' => ['Review Date' => 'Approval Status'],
    'sat_reason_for_status_change' => ['Reason Status Changed' => 'Approval Status'],
    
    'cadt_office_name' => ['Office Name' => 'Address'],
    'cadt_address' => ['Address' => 'Address'],
    'cadt_city' => ['City' => 'Address'],
    'cadt_state' => ['State' => 'Address'],
    'cadt_country' => ['Country' => 'Address'],
    'cadt_postcode' => ['Postcode' => 'Address'],
    'cadt_default_address' => ['Default Address' => 'Address'],
    
    'ict_salutation_1' => ['Contact Salutation 1' => 'Contact 1'],
    'ict_first_name_1' => ['First Name 1' => 'Contact 1'],
    'ict_surname_1' => ['Surname 1' => 'Contact 1'],
    'ict_job_title_1' => ['Job Title 1' => 'Contact 1'],
    'ict_email_1' => ['Contact Email 1' => 'Contact 1'],
    'ict_number_1' => ['Contact Telephone 1' => 'Contact 1'],
    'ict_mobile_number_1' => ['Contact Mobile No. 1' => 'Contact 1'],
    'ict_info_source_1' => ['Details From 1' => 'Contact 1'],
    
    'ict_salutation_2' => ['Contact Salutation 2' => 'Contact 2'],
    'ict_first_name_2' => ['First Name 2' => 'Contact 2'],
    'ict_surname_2' => ['Surname 2' => 'Contact 2'],
    'ict_job_title_2' => ['Job Title 2' => 'Contact 2'],
    'ict_email_2' => ['Contact Email 2' => 'Contact 2'],
    'ict_number_2' => ['Contact Telephone 2' => 'Contact 2'],
    'ict_mobile_number_2' => ['Contact Mobile No. 2' => 'Contact 2'],
    'ict_info_source_2' => ['Details From 2' => 'Contact 2'],
    
    'ict_salutation_3' => ['Contact Salutation 3' => 'Contact 3'],
    'ict_first_name_3' => ['First Name 3' => 'Contact 3'],
    'ict_surname_3' => ['Surname 3' => 'Contact 3'],
    'ict_job_title_3' => ['Job Title 3' => 'Contact 3'],
    'ict_email_3' => ['Contact Email 3' => 'Contact 3'],
    'ict_number_3' => ['Contact Telephone 3' => 'Contact 3'],
    'ict_mobile_number_3' => ['Contact Mobile No. 3' => 'Contact 3'],
    'ict_info_source_3' => ['Details From 3' => 'Contact 3'],
];
?>
<script>
    /* This function is given an array and returns an array of duplicated values if they exist */
    function identifyDuplicatesFromArray(arr) {
        var i;
        var len = arr.length;
        var obj = {};
        var duplicates = [];

        for (i = 0; i < len; i++) {

            if (!obj[arr[i]]) {

                obj[arr[i]] = {};

            }

            else
            {
                duplicates.push(arr[i]);
            }

        }
        return duplicates;
    }
    /* This function gathers all the selected options and puts them in an array to check for duplicates */
    $(function () {
        $('#form').on('submit', function (e) {
            e.preventDefault(); // prevent the usual submit
            var checkArray = [];

            $("select").each(function (index) {
                var selected = $(this).find(":selected").text();

                if (selected !== "Not Required") {
                    checkArray.push(selected);
                }
            });

            var result = identifyDuplicatesFromArray(checkArray);
            if (result.length < 1) {
                this.submit();
            } else {
                alert("You have used the same column name twice.\r\n Option(s) used " + result);
            }
        });
    });
</script>

<h3>FILE: <?php echo $name; ?></h3>
<form id="form" name="form" action="index.php?portal=suppliers&page=upload&action=upload" method="post" enctype="multipart/form-data">
    <input type="hidden" name="fileName" value="<?php echo $fileName; ?>"/>
    Column Names are included in file <input type="checkbox" name="headers" value="1" checked/>
    <table>
        <tr>

<?php

if (is_array($array) || $array instanceof Traversable) {
    foreach ($array as $key => $fileColumnName) {
        
        if ($fileColumnName != " ") {
            print "<td><strong>" . $fileColumnName . "</strong><br/>[<span class='tinyText'>Example: " . $lineOfText[$key] . "</span>]</td>";
        }
    }
}

?>
        </tr>
        <tr>

            <?php
            if (is_array($array) || $array instanceof Traversable) {
                foreach ($array as $key => $fileColumnName) {

                    print "<td><select name='" . $key . "_" . $fileColumnName . "'>";
                    print "<option value=''>Not Required</option>";
                    foreach ($optGroups as $optgroup) {
                        print "<optgroup label='" . $optgroup . "'>";
                        $group = $optgroup;
                        foreach ($dbInstallationArray as $key => $array1) {
                            foreach ($array1 as $name => $type) {
                                 if ($type === $group) {
                                    // match the file column names to the database values.
                                    if (strcasecmp($fileColumnName, $name) == 0) {
                                        print "<option value='$key' selected>$name</option>";
                                    } else {
                                        print "<option value='$key'>$name</option>";
                                    }
                                }
                            }
                        }
                        print "</optgroup>";
                    }

                    print "</select></td>";
                }
            }
            
            ?>
        </tr>
    </table>


    <input type="submit" name="submitVerify" value="Import Data"/>
</form>