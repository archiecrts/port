<h3>Upload Files</h3>
<script>
    function checkFileExists(destination) {

        var x = document.getElementById("file");
        var server = '<?php print $_SESSION['server']; ?>';

        if ('files' in x) {
            if (x.files.length > 0) {
                for (var i = 0; i < x.files.length; i++) {
                    var file = x.files[i];
                    if ('name' in file) {
                        // This is annoying but the checkFileExists... file is unable to see the session variable. There should be no reason for this.
                        // So I am going to have to pass it manually.
                        $.ajax({url: 'Views/Uploads/checkFileExistsBeforeImport.php?destination=' + destination + '&file=' + file.name + '&server=' + server,
                            success: function (output) {
                                //alert(output);
                                if (output > 0) {

                                    var html = "<h4>File already exists</h4>";
                                    html = "<p>Minor Update? <input type='checkbox' name='minor' value='1'/>(No notice will be posted to the Notice Board)</p>";
                                    html += "<p>Notice Board Comment <input type='text' name='comment' value=''/></p>";
                                    html += "<input type='hidden' name='isUpdate' value='update'/></p>";

                                    document.getElementById("updateHtml").innerHTML = html;
                                } else {
                                    var html = '<p>Add a notification to the notice board <input type="checkbox" name="sendNotification" value="1"/></p>';
                                    document.getElementById("updateHtml").innerHTML = html;
                                }


                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(xhr.status + " " + thrownError);
                            }
                        });
                    }
                }
            }
        }
    }
</script>
<?php
include_once("Controllers/UserHistoryController.php");
$userHistoryController = new UserHistoryController();

if (isset($_GET['action'])) {
    $action = $_GET['action'];
    $destination = $_GET['destination'];
} else {
    $action = "";
}
switch ($action) {
    case "process":


        // Check that file is .csv
        $fileExt = explode('.', $_FILES["file"]["name"]);

        if ($_FILES["file"]["error"] == 1) {
            print "<p class='error'>Your file exceeds the server level max file size of 15MB</p>";
        }
        if ($_FILES["file"]["error"] == 2) {
            print "<p class='error'>Your file exceeds the HTML FORM level max file size</p>";
        }
        if ($_FILES["file"]["error"] == 3) {
            print "<p class='error'>Your file was only partially uploaded</p>";
        }
        if ($_FILES["file"]["error"] == 4) {
            print "<p class='error'>No file was uploaded</p>";
        }
        if ($_FILES["file"]["error"] == 0) {
            $tmp_name = $_FILES["file"]["tmp_name"];
            $name = $_FILES["file"]["name"];
            move_uploaded_file($tmp_name, "files/$destination/$name");

            // Log in the DB
            $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "other", "New document added : files/" . $destination . "/" . addslashes($name));


            include_once 'Controllers/DocumentsController.php';
            include_once 'Controllers/NoticeBoardCategoriesController.php';
            $documentsController = new DocumentsController();
            $noticeBoardCategoriesController = new NoticeBoardCategoriesController();
            $category = $noticeBoardCategoriesController->getCategoryByName("Documents");

            // Make an entry in the table or update an existing entry.
            if (isset($_POST['isUpdate'])) {
                //   if ($destination == 'policies') {
                // update the record.
                $documentObject = new Document("", addslashes("files/$destination/$name"), date("Y-m-d h:i:s", time()), $_POST['readByDate'], '0', $destination);
                $document = $documentsController->updateDocument($documentObject);


                if (!isset($_POST['minor'])) {

                    $article = new NoticeBoard("", "Update to $name", date("Y-m-d h:i:s", time()), addslashes("<p>The HR team have just uploaded an amendment to the above policy.<br/>Please read and mark as read by <strong>" . date("d M Y", strtotime($_POST['readByDate'])) . "</strong>.</p>"
                                    . "<p>The following comments were attached to the upload: <span class='red'>" . $_POST['comment'] . "</span></p>"), $_SESSION['user_id'], $category->categoryID, '0', '1', $document);
                    $createNotice = $noticeBoardController->setNewNoticeArticle($article);
                }
                // }
            } else {
                // add a new record.
                //  if ($destination == 'policies') {

                $documentObject = new Document("", addslashes("files/$destination/$name"), date("Y-m-d h:i:s", time()), $_POST['readByDate'], '0', $destination);
                $document = $documentsController->addDocument($documentObject);

                if (isset($_POST['sendNotification'])) {

                    $article = new NoticeBoard("", "New Document Added - " . $name . "", date("Y-m-d h:i:s", time()), addslashes("<p>The HR team have just uploaded a new document in " . $destination . "->" . $name . ".<br/>Please read and mark as read by <strong>" . date("d M Y", strtotime($_POST['readByDate'])) . "</strong>.</p>"), $_SESSION['user_id'], $category->categoryID, '0', '1', $document);

                    $createNotice = $noticeBoardController->setNewNoticeArticle($article);
                }
                // Each user needs a training table record for this document so they can mark it as read.
                if ($destination == 'policies') {
                    $users = $userController->getUsers();

                    include_once 'Controllers/TrainingController.php';
                    $trainingController = new TrainingController();

                    foreach ($users as $user => $object) {
                        $addDocument = $trainingController->addDocumentToUser($object->id, $document);
                    }
                }
                //}
            }

            print '<meta http-equiv="refresh" content="0;url=index.php?portal=intranet&page=' . $destination . '">';
        }



    default:
        ?>
        <form name="form" action='index.php?portal=intranet&page=upload&action=process&destination=<?php echo $_GET['destination']; ?>' method="post" enctype="multipart/form-data">

            <p><input type="file" name="file" id="file" onchange="checkFileExists('<?php echo $_GET['destination']; ?>')"/> (up to 15MB)</p>
            <p>Must be marked as read by <input type="text" name="readByDate" value="<?php echo date("Y-m-d", strtotime("+1 month", time())); ?>"/></p>
            <div id="updateHtml"><p>Add a notification to the notice board <input type="checkbox" name="sendNotification" value="1"/></p></div>


            <p><input type="submit" name="submit" value="Submit"/></p>

        </form>

        <?php
        break;
}
