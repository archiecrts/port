<?php
$string = date('Y-m-d', time());
$date = DateTime::createFromFormat("Y-m-d", $string);
//echo $date->format("d");

?>
<select name="actionDueDay">
                    <option value=""></option>
                    <?php
                    
                    for($day = 1; $day < 32; $day++) {
                        $selectedDay;
                        if(($date->format("d") == $day) && (isset($_GET['action']))) {
                            $selectedDay = "selected";
                        } else {
                            $selectedDay = "";
                        }
                        print '<option value="'.$day.'" '.$selectedDay.'>'.$day.'</option>';
                    }
                    ?>
                </select>
                <select name="actionDueMonth">
                    <option value=""></option>
                    <?php
                    $monthArray = ['01', '02', '03', '04',
                        '05', '06', '07', '08', '09', '10',
                        '11', '12'];
                    foreach($monthArray as $month) {
                        $selectedMonth;
                        if(($date->format("m") == $month) && (isset($_GET['action']))) {
                            $selectedMonth = "selected";
                        } else {
                            $selectedMonth = "";
                        }
                        print '<option value="'.$month.'" '.$selectedMonth.'>'.$month.'</option>';
                    }
                    ?>
                </select>
                
                <select name="actionDueYear">
                    <option value=""></option>
                    <?php
                    $currentYear = date("Y", time());
                    $year = date("Y", time());

                    for($year; $year < ($currentYear+5); $year++) {
                        $selectedYear;
                        if(($date->format("Y") == $year) && (isset($_GET['action']))) {
                            $selectedYear = "selected";
                        } else {
                            $selectedYear = "";
                        }
                        print '<option value="'.$year.'" '.$selectedYear.'>'.$year.'</option>';
                    }
                    
                    ?>
                </select>