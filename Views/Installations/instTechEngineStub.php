<table  style="width:100%;border-collapse:collapse; table-layout:fixed;"  >
    <tr>
        <td rowspan='2' style="word-wrap:break-word; font-weight: bold;">
            <?php
            if (strpos($productObject->productUnitName, "UPDATED") !== false) {
                echo "<br/><span class='red'>" . htmlXentities($productObject->productUnitName) . "</span>";
            } else {
                echo stripslashes($installation->installationName) . " " . $typeOfProduct->toplShortCode . " " . $typeCountArray[$typeOfProduct->toplID];
            }
            ?>
        </td>
        <td class="title">Manufacturer</td>
        <td class="title">Designer</td>
        <td class="title">Cyl. #</td>
        <td class="title">Config.</td>
        <td class="title">Series</td>
        <td class="title">Model</td>
        <td class="title">Bore/Stroke</td>
        <td class="title">Fuel</td>
        <td class="title">Output</td>
        <td class="title">Load</td>
        <td class='buttonBar'><?php /*if ($newID != 'Engine') {*/ ?> <!--<a href="index.php?portal=mr&page=report&action=showInstallation&customerID=<?php echo $customerID; ?>&installationID=<?php echo $installation->installationID; ?>&tab=technical&reportID=<?php echo $reportOverviewID; ?>&edit=edit&productID=<?php echo $key; ?>">Edit</a>--> <?php //} ?>
        </td>

    </tr>
    <tr>

        <td><?php echo htmlXentities($engineObject->engineBuilder); ?></td>
        <td><?php echo htmlXentities($designerLookupController->getDesignerByVersionLookupID($productObject->versionLookupID)->designerDescription); ?><br/>
            <?php if ($designerLookupController->getDesignerByDesignerID($productObject->sourceAliasID)->designerDescription != "") {
                echo "(" . htmlXentities($designerLookupController->getDesignerByDesignerID($productObject->sourceAliasID)->designerDescription) . ")";
            } ?></td>
        <td><?php echo $version->cylinderCount; ?></td>
        <td><?php echo $version->cylinderConfiguration; ?></td>
        <td><?php echo htmlXentities($seriesLookupController->getSeriesByVersionLookupID($productObject->versionLookupID)->seriesDescription); ?></td>
        <td><?php echo htmlXentities($engineObject->modelDescription); ?></td>
        <td><?php echo $engineObject->boreSize; ?> / <?php echo $engineObject->stroke; ?></td>
        <td><?php echo $engineObject->fuelType; ?></td>
        <td><?php echo $engineObject->output . " " . $engineObject->outputUnitOfMeasurement; ?></td>
        <td><?php echo $loadPriorityController->getLoadPriorityByID($engineObject->loadPriorityID)->loadPriority; ?></td>
        <td class='buttonBar'><?php if ($newID != 'Engine') { ?> <a href="index.php?portal=mr&page=report&action=showInstallation&customerID=<?php echo $customerID; ?>&installationID=<?php echo $installation->installationID; ?>&tab=technical&reportID=<?php echo $reportOverviewID; ?>&edit=add">Add New</a> <?php } ?>

<!--<a href="index.php?portal=mr&page=report&action=showInstallation&customerID=<?php echo $customerID; ?>&installationID=<?php echo $installationID; ?>&tab=technical&reportID=<?php echo $reportOverviewID; ?>&move=move&productID=<?php echo $key; ?>">Move</a>--></td>
    </tr>
    <tr>
        <td></td>
        <td class="title">Drawing #</td>
        <td class="title">Serial #</td>
        <td class="title">Main/Aux</td>
        <td class="title">Position if Main</td>
        <td class="title">Release</td>
        <td class="title" colspan='4'>Notes</td>
        <td class="title">Verified</td>
        <td class='buttonBar'><a href='index.php?portal=mr&page=report&action=showTechnicalHistory&installationID=<?php echo $installationID; ?>&productID=<?php echo $key; ?>&position=<?php echo $engineObject->positionIfMain; ?>'>History</a></td>
    </tr>
    <tr>
        <td></td>
        <td><?php echo $productObject->productDrawingNumber; ?></td>
        <td><?php echo $productObject->productSerialNumber; ?></td>
        <td><?php echo $engineObject->mainOrAux; ?></td>
        <td><?php echo $engineObject->positionIfMain; ?></td>
        <td><?php
            if (($engineObject->releaseDate != "0000-00-00 00:00:00") && ($engineObject->releaseDate != null)) {
                echo date("Y", strtotime($engineObject->releaseDate));
            } else {
                echo "Unknown";
            }
            ?></td>
        <td colspan='4' style="border:1px solid lightsteelblue;"><?php echo $productObject->productComment; ?></td>
        <td style="font-weight:bold;"><?php
            if ($engineObject->engineVerified == 1) {
                echo '<span class="green">Yes</span>';
            } else {
                echo '<span class="red">No</span>';
            }
            ?></td>
        <td class='buttonBar'></td>
    </tr>
    <tr>
        <td colspan="9"></td>
        <td></td>
    </tr>

</table>