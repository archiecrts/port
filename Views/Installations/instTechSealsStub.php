<table  style="width:100%;border-collapse:collapse; table-layout:fixed;"  >
    <tr>
        <td rowspan='2' style="word-wrap:break-word; font-weight: bold;">
            <?php
            if (strpos($productObject->productUnitName, "UPDATED") !== false) {
                echo "<br/><span class='red'>" . htmlXentities($productObject->productUnitName) . "</span>";
            } else {
                echo stripslashes($installation->installationName) . " " . $typeOfProduct->toplShortCode . " " . $typeCountArray[$typeOfProduct->toplID];
            }
            ?>
        </td>
        <td class="title">Manufacturer</td>
        <td class="title">Designer</td>
        <td class="title">Series</td>
        <td class="title">Model</td>
        <td class="title">Drawing #</td>
        <td class="title">Serial #</td>
        <td class="title">Release</td>
        <td class="title">Verified</td>
        <td class='buttonBar'><?php if ($newID != 'Engine') { ?> <a href="index.php?portal=mr&page=report&action=showInstallation&customerID=<?php echo $customerID; ?>&installationID=<?php echo $installation->installationID; ?>&tab=technical&reportID=<?php echo $reportOverviewID; ?>&edit=edit&productID=<?php echo $key; ?>">Edit</a> <?php } ?>
        </td>

    </tr>
    <tr>

        <td><?php echo htmlXentities($engineObject->engineBuilder); ?></td>
        <td><?php echo htmlXentities($designerLookupController->getDesignerByVersionLookupID($productObject->versionLookupID)->designerDescription); ?><br/>
            <?php if ($designerLookupController->getDesignerByDesignerID($productObject->sourceAliasID)->designerDescription != "") {
                echo "(" . htmlXentities($designerLookupController->getDesignerByDesignerID($productObject->sourceAliasID)->designerDescription) . ")";
            } ?></td>
        <td><?php echo htmlXentities($seriesLookupController->getSeriesByVersionLookupID($productObject->versionLookupID)->seriesDescription); ?></td>
        <td><?php echo htmlXentities($engineObject->modelDescription); ?></td>
        <td><?php echo $productObject->productDrawingNumber; ?></td>
        <td><?php echo $productObject->productSerialNumber; ?></td>
        <td><?php
            if (($engineObject->releaseDate != "0000-00-00 00:00:00") && ($engineObject->releaseDate != null)) {
                echo date("Y", strtotime($engineObject->releaseDate));
            } else {
                echo "Unknown";
            }
            ?></td>
        <td style="font-weight:bold;"><?php
            if ($engineObject->engineVerified == 1) {
                echo '<span class="green">Yes</span>';
            } else {
                echo '<span class="red">No</span>';
            }
            ?></td>
        <td class='buttonBar'><?php if ($newID != 'Engine') { ?> <a href="index.php?portal=mr&page=report&action=showInstallation&customerID=<?php echo $customerID; ?>&installationID=<?php echo $installation->installationID; ?>&tab=technical&reportID=<?php echo $reportOverviewID; ?>&edit=add">Add New</a> <?php } ?>

<!--<a href="index.php?portal=mr&page=report&action=showInstallation&customerID=<?php echo $customerID; ?>&installationID=<?php echo $installationID; ?>&tab=technical&reportID=<?php echo $reportOverviewID; ?>&move=move&productID=<?php echo $key; ?>">Move</a>--></td>
    </tr>
    <tr>
        <td></td>
        <td class="title" colspan='8'>Notes</td>
        <td class='buttonBar'><a href='index.php?portal=mr&page=report&action=showTechnicalHistory&installationID=<?php echo $installationID; ?>&productID=<?php echo $key; ?>&position=<?php echo $engineObject->positionIfMain; ?>'>History</a></td>
    </tr>
    <tr>
        <td></td>
        <td colspan='8' style="border:1px solid lightsteelblue;"><?php echo $productObject->productComment; ?></td>
        <td class='buttonBar'>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="9"></td>
        <td></td>
    </tr>

</table>