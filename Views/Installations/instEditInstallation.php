<script src="./js/jquery.are-you-sure.js"></script>
<script src="./js/ays-beforeunload-shim.js"></script>

<script>
    $(function () {

        $('#instForm').areYouSure();

        $('#locationType').on('change', function () {
            var locationType = $(this).find(":selected").val();
            //alert( locationType );
            if (locationType === 'MARINE') {
                $('#marineDetails').css('display', 'block');
            } else {
                $('#marineDetails').css('display', 'none');
            }
        });
    });
</script>

<div id='installationContainer'>
    <div id="installationHeader">
        <img src="./icons/crm_grey.png" alt="" height='40px'/>
        <h2>Company Information</h2>
    </div>

    <div id='installationSubHeader'>
        <h4><?php echo stripslashes($installationObject->installationName); ?> - <?php echo $customer->customerName; ?></h4>
    </div>

    <div id='installationContentWide'>

        <form name="instForm" id="instForm" action="index.php?portal=mr&page=report&action=showInstallation&customerID=<?php echo $customerID; ?>&installationID=<?php echo $installationObject->installationID; ?>&tab=installation&reportID=<?php echo $reportOverviewID; ?>&save=save" method="post" enctype="multipart/form-data">
            <p><input type="submit" name="submit" value="Save"></p>
            <table>
                <tr>
                    <td class="title">Installation Name</td>
                    <td><input type="text" name="installationName" value="<?php echo stripslashes($installationObject->installationName); ?>"</td>
                    <td class="title">Installation Type</td>
                    <td><input type="text" name="installation" value="<?php echo $installationObject->installation; ?>"</td></td>
                </tr>
                <tr>
                    <td class="title">IMO Number (if applicable)</td>
                    <td><input type="text" name="imo" value="<?php echo $installationObject->imo; ?>"/></td>
                    <td class="title">LAND / MARINE</td>
                    <td><select name="type" id="locationType">
                            <?php
                            $typeArray = ['LAND', 'MARINE'];
                            foreach ($typeArray as $val) {
                                $selected = "";
                                if ($val == $installationObject->type) {
                                    $selected = "selected='selected'";
                                }
                                print '<option value="' . $val . '" ' . $selected . '>' . $val . '</option>';
                            }
                            ?>
                        </select>
                    </td>
                </tr>

                <tr>

                    <td class="title"><!--Owner / Parent Company--></td>
                    <td><!--<select name="operatingCompany">
                            <option value="">None Selected</option>
                            <?php
                            /*$operatorArray = $customerController->getAllCustomers();
                            foreach ($operatorArray as $id => $customer) {
                                $selected = "";
                                if ($installationObject->operatingCompany == $id) {
                                    $selected = "selected='selected'";
                                }
                                print "<option value='$id' $selected>$customer->customerName</option>";
                            }*/
                            ?>
                        </select>--></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>

                    <td class="title"><!--Technical Management Company--></td>
                    <td style="max-width:200px;">
                        <!--<select name="technicalManager">
                            <option value="">None Selected</option>
                            <?php
                            //$technicalArray = $customerController->getAllCustomers();
                            /*foreach ($operatorArray as $id => $customer) {

                                $selected = "";
                                if ($installationObject->technicalManager == $id) {
                                    $selected = "selected='selected'";
                                }
                                print "<option value='$id' $selected>$customer->customerName</option>";
                            }*/
                            ?>
                        </select>-->
                    </td>
                    <td></td>
                    <td></td>
                </tr>

                <tr>
                    <td class="title">Status</td>
                    <td><select name="status">
                            <?php
                            $instStatusArray = $installationStatusController->getAllInstallationStatusObjects();
                            $selected = "";
                            foreach ($instStatusArray as $id => $status) {
                                if ($id == $installationObject->status) {
                                    $selected = "selected='selected'";
                                } else {
                                    $selected = "";
                                }
                                print "<option value='" . $id . "' $selected>" . $status->statusDescription . " - (" . $status->type . ")</option>";
                            }
                            ?>

                        </select></td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td class="title">Original Source</td>
                    <td><input type="text" name="source" value="<?php echo $installationObject->originalSource; ?>"/></td>
                    <td class="title">Flag Name</td>
                    <td><input type="text" name="flagName" value="<?php echo $installationObject->flagName; ?>"/></td>
                </tr>
                <tr>
                    <td class="title">Original Source Date</td>
                    <td><input type="text" name="sourceDate" value="<?php
                        if ($installationObject->originalSourceDate != "0000-00-00 00:00:00") {
                            $date = date("Y", strtotime($installationObject->originalSourceDate));
                            echo trim($date);
                        } else {
                            echo null;
                        }
                        ?>"
                               /></td>
                    <td class="title"></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="title">Archive Installation</td>
                    <?php
                    $selectedFlag = '';
                    if ($installationObject->archiveFlag == 1) {
                        $selectedFlag = 'checked="checked"';
                    }
                    ?>
                    <td><input type="checkbox" name="archiveFlag" value="1" <?php print $selectedFlag; ?>/></td>
                    <td class="title">Archive Reason</td>
                    <td><input type="text" name="archiveReason" value="<?php echo $installationObject->archiveReason; ?>"/></td>
                </tr>
            </table>
            <?php
            $style = 'style="display:block;"';

            if ($installationObject->type == 'LAND') {
                $style = 'style="display:none;"';
            }
            ?>
            <table id="marineDetails" <?php print $style; ?>>
                <tr><td colspan="4">Marine Only Details</td></tr>
               <!-- <tr>
                    <td class="title">Hull Number</td>
                    <td><input type="text" name="hullNumber" value="<?php echo $installationObject->hullNumber; ?>"/></td>
                    <td class="title">Hull Type</td>
                    <td><input type="text" name="hullType" value="<?php echo $installationObject->hullType; ?>"/></td>
                </tr>-->
                <tr>
                    <td class="title">Yard Built</td>
                    <td><input type="text" name="yardBuilt" value="<?php echo $installationObject->yardBuilt; ?>"/></td>
                    <td class="title">Built Date</td>
                    <td><input type="text" name="builtDate" value="<?php echo date('Y-m', strtotime($installationObject->builtDate)); ?>"/></td>
                </tr>
                <!--<tr>
                    <td class="title">Length Overall</td>
                    <td><input type="text" name="lengthOverall" value="<?php echo $installationObject->lengthOverall; ?>"/></td>
                    <td class="title">Length</td>
                    <td><input type="text" name="length" value="<?php echo $installationObject->length; ?>"/></td>
                </tr>
                <tr>
                    <td class="title">Beam Extreme</td>
                    <td><input type="text" name="beamExtreme" value="<?php echo $installationObject->beamExtreme; ?>"/></td>
                    <td class="title">Beam Moulded</td>
                    <td><input type="text" name="beamMoulded" value="<?php echo $installationObject->beamMoulded; ?>"/></td>
                </tr>
                <tr>
                    <td class="title">Deadweight</td>
                    <td><input type="text" name="deadweight" value="<?php echo $installationObject->deadWeight; ?>"/></td>
                    <td class="title">Draught</td>
                    <td><input type="text" name="draught" value="<?php echo $installationObject->draught; ?>"/></td>
                </tr>-->
                <tr>
                    <!--<td class="title">Gross Tonnage</td>
                    <td><input type="text" name="grossTonnage" value="<?php echo $installationObject->grossTonnage; ?>"/></td>-->
                    <td class="title"><!--Build Year--></td>
                    <td><!--<input type="text" name="buildYear" value="<?php echo date('Y', strtotime($installationObject->buildYear)); ?>"/>--></td>
                    <td class="title">Ship Builder</td>
                    <td><input type="text" name="shipBuilder" value="<?php echo $installationObject->shipBuilder; ?>"/></td>
                </tr>
                <tr>
                    <td class="title">Propeller Type</td>
                    <td><input type="text" name="propellerType" value="<?php echo $installationObject->propellerType; ?>"/></td>
                    <td class="title">Propulsion Unit Count</td>
                    <td><input type="text" name="propulsionUnitCount" value="<?php echo $installationObject->propulsionUnitCount; ?>"/></td>
                </tr>
                <tr>
                    <!--<td class="title">TEU</td>
                    <td><input type="text" name="teu" value="<?php echo $installationObject->teu; ?>"/></td>-->
                    <td class="title">Classification Soc.</td>
                    <td><input type="text" name="classificationSociety" value="<?php echo $installationObject->classificationSociety; ?>"/></td>
                    <td class="title">Detailed Installation Type</td>
                    <td><input type="text" name="shipTypeLevel4" value="<?php echo $installationObject->shipTypeLevel4; ?>"/></td>
                </tr>
            </table>
        </form>       

    </div>
</div>