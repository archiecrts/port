<h3>Product History</h3>

<p><a href="index.php?portal=mr&page=report&action=showInstallation&installationID=<?php echo $_GET['installationID']; ?>&customerID=&submit=Go">Back to installation</a></p>

<?php
$historyOfProduct = $productHistoryController->getProductHistory($_GET['productID']);


if (isset($historyOfProduct) && ($historyOfProduct != false)) {
    foreach ($historyOfProduct as $key => $productObject) {
        $engineObject = $installationEngineController->getEngineByProductID($_GET['productID']);
        $typeOfProduct = $typeOfProductLookupController->getTypeOfProductByID($seriesLookupController->getSeriesByVersionLookupID($productObject->versionLookupTblVltID)->typeOfProductLookupID);
        $version = $versionLookupController->getVersionByID($productObject->versionLookupTblVltID);

        ?>
        <table  class="dataTable" style="width:100%;border-collapse:collapse; table-layout:fixed;">
            <tr>
                <td rowspan='2' style="width:150px; word-wrap:break-word; font-weight: bold;">
        <?php
        echo "Changed Date:<br/><span class='blue'>".htmlXentities($productObject->productHistoryChangedDate)."</span>";  
        echo "<br/> Changed By : <br/><span class='blue'>".$userController->getUserByID($productObject->productHistoryChangedByID)->name."</span>";
        ?>
                </td>
                <td class="title">Manufacturer</td>
                <td class="title">Designer</td>
                <td class="title">Cyl. #</td>
                <td class="title">Config.</td>
                <td class="title">Series</td>
                <td class="title">Model</td>
                <td class="title">Bore/Stroke</td>
                <td class="title">Fuel</td>
                <td class="title">Output</td>
                <td class="title">Load</td>


            </tr>
            <tr>

                <td><?php echo htmlXentities($engineObject->engineBuilder); ?></td>
                <td><?php echo htmlXentities($designerLookupController->getDesignerByVersionLookupID($productObject->versionLookupTblVltID)->designerDescription); ?><br/>
        <?php if ($productObject->sourceAliasID != "") {
            echo "(" . htmlXentities($designerLookupController->getDesignerByDesignerID($productObject->sourceAliasID)->designerDescription) . ")";
        } ?></td>
                <td><?php echo $version->cylinderCount; ?></td>
                <td><?php echo $version->cylinderConfiguration; ?></td>
                <td><?php echo htmlXentities($seriesLookupController->getSeriesByVersionLookupID($productObject->versionLookupTblVltID)->seriesDescription); ?></td>
                <td><?php echo htmlXentities($engineObject->modelDescription); ?></td>
                <td><?php echo $engineObject->boreSize; ?> / <?php echo $engineObject->stroke; ?></td>
                <td><?php echo $engineObject->fuelType; ?></td>
                <td><?php echo $engineObject->output . " " . $engineObject->outputUnitOfMeasurement; ?></td>
                <td><?php echo $loadPriorityController->getLoadPriorityByID($engineObject->loadPriorityID)->loadPriority; ?></td>
            </tr>
            <tr>
                <td></td>
                <td class="title">Drawing #</td>
                <td class="title">Serial #</td>
                <td class="title">Main/Aux</td>
                <td class="title">Position if Main</td>
                <td class="title">Release</td>
                <td class="title" colspan='4'>Notes</td>
                <td class="title">Verified</td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo $productObject->productDrawingNumber; ?></td>
                <td><?php echo $productObject->productSerialNumber; ?></td>
                <td><?php echo $engineObject->mainOrAux; ?></td>
                <td><?php echo $engineObject->positionIfMain; ?></td>
                <td><?php
            if ($engineObject->releaseDate != "0000-00-00 00:00:00") {
                echo date("Y", strtotime($engineObject->releaseDate));
            }
        ?></td>
                <td colspan='4' style="border:1px solid lightsteelblue;"><?php echo $productObject->productComments; ?></td>
                <td style="font-weight:bold;"><?php
            if ($engineObject->engineVerified == 1) {
                echo '<span class="green">Yes</span>';
            } else {
                echo '<span class="red">No</span>';
            }
        ?></td>

            </tr>
            <tr>
                <td colspan="9"></td>
                <td></td>
            </tr>

        </table>
        <hr/>
        <?php
    }
} else {
    print "<p>No history to display</p>";
}