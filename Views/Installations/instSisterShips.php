<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
<script>   
    
    $(function(){
        $('#sisterShipsTbl').tablesorter(); 
   });
   
   
    $(function () {
        //var scrolled = 0;
        $("#sisterShipsHeader").click(function () {
            
                if ($('#sisterShipsTbl').css('display') == 'none')
                {
                    $('#sisterShipsTbl').removeClass('hiddenBlock').addClass('showTable');
                } else {
                    $('#sisterShipsTbl').removeClass('showTable').addClass('hiddenBlock');
                }
        });
    });
 </script>
 <style>
     /* SORTER */
#sisterShipsTbl thead tr th.headerSortUp span { 
        /* background: #1D6191;*/
         background-image: url('./icons/arrow-up.png');
 
}

#sisterShipsTbl thead tr th.headerSortDown span {
   /* background: #9eafec;*/
  background-image: url('./icons/arrow-down.png');
  
}

#sisterShipsTbl thead {
  cursor: pointer;
  background: #c9dff0;
}
</style>
<div id="sisterShipsHeader">
    <div id="officeLocationsHeader"><img src="./icons/technicalDataIcon.png" alt="" height='20px'/> Sister Ships:</div>
</div>

<table style="width:100%;" id="sisterShipsTbl"  class='hiddenBlock'>
<thead>
    <tr>
        <th class='headerSortDown'>Vessel Name <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>
        <th>IMO <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>
        <th>Ship Manager <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>
        <th>Country <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>
    </tr>
</thead>
<tbody>
    <?php
    if (isset($sisterShipsList)) {
        foreach ($sisterShipsList as $k => $ss) {
            $customer = $customerController->getCustomerIDByInstallationID($ss->installationID);
            $customerAddress = $customerAddressController->getCustomerAddressByID($ss->customerAddressID);
            print "<tr>";
            print "<td><a href='index.php?portal=mr&page=report&action=showInstallation&customerID=".$customer->customerID."&installationID=" . $ss->installationID . "&tab=installation&reportID=" . $reportOverviewID . "' target='_blank'>$ss->installationName</a></td>";
            print "<td>$ss->imo</td>";
            print "<td>$customer->customerName</td>";
            print "<td>$customerAddress->country</td>";
            print "</tr>";
        }
    } else {
        print "<tr>";
        print "<td colspan='4'>Nothing to display</td>";
        print "</tr>";
    }
    ?>
</tbody>
</table>