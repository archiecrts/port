<script>   
    $(function () {
        //var scrolled = 0;
        $("#shipNameHistory").click(function () {
            
                if ($('#nameHistoryTbl').css('display') == 'none')
                {
                    $('#nameHistoryTbl').removeClass('hiddenBlock').addClass('showTable');
                } else {
                    $('#nameHistoryTbl').removeClass('showTable').addClass('hiddenBlock');
                }
        });
    });
 </script>
 <div id="shipNameHistory">

    <div id="officeLocationsHeader"><img src="./icons/technicalDataIcon.png" alt="" height='20px'/> Ship Name History (post 2000):
    </div>
</div>

<table style="width:100%;" id="nameHistoryTbl"  class='hiddenBlock'>

    <tr>
        <th>Sequence</th>
        <th>Vessel Name</th>
        <th>Effective Date</th>
    </tr>

    <?php
    if (isset($shipNameHistoryList)) {
        foreach ($shipNameHistoryList as $k => $snh) {
            print "<tr>";
            print "<td>$snh->shipNameHistorySequence</td>";
            print "<td>$snh->shipNameHistoryVesselName</td>";
            print "<td>".date("d-m-Y", strtotime($snh->shipNameHistoryEffectiveDate))."</td>";
            print "</tr>";
        }
    } else {
        print "<tr>";
        print "<td colspan='2'>Nothing to display</td>";
        print "</tr>";
    }
    ?>
</table>