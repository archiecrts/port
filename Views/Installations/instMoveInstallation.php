<script src="./js/jquery.are-you-sure.js"></script>
<script src="./js/ays-beforeunload-shim.js"></script>

<script>
  $(function() {
 
    $('#instForm').areYouSure();
    
  });
</script>

<div id='installationContainer'>
<div id="installationHeader">
    <img src="./icons/crm_grey.png" alt="" height='40px'/>
    <h2>Company Information</h2>
</div>

<div id='installationSubHeader'>
    <h4><?php echo $installation->installationName; ?> - <?php echo $customer->customerName; ?></h4>
</div>

<div id='installationContentWide'>

<form name="instForm" id="instForm" action="index.php?portal=mr&page=report&action=showInstallation&customerID=<?php echo $customerID; ?>&installationID=<?php echo $installation->installationID; ?>&tab=installation&reportID=<?php echo $reportOverviewID; ?>&save=move" method="post" enctype="multipart/form-data">
    <p><input type="submit" name="submit" value="Save"></p>
<table>
    <tr>
        <td class="title">Installation Name</td>
        <td><?php echo $installation->installationName; ?></td>
        <td class="title">Installation Type</td>
        <td><?php echo $installation->installation; ?></td>
    </tr>
    <tr>
        <td class="title">IMO Number (if applicable)</td>
        <td><?php echo $installation->imo; ?></td>
        <td class="title"></td>
        <td></td>
    </tr>
    
    <tr>
        
        <td class="title">Current Customer</td>
        <td><?php echo $customerController->getCustomerIDByInstallationID($installation->installationID)->customerName; ?></td>
            <td></td>
            <td></td>
    </tr>
    <tr>
        
        <td class="title">New Customer</td>
        <td style="max-width:200px;">
            <select name="newCustomer">
                <option value="">None Selected</option>
                <?php
                $newCustomerArray = $customerController->getAllCustomers();
                foreach ($newCustomerArray as $id => $customer) {
                    
                    print "<option value='$id'>$customer->customerName</option>";
                }
                ?>
            </select>
        </td>
        <td></td>
        <td></td>
    </tr>
    
    
    
</table>
</form>       
    
</div>
</div>