<?php
$productList['Engine'] = $productController->getProductsByInstallationIDAndProductType($installation->installationID, 1);
if ($installation->type == "MARINE"){
    $productList['Seals'] = $productController->getProductsByInstallationIDAndProductType($installation->installationID, 2);
    $productList['Oil Water Separator'] = $productController->getProductsByInstallationIDAndProductType($installation->installationID, 3);
}
$countOfProducts = $productController->countOfProducts($installation->installationID);
?>
<script>
    $(function () {
        //var scrolled = 0;
        $("#subBar_Engine").click(function () {

                if ($('#dataTable_Engine').css('display') == 'none')
                {
                    $('#dataTable_Engine').removeClass('hiddenBlock').addClass('showTable');
                } else {
                    $('#dataTable_Engine').removeClass('showTable').addClass('hiddenBlock');
                }


        });

        $("#subBar_Seals").click(function () {

                if ($('#dataTable_Seals').css('display') == 'none')
                {
                    $('#dataTable_Seals').removeClass('hiddenBlock').addClass('showTable');
                } else {
                    $('#dataTable_Seals').removeClass('showTable').addClass('hiddenBlock');
                }


        });

        $("#subBar_Oil_Water_Separator").click(function () {

                if ($('#dataTable_Oil_Water_Separator').css('display') == 'none')
                {
                    $('#dataTable_Oil_Water_Separator').removeClass('hiddenBlock').addClass('showTable');
                } else {
                    $('#dataTable_Oil_Water_Separator').removeClass('showTable').addClass('hiddenBlock');
                }


        });
    });
 </script>
<style>
    td {
        vertical-align: top;
    }

    .buttonBar {
        padding-left: 5px;
        background:#dbe4f0;
    }

    .subBar {
        height: 2em;
        background: #ededed;
        box-shadow: 2px 2px 2px #888888;
        padding-left: 10px;
        padding-top: 9px;
        font-weight: bold;
        margin-bottom: 5px;
    }
</style>
<div id="productsHeader">
<?php
if (isset($productList)) {
    $typeArray = $typeOfProductLookupController->getAllProductTypes();
    $typeCountArray = [];
    foreach ($typeArray as $key => $top) {
        $typeCountArray[$key] = 0;
    }
    foreach ($productList as $typeKey => $object) {
        $newID = preg_replace("/[\s]+/", "_", $typeKey);
        print "<div class='subBar' id='subBar_$newID'>$typeKey</div>";
        print '<div  id="dataTable_'.$newID.'" class="hiddenBlock">';
        if (isset($object)) {
            foreach ($object as $key => $productObject) {
                $engineObject = NULL;
                $engineCheck = $installationEngineController->getEngineByProductID($key);
                $typeOfProduct = $typeOfProductLookupController->getTypeOfProductByID($seriesLookupController->getSeriesByVersionLookupID($productObject->versionLookupID)->typeOfProductLookupID);


                $typeCountArray[$typeOfProduct->toplID] += 1;

                if (isset($engineCheck)) {
                    $engineObject = $engineCheck;
                }
                $version = $versionLookupController->getVersionByID($productObject->versionLookupID);

                if ($newID == 'Engine') {
                    include "Views/Installations/instTechEngineStub.php";
                } else {
                    include "Views/Installations/instTechSealsStub.php";
                }

                }
            } else {
                print "<p>No products to display</p>";
                if ($newID != 'Engine') { ?> <a href="index.php?portal=mr&page=report&action=showInstallation&customerID=<?php echo $customerID; ?>&installationID=<?php echo $installation->installationID; ?>&tab=technical&reportID=<?php echo $reportOverviewID; ?>&edit=add">Add New</a> <?php }
            }
            print "</div>";
        }
    } else {
        print "<p>No products to display</p>";
        ?>
    <img src="./icons/edit_pencil.png" alt="edit" height="15px"/> &nbsp; <a href="index.php?portal=mr&page=report&action=showInstallation&customerID=<?php echo $customerID; ?>&installationID=<?php echo $installation->installationID; ?>&tab=technical&reportID=<?php echo $reportOverviewID; ?>&edit=add">Add Product</a>

    <?php
}
?>
</div>
<br/>