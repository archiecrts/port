<script>   
    $(function () {
        //var scrolled = 0;
        $("#specialSurveyHeader").click(function () {
            
                if ($('#specialSurveyTbl').css('display') == 'none')
                {
                    $('#specialSurveyTbl').removeClass('hiddenBlock').addClass('showTable');
                } else {
                    $('#specialSurveyTbl').removeClass('showTable').addClass('hiddenBlock');
                }
        });
    });
 </script>
<div id="specialSurveyHeader">

    <div id="officeLocationsHeader"><img src="./icons/technicalDataIcon.png" alt="" height='20px'/> Special Survey Due and History:
    </div>
</div>
 <div id="specialSurveyTbl"  class='hiddenBlock'>
<h4>Survey Due Date</h4>
<table style="width:100%;">

    <tr>
        <th>Installation</th>
        <th>Special Survey Due Date</th>
    </tr>

    <?php
   
    if (($surveyDueList->specialSurveyDueDate != "0000-00-00") && ($surveyDueList->specialSurveyDueDate != "")) {
        if (strtotime($surveyDueList->specialSurveyDueDate) < time()) {
            $oldDate = strtotime($surveyDueList->specialSurveyDueDate);
            $t2 = strtotime('+5 years', $oldDate);
            while($t2 < time())  {
                $t2 = strtotime('+5 years', $t2);
            }
            print "<tr>";
            print "<td class='blue'>" . $installationController->getInstallationByInstallationID($surveyDueList->installationID)->installationName . "</td>";
            print "<td class='blue'>" . date("d-m-Y", $t2) . " (Projected Next Survey)</td>";
            print "<td></td>";
            print "</tr>";
        }
        print "<tr>";
        print "<td>" . $installationController->getInstallationByInstallationID($surveyDueList->installationID)->installationName . "</td>";
        print "<td>" . date("d-m-Y", strtotime($surveyDueList->specialSurveyDueDate)) . "</td>";
        print "<td></td>";
        print "</tr>";
    } else {
        print "<tr>";
        print "<td colspan='4'>Nothing to display</td>";
        print "</tr>";
    }

    // $surveyHistoryList;
    ?>
    
    </table>
    <h4>Survey History Dates</h4>
    <table style="width:100%;">

        <tr>
            <th>Installation</th>
            <th>Special Survey History Date</th>
        </tr>

<?php

$countSDH = 0;
if (isset($surveyHistoryList)) {
    foreach ($surveyHistoryList as $k => $sdh) {
        if (($sdh->specialSurveyHistoryDate != "0000-00-00") && ($sdh->specialSurveyHistoryDate != "")) {
            print "<tr>";
            print "<td>" . $installationController->getInstallationByInstallationID($sdh->installationID)->installationName . "</td>";
            print "<td>" . date("d-m-Y", strtotime($sdh->specialSurveyHistoryDate)) . "</td>";
            print "</tr>";
            $countSDH++;
        }
    }
}
if ($countSDH == 0) {
    print "<tr>";
    print "<td colspan='4'>Nothing to display</td>";
    print "</tr>";
}
?>
    </table>
 </div>