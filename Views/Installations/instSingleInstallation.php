<style>
    /* Tooltip container */
    .tooltip {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black; /* If you want dots under the hoverable text */
    }

    /* Tooltip text */
    .tooltip .tooltiptext {
        visibility: hidden;
        width: 120px;
        background-color: black;
        color: #fff;
        text-align: center;
        padding: 5px 0;
        border-radius: 6px;

        /* Position the tooltip text - see examples below! */
        position: absolute;
        z-index: 1;
    }

    /* Show the tooltip text when you mouse over the tooltip container */
    .tooltip:hover .tooltiptext {
        visibility: visible;
    }
    
    .countryFlag {
        margin-right:10px;
        float:right;
        position: relative;
    }
    
    .countryFlag img {
        margin-top: -12px;
        margin-left: 10px;
        vertical-align: top;
        height:40px;
    }
    
    /* Scroll menu */
    #menuScroll {
        float: left;
        position: fixed;
        top: 100px;
        width: 15%;
        padding-right: 5px;
        margin-left: -15px;
        font-size: 0.9em;
        line-height: 2.5em;
    }
    
    #menuScroll ul {
        list-style-type: none;
        margin: 0;
        padding: 10px;
    }
    
    #gotoMenu {
        background: #d3d3d4;
        padding: 3px;
        border-radius: 15px;
        box-shadow: 2px 2px 2px #888888;
    }
    
    #gotoMenu h4 {
        margin-left: 10px;
        margin-bottom: 0px;
    }
    
    #installationContainerTechnical {
        float: right;
        position: relative;
        width: 84%;
    }
</style>
<div id="menuScroll">
    <div id="gotoMenu">
        <h4>Go To</h4>
        <ul>
            <li><a href='#installationHeader'>Installation Information</a></li>
            <li><a href='#productsHeader'>Products</a></li>
            <li><a href="#tradingZoneHeader">Trading Zone Last Seen</a></li>
            <li><a href='#specialSurveyHeader'>Special Survey Data</a></li>
            <li><a href='#shipNameHistory'>Ship Name History</a></li>
            <li><a href='#sisterShipsHeader'>Sister Ships</a></li>
        </ul>
    </div>
</div>
<div id='installationContainerTechnical' style="padding-top: 110px; margin-top: -110px;">
    <div id="installationHeader">
        <div id="officeLocationsHeader">
            <img src="./icons/gear37.png" alt="" height='20px'/> Installation Information:  
        </div>
    </div>



    <?php
    if (isset($installationObject)) {
        
?>
        <div id="technical_<?php echo $installationObject->installationID; ?>" >
        <?php
        echo "<span style='font-weight:bold;'>".stripslashes($installationObject->installationName)." - ";
        echo $installationObject->imo."</span>";
        ?>
        <span class='countryFlag'>
        <?php
        if ($installationObject->flagName == "") {
            $installationObject->flagName = $customerAddressController->getCustomerAddressByID($installationObject->customerAddressID)->country;
        }
        echo $installationObject->flagName;
        $flagName = preg_replace("/[\s_]/", "-", $installationObject->flagName);
        
        if (strcasecmp ( $flagName , 'United-States-Of-America' ) == 0) {
            $flagName = 'United-States';
        }
        echo " <img src='./icons/Flags/".$flagName.".png' />";
        ?>
        </span>
    </div>
    
    <div id="technicalDataBlock_<?php echo $installationObject->installationID; ?>">
    <?php

            include("Views/Installations/instInstallation.php");
            $installation = $installationObject;
            include("Views/Installations/instTechnicalData.php");
            if ($installation->type == "MARINE") {
                $tradingZoneList = $tradingZoneLastSeenController->getAllTradingZonesForInstallation($installationObject->installationID);
                include("Views/Installations/instTradingZone.php");
                $surveyDueList = $specialSurveyDueDateController->getSpecialSurveyDueDateByInstallationID($installationObject->installationID);
                $surveyHistoryList = $specialSurveyHistoryDatesController->getSpecialSurveyHistoryDatesByInstallationID($installationObject->installationID);
                include("Views/Installations/instSurveyDateAndHistory.php");
                $shipNameHistoryList = $shipNameHistoryController->getAllShipNameHistoryForInstallation($installationObject->installationID);
                include("Views/Installations/instShipNameHistory.php");
                
                if ($installationObject->leadShipByIMO != "") {
                    $sisterShipsList = $installationController->getSisterShipsByLeadIMO($installationObject->leadShipByIMO);
                } else {
                    $sisterShipsList = $installationController->getSisterShipsByLeadIMO($installationObject->imo);
                }
                include("Views/Installations/instSisterShips.php");
            }
            ?>
        
        
    </div>
<?php
            
    } else {
        print "<table>";
        print "<tr>";
        print "<td colspan='10'>You have not created any installations. <a href='index.php?portal=mr&page=addNewInstallation&step=installation&customerID=$customerID'>You can create one here</a></td>";
        print "</tr>";
        print "</table>";
    }
?>

    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
</div>