<?php
$installation = $installationObject;
$address = $customerAddressController->getCustomerAddressByID($installation->customerAddressID);
$reportOverviewID = "";
// TM/OPS
$technicalManager = $customerController->getCustomerByID($installation->technicalManager);
$technicalManagerAddress = $customerAddressController->getDefaultCustomerAddressByCustomerID($installation->technicalManager);
$tmEmail = $customerContactsController->getFirstContactByCustomerID($installation->technicalManager);
// Physical Address Plant

// Mailing Address

// Plant manager email and phone.
?>

<table   class="dataTable" style="width:100%;">
    <tr>
        <td class="title">Land/Marine</td>
        <td><?php echo $installation->type; ?></td>
        <td class="title">Installation Manager</td>
        <td><a href="index.php?portal=mr&page=report&action=showCustomer&customerID=<?php echo $customerAddressController->getCustomerAddressByID($installation->customerAddressID)->customerID; ?>&installationID=<?php echo $installationID; ?>&tab=installation&reportID=<?php echo $reportOverviewID; ?>" target="_blank"><?php echo $customerController->getCustomerByID($customerAddressController->getCustomerAddressByID($installation->customerAddressID)->customerID)->customerName; ?></a></td>
        <th class="title">IM Country</th>
        <td><?php echo $address->country; ?></td>
        <td class="title" style='border-top:1px solid black;border-left:1px solid black;border-right:1px solid black;'>Original Source/Date</td>
    </tr>
    <tr>
        <td class="title">Type (general)</td>
        <td><?php echo $installation->installation; ?></td>
        <td class="title">Tech/Op. Company</td>
        <td><a href="index.php?portal=mr&page=report&action=showCustomer&customerID=<?php echo $technicalManager->customerID; ?>&installationID=<?php echo $installationID; ?>&tab=installation&reportID=<?php echo $reportOverviewID; ?>" target="_blank"><?php echo $technicalManager->customerName; ?></a></td>
        <td class="title">T/OP Country</td>
        <td><?php echo $technicalManagerAddress->country; ?></td>
        <td style='border-bottom:1px solid black;border-left:1px solid black;border-right:1px solid black;'><?php echo $installation->originalSource; ?> 
            <?php echo date("m-Y", strtotime($installation->originalSourceDate)); ?></td> 
    </tr>
    <tr>
        <td class="title">Type (detailed)</td>
        <td><?php echo $installation->shipTypeLevel4; ?></td>
        <td class="title">Group Owner</td>
        <td><a href="index.php?portal=mr&page=report&action=showCustomer&customerID=<?php echo $customerController->getCustomerByID($installation->groupOwner)->customerID; ?>&installationID=<?php echo $installationID; ?>&tab=installation&reportID=<?php echo $reportOverviewID; ?>" target="_blank"><?php echo $customerController->getCustomerByID($installation->groupOwner)->customerName; ?></a></td>
        <td class="title">GP Country</td>
        <td><?php echo $customerAddressController->getDefaultCustomerAddressByCustomerID($installation->groupOwner)->country; ?></td>
        <td class="title"></td>  
    </tr>
    <tr>
        <td class="title">Status</td>
        <td><?php echo $installationStatusController->getInstallationStatusByID($installation->status)->statusDescription; ?></td>
        <td class="title"></td>
        <td></td>
        <td colspan="3"></td>  
    </tr>
    <?php
    if ($installation->type == "MARINE") {
        // show the marine stuff.
        ?>
        <tr>
            <td class="title">Built Date</td><td><?php echo date("Y-m", strtotime($installation->builtDate)); ?></td>
            <td class="title">Ship Builder</td><td><?php echo $installation->shipBuilder; ?></td>
            
            <td class="title">Yard Number</td><td><?php echo $installation->yardBuilt; ?></td>
        </tr>
        <tr>
            <td class="title">Propeller Type</td><td><?php echo $installation->propellerType; ?></td>
            <td class="title">Propulsion Unit Count</td><td><?php echo $installation->propulsionUnitCount; ?></td>
            <td class="title">Classification Soc.</td><td><?php echo $installation->classificationSociety; ?></td>
        </tr>
        <?php
    } else {
        // addresses and plant manager details
        ?>
        
        <tr>
            <td class="title">TM/Op. Address</td>
            <td>
        
        <?php
        print "Office Name: ".$technicalManagerAddress->officeName."<br/>";
        print "Address: ".$technicalManagerAddress->address."<br/>";
        print "City: ".$technicalManagerAddress->city."<br/>";
        print "State: ".$technicalManagerAddress->state."<br/>";
        print "Country: ".$technicalManagerAddress->country."<br/>";
        print "Postcode: ".$technicalManagerAddress->postcode."<br/>";
        ?>
            </td>
            <td class="title">TM/Op. Contact Details</td>
            <td>
        <?php        
        
            print "Main Tel: ".$technicalManager->mainTelephone."<br/>";
            print "Main Fax: ".$technicalManager->mainFax."<br/>";
            print "Website: ".$technicalManager->website."<br/>";
            print "Email: ".$tmEmail->email ."<br/>";
        ?>
            </td><td></td><td></td></tr>
        <tr>
            <td class="title"></td><td></td>
            <td class="title"></td><td></td>
            <td class="title"></td><td></td>
        </tr>
    <?php
    }
    ?> 
    
    

    <tr>
        <td><a href="index.php?portal=mr&page=report&action=showInstallation&customerID=<?php echo $customerID; ?>&installationID=<?php echo $installation->installationID; ?>&tab=installation&reportID=<?php echo $reportOverviewID; ?>&edit=edit">Edit</a> 
            | <a href="index.php?portal=mr&page=report&action=installationHistory&customerID=<?php echo $customerID; ?>&installationID=<?php echo $installation->installationID; ?>&tab=installation&reportID=<?php echo $reportOverviewID; ?>">History</a> 
<!--| <a href="index.php?portal=mr&page=report&action=showInstallation&customerID=<?php echo $customerID; ?>&installationID=<?php echo $installation->installationID; ?>&tab=installation&reportID=<?php echo $reportOverviewID; ?>&move=move">Move</a>--></td>
    </tr>

    
</table>
