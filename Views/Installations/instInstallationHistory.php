<?php

/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

// Show the history in human readable form.

$installationID = $_GET['installationID'];

print '<span id="returnLink"><a href="index.php?portal=mr&page=report&action=showInstallation&installationID='.$installationID.'&customerID='.$_GET['customerID'].'">Return to Installation</a>'
            . ' <img src="./icons/returnArrow.png" alt="" height="15px" />'
            . '</span>';


print "<h3>".stripslashes($installationController->getInstallationByInstallationID($installationID)->installationName)."</h3>";
$installationHistory = $customerHasInstallationsHistoryController->getHistoryForInstallation($installationID);
if (isset($installationHistory)){
    print "<table>";
    foreach ($installationHistory as $key => $history) {
        print "<tr>";
        print "<td>".$customerController->getCustomerByID($history->customerID)->customerName."</td>";
        print "<td>".$userController->getUserByID($history->customerHasInstallationsHistoryChangedByID)->name."</td>";
        print "<td>".date("d<\s\u\p>S</\s\u\p> M Y H:i:s", strtotime($history->customerHasInstallationsChangedDate))."</td>";
        print "<tr>";
    }
    print "</table>";
}

print "<p>Currently attached to <strong>".$customerController->getCustomerIDByInstallationID($installationID)->customerName."</strong></p>";


// Log change history
?>
<h3>History of detail changes</h3>
<?php

$instDetailHistory = $installationHistoryController->getInstallationHistory($installationID);

if (isset($instDetailHistory) && (is_array($instDetailHistory))){
    print "<table  class='borderedTable dataTable' id='reportTbl'>";
    print "<tr><th class='title' style='width:150px;'>Location</th>"
            . "<th class='title'>Name</th>"
            . "<th class='title'>IMO</th>"
            . "<th class='title'>Manager</th>"
            . "<th class='title'>Type (Level 2)</th>"
            . "<th class='title'>Type (Level 4)</th>"
            . "<th class='title'>Status</th>"
            . "<th class='title'>Parent</th>"
            . "<th class='title'>Tech. Mgr</th>"
            . "<th class='title'>Original Source</th>"
            . "<th class='title'>Source Date</th>"
            . "<th class='title'>Yard</th>"
            . "<th class='title'>Built Date</th>"
            . "<th class='title'>Prop. Type</th>"
            . "<th class='title'>Prop. Count</th>"
            . "<th class='title'>Ship Builder</th>"
            //. "<th class='title'>Build Year</th>"
            . "<th class='title'>Archived?</th>"
            . "<th class='title'>Archive reason</th>"
            . "<th class='title'>Flag Name</th>"
            . "<th class='title'>Classification Soc.</th>"
            . "<th class='title'>Changed By</th>"
            . "<th class='title'>Change Date</th>"
            . "</tr>";
    $countForRow = 1;
    foreach ($instDetailHistory as $key => $history) {
        $rowCol = "";
        if (($countForRow % 2) == 0) {
            $rowCol = "style='background-color:#f0f4f9;'";
        }
        $countForRow++;
        print "<tr $rowCol>";
        print "<td>".$history->installationType."</td>";
        print "<td>".stripslashes($history->installationName)."</td>";
        print "<td>".$history->installationIMO."</td>";
        print "<td>".$customerController->getCustomerIDByInstallationID($history->installationID)->customerName."</td>";
        print "<td>".$history->installation."</td>";
        print "<td>".$history->shipTypeLevel4."</td>";
        print "<td>".$installationStatusController->getInstallationStatusByID($history->installationStatusID)->statusDescription."</td>";
        print "<td>".$customerController->getCustomerByID($history->installationOwnerParentCompany)->customerName."</td>";
        print "<td>".$customerController->getCustomerByID($history->installationTechnicalManager)->customerName."</td>";
        print "<td>".$history->installationOriginalSource."</td>";
        print "<td>".$history->installationOriginalSourceDate."</td>";
        print "<td>".$history->installationYardBuilt."</td>";
        print "<td>".$history->installationBuiltDate."</td>";
        print "<td>".$history->installationPropellerType."</td>";
        print "<td>".$history->installationPropulsionUnitCount."</td>";
        print "<td>".$history->installationShipBuilder."</td>";
        //print "<td>".$history->installationBuildYear."</td>";
        print "<td>".($history->installationArchiveFlag == '1' ? "Yes" : "No")."</td>";
        print "<td>".$history->installationArchiveReason."</td>";
        print "<td>".$history->flagName."</td>";
        print "<td>".$history->classificationSociety."</td>";
        print "<td>".$userController->getUserByID($history->installationHistoryChangedByID)->name."</td>";
        print "<td>".$history->installationHistoryDateChanged."</td>";
        print "<tr>";
    }
    print "</table>";
} else {
    print "No history to display";
}