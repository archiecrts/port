<script src="./js/jquery.are-you-sure.js"></script>
<script src="./js/ays-beforeunload-shim.js"></script>

<script>
    $(function () {

        $('#techForm').areYouSure();
        
    });
</script>

<div id='installationContainer'>
    <div id="installationHeader">

        <img src="./icons/technicalDataIcon.png" alt="" height='40px'/>
        <h3>Technical Data</h3>
    </div>

    <div id='installationSubHeader'>
        <p>You can move engines to another installation within the customer but not outside of that customer</p>
    </div>
    <?php
    $engineID = "";
    $engine;
    $unit;
    $typeOfProduct = "";
    if (isset($_GET['productID'])) {
        $productID = $_GET['productID'];
        $product = $productController->getProductByID($productID);
        $engine = $installationEngineController->getEngineByProductID($productID);
        $engineID = $engine->installationEngineID;
        $typeOfProduct = $typeOfProductLookupController->getTypeOfProductByID($seriesLookupController->getSeriesByVersionLookupID($product->versionLookupID)->typeOfProductLookupID);
            
        $installationID = $_GET['installationID'];
    ?>
    <div id='installationContentWide'>         
        <form name="techForm" id="techForm" action="index.php?portal=mr&page=report&action=showInstallation&customerID=<?php echo $customerID; ?>&installationID=<?php echo $installationID; ?>&tab=installation&reportID=<?php echo $reportOverviewID; ?>&save=moveTechnical&productID=<?php echo $productID; ?>" method="post" enctype="multipart/form-data">
            <p><input type="submit" name="submit" value="Save"></p>
            <table>
                <tr>
                    <td class="title">Type of Product</td>
                    <td><span class="red"><?php echo $typeOfProduct->toplProductDescription; ?></span></td>
                    <td class="title">Drawing Number</td>
                    <td><span class="red"><?php echo $product->productDrawingNumber; ?></span></td>
                    <td class="title">Serial Number</td>
                    <td><span class="blue"><?php echo $product->productSerialNumber; ?></span></td>
                    <td class="title">Comment</td>
                    <td><?php echo $product->productComment; ?></td>
                    
                </tr>
                <tr>
                    <td class="title"></td>
                    <td style="width:100px; word-wrap:break-word;"></td>
                    <td class="title">Manufacturer</td>
                    <td><?php $designer = $designerLookupController->getDesignerByVersionLookupID($product->versionLookupID);
                        
                        print $designer->designerDescription; ?></td>
                    <td class="title">Series</td>
                    <td><?php echo htmlXentities($seriesLookupController->getSeriesByVersionLookupID($product->versionLookupID)->seriesDescription); ?></td>
                    <td></td>
                    <td></td>
                </tr>
                <?php
                if ($typeOfProduct->toplProductDescription == 'Engine') {
                
                ?>
                <tr>
                    <td class="title">Model</td>
                    <td><?php echo htmlXentities($engine->modelDescription); ?></td>
                    <td class="title">No. of Cylinders</td>
                    <td><?php echo $engine->engineCylinderCount;; ?></td>
                    <td class="title">Cylinder Configuration</td>
                    <td><?php echo $engine->engineCylinderConfiguration; ?></td>
                    <td class="title">Fuel Type</td>
                    <td><?php echo $engine->fuelType; ?></td>
                    
                </tr>
                <tr>
                    <td class="title">Main or Auxiliary</td>
                    <td><?php echo $engine->mainOrAux; ?></td>
                    <td class="title">Output</td>
                    <td><?php echo $engine->output . " " . $engine->outputUnitOfMeasurement; ?></td>
                    <td class="title">Release Date</td>
                    <td><?php
                        if ($engine->releaseDate != "0000-00-00 00:00:00") {
                            echo date("Y", strtotime($engine->releaseDate));
                        }
                        ?>
                    </td>
                    
                    <td class="title">Bore Size</td>
                    <td><?php echo $engine->boreSize; ?></td>
                    
                </tr>
                <tr>
                    
                    <td class="title">Stroke</td>
                    <td><?php echo $engine->stroke; ?></td>
                    <td class="title">Load Priority</td>
                    <td><?php echo $loadPriorityController->getLoadPriorityByID($engine->loadPriorityID)->loadPriority; ?></td>
                    
                    <td class="title">Engine Verified</td>
                    <td><?php
                        if ($engine->engineVerified == 1) {
                            echo "yes";
                        } else {
                            echo "no";
                        }
                        ?></td>
                    <td></td>
                    <td></td>
                </tr>
                
                <?php
                }
                ?>
                
            </table>
            <h3>Move to Installation</h3>
            <table>
                <tr>
                    <td>Installation Name</td>
                    <td><select name="installationID">
                            <option value="">Please Choose...</option>
                            <?php
                            if (isset($installationsArray)) {
                            foreach ($installationsArray as $key => $installationObject) {
                                $installation = $installationObject;
                                print "<option value='".$installation->installationID."'>".$installation->installationName."</option>";
                            }
                            }
                ?>
                        </select></td>
                </tr>
            </table>
        </form>

    </div>
    <?php
    }
    ?>
</div>