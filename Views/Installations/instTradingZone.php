<script>   
    $(function () {
        //var scrolled = 0;
        $("#tradingZoneHeader").click(function () {
            
                if ($('#tradingZoneTbl').css('display') == 'none')
                {
                    $('#tradingZoneTbl').removeClass('hiddenBlock').addClass('showTable');
                } else {
                    $('#tradingZoneTbl').removeClass('showTable').addClass('hiddenBlock');
                }
        });
    });
 </script>
<div id="tradingZoneHeader">

    <div id="officeLocationsHeader"><img src="./icons/technicalDataIcon.png" alt="" height='20px'/> Trading Zone Last Seen:
    </div>
</div>

 <table style="width:100%;" id="tradingZoneTbl"  class='hiddenBlock'>

    <tr>
        <th>Trading Zone</th>
        <th>Last Seen</th>
        <th>Installation Name</th>
        <th>IMO</th>
    </tr>

    <?php
    if (isset($tradingZoneList)) {
        foreach ($tradingZoneList as $k => $tz) {
            print "<tr>";
            print "<td>$tz->tradingZone</td>";
            print "<td>".date("d-m-Y H:i:s", strtotime($tz->lastSeenDate))."</td>";
            print "<td>" . $installationController->getInstallationByInstallationID($tz->installationID)->installationName . "</td>";
            print "<td>$tz->LRNO</td>";
            print "</tr>";
        }
    } else {
        print "<tr>";
        print "<td colspan='4'>Nothing to display</td>";
        print "</tr>";
    }
    ?>
</table>