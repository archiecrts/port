<script src="./js/jquery.are-you-sure.js"></script>
<script src="./js/ays-beforeunload-shim.js"></script>
<?php
$engineID = "";
$productID = "";
$engine;
$unit;
$product;
$productTypeID = "";
if (isset($_GET['productID'])) {
    $productID = $_GET['productID'];
    $product = $productController->getProductByID($productID);
    $productTypeID = $typeOfProductLookupController->getTypeOfProductByID($seriesLookupController->getSeriesByVersionLookupID($product->versionLookupID)->typeOfProductLookupID)->toplID;
    $engine = $installationEngineController->getEngineByProductID($productID);
    $engineID = $engine->installationEngineID;
} else {
    $product = new Product("", "", "", "", "", "", "", "", "", "", "");
    $engine = new InstallationEngine("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "3", "", "");
}
?>
<script>
    $(function () {

        $('#techForm').areYouSure();

        // this shows and hides the engines bit.
        $("#type").change(function () {
            var selectvalue = $(this).val();
            //alert(selectvalue);
            /*if (selectvalue == "1") {
                $('#engineBits').find('input:text').val('');
                document.getElementById('engineBits').style.display = "block";
            } else {
                document.getElementById('engineBits').style.display = "none";
            }*/

            // get the new dropdown list to populate manufacturer with.
            
            //alert(selectvalue);
            $.ajax({url: 'Views/AddInstallation/typeOfProductDropDownAjax.php?tvalue=' + selectvalue,
            success: function (output) {
                //alert(output);
                $('#list-select').html(output);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status + " " + thrownError);
            }});
        });


        var list_target_id = {};
        var list_select_id = {};
        var list_version_id = {};



        list_target_id['list-target'] = 'list-target'; //second select list ID (SERIES)
        list_select_id['list-select'] = 'list-select'; //first select list ID (MANUFACTURER)
        list_version_id['list_version'] = 'list_version'; // third select list ID (VERISON)

        var initialTypeValue = $("#type").val();

        var initialManufacturer = "<?php echo $designerLookupController->getDesignerByVersionLookupID($product->versionLookupID)->designerID; ?>";
        var initialIDValue = "<?php echo $seriesLookupController->getSeriesByVersionLookupID($product->versionLookupID)->seriesID; ?>";



        $.ajax({url: 'Views/AddInstallation/dynamicSeriesDropDown.php?svalue=' + initialManufacturer + '&tvalue=' + initialTypeValue + '&selected=' + initialIDValue,
            success: function (output) {
                 //alert(output);
                $('#' + list_target_id['list-target']).html(output);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status + " " + thrownError);
            }});




        $('#' + list_select_id['list-select']).change(function (e) {
            // var changedID = this.id;
            // alert("change " + this.id);
            //Grab the chosen value on first select list change
            var selectvalue = $(this).val();
            var typeValue = $('#type').val();

            //Display 'loading' status in the target select list
            $('#' + list_target_id['list-target']).html('<option value="">Loading...</option>');

            if (selectvalue === "") {
                //Display initial prompt in target select if blank value selected
                $('#' + list_target_id['list-target'] + ' option[value=' + initialIDValue + ']').prop('selected', 'true');
            } else {
                //Make AJAX request, using the selected value as the GET

                $.ajax({url: 'Views/AddInstallation/dynamicSeriesDropDown.php?svalue=' + selectvalue + '&tvalue=' + typeValue,
                    success: function (output) {
                          //alert(output);
                        $('#' + list_target_id['list-target']).html(output);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status + " " + thrownError);
                    }});
            }
        });

        $('#' + list_target_id['list-target']).change(function (e) {
            //var changedID = this.id;
            //var target = "list-target_" + changedID[1];
            // alert("change " + this.id);
            //Grab the chosen value on first select list change
            var selectvalue = $(this).val();
            //var typeValue = $('#type').val();

            //Display 'loading' status in the target select list
            $('#' + list_version_id['list-version']).html('<option value="">Loading...</option>');

            if (selectvalue === "") {
                //Display initial prompt in target select if blank value selected
                $('#' + list_version_id['list-version'] + ' option[value=' + initialIDValue + ']').prop('selected', 'true');
            } else {
                //Make AJAX request, using the selected value as the GET

                $.ajax({url: 'Views/AddInstallation/dynamicVersionDropDown.php?svalue=' + selectvalue,
                    success: function (output) {
                        //alert(output);
                        $('#list-version').html(output);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status + " " + thrownError);
                    }
                });
            }
        });

    });
</script>

<div id='installationContainer'>
    <div id="installationHeader">

        <img src="./icons/technicalDataIcon.png" alt="" height='40px'/>
        <h3>Technical Data</h3>
    </div>

    <div id='installationSubHeader'>

    </div>
    <?php
    $installationID = $_GET['installationID'];
    $installation = $installationController->getInstallationByInstallationID($installationID);
    ?>
    <div id='installationContentWide'>         
        <form name="techForm" id="techForm" action="index.php?portal=mr&page=report&action=showInstallation&customerID=<?php echo $customerID; ?>&installationID=<?php echo $installationID; ?>&tab=installation&reportID=<?php echo $reportOverviewID; ?>&save=saveTechnical&productID=<?php echo $productID; ?>" method="post" enctype="multipart/form-data">
            <p><input type="submit" name="submit" value="Save"></p>


            <h3>Product Details</h3>
            <table>
                <tr>
                    <td class="title">Product Type</td>
                    <td><select id="type" name="productType">
                            <?php
                            if ($_GET['edit'] == 'add') {
                                print "<option value='' selected='selected'>Choose One</option>";
                                print "<option value='2' >Seal</option>";
                                print "<option value='3' >Oil Water Separator</option>";
                            } else {
                                $productTypes = $typeOfProductLookupController->getAllProductTypes();

                                foreach ($productTypes as $key => $productType) {
                                    $typeSelect = '';
                                    if ($productType->toplID == $productTypeID) {
                                        $typeSelect = 'selected = "selected"';
                                    }
                                    print "<option value='$key' $typeSelect>$productType->toplProductDescription</option>";
                                }
                            }
                            ?>
                        </select></td>
                    <td class="title"></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="4"><hr/></td>
                </tr>

                <tr>
                    <td class="title">Manufacturer</td>
                    <td><select name="engineManufacturer"  id="list-select">
                            <option value="">Select a type</option>
                            <?php
                            if ($_GET['edit'] == 'edit') {
                            $engineManufacturers = $designerLookupController->getDistinctDesignersByTypeOfProductID($productTypeID);
                            
                            foreach ($engineManufacturers as $name => $designer) {
                                $manufacturerSelect = '';
                                if ($designer->designerDescription != "") {
                                    
                                    if ($designerLookupController->getDesignerByVersionLookupID($product->versionLookupID)->designerID == $designer->designerID) {
                                        $manufacturerSelect = 'selected="selected"';
                                    }
                                    echo '<option value="' . htmlXentities($designer->designerID) . '" ' . $manufacturerSelect . '>' . htmlXentities($designer->designerDescription) . '</option>';
                                }
                            }
                            
                            }
                            ?>
                        </select>
                    </td>
                    <td class="title">Series</td>
                    <td><select name="series" id="list-target" >
                            <?php
                            $engineSeries = $seriesLookupController->getDistinctModelSeries();
                            if (isset($engineSeries)) {
                                foreach ($engineSeries as $name => $series) {
                                    if ($series->seriesDescription != "") {
                                        $selected = "";
                                        if ($series->seriesDescription != "") {
                                            if ($seriesLookupController->getSeriesByVersionLookupID($product->versionLookupID)->seriesID == $series->seriesID) {
                                                $selected = 'selected="selected"';
                                            }
                                            echo '<option value="' . htmlXentities($series->seriesDescription) . '" ' . $selected . '>' . htmlXentities($series->seriesDescription) . '</option>';
                                        }
                                    }
                                }
                            }
                            ?>
                        </select>     
                    </td>
                </tr>
                <tr>
                    <td class="title"></td>
                    <td></td>
                    <td class="title">Serial Number</td>
                    <td><input type="text" name="serialNumber" id="serialNumber" value="<?php echo $product->productSerialNumber; ?>"/></td>
                </tr>
                <tr>
                    <td class="title">Description</td>
                    <td><input type="text" name="description" value="<?php echo $product->productDescription; ?>"/></td>
                    <td class="title">Drawing Number</td>
                    <td><input type="text" name="drawingNumber" value="<?php echo $product->productDrawingNumber; ?>"/></td>
                </tr>
                <tr>
                    <td class="title">Comments</td>
                    <td colspan="3"><textarea name="comment" cols="70" rows="6"><?php echo $product->productComment; ?></textarea></td>
                </tr>
            </table>


            <!--<div id="engineBits">
                <h3>Engine Sub Details</h3>
                <table>
                    <tr>
                        <td class="title">Main or Aux</td>
                        <td><select name="mainOrAux">
            <?php
            $engineOption = ['Main', 'Aux'];
            foreach ($engineOption as $value) {
                $mainAuxSelected = "";
                if ($engine->mainOrAux == $value) {
                    $mainAuxSelected = "selected";
                }
                print '<option value="' . $value . '" ' . $mainAuxSelected . '>' . $value . '</option>';
            }
            ?>

                            </select></td>
                        <td class="title">Engine Verified</td>
            <?php
            $checked = "";
            if ($engine->engineVerified == 1) {
                $checked = "checked";
            }
            ?>
                        <td><input type="checkbox" id="engineVerifiedCheckBox" name="engineVerified" value="1" <?php echo $checked; ?> /></td>

                    </tr>
                    <tr>
                        <td class="title">Engine Model Description</td>
                        <td><input type='text' name="engineModel" value="<?php echo $engine->modelDescription; ?>" ></td>
                        <td class="title">Version</td>
                        <td>
                            <select name="versionID" id="list-version">
            <?php
            $engineVersion = $versionLookupController->getVersionIDBySeriesID($seriesLookupController->getSeriesByVersionLookupID($product->versionLookupID)->seriesID);
            if (isset($engineVersion)) {

                foreach ($engineVersion as $name => $version) {
                    $selected = "";

                    if ($product->versionLookupID == $version->versionID) {
                        $selected = 'selected="selected"';
                    }
                    echo '<option value="' . $version->versionID . '" ' . $selected . '>' . htmlXentities($version->versionProductDescription) . '</option>';
                }
            } else {
                print '<option>Please Select Series...</option>';
            }
            ?>




                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="title"></td>
                        <td></td>
                        <td class="title"></td>

                        <td></td>
                    </tr>


                    <tr>
                        <td class="title">Output</td>
                        <td><input type="text" name="output" value="<?php echo $engine->output; ?>"/></td>
                        <td class="title"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="title">Fuel Type</td>
            <?php
            $fuelArray = ['Gas', 'Oil', 'Duel Fuel', 'Unknown'];
            ?>
                        <td><select name="fuelType">
            <?php
            foreach ($fuelArray as $val) {
                $fuelSelect = "";
                if ($val == $engine->fuelType) {
                    $fuelSelect = 'selected = "selected"';
                }
                print '<option value="' . $val . '" ' . $fuelSelect . '>' . $val . '</option>';
            }
            ?>
                            </select></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="title">Bore Size</td>
                        <td><input type="text" name="boreSize" value="<?php echo $engine->boreSize; ?>"/> (mm)</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="title">Stroke</td>
                        <td><input type="text" name="stroke" value="<?php echo $engine->stroke; ?>"/> (mm)</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="title">Release Date</td>
                        <td><input type="text" name="releaseDate" value="<?php echo date('Y', strtotime($engine->releaseDate)); ?>" placeholder="registration year of engine" pattern="[0-9]{4}" /></td>
                        <td></td>
                        <td></td>
                    </tr>
            <?php
            if ($installation->type == "LAND") {
                ?>
                            <tr>
                                <td class="title">Load Priority</td>
                                <td><select name="loadPriority">
                <?php
                $loadPriorityArray = $loadPriorityController->getLoadPriorities();
                foreach ($loadPriorityArray as $key => $loadPriority) {
                    $loadSelected = "";
                    if ($key == $engine->loadPriorityID) {
                        $loadSelected = 'selected = "selected"';
                    }
                    print '<option value="' . $key . '" ' . $loadSelected . '>' . $loadPriority->loadPriority . '</option>';
                }
                ?>
                                    </select></td>
                                <td></td>
                                <td></td>
                            </tr>
                <?php
            } else {
                ?>
                            <input type="hidden" name="loadPriority" value="3"/>
                <?php
            }
            ?>

                </table>
            </div>-->


        </form>

    </div>
</div>

<script>
    $(function () {

    var initialEngineBitsDisplay = '<?php echo $productTypeID; ?>';
            if (initialEngineBitsDisplay === '1') {
    document.getElementById('engineBits').style.display = "block";
    } else {
    document.getElementById('engineBits').style.display = "none";
    }
    }
</script>