<?php
include_once 'Controllers/UserHistoryController.php';
$userHistoryController = new UserHistoryController();
$handle = fopen("./logs/autoupdate.txt", "a");

// temp place to check FTP folder for new data.

include ("./Helpers/ftp/ftpListFiles.php");
//print $server_file;
$expFileName = explode("_", $server_file);
$expDate = explode(".", $expFileName[1]);

$format = 'Ymd';
$date = DateTime::createFromFormat($format, $expDate[0]);
//echo "Format:  " . $date->format('Y-m-d') . "<br/>";

$proceed = false;

if ($userHistoryController->lastSeawebUpdateAt()->itemDate != "") {
    if ($userHistoryController->lastSeawebUpdateAt()->itemDate < $date) {
        $proceed = true;
    }
} else {
    $proceed = true;
}

if ($proceed == true) {
    
    // ftp seaweb download
    fwrite($handle, date("Y-m-d H:i:s", time()) . " Download Started \r\n");
    include ("./Helpers/ftp/ftpSeaWebDownload.php");
    

    // unzip the folder
    if ($downloadSuccess == true) {
        fwrite($handle, date("Y-m-d H:i:s", time()) . " Unzip started \r\n");
        include ("./Helpers/ftp/unzipFile.php");


        if ($unzip == true) {
            // run the update script.
            fwrite($handle, date("Y-m-d H:i:s", time()) . " Update Started \r\n");
            include 'Views/Uploads/updateSeaweb.php';
        }

        unlink($local_file);
    } else {
        fwrite($handle, date("Y-m-d H:i:s", time()) . " Download failed \r\n");
    }
}
fclose($handle);