<h3>Notify PORT Developer Team</h3>
<?php
include_once 'Controllers/NotifyAdminController.php';
include_once 'Controllers/NotifyAdminCategoryController.php';
$notifyAdminController = new NotifyAdminController();
$notifyAdminCategoryController = new NotifyAdminCategoryController();

$action = "";
if (isset($_GET['action'])) {
    $action = $_GET['action'];
}
switch ($action) {
    case "send":

        include_once('Helpers/PHPMailer/PHPMailerAutoload.php');

        $notifyAdminObject = new NotifyAdmin("", $_SESSION['user_id'], date("Y-m-d H:i:s", time()), addslashes($_POST['section']." : ".$_POST['content']), "open", addslashes($_POST['category']));
        $enquiry = $notifyAdminController->setEnquiry($notifyAdminObject);
        // Create record of enquiry.
        if (isset($enquiry)) {
            

            // EMAIL VIA POP3
            $mail = new PHPMailer;

            //$mail->SMTPDebug = 3;                               // Enable verbose debug output

            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'mail.uk2.net';                         // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'port@simplexconverting.com';        // SMTP username
            $mail->Password = 'GreatThing22';                          // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to

            $mail->From = 'acurtis@dieselmarinegroup.com';
            $mail->FromName = 'PORT : S-T Diesel Marine Group';
            $mail->addAddress('acurtis@dieselmarinegroup.com', 'PORT Administrator');     // Add a recipient
            $mail->addReplyTo('acurtis@dieselmarinegroup.com', 'PORT Administrator');
            //$mail->addCC('cc@example.com');
            //$mail->addBCC('bcc@example.com');
            //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
            $mail->isHTML(true);                                  // Set email format to HTML

            $mail->Subject = 'PORT: Notify Admin Request';
            $mail->Body = "<p>Subject: " . $_POST['subject'] . "</p>"
                    . "<p>Section: ". $_POST['section']. "</p>"
                    . "<p>Category: " . $_POST['category'] . "</p>"
                    . "<p>DB Entry ID: " . $enquiry . "</p>"
                    . "<p>Content: " . $_POST['content'] . "</p>"
                    . "<p>User : " . $_SESSION['name'] . "</p>";
            $mail->AltBody = "Subject: " . $_POST['subject'] . "\r\n"
                    . "Section: ". $_POST['section']. "\r\n"
                    . "Category: " . $_POST['category'] . "\r\n"
                    . "DB Entry ID: " . $enquiry . "\r\n"
                    . "Content: " . $_POST['content'] . "\r\n"
                    . "User : " . $_SESSION['name'] . "";

            if (!$mail->send()) {
                echo 'Message could not be sent.';
                echo 'Mailer Error: ' . $mail->ErrorInfo;
                echo '<p><input type="button" name="backToNotifyAdmin" value="Please try again" onclick="javascript:window.location=\'index.php?portal=notifyAdmin\';"/></p>';
            } else {
                echo '<p>Message has been sent</p>';
                echo '<p><input type="button" name="backToNotifyAdmin" value="Add another enquiry" onclick="javascript:window.location=\'index.php?portal=notifyAdmin\';"/></p>';
            }
        } else {
            print "Your enquiry has failed! - Please poke the PORT administrator with a spoon.";
        }


        break;
    default :
        ?>



        <p>Contact the PORT Development Team to report an issue, request a new feature or for help using the system.</p>

        <form name="adminForm" action="index.php?portal=notifyAdmin&action=send" method="post" enctype="multipart/form-data">

            <table>
                <tr>
                    <td>Subject</td>
                    <td><input type="text" name="subject" value="" size="80"/></td>
                </tr>
                <tr>
                    <td>Website Section</td>
                    <td>
                        <?php
                        
                        $sectionArray = ['', 'Campaigns', 'CRM', 'Dashboard', 'Dispatch', 'Enquiries', 
                            'Intranet', 'Invoices', 'Market Research', 
                             'Opera', 'Orders', 'Parts', 'Reports', 'Stock Control', 
                                'User Account', 'Suppliers'];
                        print "<select name='section'>";
                                foreach ($sectionArray as $value) {
                                    print "<option value='".$value."'>".$value."</option>";
                                }
                        print "</select>";
                        
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Category</td>
                    <td><select name="category">
                            <?php
                            $categories = $notifyAdminCategoryController->getAllCategories();
                            if (isset($categories)) {
                                foreach ($categories as $key => $object) {
                                    print '<option value="'.$key.'">'.$object->category.'</option>';
                                }
                            }
                            ?>
                            
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style='text-align: right;'><textarea name="content" cols="81" rows="10"></textarea></td>
                </tr>
                <tr>
                    <td></td>
                    <td style='text-align: right;'><input type="submit" name="submit" value="Send"/></td>
                </tr>
            </table>
        </form>
    <?php
}
