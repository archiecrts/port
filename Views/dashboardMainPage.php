<?php
    include_once ('Controllers/ReportOverviewController.php');
    $reportOverviewController = new ReportOverviewController();
    
?>


<?php
    // Admin Dashboard Widget.
    if ($_SESSION['level'] == '1') {
        include 'Views/Dashboard/notifyAdminDashboardWidget.php';
        
        include 'Views/Dashboard/allUserHistoriesWidget.php';
    }
    if ($_SESSION['level'] != '1') {
        include 'Views/Dashboard/usersNotifyAdminWidget.php';
        
    }
    
    // Actions Required Widget.
   // include 'Views/Dashboard/crmActionsRequiredWidget.php';
    


