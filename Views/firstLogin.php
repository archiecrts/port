<h3>Change Your Password</h3>
    
<p>This is the very first time you have logged into PORT and you are currently using an 
    auto-generated password. Please create a new password for future use.</p>
<p>Passwords must contain:
<ul>
    <li>At least one number</li>
    <li>Upper and lower case</li>
    <li>Between 8 and 15 characters</li>
</ul></p>
    
    <?php

/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once 'Controllers/UserController.php';
$userController = new UserController();
if (isset($_POST['submit'])) {
    if (isset($_POST['password'])) {
        $resetPassword = $userController->resetPasswordFirstLogin($_POST['password'], $_SESSION['user_id']);
        
        if ($resetPassword) {
            print "<h3 class='green'>Your password has been changed</h3>";
        }
    }
}
?>

<script>

$(function ()  {
       
        $('#nda').click(function() {
            
            if ($('#nda').is(':checked')) {
                $('input[type="submit"]').prop('disabled', false);
            } else {
                $('input[type="submit"]').prop('disabled', true);
            }
        });
    });

</script>



<script type="text/javascript" src="js/validateFirstLoginForm.js"></script>
<form name="form" onsubmit="return validateFirstLogin();" action="index.php" method="post" enctype="multipart/form-data">
    
    <p id="error"></p>
    <table>
        <tr>
            <td>New Password</td>
            <td><input type="password" name="password" required /></td>
        </tr>
        <tr>
            <td>Confirm New Password</td>
            <td><input type="password" name="confirmPassword" required /></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" id="submit" name="submit" value="Set New Password" disabled="disabled" /></td>
        </tr>
    </table>
    
    <h3>Non Disclosure Agreement</h3>
    <p>To use PORT or any related system (e.g. DRYDOCK), you must confirm that you understand that all data held in this system is
    the property of Simplex Turbulo Diesel and Marine Group and that you may not share it with 
    anyone outside of the company (including past employees) without written group director level authorisation. Failure to protect this
    data could result in disciplinary action or contractual termination. You understand that all 
    activities performed using your login are recorded and you are responsible for all those activities. 
    You may not share your login details with anyone else.</p>
    <p>I agree to the terms of the NDA <input type="checkbox" id="nda" name="nda" value="1"/></p>
</form>
