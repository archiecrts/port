<script>
    $(function () {
        $("#accordion").accordion({
            collapsible: true,
            heightStyle: "content",
            active: false
        });
    });
    
    function saveSettings() {
        var settings = [
            {column: 'type', status: document.getElementById('type').checked},
            {column: 'installation',  status: document.getElementById('installation').checked},
            {column: 'imo',  status: document.getElementById('imo').checked},
            {column: 'surveyDueDate',  status: document.getElementById('surveyDueDate').checked},
            {column: 'city',  status: document.getElementById('city').checked},
            {column: 'state',  status: document.getElementById('state').checked},
            {column: 'region',  status: document.getElementById('region').checked},
            {column: 'country',  status: document.getElementById('country').checked},
            {column: 'area',  status: document.getElementById('area').checked},
            {column: 'continent',  status: document.getElementById('continent').checked},
            {column: 'operatingCompany',  status: document.getElementById('operatingCompany').checked},
            {column: 'technicalManager',  status: document.getElementById('technicalManager').checked},
            {column: 'tmCity',  status: document.getElementById('tmCity').checked},
            {column: 'tmCountry',  status: document.getElementById('tmCountry').checked},
            {column: 'tmArea',  status: document.getElementById('tmArea').checked},
            {column: 'tmContinent',  status: document.getElementById('tmContinent').checked},
            {column: 'status',  status: document.getElementById('status').checked},
            {column: 'engineManufacturer',  status: document.getElementById('engineManufacturer').checked},
            {column: 'engineModel',  status: document.getElementById('engineModel').checked},
            {column: 'engineSeries',  status: document.getElementById('engineSeries').checked},
            {column: 'mainAux',  status: document.getElementById('mainAux').checked},
            {column: 'engineCount',  status: document.getElementById('engineCount').checked},
            {column: 'buildYear',  status: document.getElementById('buildYear').checked},
            {column: 'source',  status: document.getElementById('source').checked},
            {column: 'accountManager',  status: document.getElementById('accountManager').checked}
            ];
            
           
            for(var col of settings){ 
                var id = col.column;
                var active = 0;
                
                if (col.status === true) {
                    active = 1;
                } else {
                    active = 0;
                }
                
                $.ajax({url: 'Views/Reports/settingsAccordionAjax.php?columnID=' + id + '&active=' + active + '&userID=' + <?php echo $_SESSION['user_id']; ?> ,
                    success: function(output) {
                      // alert(output);
                   },
                   error: function (xhr, ajaxOptions, thrownError) {
                       alert(xhr.status + " "+ thrownError);
                   }
                });
            }    
            //alert("RELOADING");
            //var a = "world";
            setTimeout(function(){window.location.reload()}, 2000);
            
    }
</script>

<h3>Data Groups</h3>
<button id='applyBtn' onclick="saveSettings()">Apply</button>
<div id="accordion">


    <h3>Installation Data</h3>


    <div>
        <table>
            <?php
            $installationDataArray = ['type' => 'Type',
                'installation' => 'Installation Type',
                'imo' => 'IMO Number',
                'installationName' => 'Installation Name',
                'surveyDueDate' => 'Survey Due Date'];

            foreach ($installationDataArray as $id => $label) {
                print "<tr>";
                $checked = "";
                if (in_arrayi($id, $settingsColumnNames)) {
                    $checked = 'checked="checked"';
                }
                $readonly = "";
                if ($id == 'installationName') {
                    $readonly = "disabled='disabled'";
                }
                print '<td><label for="' . $id . '">' . $label . '</label></td>';
                print "<td><input type='checkbox' id='" . $id . "' name='" . $id . "' value='1' " . $checked . " " . $readonly . "  /></td>";

                print "</tr>";
            }
            ?>

        </table>
    </div>


    <h3>Ship Manager / Company Data</h3>


    <div>
        <table>
            <?php
            $companyDataArray = ['customerName' => 'Customer Name',
                'city' => 'City',
                'state' => 'State',
                'region' => 'Region',
                'country' => 'Country',
                'area' => 'Area',
                'continent' => 'Continent'];

            foreach ($companyDataArray as $id => $label) {
                print "<tr>";
                $checked = "";
                if (in_arrayi($id, $settingsColumnNames)) {
                    $checked = 'checked="checked"';
                }
                $readonly = "";
                if ($id == 'customerName') {
                    $readonly = "disabled='disabled'";
                }
                print '<td><label for="' . $id . '">' . $label . '</label></td>';
                print "<td><input type='checkbox' id='" . $id . "' name='" . $id . "' value='1' " . $checked . " " . $readonly . " /></td>";

                print "</tr>";
            }
            ?>

        </table>
    </div>


    <h3>TM | Parent  Company Data</h3>


    <div>
        <table>

            <?php
            $parentDataArray = ['operatingCompany' => 'Owner / Parent Company',
                'technicalManager' => 'Technical Manager',
                'tmCity' => 'TM City',
                'tmCountry' => 'TM Country',
                'tmArea' => 'TM Area',
                'tmContinent' => 'TM Continent'];

            foreach ($parentDataArray as $id => $label) {
                print "<tr>";
                $checked = "";
                if (in_arrayi($id, $settingsColumnNames)) {
                    $checked = 'checked="checked"';
                }

                print '<td><label for="' . $id . '">' . $label . '</label></td>';
                print "<td><input type='checkbox' id='" . $id . "' name='" . $id . "' value='1' " . $checked . " /></td>";

                print "</tr>";
            }
            ?>


        </table>

    </div>
    
    
    <h3>Unit Data</h3>
    
    
    <div>
        <table>
            
            <?php
            $unitDataArray = ['status' => 'Status'];

            foreach ($unitDataArray as $id => $label) {
                print "<tr>";
                $checked = "";
                if (in_arrayi($id, $settingsColumnNames)) {
                    $checked = 'checked="checked"';
                }

                print '<td><label for="' . $id . '">' . $label . '</label></td>';
                print "<td><input type='checkbox' id='" . $id . "' name='" . $id . "' value='1' " . $checked . " /></td>";

                print "</tr>";
            }
            ?>
            
            
           
        </table>
    </div>
    <h3>Technical Data</h3>
    <div>
        <table>
            
            
            <?php
            $engineDataArray = ['engineManufacturer' => 'Engine Manufacturer',
                                'engineSeries' => 'Engine Series',
                                'engineModel' => 'Engine Model',
                                'mainAux' => 'Main or Aux',
                                'engineCount' => 'Engine Count',
                                'buildYear' => 'Build Year'];

            foreach ($engineDataArray as $id => $label) {
                print "<tr>";
                $checked = "";
                if (in_arrayi($id, $settingsColumnNames)) {
                    $checked = 'checked="checked"';
                }

                print '<td><label for="' . $id . '">' . $label . '</label></td>';
                print "<td><input type='checkbox' id='" . $id . "' name='" . $id . "' value='1' " . $checked . " /></td>";

                print "</tr>";
            }
            ?>
            
            
            
        </table>
    </div>
    
    
    <h3>STDMG Internal Data</h3>
    
    
    <div>
        <table>
            
            <?php
            $stdmgDataArray = ['source' => 'Source',
                                'accountManager' => 'Account Manager'];

            foreach ($stdmgDataArray as $id => $label) {
                print "<tr>";
                $checked = "";
                if (in_arrayi($id, $settingsColumnNames)) {
                    $checked = 'checked="checked"';
                }

                print '<td><label for="' . $id . '">' . $label . '</label></td>';
                print "<td><input type='checkbox' id='" . $id . "' name='" . $id . "' value='1' " . $checked . " /></td>";

                print "</tr>";
            }
            ?>
            

        </table>
    </div>
</div>