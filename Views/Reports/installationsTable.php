 <script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
 <script>
    $(function(){
     $('#reportTbl').tablesorter(); 
   }); 
 </script>
 
 <style>
     #reportTbl thead tr th.headerSortUp span { 
        /* background: #1D6191;*/
         background-image: url('./icons/arrow-up.png');
 
}

#reportTbl thead tr th.headerSortDown span {
   /* background: #9eafec;*/
  background-image: url('./icons/arrow-down.png');
  
}

#reportTbl thead tr th.headerSortUp{
 /* background: #9eafec;*/
}
#reportTbl thead tr th.headerSortDown {
  /*background: #1D6191;*/
}

#reportTbl thead {
  cursor: pointer;
  background: #c9dff0;
}
     </style>
<?php
require_once 'Helpers/charsetFunctions.php';
?>
<div id="installationsTable">
<h3>Installations</h3>
<?php

$masterHeaderArray = [
    "TYPE" => "INSTALLATION DATA",
    "INSTALLATION" => "INSTALLATION DATA",
    "IMO" => "INSTALLATION DATA",
    "INSTALLATION NAME" => "INSTALLATION DATA",
    "SURVEY DUE DATE" => "INSTALLATION DATA",
    "CUSTOMER NAME" => "SHIPMANAGER / COMPANY DATA",
    "CITY" => "SHIPMANAGER / COMPANY DATA",
    "STATE" => "SHIPMANAGER / COMPANY DATA",
    "REGION" => "SHIPMANAGER / COMPANY DATA",
    "COUNTRY" => "SHIPMANAGER / COMPANY DATA",
    "AREA" => "SHIPMANAGER / COMPANY DATA",
    "CONTINENT" => "SHIPMANAGER / COMPANY DATA",
    "OPERATING COMPANY" => "OPERATOR / PARENT COMPANY DATA", // this needs to change to owner parent
    "TECHNICAL MANAGER" => "TECHNICAL MANAGER DATA",
    "TM CITY" => "TECHNICAL MANAGER DATA",
    "TM COUNTRY" => "TECHNICAL MANAGER DATA",
    "TM AREA" => "TECHNICAL MANAGER DATA",
    "TM CONTINENT" => "TECHNICAL MANAGER DATA",
    "STATUS" => "UNIT DATA",
    "PRODUCT TYPE" => "TECHNICAL DATA",
    "ENGINE MANUFACTURER" => "TECHNICAL DATA",
    "ENGINE SERIES" => "TECHNICAL DATA",
    "ENGINE MODEL" => "TECHNICAL DATA",
    "ENGINE COUNT" => "TECHNICAL DATA",
    "MAIN AUX" => "TECHNICAL DATA",
    "BUILD YEAR" => "TECHNICAL DATA",
    "SOURCE" => "STDMG INTERNAL DATA",
    "ACCOUNT MANAGER" => "STDMG INTERNAL DATA"];

/*
 * This below uses the above master header array to make a shortened header array.
 */
$headerArray = [];
foreach ($masterHeaderArray as $key => $value) {

    if (in_arrayi(str_replace(' ', '', $key), $settingsColumnNames)) {
        $headerArray[$key] = $value;
    }
        
}
   

$dataGroupsArray = array_unique($headerArray, SORT_REGULAR); // this makes the top row by getting unique entries from the above array.
?>
<table class='borderedTable dataTable' id='reportTbl'>
    <thead>
        <tr>
            <!-- DATA GROUPS -->
            <?php
            $colspan = array_count_values($headerArray);

            $roundedCornerLeft = '';
            $isFirst = true;
            foreach ($dataGroupsArray as $value) {
                
                if ($isFirst == true) {
                    $roundedCornerLeft = 'class="rounded-corners-left-top"';
                } else {
                    $roundedCornerLeft = '';
                }
                print "<th colspan='" . $colspan[$value] . "' " . $roundedCornerLeft . ">" . $value . "</th>";
                $isFirst = false;
            }
            ?>
            <th class="rounded-corners-right-top">&nbsp;</th>
        </tr>
        <!-- / END DATA GROUPS -->
        <tr class="header-border">
            <?php
            /*
             * This is the second header row.
             */
            foreach ($headerArray as $key => $value) {

                if (in_arrayi(str_replace(' ', '', $key), $settingsColumnNames)) {
                    if ($key == 'INSTALLATION NAME') {
                        print "<th class='headerSortDown'>" . $key . "<span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>";
                        
                    } else if ($key == "ENGINE MANUFACTURER" || $key == "ENGINE SERIES" || $key == "ENGINE MODEL") {
                        print "<th>" . substr($key, 7) . "<span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>";
                    } else {
                        print "<th>" . $key . "<span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></th>";
                    }
                }
            }
            ?>
            <th></th>
    
        </tr>
        <!-- END SECOND HEADER ROW -->
    </thead>
    
    <tbody>
<?php
if (isset($param)) {
    $countForRow = 1;
    foreach ($param as $key => $installationObject) {
        $rowCol = "";
        if (($countForRow % 2) == 0) {
            $rowCol = "style='background-color:#f0f4f9;'";
        }
        print '<tr '.$rowCol.'>'; // Start row.
        
        $installationID;
        $customerID;
        
        foreach ($installationObject as $name => $value) {
            
            
            if ($name == 'installationID') {
                $installationID = $value;
                // check if its been on a campaign recently.
                $previousCampaigns = $reportController->checkInstallationForPreviousCampaignInclusion($installationID, $reportOverviewID, $report->dateCreated);
            }
            if ($name == 'customerID') {
                $customerID = $value;
            }
            
            if (in_arrayi($name, $settingsColumnNames)) {

                $highlight = "";
                if (isset($previousCampaigns)) {
                    if ($previousCampaigns === '1') {
                        $highlight = "class='amberHighlight'";
                    } else if ($previousCampaigns > '1') {
                        $highlight = "class='redHighlight'";
                    }
                }
                if ($name == 'installationName') {
                    print "<td $highlight><a href='index.php?portal=mr&page=report&action=showInstallation&customerID=".$customerID."&installationID=" . $installationID . "&tab=installation&reportID=" . $reportOverviewID . "' target='_blank'>$value</a></td>";
                } else if ($name == 'customerName') {
                    print "<td><a href='index.php?portal=mr&page=report&action=showCustomer&customerID=" . $customerID . "&reportID=" . $reportOverviewID . "' target='_blank'>$value<a></td>";
                } else if ($name == 'operatingCompany') {
                    print "<td><a href='index.php?portal=mr&page=report&action=showCustomer&customerID=" . $value . "&reportID=" . $reportOverviewID . "' target='_blank'>" . $customerController->getCustomerByID($value)->customerName . "</a></td>";
                } else if ($name == 'technicalManager') {
                    print "<td><a href='index.php?portal=mr&page=report&action=showCustomer&customerID=" . $value . "&reportID=" . $reportOverviewID . "' target='_blank'>" . $customerController->getCustomerByID($value)->customerName . "</a></td>";
                } else if ($name == 'installationEngineManufacturer') {
                    print "<td>" . htmlXentities($value) . "</td>";
                } else if ($name == 'status') {
                    print "<td>" . $installationStatusController->getInstallationStatusByID($value)->statusDescription . "</td>";
                } else if ($name == 'accountManager') {
                    
                    if ($value != '') {
                        print "<td>" . $userController->getUserByID($value)->name . "</td>";
                    } else {
                        $acName = '';
                        if ($installationObject->country !== "") {
                            $territoryAccountManager = $userTerritoryController->getUserIDByTerritory($installationObject->country);
                            $acName = $userController->getUserByID($territoryAccountManager)->name;
                        }
                        print "<td>" . $acName . "</td>";
                    }
                    
                } else if ($name == 'buildYear') {
                    if ($value == NULL) {
                        print "<td></td>";
                    } else {
                        print "<td>" . date("Y", strtotime($value)) . "</td>";
                    }
                } else if ($name == 'surveyDueDate') {
                    if ($value == "0000-00-00") {
                        print "<td></td>";
                    } else {
                        print "<td>" . $value . "</td>";
                    }
                } else if ($name == 'engineManufacturer') {
                    $alias = "";
                    $aliasID = $installationObject->aliasID;
                    if ($aliasID != "") {
                        $alias = $designerLookupController->getDesignerByDesignerID($installationObject->aliasID)->designerDescription;
                    }
                    
                    if ($alias != $value && $alias != "") {
                        $alias = "(".$alias.")";
                    } else {
                        $alias = "";
                    }
                    
                    print "<td>" . $value . " $alias</td>";
                } else if ($name == 'productType') {
                    $value = $typeOfProductLookupController->getTypeOfProductByID($value)->toplProductDescription;
                    print "<td>" . $value . "</td>";
                    
                } else if (($name != 'installationID') && ($name != 'unit') && ($name != 'serialNumber') && ($name != 'telephoneNumber') && ($name != 'faxNumber')) {
                
                
                
                    print "<td>" . $value . "</td>";
                }
            }
        }
        print "<td>&nbsp;</td>";
        print '</tr>'; // close table row for installation
        $countForRow++;
    }

    print "<tr>"; // footer table row.
    foreach ($headerArray as $key => $value) {
        print "<td>&nbsp;</td>";
    }


    print "<td>&nbsp;</td>
            </tr></tbody>";
} else {
    print "<span class='red'>No Records to display</span>";
}
?>

</table>

</div>