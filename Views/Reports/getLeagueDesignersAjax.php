<?php
include_once("../../Models/Database.php");
require_once '../../Helpers/charsetFunctions.php';


    $result = mysqli_query(Database::$connection, "SELECT d.* FROM designer_lookup_tbl AS d WHERE d.dlt_stc_league_engine = '1' AND 
d.dlt_id IN (SELECT distinct (a.designer_lookup_tbl_dlt_id) FROM alias_tbl AS a) ORDER BY dlt_designer_description ASC");
    
    while($row = mysqli_fetch_assoc($result)) {
        echo '<option value="' . htmlXentities($row['dlt_designer_description']) . '">' . htmlXentities($row['dlt_designer_description']) . '</option>';
    }
