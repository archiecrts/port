<?php
// CONTROLLERS
include_once('Controllers/CountryController.php');
include_once('Controllers/UserController.php');
include_once('Controllers/InstallationController.php');

$countryController = new CountryController();
$userController = new UserController();
$installationController = new InstallationController();

require_once 'Helpers/charsetFunctions.php';
?>
<script type="text/javascript" src="./js/campaignParametersForm.js"></script> 

<h4>Choose filters or search on name</h4>
<!-- CSS/JS for DatePicker  -->
<link rel="stylesheet" href="./css/jquery-ui.css">
<link rel="stylesheet" href="./css/jquery-ui.min.css">
<link rel="stylesheet" href="./css/jquery-ui.structure.css">
<link rel="stylesheet" href="./css/jquery-ui.structure.min.css">
<link rel="stylesheet" href="./css/jquery-ui.theme.css">
<link rel="stylesheet" href="./css/jquery-ui.theme.min.css">



<script>
    $(function () {       
       $('#league').change(function () {
           if ( $('#league').is(':checked') ) {
                $.ajax({url: 'Views/Reports/getLeagueDesignersAjax.php?',
                        success: function (output) {
                           // alert(output);
                            $("#select1").html(output);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.status + " " + thrownError);
                        }});
                    
                var designerArray = "";
                $("#select2 option").each(function() {
                    designerArray += $(this).val()+",";
                });

                
                $.ajax({url: 'Views/Reports/getSelectedSeriesAjax.php?designerArray=' + designerArray + '&league=1',
                        success: function (output) {
                           // alert(output);
                            $("#model1").html(output);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.status + " " + thrownError);
                        }});
            } else {
                $.ajax({url: 'Views/Reports/getAllDesignersAjax.php?',
                        success: function (output) {
                           // alert(output);
                            $("#select1").html(output);
                            
                            var designerArray = "";
                            $("#select2 option").each(function() {
                                designerArray += $(this).val()+",";
                            });
                            //alert(designerArray);

                            $.ajax({url: 'Views/Reports/getSelectedSeriesAjax.php?designerArray=' + designerArray + '&league=0',
                                    success: function (output) {
                                        //alert(output);
                                        $("#model1").html(output);
                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {
                                        alert(xhr.status + " " + thrownError);
                                    }});
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.status + " " + thrownError);
                        }});
            }
       });
        
        // add the series options when designers are chosen.
        $("#add").click(function() {
            var leagueOnly = '0';
            if ( $('#league').is(':checked') ) {
                leagueOnly = '1';
            } 
            var designerArray = JSON.stringify($( "#select2" ).val());
            
            var newdesignerArray = designerArray.replace("&", "-amp-");
            var unstring = JSON.parse(newdesignerArray);

            $.ajax({url: 'Views/Reports/getSelectedSeriesAjax.php?designerArray=' + unstring + '&league=' + leagueOnly,
                        success: function (output) {
                            //alert(output);
                            $("#model1").html(output);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.status + " " + thrownError);
                        }});
        });
        
        $("#remove").click(function() {
            var leagueOnly = '0';
            if ( $('#league').is(':checked') ) {
                leagueOnly = '1';
            } 
            var designerArray = "";
            $("#select2 option").each(function() {
                designerArray += $(this).val()+",";
            });
            designerArray = designerArray.slice(0,-1);
            $.ajax({url: 'Views/Reports/getSelectedSeriesAjax.php?designerArray=' + designerArray + '&league=' + leagueOnly,
                        success: function (output) {
                            $("#model1").html(output);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.status + " " + thrownError);
                        }});
        });
    });
</script>
<!-- /End CSS/JS for DatePicker -->
<?php
include './Views/AutoCompleteSearch/autoCompleteSearchCustomerName.php';

include './Views/AutoCompleteSearch/autoCompleteSearchInstallation.php';

include './Views/AutoCompleteSearch/autoCompleteSearchPastInstallationNames.php';

include './Views/AutoCompleteSearch/autoCompleteSearchIMO.php';

//if ($_SESSION['level'] == '1') {
?>
<hr/>
<h4>Filters</h4>
<p class="red">WARNING: Due to the amount of data, running reports with filters can take 15+ minutes.</p>
<form name='form' action="index.php?portal=mr&page=report&action=create" method="post" enctype="multipart/form-data">
    
    
    <table>
        <tr>
            <td colspan="3" class="blue">Search partial company / ship manager name and generate a report of matches</td>
        </tr>
        <tr>
            <td colspan="3"><input type="text" name="companyNameSearch" size="60" placeholder="Type company / ship manager name" /></td>
        </tr>
        <tr>
            <td colspan="3" class="blue">Search partial installation name and generate a report of matches</td>
        </tr>
        <tr>
            <td colspan="3"><input type="text" name="instNameSearch" size="60" placeholder="Type installation name" /></td>
        </tr>
        <tr>
            <td colspan="3" class="blue">Search based on Special Survey Due Dates</td>
        </tr>
        <tr>
            <td colspan="3" >From 
                <select name="ssFromDay">
                    <option value="any">Any</option>
                <?php
                    for ($i=1; $i <= 31; $i++) {
                        print '<option value="'.$i.'">'.$i.'</option>';
                    }
                
                ?>
                </select>
                
                <select name="ssFromMonth">
                    <option value="any">Any</option>
                <?php
                    for ($i=1; $i <= 12; $i++) {
                        print '<option value="'.$i.'">'.$i.'</option>';
                    }
                
                ?>
                </select>
                
                <input type="text" name="ssFromYear" value="" size="6" placeholder="YYYY" pattern="[0-9]{4}"/>
                
                
                To <select name="ssToDay">
                    <option value="any">Any</option>
                <?php
                    for ($i=1; $i <= 31; $i++) {
                        print '<option value="'.$i.'">'.$i.'</option>';
                    }
                
                ?>
                </select>
                
                <select name="ssToMonth">
                    <option value="any">Any</option>
                <?php
                    for ($i=1; $i <= 12; $i++) {
                        print '<option value="'.$i.'">'.$i.'</option>';
                    }
                
                ?>
                </select>
                <input type="text" name="ssToYear" value="" size="6" placeholder="YYYY" pattern="[0-9]{4}"/>
            </td>
            
        </tr>
        <tr>
            <td colspan="3" class="blue">Search if record is in continent OR area OR country</td>
        </tr>
        <tr>
            <td style="vertical-align:top;" >Continent</td>
            <td>
                <Table>
                    <tr>
                        <td><select name="allContinents[]" multiple size="6" class='multiSelect' id='selectContinent1'>
                                <?php
                                $continentArray = $countryController->getDistinctContinents();

                                foreach ($continentArray as $id => $continent) {
                                    if ($continent != "") {
                                        echo '<option value="' . htmlXentities($continent) . '">' . htmlXentities($continent) . '</option>';
                                    }
                                }
                                ?>      

                            </select>
                        </td>
                        <td><div class='multiSelectButtons'>
                                <table>
                                    <tr>
                                        <td><button id="addContinent" type="button"> > </button></td>
                                    </tr>
                                    <tr>
                                        <td><button id="removeContinent" type="button"> < </button></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td><select name="continent[]" multiple size='6' class='multiSelect' id="selectContinent2">
                               <?php
                                if (isset($_POST['parameterArray']['continent'])) {
                                    foreach ($_POST['parameterArray']['continent'] as $number => $continent) {
                                        print "<option value = '".$continent."' selected = 'selected'>".$continent."</option>";
                                    }
                                }
                                
                                ?>
                            </select></td>
                    </tr>
                </Table>



                </div>
            </td>
        </tr>
        <tr>
            <td style="vertical-align:top;">Area</td>
            <td>
                <Table>
                    <tr>
                        <td><select name="allAreas[]" multiple size="6" class='multiSelect' id='selectArea1'>
                                <?php
                                $areaArray = $countryController->getDistinctAreas();

                                foreach ($areaArray as $id => $area) {
                                    if ($area != "") {
                                        echo '<option value="' . htmlXentities($area) . '">' . htmlXentities($area) . '</option>';
                                    }
                                }
                                ?>      

                            </select>
                        </td>
                        <td><div class='multiSelectButtons'>
                                <table>
                                    <tr>
                                        <td><button id="addArea" type="button"> > </button></td>
                                    </tr>
                                    <tr>
                                        <td><button id="removeArea" type="button"> < </button></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td><select name="area[]" multiple size='6' class='multiSelect' id="selectArea2">
                            <?php
                                if (isset($_POST['parameterArray']['area'])) {
                                    foreach ($_POST['parameterArray']['area'] as $number => $area) {
                                        print "<option value = '".$area."' selected = 'selected'>".$area."</option>";
                                    }
                                }
                                
                                ?>
                            </select></td>
                    </tr>
                </Table>

                </div>
            </td>
        </tr>
        <tr>
            <td style="vertical-align:top;">Country</td>
            <td>
                <Table>
                    <tr>
                        <td><select name="allCountries[]" multiple size="6" class='multiSelect' id='selectCountry1'>
                                <?php
                                $countryArray = $countryController->getCountries();

                                foreach ($countryArray as $countryName => $country) {
                                    if ($country != "") {
                                        echo '<option value="' . $country->countryName . '">' . $country->countryName . '</option>';
                                    }
                                }
                                ?>      

                            </select>
                        </td>
                        <td><div class='multiSelectButtons'>
                                <table>
                                    <tr>
                                        <td><button id="addCountry" type="button"> > </button></td>
                                    </tr>
                                    <tr>
                                        <td><button id="removeCountry" type="button"> < </button></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td><select name="country[]" multiple size='6' class='multiSelect' id="selectCountry2">
                                <?php
                                if (isset($_POST['parameterArray']['country'])) {
                                    foreach ($_POST['parameterArray']['country'] as $number => $country) {
                                        print "<option value = '".$country."' selected = 'selected'>".$country."</option>";
                                    }
                                }
                                
                                ?>
                            </select></td>
                    </tr>
                </Table>
                </div>
            </td>
        </tr>

        <tr>
            <td colspan="2"></td>
        </tr>
       
        <tr>
            <td>Type of Product</td>
            <td><input type="hidden" name="typeProduct" value="all" />
                <select name="typeProduct" id="typeProduct">
                    <option value="all">All</option>
                    <?php
                    $selectedType = "";
                    if (isset($_POST['parameterArray']['productType'])) {
                        $selectedType = $_POST['parameterArray']['productType'];
                    }
                    $typeOfProductList = $typeOfProductLookupController->getAllProductTypes();
                    
                    foreach ($typeOfProductList as $key => $type) {
                        $selected = "";
                        if ($key == $selectedType) {
                            $selected = "selected='selected'";
                        }
                        print '<option value="'.$key.'" '.$selected.'>'.$type->toplProductDescription.'</option>';
                    }
                    
                    ?>
                   

                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td>STC League Only</td>
            <td><input type="checkbox" id='league' name="league" value="1" checked="checked"/></td>
        </tr>
        <tr>
            <td style="vertical-align:top;">Manufacturer / Product Name</td>
            <td>
                <Table>
                    <tr>
                        <td><select name="allEngines[]" multiple size="6" class='multiSelect' id='select1'>
                                <?php
                                $engineManufacturers = $designerLookupController->getDistinctDesignersLeagueOnly();

                                foreach ($engineManufacturers as $name => $engine) {
                                    if ($engine->designerDescription != "") {
                                        echo '<option value="' . htmlXentities($engine->designerDescription) . '">' . htmlXentities($engine->designerDescription) . '</option>';
                                    }
                                }
                                ?>      

                            </select>
                        </td>
                        <td><div class='multiSelectButtons'>
                                <table>
                                    <tr>
                                        <td><button id="add" type="button"> > </button></td>
                                    </tr>
                                    <tr>
                                        <td><button id="remove" type="button"> < </button></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td><select name="engineDesign[]" multiple size='6' class='multiSelect' id="select2">
                                <?php
                                if (isset($_POST['parameterArray']['engineManufacturer'])) {
                                    foreach ($_POST['parameterArray']['engineManufacturer'] as $number => $designer) {
                                        print "<option value = '".$designer."' selected = 'selected'>".$designer."</option>";
                                    }
                                }
                                
                                ?>
                            </select></td>
                    </tr>
                </Table>



                </div>
            </td>
        </tr>
        <tr>
            <td style="vertical-align:top;">Series</td>
            <td>
                <Table>
                    <tr>
                        <td><select name="allModels[]" multiple size="6" class='multiSelect' id='model1'>
                               
                            </select>
                        </td>
                        <td><div class='multiSelectButtons'>
                                <table>
                                    <tr>
                                        <td><button id="addModel" type="button"> > </button></td>
                                    </tr>
                                    <tr>
                                        <td><button id="removeModel" type="button"> < </button></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td><select name="engineModel[]" multiple size='6' class='multiSelect' id="model2">
                                <?php
                                if (isset($_POST['parameterArray']['engineModel'])) {
                                    foreach ($_POST['parameterArray']['engineModel'] as $number => $series) {
                                        print "<option value = '".$series."' selected = 'selected'>".$series."</option>";
                                    }
                                }
                                
                                ?>
                            </select></td>
                    </tr>
                </Table>

            </td>
        </tr>

        <tr>
            <td>Main or Aux</td>
            <td>
                <select name="mainOrAux">
                    <option value="all">All</option>
                    <option value="Main">Main</option>
                    <option value="Aux">Aux</option>
                </select>
            </td>
        </tr>

        <tr>
            <td>Account Manager</td>
            <td>
                <select name="owner">
                    <option value="all">All</option>
                    <?php
                    $salesManagers = $userController->getSalesManagers();

                    foreach ($salesManagers as $name => $salesManager) {
                        echo '<option value="' . $salesManager->id . '">' . $salesManager->name . '</option>';
                    }
                    ?>                 
                </select>
            </td>
        </tr>
    </table>
    <p><input type='submit' name='submit' value="submit"/></p>
</form>
<?php
//}