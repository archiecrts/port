<?php

/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
/*$server="";
if (isset($_SESSION['server'])) {
    $server = $_SESSION['server'];
} else if (isset($_GET['server'])) {
    $server = $_GET['server'];
}*/
$user="";
if (isset($_SESSION['user_id'])) {
    $user = $_SESSION['user_id'];
}  else if (isset($_GET['user'])) {
    $user = $_GET['user'];
}
include_once("../../Models/Database.php");
include_once("../../Models/DatabaseHistory.php");

$name = "";
$id = "";

if (isset($_GET['name'])) {
    $name = addslashes($_GET['name']);  
    $id = $_GET['id'];
}
// Get the previous record for the history
$history = mysqli_query(Database::$connection, "SELECT * FROM report_overview_tbl WHERE rot_id='".$id."'");
$row = mysqli_fetch_assoc($history);
// update the current record
$result = "UPDATE report_overview_tbl SET rot_report_name = '".$name."' WHERE rot_id = '".$id."'";
 
if (!mysqli_query(Database::$connection, $result)) {
    echo mysqli_error();
} else {
    $result = "INSERT INTO ".$databaseHistory.".report_overview_history_tbl (rot_id, rot_report_name, "
                . "rot_date_created, rot_user_id, rot_email_sent, rot_dynamic_flag, rot_report_active, "
                . "report_history_changed_by_id, report_history_date_changed) "
                . "VALUES ("
                . "'".$row["rot_id"]."', "
                . "'".$row["rot_report_name"]."', "
                . "'".$row["rot_date_created"]."', "
                . "'".$row["user_tbl_usr_id"]."', "
                . "'".$row["rot_email_sent"]."', "
                . "'".$row["rot_dynamic_flag"]."', "
                . "'".$row["rot_report_active"]."', "
                . "'".$user."', "
                . "'".date("Y-m-d H:m:s", time())."')";
        
        if (!mysqli_query(DatabaseHistory::$connection, $result))
        {
            echo mysqli_error(DatabaseHistory::$connection);
        }
    echo $name;
}
