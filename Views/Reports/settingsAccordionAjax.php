<?php

/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
/*if (isset($_SESSION['server'])) {
    $server = $_SESSION['server'];
} else if (isset($_GET['server'])) {
    $server = $_GET['server'];
}*/
include_once("../../Models/Database.php");


$columnID = "";
$userID = "";
$active = "";



if (isset($_GET['columnID'])) { 
    $columnID = $_GET['columnID'];
}
if (isset($_GET['userID'])) { 
    $userID = $_GET['userID'];
}
if (isset($_GET['active'])) {
    $active = $_GET['active'];
}


if (($userID != "") && ($columnID != "") && ($active != "")) {
    $result = "UPDATE report_user_display_settings_tbl SET ruds_active_flag = '".$active."' "
            . "WHERE report_user_display_column_names_tbl_rudcn_id IN (SELECT rudcn_id FROM "
            . "report_user_display_column_names_tbl WHERE rudcn_column_name = '".$columnID."' ORDER BY rudcn_id) AND "
            . "user_tbl_usr_id = '".$userID."'";
 
    if (!mysqli_query(Database::$connection, $result)) {
        echo mysqli_error(Database::$connection);
    } else {
        echo mysqli_affected_rows(Database::$connection);
    }

}
