<script>
$(document).ready(function() {
    document.getElementById('hide').style.display = "none";
    $("#show").click(function(){
        document.getElementById('hide').style.display = "block";
        document.getElementById('show').style.display = "none";
        document.getElementById('reportParametersTable').style.display = "block";
    }); 
    
    $("#hide").click(function(){
        document.getElementById('hide').style.display = "none";
        document.getElementById('show').style.display = "block";
        document.getElementById('reportParametersTable').style.display = "none";
    }); 
});
</script>
<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
$parameters = $reportParametersController->getAllParametersByReportOverviewID($report->reportOverviewID);
print "<div class='parametersBorder'><table id='header'><tr><td>Report Parameters</td>";
print "<td><button id='show' style='width:25px'>+</button><button id='hide' style='width:25px'>-</button></td></tr></table>";
print "<table id='reportParametersTable'>";
foreach ($parameters as $paramID => $object) {
    $value = htmlXentities($object->value);
    if ($object->parameter == 'salesManager') {
        $value = $userController->getUserByID($object->value)->name;
    }
    $split = preg_split("/(?<=[a-z])(?![a-z])/", ucfirst ($object->parameter), -1, PREG_SPLIT_NO_EMPTY);
    print "<tr><td><strong>"; 
    foreach ($split as $key => $val) {
        print $val." ";
    }
    print "</strong></td><td>" . $value . "</td></tr>";
}

print "</table>";
print "</div>";