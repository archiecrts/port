<?php

/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

?>
<script>
    
    $(function() {
    $( "#dialog-confirm" ).dialog({
        autoOpen: false,
        resizable: false,
        height:220,
        modal: true,
        buttons: {
          "Confirm": function() {
              
            $( this ).dialog( "close" );
            $('#removeForm').submit();
          },
          Cancel: function() {
            $( this ).dialog( "close" );
          }
        },
        create:function () {
            $(this).closest(".ui-dialog").find(".ui-button").addClass("custom");
        }
    });
  });
  
  function removeEntries() {
      $( "#dialog-confirm" ).dialog( "open" );
  };
</script>

<div id="dialog-confirm" title="Are you sure?">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This will remove all highlighted installations.<br/>Are you sure?</p>
</div>
<p>Installations in orange have appeared on another campaign within the last 3 months. <br/>
Installations in red have appeared on two or more campaigns within the last 3 months.</p>
<form id="removeForm" name="removeForm" action="index.php?portal=mr&page=report&action=showReportCampaign&campaignOverviewID=<?php echo $reportOverviewID; ?>&remove=remove" method="post" enctype="multipart/form-data">
<table>
    <tr>
        <td><label for="orangeInst">Remove Orange Installations</label><input type="checkbox" id="orangeInst" name="orange" value="1" /></td>
        <td><label for="redInst">Remove Red Installations</label><input type="checkbox" name="red" id="redInst" value="1" /></td>
        <td><input type="button" name="remove" value="remove" onclick='removeEntries()'/></td>
    </tr>
</table>
</form>