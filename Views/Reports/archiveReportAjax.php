<?php

$user = "";
if (isset($_SESSION['user_id'])) {
    $user = $_SESSION['user_id'];
} else if (isset($_GET['user_id'])) {
    $user = $_GET['user_id'];
}
include_once("../../Models/Database.php");
include_once("../../Models/DatabaseHistory.php");

$reportID = "";

if (isset($_GET['reportID'])) {
    $reportID = $_GET['reportID'];

// Get the previous record for the history
    $history = mysqli_query(Database::$connection, "SELECT * FROM report_overview_tbl WHERE rot_id='" . $reportID . "'");
    $row = mysqli_fetch_assoc($history);
// update the current record
    $result = "UPDATE report_overview_tbl SET rot_report_active = '0' WHERE rot_id = '" . $reportID . "'";

    if (!mysqli_query(Database::$connection, $result)) {
        echo mysqli_error(Database::$connection);
    } else {
        $result = "INSERT INTO ".$databaseHistory.".report_overview_history_tbl (rot_id, rot_report_name, "
                . "rot_date_created, rot_user_id, rot_email_sent, rot_dynamic_flag, rot_report_active, "
                . "report_history_changed_by_id, report_history_date_changed) "
                . "VALUES ("
                . "'" . $row["rot_id"] . "', "
                . "'" . $row["rot_report_name"] . "', "
                . "'" . $row["rot_date_created"] . "', "
                . "'" . $row["user_tbl_usr_id"] . "', "
                . "'" . $row["rot_email_sent"] . "', "
                . "'" . $row["rot_dynamic_flag"] . "', "
                . "'" . $row["rot_report_active"] . "', "
                . "'" . $user . "', "
                . "'" . date("Y-m-d H:m:s", time()) . "')";

        if (!mysqli_query(DatabaseHistory::$connection, $result)) {
            echo mysqli_error(DatabaseHistory::$connection);
        }
        echo "success";
    }
} else {
    echo "no report ID";
}

//mysqli_close($dbhandle);
