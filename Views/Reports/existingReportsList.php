<h3>Manage Reports</h3>

<script>
    $(document).ready(function () {

        // This will automatically grab the 'title' attribute and replace
        // the regular browser tooltips for all <div> elements with a title attribute!
        $('div[title]').qtip();
    });

    $(function () {
        $("#dialog-confirm").dialog({
            autoOpen: false,
            resizable: false,
            height: 220,
            modal: true,
            buttons: {
                "Confirm": function () {
                    $(this).dialog("close");
                    var reportID = $(this).data("reportId");
                    var userID = '<?php print $_SESSION['user_id']; ?>';
                    // alert(reportID);
                    $.ajax({url: 'Views/Reports/archiveReportAjax.php?reportID=' + reportID + '&user=' + userID,
                        success: function (output) {
                            //alert(output);
                            window.location.reload();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.status + " " + thrownError);
                        }});
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            },
            create: function () {
                $(this).closest(".ui-dialog").find(".ui-button").addClass("custom");
            }
        });
    });
// Create the tooltips only when document ready
    /*$(document).ready(function () {

        // This will automatically grab the 'title' attribute and replace
        // the regular browser tooltips for all <div> elements with a title attribute!
        $('div[title]').qtip();


        // archive report.
        $('.deleteReport').click(function () {
            var reportID = $(this).attr('id');
            var userID = '<?php //print $_SESSION['user_id']; ?>';
            // alert(reportID);
            $.ajax({url: 'Views/Reports/archiveReportAjax.php?reportID=' + reportID + '&user=' + userID,
                success: function (output) {
                    //alert(output);
                    window.location.reload();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + " " + thrownError);
                }});
        });

    });*/

    function deleteReport(reportId) {
        $("#dialog-confirm").data('reportId', reportId).dialog("open");
    };
</script>
<div id="dialog-confirm" title="Are you sure?">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This will archive the report.<br/>Are you sure?</p>
</div>
<table class='borderedTable'>
    <tr>
        <th class="rounded-corners-left-top">Report Name</th>
        <th>Filters</th>
       <!-- <th>Type</th>-->
        <th>Start Date</th>
        <th>Creator</th>
        <th class="rounded-corners-right-top"></th>
    </tr>
    <?php
    if (isset($reports)) {
        foreach ($reports as $name => $report) {
            if ($report->reportName != "AUTO GENERATED NAME") {
                $parameters = $reportParametersController->getAllParametersByReportOverviewID($report->reportOverviewID);

                $parametersString = "";
                foreach ($parameters as $paramID => $object) {
                    $value = htmlXentities($object->value);
                    if ($object->parameter == 'salesManager') {
                        $value = $userController->getUserByID($object->value)->name;
                    }
                    $split = preg_split("/(?<=[a-z])(?![a-z])/", ucfirst($object->parameter), -1, PREG_SPLIT_NO_EMPTY);
                    foreach ($split as $key => $val) {
                        $parametersString .= "" . $val . " ";
                    }
                    $parametersString .= ": " . $value . "<br />";
                }




                echo '<tr>'
                . '<td><a href="index.php?portal=mr&page=report&action=showReport&reportOverviewID=' . $report->reportOverviewID . '"><strong>' . $report->reportName . '</strong></a></td>';
                echo '<td align="center">
                <div title="<span class=\'qtip-cust-title\'>PARAMETERS</span><br/>' . $parametersString . '" class="hasTooltip"><img src="./icons/questionMarkCircle.png" height="16px" /></div>
                </td>';

                echo '<td>' . date("d-m-Y", strtotime($report->dateCreated)) . '</td>';

                echo '<td>' . $userController->getUserByID($report->userID)->name . '</td>';
                echo "<td><a href='javascript:;' onclick='deleteReport($report->reportOverviewID)' class='deleteReport' id='" . $report->reportOverviewID . "'><img src='./icons/removeArticle.png' alt='remove article' width='20px height='20px' border='0'></a></td>";
                echo '</tr>';
            }
        }
    }
    ?>

</table>
<p>&nbsp;</p>