<script>
    function alterReportName() {
        var div = document.getElementById("reportNameDiv");
        var form = document.getElementById("reportNameDivForm");

        div.style.display = 'none';
        form.style.display = 'block';

    }

    function saveForm(reportID) {
        var div = document.getElementById("reportNameDiv");
        var form = document.getElementById("reportNameDivForm");
        var text = document.getElementById("newName").value;
        var server = '<?php print $_SESSION['server']; ?>';
        var userID = '<?php print $_SESSION['user_id']; ?>';

        $.ajax({url: 'Views/Reports/changeReportName.php?id=' + reportID + '&name=' + text + '&server=' + server + '&user=' + userID,
            success: function (output) {
                //alert(output);
                $('#reportName').html(output);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status + " " + thrownError);
            }});



        div.style.display = 'block';
        form.style.display = 'none';
    }
</script>



<?php
/**
 * SET CONTROLLERS.
 */
include_once ('Controllers/InstallationController.php');
include_once ('Controllers/GetInstallationsArrayController.php');
include_once ('Controllers/CustomerContactsController.php');
include_once ('Controllers/CustomerConversationController.php');
include_once ('Controllers/ReportOverviewController.php');
include_once ('Controllers/ReportParametersController.php');
include_once ('Controllers/ReportController.php');
include_once 'Controllers/ReportUserDisplaySettingsController.php';
include_once ('Controllers/UserController.php');
include_once 'Controllers/UserTerritoryController.php';
include_once 'Controllers/CountryController.php';
include_once ("Controllers/UserHistoryController.php");
include_once 'Controllers/CustomerController.php';
include_once 'Controllers/CustomerAddressController.php';
include_once 'Controllers/CustomerStatusLookupController.php';
include_once 'Controllers/CustomerStatusController.php';
include_once 'Controllers/InstallationEngineController.php';
include_once 'Controllers/InstallationStatusController.php';
include_once 'Controllers/LoadPriorityController.php';
include_once 'Controllers/ProductController.php';
include_once 'Controllers/DesignerLookupController.php';
include_once 'Controllers/SeriesLookupController.php';
include_once 'Controllers/TypeOfProductLookupController.php';
include_once 'Controllers/CustomerHasInstallationsController.php';
include_once 'Controllers/VersionLookupController.php';
include_once 'Controllers/TradingZoneLastSeenController.php';
include_once 'Controllers/CustomerFleetCountsController.php';
include_once 'Controllers/SpecialSurveyDueDateController.php';
include_once 'Controllers/SpecialSurveyHistoryDatesController.php';
include_once 'Controllers/ShipNameHistoryController.php';

// History Controllers
include_once 'ControllersHistory/ReportOverviewHistoryController.php';
include_once 'ControllersHistory/ReportParametersHistoryController.php';
include_once 'ControllersHistory/CustomerHistoryController.php';
include_once 'ControllersHistory/CustomerAddressHistoryController.php';
include_once 'ControllersHistory/CustomerContactHistoryController.php';
include_once 'ControllersHistory/CustomerCreditHistoryController.php';
include_once 'ControllersHistory/InstallationHistoryController.php';
include_once 'ControllersHistory/InstallationEngineHistoryController.php';
include_once 'ControllersHistory/CustomerStatusHistoryController.php';
include_once 'ControllersHistory/ProductHistoryController.php';
include_once 'ControllersHistory/CustomerHasInstallationsHistoryController.php';



$installationStatusController = new InstallationStatusController();
$countryController = new CountryController();
$customerAddressController = new CustomerAddressController();
$customerController = new CustomerController();
$customerContactsController = new CustomerContactsController();
$customerConversationController = new CustomerConversationController();
$getInstallationsArrayController = new GetInstallationsArrayController();
$installationController = new InstallationController();
$installationEngineController = new InstallationEngineController();
$reportController = new ReportController();
$reportOverviewController = new ReportOverviewController();
$reportParametersController = new ReportParametersController();
$reportUserDisplaySettingsController = new ReportUserDisplaySettingsController();
$userController = new UserController();
$userHistoryController = new UserHistoryController();
$userTerritoryController = new UserTerritoryController();
$loadPriorityController = new LoadPriorityController();
$customerStatusLookupController = new CustomerStatusLookupController();
$customerStatusController = new CustomerStatusController();
$productController = new ProductController();
$designerLookupController = new DesignerLookupController();
$seriesLookupController = new SeriesLookupController;
$typeOfProductLookupController = new TypeOfProductLookupController();
$customerHasInstallationsController = new CustomerHasInstallationsController();
$versionLookupController = new VersionLookupController();
$tradingZoneLastSeenController = new TradingZoneLastSeenController();
$customerFleetCountsController = new CustomerFleetCountsController();
$specialSurveyHistoryDatesController = new SpecialSurveyHistoryDatesController();
$specialSurveyDueDateController = new SpecialSurveyDueDateController();
$shipNameHistoryController = new ShipNameHistoryController();
// History Controllers
$reportOverviewHistoryController = new ReportOverviewHistoryController();
$reportParametersHistoryController = new ReportParametersHistoryController();
$customerHistoryController = new CustomerHistoryController();
$customerAddressHistoryController = new CustomerAddressHistoryController();
$customerContactHistoryController = new CustomerContactHistoryController();
$customerCreditHistoryController = new CustomerCreditHistoryController();
$installationHistoryController = new InstallationHistoryController();
$installationEngineHistoryController = new InstallationEngineHistoryController();
$customerStatusHistoryController = new CustomerStatusHistoryController();
$productHistoryController = new ProductHistoryController();
$customerHasInstallationsHistoryController = new CustomerHasInstallationsHistoryController();






require_once 'Helpers/charsetFunctions.php';
$action = "none";
if (isset($_GET['action'])) {
    $action = $_GET['action'];
}

/**
 * SET variables.
 */
$continent = "";
$area = "";
$country = "";
$salesManager = "";
$engineManufacturer = "";
$engineModel = "";
$actionDueFrom = "";
$actionDueUntil = "";
$lastCalledFrom = "";
$lastCalledUntil = "";
$actionType = "";
$productType = "";
$mainOrAux = "";
$companySearchName = "";
$instSearchName = "";
$ssFromDate = "";
$ssToDate = "";

// this is a function that allows in_array to be used in a case insensitive way.
function in_arrayi($needle, $haystack) {
    return in_array(strtolower($needle), array_map('strtolower', $haystack));
}

switch ($action) {
    case 'create':
        /*
         * This case shows the results of the chosen form parameters.
         */
        $parametersArray = [];

        if (isset($_POST['companyNameSearch']) && ($_POST['companyNameSearch'] != '')) {
            $companySearchName = $_POST['companyNameSearch'];
            $parametersArray['companySearchName'] = $companySearchName;
        }

        if (isset($_POST['instNameSearch']) && ($_POST['instNameSearch'] != '')) {
            $instSearchName = $_POST['instNameSearch'];
            $parametersArray['instSearchName'] = $instSearchName;
        }

        if ((isset($_POST['continent'])) && ($_POST['continent'] != 'all')) {
            $continent = $_POST['continent'];
            $parametersArray['continent'] = $continent;
        }

        if ((isset($_POST['area'])) && ($_POST['area'] != 'all')) {
            $area = $_POST['area'];
            $parametersArray['area'] = $area;
        }

        if ((isset($_POST['country'])) && ($_POST['country'] != 'all')) {
            $country = $_POST['country'];
            $parametersArray['country'] = $country;
        }
        if ((isset($_POST['owner'])) && ($_POST['owner'] != 'all')) {
            $salesManager = $_POST['owner'];
            $parametersArray['salesManager'] = $salesManager;
        }
        // ENGINES
        if ((isset($_POST['engineDesign'])) && ($_POST['engineDesign'] != 'all')) {
            $engineManufacturer = $_POST['engineDesign'];
            $parametersArray['engineManufacturer'] = $engineManufacturer;
        }
        if ((isset($_POST['engineModel'])) && ($_POST['engineModel'] != 'all')) {
            $engineModel = $_POST['engineModel'];
            $parametersArray['engineModel'] = $engineModel;
        }

        /* if (((isset($_POST['actionDueFrom'])) && ($_POST['actionDueFrom'] != '')) &&
          ((isset($_POST['actionDueUntil'])) && ($_POST['actionDueUntil'] != ''))) {
          $actionDueFrom = $_POST['actionDueFrom'];
          $actionDueUntil = $_POST['actionDueUntil'];
          $parametersArray['actionDueFrom'] = $actionDueFrom;
          $parametersArray['actionDueUntil'] = $actionDueUntil;
          }

          if (((isset($_POST['lastCalledFrom'])) && ($_POST['lastCalledFrom'] != '')) &&
          ((isset($_POST['lastCalledUntil'])) && ($_POST['lastCalledUntil'] != ''))) {
          $lastCalledFrom = $_POST['lastCalledFrom'];
          $lastCalledUntil = $_POST['lastCalledUntil'];
          $parametersArray['lastCalledFrom'] = $lastCalledFrom;
          $parametersArray['lastCalledUntil'] = $lastCalledUntil;
          }
          if ((isset($_POST['typeAction'])) && ($_POST['typeAction'] != 'all')) {
          $actionType = $_POST['typeAction'];
          $parametersArray['actionType'] = $actionType;
          } */
        if ((isset($_POST['mainOrAux'])) && ($_POST['mainOrAux'] != 'all')) {
            $mainOrAux = $_POST['mainOrAux'];
            $parametersArray['mainOrAux'] = $mainOrAux;
        }
        if ((isset($_POST['typeProduct'])) && ($_POST['typeProduct'] != 'all')) {
            $productType = $_POST['typeProduct'];
            $parametersArray['productType'] = $productType;
        }

        if ((isset($_POST['ssFromDay'])) && ($_POST['ssFromDay'] != 'any') &&
                (isset($_POST['ssFromMonth'])) && ($_POST['ssFromMonth'] != 'any') &&
                (isset($_POST['ssFromYear'])) && ($_POST['ssFromYear'] != "")) {
            $ssFromDate = $_POST['ssFromYear'] . "-" . $_POST['ssFromMonth'] . "-" . $_POST['ssFromDay'];
            $parametersArray['ssFromDate'] = $ssFromDate;
        } else if ((($_POST['ssFromDay'] == 'any') || ($_POST['ssFromMonth'] == 'any')) && ($_POST['ssFromYear'] != "")) {

            if ($_POST['ssFromDay'] == 'any') {
                $_POST['ssFromDay'] = 01;
            }

            if ($_POST['ssFromMonth'] == 'any') {
                $_POST['ssFromMonth'] = 01;
            }

            $ssFromDate = $_POST['ssFromYear'] . "-" . $_POST['ssFromMonth'] . "-" . $_POST['ssFromDay'];
            $parametersArray['ssFromDate'] = $ssFromDate;
        }

        if ((isset($_POST['ssToDay'])) && ($_POST['ssToDay'] != 'any') &&
                (isset($_POST['ssToMonth'])) && ($_POST['ssToMonth'] != 'any') &&
                (isset($_POST['ssToYear'])) && ($_POST['ssToYear'] != "")) {
            $ssToDate = $_POST['ssToYear'] . "-" . $_POST['ssToMonth'] . "-" . $_POST['ssToDay'];
            $parametersArray['ssToDate'] = $ssToDate;
        } else if ((($_POST['ssToDay'] == 'any') || ($_POST['ssToMonth'] == 'any')) && ($_POST['ssToYear'] != "")) {


            if ($_POST['ssToMonth'] == 'any') {
                $_POST['ssToMonth'] = 12;
            }

            if ($_POST['ssToDay'] == 'any') {
                $_POST['ssToDay'] = cal_days_in_month(CAL_GREGORIAN, $_POST['ssToMonth'], $_POST['ssToYear']);
            }

            $ssToDate = $_POST['ssToYear'] . "-" . $_POST['ssToMonth'] . "-" . $_POST['ssToDay'];
            $parametersArray['ssToDate'] = $ssToDate;
        }

        // check that they aren't ALL set to 'all' otherwise the entire database will be selected and this will probably
        // crash everything.
        if (sizeof($parametersArray) < 1) {

            print '<meta http-equiv="refresh" content="0;url=index.php?portal=mr&page=report&error=all"/>';
        } else {

            // Get the results.
            $param = $getInstallationsArrayController->
                    getInstallationsArray($continent, $area, $country, $salesManager, $engineManufacturer, $engineModel, $mainOrAux, $actionDueFrom, $actionDueUntil, $lastCalledFrom, $lastCalledUntil, $actionType, $productType, $instSearchName, $companySearchName, $ssFromDate, $ssToDate);

            // debugWriter("./debug.txt", sizeof($param));
            // Create a reportOverview object to be set.
            $report = new ReportOverview("", "AUTO GENERATED NAME", date("Y-m-d H:i:s"), $_SESSION['user_id'], "", 1, 1);
            $mysqlAutogeneratedId = $reportOverviewController->setReportOverview($report);

            if (!isset($reportOverviewID)) {
                $reportOverviewID = $mysqlAutogeneratedId;
            }
            // Set the parameters used for the new query. This will make it a report (dynamic).
            foreach ($parametersArray as $parameter => $value) {
                if ($parameter == 'engineManufacturer') {
                    foreach ($value as $engManufacturer => $manufacturer) {
                        $result = $reportParametersController->
                                setReportParameter($mysqlAutogeneratedId, $parameter, $manufacturer);
                    }
                } else if ($parameter == 'engineModel') {
                    foreach ($value as $engineModel => $model) {
                        $result = $reportParametersController->
                                setReportParameter($mysqlAutogeneratedId, $parameter, $model);
                    }
                } else if ($parameter == 'area') {
                    foreach ($value as $geoArea => $area) {
                        $result = $reportParametersController->
                                setReportParameter($mysqlAutogeneratedId, $parameter, $area);
                    }
                } else if ($parameter == 'continent') {
                    foreach ($value as $world => $continent) {
                        $result = $reportParametersController->
                                setReportParameter($mysqlAutogeneratedId, $parameter, $continent);
                    }
                } else if ($parameter == 'country') {
                    foreach ($value as $geoCountry => $country) {
                        $result = $reportParametersController->
                                setReportParameter($mysqlAutogeneratedId, $parameter, $country);
                    }
                } else {
                    $result = $reportParametersController->
                            setReportParameter($mysqlAutogeneratedId, $parameter, $value);
                }
            }
            // Settings 
            $settingsArray = $reportUserDisplaySettingsController->getSettingsByID($_SESSION['user_id']);
            $settingsColumnNames = [];

            foreach ($settingsArray as $key => $object) {
                array_push($settingsColumnNames, $object->columnName);
            }
            // Show Save functions.
            include('Views/Reports/saveReportForm.php');

            include ('Views/Reports/refineSearch.php');
            // Create the visible table of results.
            include('Views/Reports/installationsTable.php');
        }
        break;

    case 'saveReportData':


        // Save the report data.
        // Get parameters of report.
        $reportName = "";
        $reportOverviewID = "";
        $dynamicFlag = 1;


        // Check the post vars are set and apply them to the above local variables for use.
        if (isset($_POST['reportName'])) {
            $reportName = addslashes($_POST['reportName']);
        }
        if (isset($_POST['autoGeneratedId'])) {
            $reportOverviewID = $_POST['autoGeneratedId'];
        }


        // Update to the new report name
        $update = $reportOverviewController->
                updateReportOverview($reportOverviewID, $reportName, $dynamicFlag);



        // Log in the DB
        $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "create", "New Report Created: (" . $reportOverviewID . ") " . $reportName);

        // Create the visible table of results.
        print '<meta http-equiv="refresh" content="0;url=index.php?portal=mr&page=report&action=showReport&reportOverviewID=' . $reportOverviewID . '">';
        break;

    case "showReport":
    case "adminDownload":

        print "<br/>";


        $reportOverviewID;
        if (isset($_GET['reportOverviewID'])) {
            $reportOverviewID = $_GET['reportOverviewID'];
        } else if (isset($_POST['autoGeneratedId'])) {
            $reportOverviewID = $_POST['autoGeneratedId'];
        }




        // Settings 
        $settingsArray = $reportUserDisplaySettingsController->getSettingsByID($_SESSION['user_id']);

        $settingsColumnNames = [];

        foreach ($settingsArray as $key => $object) {
            array_push($settingsColumnNames, $object->columnName);
        }



        // Display the report 
        $report = $reportOverviewController->getSingleReportByReportOverviewID($reportOverviewID);


        print "<div id='reportHeaderLeft'>";

        print "<div id='reportNameDiv'>"
                . "<h3>"
                . "<button onclick='alterReportName()'><img src='./icons/editSpanner.png' alt='edit' height='10px' /></button>"
                . " <span id='reportName'>" . $report->reportName . "</span></h3>";


        print "<p>Installations in orange have appeared on a marketing campaign within the last 3 months. <br/>
Installations in red have appeared on two or more marketing campaigns within the last 3 months.</p>";
        print "</div>";


        print "<div id='reportNameDivForm' style='display:none;'>"
                . "<h3><input type='text' id='newName' name='newName' value='" . $report->reportName . "' /> "
                . "<button onclick='saveForm(" . $reportOverviewID . ")'>Save</button>"
                . "</h3>"
                . "</div>";





        if (isset($_GET['remove'])) {
            // Need to remove unrequired installations from the table before displaying them.

            $orange = '0';
            if (isset($_POST['orange'])) {
                $orange = $_POST['orange'];
            }
            $red = '0';
            if (isset($_POST['red'])) {
                $red = $_POST['red'];
            }

            $removeInstallations = $reportController->removeInstallationFromCurrentCampaign($reportOverviewID, $report->dateCreated, $orange, $red);
        }



        // if its a report get the parameters and   make the report. else get the records.
        if ($report->dynamicFlag == '1') {
            $paramsArray = $reportParametersController->getAllParametersByReportOverviewID($reportOverviewID);




            $area = [];
            $continent = [];
            $country = [];
            $engineManufacturer = [];
            $engineModel = [];
            $auxEngineManufacturer = [];
            $auxEngineModel = [];
            if (isset($paramsArray)) {
                foreach ($paramsArray as $parameter => $object) {

                    if (($object->parameter == 'engineManufacturer') || ($object->parameter == 'engineModel') || ($object->parameter == 'auxEngineManufacturer') || ($object->parameter == 'auxEngineModel') || ($object->parameter == 'area') || ($object->parameter == 'continent') || ($object->parameter == 'country')) {
                        array_push(${$object->parameter}, utf8_encode($object->value));
                    } else {
                        ${$object->parameter} = $object->value;
                    }
                }
            }
            // Params set for the installations table.
            $param = $getInstallationsArrayController->
                    getInstallationsArray($continent, $area, $country, $salesManager, $engineManufacturer, $engineModel, $mainOrAux, $actionDueFrom, $actionDueUntil, $lastCalledFrom, $lastCalledUntil, $actionType, $productType, $instSearchName, $companySearchName, $ssFromDate, $ssToDate);
        } else {
            $param = $reportController->getAllInstallationsByReportOverviewID($reportOverviewID);
        }

        if ($report->dynamicFlag == '0') {
            include('Views/Reports/removeRecentlyContactedInstallationsForm.php');
        }

        include 'Views/Reports/reportParametersSnippet.php';
        print "</div>";


        // Settings Accordion
        print "<div id='settings'>";
        include 'Views/Reports/settingsAccordion.php';
        print "</div>";

        if ($_SESSION['level'] == 1) {
            print "<div id='adminDownload'>";
            print "<h3><img src='icons/graph.png'/><a href='index.php?portal=mr&page=report&action=adminDownload&reportOverviewID=" . $_GET['reportOverviewID'] . "'>Download Report</a></h3>";

            if ($_GET['action'] == 'adminDownload') {
                $fileName = "AdminDownload" . $_SESSION['user_id'] . time() . ".csv";
                $file = fopen("./download/" . $fileName, "w");
                $headers = reset($param); // this gets the first element of the array

                $fields = [];
                foreach ($headers as $key => $field) {
                    if (($key != "customerID") && ($key != "customerAddressID") &&
                            ($key != "installationID") && ($key != "installationEngineID")) {
                        $fields[$key] = $key;
                    }
                }
                fputcsv($file, $fields);



                fclose($file);
                $filea = fopen("./download/" . $fileName, "a");
                foreach ($param as $reportResultObject) {
                    $fields = [];
                    foreach ($reportResultObject as $key => $field) {
                        if (($key != "customerID") && ($key != "customerAddressID") &&
                                ($key != "installationID") && ($key != "installationEngineID")) {
                            if (($key == 'operatingCompany') || ($key == 'technicalManager')) {
                                $name = "";

                                if (($field != "0") && ($field != "")) {
                                    $name = $customerController->getCustomerByID($field)->customerName;
                                }
                                $fields[$key] = $name;
                            } else if ($key == 'status') {
                                $fields[$key] = $installationStatusController->getInstallationStatusByID($field)->statusDescription;
                            } else if ($key == 'accountManager') {
                                if ($field != '') {
                                    $fields[$key] = $userController->getUserByID($field)->name;
                                } else {
                                    $acName = '';
                                    if ($fields['country'] !== "") {
                                        $territoryAccountManager = $userTerritoryController->getUserIDByTerritory($fields['country']);
                                        $acName = $userController->getUserByID($territoryAccountManager)->name;
                                    }
                                    $fields[$key] = $acName;
                                }
                            } else if ($key == 'buildYear') {
                                $year = date('Y', strtotime($field));
                                $fields[$key] = $year;
                            } else if ($key == 'aliasID') {
                                $alias = "";
                                $aliasID = $field;
                                if ($aliasID != "") {
                                    $alias = $designerLookupController->getDesignerByDesignerID($field)->designerDescription;
                                }

                                if ($alias != $value && $alias != "") {
                                    $alias = $alias;
                                } else {
                                    $alias = "";
                                }

                                $fields[$key] = $alias;
                            } else {
                                $fields[$key] = $field;
                            }
                        }
                    }
                    fputcsv($filea, $fields);
                }

                fclose($filea);
                print "<p><a href='./download/" . $fileName . "' target='_blank'>Download File</a></p>";
            }

            print "</div>";
        }
        // Create the visible table of results.
        include('Views/Reports/installationsTable.php');
        break;


    case "showCustomer":


        $customerID = $_GET['customerID'];

        $installationID = 1;
        $customer = $customerController->getCustomerByID($customerID);
        $customerAddress = $customerAddressController->getDefaultCustomerAddressByCustomerID($customerID);
        $contactsArray = $customerContactsController->getContactsByCustomerID($customerID);

        $customerAddresses = $customerAddressController->getCustomerAddressesByCustomerID($customerID);
        $installationsArray = $installationController->getAllInstallationsByCustomer($customerID);

        $reportOverviewID;
        if (isset($_GET['reportID'])) {
            $reportOverviewID = $_GET['reportID'];
        }

        $tab = "";
        if (isset($_GET['tab'])) {
            $tab = $_GET['tab'];
        }
        if (isset($_GET['edit'])) {
            $edit = $_GET['edit'];
        }
        if (isset($_GET['save'])) {
            $save = $_GET['save'];
        }
        if (isset($_GET['move'])) {
            $move = $_GET['move'];
        }

        include('Views/Customers/installation.php');
        break;

    case "showInstallation":
        

        $customerID = $_GET['customerID'];
        $installationID = $_GET['installationID'];
        $installationObject = $installationController->getInstallationByInstallationID($installationID);
        if ($customerID == "") {
            // get customer ID froms customerAddressID
            $customerID = $customerAddressController->getCustomerAddressByID($installationObject->customerAddressID)->customerID;
            $_GET['customerID'] = $customerID;
        }
        $customer = $customerController->getCustomerByID($customerID);
        $customerAddress = $customerAddressController->getDefaultCustomerAddressByCustomerID($customerID);
        $contactsArray = $customerContactsController->getContactsByCustomerID($customerID);
        $customerAddresses = $customerAddressController->getCustomerAddressesByCustomerID($customerID);
        
        //print_r($_GET);
        //print_r($_POST);

        $reportOverviewID;
        if (isset($_GET['reportID'])) {
            $reportOverviewID = $_GET['reportID'];
        }

        $tab = "";
        if (isset($_GET['tab'])) {
            $tab = $_GET['tab'];
        }
        if (isset($_GET['edit'])) {
            $edit = $_GET['edit'];
        }
        if (isset($_GET['save'])) {
            $save = $_GET['save'];
        }
        if (isset($_GET['move'])) {
            $move = $_GET['move'];
        }
        
        //print "TAB".$tab;

        // save details
        if (isset($save) && ($save === "saveTechnical") &&
                isset($_GET['productID']) && ($_GET['productID'] != "")) {

            $productID = $_GET['productID'];
            $oldProductRecord = $productController->getProductByID($productID);
            $saveProduct = $productController->getProductByID($productID);

            $versionLookupID = "";
            if (!isset($_POST['versionID']) || ($_POST['versionID'] == "")) {
                $seriesArray = $versionLookupController->getVersionIDBySeriesID($_POST['series']);
                $versionLookupID = reset($seriesArray)->versionID;
                //print "NO VERSION ID";
            } else {
                $versionLookupID = $_POST['versionID'];
                //print "VERSION ID";
            }

            $saveProduct->versionLookupID = $versionLookupID;
            $saveProduct->productSerialNumber = addslashes($_POST['serialNumber']);
            $saveProduct->productComment = addslashes($_POST['comment']);
            $saveProduct->productDrawingNumber = addslashes($_POST['drawingNumber']);
            $saveProduct->productDescription = addslashes($_POST['description']);

            $updateProductObject = $productController->updateProductObject($saveProduct);

            $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "update", "Installation Product Details Updated: ID " . $saveProduct->productID);

            $productHistory = new HistoryProduct("", $oldProductRecord->productID, $oldProductRecord->productSerialNumber, $oldProductRecord->productDescription, $oldProductRecord->productDrawingNumber, $oldProductRecord->productComment, $oldProductRecord->productEnquiryOnlyFlag, $oldProductRecord->customerID, $oldProductRecord->installationID, $oldProductRecord->versionLookupID, $oldProductRecord->productUnitName, $oldProductRecord->sourceAliasID, $_SESSION['user_id'], date("Y-m-d H:i:s", time()));

            $setProductHistory = $productHistoryController->setProductHistory($productHistory);
        }

        if (isset($save) && ($save === "saveTechnical") &&
                (!isset($_GET['productID']) || ($_GET['productID'] == ""))) {


            $versionLookupID = "";
            if (!isset($_POST['versionID']) || ($_POST['versionID'] == "")) {
                $seriesArray = $versionLookupController->getVersionIDBySeriesID($_POST['series']);
                $versionLookupID = reset($seriesArray)->versionID;
                //print "NO VERSION ID";
            } else {
                $versionLookupID = $_POST['versionID'];
                //print "VERSION ID";
            }

            $customerID = $customerHasInstallationsController->getCustomerIDFromInstallationID($_GET['installationID'])->customerID;

            $product = new Product("", addslashes($_POST['serialNumber']), addslashes($_POST['description']), addslashes($_POST['drawingNumber']), addslashes($_POST['comment']), 0, $customerID, $_GET['installationID'], $versionLookupID, "", "");



            $productController->setNewProduct($product);
        }
        
        // save changes to the installation
        if (isset($save) && ($save === "save") &&
                (isset($_GET['installationID']) || ($_GET['installationID'] != ""))) {
            
            // get original for history table
            $originalInstObject = $installationController->getInstallationByInstallationID($installationID);
            
            //print_r($originalInstObject);
            $editedInstObject = $installationController->getInstallationByInstallationID($installationID);
            
            
            
            
            $editedInstObject->installationName = addslashes($_POST['installationName']); // => LEE A. TREGURTHA
            $editedInstObject->installation = addslashes($_POST['installation']); // => Bulk Carriers 
            $editedInstObject->imo = addslashes($_POST['imo']); // => 5385625 
            $editedInstObject->type = addslashes($_POST['type']); // => MARINE 
            //$editedInstObject->operatingCompany = addslashes($_POST['operatingCompany']); // => 
            //$editedInstObject->technicalManager = addslashes($_POST['technicalManager']); // => 8 
            $editedInstObject->status = addslashes($_POST['status']); // => 5 
            $editedInstObject->originalSource = addslashes("INTERNAL CHANGE"); // => SeaWeb 
            $editedInstObject->originalSourceDate = date("Y-m-d H:i:s", time()); // => 2018 
            
            if (isset($_POST['archiveFlag'])) {
                $editedInstObject->archiveFlag = $_POST['archiveFlag']; // => 1 
            } else {
                $editedInstObject->archiveFlag = '0';
            }
            
            $editedInstObject->flagName = addslashes($_POST['flagName']);
            $editedInstObject->archiveReason = addslashes($_POST['archiveReason']); // => NO REASON 
            //$editedInstObject->hullNumber = addslashes($_POST['hullNumber']); // => w354 
            //$editedInstObject->hullType = addslashes($_POST['hullType']); // => 234 
            $editedInstObject->yardBuilt = addslashes($_POST['yardBuilt']); // => 4378 
            $editedInstObject->builtDate = addslashes($_POST['builtDate']."-01 12:00:00"); // => 1942-01 
            //$editedInstObject->lengthOverall = addslashes($_POST['lengthOverall']); // => 23423 
            //$editedInstObject->length = addslashes($_POST['length']); // => 234 
            //$editedInstObject->beamExtreme = addslashes($_POST['beamExtreme']); // => 2342432 
            //$editedInstObject->beamMoulded = addslashes($_POST['beamMoulded']); // => 234 
            //$editedInstObject->deadWeight = addslashes($_POST['deadweight']); // => 23423 
            //$editedInstObject->draught = addslashes($_POST['draught']); // => 234 
            //$editedInstObject->grossTonnage = addslashes($_POST['grossTonnage']); // => 234 
            $editedInstObject->buildYear = ""; //addslashes($_POST['buildYear']."-01-01 12:00:00"); // => 234 
            $editedInstObject->propellerType = addslashes($_POST['propellerType']); // => Controllable Pitch 
            $editedInstObject->propulsionUnitCount = addslashes($_POST['propulsionUnitCount']); // => 1 
            //$editedInstObject->teu = addslashes($_POST['teu']); // => 234 
            $editedInstObject->shipBuilder = addslashes($_POST['shipBuilder']); // => Bethlehem-Sparrows 
            $editedInstObject->classificationSociety = addslashes($_POST['classificationSociety']);
            $editedInstObject->shipTypeLevel4 = addslashes($_POST['shipTypeLevel4']);
            
            
            
            // history object
            $historyInstObject = new HistoryInstallation(
                    "", 
                    $originalInstObject->installationID, 
                    $originalInstObject->type, 
                    $originalInstObject->installation, 
                    $originalInstObject->installationName, 
                    $originalInstObject->imo, 
                    $originalInstObject->status, 
                    $originalInstObject->operatingCompany, 
                    $originalInstObject->technicalManager, 
                    $originalInstObject->originalSource, 
                    $originalInstObject->originalSourceDate, 
                    "", 
                    $originalInstObject->hullNumber, 
                    $originalInstObject->hullType, 
                    $originalInstObject->yardBuilt, 
                    $originalInstObject->deadWeight, 
                    $originalInstObject->lengthOverall, 
                    $originalInstObject->beamExtreme, 
                    $originalInstObject->beamMoulded, 
                    $originalInstObject->builtDate."-01 12:00:00", 
                    $originalInstObject->draught, 
                    $originalInstObject->grossTonnage, 
                    $originalInstObject->length, 
                    $originalInstObject->propellerType, 
                    $originalInstObject->propulsionUnitCount, 
                    $originalInstObject->shipBuilder, 
                    $originalInstObject->teu, 
                    "", 
                    $originalInstObject->archiveFlag, 
                    $originalInstObject->archiveReason, 
                    $originalInstObject->customerAddressID, 
                    $originalInstObject->flagName, 
                    $originalInstObject->leadShipByIMO, 
                    $originalInstObject->shipTypeLevel4,
                    $originalInstObject->classificationSociety,
                    $_SESSION['user_id'], 
                    date("Y-m-d H:i:s", time()));
                    
            //Make a new object with changes
            $insertHistoryObject = $installationHistoryController->setInstallationHistory($historyInstObject);
            $updateInstallationObject = $installationController->updateInstallationObject($editedInstObject);
            $installationObject = $installationController->getInstallationByInstallationID($_GET['installationID']);
            //Save both.
        }



        if (($tab == 'technical') && ($edit = 'edit')) {
            include("Views/Installations/instEditTechnicalData.php");
        } else if (($tab == 'installation') && (isset($edit) && ($edit == 'edit'))) {
            include("Views/Installations/instEditInstallation.php");
        } else {
            include("Views/Installations/instSingleInstallation.php");
        }
        break;

    case "showTechnicalHistory":
        include("Views/Installations/instTechnicalDataHistory.php");
        break;

    case "editParameters":
        // TODO dont forget history
        //$parameterHistory = $reportParametersHistoryController->setNewReportParameter($reportParameter);
        break;

    case "installationHistory":
        include('Views/Installations/instInstallationHistory.php');
        break;
    case "refineSearch":
        print "<h3>Refine Search</h3>";

        print "<br/>";
        if (isset($_GET['error']) && ($_GET['error'] == 'all')) {
            print "<h3 class='red'>You cannot select the entire database</h3>";
        }
        /**
         * Form for choosing the parameters for the query
         */
        print "<div id='leftColumn'>";
        include('Views/Reports/reportParametersForm.php');
        print "</div>";

        print "<div id='rightColumn'>";
        print "<h3>Current Parameters</h3>";
        print "<table style='border: 1px solid lightsteelblue; width:300px;'>";
        foreach ($_POST['parameterArray'] as $key => $array) {
            print "<tr><td style='vertical-align:top; width:150px;'>$key</td><td style='vertical-align:top; width:150px;'>";
            if (is_array($array)) {
                foreach ($array as $value) {
                    print $value . "<br/>";
                }
            } else {
                print $array;
            }
            print "</td></tr>";
            $count = 0;
            if (is_array($array)) {
                foreach ($array as $value) {
                    print "<input type='hidden' name='parameterArray[$key][$count]' value='$value'/>";
                    $count++;
                }
            } else {
                print "<input type='hidden' name='parameterArray[$key]' value='$array'/>";
            }
        }
        print "</table>";
        print "</div>";
        break;
    default:

        print "<br/>";
        if (isset($_GET['error']) && ($_GET['error'] == 'all')) {
            print "<h3 class='red'>You cannot select the entire database</h3>";
        }
        /**
         * Form for choosing the parameters for the query
         */
        print "<div id='leftColumn'>";
        include('Views/Reports/reportParametersForm.php');
        print "</div>";
        /**
         * Invoke the list of currently existing reports.
         */
        include_once('Controllers/ReportOverviewController.php');
        $reportOverviewController = new ReportOverviewController();


        if (($_SESSION['level'] == '1') || ($_SESSION['level'] == '2')) {
            $reports = $reportOverviewController->getAllReportOverviews();
        } else {
            $reports = $reportOverviewController->getAllReportOverviewsByUserID($_SESSION['user_id']);
        }
        print "<div id='rightColumn'>";
        include 'Views/Reports/existingReportsList.php';
        print "</div>";
        break;
}
    