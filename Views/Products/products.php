<script>
    
    $(function() {
        
        $("#designerSelect").change(function() {
            var designer = $("#designerSelect option:selected").text();
            
            
            $.ajax({url: 'Views/Products/getSelectedSeriesAjax.php?designerArray=' + designer + '',
                        success: function (output) {
                            //alert(output);
                            var outputString = "<option value=''>Select...</option>" + output;
                            $("#series").html(outputString);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.status + " " + thrownError);
                        }});
        });
        
        
        $("#series").change(function() {
            var series = $("#series option:selected").val();
            
            
            $.ajax({url: 'Views/Products/getVersions.php?series=' + series + '',
                        success: function (output) {
                            //alert(output);
                            $("#versionBody").html(output);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.status + " " + thrownError);
                        }});
        });
    });
    
</script>






<h3>Products</h3>


<h4>Manufacturer Names</h4>
<?php

    $designerList = $designerLookupController->getDistinctDesignersLeagueOnly();
    print "<select id='designerSelect' name='designerSelect'>";
    print "<option value=''>Select...</option>";
    if(isset($designerList)) {
        foreach ($designerList as $key => $designer) {
            print "<option value='$key'>".$designer->designerDescription."</option>";
        }
    }
    print "</select>";
    
    
    
    ?>

<select id="series" name='series'></select>


<table id="versionTbl">
    <thead>
    <tr>
        <th>Name</th>
        <th>Cycle #</th>
        <th>Configuration</th>
        <th>Used In</th>
    </tr>
    </thead>
    <tbody id="versionBody">
        <tr>
            <td colspan="4">NONE</td>
        </tr>
    </tbody>
</table>
    
    