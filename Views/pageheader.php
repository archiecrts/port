<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Cache-control" content="no-cache">

        <link rel="stylesheet" href="./css/layout.css"  type="text/css"/>
        <link rel="stylesheet" href="./css/topMenu.css"  type="text/css"/>
        <link rel="stylesheet" href="./css/subMenu.css"  type="text/css"/>
        <link rel="stylesheet" href="./css/main.css"  type="text/css"/>
        <link rel="stylesheet" href="./css/table.css"  type="text/css"/>
        <link rel="stylesheet" href="./css/installation.css" type="text/css"/>
       <!--  <link rel="stylesheet" href="./css/wideMenu.css"  type="text/css"/>-->
        
        <link rel="stylesheet" href="./css/divTable.css"  type="text/css"/>
        <link rel="stylesheet" href="./css/subMenuRoundButtons.css" type="text/css"/>
        <link rel="stylesheet" href="./css/accordion.css" type="text/css"/>
        
        <link rel="stylesheet" href="./css/campaignParametersSnippet.css" type="text/css"/>
        <link rel="stylesheet" href="./css/EnquiriesScreen.css" type="text/css" />
        <!-- DIALOG -->
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />
        <!--<link rel="stylesheet" href="./css/jquery-ui.css" />--><!--BACKUP SCRIPT FOR NO INTERNET -->
        <link rel="stylesheet" href="./css/dialog.css">  
        <!-- CSS/JS for DatePicker  -->
        <link rel="stylesheet" href="./css/jquery/jquery-ui.css">
        <link rel="stylesheet" href="./css/jquery/jquery-ui.min.css">
        <link rel="stylesheet" href="./css/jquery/jquery-ui.structure.css">
        <link rel="stylesheet" href="./css/jquery/jquery-ui.structure.min.css">
        <link rel="stylesheet" href="./css/jquery/jquery-ui.theme.css">
        <link rel="stylesheet" href="./css/jquery/jquery-ui.theme.min.css">
        <link rel="stylesheet" href="./css/jquery/jquery.qtip.css" /> 
        
        <link href="icons/favicon.ico" rel="icon" type="image/x-icon" />
        
        <!--<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>-->
        <script src="./js/jquery-1.11.1.minBACKUP.js"></script><!--BACKUP SCRIPT FOR NO INTERNET -->
        <!--<script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js">--></script>
        <script src="./js/jquery-ui-1.11.1.minBACKUP.js"></script><!--BACKUP SCRIPT FOR NO INTERNET -->
        <!-- DATEPICKER -->
        <script src="./js/1.11.4-jquery-ui.js"></script>
        <!-- POPUP -->
        <script type="text/javascript" src="./js/jquery.qtip.js"></script>
        
        <title>
            <?php 
            
            if (basename(getcwd()) == "DRYDOCK") {
                // Release Candidate Version
                echo "DRYDOCK";
            }

            if (basename(getcwd()) == "PORT") {
                // Production Version
                echo "PORT";
            }

            if (basename(getcwd()) == "port") {
                // Development Version
                echo "DEVELOPMENT";
            }
            ?>
        </title>
    </head>
    
    <body>
        
        <?php
        include("Menus/topBarTitleWelcome.php");
        include("History/destroySessions.php"); // If the session is more than 15 hours old it should be destroyed.
        include("Helpers/debugWriter.php"); // pass in the parameters to fwrite.        
