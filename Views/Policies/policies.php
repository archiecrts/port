<h3>Policies</h3>
<script>
    
    function removeDocument(docID, userID) {
        var server = '<?php print $_SESSION['server']; ?>';
        $.ajax({url: 'Views/Policies/removeDocument.php?file=' + docID + '&action=1&userID=' + userID + '&server=' + server,
                success: function (output) {
                    location.href = 'index.php?portal=intranet&page=policies';
    
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + " " + thrownError);
                }
        });
    }
    
    
    function confirmReadDocument(cb, userID) {
        var server = '<?php print $_SESSION['server']; ?>';
        if (cb.checked === true) {
            $.ajax({url: 'Views/Policies/confirmReadDocument.php?file=' + cb.value + '&action=1&userID=' + userID + '&server=' + server,
                success: function (output) {
                    //alert(output);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + " " + thrownError);
                }
            });
        } else {
            
            $.ajax({url: 'Views/Policies/confirmReadDocument.php?file=' + cb.value + '&action=0&userID=' + userID + '&server=' + server,
                success: function (output) {
                    //alert(output);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + " " + thrownError);
                }
            });
        }
    }
</script>
<?php
if (($_SESSION['level'] == '2') || ($_SESSION['level'] == '1')) {
    print '<p><a href="index.php?portal=intranet&page=upload&destination=policies">Add More Files</a></p>';
}




$fileArray = $documentsController->getPolicyDocuments();

if (isset($fileArray)) {
    print '<table>';
    print '<tr><td></td><th>Read By</th><th>Mark As Read</th>';
    if (($_SESSION['level'] == '2') || ($_SESSION['level'] == '1')) {
        print '<th>Unread By Staff</th><th>Remove</th>';
    }
    print '</tr>';
    
    foreach ($fileArray as $docObject) {

            print '<tr>';
            print '<td><a href="'. $docObject->url . '" target="blank">';
            $explodeUrl = explode("/", $docObject->url);
            print $explodeUrl[2];
            print '</a></td>';
            
            
           

            print '<td '.(strtotime($docObject->readByDate) > time() ? 'class="blue"' : 'class="red"').'>' . date("d M Y", strtotime($docObject->readByDate)) . '</td>';
            
            $trainingRecord = $trainingController->getRecordByUserIDandDocID($_SESSION['user_id'], $docObject->documentID);
            $checked = "";
            if ($trainingRecord->hasBeenReadFlag == true) {
                $checked = "checked";
            }
            print '<td align="center"><input type="checkbox" name="confirmGroup" value="' . $docObject->url . '" onchange="confirmReadDocument(this, '.$_SESSION['user_id'].')" '.$checked.' /></td>';
            
            // for admins and HR. If a policy has not been read by everyone registered on PORT a report can
            // be generated to show those users.
            if (($_SESSION['level'] == '2') || ($_SESSION['level'] == '1')) {
                $report = $trainingController->getReadReceiptsByDocumentID($docObject->documentID);
                print '<td align="center">';
                if($report != false) {
                    print '<a href="index.php?portal=intranet&page=policyReport&document='.$docObject->documentID.'&documentName='.$explodeUrl[2].'">Report</a>';
                }
                print '</td>';
                print '<td class="removeDoc"><button class="removeDocButton" onclick="removeDocument('.$docObject->documentID.', '.$_SESSION['user_id'].');"><img src="./icons/removeArticle.png" height="15px" border="0"  style="vertical-align: middle;"/></button></td>';
            }
            
            print '</tr>';

    }
    print '</table>';
} else {
    print "<p>No Files Available</p>";
}