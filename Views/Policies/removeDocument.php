<?php

/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
if (isset($_SESSION['server'])) {
    $server = $_SESSION['server'];
} else if (isset($_GET['server'])) {
    $server = $_GET['server'];
}
include_once("../../Models/Database.php");


$file = $_GET['file'];
$action = $_GET['action'];
$userID = $_GET['userID'];



$result = "UPDATE documents_tbl SET doc_archived = '1' "
        . "WHERE doc_id = '".$file."'";

//$row = mysqli_fetch_assoc($result);
if (!mysqli_query(Database::$connection, $result)) {
    echo "Failed: ".  mysqli_error(Database::$connection);
} else {
    $history = mysqli_query(Database::$connection, "INSERT INTO user_history_tbl "
                . "(uht_item_date, uht_item_type, "
                . "uht_comment, user_tbl_usr_id) "
                . "VALUES ('".date("Y-m-d H:i:s", time())."', 'delete', 'Document ".$file." Archived', '".$userID."')");
    echo $file;
}
    
