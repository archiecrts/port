<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

include_once('Helpers/PHPMailer/PHPMailerAutoload.php');


// EMAIL VIA POP3
$mail = new PHPMailer;

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'mail.uk2.net';                         // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'acurtis@simplexturbulo.es';        // SMTP username
$mail->Password = 'GreatThing22';                     // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                                    // TCP port to connect to

$mail->From = 'acurtis@dieselmarinegroup.com';
$mail->FromName = 'PORT : S-T Diesel Marine Group';


$report = $trainingController->getReadReceiptsByDocumentID($_GET['document']);

foreach ($report as $training => $trainingObject) {
    $user = $userController->getUserByID($trainingObject->userID);
    $mail->addAddress($user->email, $user->name);     // Add a recipient
    
}
$documentName = $_GET['documentName'];

$mail->addReplyTo('acurtis@dieselmarinegroup.com', 'Information');
//$mail->addCC('cc@example.com');
//$mail->addBCC('bcc@example.com'); -> send a copy to Kate
//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = 'Read Policy Reminder';
$mail->Body = "<p>The deadline to read the policy document entitled <strong>".$documentName."</strong> has expired.</p>"
        . "Please log into your PORT account and read this and any other outstanding documents "
        . "in the Policies section and mark them as read.<br/>"
        . "Yours Sincerely - HR";
$mail->AltBody = "The deadline to read the policy document entitled ".$documentName." has expired. \r\n"
        . "Please log into your PORT account and read this and any other outstanding documents "
        . "in the Policies section and mark them as read.\r\n"
        . "Yours Sincerely - HR";

if (!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo 'Message has been sent';
}
            