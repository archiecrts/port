<?php

/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

print "<h3>Outstanding Reader Report : ".$_GET['documentName']."</h3>";
$document = $_GET['document'];

$report = $trainingController->getReadReceiptsByDocumentID($document);
print "<table>";
foreach ($report as $training => $trainingObject) {
    $user = $userController->getUserByID($trainingObject->userID);
    print "<tr><td>".$user->name."</td></tr>";
    
}
print "</table>";

print "<p><a href='index.php?portal=intranet&page=emailReadReport&document=".$document."&documentName=".$_GET['documentName']."'>Email a reminder?</a></p>";