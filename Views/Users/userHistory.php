<table class='borderedTable'>
    <tr>
        <th class="rounded-corners-left-top"></th>
        <th>Login</th>
        <th>User Name</th>
        <th>Level</th>
        <th>Department</th>
        <th>Email</th>
        <th>Initial Login</th>
        <th>Retired</th>
        <th>Changed By</th>
        <th class="rounded-corners-right-top">Date Changed</th>
    </tr>
    <?php
    if (isset($currentRecord)) {
        print "<tr class='green'>";
        print "<td>Current</td>";
            foreach ($currentRecord as $key => $value) {
                if (($key != 'id') && ($key != 'userID') && ($key != 'password')) {
                    if ($key == 'level') {
                        print "<td>".$userLevelController->getUserLevelByID($value)->userLevel."</td>";
                    }else if ($key == 'department') {
                        print "<td>".$userDepartmentController->getUserDepartmentByID($value)->userDepartment."</td>";
                    }else if ($key == 'changedByUserID') {
                        print "<td>".$userController->getUserByID($value)->name."</td>";
                    }else if ($key == 'retire') {
                        if ($value == 0) {
                            $retire = 'No';
                        } else {
                            $retire = 'Yes';
                        }
                        print "<td>".$retire."</td>";
                    }else if ($key == 'initialLogin') {
                        if ($value == 0) {
                            $initialLogin = 'No';
                        } else {
                            $initialLogin = 'Yes';
                        }
                        print "<td>".$initialLogin."</td>";
                    } else {
                        print "<td>$value</td>";
                    }
                }
            }
            print "<td></td><td></td>";
            print "</tr>";
    }
    if (isset($history)) {
        foreach ($history as $key => $userHistory) {
            print "<tr>";
            print "<td>Changed From</td>";
            foreach ($userHistory as $key => $value) {
                if (($key != 'id') && ($key != 'userID') && ($key != 'password')) {
                    if ($key == 'level') {
                        print "<td>".$userLevelController->getUserLevelByID($value)->userLevel."</td>";
                    }else if ($key == 'department') {
                        print "<td>".$userDepartmentController->getUserDepartmentByID($value)->userDepartment."</td>";
                    }else if ($key == 'changedByUserID') {
                        print "<td>".$userController->getUserByID($value)->name."</td>";
                    }else if ($key == 'retire') {
                        if ($value == 0) {
                            $retire = 'No';
                        } else {
                            $retire = 'Yes';
                        }
                        print "<td>".$retire."</td>";
                    }else if ($key == 'initialLogin') {
                        if ($value == 0) {
                            $initialLogin = 'No';
                        } else {
                            $initialLogin = 'Yes';
                        }
                        print "<td>".$initialLogin."</td>";
                    } else {
                        print "<td>$value</td>";
                    }
                }
            }
            print "</tr>";
        }
    }
    ?>
</table>