<table class='borderedTable'>
    <tr>
        <th class="rounded-corners-left-top">Name</th>
        <th>Username</th>
        <th>Email</th>
        <th>Level</th>
        <th>Department</th>
        <th>Has logged in</th>
        <th>Territories</th>
        <th>Retired</th>
        <th>Limit to territory</th>
        <th class="rounded-corners-right-top">History</th>
    </tr>
    <?php
    $allUsersList = $userController->getUsers();
    
    // display all users
    foreach($allUsersList as $key => $userObject)
    {
        print "<tr>";
        print "<td><a href='index.php?portal=users&action=edit&id=".$userObject->id."'>".$userObject->name."</a></td>";
        print "<td>".$userObject->login."</td>";
        print "<td>".$userObject->email."</td>";
        print "<td>".$userLevelController->getUserLevelByID($userObject->level)->userLevel."</td>";
        print "<td>".$userDepartmentController->getUserDepartmentByID($userObject->department)->userDepartment."</td>";
        print "<td>";
        
        if ($userObject->initialLogin == 1)
        {
            print "No";
        } else {
            print "Yes";
        }
        print "</td>";
        if ($userObject->level == "3" && $userObject->retire == '0')
        {
            print "<td><a href='index.php?portal=users&action=addTerritories&id=".$userObject->id."'>Add/Edit Territories</a></td>";
        } else if ($userObject->level == "3" && $userObject->retire == '1')
        {
            print "<td>NOT AVAILABLE</td>";
        } else {
            print "<td></td>";
        }
        
        print "<td>";
        
        if ($userObject->retire == 0)
        {
            print "No";
        } else {
            print "Yes";
        }
                
        print "</td>";
        print "<td>";
        
        if ($userObject->limitToTerritory == 0)
        {
            print "No";
        } else {
            print "Yes";
        }
                
        print "</td>";
        
        print "<td><a href='index.php?portal=users&action=history&id=".$userObject->id."'>View</a></td>";
        
        print "</tr>";
    }
    ?>
</table>