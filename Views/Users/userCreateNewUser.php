<script type="text/javascript" src="js/validateAddUserForm.js"></script>
<form name="createUserForm" onsubmit="return validateAddUser();" action="index.php?portal=users&page=users&action=create" method="post" enctype="multipart/form-data">
    <p id="error"></p>
    <table>
        <tr>
            <td>Display Name</td>
            <td><input type="text" name="displayName" id="displayName" required/></td>
        </tr>
        <tr>
            <td>Login Username</td>
            <td><input type="text" name="login" id="login" required/></td>
        </tr>
        <tr>
            <td>Level</td>
            <td><select name="level" required>

                    <?php
                    $levelArray = $userLevelController->getUserLevels();
                    foreach ($levelArray as $id => $level) {
                        print "<option value='" . $level->id . "'>" . $level->userLevel . "</option>";
                    }
                    ?>

                </select></td>
        </tr>
        <tr>
            <td>Department</td>
            <td><select name="department" required>
                    <?php
                    $deptArray = $userDepartmentController->getUserDepartments();
                    foreach ($deptArray as $id => $dept) {
                        print "<option value='" . $dept->id . "'>" . $dept->userDepartment . "</option>";
                    }
                    ?>
                    
                </select></td>
        </tr>
        <tr>
            <td>Email Address</td>
            <td><input type="text" name="email" id="email" required/></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" name="submit" id="submit" value="Create New User"/></td>
        </tr>
    </table>
    <p>Passwords are auto generated and sent to the user who may change it on initial login.</p>
</form>