<?php
include_once('Controllers/UserController.php');
include_once('Controllers/UserLevelController.php');
include_once('Controllers/UserDepartmentController.php');
include_once('Controllers/UserTerritoryController.php');
include_once('Controllers/ReportUserDisplaySettingsController.php');
include_once("Controllers/UserHistoryController.php");
include_once 'Models/Entities/User.php';
include_once 'ControllersHistory/UserHistoryDBController.php';
include_once "Controllers/UserDepartmentController.php";
$userDepartmentController = new UserDepartmentController();
$userHistoryController = new UserHistoryController();
$userController = new UserController();
$userLevelController = new UserLevelController();
$userDepartmentController = new UserDepartmentController();
$userTerritoryController = new UserTerritoryController();
$reportUserDisplaySettingsController = new ReportUserDisplaySettingsController();
// controller for the history DB
$historyDBUserHistoryController = new UserHistoryDBController();



if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        case "create":

            include_once('Helpers/PHPMailer/PHPMailerAutoload.php');

            $name = $_POST['displayName'];
            $level = $_POST['level'];
            $login = $_POST['login'];
            $email = $_POST['email'];
            $department = $_POST['department'];
            $password = substr(md5($_POST['email']), 0, 8);
            $flag = 1;
            // check if user exists before creating a new one.
            $checkUserExists = $userController->getUser($name);


            if ($checkUserExists == false) {
                $newUser = $userController->setNewUser($login, $password, $name, $level, $department, $email, $flag, 0);

                // Log in the DB
                $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "create", "New user account added : ID " . $newUser . " Name : " . $name);

                // create user report settings.

                
                $settingsArray = $reportUserDisplaySettingsController->getAllSettings();
                foreach ($settingsArray as $key => $value) {
                    $settingsArray[$key] = 1;
                    
                }

                $saveSettings = $reportUserDisplaySettingsController->setSettingsForUser($settingsArray, $newUser);

                // Send email.
                print "<span class='blue'>New User Created</span> "; //.$newUser (last insert id)
                // EMAIL VIA POP3
                $mail = new PHPMailer;

                //$mail->SMTPDebug = 3;                               // Enable verbose debug output

                $mail->isSMTP();                                      // Set mailer to use SMTP
                $mail->Host = 'mail.uk2.net';                         // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                $mail->Username = 'port@simplexconverting.com';        // SMTP username
                $mail->Password = 'GreatThing22';                     // SMTP password
                $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                $mail->Port = 587;                                    // TCP port to connect to

                $mail->From = 'acurtis@dieselmarinegroup.com';
                $mail->FromName = 'PORT : S-T Diesel Marine Group';
                $mail->addAddress($email, $name);     // Add a recipient
                $mail->addReplyTo('acurtis@dieselmarinegroup.com', 'Information');
                //$mail->addCC('cc@example.com');
                //$mail->addBCC('bcc@example.com');
                //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
                //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
                $mail->isHTML(true);                                  // Set email format to HTML

                $mail->Subject = 'Account Setup';
                $mail->Body = "<p>Your account on PORT has been created.</p>"
                        . "Your username is " . $login . " <br/>"
                        . "Your password is " . $password . " <br/>"
                        . "Website is currently hosted at https://www.dieselmarinegroup.com/PORT/index.php<br/>"
                        . "You may change this by logging into your account. <br/>"
                        . "Yours Sincerely - Webmaster";
                $mail->AltBody = "Your account on POST has been created \r\n"
                        . "Your username is " . $login . " \r\n"
                        . "Your password is " . $password . "\r\n"
                        . "Website is currently hosted at https://www.dieselmarinegroup.com/PORT/index.php\r\n"
                        . "You may change this by logging into your account.\r\n"
                        . "Yours Sincerely - Webmaster";

                if (!$mail->send()) {
                    echo 'Message could not be sent.';
                    echo 'Mailer Error: ' . $mail->ErrorInfo;
                } else {
                    echo 'Message has been sent';
                }
            }
            ?>

            <h3>Create a new User</h3>
            <?php
            include('Views/Users/userCreateNewUser.php');
            ?>
            <hr/>
            <p>Display Users</p>
            <?php
            include('Views/Users/userDisplayUsers.php');



            break;

        case "addTerritories":
            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $user = $userController->getUserByID($id);

                if (isset($_POST['addTerritory'])) {
                    // TODO: make sure this doesnt add multiple discrete entries.
                    $territory = new UserTerritory(0, $user->id, $_POST['territory']);
                    $setTerritory = $userTerritoryController->setTerritory($territory);

                    // Log in the DB
                    $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "create", "New Territories added to user : ID " . $user->id . " Territory: " . $_POST['territory']);
                }
            }



            include("Views/Users/userAddTerritories.php");
            break;
        case "deleteTerritory":
            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $user = $userController->getUserByID($id);

                if (isset($_POST['territoryToDelete'])) {

                    $territoryID = $_POST['territoryToDelete'];
                    $deleteTerritory = $userTerritoryController->deleteTerritory($territoryID);

                    // Log in the DB
                    $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "delete", "Territories deleted from user : ID " . $id . " Territory: " . $_POST['territoryToDelete']);
                }
            }



            include("Views/Users/userAddTerritories.php");
            break;

        case "edit":
            if (isset($_GET['user'])) {
                

                if (isset($_POST['retire'])) {
                    $retire = 1;
                } else {
                    $retire = 0 ;
                }
                if (isset($_POST['limitToTerritory'])) {
                    $limit = 1;
                } else {
                    $limit = 0 ;
                }
                $user = new User($_GET['user'], 
                        $_POST['username'], 
                        $_POST['password'], 
                        $_POST['displayName'], 
                        $_POST['level'], 
                        $_POST['department'], 
                        $_POST['email'], 
                        $_POST['initialLogin'], 
                        $retire, $limit);
            
                $oldUserRecord = $userController->getUserByID($_GET['user']);
                $oldUserHistoryRecord = new HistoryUserHistory(
                        "", 
                        $oldUserRecord->id, 
                        $oldUserRecord->login, 
                        $oldUserRecord->password, 
                        $oldUserRecord->name, 
                        $oldUserRecord->level, 
                        $oldUserRecord->department, 
                        $oldUserRecord->email, 
                        $oldUserRecord->initialLogin, 
                        $oldUserRecord->retire, 
                        $_SESSION['user_id'], 
                        date("Y-m-d H:i:s", time()));
                
                $update = $userController->updateUser($user);
                // Log in the DB
                $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "update", "User Updated : ID " . $_GET['user'] . " ". $_POST['displayName'] );
                $addHistoryDBEntry = $historyDBUserHistoryController->setNewUserHistory($oldUserHistoryRecord);
                // redirect
               print '<meta http-equiv="refresh" content="0;url=index.php?portal=users" />';
   
            } else if (isset($_GET['id'])) {
                $user = $userController->getUserByID($_GET['id']);
            }
            
            
            print "<h3>Edit User</h3>";
            include('Views/Users/userEditUserAccount.php');
           
            
            break;
            
        case "history":
            print "<h3>User History</h3>";
            $userID = '';
            if (isset($_GET['id'])) {
                $userID = $_GET['id'];
            }    
            $currentRecord = $userController->getUserByID($userID);
            $history = $historyDBUserHistoryController->getUserHistoryByID($userID);
            include 'Views/Users/userHistory.php';
            break;
        case "dept":
            include "Views/Users/addNewDepartment.php";
            break;
        case "addDept":
            
            if (isset($_POST['submit'])) {
                
                if (isset($_POST['department']) && ($_POST['department']) != '') {
                    
                    $record = $userDepartmentController->insertUserDepartment($_POST['department']);

                    if ($record != null) {
                        print $_POST['department']. " has been added<br/>";
                    }
                }
            }

            include "Views/Users/addNewDepartment.php";
            break;
        default:
            ?>
            <h3>Create a new User</h3>
            <?php
            include('Views/Users/userCreateNewUser.php');
            ?>
            <hr/>
            <p>Display Users</p>
            <?php
            include('Views/Users/userDisplayUsers.php');

            break;
    }
} else {
    ?>
    <h3>Create a new User</h3>
    <?php
    include('Views/Users/userCreateNewUser.php');
    ?>
    <hr/>
    <p>Display Users</p>
    <?php
    include('Views/Users/userDisplayUsers.php');
}
