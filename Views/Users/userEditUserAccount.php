<script type="text/javascript" src="js/validateEditUserForm.js"></script>
<form name="editUserForm" onsubmit="return validateEditUser();"  action="index.php?portal=users&action=edit&user=<?php echo $_GET['id']; ?>" method="post" enctype="multipart/form-data">


    <p id="error"></p>
    <input type="hidden" name="initialLogin" value="<?php echo $user->initialLogin; ?>" required/>
    <table>
        <tr>
            <td>Display Name</td>
            <td><input type="text" name="displayName" id="displayName" value="<?php echo $user->name; ?>" required/></td>
        </tr>
        <tr>
            <td>Username</td>
            <td><input type="text" name="username" id="login" value="<?php echo $user->login; ?>" required/></td>
        </tr>
        <tr>
            <td>Password</td>
            <td><input type="password" name="password" id="login" value=""/> <span class="red">LEAVE BLANK IF NOT CHANGED</span></td>
        </tr>
        <tr>
            <td>Confirm Password</td>
            <td><input type="password" name="confirmPassword" id="login" value=""/> <span class="red">LEAVE BLANK IF NOT CHANGED</span></td>
        </tr>
        <tr>
            <td>Level</td>
            <td><select name="level" required>
                    <?php
                    $levelArray = $userLevelController->getUserLevels();
                    foreach ($levelArray as $id => $level) {
                        if ($user->level == $id){
                            $selected = "selected='selected'";
                        } else {
                            $selected = "";
                        }
                        print "<option value='" . $level->id . "' $selected>" . $level->userLevel . "</option>";
                    }
                    ?>
                </select></td>
        </tr>
        <tr>
            <td>Department</td>
            <td><select name="department" required>
                    <?php
                    $deptArray = $userDepartmentController->getUserDepartments();
                    foreach ($deptArray as $key => $dept) {
                        if ($user->department == $dept->id){
                            $selectedDept = "selected='selected'";
                        } else {
                            $selectedDept = "";
                        }
                        print "<option value='" . $dept->id . "' $selectedDept>" . $dept->userDepartment . "</option>";
                    }
                    ?>
                </select></td>
        </tr>
        <tr>
            <td>Email Address</td>
            <td><input type="text" name="email" id="email" value="<?php echo $user->email; ?>" required/></td>
        </tr>
        <tr>
            <td>Retired / Quit</td>
            <?php
            if (isset($user->retire) && ($user->retire) == '1') {
                $selected = "checked='checked'";
            } else {
                $selected = '';
            }
            ?>
            <td><input type="checkbox" name="retire" id="retire" value="1" <?php echo $selected; ?>/></td>
        </tr>
        <tr>
            <td>Limit to territory</td>
            <?php
            if (isset($user->limitToTerritory) && ($user->limitToTerritory) == '1') {
                $selected = "checked='checked'";
            } else {
                $selected = '';
            }
            ?>
            <td><input type="checkbox" name="limitToTerritory" id="limitToTerritory" value="1" <?php echo $selected; ?>/></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" name="submit" id="submit" value="Update User"/></td>
        </tr>

</form>