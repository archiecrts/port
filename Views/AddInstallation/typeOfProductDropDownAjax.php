<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("../../Models/Database.php");

$typeOfProductID = $_GET['tvalue'];

$result = mysqli_query(Database::$connection, "SELECT * FROM designer_lookup_tbl WHERE dlt_id IN 
                                (SELECT designer_lookup_tbl_dlt_id FROM series_lookup_tbl 
                                WHERE type_of_product_lookup_tbl_toplt_id = '" . $typeOfProductID . "')");
echo '<option value="">Please select...</option>';
while ($row = mysqli_fetch_assoc($result)) {
    
    echo '<option value="' . $row['dlt_id'] . '" >' . $row['dlt_designer_description'] . "</option>";
}

mysqli_free_result($result);

