<script>
    $(document).ready(function ($) {

        var land = '<option value="1">Operational (LAND)</option><option value="2">Planned (LAND)</option><option value="3">Under Construction (LAND)</option><option value="4">Stationary (LAND)</option>';
        
        var marine = '<option value="5">In Service/Commission (MARINE)</option><option value="5">Laid Up (MARINE)</option><option value="7">Converting/Rebuilding (MARINE)</option><option value="8">In Casualty or Repairing (MARINE)</option><option value="9">Not Operating (MARINE)';    
        
        var initial_target_html = '<option value="">Select a type...</option>';
        
        $('#status').html(initial_target_html);

        $("#type").change(function () {
            var selectvalue = $(this).val();

            $('#status').html('<option value="">Loading...</option>');

            if (selectvalue == "") {
                //Display initial prompt in target select if blank value selected
                $('#status').html(initial_target_html);
            } else if (selectvalue == "MARINE") {
                $('#status').html(marine);
                document.getElementById('marineBlock').style.display = "block";
                $('#imo').removeAttr('disabled');
            } else if (selectvalue == "LAND") {
                $('#status').html(land);
                $('#marineBlock').find('input:text').val('');
                document.getElementById('marineBlock').style.display = "none";
                $('#imo').attr('disabled','disabled');
            }

        });
    });
</script>
<?php
include_once('Controllers/CountryController.php');
$countryController = new CountryController();
require_once 'Helpers/charsetFunctions.php';
?>
<p id="error"></p>
<form name="instForm" action="index.php?portal=mr&page=addNewInstallation&step=engines" method="post" enctype="multipart/form-data">
    <input type="hidden" name="origin" value="addInstallation"/>

    <?php
    if (isset($_GET['customerID'])) {
        print "<h3>".$customerController->getCustomerByID($_GET['customerID'])->customerName."</h3>";
        print '<input type="hidden" id="customerID" name="customerID" value="'.$_GET['customerID'].'"/>';
        $customerAddresses = $customerAddressController->getCustomerAddressesByCustomerID($_GET['customerID']);
        print '<strong>Choose Office Address</strong> <select name="customerAddressID">';
        foreach ($customerAddresses as $id => $addr) {
            print "<option value='$id'>".$addr->officeName." - ".$addr->country."</option>";
        }
        print '</select>';
    } else {
        include './Views/AutoCompleteSearch/autoCompleteSearchNoPost.php';
    }
    ?>
    <table>
        <tr>
            <td class="title">Location Type</td>
            <td><select name="type" id="type" >
                    <option value=""></option>
                    <option value="MARINE">Marine</option>
                    <option value="LAND">Land</option>
                </select></td>
            <td class="title">Installation Type</td>
            <td><input type="text" name="installation" value="" /></td>
        </tr>
        <tr>
            <td class="title">Installation / Trader Name <span class="red">*</span><br/>
                <span class="red">(Enter 'Unknown' if name is not known)</span></td>
            <td><input type="text" name="installationName" value="" required /></td>
            <td class="title">IMO Ship Number</td>
            <td><input type="text" id="imo" name="imo" value=""  /></td>
        </tr>

        <tr>
            <td class="title">Source of Information</td>
            <td><select name="source" >
                    <option value=""></option>
                    <option value="Web">Web</option>
                    <option value="Email">Email</option>
                    <option value="Magazine">Magazine</option>
                    <option value="Platts">Platts</option>
                    <option value="IIR">IIR</option>
                    <option value="SeaWeb">SeaWeb</option>
                    <option value="Word of Mouth">Word of Mouth</option>
                </select>
            </td>
            <td class="title">Status</td>
            <td><select name="status" id="status">
                    <!-- This gets filled in by jquery above -->
                </select></td>
        </tr>
        <tr>
            <td class="title">Source Date</td>
            <td><input type="text" name="originalSourceDate" placeholder="Year (YYYY)" pattern="[0-9]{4}"/></td>
        </tr>
        
        <tr>
            <td class="title">Owner Parent</td>
            <td><select name="ownerParent">
                    <option value="">None Selected</option>
                <?php
                $operatorArray = $customerController->getAllCustomers();
                foreach ($operatorArray as $id => $customer) {
                    $selected = "";
                    if (isset($_GET['customerID'])) {
                        if (isset($installation)) {
                            if ($installation->operatingCompany == $_GET['customerID']) {
                                $selected = "selected='selected'";
                            }
                        } else  {
                            if ($id == $_GET['customerID']) {
                                $selected = "selected='selected'";
                            }
                        }
                    }
                    print "<option value='$id' $selected>$customer->customerName</option>";
                }
                ?>
                </select></td>
        </tr>
        <tr>
            <td class="title">Technical Manager</td>
            <td><select name="technicalManager"/>
                 <option value="">None Selected</option>
                <?php
                $technicalArray = $customerController->getAllCustomers();
                foreach ($technicalArray as $id => $customer) {
                    
                    $selected = "";
                    if (isset($_GET['customerID'])) {
                        if (isset($installation)) {
                            if ($installation->technicalManager == $_GET['customerID']) {
                                $selected = "selected='selected'";
                            }
                        } else  {
                            if ($id == $_GET['customerID']) {
                                $selected = "selected='selected'";
                            }
                        }
                    }
                    print "<option value='$id' $selected>$customer->customerName</option>";
                }
                ?>
                </select></td>
        </tr>
    </table>
    
    <!-- Marine Block -->
    <div id="marineBlock" style="display: none;">
        <table>
            <tr>
        <td class="title">Hull Number</td>
        <td><input type="text" name="hullNumber" value=""/></td>
        <td class="title">Hull Type</td>
        <td><input type="text" name="hullType" value=""/></td>
    </tr>
    <tr>
        <td class="title">Yard Built</td>
        <td><input type="text" name="yardBuilt" value=""/></td>
        <td class="title"></td>
        <td></td>
    </tr>
    <tr>
        <td class="title">Length Overall</td>
        <td><input type="text" name="lengthOverall" value=""/> (m)</td>
        <td class="title">Length</td>
        <td><input type="text" name="length" value=""/> (m)</td>
    </tr>
    <tr>
        <td class="title">Beam Extreme</td>
        <td><input type="text" name="beamExtreme" value=""/> (m)</td>
        <td class="title">Beam Moulded</td>
        <td><input type="text" name="beamMoulded" value=""/> (m)</td>
    </tr>
    <tr>
        <td class="title">Deadweight</td>
        <td><input type="text" name="deadweight" value=""/> (t)</td>
        <td class="title">Draught</td>
        <td><input type="text" name="draught" value=""/> (m)</td>
    </tr>
    <tr>
        <td class="title">Gross Tonnage</td>
        <td><input type="text" name="grossTonnage" value=""/> (t)</td>
        <td class="title">Build Date</td>
        <td><input type="text" name="builtDate" value="" placeholder="YYYY-MM" pattern="[0-9]{4}-[0-9]{2}"/></td>
    </tr>
    <tr>
        <td class="title">Propeller Type</td>
        <td><input type="text" name="propellerType" value=""/></td>
        <td class="title">Propulsion Unit Count</td>
        <td><input type="text" name="propulsionUnitCount" value=""/></td>
    </tr>
    <tr>
        <td class="title">TEU</td>
        <td><input type="text" name="teu" value=""/></td>
        <td class="title">Ship Builder</td>
        <td><input type="text" name="shipBuilder" value=""/></td>
    </tr>
        </table>
    </div>
    <table>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td style="text-align: right;"><input type="submit" name="submit" value="Save and Continue"></td>
        </tr>
    </table>
</form>       