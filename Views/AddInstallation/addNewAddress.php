<h3>Add new address</h3>
<form name="instForm" action="index.php?portal=mr&page=addNewInstallation&step=address&action=saveAddress" method="post" enctype="multipart/form-data">
 
    <table>
        <tr>
            <td>Office Name (e.g. Main Office / Dispatch Office)</td>
            <td><input type='text' name="officeName"/></td>
        </tr>
        <tr>
            <td>Address</td>
            <td><input type='text' name="address"/></td>
        </tr>
        <tr>
            <td>City</td>
            <td><input type='text' name="city"/></td>
        </tr>
        <tr>
            <td>State</td>
            <td><input type='text' name="state"/></td>
        </tr>
        <tr>
            <td>Country</td>
            <td><select name="country">
                <?php
                    $countryArray = $countryController->getCountries();

                    foreach ($countryArray as $countryName => $cname) {
                        
                        if ($cname != "") {
                            
                            echo '<option value="' . $cname->countryName . '">' . $cname->countryName . '</option>';
                        }
                    }
                ?>    
                </select></td>
        </tr>
        <tr>
            <td>Postcode / ZIP</td>
            <td><input type='text' name="postcode"/></td>
        </tr>
        <tr>
            <td>Address Verified?</td>
            <td><input type='checkbox' name="verified" value='1'/></td>
        </tr>
        <tr>
            <td>Default Address?</td>
            <td><input type='checkbox' name="default" value="1"/></td>
        </tr>
        <tr>
            <td><input type='submit' name="addAddress" value="Add and Continue to Contacts"/></td>
            <td><input type='submit' name="addAddressPlus" value="Continue Adding Addresses"/></td>
        </tr>
    </table>
    <input type="hidden" name="customerID" value="<?php if (isset($customerID)) { echo $customerID; } else { echo $_POST['customerID']; } ?>"/>
</form>