<h3>Add a New Record</h3>
<script src="js/validateAddNewInstallation.js" type="text/javascript"></script>

<?php
include_once 'Controllers/InstallationController.php';
include_once 'Controllers/CustomerController.php';
include_once 'Controllers/CustomerContactsController.php';
include_once 'Controllers/DesignerLookupController.php';
include_once 'Controllers/EngineModelController.php';
include_once 'Controllers/SeriesLookupController.php';
include_once 'Controllers/UserHistoryController.php';
include_once 'Controllers/InstallationEngineController.php';
include_once 'Controllers/LoadPriorityController.php';
include_once 'Controllers/CustomerStatusLookupController.php';
include_once 'Controllers/CustomerStatusController.php';
include_once 'Controllers/UserTerritoryController.php';
include_once 'Controllers/UserController.php';
include_once 'Controllers/CountryController.php';
include_once 'Controllers/CustomerAddressController.php';
include_once 'Controllers/ProductController.php';
include_once 'Controllers/TypeOfProductLookupController.php';
include_once 'Controllers/VersionLookupController.php';
$userHistoryController = new UserHistoryController();
$installationController = new InstallationController();
$customerController = new CustomerController();
$customerContactsController = new CustomerContactsController();
$designerLookupController = new DesignerLookupController();
$seriesLookupController = new SeriesLookupController();
$engineModelController = new EngineModelController();
$installationEngineController = new InstallationEngineController();
$loadPriorityController = new LoadPriorityController();
$customerStatusLookupController = new CustomerStatusLookupController();
$customerStatusController = new CustomerStatusController();
$userTerritoryController = new UserTerritoryController();
$userController = new UserController();
$countryController = new CountryController();
$customerAddressController = new CustomerAddressController();
$productController = new ProductController();
$typeOfProductLookupController = new TypeOfProductLookupController();
$versionLookupController = new VersionLookupController();


require_once 'Helpers/charsetFunctions.php';
/**
 * Set the installation in parts so it is less overwhelming.
 */
$step = "";
if (isset($_GET['step'])) {
    $step = $_GET['step'];
}

include("./Views/Menus/addNewRecordMenu.php");
switch ($step) {
    case "customer":
        print "<h3>Add Customer</h3>";
        include 'Views/AddInstallation/addNewCustomer.php';
        break;
    case "address":

        if (isset($_GET['action']) && ($_GET['action'] == 'saveCustomer')) {
            // save the customer
            $newCustomer = new Customer(
                    "", $_POST['customerName'], $_POST['mainTelephone'], 
                    $_POST['mainFax'], $_POST['website'], "", $_POST['tbhAccountID'], 
                    $_POST['tbhAccountCode'], $_POST['ldAccountID'], $_POST['ldAccountCode'], 
                    $_POST['headerNotes'], '', '', '', '', '', '');


            $customerID = $customerController->setCustomerObject($newCustomer);
            $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "create", "New Customer created ID: " . $customerID . " " . $_POST['customerName']);

            // set status
            foreach ($_POST as $key => $value) {
                if (substr($key, 0, -2) == "status") {
                    $saveStatus = new CustomerStatus("", $customerID, $value);
                    $newStatus = $customerStatusController->setNewCustomerStatusObject($saveStatus);
                }
            }
            include 'Views/AddInstallation/addNewAddress.php';
        } else if (isset($_GET['action']) && ($_GET['action'] == 'saveAddress')) {

            $addressVerified = 0;
            if (isset($_POST['verified'])) {
                $addressVerified = $_POST['verified'];
            }
            $defaultAddress = 0;
            if (isset($_POST['default'])) {
                $defaultAddress = $_POST['default'];
            }
            $archived = 0;


            $saveAddress = new CustomerAddress(
                    '', addslashes($_POST['officeName']), addslashes($_POST['address']), addslashes($_POST['city']), addslashes($_POST['state']), addslashes($_POST['country']), addslashes($_POST['postcode']), $addressVerified, $_POST['customerID'], $archived, $defaultAddress, "");


            $setCustomerAddress = $customerAddressController->setCustomerAddress($saveAddress);

            // Log in the DB
            $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "create", "Customer Address Added: Address ID " . $setCustomerAddress);
            if (isset($_POST['addAddressPlus'])) {
                // save address.
                print "<p class='blue'>New Address Added - " . $_POST['officeName'] . "</p>";
                include 'Views/AddInstallation/addNewAddress.php';
            } else {
                // Save address and show contacts form.
                include 'Views/AddInstallation/addContacts.php';
            }
        } else if (isset($_GET['action']) && ($_GET['action'] == 'saveContact')) {

            $contactValidated = 0;
            if (isset($_POST['validated'])) {
                $contactValidated = $_POST['validated'];
            }
            // Show review of customer, ask to create installation.
            $saveContact = new CustomerContact(
                    "", addslashes($_POST['salutation']), addslashes($_POST['firstName']), addslashes($_POST['surname']), addslashes($_POST['jobTitle']), addslashes($_POST['email']), addslashes($_POST['number']), addslashes($_POST['mobileNumber']), addslashes($_POST['infoSource']), 0, $contactValidated, $_POST['customerID'], $_POST['customerAddressID']);

            $setObject = $customerContactsController->setContact($saveContact);

            // Log in the DB
            $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "create", "Customer Contact Created: ID " . $setObject);
            if (isset($_POST['saveContactPlus'])) {
                print "<p class='blue'>New Contact Added</p>";
                include 'Views/AddInstallation/addContacts.php';
            } else {
                // show review
                include 'Views/AddInstallation/newCustomerReview.php';
            }
        }


        break;
    case "installation":
        print "<h3>Add Installation to Customer</h3>";
        // customer has already been added or needs to be picked.
        include 'Views/AddInstallation/addInstallation.php';
        break;
    case "engines":
        if (isset($_GET['action']) && ($_GET['action'] == 'saveProduct')) {
           /* print "<pre>";
            print_r($_POST);
            print_r($_GET);
            print "</pre>";*/
            // save the engine and either move on to confirmation or add another engine
            $customerID = $_GET['customerID'];
            $installationID = $_GET['installationID'];
            $engineVerified = 0;

            // get the customer and the installation
            $customer = $customerController->getCustomerByID($customerID);
            $installation = $installationController->getInstallationByInstallationID($installationID);

            if (isset($_POST['engineVerifiedCheckBox'])) {
                $engineVerified = '1';
            }
     
           // $versionLookup = $versionLookupController->getVersionIDBySeriesID($_POST['series']);
            

            if (isset($_POST['useBulkEntry'])) {
                // Bulk entry has been chosen so need to check how many entries of the same
                // engine need to be made.
                $engineCountEntries = 1;
                if (isset($_POST['engineCount'])) {
                    $engineCountEntries = $_POST['engineCount'];
                }
                $countOfProducts = $productController->countOfProducts($installation->installationID);
                $copPlus1 = $countOfProducts + 1;
                for ($i = $copPlus1; $i < ($copPlus1+$engineCountEntries); $i++) {
                    $typeOfProductShortCode = $typeOfProductLookupController->getTypeOfProductByID($seriesLookupController->getSeriesByID($_POST['series'])->typeOfProductLookupID)->toplShortCode;
                    $unitName = $installation->installationName." ".$typeOfProductShortCode." ".$i;
                    
                    $versionID = "";
                    if (!isset($_POST['versionID']) || ($_POST['versionID'] == "")) {
                        $versionID = reset($versionLookupController->getVersionIDBySeriesID($_POST['series']))->versionID;
                        //print "NO VERSION ID";
                    } else {
                        $versionID = $_POST['versionID'];
                        //print "VERSION ID";
                    }
                    
                    
                    $product = new Product(
                            "", "", $_POST['description'], $_POST['drawingNumber'], $_POST['comments'], 0, $customerID, $installationID, $versionID, $unitName, '');

                    $productID = $productController->setNewProduct($product);
                    $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "create", "Product Created: ID " . $productID);

                    if (isset($_POST['productType']) && ($_POST['productType'] == '1')) {
                        $loadPriority = '3';
                        if (isset($_POST['loadPriority'])) {
                            $loadPriority = $_POST['loadPriority'];
                        }
                        $engine = new InstallationEngine(
                                "", $productID, $_POST['engineModelDescription'], $_POST['mainOrAux'], $engineVerified, $_POST['output'], 'MW', $_POST['fuelType'], $_POST['boreSize'], $_POST['stroke'], $_POST['releaseDate'] . "-01-01 00:00:00", 0, $loadPriority, "", "");
                        $newEngine = $installationEngineController->setNewEngine($engine);
                        $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "create", "Engine Sub Details Created: ID " . $newEngine);
                    }
                }
            } else {
                
                $typeOfProductShortCode = $typeOfProductLookupController->getTypeOfProductByID($seriesLookupController->getSeriesByID($_POST['series'])->typeOfProductLookupID)->toplShortCode;
                $countOfProductsByType = $productController->countOfProductsByType($installationID, $_POST['versionID']);
                
                $unitName = $installation->installationName." ".$typeOfProductShortCode." ".($countOfProductsByType+2);
               
                $versionID = "";
                    if (!isset($_POST['versionID']) || ($_POST['versionID'] == "")) {
                        $versionID = reset($versionLookupController->getVersionIDBySeriesID($_POST['series']))->versionID;
                        //print "NO VERSION ID";
                    } else {
                        $versionID = $_POST['versionID'];
                        //print "VERSION ID";
                    }
                    
                // a single product has been added so this is just one entry to the DB.
                $product = new Product(
                        "", $_POST['serialNumber'], $_POST['description'], $_POST['drawingNumber'], $_POST['comments'], 0, $customerID, $installationID, $versionID, $unitName, '');

                $productID = $productController->setNewProduct($product);
                $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "create", "Product Created: ID " . $productID);

                if (isset($_POST['productType']) && ($_POST['productType'] == '1')) {
                    $loadPriority = '3';
                    if (isset($_POST['loadPriority'])) {
                        $loadPriority = $_POST['loadPriority'];
                    }
                    $engine = new InstallationEngine(
                            "", $productID, $_POST['engineModelDescription'], $_POST['mainOrAux'], $engineVerified, $_POST['output'], 'MW', $_POST['fuelType'],  $_POST['boreSize'], $_POST['stroke'], $_POST['releaseDate'] . "-01-01 00:00:00", 0, $loadPriority, "", "");
                    $newEngine = $installationEngineController->setNewEngine($engine);
                    $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "create", "Engine Sub Details Created: ID " . $newEngine);
                }
            }

            if (isset($_POST['submitPlus'])) {
                include 'Views/AddInstallation/addNewEngines.php';
            } else {
                print "Confirmation";
               // print '<meta http-equiv="refresh" content="0;url=index.php?portal=mr&page=report&action=showInstallation&installationID=&customerID=' . $customerID . '&submit=Go"/>';
            }
        } else {
            // its come from the installations page. Add the installation.
            $customerID = $_POST['customerID'];
            $customer = $customerController->getCustomerByID($customerID);

            $type = $_POST['type'];
            $installationName = $_POST['installationName'];
            $installationType = $_POST['installation'];
            $imo = "";
            $addressID = "";
            if (isset($_POST['customerAddressID'])) {
                $addressID = $_POST['customerAddressID'];
            }
            if (isset($_POST['imo'])) {
                $imo = $_POST['imo'];
            }
            $originalSource = $_POST['source'];
            $status = $_POST['status'];
            $originalSourceDate = $_POST['originalSourceDate'] . "-01-01 00:00:00";
            $ownerParent = $_POST['ownerParent'];
            $technicalManager = $_POST['technicalManager'];
            // MARINE BLOCK
            $hullNumber = "";
            $hullType = "";
            $yardBuilt = "";
            $builtDate = "";
            $lengthOverall = "";
            $length = "";
            $beamExtreme = "";
            $beamMoulded = "";
            $deadweight = "";
            $draught = "";
            $grossTonnage = "";
            $buildYear = "";
            $propellerType = "";
            $propulsionUnitCount = "";
            $teu = "";
            $shipBuilder = "";
            if ($_POST['type'] == 'MARINE') {
                $hullNumber = $_POST['hullNumber'];
                $hullType = $_POST['hullType']; // leaving in for future but currently redundant.
                $yardBuilt = $_POST['yardBuilt'];
                $builtDate = $_POST['builtDate'] . "-01 00:00:00";
                $lengthOverall = $_POST['lengthOverall'];
                $length = $_POST['length'];
                $beamExtreme = $_POST['beamExtreme'];
                $beamMoulded = $_POST['beamMoulded'];
                $deadweight = $_POST['deadweight'];
                $draught = $_POST['draught'];
                $grossTonnage = $_POST['grossTonnage'];
                $buildYear = "";
                $propellerType = $_POST['propellerType'];
                $propulsionUnitCount = $_POST['propulsionUnitCount'];
                $teu = $_POST['teu'];
                $shipBuilder = $_POST['shipBuilder'];
            }


            if (isset($_POST['installationName'])) {
                $checkForInstallationExistance = $installationController->getInstallationsByInstallationName($_POST['installationName']);

                if (!isset($checkForInstallationExistance)) {
                    $newInstallation = new Installation("", $type, $installationType, $installationName, $imo, $ownerParent, $technicalManager, 0, $status, $originalSource, $originalSourceDate, "", $hullNumber, $hullType, $yardBuilt, $deadweight, $lengthOverall, $beamExtreme, $beamMoulded, $builtDate, $draught, $grossTonnage, $length, $propellerType, $propulsionUnitCount, $shipBuilder, $teu, $buildYear, '0', '', $addressID, "", "", "", "");

                    $installationID = $installationController->createNewInstallationObject($newInstallation, $customerID);
                    $userHistoryController->setUserHistory($_SESSION['user_id'], date("Y-m-d H:i:s", time()), "create", "New installation created ID: " . $installationID . " " . $newInstallation->installationName);
                } else {
                    reset($checkForInstallationExistance);
                    $first_key = key($checkForInstallationExistance);
                    $installationID = $first_key;
                }
            }
            $installation = $installationController->getInstallationByInstallationID($installationID);
            include 'Views/AddInstallation/addNewEngines.php';
        }

        break;

    default :
        /*print "<h3>Add Customer</h3>";
        include 'Views/AddInstallation/addNewCustomer.php';*/
        print "Please select one option from the menu. ";
        break;
}