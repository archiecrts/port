<?php
/**
 * I feel a note is required here to remind my future self what a balls-ache this was.
 * For some reason when accessing a PHP page via JQuery it loses the ability to see the
 * included Controller and Models as expected. I could either generate an error by adding the
 * ../../ to to the controller or to the model. neither worked correctly. So I had to use
 * business logic without going through a model/controller setup. It works. Dont change it.
 * Don't bother trying to waste your time making this MVC, its evil.
 */

include_once("../../Models/Database.php");


$designvalue = $_GET['svalue'];
$typevalue = $_GET['tvalue'];
$selectedValue = '';
if (isset($_GET['selected'])) {
    $selectedValue = $_GET['selected'];
}

$result = mysqli_query(Database::$connection, "SELECT * FROM series_lookup_tbl WHERE designer_lookup_tbl_dlt_id = '".$designvalue."' AND "
        . "type_of_product_lookup_tbl_toplt_id = '".$typevalue."' ORDER BY mlt_series ASC");
 
echo '<option value="">Please select...</option>';
while($row = mysqli_fetch_array($result))
  {
    $selected = "";
    if ($selectedValue == $row['mlt_id']) {
        $selected = "selected = 'selected'";
    }
    echo '<option value="'.$row['mlt_id'].'" '.$selected.'>' . $row['mlt_series'] . "</option>";
  }
 
mysqli_free_result($result);
