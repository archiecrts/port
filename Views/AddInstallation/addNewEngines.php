<script type="text/javascript">

    // MODELS drop down
    $(document).ready(function ($) {

        // this shows and hides the engines bit.
        $("#type").change(function () {
            var selectvalue = $(this).val();
            //alert(selectvalue);
            if (selectvalue == "1") {
                $('#engineBits').find('input:text').val('');
                document.getElementById('engineBits').style.display = "block";
            } else {
                document.getElementById('engineBits').style.display = "none";
            }
            
            // get the new dropdown list to populate manufacturer with.
            $.ajax({url: 'Views/AddInstallation/typeOfProductDropDownAjax.php?tvalue=' + selectvalue,
                    success: function (output) {
                          //  alert(output);
                        $('#list-select').html(output);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status + " " + thrownError);
                    }});

        });
        
        $("#useBulkEntry").click(function () {
            var checked = $("#useBulkEntry").is(":checked");
            //alert ('changed' + checked);
            
            if (checked) {
                $('#unitName').attr('disabled','disabled');
                 $('#serialNumber').attr('disabled','disabled');
                 $('#serialNumber').val('');
                 $('#engineCount').removeAttr('disabled'); 
            } else {
                $('#unitName').removeAttr('disabled'); 
                $('#serialNumber').removeAttr('disabled'); 
                $('#engineCount').attr('disabled','disabled');
            }
        });


        var list_target_id = {};
        var list_select_id = {};
        var list_version_id = {};


        list_target_id['list-target'] = 'list-target'; //first select list ID (MANUFACTURER)
        list_select_id['list-select'] = 'list-select'; //second select list ID (SERIES)
        list_version_id['list_version'] = 'list_version'; // third select list ID (VERISON)
        
        var initial_target_html = '<option value="">Please select a make...</option>'; //Initial prompt for target select

        $('#' + list_target_id['list-target']).html(initial_target_html); //Give the target select the prompt option

        $('#' + list_select_id['list-select']).change(function (e) {
           // var changedID = this.id;
            //var target = "list-target_" + changedID[1];
            // alert("change " + this.id);
            //Grab the chosen value on first select list change
            var selectvalue = $(this).val();
            var typeValue = $('#type').val();

            //Display 'loading' status in the target select list
            $('#' + list_target_id['list-target']).html('<option value="">Loading...</option>');

            if (selectvalue === "") {
                //Display initial prompt in target select if blank value selected
                $('#' + list_target_id['list-target']).html(initial_target_html);
            } else {
                //Make AJAX request, using the selected value as the GET

                $.ajax({url: 'Views/AddInstallation/dynamicSeriesDropDown.php?svalue=' + selectvalue + '&tvalue=' + typeValue,
                    success: function (output) {

                        $('#' + list_target_id['list-target']).html(output);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status + " " + thrownError);
                    }});
            }
        });
        
         $('#' + list_target_id['list-target']).change(function (e) {
            //var changedID = this.id;
            //var target = "list-target_" + changedID[1];
            // alert("change " + this.id);
            //Grab the chosen value on first select list change
            var selectvalue = $(this).val();
            //var typeValue = $('#type').val();

            //Display 'loading' status in the target select list
            $('#' + list_version_id['list-version']).html('<option value="">Loading...</option>');

            if (selectvalue === "") {
                //Display initial prompt in target select if blank value selected
                $('#' + list_version_id['list-version'] + ' option[value=' + initialIDValue + ']').prop('selected', 'true');
            } else {
                //Make AJAX request, using the selected value as the GET

                $.ajax({url: 'Views/AddInstallation/dynamicVersionDropDown.php?svalue=' + selectvalue,
                    success: function (output) {
                        //alert(output);
                        $('#list-version').html(output);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status + " " + thrownError);
                    }
                });
            }
        });
    });

</script>


<?php
print "<h3>Add New Product</h3>";
print "<p>" . $customer->customerName . "</p>";
print "<p>" . $installation->installationName . "</p>";
?>
<form name="form1" action="index.php?portal=mr&page=addNewInstallation&step=engines&installationID=<?php echo $installationID; ?>&customerID=<?php echo $customerID; ?>&action=saveProduct" method="post" enctype="multipart/form-data">
    <h3>Product Details</h3>
    <table>
        <tr>
            <td class="title">Product Type</td>
            <td><select id="type" name="productType">
                    <?php
                    $productTypes = $typeOfProductLookupController->getAllProductTypes();
                    foreach ($productTypes as $key => $productType) {
                        print "<option value='$key'>$productType->toplProductDescription</option>";
                    }
                    ?>
                </select></td>
            <td class="title"></td>
            <td></td>
        </tr>
        <tr>
            <td colspan='4'><hr/></td>
        </tr>
        <tr>
            <td class="title">Manufacturer</td>
            <td><select name="engineManufacturer"  id="list-select">
                    <option value="">SELECT</option>
                    <?php
                    $engineManufacturers = $designerLookupController->getDistinctDesignersByTypeOfProductID(1);

                    foreach ($engineManufacturers as $name => $engine) {
                        if ($engine->designerDescription != "") {
                            echo '<option value="' . htmlXentities($engine->designerID) . '">' . htmlXentities($engine->designerDescription) . '</option>';
                        }
                    }
                    ?>
                </select>
            </td>
            <td class="title">Series</td>
            <td><select name="series" id="list-target">
                    <?php
                    $engineSeries = $seriesLookupController->getDistinctModelSeries();
                    if (isset($engineSeries)) {
                        foreach ($engineSeries as $name => $series) {
                            if ($series->seriesDescription != "") {
                                echo '<option value="' . htmlXentities($series->seriesDescription) . '">' . htmlXentities($series->seriesDescription) . '</option>';
                            }
                        }
                    }
                    ?>
                </select>     
            </td>
        </tr>
        <tr>
            <td class="title"><!--Unit Name--></td>
            <td><!--<input type="text" name="unitName" id="unitName" value=""/>--></td>
            <td class="title">Serial Number</td>
            <td><input type="text" name="serialNumber" id="serialNumber" value=""/></td>
        </tr>
        
        <tr>
            <td class="title">Description</td>
            <td><input type="text" name="description" value=""/></td>
            <td class="title">Drawing Number</td>
            <td><input type="text" name="drawingNumber" value=""/></td>
        </tr>
        <tr>
            <td class="title">Comments</td>
            <td colspan="3"><textarea name="comments" cols="70" rows="6"></textarea></td>

        </tr>
    </table>
    <div id="bulkCheck">
        <h3>Bulk Entry</h3>
        <table>
            <tr>
                <td class="title">Use Bulk Entry?</td>
                <td><input type="checkbox" name="useBulkEntry" id="useBulkEntry" value="1"/></td>
                <td>Number of copies of products to generate</td>
                <td><input type="text" name="engineCount" id="engineCount" disabled="disabled"/></td>
            </tr>
        </table>
    </div>
    <div id="engineBits">
        <h3>Engine Sub Details</h3>
        <table>
            <tr>
                <td class="title">Main or Aux</td>
                <td><select name="mainOrAux">
                        <?php
                        $engineOption = ['Main', 'Aux'];
                        foreach ($engineOption as $value) {

                            print '<option value="' . $value . '" >' . $value . '</option>';
                        }
                        ?>

                    </select></td>
                <td class="title">Engine Verified</td>

                <td><input type="checkbox" id="engineVerifiedCheckBox" name="engineVerified" value="1"  /></td>

            </tr>
            <tr>
                <td class="title">Engine Model Description</td>
                <td><input type='text' name="engineModelDescription" ></td>
                <td class="title"></td>
                <td></td>
            </tr>
            <tr>
                <td class="title">Unit Name</td>
                <td><input type="text" name="unitName" value="" id="unitName"/></td>
                <td class="title">Version</td>
                        <td>
                            <select name="versionID" id="list-version">
                                <option>Please Select Series...</option>
                            </select>
                        </td>
            </tr>


            <tr>
                <td class="title">Output</td>
                <td><input type="text" name="output" value=""/></td>
                <td class="title"></td>
                <td></td>
            </tr>
            <tr>
                <td class="title">Fuel Type</td>
                <td><select name="fuelType">
                        <option value="Gas">Gas</option>
                        <option value="Oil">Oil</option>
                        <option value="Duel Fuel">Duel Fuel</option>
                        <option value="Unknown">Unknown</option>
                    </select></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="title">Bore Size</td>
                <td><input type="text" name="boreSize" value=""/> (mm)</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="title">Stroke</td>
                <td><input type="text" name="stroke" value=""/> (mm)</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="title">Release Date</td>
                <td><input type="text" name="releaseDate" value="" placeholder="registration year of engine" pattern="[0-9]{4}" /></td>
                <td></td>
                <td></td>
            </tr>
            <?php
            if ($installation->type == "LAND") {
            ?>
            <tr>
                <td class="title">Load Priority</td>
                <td><select name="loadPriority">
                        <?php
                        $loadPriorityArray = $loadPriorityController->getLoadPriorities();
                        foreach ($loadPriorityArray as $key => $loadPriority) {
                            print '<option value="' . $key . '">' . $loadPriority->loadPriority . '</option>';
                        }
                        ?>
                    </select></td>
                <td></td>
                <td></td>
            </tr>
            <?php
            }
            ?>
            <tr>
                <td colspan="4"><hr/></td>
            </tr>

        </table>
    </div>


    <input type="submit" name="submit" value="Save and Continue"/>
    <input type="submit" name="submitPlus" value="Save and Add New Product"/>
</form>
<div id="currentlyFittedEngines">
    <h3>Currently Fitted Engines</h3>
    <?php
    $productsList = $productController->getProductsByInstallationID($installation->installationID);
    if (isset($productsList)) {
        foreach ($productsList as $key => $productObject) {
            $engineObject = NULL;
            $engineCheck = $installationEngineController->getEngineByProductID($key);   
            $typeOfProduct = $typeOfProductLookupController->getTypeOfProductByID($seriesLookupController->getSeriesByVersionLookupID($productObject->versionLookupID)->typeOfProductLookupID)->toplProductDescription;
            if (isset($engineCheck)) {
                $engineObject = $engineCheck;
            }
            $versionObject = $versionLookupController->getVersionByID($productObject->versionLookupID);
            //print_r($engineObject);
            ?>
            <table style="width:100%;">
                <tr>
                    <td colspan="8"><img src="./icons/edit_pencil.png" alt="edit" height='15px'/> &nbsp; <a href="index.php?portal=mr&page=report&action=showInstallation&customerID=<?php echo $customerID; ?>&installationID=<?php echo $installationID; ?>&tab=technical&reportID=<?php echo $reportOverviewID; ?>&edit=edit&productID=<?php echo $key; ?>">Edit</a></td>
                </tr>
                <tr>
                    <td class="title">Type of Product</td>
                    <td><span class="red"><?php echo $typeOfProduct; ?></span></td>
                    <td class="title">Drawing Number</td>
                    <td><span class="red"><?php echo $productObject->productDrawingNumber; ?></span></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="title">Serial Number</td>
                    <td><span class="blue"><?php echo $productObject->productSerialNumber; ?></span></td>
                    <td class="title">Main or Auxiliary</td>
                    <td><?php echo $engineObject->mainOrAux; ?></td>
                    <td class="title">Output</td>
                    <td><?php echo $engineObject->output . " " . $engineObject->outputUnitOfMeasurement; ?></td>
                    <td class="title">Release Date</td>
                    <td><?php
                        if ($engineObject->releaseDate != "0000-00-00 00:00:00") {
                            echo date("Y", strtotime($engineObject->releaseDate));
                        }  
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class="title">Unit Name</td>
                    <td><?php echo htmlXentities($productObject->productUnitName); ?></td>
                    <td class="title">No. of Cylinders</td>
                    <td><?php echo $versionObject->cylinderCount; ?></td>
                    <td class="title">Fuel Type</td>
                    <td><?php echo $engineObject->fuelType; ?></td>
                    <td class="title">Comment</td>
                    <td><?php echo $productObject->productComment; ?></td>
                </tr>
                <tr>
                    <td class="title">Engine Manufacturer</td>
                    <td><?php echo htmlXentities($designerLookupController->getDesignerByVersionLookupID($productObject->versionLookupID)->designerDescription); ?></td>
                    <td class="title">Cylinder Configuration</td>
                    <td><?php echo $versionObject->cylinderConfiguration; ?></td>
                    <td class="title">Bore Size</td>
                    <td><?php echo $engineObject->boreSize; ?></td>
                    <td class="title">Load Priority</td>
                    <td><?php echo $loadPriorityController->getLoadPriorityByID($engineObject->loadPriorityID)->loadPriority; ?></td>
                    
                </tr>
                <tr>
                    <td class="title">Engine Model</td>
                    <td><?php echo htmlXentities($engineObject->modelDescription); ?></td>
                    <td class="title">Series</td>
                    <td><?php echo htmlXentities($seriesLookupController->getSeriesByVersionLookupID($productObject->versionLookupID)->seriesDescription); ?></td>
                    <td class="title">Stroke</td>
                    <td><?php echo $engineObject->stroke; ?></td>
                    <td class="title">Engine Verified</td>
                    <td><?php
                        if ($engineObject->engineVerified == 1) {
                            echo "yes";
                        } else {
                            echo "no";
                        }
                        ?></td>
                </tr>
                

            </table>
            <?php
        }
    } else {
        print "No engines to display</p>";
    }
    ?>
</div>