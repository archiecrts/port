<form name="instForm" action="index.php?portal=mr&page=addNewInstallation&step=address&action=saveCustomer" method="post" enctype="multipart/form-data">
    
<table>
    <tr>
        <td class="title">Customer Name</td>
        <td><input type="text" name="customerName" value=""  autocomplete="off"/></td>
    </tr>
    <tr>
        <td class="title">Account Type</td>
        <td><table>
                <?php
                $accountTypes = $customerStatusLookupController->getAllCustomerStatusLookupValues();

                foreach ($accountTypes as $type => $status) {
                    
                    print "<tr><td>" . $status->custStatusDescription
                            . "</td><td><input type='checkbox' name='status_" . $type . "' value='$type'  /></td></tr>";
                }
                ?>

            </table></td>
    </tr>
    <tr>
        <td class="title">Website</td>
        <td><input type="text" name="website" value="" /></td>
    </tr>
    <tr>
        <td class="title">Main Telephone</td>
        <td><input type="text" name="mainTelephone" value="" /></td>
    </tr>
    <tr>
        <td class="title">Main Fax</td>
        <td><input type="text" name="mainFax" value="" /></td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <td class="title">Account Manager</td>
        <td><select name="accountManager">
                <option value="">Set by territory</option>
            <?php
             $accountManagers = $userController->getSalesManagers();
              foreach ($accountManagers as $key => $manager) {
              $selected = "";
              if ($customer->accountManager == $manager->id) {
              $selected = "selected";
              }
              print "<option value='" . $manager->id . "' $selected>" . $manager->name . "</option>";
              } 
            ?>
            </select>

        </td>
    </tr>
    
    <tr>
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <td class="title">TBH Account ID</td>
        <td><input type='text' name="tbhAccountID"/></td>
    </tr>
    <tr>
        <td class="title">TBH Account Code</td>
        <td><input type='text' name="tbhAccountCode"/></td>
    </tr>
    <tr>
        <td class="title">LD Account ID</td>
        <td><input type='text' name="ldAccountID"/></td>
    </tr>
    <tr>
        <td class="title">LD Account Code</td>
        <td><input type='text' name="ldAccountCode"/></td>
    </tr>
    <tr>
        <td class="title">Header Notes</td>
        <td><textarea name="headerNotes"></textarea></td>
    </tr>
    <tr>
        <td></td>
        <td><input type="submit" name="submit" value="Save and Continue"></td>
    </tr>
</table>
    
</form>