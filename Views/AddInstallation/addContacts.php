<h3>Add New Contacts</h3>
<form name="contactForm" action="index.php?portal=mr&page=addNewInstallation&step=address&action=saveContact" method="post" enctype="multipart/form-data">
<table>
    <tr>
        <td class="title">Title</td>
        <td><input type="text" name="salutation" value=""/></td>
    </tr>
    <tr>
        <td class="title">Contact Name</td>
        <td><input type="text" name="firstName" value="" placeholder="First Name"/> 
            <input type="text" name="surname" value="" placeholder="Surname"/></td>
    </tr>
    <tr>
        <td class="title">Job Title</td>
        <td><input type="text" name="jobTitle" value=""/></td>
    </tr>
    <tr>
        <td class="title">Office</td>
        <td><select name="customerAddressID">
                <option value=""></option>
                <?php
                $customerAddresses = $customerAddressController->getCustomerAddressesByCustomerID($_POST['customerID']);
                foreach ($customerAddresses as $key => $address) {
                    
                    print "<option value='" . $address->addressID . "'>" . $address->officeName . "</option>";
                }
                ?>

            </select></td>
    </tr>
    <tr>
        <td class="title">Email</td>
        <td><input type="text" name="email" value=""/></td>
    </tr>
    <tr>
        <td class="title">Number</td>
        <td><input type="text" name="number" value=""/></td>
    </tr>
    <tr>
        <td class="title">Mobile Number</td>
        <td><input type="text" name="mobileNumber" value=""/></td>
    </tr>
    <tr>
        <td class="title">Info Source</td>
        <td><input type="text" name="infoSource" value=""/></td>
    </tr>
    <tr>
        <td class="title">Contact Validated (are you sure contact exists)</td>
       
        <td><input type="checkbox" name="validated" value="1"/></td>
    </tr>
    <tr>
        <td><input type="submit" name="saveContact" value="Save and review"/></td>
        <td style="text-align: right;"><input type="submit" name="saveContactPlus" value="Add more contacts"/></td>
    </tr>
</table>
    
    <input type="hidden" name="customerID" value="<?php echo $_POST['customerID']; ?>"/>
</form>