

<?php

/* 
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
$customerID = $_POST['customerID'];
$customer = $customerController->getCustomerByID($customerID);
$customerAddresses = $customerAddressController->getCustomerAddressesByCustomerID($customerID);
$defaultAddress = $customerAddressController->getDefaultCustomerAddressByCustomerID($customerID);
$customerContacts = $customerContactsController->getContactsByCustomerID($customerID);
?>
<style>
    a.buttonLink {
    -webkit-appearance: button;
    -moz-appearance: button;
    appearance: button;
    padding: 5px;
    font-weight: bold;
    text-decoration: none;
    color: initial;
}
</style>

    
<p><a class="buttonLink" href="index.php?portal=mr&page=addNewInstallation&step=installation&customerID=<?php echo $customerID; ?>">Add installation to this customer</a></p>
    


<table style="width:100%;">
    <tr>
        <th colspan="3">
            <img src="./icons/crm_grey.png" alt="" height="20px"/>
            Customer Information</th>
        <th style="text-align: right;"><img src="./icons/edit_pencil.png" alt="edit" height='15px'/> <a href="index.php?portal=mr&page=report&action=showInstallation&tab=customer&reportID=&edit=edit&customerID=<?php echo $customerID; ?>">Edit</a></th>
    </tr>
    <tr>
        <td class="title">Name</td>
        <td><?php
            if ($customer->customerName == "") {
                print "Unknown Customer";
            } else {
                print $customer->customerName;
            }
            ?>
        </td>
        <td class="title">Main Fax</td>
        <td><?php print $customer->mainFax; ?></td>
    </tr>
    <tr>
        <td class="title">Account Type(s)</td>
        <td><?php
        
            $custStatusArray = $customerStatusController->getAllCustomerStatusValuesByCustomerID($customer->customerID);
            
            if (isset($custStatusArray)) {
                foreach ($custStatusArray as $key => $status) {
                    print $customerStatusLookupController->getCustomerStatusLookupByID($status->custStatusLookupID)->custStatusDescription . "<br/>";
                }
            }
            ?></td>
        <td class="title">Account Manager</td>
        <td>
            <?php
            if ($customer->accountManager != '') {
                echo $userController->getUserByID($customer->accountManager)->name;
            } else {
                echo "Set by territory";
            }
            ?>
        </td>
    </tr>
    <tr>
        <td class="title">Website</td>
        <td><a href="
            <?php
            if (substr($customer->website, 0, 7) == "http://") {
                print $customer->website;
            } else {
                print "http://" . $customer->website;
            }
            ?>" target="_blank"><?php print $customer->website; ?></a></td>
        <td class="title">Territory Assigned to</td>
        <td><?php
            $territoryAccountManager = $userTerritoryController->getUserIDByTerritory($defaultAddress->country);

            echo $userController->getUserByID($territoryAccountManager)->name;
            ?>
        </td>
    </tr>
    <tr>
        <td class="title">Main Telephone</td>
        <td><?php print $customer->mainTelephone; ?></td>
        <td class="title">Header Notes</td>
        <td><?php echo $customer->headerNotes; ?></td>
    </tr>

</table>


<div id="officeLocations">
    <div id="officeLocationsHeader"><img src="./icons/office.png" alt="" height='20px'/> Office Locations:</div>
    <?php
    if (isset($customerAddresses)) {
        foreach ($customerAddresses as $key => $address) {
            ?>
            <div id="address_<?php echo $address->addressID; ?>" class="selectorDropDownBar">
                <?php
                echo $address->city . ", " . $address->country;
                if ($address->defaultAddress === '1') {
                    print " (default address)";
                }
                ?>
            </div>
            <div>
                <?php
                print '<table>
                    <tr>
                        <td rowspan="7"><img src="./icons/office.png" alt="edit" height="40px"/></td>
                        <td colspan="4"><a href="index.php?portal=mr&page=report&action=showInstallation&customerID=' . $customerID . '&tab=customerAddress&reportID=&edit=edit&addressID=' . $address->addressID . '">Edit</a></td>
                    </tr>
                    <tr>
                        <td class="title">Office Name</td>
                        <td>' . $address->officeName . '</td>
                        <td class="title">Area</td>
                        <td>' . $countryController->getTimeZoneByCountryName($address->country)->geoArea . '</td>    
                    </tr>
                    <tr>
                        <td class="title">Address</td>
                        <td>' . $address->address . '</td>
                        <td class="title">Continent</td>
                        <td>' . $countryController->getTimeZoneByCountryName($address->country)->continent . '</td>
                    </tr>
                    <tr>
                        <td class="title">City</td>
                        <td>' . $address->city . '</td>
                        <td class="title">Default Address</td>
                        <td>';
                if ($address->defaultAddress === '1') {
                    print "Yes";
                } else {
                    print "No";
                }
                print '</td>
                    </tr>
                        <td class="title">State</td>
                        <td>' . $address->state . '</td>
                        <td class="title">Address Verified</td>
                                <td>';
                if ($address->addressVerified === '1') {
                    print "Yes";
                } else {
                    print "No";
                }
                print '</td>
                    </tr>
                    <tr>
                        <td class="title">Post Code</td>
                        <td>' . $address->postcode . '</td>
                        <td class="title">Address Archived</td>
                                <td>';
                if ($address->archived === '1') {
                    print "Yes";
                } else {
                    print "No";
                }
                print '</td>
                    </tr>
                    <tr>
                        <td class="title">Country</td>
                        <td>' . $address->country . '</td>
                        <td colspan="2"></td>
                    </tr>
                            
                        </table></div>';
            }
        }
        ?>

    </div>
    
    <div id='installationContainer'>
    <div id="installationHeader">
        
        <div id="officeLocationsHeader"><img src="./icons/PhoneIcon50x50.png" alt="" height='20px'/> Contacts: </div>
    </div>

&nbsp;
    <div id='installationContentWide'>
        <table style="width:100%;">
            <tr>
                <td></td>
                <td><strong>Office</strong></td>
                <td><strong>Name</strong></td>
                <td><strong>Job Title</strong></td>
                <td><strong>Email</strong></td>
                <td><strong>Number</strong></td>
                <td><strong>Mobile</strong></td>
                <td><strong>Inactive</strong></td>
                <td><strong>Validated</strong></td>
            </tr>
            <?php
                        $contactsArray = $customerContactsController->getContactsByCustomerID($customerID);
                        if (isset($contactsArray)) {
                            foreach ($contactsArray as $key => $value) {
                                print "<tr>";
                                print '<td><a href="index.php?portal=mr&page=report&action=showInstallation&customerID='.$customerID.'&tab=contacts&reportID=&edit=edit&contactID='.$value->customerContactID.'">Edit</a></td>';
                                $address = $customerAddressController->getCustomerAddressByID($value->customerAddressID);
                                print "<td>$address->officeName</td>";
                                print "<td>$value->salutation $value->firstName $value->surname</td>
                                       <td>$value->jobTitle</td>
                                       <td>$value->email</td>
                                       <td>$value->number</td>
                                       <td>$value->mobile</td>
                                       <td>";
                                    if ($value->inactive == 0) {
                                        print "No";
                                    } else {
                                        print "Yes";
                                    }
                                    print "</td>
                                        <td>";
                                    if ($value->contactValidated == 1) {
                                        print "Yes";
                                    } else {
                                        print "No";
                                    }
                                    print "</tr>";
                            }
                        }
            ?>
        </table>
    </div>
</div>
