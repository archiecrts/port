<?php
/**
 * 
 *  I BELIEVE THIS PAGE IS NO LONGER REQUIRED
 */
/* 
 * Check that the origin is set. I want to make sure people don't directly get 
 * to this page without first completing the addInstallation page or be redirected here
 * by a partial creation.
 */
if (isset($_POST['origin']) && $_POST['origin'] == 'addInstallation') {
    $installationID = "";
    $type = addslashes($_POST['type']);
    $installation = addslashes($_POST['installationType']);
    $imo = addslashes($_POST['imo']);
    $installationName = addslashes($_POST['installationName']);
    $managingCompany = addslashes($_POST['managingCompany']);
    $city = addslashes($_POST['city']);
    $state = addslashes($_POST['state']);
    $country = addslashes($_POST['country']);
    $area = "";
    $continent = "";
    $operatingCompany = addslashes($_POST['operatingCompany']); 
    $operatingCompanyCountry = addslashes($_POST['operatingCompanyCountry']);
    $output = "";
    $status = "";
    $year = "";
    $unitType = "";
    $fuelType = "";
    $mainFuel = "";
    $altFuel = ""; 
    $engineManufacturer = "";
    $engineModel = "";
    $engineCylinderCount = "";
    $engineCylinderConfiguration = "";
    $unitsQuantity = "";
    $auxEngineMfr  = "";
    $auxEngineModel = "";
    $auxEngineCylinderCount = "";
    $auxEngineCylinderConfiguration = "";
    $source = addslashes($_POST['source']);
    $website = addslashes($_POST['website']);
    $accountManager = addslashes($_SESSION['user_id']);
    $telephoneNumber = addslashes($_POST['telephoneNumber']);
    $faxNumber = addslashes($_POST['faxNumber']);
    
    $newInstallation = new Installation($installationID, $type, $installation, 
             $installationName, $imo, $year, $status, $operatingCompany, 0, 0, 
            $operatingCompanyCountry, $unitsQuantity, $source, "", "", "", "", "");
    
    $_SESSION['newInstallation'] = serialize($newInstallation);
}





?>






<h3>Technical Data</h3>

<form name="techForm" onsubmit="return validateAddTechnicalData();" action="index.php?portal=mr&page=addNewInstallation&step=contacts" method="post" enctype="multipart/form-data">
<input type="hidden" name="origin" value="technical"/>
<table>
    <tr>
        <td>
            <table>
                <tr>
                    
                    <td class="title">No. of Engines</td>
                    <td><input type="text" name="unitsQuantity" value=""/></td>
                    <td class="title"></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="title">Output</td>
                    <td><input type="text" name="output" value=""/></td>
                    <td class="title"></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="title">Status</td>
                    <td><input type="text" name="status" value=""/></td>
                    <td class="title">Year</td>
                    <td><input type="text" name="year" value=""/></td>
                </tr>
                <tr>
                    <td class="title">Unit Type</td>
                    <td><input type="text" name="unitType" value=""/></td>
                    <td></td>
                    <td></td>
                </tr>

                <tr>
                    <td class="title">Fuel Type</td>
                    <td><input type="text" name="fuelType" value=""/></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="title">Main Fuel</td>
                    <td><input type="text" name="mainFuel" value=""/></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="title">Alt Fuel</td>
                    <td><input type="text" name="altFuel" value=""/></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="4"><hr/></td>
                </tr>
                <tr>
                    <td class="title">Engine Manufacturer</td>
                    <td><select name="engineManufacturer">
                            <option value=""> </option>
                            <?php
                            
                            $engineList = $engineManufacturerController->getEngineBuilders();
                            foreach ($engineList as $id => $engineObject) {
                                print "<option value='".$engineObject->engineBuilder."'>".$engineObject->engineBuilder."</option>";
                            }
                            ?>
                        </select>
                        
                    </td>
                    <td class="title">No. of Cylinders</td>
                    <td><input type="text" name="engineCylinderCount" value=""/></td>
                </tr>
                <tr>
                    <td class="title">Engine Model</td>
                    <td><input type="text" name="engineModel" value=""/></td>
                    <td class="title">Cylinder Configuration</td>
                    <td><input type="text" name="engineCylinderConfiguration" value=""/></td>
                </tr>
                <tr>
                    <td colspan="4"><hr/></td>
                </tr>
                <tr>
                    <td class="title">Aux Engine Mfr</td>
                    <td><select name="auxEngineManufacturer">
                            <option value=""> </option>
                            <?php
                            foreach ($engineList as $id => $engineObject) {
                                print "<option value='".$engineObject->engineBuilder."'>".$engineObject->engineBuilder."</option>";
                            }
                            ?>
                        </select>
                        
                    </td>
                    <td class="title">No. of Cylinders</td>
                    <td><input type="text" name="auxEngineCylinderCount" value=""/></td>
                </tr>
                <tr>
                    <td class="title">Aux Engine Model</td>
                    <td><input type="text" name="auxEngineModel" value=""/></td>
                    <td class="title">Cylinder Configuration</td>
                    <td><input type="text" name="auxEngineCylinderConfiguration" value=""/></td>
                </tr>
                
            </table>
        </td>
        
        <td  style="vertical-align:top">
            
           
        </td>
    </tr>
     <tr>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: right;"><input type="submit" name="submit" value="Next"></td>
    </tr>
</table>
</form>