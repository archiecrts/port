<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once("Models/UserModel.php");
/**
 * Description of UserController
 *
 * @author Archie
 */
class UserController {
    /**
     *
     * @var type UserModel
     */
    public $userModel;
    
    /**
     * __construct function.
     */
    public function __construct()
    {
        $this->userModel = new UserModel();
    }
    
    /**
     * 
     */
    public function invoke()
    {
        include 'Views/Users/users.php';
    }
    
    /**
     * 
     * @return type
     */
    public function getUser($name) 
    {
        return $this->userModel->getUser($name);
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function getUserByID($id) 
    {
        return $this->userModel->getUserByID($id);
    }
    
    /**
     * 
     * @return type
     */
    public function getUsers() 
    {
        return $this->userModel->getUsers();
    }
    
    /**
     * 
     * @param type $userLogin
     * @param type $userPassword
     * @return type
     */
    public function getUserAccessControl($userLogin, $userPassword) {
        return $this->userModel->getUserAccessControl($userLogin, $userPassword);
    }
    
    /**
     * 
     * @return type
     */
    public function getSalesManagers() {
        return $this->userModel->getSalesManagers();
    }
    
    
    
    /**
     * 
     * @param type $login
     * @param type $password
     * @param type $name
     * @param type $level
     * @param type $department
     * @param type $email
     * @param type $flag
     * @param type $limitToTerritory
     * @return type
     */
    public function setNewUser($login, $password, $name, $level, $department, $email, $flag, $limitToTerritory) 
    {
        return $this->userModel->setNewUser($login, $password, $name, $level, $department, $email, $flag, $limitToTerritory);
    }
    
    /**
     * updateUser
     * @param User $user
     * @return boolean
     */
    public function updateUser($user) {
        return $this->userModel->updateUser($user);
    }
    
    
    /**
     * Reset the users password and their initial login state.
     * @param String $password
     * @param INT $userID
     * @return boolean true/false
     */
    public function resetPasswordFirstLogin($password, $userID) {
        return $this->userModel->resetPasswordFirstLogin($password, $userID);
    }
    
    
    /**
     * Reset the users password
     * @param String $password
     * @param INT $userID
     * @return boolean true/false
     */
    public function resetPassword($password, $userID) {
        return $this->userModel->resetPassword($password, $userID);
    }
}
