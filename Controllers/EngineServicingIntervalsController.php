<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/EngineServicingIntervalsModel.php");
/**
 * Description of EngineServicingIntervalsController
 *
 * @author Archie
 */
class EngineServicingIntervalsController {
    public $engineServicingIntervalsModel;
    
    
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->engineServicingIntervalsModel = new EngineServicingIntervalsModel();
    }
    
    /**
     * Invoke Method.
     */
    public function invoke()
    {
        //include(page);
        
    }
}
