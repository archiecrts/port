<?php

/*
 * Property of S-T Diesel Marine Group.
 */
include_once("Models/TrainingModel.php");

/**
 * Description of TrainingController
 *
 * @author Archie
 */
class TrainingController {
   
    /**
     *
     * @var type 
     */
    public $trainingModel;
    
    /**
     * __construct function.
     */
    public function __construct()
    {
        $this->trainingModel = new TrainingModel();
    }
    
    /**
     * 
     */
    public function invoke()
    {
        
    }
    /**
     * addDocumentToUser adds a recently uploaded document to a user so it can be tracked as read.
     * @param INT $userID
     * @param INT $documentID
     * @return boolean
     */
    public function addDocumentToUser($userID, $documentID) {
        return $this->trainingModel->addDocumentToUser($userID, $documentID);
    }
    
    
    /**
     * Returns one record based on user ID and document ID.
     * @param INT $userID
     * @param INT $documentID
     * @return Training Object
     */
    public function getRecordByUserIDandDocID($userID, $documentID) {
        return $this->trainingModel->getRecordByUserIDandDocID($userID, $documentID);
    }
    
    /**
     * getReadReceiptsByDocumentID Gets all training rows where users 
     * have not clicked to say they have read the form.
     * @param INT $documentID
     * @return boolean|\Training
     */
    public function getReadReceiptsByDocumentID($documentID) {
        return $this->trainingModel->getReadReceiptsByDocumentID($documentID);
    }
    
    
    
}
