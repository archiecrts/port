<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once("Models/InsertOWSFromCSVModel.php");
/**
 * Description of InsertOWSFromCSVController
 *
 * @author Archie
 */
class InsertOWSFromCSVController {
    
    public $insertOWSFromCSVModel;
    
    public function __construct()
    {
        $this->insertOWSFromCSVModel = new InsertOWSFromCSVModel();
    }
    
    public function invoke() {}
    
    /**
     * insertDataFromCSV
     */
    public function insertDataFromCSV($fileName) {
        return $this->insertOWSFromCSVModel->insertDataFromCSV($fileName);
    }
}
