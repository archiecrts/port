<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/PartDescriptionLookupModel.php");
/**
 * Description of PartDescriptionLookupController
 *
 * @author Archie
 */
class PartDescriptionLookupController {
    
    public $partDescriptionLookupModel;
    
    public function __construct()
    {
        $this->partDescriptionLookupModel = new PartDescriptionLookupModel();
    }
    
    public function invoke()
    {
        //include(page);   
    }
    
    /**
     * getAllDescriptions
     * @return array
     */
    public function getAllDescriptions() {
        return $this->partDescriptionLookupModel->getAllDescriptions();
    }
    
    /**
     * getAllMajorDescriptions
     * @return type
     */
    public function getAllMajorDescriptions() {
        return $this->partDescriptionLookupModel->getAllMajorDescriptions();
    }
    
    /**
     * getAllDescriptionsLikeString
     * @param STRING $likeString
     * @return type
     */
    public function getAllDescriptionsLikeString($likeString) {
        return $this->partDescriptionLookupModel->getAllDescriptionsLikeString($likeString);
    }
    
     /**
     * getDescriptionByID
     * @param type $descID
     * @return type
     */
    public function getDescriptionByID($descID) {
        return $this->partDescriptionLookupModel->getDescriptionByID($descID);
    }
}
