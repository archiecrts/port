<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/InsertSuppliersFromCSVModel.php");
/**
 * Description of InsertSuppliersFromCSVController
 *
 * @author Archie
 */
class InsertSuppliersFromCSVController {
    public $insertSuppliersFromCSVModel;
    
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->insertSuppliersFromCSVModel = new InsertSuppliersFromCSVModel();
    }
    
    /**
     * Invoke Method.
     */
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * importDataFromCSV
     * @param type $fileName
     * @param type $columnNameArray
     * @param type $headers
     * @param type $type
     * @return type
     */
    public function importDataFromCSV($fileName, $columnNameArray, $headers, $type) {
        return $this->insertSuppliersFromCSVModel->importDataFromCSV($fileName, $columnNameArray, $headers, $type);
    }
}
