<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/MaintenanceModel.php");
/**
 * Description of MaintenanceController
 *
 * @author Archie
 */
class MaintenanceController {
    public $maintenanceModel;
    
    public function __construct()
    {
        $this->maintenanceModel = new MaintenanceModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    
    /**
     * getMaintenanceSwitch
     * @return type
     */
    public function getMaintenanceSwitch() {
        return $this->maintenanceModel->getMaintenanceSwitch();
    }
    
    /**
     * setMaintenanceSwitch
     * @param INT $switch
     * @return boolean
     */
    public function setMaintenanceSwitch() {
        return $this->maintenanceModel->setMaintenanceSwitch();
    }
}
