<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/CustomerConversationHistoryModel.php");
/**
 * Description of InstallationConversationHistoryController
 *
 * @author Archie
 */
class CustomerConversationHistoryController {
    public $customerConversationHistoryModel;
    
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->customerConversationHistoryModel = new CustomerConversationHistoryModel();
    }
    
    /**
     * Invoke Method.
     */
    public function invoke()
    {
        //include(page);
        
    }
    
    
    
    /**
     * 
     * @param type $conversationID
     * @return type
     */
    public function isConversationItemClosed($conversationID) {
        return $this->customerConversationHistoryModel->isConversationItemClosed($conversationID);
    }
    
    /**
     * 
     * @param type $conversationID
     * @return \InstallationConversationHistory
     */
    public function getHistoryByConversationID($conversationID) {
        return $this->customerConversationHistoryModel->getHistoryByConversationID($conversationID);
    }
}

