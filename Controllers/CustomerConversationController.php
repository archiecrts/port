<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once("Models/CustomerConversationModel.php");
/**
 * Description of CustomerConversationController
 *
 * @author Archie
 */
class CustomerConversationController {
   
    public $customerConversationModel;
    
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->customerConversationModel = new CustomerConversationModel();
    }
    
    /**
     * Invoke Method.
     */
    public function invoke()
    {
        //include(page);
        
    }
    /**
     * 
     * @param type $customerID
     * @return array of conversation objects.
     */
    public function getConversationsByCustomerID($customerID)
    {
        return $this->customerConversationModel->getConversationsByCustomerID($customerID);
    }
    
    
    /**
     * 
     * @param type $customerID
     * @param type $offset
     * @param type $rowsPerPage
     * @return type
     */
    public function getConversationsByCustomerIDPaged($customerID, $offset, $rowsPerPage)
    {
        return $this->customerConversationModel->getConversationsByCustomerIDPaged($customerID, $offset, $rowsPerPage);
    }
    
    /**
     * 
     * @param type $userID
     * @param type $entryDate
     * @param type $content
     * @param type $actionDueDate
     * @param type $actionType
     * @param type $contactID
     * @param type $campaignOverviewID
     * @return type
     */
    public function setConversationEntryFromUser($userID, $entryDate, $content, $actionDueDate, $actionType, 
            $contactID, $campaignOverviewID, $replyToConversationItemID) {
     
        
        return $this->customerConversationModel->setConversationEntryFromUser($userID, $entryDate, $content, $actionDueDate, $actionType, 
            $contactID, $campaignOverviewID, $replyToConversationItemID);
    }
    
    
    /**
     * 
     * @param type $conversationID
     * @param type $userID
     * @return boolean
     */
    public function closeConversationItem($conversationID, $userID) {
        return $this->customerConversationModel->closeConversationItem($conversationID, $userID);
    }
}
