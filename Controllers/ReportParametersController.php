<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once("Models/ReportParametersModel.php");
/**
 * Description of ReportParametersController
 *
 * @author Archie
 */
class ReportParametersController {
    public $reportParametersModel;
    
    /**
     * 
     */
    public function __construct()
    {
        $this->reportParametersModel = new ReportParametersModel();
    }
    
    /**
     * 
     */
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * Set Report Parameter function.
     * @param type $reportOverviewID ID
     * @param type $parameter Name of the parameter
     * @param type $value Value of the parameter
     */
    public function setReportParameter($reportOverviewID, $parameter, $value)
    {
        $this->reportParametersModel->setReportParameter($reportOverviewID, $parameter, $value);
    }
    
    /**
     * Get all parameters by report overview ID.
     * @param type $reportOverviewID INT ID
     * @return type Array of Report Parameters Objects
     */
    public function getAllParametersByReportOverviewID($reportOverviewID)
    {
        return $this->reportParametersModel->getAllParametersByReportOverviewID($reportOverviewID);
    }
}
