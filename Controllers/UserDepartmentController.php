<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/UserDepartmentModel.php");
/**
 * Description of UserDepartmentController
 *
 * @author Archie
 */
class UserDepartmentController {
    public $userDepartmentModel;
    
    public function __construct()
    {
        $this->userDepartmentModel = new UserDepartmentModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * getUserDepartments
     * @return Array UserDepartment
     */
    public function getUserDepartments() {
        return $this->userDepartmentModel->getUserDepartments();
    }
    
    /**
     * getUserDepartmentByID
     * @param INT $deptID
     * @return UserDepartment
     */
    public function getUserDepartmentByID($deptID) {
        return $this->userDepartmentModel->getUserDepartmentByID($deptID);
    }
    
    /**
     * insertUserDepartment
     * @param type $department
     * @return type
     */
    public function insertUserDepartment($department) {
        return $this->userDepartmentModel->insertUserDepartment($department);
    }
}
