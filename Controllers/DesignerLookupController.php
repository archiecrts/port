<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/DesignerLookupModel.php");
/**
 * Description of DesignerLookupController
 *
 * @author Archie
 */
class DesignerLookupController {
    public $designerLookupModel;
    
    public function __construct()
    {
        $this->designerLookupModel = new DesignerLookupModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * getDesignerByVersionLookupID
     * @param type $versionLookupID
     * @return type
     */
    public function getDesignerByVersionLookupID($versionLookupID) {
        return $this->designerLookupModel->getDesignerByVersionLookupID($versionLookupID);
    }
    
    /**
     * getDesignerByDesignerID
     * @param type $designerID
     * @return type
     */
    public function getDesignerByDesignerID($designerID) {
        return $this->designerLookupModel->getDesignerByDesignerID($designerID);
    }
    /**
     * getDistinctDesigners
     * @return type
     */
    public function getDistinctDesigners() {
        return $this->designerLookupModel->getDistinctDesigners();
    }
    
    /**
     * getDistinctDesignersLeagueOnly
     * @return type
     */
    public function getDistinctDesignersLeagueOnly() {
        return $this->designerLookupModel->getDistinctDesignersLeagueOnly();
    }
    
    /**
     * getDistinctDesignersByTypeOfProductID
     * @param type $typeOfProductID
     * @return type
     */
    public function getDistinctDesignersByTypeOfProductID($typeOfProductID) {
        return $this->designerLookupModel->getDistinctDesignersByTypeOfProductID($typeOfProductID);
    }
    
    /**
     * getDistinctDesignersWithIntervals
     * @return type
     */
    public function getDistinctDesignersWithIntervals() {
        return $this->designerLookupModel->getDistinctDesignersWithIntervals();
    }
}
