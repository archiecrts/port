<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/LoadPriorityModel.php");
/**
 * Description of LoadPriorityController
 *
 * @author Archie
 */
class LoadPriorityController {
    public $loadPriorityModel;
    
    public function __construct()
    {
        $this->loadPriorityModel = new LoadPriorityModel();
    }
    
    public function invoke()
    {
        //include(page);
    }
    
    /**
     * getLoadPriorityByID
     * @param INT $loadPriorityID
     * @return type
     */
    public function getLoadPriorityByID($loadPriorityID)
    {
        return $this->loadPriorityModel->getLoadPriorityByID($loadPriorityID);
    }
    
    /**
     * getLoadPriorities
     * @return type
     */
    public function getLoadPriorities()
    {
        return $this->loadPriorityModel->getLoadPriorities();
    }
}
