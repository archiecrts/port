<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once("Models/CustomerContactsModel.php");
/**
 * Description of CustomerContactsController
 *
 * @author Archie
 */
class CustomerContactsController {
    public $customerContactsModel;
    
    public function __construct()
    {
        $this->customerContactsModel = new CustomerContactsModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * 
     * @param type $customerID
     * @return type
     */
    public function getContactsByCustomerID($customerID)
    {
        return $this->customerContactsModel->getContactsByCustomerID($customerID);
    }
    
    /**
     * getFirstContactByCustomerID
     * @param type $customerID
     * @return type
     */
    public function getFirstContactByCustomerID($customerID)
    {
        return $this->customerContactsModel->getFirstContactByCustomerID($customerID);
    }
    
    /**
     * 
     * @param type $customerID
     * @return \InstallationContacts
     */
    public function getActiveContactsByCustomerID($customerID)
    {
        return $this->customerContactsModel->getActiveContactsByCustomerID($customerID);
    }
    
    /**
     * 
     * @param type $contactID
     * @return type
     */
    public function getContactByID($contactID)
    {
        return $this->customerContactsModel->getContactByID($contactID);
    }
    
    
    /**
     * 
     * @param type $contactObject
     * @return type
     */
    public function updateContactDetails($contactObject) {
        return $this->customerContactsModel->updateContactDetails($contactObject);
    }
    
    
    /**
     * 
     * @param type $contactObject
     * @return type
     */
    public function setContact($contactObject) {
        return $this->customerContactsModel->setContact($contactObject);
    }
}
