<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/EnquiryModel.php");
/**
 * Description of EnquiryController
 *
 * @author Archie
 */
class EnquiryController {
    public $enquiryModel;
    
    public function __construct()
    {
        $this->enquiryModel = new EnquiryModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * createEnquiry
     * @param type $enquiry
     * @return type
     */
    public function createEnquiry($enquiry) {
        return $this->enquiryModel->createEnquiry($enquiry);
    }
    
    /**
     * getEnquiryByID
     * @param ID $addEnquiry
     * @return type
     */
    public function getEnquiryByID($addEnquiry) {
        return $this->enquiryModel->getEnquiryByID($addEnquiry);
    }
    
    /**
     * addEnquiryOurReference
     * @param INT $enquiryID
     * @param STRING $reference
     * @return boolean
     */
    public function addEnquiryOurReference($enquiryID, $reference) {
        return $this->enquiryModel->addEnquiryOurReference($enquiryID, $reference);
    }
}
