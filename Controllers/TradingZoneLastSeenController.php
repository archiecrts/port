<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/TradingZoneLastSeenModel.php");
/**
 * Description of TradingZoneLastSeenController
 *
 * @author Archie
 */
class TradingZoneLastSeenController {
    public $tradingZoneLastSeenModel;
    
    public function __construct()
    {
        $this->tradingZoneLastSeenModel = new TradingZoneLastSeenModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * getAllTradingZonesForInstallation
     * @param type $installationID
     * @return type
     */
    public function getAllTradingZonesForInstallation($installationID) {
        return $this->tradingZoneLastSeenModel->getAllTradingZonesForInstallation($installationID);
    }
}
