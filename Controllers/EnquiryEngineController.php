<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/EnquiryEngineModel.php");
/**
 * Description of EnquiryEngineController
 *
 * @author Archie
 */
class EnquiryEngineController {
    public $enquiryEngineModel;
    
    public function __construct()
    {
        $this->enquiryEngineModel = new EnquiryEngineModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    
    
    /**
     * createEnquiryEngine
     * @param Entity $engine
     * @return INT | null
     */
    public function createEnquiryEngine($engine) {
        return $this->enquiryEngineModel->createEnquiryEngine($engine);
    }
    
    /**
     * getEnquiryEnginesByEnquiryID
     * @param type $addEnquiry
     * @return type
     */
    public function getEnquiryEnginesByEnquiryID($addEnquiry) {
        return $this->enquiryEngineModel->getEnquiryEnginesByEnquiryID($addEnquiry);
    }
}
