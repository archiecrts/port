<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MarketResearchController
 *
 * @author Archie
 */
class MarketResearchController {
    public function __construct()
    {
        
    }
    /**
     * Decide which page to send the user to.
     */
    public function invoke()
    {
        $page="main";
        // This is checking that the users session has been created.
        // if not we force the redirect to access control.
        if (!isset($_SESSION['name'])) {
            $portal = "accessControl";
        } else {
            if(isset($_GET['page'])) {
                $page = $_GET['page'];
            }
        }
        switch ($page) {
            case "accessControl":
                include 'Views/accessControl.php';
                break;
            case "campaign":
                include 'Views/Campaigns/campaign.php';
                break;
            case "report":
                include 'Views/Reports/report.php';
                break;
            case "upload":
                include 'Views/Uploads/upload.php';
                break;
            case "uploadSeaweb":
                include 'Views/Uploads/uploadSeaWeb.php';
                break;
            case "updateSeaweb":
                include 'Views/Uploads/updateSeaweb.php';
                break;
            case "uploadFiles":
                include 'Views/Uploads/uploadBulkSeawebFilesToFolder.php';
                break;
            case "checkSeawebChanges":
                include 'Views/Uploads/seawebChanges.php';
                break;
            case "users":
                include 'Views/Users/users.php';
                break;
            case "countries":
                include 'Views/Countries/countries.php';
                break;
            case "addNewInstallation":
                include 'Views/AddInstallation/addNewInstallation.php';
                break;
            default:
                include 'Views/Reports/report.php';
        }    
    }
}
