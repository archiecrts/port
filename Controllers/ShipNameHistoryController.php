<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/ShipNameHistoryModel.php");
/**
 * Description of ShipNameHistoryController
 *
 * @author Archie
 */
class ShipNameHistoryController {
    public $shipNameHistoryModel;
    
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->shipNameHistoryModel = new ShipNameHistoryModel();
    }
    
    /**
     * Invoke Method.
     */
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * getAllShipNameHistoryForInstallation
     * @param type $installationID
     * @return type
     */
    public function getAllShipNameHistoryForInstallation($installationID) {
        return $this->shipNameHistoryModel->getAllShipNameHistoryForInstallation($installationID);
    }
}
