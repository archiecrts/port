<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of IntranetController
 *
 * @author Archie
 */
class IntranetController {
    public function __construct()
    {
        
    }
    /**
     * Decide which page to send the user to.
     */
    public function invoke()
    {
        include('Views/intranetMainPage.php');
    }
}
