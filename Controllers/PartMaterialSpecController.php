<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/PartMaterialSpecModel.php");
/**
 * Description of PartMaterialSpecController
 *
 * @author Archie
 */
class PartMaterialSpecController {
    public $partMaterialSpecModel;
    
    public function __construct()
    {
        $this->partMaterialSpecModel = new PartMaterialSpecModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * getMaterialSpecByID
     * @param type $materialID
     * @return type
     */
    public function getMaterialSpecByID ($materialID) {
        return $this->partMaterialSpecModel->getMaterialSpecByID($materialID);
    }
    
    /**
     * insertMaterialSpec
     * @param Object $spec
     * @return boolean or insert id
     */
    public function insertMaterialSpec ($spec) {
        return $this->partMaterialSpecModel->insertMaterialSpec($spec);
    }
    
    /**
     * getMaterialsFromCriteriaOptions
     * @param type $spec
     * @return type
     */
    public function getMaterialsFromCriteriaOptions ($spec) {
        return $this->partMaterialSpecModel->getMaterialsFromCriteriaOptions($spec);
    }
        
}
