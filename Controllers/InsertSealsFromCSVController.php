<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once("Models/InsertSealsFromCSVModel.php");
/**
 * Description of InsertSealsFromCSVController
 *
 * @author Archie
 */
class InsertSealsFromCSVController {
    
    public $insertSealsFromCSVModel;
    
    public function __construct()
    {
        $this->insertSealsFromCSVModel = new InsertSealsFromCSVModel();
    }
    
    public function invoke() {}
    
    /**
     * insertDataFromCSV
     */
    public function insertDataFromCSV($fileName) {
        return $this->insertSealsFromCSVModel->insertDataFromCSV($fileName);
    }
}
