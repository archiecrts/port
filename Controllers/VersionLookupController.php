<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/VersionLookupModel.php");
/**
 * Description of VersionLookupController
 *
 * @author Archie
 */
class VersionLookupController {
    public $versionLookupModel;
    
    public function __construct()
    {
        $this->versionLookupModel = new VersionLookupModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * getVersionIDBySeriesID
     * @param type $seriesLookupID
     * @return type
     */
    public function getVersionIDBySeriesID($seriesLookupID) {
        return $this->versionLookupModel->getVersionIDBySeriesID($seriesLookupID);
    }
    
    /**
     * getVersionByID
     * @param type $versionID
     * @return type
     */
    public function getVersionByID($versionID) {
        return $this->versionLookupModel->getVersionByID($versionID);
    }
}
