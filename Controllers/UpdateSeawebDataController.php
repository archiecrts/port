<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/UpdateSeawebDataModel.php");
/**
 * Description of UpdateSeawebDataController
 *
 * @author Archie
 */
class UpdateSeawebDataController {
    public $updateSeawebDataModel;
    
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->updateSeawebDataModel = new UpdateSeawebDataModel();
    }
    
    /**
     * Invoke Method.
     */
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * updateSeawebData
     * @param type $folder
     * @return type
     */
    public function updateSeawebData($folder) {
        return $this->updateSeawebDataModel->updateSeawebData($folder);
    }
    
    public function updateCustomerTable() {
        return $this->updateSeawebDataModel->updateCustomerTable();
    }
    
    public function updateShipDataTable() {
        return $this->updateSeawebDataModel->updateShipDataTable();
    }
    
    public function updateMainEngineTable() {
        return $this->updateSeawebDataModel->updateMainEngineTable();
    }
    
    public function updateAuxEngineTable() {
        return $this->updateSeawebDataModel->updateAuxEngineTable();
    }
    
    public function updateSurveyDateHistoryTable() {
        return $this->updateSeawebDataModel->updateSurveyDateHistoryTable();
    }
    
    public function updateSurveyDueDateTable() {
        return $this->updateSeawebDataModel->updateSurveyDueDateTable();
    }
    
    public function updateTradingZoneTable() {
        return $this->updateSeawebDataModel->updateTradingZoneTable();
    }
    
    public function updateNameHistoryTable() {
        return $this->updateSeawebDataModel->updateNameHistoryTable();
    }
}
