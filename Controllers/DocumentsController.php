<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/DocumentsModel.php");
/**
 * Description of PolicyController
 *
 * @author Archie
 */
class DocumentsController {
    public $documentsModel;
    
    public function __construct()
    {
        $this->documentsModel = new DocumentsModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    
    /**
     * Add Document adds a document details to the DB after the physical doc has been placed in the folder.
     * @param Documents Object $documentObject
     * @return boolean
     */
    public function addDocument($documentObject) {
        return $this->documentsModel->addDocument($documentObject);
    }
    
    /**
     * UpdateDocument changes the dates for a document.
     * @param Document $documentObject
     * @return boolean
     */
    public function updateDocument($documentObject) {
        return $this->documentsModel->updateDocument($documentObject);
    }
    
    
    /**
     * This returns an array of Document Objects.
     * @return boolean| Array of Document objects
     */
    public function getDocuments() {
        return $this->documentsModel->getDocuments();
    }
    
    /**
     * This returns an array of Document Objects.
     * @return boolean| Array of Document objects
     */
    public function getPolicyDocuments() {
        return $this->documentsModel->getPolicyDocuments();
    }
    
    /**
     * This returns an array of Document Objects.
     * @return boolean| Array of Document objects
     */
    public function getTrainingDocuments() {
        return $this->documentsModel->getTrainingDocuments();
    }
    
    /**
     * This returns an array of Document Objects.
     * @return boolean| Array of Document objects
     */
    public function getBrochureDocuments() {
        return $this->documentsModel->getBrochureDocuments();
    }
    
    
    /**
     * 
     * @param type $documentID
     * @return type
     */
    public function getDocumentByID($documentID) {
        return $this->documentsModel->getDocumentByID($documentID);
    }
}
