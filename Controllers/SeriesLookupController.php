<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("./Models/SeriesLookupModel.php");
/**
 * Description of SeriesLookupController
 *
 * @author Archie
 */
class SeriesLookupController {
    public $seriesLookupModel;
    
    public function __construct()
    {
        $this->seriesLookupModel = new SeriesLookupModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * getSeriesByVersionLookupID
     * @param type $versionLookupID
     * @return type
     */
    public function getSeriesByVersionLookupID($versionLookupID) {
       return $this->seriesLookupModel->getSeriesByVersionLookupID($versionLookupID);
    }
    
    /**
     * getSeriesByID
     * @param type $seriesID
     * @return type
     */
    public function getSeriesByID($seriesID) {
        return $this->seriesLookupModel->getSeriesByID($seriesID);
    }
    
    /**
     * getDistinctModelSeriesByDesigner
     * @param INT $versionLookupID
     * @param INT $designerID
     * @return Series Object
     */
    public function getDistinctModelSeriesByDesigner($designerID) {
        return $this->seriesLookupModel->getDistinctModelSeriesByDesigner($designerID);
    }
    
    /**
     * getDistinctModelSeries
     * @return type
     */
    public function getDistinctModelSeries() {
        return $this->seriesLookupModel->getDistinctModelSeries();
    }
    
    /**
     * getDistinctSeriesByCustomerID
     * @param type $customerID
     * @return type
     */
    public function getDistinctSeriesByCustomerID($customerID) {
        return $this->seriesLookupModel->getDistinctSeriesByCustomerID($customerID);
    }
        
}
