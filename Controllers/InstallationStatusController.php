<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/InstallationStatusModel.php");
/**
 * Description of InstallationStatusController
 *
 * @author Archie
 */
class InstallationStatusController {
    public $installationStatusModel;
    
    public function __construct()
    {
        $this->installationStatusModel = new InstallationStatusModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * getInstallationStatusByID
     * @param type $statusID
     * @return type
     */
    public function getInstallationStatusByID($statusID)
    {
        return $this->installationStatusModel->getInstallationStatusByID($statusID);
    }
    
    /**
     * getAllIstallationStatusObjects
     * @return type
     */
    public function getAllInstallationStatusObjects() {
        return $this->installationStatusModel->getAllInstallationStatusObjects();
    }
}
