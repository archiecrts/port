<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/SupplierPartsModel.php");
/**
 * Description of SupplierPartsController
 *
 * @author Archie
 */
class SupplierPartsController {
    public $supplierPartsModel;
    
    public function __construct()
    {
        $this->supplierPartsModel = new SupplierPartsModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    
    
    /**
     * getSupplierPartByID
     * @param type $partID
     * @return type
     */
    public function getSupplierPartByID($partID) {
        return $this->supplierPartsModel->getSupplierPartByID($partID);
    }
    
    /**
     * getPartsBySupplierID
     * @param type $supplierID
     * @return type
     */
    public function getPartsBySupplierID($supplierID) {
        return $this->supplierPartsModel->getPartsBySupplierID($supplierID);
    }
    
    /**
     * getAllSuppliersByPartCodeID
     * @param type $partCodeID
     * @return type
     */
    public function getAllSuppliersByPartCodeID($partCodeID) {
        return $this->supplierPartsModel->getAllSuppliersByPartCodeID($partCodeID);
    }
    
    
    /**
     * getAllProductVersionsOfSupplier
     * @param type $supplierID
     * @return type
     */
    public function getAllProductVersionsOfSupplier($supplierID) {
        return $this->supplierPartsModel->getAllProductVersionsOfSupplier($supplierID);
    }
    
    
    /**
     * setSupplierPart
     * @param type $supplierPartObject
     * @return type
     */
    public function setSupplierPart($supplierPartObject) {
        return $this->supplierPartsModel->setSupplierPart($supplierPartObject);
    }
        
}
