<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/SupplierApprovalLookupModel.php");
/**
 * Description of SupplierApprovalStatusLookupController
 *
 * @author Archie
 */
class SupplierApprovalStatusLookupController {
    
    
    public $supplierApprovalLookupModel;
    
    public function __construct()
    {
        $this->supplierApprovalLookupModel = new SupplierApprovalLookupModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * getSupplierApprovalLookupByID
     */
    public function getSupplierApprovalLookupByID($lookupID) {
        return $this->supplierApprovalLookupModel->getSupplierApprovalLookupByID($lookupID);
    }
}
