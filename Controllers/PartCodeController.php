<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/PartCodeModel.php");
/**
 * Description of PartCodeController
 *
 * @author Archie
 */
class PartCodeController {
    public $partCodeModel;
    
    public function __construct()
    {
        $this->partCodeModel = new PartCodeModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    
    /**
     * getPartCodeByID
     * @param type $partID
     * @return type
     */
    public function getPartCodeByID($partID) {
        return $this->partCodeModel->getPartCodeByID($partID);
    }
    
    /**
     * getAllPartCodes
     * @return type
     */
    public function getAllPartCodes() {
        return $this->partCodeModel->getAllPartCodes();
    }
    
    /**
     * insertPartCode
     * @param type $partCode
     * @return boolean
     */
    public function insertPartCode($partCode) {
        return $this->partCodeModel->insertPartCode($partCode);
    }
    
    /**
     * getPartCodeByCriteriaOptions
     * @param type $partCode
     * @return type
     */
    public function getPartCodeByCriteriaOptions($partCode) {
        return $this->partCodeModel->getPartCodeByCriteriaOptions($partCode);
    }
}
