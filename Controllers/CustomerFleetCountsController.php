<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/CustomerFleetCountsModel.php");
/**
 * Description of CustomerFleetCountsController
 *
 * @author Archie
 */
class CustomerFleetCountsController {
    public $customerFleetCountsModel;
    
    public function __construct()
    {
        $this->customerFleetCountsModel = new CustomerFleetCountsModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * getCustomerFleetCountsByCustomerID
     * @param type $customerID
     * @return type
     */
    public function getCustomerFleetCountsByCustomerID($customerID) {
        return $this->customerFleetCountsModel->getCustomerFleetCountsByCustomerID($customerID);
    }
}
