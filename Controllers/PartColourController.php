<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/PartColourModel.php");
/**
 * Description of PartColourController
 *
 * @author Archie
 */
class PartColourController {
    public $partColourModel;
    
    public function __construct()
    {
        $this->partColourModel = new PartColourModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * getPartColourByID
     * @param type $colourID
     * @return type
     */
    public function getPartColourByID($colourID) {
        return $this->partColourModel->getPartColourByID($colourID);
    }
    
    /**
     * getAllPartColours
     * @return Array
     */
    public function getAllPartColours() {
        return $this->partColourModel->getAllPartColours();
    }
}
