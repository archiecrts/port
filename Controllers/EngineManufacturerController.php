<?php

include_once("Models/EngineManufacturerModel.php");
/**
 * Description of EngineManufacturerController
 *
 * @author Archie
 */
class EngineManufacturerController {
    public $engineManufacturerModel;
    
    
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->engineManufacturerModel = new EngineManufacturerModel();
    }
    
    /**
     * Invoke Method.
     */
    public function invoke()
    {
        //include(page);
        
    }
    
    
    /**
     * 
     * @return type
     */
    public function getDistinctEngineManufacturers()
    {
        return $this->engineManufacturerModel->getDistinctEngineManufacturers();
    }
    
    
    /**
     * 
     * @return type
     */
    public function getDistinctAuxEngineManufacturers()
    {
        return $this->engineManufacturerModel->getDistinctAuxEngineManufacturers();
    }
    
    
    /**
     * 
     * @return type
     */
    public function getDistinctEngineBuildersWithIntervals() {
        return $this->engineManufacturerModel->getDistinctEngineBuildersWithIntervals();
    }
    
    
    /**
     * 
     * @return type
     */
    public function getEngineBuilders() {
        return $this->engineManufacturerModel->getEngineBuilders();
    }
    
    
    /**
     * 
     * @param type $engineBuilder
     * @return type
     */
    public function setEngineBuilder($engineBuilder) {
        return $this->engineManufacturerModel->setEngineBuilder($engineBuilder);
    }
    
    
    /**
     * 
     * @param type $manufacturerID
     * @param type $updatedManufacturer
     * @return type
     */
    public function updateEngineBuilder($manufacturerID, $updatedManufacturer) {
        return $this->engineManufacturerModel->updateEngineBuilder($manufacturerID, $updatedManufacturer);
    }
    
    /**
     * getEngineBuilderByID
     * @param type $engineBuilderID
     * @return type
     */
    public function getEngineBuilderByID($engineBuilderID) {
        return $this->engineManufacturerModel->getEngineBuilderByID($engineBuilderID);
    }
}
