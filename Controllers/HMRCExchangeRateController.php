<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/HMRCExchangeRateModel.php");
/**
 * Description of HMRCExchangeRateController
 *
 * @author Archie
 */
class HMRCExchangeRateController {
    public $hmrcExchangeRateModel;
    
    public function __construct()
    {
        $this->hmrcExchangeRateModel = new HMRCExchangeRateModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * getAllExchangeRates
     * @return array of exchange rates
     */
    public function getAllExchangeRates() {
        return $this->hmrcExchangeRateModel->getAllExchangeRates();
    }
    
    /**
     * getExchangeRateByCountryName
     * @param STRING $countryName
     * @return Exchange Rate
     */
    public function getExchangeRateByCountryName($countryName) {
        return $this->hmrcExchangeRateModel->getExchangeRateByCountryName($countryName);
    }
    
    /**
     * loadExchangeRateCSV
     * @param Exchange Rate $exchangeRate
     * @return INT | null
     */
    public function loadExchangeRateCSV($fileName) {
        return $this->hmrcExchangeRateModel->loadExchangeRateCSV($fileName);
    }
    
    /**
     * truncateTable
     * This removes all rows and resets the index back to 1.
     * @return boolean
     */
    public function truncateTable() {
        return $this->hmrcExchangeRateModel->truncateTable();
    }
        
}
