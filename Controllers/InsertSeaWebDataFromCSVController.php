<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/InsertSeaWebDataFromCSVModel.php");
/**
 * Description of InsertSeaWebDataFromCSVController
 *
 * @author Archie
 */
class InsertSeaWebDataFromCSVController {
    public $insertInstallationsFromCSVModel;
    
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->insertInstallationsFromCSVModel = new InsertSeaWebDataFromCSVModel();
    }
    
    /**
     * Invoke Method.
     */
    public function invoke()
    {
        //include(page);
        
    }
    
    
    /**
     * importSeaWebDataFromCSV
     * @param type $fileName
     * @param type $columnNameArray
     * @param type $headers
     * @param type $fileType
     * @return type
     */
    public function importSeaWebDataFromCSV($fileName, $columnNameArray, $headers, $fileType) {
        return $this->insertInstallationsFromCSVModel->importSeaWebDataFromCSV($fileName, $columnNameArray, $headers, $fileType);
    }
}
