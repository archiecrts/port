<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/CustomerHasInstallationsModel.php");
/**
 * Description of CustomerHasInstallationsController
 *
 * @author Archie
 */
class CustomerHasInstallationsController {
   
    public $customerHasInstallationsModel;
    
    public function __construct()
    {
        $this->customerHasInstallationsModel = new CustomerHasInstallationsModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * setNewCustomerHasInstallationsRecord
     * @param type $customerID
     * @param type $installationID
     * @return type
     */
    public function setNewCustomerHasInstallationsRecord($customerID, $installationID) {
        return $this->customerHasInstallationsModel->setNewCustomerHasInstallationsRecord($customerID, $installationID);
    }
    
    /**
     * updateCustomerHasInstallationsRecord
     * @param type $customerID
     * @param type $installationID
     * @param type $newCustomerHasInstRecord
     * @return type
     */
    public function updateCustomerHasInstallationsRecord($customerID, $installationID, 
            $newCustomerHasInstRecord) {
        return $this->customerHasInstallationsModel->updateCustomerHasInstallationsRecord(
                $customerID, $installationID, $newCustomerHasInstRecord);
    }
    
    /**
     * getCustomerIDFromInstallationID
     * @param type $installationID
     * @return type
     */
    public function getCustomerIDFromInstallationID($installationID) {
        return $this->customerHasInstallationsModel->getCustomerIDFromInstallationID($installationID);
    }
    
    /**
     * getInstallationIDArrayFromCustomerID
     * @param type $customerID
     * @return type
     */
    public function getInstallationIDArrayFromCustomerID($customerID) {
        return $this->customerHasInstallationsModel->getInstallationIDArrayFromCustomerID($customerID);
    }
}
