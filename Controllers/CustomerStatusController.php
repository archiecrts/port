<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/CustomerStatusModel.php");
/**
 * Description of CustomerStatusController
 *
 * @author Archie
 */
class CustomerStatusController {
    public $customerStatusModel;
    
    public function __construct()
    {
        $this->customerStatusModel = new CustomerStatusModel();
    }
    
    public function invoke() {}
    
    /**
     * getCustomerStatusByID
     * @param INT $customerStatusID
     * @return Customer Status Object
     */
    public function getCustomerStatusByID($customerStatusID) {
        return $this->customerStatusModel->getCustomerStatusByID($customerStatusID);
    }
    
    /**
     * getAllCustomerStatusValuesByCustomerID
     * @param INT $customerID
     * @return Array of Customer Status Objects
     */
    public function getAllCustomerStatusValuesByCustomerID($customerID) {
        return $this->customerStatusModel->getAllCustomerStatusValuesByCustomerID($customerID);
    }
    
    
    /**
     * setNewCustomerStatusObject
     * @param Customer Status Object $statusObject
     * @return INT
     */
    public function setNewCustomerStatusObject($statusObject) {
        return $this->customerStatusModel->setNewCustomerStatusObject($statusObject);
    }
    
    
    /**
     * deleteCustomerStatusObjectsByStatusID
     * @param INT $statusID
     * @return boolean
     */
    public function deleteCustomerStatusObjectsByStatusID($statusID) {
        return $this->customerStatusModel->deleteCustomerStatusObjectsByStatusID($statusID);
    }
    
    
    /**
     * deleteCustomerStatusObjectsByCustomerID
     * @param INT $customerID
     * @return boolean
     */
    public function deleteCustomerStatusObjectsByCustomerID($customerID) {
        return $this->customerStatusModel->deleteCustomerStatusObjectsByCustomerID($customerID);
    }
        
}
