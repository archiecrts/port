<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/CountryModel.php");
/**
 * Description of CountryController
 *
 * @author Archie
 */
class CountryController {
    public $countryModel;
    
    public function __construct()
    {
        $this->countryModel = new CountryModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    public function getCountries() {
        return $this->countryModel->getCountries();
    }
    
    
    public function getDistinctAreas() 
    {
        return $this->countryModel->getDistinctAreas();
    }
    
    /**
     * getDistinctContinents
     * @return type
     */
    public function getDistinctContinents() 
    {
        return $this->countryModel->getDistinctContinents();
    }
    
    public function setAllCountries($fileName) {
        return $this->countryModel->setAllCountries($fileName);
    }
    
    /*public function setAreas() {
        return $this->countryModel->setAreas();
    }*/
    
    public function getTimeZoneByCountryName($countryName) {
        return $this->countryModel->getTimeZoneByCountryName($countryName);
    }
}
