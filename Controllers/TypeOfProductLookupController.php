<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/TypeOfProductLookupModel.php");
/**
 * Description of TypeOfProductLookupController
 *
 * @author Archie
 */
class TypeOfProductLookupController {
    public $typeOfProductLookupModel;
    
    public function __construct()
    {
        $this->typeOfProductLookupModel = new TypeOfProductLookupModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * getTypeOfProductByID
     * @param type $toplID
     * @return type
     */
    public function getTypeOfProductByID($toplID) {
        return $this->typeOfProductLookupModel->getTypeOfProductByID($toplID);
    }
    
    /**
     * getAllProductTypes
     * @return type
     */
    public function getAllProductTypes() {
        return $this->typeOfProductLookupModel->getAllProductTypes();
    }
}
