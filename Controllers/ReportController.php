<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once ("Models/ReportModel.php");
include_once("Models/InstallationModel.php");
/**
 * Description of ReportController
 *
 * @author Archie
 */
class ReportController {
    public $reportModel;
    private $installationModel;
    
    public function __construct()
    {
        $this->reportModel = new ReportModel();
        $this->installationModel = new InstallationModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * This will set the report participants ID into the report table.
     * Thus creating a snapshot of the report.
     * @param INT $installationID
     * @param INT $reportOverviewID
     * @return boolean | mysql error
     */
    public function setReportParticipant($installationID, $reportOverviewID) {
        return $this->reportModel->setReportParticipant($installationID, $reportOverviewID);
    }
    
    
    
    /**
     * This will check each installation to see if it appears on any campaigns in the past three months
     * @param INT $installationID
     * @return INT | NULL
     */
    public function checkInstallationForPreviousCampaignInclusion($installationID, $overviewID, $overviewDate) {
        return $this->reportModel->checkInstallationForPreviousCampaignInclusion($installationID, $overviewID, $overviewDate);
    }
    
    
    /**
     * This will return a set list of installations created at the time of a report creation event
     * @param INT $reportOverviewID
     * @return \Installation
     */
    public function getAllInstallationsByReportOverviewID($reportOverviewID) {
        return $this->reportModel->getAllInstallationsByReportOverviewID($reportOverviewID);
    }
    
    /**
     * This uses previously created functions to decide which campaign IDs to remove.
     * @param INT $campaignOverviewID
     * @param DATETIME $createdDate
     * @param BOOLEAN $orange
     * @param BOOLEAN $red
     * @return boolean TRUE
     */
    public function removeInstallationFromCurrentCampaign($reportOverviewID, $createdDate, $orange, $red) {
        return $this->reportModel->removeInstallationFromCurrentCampaign($reportOverviewID, $createdDate, $orange, $red);
    }
        
}
