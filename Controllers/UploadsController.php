<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */

/**
 * Description of UploadsController
 *
 * @author Archie
 */
class UploadsController {
    public function __construct()
    {
        
    }
    /**
     * Decide which page to send the user to.
     */
    public function invoke()
    {
        $page="main";
        // This is checking that the users session has been created.
        // if not we force the redirect to access control.
        if (!isset($_SESSION['name'])) {
            $portal = "accessControl";
        } else {
            if(isset($_GET['page'])) {
                $page = $_GET['page'];
            }
        }
        switch ($page) {
            case "upload":
                include 'Views/Uploads/upload.php';
                break;
            case "uploadSeaweb":
                include 'Views/Uploads/uploadSeaWeb.php';
                break;
            case "updateSeaweb":
                include 'Views/Uploads/updateSeaweb.php';
                break;
            case "checkSeawebChanges":
                
                if ((reset($_POST) == "savecustomer") || (reset($_POST) == 'saveaddress')) {
                    print "SAVE OR DELETE Customer";
                    foreach ($_POST as $key => $action) {
                        if ($key != 'submitCustomer') {
                            $exp = explode("_", $key);
                            $recordID = $exp[1];
                            include 'Views/Uploads/saveDeleteUpdateChanges.php';
                        }
                    }
                }
                if (isset($_POST["postShip"])) {
                    print "SAVE OR DELETE SHIP";
                    foreach ($_POST as $key => $action) {
                        if ($key != 'submitShip') {
                            $exp = explode("_", $key);
                            if (isset ($exp[1])) {
                                $recordID = $exp[1];
                            }
                            include 'Views/Uploads/saveDeleteUpdateChanges.php';
                        }
                    }
                }
                include 'Views/Uploads/seawebChanges.php';
                break;
            case "uploadSeals":
                include 'Views/Uploads/uploadSealsData.php';
                break;
            case "uploadOWS":
                include 'Views/Uploads/uploadOWSData.php';
                break;
            default:
                include 'Views/Uploads/upload.php';
        }    
    }
}
