<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/GetInstallationsArrayModel.php");
/**
 * Description of getInstallationsArrayModel
 *
 * @author Archie
 */
class GetInstallationsArrayController {
    public $getInstallationsArrayModel;
    
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->getInstallationsArrayModel = new GetInstallationsArrayModel();
    }
    
    /**
     * Invoke Method.
     */
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * @param type $continent
     * @param type $area
     * @param type $country
     * @param type $salesManager
     * @param type $engineManufacturer
     * @param type $engineModel
     * @param type $actionDue
     * @param type $lastCalled
     * @param type $actionType
     * @param type $productType
     * @return type
     */
    public function getInstallationsArray($continent, $area, $country, $salesManager, 
            $engineManufacturer, $engineModel, $mainOrAux,
            $actionDueFrom, $actionDueUntil, $lastCalledFrom, $lastCalledUntil,
            $actionType, $productType, $instSearchName, $companySearchName,
            $ssFromDate, $ssToDate) 
    {
        
        return $this->getInstallationsArrayModel->getInstallationsArray($continent, $area, $country, $salesManager, 
            $engineManufacturer, $engineModel, $mainOrAux,
            $actionDueFrom, $actionDueUntil, $lastCalledFrom, $lastCalledUntil,
            $actionType, $productType, $instSearchName, $companySearchName,
            $ssFromDate, $ssToDate);
    }
}
