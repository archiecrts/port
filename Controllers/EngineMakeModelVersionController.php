<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/EngineMakeModelVersionModel.php");
/**
 * Description of EngineMakeModelVersionController
 *
 * @author Archie
 */
class EngineMakeModelVersionController {
    public $engineMakeModelVersionModel;
    
    
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->engineMakeModelVersionModel = new EngineMakeModelVersionModel();
    }
    
    /**
     * Invoke Method.
     */
    public function invoke()
    {
        //include(page);
        
    }
    
    
    /**
     * 
     * @return type
     */
    public function getAllEngines() {
        return $this->engineMakeModelVersionModel->getAllEngines();
    }
    
    /**
     * 
     * @param type $engineVersionID
     * @return type
     */
    public function getCountOfAssociatedServiceIntervals($engineVersionID) {
        return $this->engineMakeModelVersionModel->getCountOfAssociatedServiceIntervals($engineVersionID);
    }
    
    
    /**
     * 
     * @param type $versionID
     * @return type
     */
    public function getEngineByVersionID($versionID) {
        return $this->engineMakeModelVersionModel->getEngineByVersionID($versionID);
    }
}
