<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/EnquirySourceModel.php");
/**
 * Description of EnquirySourceController
 *
 * @author alexandra
 */
class EnquirySourceController {
public $enquirySourceModel;
    
    public function __construct()
    {
        $this->enquirySourceModel = new EnquirySourceModel();
    }
    
    public function invoke()
    {
        
    }

     /**
     * getAllEnquirySources
     * @return Array | null
     */
    public function getAllEnquirySources() {
        return $this->enquirySourceModel->getAllEnquirySources();
    }
    
    /**
     * getEnquirySourceByID
     * @param INT $sourceID
     * @return array | null
     */
    public function getEnquirySourceByID($sourceID) {
        return $this->enquirySourceModel->getEnquirySourceByID($sourceID);
    }
}
