<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/CustomerAddressModel.php");
/**
 * Description of CustomerAddressController
 *
 * @author Archie
 */
class CustomerAddressController {
    
    public $customerAddressModel;
    
    public function __construct()
    {
        $this->customerAddressModel = new CustomerAddressModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
     /**
     * Get CustomerAddress Record by Customer ID.
     * @param INT $customerID
     * @return CustomerAddress Object
     */
    public function getDefaultCustomerAddressByCustomerID($customerID) {
        return $this->customerAddressModel->getDefaultCustomerAddressByCustomerID($customerID);
    }
    
    
    /**
     * getCustomerAddressesByCustomerID
     * @param INT $customerID
     * @return Array of Customer Address Objects
     */
    public function getCustomerAddressesByCustomerID($customerID) {
        return $this->customerAddressModel->getCustomerAddressesByCustomerID($customerID);
    }
    
    /**
     * getCustomerAddressByID
     * @param INT $addressID
     * @return OBJECT
     */
    public function getCustomerAddressByID($addressID) {
        return $this->customerAddressModel->getCustomerAddressByID($addressID);
    }
    
    /**
     * updateCustomerAddress
     * @param Entity Object $customerAddressObject
     * @return boolean
     */
    public function updateCustomerAddress($customerAddressObject) {
        return $this->customerAddressModel->updateCustomerAddress($customerAddressObject);
    }
    
    /**
     * setCustomerAddress
     * @param Object $customerAddressObject
     * @return string
     */
    public function setCustomerAddress($customerAddressObject) {
        return $this->customerAddressModel->setCustomerAddress($customerAddressObject);
    }
}
