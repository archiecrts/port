<?php 

require_once 'Helpers/charsetFunctions.php';
?>
<script type="text/javascript" src="./js/campaignParametersForm.js"></script> 
<script>
    $(function () {

        $('#majorDescription').change(function (e) {

            //Grab the chosen value on first select list change
            var selectvalue = $(this).val();

            //Display 'loading' status in the target select list
            $('#minorDescription').html('<option value="">Loading...</option>');


            //Make AJAX request, using the selected value as the GET

            $.ajax({url: 'Views/Parts/getMinorDescriptionAjax.php?majorArray=' + selectvalue,
                success: function (output) {

                    $('#minorDescription').html(output);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + " " + thrownError);
                }
            });

        });
        
        // dim changes
        $('.dimValues').change(function (e) {

            //Grab the chosen value on first select list change
            var odValue = $('#dimOD').val();
            var pdValue = $('#dimPD').val();
            var dimLength = $('#dimLength').val();
            var dimDIN = $('#dimDIN').val();
            var dimHeadType = $('#dimHeadType').val();
            var dimAngle = $('#dimAngle').val();

            //Make AJAX request, using the selected value as the GET

            $.ajax({url: 'Views/Parts/searchUniqueDimensionAjax.php?dimOD=' + odValue + '&dimPD=' + pdValue + '&dimLength=' + dimLength + '&dimDIN=' + dimDIN + '&dimHeadType=' + dimHeadType + '&dimAngle=' + dimAngle,
                success: function (output) {
                    //alert(output);
                    $('#dimOptions').html(output);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + " " + thrownError);
                }
            });

        });
        
         // dim changes
        $('.specValues').change(function (e) {

            //Grab the chosen value on first select list change
            var materialCode = $('#materialCode').val();
            var materialDesc = $('#materialDesc').val();
            var materialHardness = $('#materialHardness').val();
            var materialDIN = $('#materialDIN').val();
            var materialSpec = $('#materialSpec').val();

            //Make AJAX request, using the selected value as the GET

            $.ajax({url: 'Views/Parts/searchUniqueMaterialSpecsAjax.php?materialCode=' + materialCode + '&materialDesc=' + materialDesc + '&materialHardness=' + materialHardness + '&materialDIN=' + materialDIN + '&materialSpec=' + materialSpec,
                success: function (output) {
                    //alert(output);
                    $('#specOptions').html(output);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + " " + thrownError);
                }
            });

        });
        
        
        $('#costs').click(function (e) {
            if ($('#stockBlock').css("display") == "none") {
                $('#costs').html("Hide Stock and Cost");
                $('#stockBlock').show();
            } else {
                $('#costs').html("Show Stock and Cost (optional data)");
                $('#stockBlock').hide();
                $('#totalStock').val("");
                $('#minStock').val("");
                $('#reservedStock').val("");
                $('#freeStockQty').val("");
                $('#onOrderQty').val("");
                $('#reservedForKits').val("");
                //$('#preferredSupplier').val("");
                $('#costPrice').val("");
                $('#costPriceSetDate').val("");
            }
        });
        
        
        $('#league').change(function () {
           if ( $('#league').is(':checked') ) {
                $.ajax({url: 'Views/Reports/getLeagueDesignersAjax.php?',
                        success: function (output) {
                           // alert(output);
                            $("#select1").html(output);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.status + " " + thrownError);
                        }});
                    
                var designerArray = "";
                $("#select2 option").each(function() {
                    designerArray += $(this).val()+",";
                });

                
                $.ajax({url: 'Views/Reports/getSelectedSeriesAjax.php?designerArray=' + designerArray + '&league=1',
                        success: function (output) {
                            //alert(output);
                            $("#model1").html(output);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.status + " " + thrownError);
                        }});
            } else {
                $.ajax({url: 'Views/Reports/getAllDesignersAjax.php?',
                        success: function (output) {
                           // alert(output);
                            $("#select1").html(output);
                            
                            var designerArray = "";
                            $("#select2 option").each(function() {
                                designerArray += $(this).val()+",";
                            });
                            //alert(designerArray);

                            $.ajax({url: 'Views/Reports/getSelectedSeriesAjax.php?designerArray=' + designerArray + '&league=0',
                                    success: function (output) {
                                        //alert(output);
                                        $("#model1").html(output);
                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {
                                        alert(xhr.status + " " + thrownError);
                                    }});
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.status + " " + thrownError);
                        }});
            }
       });
        
        // add the series options when designers are chosen.
        $("#add").click(function() {
            var leagueOnly = '0';
            if ( $('#league').is(':checked') ) {
                leagueOnly = '1';
            } 
            var designerArray = JSON.stringify($( "#select2" ).val());
            
            var newdesignerArray = designerArray.replace("&", "-amp-");
            var unstring = JSON.parse(newdesignerArray);

            $.ajax({url: 'Views/Parts/getSelectedSeriesAjax.php?designerArray=' + unstring + '&league=' + leagueOnly,
                        success: function (output) {
                            //alert(output);
                            $("#model1").html(output);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.status + " " + thrownError);
                        }});
        });
        
        $("#remove").click(function() {
            var leagueOnly = '0';
            if ( $('#league').is(':checked') ) {
                leagueOnly = '1';
            } 
            var designerArray = "";
            $("#select2 option").each(function() {
                designerArray += $(this).val()+",";
            });
            designerArray = designerArray.slice(0,-1);
            $.ajax({url: 'Views/Reports/getSelectedSeriesAjax.php?designerArray=' + designerArray + '&league=' + leagueOnly,
                        success: function (output) {
                            $("#model1").html(output);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.status + " " + thrownError);
                        }});
        });
        
    });
    
</script>


<h3>Add Part to Database</h3>

<form name="form" action="index.php?portal=parts&page=addPart&action=save" method="POST" enctype="multipart/form-data">
    <table>
        <tr>
            <td style="vertical-align: top;" rowspan="3">
                <table>
                    <tr>
                        <td>Component Part Number</td><td><input type="text" name="partCode" /></td>
                    </tr>
                    <tr>
                        <td>LD System Part Number</td><td><input type="text" name="ldPartNumber" /></td>
                    </tr>
                    <tr>
                        <td>Description Major</td>
                        <td><select id='majorDescription' name="majorDescription">
                                <option value="">Select...</option>
<?php
$majorDescription = $partDescriptionLookupController->getAllMajorDescriptions();

foreach ($majorDescription as $key => $object) {
    print "<option value='$object->majorType'>$object->majorType</option>";
}
?>
                            </select></td>
                    </tr>
                    <tr>
                        <td>Description Minor</td>
                        <td><select id='minorDescription' name="minorDescription">
                                <option value="">Select a major option first</option>
                            </select></td>
                    </tr>
                    <tr>
                        <td>Description for invoice</td><td><input type="text" name="description" /></td>
                    </tr>
                    <tr>
                        <td>Weight (kg)</td><td><input type="text" name="weight" /></td>
                    </tr>
                    <tr>
                        <td>Colour</td>
                        <td><select name="colour">
                                <option value="">Select...</option>
<?php
$colourArray = $partColourController->getAllPartColours();
foreach ($colourArray as $key => $value) {
    print "<option value='" . $key . "'>$value->partColour</option>";
}
?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Wear item</td>
                        <td><select name="wearItem" >
                                <option value="Y">Yes</option>
                                <option value="N">No</option>
                                <option value="U">Unsure</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-weight:bold;">Dimensions</td>
                    </tr>
                    <tr>
                        <td>Dimensions Outer Diameter (mm)</td><td><input type="text" id="dimOD" name="dimOD" class="dimValues" /></td>
                    </tr>
                    <tr>
                        <td>Dimensions Inner Diameter (mm)</td><td><input type="text" id="dimID" name="dimID" class="dimValues" /></td>
                    </tr>
                    <tr>
                        <td>Dimensions Profile Diameter (mm)</td><td><input type="text" id="dimPD" name="dimPD" class="dimValues" /></td>
                    </tr>
                    <tr>
                        <td>Dimensions Length(mm)</td><td><input type="text" id="dimLength" name="dimLength" class="dimValues" /></td>
                    </tr>
                    <tr>
                        <td>Dimensions DIN Number</td><td><input type="text" id="dimDIN" name="dimDIN" class="dimValues" /></td>
                    </tr>
                    <tr>
                        <td>Dimensions Head Type</td><td><input type="text" id="dimHeadType" name="dimHeadType" class="dimValues" /></td>
                    </tr>
                    <tr>
                        <td>Dimensions Valve/Seat Angle (Degrees)</td><td><input type="text" id="dimAngle" name="dimAngle" class="dimValues" /></td>
                    </tr>
                    <tr>
                        <td colspan='2' style="font-weight:bold;">Material Details</td>
                    </tr>
                    <tr>
                        <td>Material Code</td><td><input type="text" id="materialCode" name="materialCode" class="specValues" /> *</td>
                    </tr>
                    <tr>
                        <td>Material Description</td><td><input type="text" id="materialDesc" name="materialDesc" class="specValues"  /> *</td>
                    </tr>
                    <tr>
                        <td>Material Hardness</td><td><input type="text" id="materialHardness" name="materialHardness" class="specValues"  /></td>
                    </tr>
                    <tr>
                        <td>Material DIN Standard</td><td><input type="text" id="materialDIN" name="materialDIN" class="specValues"  /></td>
                    </tr>
                    <tr>
                        <td>Material Spec.</td><td><input type="text" id="materialSpec" name="materialSpec" class="specValues"  /></td>
                    </tr>
                </table>


                <p><button type="button" name="costs" id="costs">Show Stock and Cost (optional data)</button></p>
                <!-- Stock and Costs -->
                <div id="stockBlock" style='display:none;'>

                    <table>
                        <tr>
                            <td colspan="2" style='font-weight: bold;'>Stock and Costs</td>
                        </tr>
                        <tr>
                            <td>Stock Item</td>
                            <td>
                                <select name='stockItem'>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Total Stock</td><td><input type="text" id="totalStock" name="totalStock" /></td>
                        </tr>
                        <tr>
                            <td>Min. Stock</td><td><input type="text" id="minStock" name="minStock" /></td>
                        </tr>
                        <tr>
                            <td>Reserved Stock</td><td><input type="text" id="reservedStock" name="reservedStock" /></td>
                        </tr>
                        <tr>
                            <td>Free Stock Qty.</td><td><input type="text" id="freeStockQty" name="freeStockQty" /></td>
                        </tr>
                        <tr>
                            <td>On Order Qty.</td><td><input type="text" id="onOrderQty" name="onOrderQty" /></td>
                        </tr>
                        <tr>
                            <td>Reserved for kits</td><td><input type="text" id="reservedForKits" name="reservedForKits" /></td>
                        </tr>
                        <!--<tr>
                            <td>Preferred supplier</td><td><input type="text" id="preferredSupplier" name="preferredSupplier" /></td>
                        </tr>-->
                        <tr>
                            <td>Cost price</td><td><input type="text" id="costPrice" name="costPrice" /></td>
                        </tr>
                        <tr>
                            <td>Cost price set date</td><td><input type="text" id="costPriceSetDate" name="costPriceSetDate" /></td>
                        </tr>
                    </table>
                </div>
                <input type="submit" name="submit" value="Save" disabled='disabled'/></td>
            <td style="vertical-align: top;"><span style="font-weight: bold;">Add to engine</span><br/>
            <Table>
                <tr>
                    <td>STC League Only</td>
                    <td><input type="checkbox" id='league' name="league" value="1" checked="checked"/></td>
                </tr>
                    <tr>
                        <td><select name="allEngines[]" multiple size="6" class='multiSelect' id='select1'>
                                <?php
                                $engineManufacturers = $designerLookupController->getDistinctDesignersLeagueOnly();

                                foreach ($engineManufacturers as $name => $engine) {
                                    if ($engine->designerDescription != "") {
                                        echo '<option value="' . htmlXentities($engine->designerDescription) . '">' . htmlXentities($engine->designerDescription) . '</option>';
                                    }
                                }
                                ?>      

                            </select>
                        </td>
                        <td><div class='multiSelectButtons'>
                                <table>
                                    <tr>
                                        <td><button id="add" type="button"> > </button></td>
                                    </tr>
                                    <tr>
                                        <td><button id="remove" type="button"> < </button></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td><select name="engineDesign[]" multiple size='6' class='multiSelect' id="select2">
                                <?php
                                if (isset($_POST['parameterArray']['engineManufacturer'])) {
                                    foreach ($_POST['parameterArray']['engineManufacturer'] as $number => $designer) {
                                        print "<option value = '".$designer."' selected = 'selected'>".$designer."</option>";
                                    }
                                }
                                
                                ?>
                            </select></td>
                    </tr>
                </Table>
                <Table>
                    <tr>
                        <td><select name="allModels[]" multiple size="6" class='multiSelect' id='model1'>
                               
                            </select>
                        </td>
                        <td><div class='multiSelectButtons'>
                                <table>
                                    <tr>
                                        <td><button id="addModel" type="button"> > </button></td>
                                    </tr>
                                    <tr>
                                        <td><button id="removeModel" type="button"> < </button></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td><select name="engineSeries[]" multiple size='6' class='multiSelect' id="model2">
                                <?php
                                if (isset($_POST['parameterArray']['engineModel'])) {
                                    foreach ($_POST['parameterArray']['engineModel'] as $number => $series) {
                                        print "<option value = '".$series."' selected = 'selected'>".$series."</option>";
                                    }
                                }
                                
                                ?>
                            </select></td>
                    </tr>
                    <tr><td colspan="3"></td></tr>
                    <tr><td colspan='3'>Quantity on engine: <input type="text" name="quantity" value="1"/></td></tr>
                </Table>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top;"><span style="font-weight: bold;">Dimension options</span><br/><div id="dimOptions">&nbsp;</div></td>
        </tr>
        <tr>
            <td style="vertical-align: top;"><span style="font-weight: bold;">Material options</span><br/><div id="specOptions">&nbsp;</div></td>
        </tr>
    </table>







</form>
<script>
    function useDim(object) {
        var string = $(object).attr("data-id");
        //alert(string);
        var theArray = string.split(",");
        //alert(theArray);
        $('#dimOD').val($.trim(theArray[0]));
        $('#dimPD').val($.trim(theArray[1]));
        $('#dimLength').val($.trim(theArray[2]));
        $('#dimDIN').val($.trim(theArray[3]));
        $('#dimHeadType').val($.trim(theArray[4]));
        $('#dimAngle').val($.trim(theArray[5]));
        $('#dimID').val($.trim(theArray[6]));
    }
    
    function useSpec(object) {
        var string = $(object).attr("data-id");
        //alert(string);
        var theArray = string.split(",");
        //alert(theArray);
        $('#materialCode').val($.trim(theArray[0]));
        $('#materialDesc').val($.trim(theArray[1]));
        $('#materialHardness').val($.trim(theArray[2]));
        $('#materialDIN').val($.trim(theArray[3]));
        $('#materialSpec').val($.trim(theArray[4]));
    }
        
</script>