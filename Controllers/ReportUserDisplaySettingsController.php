<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/ReportUserDisplaySettingsModel.php");
/**
 * Description of ReportUserDisplaySettingsController
 *
 * @author Archie
 */
class ReportUserDisplaySettingsController {
    public $reportUserDisplaySettingsModel;
    
    public function __construct()
    {
        $this->reportUserDisplaySettingsModel = new ReportUserDisplaySettingsModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    
    /**
     * getSettingsByID
     * @param INT $userID
     * @return ARRAY \ReportUserDisplaySettingsModel
     */
    public function getSettingsByID($userID) {
        return $this->reportUserDisplaySettingsModel->getSettingsByID($userID);
    }
    
    /**
     * setSettingsForUser
     * @param ARRAY $settingsArray
     * @return boolean
     */
    public function setSettingsForUser($settingsArray, $userID) {
        return $this->reportUserDisplaySettingsModel->setSettingsForUser($settingsArray, $userID);
    }
    
    /**
     * getAllSettings
     */
    public function getAllSettings() {
        return $this->reportUserDisplaySettingsModel->getAllSettings();
    }
}
