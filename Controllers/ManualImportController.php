<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/ManualImportModel.php");
/**
 * Description of ManualImportController
 *
 * @author Archie
 */
class ManualImportController {
    public $manualImportModel;
    
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->manualImportModel = new ManualImportModel();
    }
    
    /**
     * Invoke Method.
     */
    public function invoke()
    {
        //include(page);
        
    }
    
     /**
     * getFromManualCheckTblByTempID
     * @return row
     */
    public function getFromManualCheckTblByTempID ($tempID) {
        return $this->manualImportModel->getFromManualCheckTblByTempID($tempID);
    }
    
    /**
     * dropManualCheckTbl
     */
    public function dropManualCheckTbl() {
        return $this->manualImportModel->dropManualCheckTbl();
    }
    
    /**
     * deleteFromManualCheckTblByTempID
     * @param type $tempID
     * @return boolean
     */
    public function deleteFromManualCheckTblByTempID ($tempID) {
        return $this->manualImportModel->deleteFromManualCheckTblByTempID($tempID);
    }
    
    /**
     * createManualCustomers
     * @param type $tempID
     */
    public function createManualCustomers($tempID) {
        return $this->manualImportModel->createManualCustomers($tempID);
    }
    
    /**
     * createContacts
     */
    public function createManualContacts($tempID, $customerID = "") {
        return $this->manualImportModel->createManualContacts($tempID, $customerID);
    }
    
    /**
     * createInstallations
     */
    public function createManualInstallations($tempID, $customerID = "") {
        return $this->manualImportModel->createManualInstallations($tempID, $customerID);
    }
    
    
    /**
     * createTechnicalData
     */
    public function createManualTechnicalData($tempID, $installationID = "", $customerID = "") {
        return $this->manualImportModel->createManualTechnicalData($tempID, $installationID, $customerID);
    }
      
}
