<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/UserHistoryModel.php");
/**
 * Description of UserHistoryController
 *
 * @author Archie
 */
class UserHistoryController {
    
    /**
     *
     * @var type UserHistoryModel
     */
    public $userHistoryModel;
    
    /**
     * __construct function.
     */
    public function __construct()
    {
        $this->userHistoryModel = new UserHistoryModel();
    }
    
    /**
     * 
     */
    public function invoke()
    {
    }
    
    /**
     * Creates a new entry into the user history table.
     * @param INT $userID
     * @param DATETIME $datetime
     * @param ENUM $type
     * @param TEXT $comment
     * @return boolean
     */
    public function setUserHistory($userID, $datetime, $type, $comment) {
        return $this->userHistoryModel->setUserHistory($userID, $datetime, $type, $comment);
    }
    
    /**
     * Gets a list of user history items for user.
     * @param INT $userID
     * @return Array of User History Objects
     */
    public function getUserHistory($userID) {
        return $this->userHistoryModel->getUserHistory($userID);
    }
    
    
    /**
     * Gets all entries between the dates set.
     * @param DATE $startDate
     * @param DATE $endDate
     * @return Array of User History Objects
     */
    public function getAllUserHistoryBetweenDates($startDate, $endDate) {
        return $this->userHistoryModel->getAllUserHistoryBetweenDates($startDate, $endDate);
    }
    
    
    /**
     * lastSeawebUpdateAt
     * @return User History Object
     */
    public function lastSeawebUpdateAt() {
        return $this->userHistoryModel->lastSeawebUpdateAt();
    }
}
