<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/InstallationEngineModel.php");
/**
 * Description of InstallationEngineController
 *
 * @author Archie
 */
class InstallationEngineController {
    
    public $installationEngineModel;
    
    public function __construct()
    {
        $this->installationEngineModel = new InstallationEngineModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    /**
     * getEngineByProductID
     * @param type $productID
     * @return type
     */
    public function getEngineByProductID($productID) {
        return $this->installationEngineModel->getEngineByProductID($productID);
        
    }
    /**
     * Get installation engines by installation id
     * @param INT $installationID
     * @return Installation Engine Object
     */
    public function getInstallationEnginesByInstallationID($installationID) {
        return $this->installationEngineModel->getInstallationEnginesByInstallationID($installationID);
    }
    
    /**
     * getInstallationEngineByID
     * @param INT $engineID
     * @return type
     */
    public function getInstallationEngineByID($engineID) {
        return $this->installationEngineModel->getInstallationEngineByID($engineID);
    }
    
    
    /**
     * updateInstallationEngineObject
     * @param type $engineObject
     */
    public function updateInstallationEngineObject($engineObject) {
        return $this->installationEngineModel->updateInstallationEngineObject($engineObject);
    }
    
    /**
     * countOfEngines
     * @param INT $installationID
     * @return INT count of engines
     */
    public function countOfEngines($installationID) {
        return $this->installationEngineModel->countOfEngines($installationID);
    }
    
    /**
     * setNewEngine
     * @param Entity $engine
     * @return INT Insert ID
     */
    public function setNewEngine($engine) {
        return $this->installationEngineModel->setNewEngine($engine);
    }
    
    /**
     * getEnginesByCustomerID
     * @param type $customerID
     * @return type
     */
    public function getEnginesByCustomerID($customerID) {
        return $this->installationEngineModel->getEnginesByCustomerID($customerID);
    }
    
    /**
     * getEngineBySerialNumberAndInstallation
     * @param STRING $serialNumber
     * @param INT $installationID
     * @return type
     */
    public function getEngineBySerialNumberAndInstallation($serialNumber, $installationID) {
        return $this->installationEngineModel->getEngineBySerialNumberAndInstallation($serialNumber, $installationID);
    }
    
    /**
     * getEngineByInstallationIDAndPosition
     * @param INT $installationID
     * @param STRING $position
     * @return Installation Engine Object Array
     */
    public function getEngineByInstallationIDAndPosition($installationID, $position) {
        return $this->installationEngineModel->getEngineByInstallationIDAndPosition($installationID, $position);
    }
}
