<?php

include_once("Models/InstallationModel.php");
/**
 * Description of InstallationController
 * This contains all the different query requests that the campaign can be created on. 
 * This is a long list.
 *
 * @author Archie
 */
class InstallationController {
    public $installationModel;
    
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->installationModel = new InstallationModel();
    }
    
    /**
     * Invoke Method.
     */
    public function invoke()
    {
        //include(page);
        
    }
    
   
    
    
    /**
     * Get installation by its ID.
     * @param type $installationID
     * @return type Object Installation
     */
    public function getInstallationByInstallationID($installationID) {
        return $this->installationModel->getInstallationByInstallationID($installationID);
    }
    
    
    
    /**
     * getInstallationByIMO
     * @param type $imo
     * @return type
     */
    public function getInstallationByIMO($imo)
    {
        return $this->installationModel->getInstallationByIMO($imo);
    }
    
   
    
    /**
     * Get installations by name.
     * @param type $installationName
     * @return type
     */
    public function getInstallationsByInstallationName($installationName)
    {
        return $this->installationModel->getInstallationsByInstallationName($installationName);
    }
    
    /**
     * getInstallationsByInstallationNameAndCustomerID
     * @param type $installationName
     * @param type $customerID
     * @return type
     */
    public function getInstallationsByInstallationNameAndCustomerID($installationName, $customerID)
    {
        return $this->installationModel->getInstallationsByInstallationNameAndCustomerID($installationName, $customerID);
    }
    
    /**
     * 
     * @param type $installationObject
     * @return type
     */
    public function updateInstallationObject($installationObject) {
        return $this->installationModel->updateInstallationObject($installationObject);
    }
    
    /**
     * getAllInstallationsByCustomer
     * @param type $customerID
     * @return type
     */
    public function getAllInstallationsByCustomer($customerID) {
        return $this->installationModel->getAllInstallationsByCustomer($customerID);
    }
    
    /**
     * getCombinedFleetByCustomer
     * @param type $customerID
     * @return type
     */
    public function getCombinedFleetByCustomer($customerID) {
        return $this->installationModel->getCombinedFleetByCustomer($customerID);
    }
    
    /**
     * getAllArchviedInstallationsByCustomer
     * @param type $customerID
     * @return \Installation
     */
    public function getAllArchviedInstallationsByCustomer($customerID) {
        return $this->installationModel->getAllArchviedInstallationsByCustomer($customerID);
    }
    
    
    /**
     * createNewInstallationObject
     * @param type $object
     * @param type $customerID
     * @return type
     */
    public function createNewInstallationObject($object, $customerID) {
        return $this->installationModel->createNewInstallationObject($object, $customerID);
    }
    
    
    
    /**
     * getInstallationsWithConversationEntriesForAccountManager
     * @param type $accountManager
     * @return \Installation
     */
    public function getInstallationsWithConversationEntriesForAccountManager($accountManager) {
        return $this->installationModel->getInstallationsWithConversationEntriesForAccountManager($accountManager);
    }
    
    /**
     * 
     * @param type $userID
     * @return type
     */
    public function getInstallationsWithConversationEntries($userID) {
        return $this->installationModel->getInstallationsWithConversationEntries($userID);
    }
    
    
    /**
     * $ownerParentID
     * @param type $ownerParentID
     * @return type
     */
    public function getInstallationsByOwnerParentID($ownerParentID) {
        return $this->installationModel->getInstallationsByOwnerParentID($ownerParentID);
    }
            
            /**
     * getInstallationsByTechnicalManagerID
     * @param type $technicalManagerID
     * @return type
     */
    public function getInstallationsByTechnicalManagerID($technicalManagerID) {
        return $this->installationModel->getInstallationsByTechnicalManagerID($technicalManagerID);
    }
    
    
    /**
     * getSisterShipsByLeadIMO
     * @param type $imo
     * @return type
     */
    public function getSisterShipsByLeadIMO($imo) {
        return $this->installationModel->getSisterShipsByLeadIMO($imo);
    }
}
