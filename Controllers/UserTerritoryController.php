<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once("Models/UserTerritoryModel.php");
/**
 * Description of UserTerritoryController
 *
 * @author Archie
 */
class UserTerritoryController {
    public $userTerritoryModel;
    
    /**
     * 
     */
    public function __construct()
    {
        $this->userTerritoryModel = new UserTerritoryModel();
    }
    
    /**
     * 
     */
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * 
     * @param type $userID
     * @return type
     */
    public function getTerritory($userID) 
    {
        return $this->userTerritoryModel->getTerritory($userID);
    }
    
    /**
     * 
     * @param type $territoryObject
     * @return type
     */
    public function setTerritory($territoryObject) {
        return $this->userTerritoryModel->setTerritory($territoryObject);
    }
    
    
    /**
     * 
     * @param type $territoryID
     * @return type
     */
    public function deleteTerritory($territoryID)
    {
        return $this->userTerritoryModel->deleteTerritory($territoryID);
    }
    
    
    /**
     * getUserIDByTerritory
     * @param STRING $country
     * @return INT
     */
    public function getUserIDByTerritory($country) {
        return $this->userTerritoryModel->getUserIDByTerritory($country);
    }
}
