<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/SpecialSurveyHistoryDatesModel.php");
/**
 * Description of SpecialSurveyHistoryDatesController
 *
 * @author Archie
 */
class SpecialSurveyHistoryDatesController {
    public $specialSurveyHistoryDatesModel;
    
    public function __construct()
    {
        $this->specialSurveyHistoryDatesModel = new SpecialSurveyHistoryDatesModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * getSpecialSurveyDueDateByInstallationID
     * @param type $installationID
     * @return type
     */        
    public function getSpecialSurveyHistoryDatesByInstallationID($installationID) {
        return $this->specialSurveyHistoryDatesModel->getSpecialSurveyHistoryDatesByInstallationID($installationID);
    }
}
