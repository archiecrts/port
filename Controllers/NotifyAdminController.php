<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/NotifyAdminModel.php");
/**
 * Description of NotifyAdminController
 *
 * @author Archie
 */
class NotifyAdminController {
    public $notifyAdminModel;
    
    public function __construct()
    {
        $this->notifyAdminModel = new NotifyAdminModel();
    }
    
    public function invoke()
    {
        //include(page);   
    }
    
    /**
     * Set Notify Admin Enquiry into table.
     * @param type $notifyAdminObject
     * @return boolean
     */
    public function setEnquiry($notifyAdminObject) {
        return $this->notifyAdminModel->setEnquiry($notifyAdminObject);
    }
    
    
    /**
     * getAllEnquiriesByCategory.
     * @param INT $categoryID
     * @param STRING $status
     * @return \NotifyAdmin
     */
    public function getAllEnquiriesByCategory($categoryID, $status) {
        return $this->notifyAdminModel->getAllEnquiriesByCategory($categoryID, $status);
    }
    
    /**
     * getAllEnquiriesByCategoryForUser
     * @param INT $categoryID
     * @param STRING $status
     * @param INT $userID
     * @return type
     */
    public function getAllEnquiriesByCategoryForUser($categoryID, $status, $userID) {
        return $this->notifyAdminModel->getAllEnquiriesByCategoryForUser($categoryID, $status, $userID);
    }
}
