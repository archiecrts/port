<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once("Models/ReportOverviewModel.php");
/**
 * Description of ReportOverviewController
 *
 * @author Archie
 */
class ReportOverviewController {
    public $reportOverviewModel;
    /**
     * 
     */
    public function __construct()
    {
        $this->reportOverviewModel = new ReportOverviewModel();
    }
    
    /**
     * 
     */
    public function invoke()
    {
        
        
    }
    
    /**
     * 
     * @return type
     */
    public function getAllReportOverviews() {
        return $this->reportOverviewModel->getAllReportOverviews();
    }
    
    public function getAllReportOverviewsByUserID($userID) {
        return $this->reportOverviewModel->getAllReportOverviewsByUserID($userID);
    }
    
    /**
     * 
     * @param type $reportOverviewID
     * @return type
     */
    public function getSingleReportByReportOverviewID($reportOverviewID)
    {
        return $this->reportOverviewModel->getSingleReportByReportOverviewID($reportOverviewID);
    }
    
    /**
     * 
     * @param type $reportOverviewObject
     * @return type
     */
    public function setReportOverview($reportOverviewObject) 
    {
        return $this->reportOverviewModel->setReportOverview($reportOverviewObject);
    }
    
    /**
     * Update the name and dynamic flag of the report overview.
     * @param type $reportOverviewID
     * @param type $name
     * @param type $dynamicFlag
     * @return type
     */
    public function updateReportOverview($reportOverviewID, $name, $dynamicFlag)
    {
        return $this->reportOverviewModel->updateReportOverview($reportOverviewID, $name, $dynamicFlag);
    }
    
    
    
    
    /**
     * 
     * @return type
     */
    public function getCountActiveReports($userID, $level) {
        
        return $this->reportOverviewModel->getCountActiveReports($userID, $level);
    }
}
