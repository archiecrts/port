<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/EngineModelVersionModel.php");
/**
 * Description of EngineModelVersionController
 *
 * @author Archie
 */
class EngineModelVersionController {
    public $engineModelVersionModel;
    
    
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->engineModelVersionModel = new EngineModelVersionModel();
    }
    
    /**
     * Invoke Method.
     */
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * 
     * @param type $version
     * @param type $engineModelId
     * @return type
     */
    public function setEngineModelVersion($version, $engineModelId) {
        return $this->engineModelVersionModel->setEngineModelVersion($version, $engineModelId);
    }
    
    
    /**
     * 
     * @param type $versionID
     * @param type $newVersion
     * @return type
     */
    public function updateEngineModelVersion($versionID, $newVersion) {
        return $this->engineModelVersionModel->updateEngineModelVersion($versionID, $newVersion);
    }
    
}
