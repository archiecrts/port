<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/InsertSuppliersProductsFromCSVModel.php");
/**
 * Description of InsertSuppliersProductsFromCSVController
 *
 * @author Archie
 */
class InsertSuppliersProductsFromCSVController {
    public $insertSuppliersProductsFromCSVModel;
    
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->insertSuppliersProductsFromCSVModel = new InsertSuppliersProductsFromCSVModel();
    }
    
    /**
     * Invoke Method.
     */
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * importDataFromCSV
     * @param type $fileName
     * @param type $columnNameArray
     * @param type $headers
     * @param type $type
     * @return type
     */
    public function importProductDataFromCSV($fileName, $columnNameArray, $headers, $type) {
        return $this->insertSuppliersProductsFromCSVModel->importProductDataFromCSV($fileName, $columnNameArray, $headers, $type);
    }
}
