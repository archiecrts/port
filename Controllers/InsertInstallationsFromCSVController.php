<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/InsertInstallationsFromCSVModel.php");
/**
 * Description of InsertInstallationsFromCSVController
 *
 * @author Archie
 */
class InsertInstallationsFromCSVController {
    public $insertInstallationsFromCSVModel;
    
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->insertInstallationsFromCSVModel = new InsertInstallationsFromCSVModel();
    }
    
    /**
     * Invoke Method.
     */
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * getAllFromManualCheckTbl
     * @return row
     */
    public function getAllFromManualCheckTbl () {
        return $this->insertInstallationsFromCSVModel->getAllFromManualCheckTbl();
    }
    
    /**
     * dropManualCheckTbl
     */
    public function dropManualCheckTbl() {
        return $this->insertInstallationsFromCSVModel->dropManualCheckTbl();
    }
    /**
     * importDataFromCSV
     * @param STRING $fileName
     * @param STRING ARRAY $columnNameArray
     * @param BOOLEAN $headers
     * @param STRING $type
     * @return int
     */
    public function importDataFromCSV($fileName, $columnNameArray, $headers, $type) {
        return $this->insertInstallationsFromCSVModel->importDataFromCSV($fileName, $columnNameArray, $headers, $type);
    }
    /**
     * Insert using LOAD DATA the installation data from the CSV.
     * @param type $filename
     * @param type $columnNameArray
     * @return type
     */
    public function insertInstallationDataFromCSV($filename, $columnNameArray, $headers, $type) {
        
        return $this->insertInstallationsFromCSVModel->insertInstallationDataFromCSV($filename, $columnNameArray, $headers, $type);
    }
}
