<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/SupplierApprovalModel.php");
/**
 * Description of SupplierApprovalConstructor
 *
 * @author Archie
 */
class SupplierApprovalController {
    
    public $supplierApprovalModel;
    
    public function __construct()
    {
        $this->supplierApprovalModel = new SupplierApprovalModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * getSupplierApprovalBySupplierID
     * @param type $supplierID
     * @return type
     */
    public function getSupplierApprovalBySupplierID($supplierID) {
        return $this->supplierApprovalModel->getSupplierApprovalBySupplierID($supplierID);
    }
}
