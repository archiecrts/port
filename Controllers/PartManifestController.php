<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/PartManifestModel.php");
/**
 * Description of PartManifestController
 *
 * @author Archie
 */
class PartManifestController {
    
    public $partManifestModel;
    
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->partManifestModel = new PartManifestModel();
    }
    
    /**
     * Invoke Method.
     */
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * getMainfestByPartID
     * @param type $partID
     */
    public function getMainfestByPartID($partID) {
        return $this->partManifestModel->getMainfestByPartID($partID);
    }
    
    /**
     * getManifestByVersionID
     * @param INT $versionID
     * @return type
     */
    public function getManifestByVersionID($versionID) {
        return $this->partManifestModel->getManifestByVersionID($versionID);
    }
    
    /**
     * insertIntoManifestBySeriesID
     * @param type $seriesID
     * @param type $partCodeID
     * @param type $quantity
     * @return boolean
     */
    public function insertIntoManifestBySeriesID($seriesID, $partCodeID, $quantity) {
        return $this->partManifestModel->insertIntoManifestBySeriesID($seriesID, $partCodeID, $quantity);
    }
    
    /**
     * insertIntoManifestByVersionID
     * @param type $versionID
     * @param type $partCodeID
     * @param type $quantity
     * @return boolean
     */
    public function insertIntoManifestByVersionID($seriesID, $versionID, $partCodeID, $quantity) {
        return $this->partManifestModel->insertIntoManifestByVersionID($seriesID, $versionID, $partCodeID, $quantity);
    }
    
    /**
     * getMainfestByPartIDAndSeriesID
     * @param type $partID
     * @param type $seriesID
     * @return type
     */
    public function getMainfestByPartIDAndSeriesID($partID, $seriesID) {
        return $this->partManifestModel->getMainfestByPartIDAndSeriesID($partID, $seriesID);
    }
    
    /**
     * matchManifestEntry
     * @param type $partID
     * @param type $versionID
     * @param type $quantity
     * @return type
     */
    public function matchManifestEntry($partID, $versionID, $quantity) {
        return $this->partManifestModel->matchManifestEntry($partID, $versionID, $quantity);
    }
    
    /**
     * insertManifestObject
     * @param type $manifest
     * @return boolean
     */
    public function insertManifestObject($manifest) {
        return $this->partManifestModel->insertManifestObject($manifest);
    }
}
