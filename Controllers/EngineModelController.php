<?php

include_once("Models/EngineModelModel.php");

/**
 * Description of EngineModelController
 *
 * @author Archie
 */
class EngineModelController {
    public $engineModelModel;
    
    
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->engineModelModel = new EngineModelModel();
    }
    
    /**
     * Invoke Method.
     */
    public function invoke()
    {
        //include(page);
        
    }
    
    
    /**
     * 
     * @return type
     */
    public function getDistinctEngineModels()
    {
        return $this->engineModelModel->getDistinctEngineModels();
    }
    
    /**
     * 
     * @return type
     */
    /*public function getDistinctAuxEngineModels()
    {
        return $this->engineModelModel->getDistinctAuxEngineModels();
    }*/
    
    /**
     * 
     * @param type $modelDescription
     * @return type
     */
    public function setEngineModelEngineDB($modelDescription, $engineBuilderID) {
        return $this->engineModelModel->setEngineModelEngineDB($modelDescription, $engineBuilderID);
    }
    
    
    /**
     * 
     * @param type $dbModelID
     * @param type $newModel
     * @return type
     */
    public function updateEngineDBModel($dbModelID, $newModel) {
        return $this->engineModelModel->updateEngineDBModel($dbModelID, $newModel);
    }
    
    
    /**
     * 
     * @param type $manufacturerID
     * @return type
     */
    public function getModelsByManufacturerID($manufacturerID) {
        return $this->engineModelModel->getModelsByManufacturerID($manufacturerID);
    }
    
    /**
     * getModelsByModelID
     * @param type $modelID
     * @return boolean
     */
    public function getModelsByModelID($modelID) {
        return $this->engineModelModel->getModelsByModelID($modelID);
    }
}
