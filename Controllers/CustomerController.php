<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/CustomerModel.php");
/**
 * Description of CustomerController
 *
 * @author Archie
 */
class CustomerController {
   
    public $customerModel;
    
    public function __construct()
    {
        $this->customerModel = new CustomerModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    /**
     * Get Customer Record by ID.
     * @param INT $customerID
     * @return Customer Object
     */
    public function getCustomerByID($customerID) {
        return $this->customerModel->getCustomerByID($customerID);
    }
    
    /**
     * getCustomerByName
     * @param type $customerName
     * @return type
     */
    public function getCustomerByName($customerName) {
        return $this->customerModel->getCustomerByName($customerName);
    }
    
    
    /**
     * getAllCustomers
     * @return array of customer objects
     */
    public function getAllCustomers() {
        return $this->customerModel->getAllCustomers();
    }
    
    /**
     * getCustomerIDByInstallationID
     * @param INT $installationID
     * @return type
     */
    public function getCustomerIDByInstallationID($installationID) {
        return $this->customerModel->getCustomerIDByInstallationID($installationID);
    }
    
    /**
     * updateCustomerObject
     * @param Entity Object $customerObject
     * @return boolean
     */
    public function updateCustomerObject($customerObject) {
        return $this->customerModel->updateCustomerObject($customerObject);
    }
    
    /**
     * setCustomerObject
     * @param Entity Object $customerObject
     * @return boolean
     */
    public function setCustomerObject($customerObject) {
        return $this->customerModel->setCustomerObject($customerObject);
    }
    
    /**
     * getInstallationsWithConversationEntriesForAccountManager
     * @param type $accountManager
     * @return \Installation
     */
    public function getCustomersWithConversationEntriesForAccountManager($accountManager) {
        return $this->customerModel->getInstallationsWithConversationEntriesForAccountManager($accountManager);
    }
    
    /**
     * 
     * @param type $userID
     * @return type
     */
    public function getCustomersWithConversationEntries($userID) {
        return $this->customerModel->getInstallationsWithConversationEntries($userID);
    }
    
    /**
     * getCustomerAddressesByCountryArray
     * @param type $country
     * @param type $area
     * @param type $continent
     * @return type
     */
    public function getCustomerByCountryArray($country, $area, $continent, $customerType) {
        return $this->customerModel->getCustomerByCountryArray($country, $area, $continent, $customerType);
    }
    
    /**
     * getCustomerByProductConfiguration
     * @param type $designerID
     * @param type $seriesID
     * @param type $cylinderCount
     * @param type $cylinderConfiguration
     */
    public function getCustomerByProductConfiguration($designerID, $seriesID, $cylinderCount, $cylinderConfiguration) {
       return $this->customerModel->getCustomerByProductConfiguration($designerID, $seriesID, $cylinderCount, $cylinderConfiguration);
    }
    
    /**
     * getCustomerParentByCode
     * @param type $customerSeawebCode
     * @return type
     */
    public function getCustomerParentByCode($customerSeawebCode) {
        return $this->customerModel->getCustomerParentByCode($customerSeawebCode);
    }
    
    /**
     * getAllCustomerChildren
     * @param type $parentCode
     * @return type
     */
    public function getAllCustomerChildren($parentCode) {
        return $this->customerModel->getAllCustomerChildren($parentCode);
    }
        
}
