<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/EnquiryTypeModel.php");
/**
 * Description of EnquiryTypeController
 *
 * @author alexandra
 */
class EnquiryTypeController {
    public $enquiryTypeModel;
    
    public function __construct()
    {
        $this->enquiryTypeModel = new EnquiryTypeModel();
    }
    
    public function invoke()
    {
        
    }

     /**
     * getAllEnquiryTypes
     * @return Array | null
     */
    public function getAllEnquiryTypes() {
        return $this->enquiryTypeModel->getAllEnquiryTypes();
    }
    
    /**
     * getEnquiryTypeByID
     * @param INT $typeID
     * @return array | null
     */
    public function getEnquiryTypeByID($typeID) {
        return $this->enquiryTypeModel->getEnquiryTypeByID($typeID);
    }
}
