<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/EnginePartsManifestModel.php");
/**
 * Description of EnginePartsManifestController
 *
 * @author Archie
 */
class EnginePartsManifestController {
    public $enginePartsManifestModel;
    
    
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->enginePartsManifestModel = new EnginePartsManifestModel();
    }
    
    /**
     * Invoke Method.
     */
    public function invoke()
    {
        //include(page);
        
    }
}
