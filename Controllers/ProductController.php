<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/ProductModel.php");
/**
 * Description of ProductController
 *
 * @author Archie
 */
class ProductController {
    public $productModel;
    
    public function __construct()
    {
        $this->productModel = new ProductModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * getProductByID
     * @param INT $productID
     * @return OBJECT PRODUCT
     */
    public function getProductByID($productID) {
        return $this->productModel->getProductByID($productID);
    }
    /**
     * getProductsByInstallationID
     * @param type $installationID
     * @return type
     */
    public function getProductsByInstallationID($installationID) {
        return $this->productModel->getProductsByInstallationID($installationID);
    }
    
    /**
     * getProductsByInstallationIDAndProductType
     * @param type $installationID
     * @param type $productType
     * @return type
     */
    public function getProductsByInstallationIDAndProductType($installationID, $productType) {
        return $this->productModel->getProductsByInstallationIDAndProductType($installationID, $productType);
    }
    
    /**
     * summaryOfEnginesByInstallationID
     * @param type $installationID
     * @return type
     */
    public function summaryOfEnginesByInstallationID($installationID) {
        return $this->productModel->summaryOfEnginesByInstallationID($installationID);
    }
    /**
     * countOfProducts
     * @param INT $installationID
     * @return INT count of products
     */
    public function countOfProducts($installationID) {
        return $this->productModel->countOfProducts($installationID);
    }
    
    /**
     * countOfProductsByType
     * @param type $installationID
     * @param type $typeID
     * @return type
     */
    public function countOfProductsByType($installationID, $typeID) {
        return $this->productModel->countOfProductsByType($installationID, $typeID);
    }
    
    /**
     * productSummaryByCustomerID
     * @param INT $customerID
     * @return $row array
     */
    public function productSummaryByCustomerID($customerID) {
        return $this->productModel->productSummaryByCustomerID($customerID);
    }
    /**
     * setNewProduct
     * @param type $product
     * @return boolean
     */
    public function setNewProduct($product) {
        return $this->productModel->setNewProduct($product);
    }
    
    
    /**
     * updateProductObject
     * @param type $product
     * @return boolean
     */
    public function updateProductObject($product) {
        return $this->productModel->updateProductObject($product);
    }
        
}
