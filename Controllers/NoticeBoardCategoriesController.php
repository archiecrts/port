<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/NoticeBoardCategoriesModel.php");
/**
 * Description of NoticeBoardCategoriesController
 * Controller gives access to model.
 * @author Archie
 */
class NoticeBoardCategoriesController {
    public $noticeBoardCategoriesModel;
    
    public function __construct()
    {
        $this->noticeBoardCategoriesModel = new NoticeBoardCategoriesModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * Gets an array of categories.
     * @return Array of NoticeBoardCategory Objects.
     */
    public function getCategories()
    {
        return $this->noticeBoardCategoriesModel->getCategories();
    }
    
    /**
     * getCategoryByID Returns an object based on ID
     * @param INT $categoryID
     * @return Object
     */
    public function getCategoryByID($categoryID)
    {
        return $this->noticeBoardCategoriesModel->getCategoryByID($categoryID);
    }
    
    /**
     * Sets a notice board category object.
     * @param STRING $category
     * @param BOOLEAN $locked
     * @return BOOLEAN
     */
    public function setNoticeBoardCategory($category, $locked)
    {
        return $this->noticeBoardCategoriesModel->setNoticeBoardCategory($category, $locked);
    }
    
    
    /**
     * Get a category by name.
     * @param String $name
     * @return \NoticeBoardCategories
     */
    public function getCategoryByName($name) {
        return $this->noticeBoardCategoriesModel->getCategoryByName($name);
    }
}
