<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/PartDimensionsModel.php");
/**
 * Description of PartDimensionsController
 *
 * @author Archie
 */
class PartDimensionsController {
    public $partDimensionsModel;
    
    public function __construct()
    {
        $this->partDimensionsModel = new PartDimensionsModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    
    /**
     * getDimensionsByID
     * @param type $dimID
     * @return type
     */
    public function getDimensionsByID($dimID) {
        return $this->partDimensionsModel->getDimensionsByID($dimID);
    }
    
    /**
     * insertDimensions
     * @param type $dimension
     * @return boolean
     */
    public function insertDimensions($dimension) {
        return $this->partDimensionsModel->insertDimensions($dimension);
    }
    
    /**
     * getDimensionByObjectCriteria
     * @param type $dimension
     * @return type
     */
    public function getDimensionByObjectCriteria($dimension) {
        return $this->partDimensionsModel->getDimensionByObjectCriteria($dimension);
    }
}
