<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/NotifyAdminCategoryModel.php");
/**
 * Description of NotifyAdminCategoryController
 *
 * @author Archie
 */
class NotifyAdminCategoryController {
  
    public $notifyAdminCategoryModel;
    
    public function __construct()
    {
        $this->notifyAdminCategoryModel = new NotifyAdminCategoryModel();
    }
    
    public function invoke()
    {
        //include(page);   
    }
    
    /**
     * Get All Categories of Notify Admin.
     * @return \NotifyAdminCategory
     */
    public function getAllCategories() {
        return $this->notifyAdminCategoryModel->getAllCategories();
    }
    
}
