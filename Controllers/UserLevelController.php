<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/UserLevelModel.php");
/**
 * Description of UserLevelController
 *
 * @author Archie
 */
class UserLevelController {
    public $userLevelModel;
    
    public function __construct()
    {
        $this->userLevelModel = new UserLevelModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * getUserLevels
     * @return Array UserLevel
     */
    public function getUserLevels() {
        return $this->userLevelModel->getUserLevels();
    }
    
    /**
     * getUserLevelByID
     * @param INT $levelID
     * @return UserLevel
     */
    public function getUserLevelByID($levelID) {
        return $this->userLevelModel->getUserLevelByID($levelID);
    }
}
