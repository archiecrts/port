<?php

/*
 * This project and all contents therein are the property of Simplex-Turbulo Diesel and Marine Group
 * Author: Archie Curtis
 */

/**
 * Description of MasterController
 *
 * @author Archie
 */
class MasterController {

    public function __construct() {
        
    }

    /**
     * Decide which page to send the user to.
     */
    public function invoke() {

        if (!isset($_SESSION['server']) || ($_SESSION['server'] == "")) {
            $_SESSION['server'] = basename(getcwd());
        }
        $portal = "mr";
        // This is checking that the users session has been created.
        // if not we force the redirect to access control.

        if (!isset($_SESSION['name'])) {
            $portal = "accessControl";
        } else {
            if (!isset($_SESSION['maintenance']) && ($_GET['portal'] != 'maintenance')) {
                // check to see if this is the first login for this user.
                if ($_SESSION['firstLogin'] == 1) {
                    $portal = "firstLogin";
                } else {
                    if (isset($_GET['portal'])) {
                        $portal = $_GET['portal'];
                    }
                }
            } else {
                print "<h4>IN MAINTENANCE</h4>";
                if (isset($_GET['action']) && ($_GET['action'] == 'set')) {
                    include 'Views/Dashboard/updateMaintenanceSwitch.php';
                } else if (isset($_GET['action']) && ($_GET['action'] == 'unset')) {
                    include 'Views/Dashboard/updateMaintenanceSwitch.php';
                    include 'Views/dashboardPortal.php';
                }
                if (isset($_SESSION['maintenance']) && ($_SESSION['level'] != '1')) {
                    $portal = "maintenance";
                } else {
                    $portal = $_GET['portal'];
                }
            }
        }

        switch ($portal) {
            case "mr":
                include 'Views/marketResearchPortal.php';
                break;
            case "uploads":
                include 'Views/uploadsPortal.php';
                break;
            case "suppliers":
                include 'Views/suppliersPortal.php';
                break;
            case "intranet":
                include 'Views/marketResearchPortal.php';
                break;
            case "users":
                include 'Views/usersPortal.php';
                break;
            case "parts":
                include 'Views/partsPortal.php';
                break;
            case "products":
                include 'Views/productsPortal.php';
                break;
            case "purchasing":
                include 'Views/purchasingPortal.php';
                break;
            case "quotes":
                include 'Views/sopPortal.php';
                break;
            case "crm":
                include 'Views/crmPortal.php';
                break;
            case "accounts":
                include 'Views/accountsPortal.php';
                break;
            case "firstLogin":
                include 'Views/firstLogin.php';
                break;
            case "myAccount":
                include 'Views/myAccountPortal.php';
                break;
            case "logout":
                include 'Views/logout.php';
                break;
            case "accessControl":
                include 'Views/accessControl.php';
                break;
            case "notifyAdmin":
                include 'Views/notifyAdminPortal.php';
                break;
            case "dashboard":
                include 'Views/dashboardPortal.php';
                break;
            case "maintenance":
                include 'Views/maintenance.php';
                break;
            case "autoUpdate":
                include 'Views/autoUpdateSeawebScript.php';
                break;
            default:
                include 'Views/marketResearchPortal.php';
        }
    }

}
