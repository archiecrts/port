<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/SpecialSurveyDueDateModel.php");
/**
 * Description of SpecialSurveyDueDateController
 *
 * @author Archie
 */
class SpecialSurveyDueDateController {
    public $specialSurveyDueDateModel;
    
    public function __construct()
    {
        $this->specialSurveyDueDateModel = new SpecialSurveyDueDateModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    
    /**
     * getSpecialSurveyDueDateByInstallationID
     * @param type $installationID
     * @return type
     */        
    public function getSpecialSurveyDueDateByInstallationID($installationID) {
        return $this->specialSurveyDueDateModel->getSpecialSurveyDueDateByInstallationID($installationID);
    }
    
    /**
     * getSpecialSurveyDueDates
     * @param type $dateOne
     * @param type $dateTwo
     * @return type
     */
    public function getSpecialSurveyDueDates($dateOne, $dateTwo) {
        return $this->specialSurveyDueDateModel->getSpecialSurveyDueDates($dateOne, $dateTwo);
    }
        
}
