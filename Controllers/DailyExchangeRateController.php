<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/DailyExchangeRateModel.php");
/**
 * Description of DailyExchangeRateController
 *
 * @author Archie
 */
class DailyExchangeRateController {
    public $dailyExchangeRateModel;
    
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->dailyExchangeRateModel = new DailyExchangeRateModel();
    }
    
    /**
     * Invoke Method.
     */
    public function invoke()
    {
        //include(page);
        
    }
    
    
    
    /**
     * getAllDailyRates
     * @return type
     */
    public function getAllDailyRates() {
        return $this->dailyExchangeRateModel->getAllDailyRates();
    }
    
    /**
     * updateDailyRate
     * @param INT $rateID
     * @param FLOAT $rate
     * @return boolean
     */
    public function updateDailyRate($rateID, $rate) {
        return $this->dailyExchangeRateModel->updateDailyRate($rateID, $rate);
    }
    
    /**
     * matchCurrencyCodesHMRCToDaily
     * @param type $currencyCode
     * @return type
     */
    public function matchCurrencyCodesHMRCToDaily($currencyCode) {
        return $this->dailyExchangeRateModel->matchCurrencyCodesHMRCToDaily($currencyCode);
    }
}
