<?php

include_once("Models/NoticeBoardModel.php");
/**
 * Description of NoticeBoardController
 *
 * @author Archie
 */
class NoticeBoardController {
    
    /**
     *
     * @var type 
     */
    public $noticeBoardModel;
    
    /**
     * __construct function.
     */
    public function __construct()
    {
        $this->noticeBoardModel = new NoticeBoardModel();
    }
    
    /**
     * 
     */
    public function invoke()
    {
        
    }
    
    
    /**
     * getTrainingNotices
     * @return array
     */
    public function getTrainingNotices() {
        return $this->noticeBoardModel->getTrainingNotices();
    }
    
    /**
     * Get all notices by descending date order.
     * @return array
     */
    public function getAllNoticesDescendingOrder() {
        return $this->noticeBoardModel->getAllNoticesDescendingOrder();
    }
    
    /**
     * getAllNoticesByCategoryDescendingOrder.
     * @param INT $categoryID
     * @return array NoticeBoard Objects
     */
    public function getAllNoticesByCategoryDescendingOrder($categoryID) {
        return $this->noticeBoardModel->getAllNoticesByCategoryDescendingOrder($categoryID);
    }
    
    /**
     * setNewNoticeArticle
     * @param type $noticeBoardObject
     * @return type
     */
    public function setNewNoticeArticle($noticeBoardObject) {
        return $this->noticeBoardModel->setNewNoticeArticle($noticeBoardObject);
    }
    
    /**
     * 
     * @param NoticeBoardObject $noticeBoardObject
     * @return Boolean
     */
    public function setNewNoticeArticleNoDocID($noticeBoardObject) {
        return $this->noticeBoardModel->setNewNoticeArticleNoDocID($noticeBoardObject);
    }
    /**
     * getArticleByID
     * @param type $articleID
     * @return type
     */
    public function getArticleByID($articleID) {
        return $this->noticeBoardModel->getArticleByID($articleID);
    }
    
    
    /**
     * updateNoticeArticle
     * @param type $noticeBoardObject
     * @return type
     */
    public function updateNoticeArticle($noticeBoardObject) {
        return $this->noticeBoardModel->updateNoticeArticle($noticeBoardObject);
    }
    
    
    /**
     * getHistoryArrayMonthsYears
     * @return type
     */
    public function getHistoryArrayMonthsYears() {
        return $this->noticeBoardModel->getHistoryArrayMonthsYears();
    }
    
    
    /**
     * getArticleByMonthAndYear
     * @param type $month
     * @param type $year
     * @return type
     */
    public function getArticleByMonthAndYear($month, $year) {
        return $this->noticeBoardModel->getArticleByMonthAndYear($month, $year);
    }
    
    /**
     * getArticleByMonthAndYearAndCategory
     * @param STRING $month
     * @param STRING $year
     * @param INT $category
     * @return Array of NoticeBoard Objects
     */
    public function getArticleByMonthAndYearAndCategory($month, $year, $category) {
        return $this->noticeBoardModel->getArticleByMonthAndYearAndCategory($month, $year, $category);
    }
    
    /**
     * Counts the number of times a category is used.
     * @param INT $categoryID
     * @return INT
     */
    public function getCountOfCategoryUsage($categoryID) {
        return $this->noticeBoardModel->getCountOfCategoryUsage($categoryID);
    }
    
    
    /**
     * This counts the number of articles a user has awaiting authorisation from above.
     * @param INT $userID
     * @return INT
     */
    public function countArticlesAwaitingAuthorisation($userID) {
        return $this->noticeBoardModel->countArticlesAwaitingAuthorisation($userID);
    }
    
    
    /**
     * This counts the number of all articles awaiting authorisation from above.
     * @return INT
     */
    public function countAllArticlesAwaitingAuthorisation() {
        return $this->noticeBoardModel->countAllArticlesAwaitingAuthorisation();
    }
    
    
    /**
     * 
     * @return \NoticeBoard
     */
    public function getAllUnauthorisedNoticesDescendingOrder() {
        return $this->noticeBoardModel->getAllUnauthorisedNoticesDescendingOrder();
    }
    
    
    /**
     * 
     * @param type $noticeBoardID
     * @return type
     */
    public function updateArticleToAuthorised($noticeBoardID) {
        return $this->noticeBoardModel->updateArticleToAuthorised($noticeBoardID);
    }
}
