<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/UpdateSeawebDetailsModel.php");
/**
 * Description of UpdateSeawebDetailsController
 *
 * @author Archie
 */
class UpdateSeawebDetailsController {
    public $updateSeawebDetailsModel;
    
    public function __construct()
    {
        $this->updateSeawebDetailsModel = new UpdateSeawebDetailsModel();
    }
    
    public function invoke()
    {
        //include(page);
        
    }
    
    /**
     * getAllUpdatedCustomerDetails
     * @return array of updateCustomer objects
     */
    public function getAllUpdatedCustomerDetails() {
        return $this->updateSeawebDetailsModel->getAllUpdatedCustomerDetails();
    }
    
    
    /**
     * getNewCustomerRecord
     * @return \Customer
     */
    public function getNewCustomerRecord() {
        return $this->updateSeawebDetailsModel->getNewCustomerRecord();
    }
    
    /**
     * getNewCustomerAddressRecord
     * @return \CustomerAddress
     */
    public function getNewCustomerAddressRecord() {
        return $this->updateSeawebDetailsModel->getNewCustomerAddressRecord();
    }
    
    /**
     * getNewCustomerContactRecord
     * @return \CustomerContact
     */
    public function getNewCustomerContactRecord() {
        return $this->updateSeawebDetailsModel->getNewCustomerContactRecord();
    }
        
    
    /**
     * getAllUpdatedShipDetails
     * @return array of updated ship detail objects
     */
    public function getAllUpdatedShipDetails() {
        return $this->updateSeawebDetailsModel->getAllUpdatedShipDetails();
    }
    
    /**
     * getNewCustomerRecordByID
     * @param type $tempID
     * @return \Customer
     */
    public function getNewCustomerRecordByID($tempID) {
        return $this->updateSeawebDetailsModel->getNewCustomerRecordByID($tempID);
    }
    
    /**
     * getNewCustomerAddressRecordByID
     * @param type $tempID
     * @return \Customer
     */
    public function getNewCustomerAddressRecordByID($tempID) {
        return $this->updateSeawebDetailsModel->getNewCustomerAddressRecordByID($tempID);
    }
    
    /**
     * getNewShipRecordByID
     * @param type $tempID
     * @return \Installation
     */
    public function getNewShipRecordByID($tempID) {
        return $this->updateSeawebDetailsModel->getNewShipRecordByID($tempID);
    }
        
    /**
     * updateduplicateRecordByType
     * @param type $type
     * @param type $record
     * @return boolean
     */
    public function updateduplicateRecordByType($type, $record) {
        return $this->updateSeawebDetailsModel->updateduplicateRecordByType($type, $record);
    }
    
    /**
     * updateDuplicateShipRecord
     * @param type $record
     * @return boolean
     */
    public function updateDuplicateShipRecord($record) {
        return $this->updateSeawebDetailsModel->updateDuplicateShipRecord($record);
    }
    
    /**
     * removeCompleteDuplicatesFromCustomer
     */
    public function removeCompleteDuplicatesFromCustomer() {
        return $this->updateSeawebDetailsModel->removeCompleteDuplicatesFromCustomer();
    }
    
    /**
     * removeCompleteDuplicatesFromShipData
     */
    public function removeCompleteDuplicatesFromShipData() {
        return $this->updateSeawebDetailsModel->removeCompleteDuplicatesFromShipData();
    }
        
        
}
