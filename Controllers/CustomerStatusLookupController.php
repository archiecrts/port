<?php

/*
 * This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
 * Author: Archie Curtis
 */
include_once("Models/CustomerStatusLookupModel.php");
/**
 * Description of CustomerStatusLookupController
 *
 * @author Archie
 */
class CustomerStatusLookupController {
    public $customerStatusLookupModel;
    
    public function __construct()
    {
        $this->customerStatusLookupModel = new CustomerStatusLookupModel();
    }
    
    public function invoke() {}
    
    /**
     * getCustomerStatusLookupByID
     * @param INT $customerStatusLookupID
     * @return Customer Status Lookup Object
     */
    public function getCustomerStatusLookupByID($customerStatusLookupID) {
        return $this->customerStatusLookupModel->getCustomerStatusLookupByID($customerStatusLookupID);
    }
    
    /**
     * getAllCustomerStatusLookupValues
     * @return Array of Customer Status Lookup Objects
     */
    public function getAllCustomerStatusLookupValues() {
        return $this->customerStatusLookupModel->getAllCustomerStatusLookupValues();
    }
    
    /**
     * setNewCustomerStatusLookupObject
     * @param STRING $status
     * @return INT | NULL
     */
    public function setNewCustomerStatusLookupObject($status) {
        return $this->customerStatusLookupModel->setNewCustomerStatusLookupObject($status);
    }
}
