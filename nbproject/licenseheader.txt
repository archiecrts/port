<#if licenseFirst??>
${licenseFirst}
</#if>
${licensePrefix}This project and all parts therein are the property of Simplex-Turbulo Diesel and Marine Group.
${licensePrefix}Author: Archie Curtis
<#if licenseLast??>
${licenseLast}
</#if>